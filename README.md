carrier ahu designer

#项目结构说明


- ahu-db:数据库脚本
- ahu-common:通用公共包
- ahu-service-api:业务接口规范定义包
- ahu-service:业务接口实现包
- ahu-rest:供前端使用的rest api包
- ahu-portal:前端包
- ahu-dao:数据库访问层包装

#项目文档地址
https://www.teambition.com/project/58b3f6cd8c431c69028022f3


## Pull the Project
--拉取项目你要先安装[git](https://git-scm.com/downloads),在官网上选择一个相应系统的安装包进行安装。[给予百度具体的图文介绍](https://jingyan.baidu.com/article/9f7e7ec0b17cac6f2815548d.html)

--安装完成后在 开始菜单里找到“Git”->“Git Bash”  蹦出命令行窗口的东西 恭喜你  已经好啦

--如果你已经熟悉并且喜欢git 命令请忽略下面的介绍.

----------

1. 推荐 使用tortoise git（[乌龟git](https://download.tortoisegit.org/tgit/)）另附中文包

2. 在官网中选择最新的版本 选择 32  还64位下载  滑倒下方下载Chinese, simplified  中文包

3. 注意要先安装主程序  不要启动 然后在安装 中文包 不停next就好了

4. 启动小乌龟后 右键→tortogit→setting，把language项改为中文，确定即可

-- git一般流程 右键-> Git Clone -> 输入url克隆-> commit 提交 -> pull拉取 -> 是否冲突 resolve -> 推送 push

----------

# Setting up the Project

First you'll need [Node.js](https://nodejs.org) and the package manager that comes with npm.

Once you've got that working, head to the command line where we'll set up our project.


# 前端的安装 #


--首先你需要[Node.js安装包](https://nodejs.org/en/download/) 下载 .msi 文件
    <li>下面开始我们的安装吧
    <li>打开下载的文件，点击Run运行
    <li>勾选协议 一路next(下一步)  当然可以修改安装的路径
    <li>最后点击 Install（安装） 开始安装Node.js<br />

--**按win+R 输入cmd   cd进入 : 文件/arrierAHU\carrier-ahu-designer\ahu-portal4 或者 找到ahu-portal4文件位置复制文件路径全选输入 cmd**<br />	

> 在Node中已经默认为安装npm软件包管理器，只需要按照下面输入命令


    $  npm install -g cnpm --registry=https://registry.npm.taobao.org

    $  cnpm install --global yarn

    $  yarn config set registry https://registry.npm.taobao.org

    $  yarn config set sass-binary-site http://npm.taobao.org/mirrors/node-sass

    $  yarn install

    $  yarn start

# 后端的安装 #


--下载[JDK8(32-bit)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "地址")并安装，安装完成后配置[Java8环境](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "地址") 注意在下载它的时候要 勾选  *Accept License Agreement*  允许他的协议你才可以下载

----------

1. 安装过程中会出现两次 安装提示. 第一次是安装 jdk,第二次是安装 jre.(建议安装在同一个java文件下的不同文件夹  两个都是同一个文件夹会报错)

2. 安装完毕后找到你安装的路径  Java\jdk  复制路径

3. 点击 我的电脑→属性→高级系统设置→高级→环境变量

4. 环境变量->新建 JAVA_HOME 变量  将复制路径放入 变量值

5. 新建 CLASSPATH 变量  变量值为 .;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar （注意前面有一个点）

6. 找到	Path变量	编辑 在变量值最后输入  %JAVA_HOME%\bin;%JAVA_HOME%\jre\bin; （注意变量值后面要是没有 ；将其加上，win10不用理会）

> 按win+R 输入cmd  在命令行输入  java -version（中间有空格）显示出你的java版本号  恭喜你 配置跟安装已经成功

----------
--下面我们将会进行安装[gradle](https://gradle.org/install/#manually)


1. 找到手动安装  进入下载 选择安装路径记得在哪

2. 新建环境变量 GRADLE_HOME，变量值为你安装的路径

3. 修改环境变量 Path，在后面添加  %GRADLE_HOME%\BIN;

4. win+R 输入cmd 进入命令行面板 输入命令行 gradle -v  查看是否安装成功

----------
--下面就是到最后启动项目推荐有两种方法 根据自己喜好使用

1. 使用工具启动
	- 下载自己电脑对应版本 [eclipse](http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen3),并解压缩内容到*D:\eclipse*
	- 下载 [lombok](https://projectlombok.org/download) 到 *D:\eclipse\lombok.jar*
	- 配置文件*D:\eclipse\eclipse.ini*中添加如下内容:
<br />  `-javaagent:D:\eclipse\lombok.jar`
<br />  `-Xbootclasspath/a:lombok.jar`
	- 启动eclipse, 选择Window->Preferences->Java->Installed JREs, 将JRE8 (32-bit)环境添加进去，并勾选为默认环境。
	- 选择File->Import->Gradle Project, 目录选择 \CarrierAHU\carrier-ahu-designer，导入;
<br />  `导入后会出现8个模块,其中如果ahu-common有编译错误(需要使用32bit jre1.8编译), 请把JRE从项目中移除，再重新添加进去.`
	- 打开ahu-rest\项目下AHUApplication.java文件， 右键Run AS->Java Application启动后端服务.

2. 使用命令行启动

		$  cd CarrierAHUCode\carrier-ahu-designer
	
		$  gradlew build --refresh-dependencies
	
		$  cd ahu-rest
		
		$  gradle -x clean build
		
		$  gradle bootRun


> 完成最后一步后会停留在96% 等你要关闭的时候  ctrl+c就可以啦 以后打开就可以

> $ cd ahu-rest  

> $ gradle bootRun


Now open up [http://localhost:8080](http://localhost:8080)

You should see a "Brand" message in the browser.