package com.carrier.ahu.app.util;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * AHU Upgrade utility class.
 * 
 * Created by Braden Zhou on 2018/05/25.
 */
@Slf4j
public class AhuUpgradeUtils {

    private static final String AHU_LIB_FOLDER = "lib/";
    private static final String LIB_FILE_EXTENSION = ".jar";
    private static final String AHU_LIB_PREFIX = "ahu-";

    static Method addURL = null;

    static {
        try {
            addURL = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
            addURL.setAccessible(true);
        } catch (Exception e) {
            log.error("Failed to get addURL method from URLClassLoader.", e);
        }
    }

    public static Object loadJar(String jarFile) {
        try {
            File file = new File(jarFile);
            if (!file.exists()) {
                throw new RuntimeException(jarFile + " not exists.");
            }
            addURL.invoke(ClassLoader.getSystemClassLoader(), new Object[] { file.toURI().toURL() });
        } catch (Exception e) {
            log.error("Failed to load jar file.", e);
        }
        return null;
    }

    public static List<String> getLibraries() throws Exception {
        Map<String, String> libraries = new HashMap<>();
        File libDir = new File(AHU_LIB_FOLDER);
        if (libDir.exists()) {
            String[] ahuLibFileNames = libDir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.endsWith(LIB_FILE_EXTENSION) && name.startsWith(AHU_LIB_PREFIX)) {
                        return true;
                    }
                    return false;
                }
            });
            for (String ahuLibFileName : ahuLibFileNames) {
                int versionIndex = ahuLibFileName.lastIndexOf("-");
                String libName = ahuLibFileName.substring(0, versionIndex);
                String libVersion = ahuLibFileName.replace(LIB_FILE_EXTENSION, "").substring(versionIndex + 1);
                if (libraries.containsKey(libName)) {
                    String otherLibVersion = libraries.get(libName);
                    if (compareVersions(libVersion, otherLibVersion) > 0) {
                        libraries.put(libName, libVersion);
                        deleteLibFile(libName, otherLibVersion);
                    } else {
                        deleteLibFile(libName, libVersion);
                    }
                } else {
                    libraries.put(libName, libVersion);
                }
            }
        }
        List<String> libraryFileList = new ArrayList<>();
        Iterator<String> libIterator = libraries.keySet().iterator();
        while (libIterator.hasNext()) {
            String libName = libIterator.next();
            String libVersion = libraries.get(libName);
            libraryFileList.add(getLibFileName(libName, libVersion));
        }
        return libraryFileList;
    }

    private static String getLibFileName(String libName, String libVersion) {
        return String.format("%s%s-%s%s", AHU_LIB_FOLDER, libName, libVersion, LIB_FILE_EXTENSION);
    }

    private static void deleteLibFile(String libName, String libVersion) {
        File libFile = new File(getLibFileName(libName, libVersion));
        if (libFile.exists()) {
            libFile.delete();
            log.debug("old lib file deleted:" + libFile.getName());
        }
    }

    /**
     * 注意：必须与VersionUtils.compare 保持一致。
     * 
     * @param newVersion
     * @param oldVersion
     * @return
     */
    private static int compareVersions(String newVersion, String oldVersion) {
        if (!StringUtils.isEmpty(newVersion) && !StringUtils.isEmpty(oldVersion)) {
            String[] newVersionChunks = newVersion.split("\\.");
            String[] oldVersionChunks = oldVersion.split("\\.");
            int size = newVersionChunks.length > oldVersionChunks.length ? newVersionChunks.length
                    : oldVersionChunks.length;

            for (int i = 0; i < size; i++) {
                String newVersionChunk = i < newVersionChunks.length ? newVersionChunks[i] : "0";
                String oldVersionChunk = i < oldVersionChunks.length ? oldVersionChunks[i] : "0";
                if (!newVersionChunk.equals(oldVersionChunk)) {
                    if (NumberUtils.toInt(newVersionChunk) > NumberUtils.toInt(oldVersionChunk)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            }
            return 0;
        }
        return -1;
    }

}
