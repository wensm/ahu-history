package com.carrier.ahu.tool.license;

import java.util.Locale;
import java.util.ResourceBundle;

import com.carrier.ahu.tool.common.ToolConstants;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Braden Zhou on 2018/04/19.
 */
public class LicenseMain extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        ResourceBundle bundle = ResourceBundle.getBundle(ToolConstants.BUNDLE_PACKAGE, Locale.CHINESE);
        Parent root = FXMLLoader.load(getClass().getResource(ToolConstants.FXML_LICENSE_FILE), bundle);

        Scene scene = new Scene(root);
        stage.setTitle(bundle.getString(ToolConstants.TEXT_LICENSE_TITLE));
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
