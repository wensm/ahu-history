package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partz;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartzMapper {
    int insert(Partz record);

    int insertSelective(Partz record);
}