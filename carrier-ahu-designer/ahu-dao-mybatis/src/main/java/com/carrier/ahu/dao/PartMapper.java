package com.carrier.ahu.dao;

import com.carrier.ahu.po.Part;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartMapper {
    int deleteByPrimaryKey(String partid);

    int insert(Part record);

    int insertSelective(Part record);

    Part selectByPrimaryKey(String partid);

    int updateByPrimaryKeySelective(Part record);

    int updateByPrimaryKey(Part record);
}