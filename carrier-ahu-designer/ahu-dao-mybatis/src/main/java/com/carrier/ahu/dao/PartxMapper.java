package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partx;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartxMapper {
    int insert(Partx record);

    int insertSelective(Partx record);
}