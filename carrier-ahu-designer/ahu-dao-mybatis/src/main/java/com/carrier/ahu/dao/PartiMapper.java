package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parti;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartiMapper {
    int insert(Parti record);

    int insertSelective(Parti record);
}