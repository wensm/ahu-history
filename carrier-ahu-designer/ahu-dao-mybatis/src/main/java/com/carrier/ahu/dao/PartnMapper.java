package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partn;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartnMapper {
    int insert(Partn record);

    int insertSelective(Partn record);
}