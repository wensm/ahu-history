package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partl;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartlMapper {
    int insert(Partl record);

    int insertSelective(Partl record);
}