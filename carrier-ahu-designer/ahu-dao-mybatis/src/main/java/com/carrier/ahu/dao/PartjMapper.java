package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partj;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartjMapper {
    int insert(Partj record);

    int insertSelective(Partj record);
}