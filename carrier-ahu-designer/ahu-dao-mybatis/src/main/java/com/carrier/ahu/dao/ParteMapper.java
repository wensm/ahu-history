package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parte;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParteMapper {
    int insert(Parte record);

    int insertSelective(Parte record);
}