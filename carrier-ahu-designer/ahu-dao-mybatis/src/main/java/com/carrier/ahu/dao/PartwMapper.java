package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partw;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartwMapper {
    int insert(Partw record);

    int insertSelective(Partw record);
}