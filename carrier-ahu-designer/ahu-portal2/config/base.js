/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function () {
  return {
    entry: {
      index: './src/index.js',
      vendor: ['babel-polyfill', 'react', 'react-dom', 'react-router', 'classnames', 'es6-promise', 'jquery', 'react-s-alert', 'react-modal', 'sweetalert', 'react-tabs', 'react-datepicker', 'immutability-helper', 'react-data-grid-addons',]
    },
    output: {
      filename: 'js/[name].[chunkhash].js',
      path: path.resolve(__dirname, '../dist')
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: ['babel-loader'],
          // use: ['babel-loader', 'eslint-loader'],
          exclude: /node_modules/
        },
        {
          test: /\.(png|bmp|jpg|gif|svg|eot|ttf|woff|woff2|mp3)$/,
          loader: 'url-loader',
          options: {
            limit: 1024,
            name: 'assets/[hash].[ext]'
          }
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin('css/[name].[contenthash].css'),
      new webpack.optimize.CommonsChunkPlugin({
          names: ['vendor', 'manifest'] // Specify the common bundle's name.
      }),
      new HtmlWebpackPlugin({
          template: 'src/index.html',
          chunksSortMode: 'dependency'
      })
    ]
  };
}
