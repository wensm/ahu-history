
/* eslint-disable */
import React from 'react';
import { Link } from 'react-router';
import ProjectBatchImportDiv from './components/ProjectBatchImportDiv.js';
import ProjectBatchExportDiv from './components/ProjectBatchExportDiv.js';
import NewAhuDiv from './components/NewAhuDiv.js';
import NewGroupDiv from './components/NewGroupDiv.js';
import http from './misc/http';
import GroupDiv from './modules/ahus/GroupDiv.js';
import AhuDiv from './components/AhuDiv';
import Report from './Report.js'
import style from './page2.css';
import logoImg from './images/LOGObg.png';
import loadingImg from './images/loding.gif';
import personImg from './images/person.png';
import Header from './Header';
import store from './modules/ahu/modules/store';
import Modal from './components/Modal';
import AhuInit from './components/AhuInit.js';
import classnames from 'classnames'
import sweetalert from 'sweetalert'
import style1 from './components/SetDirectionDiv.css';

export default class Page2 extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      batchModalIsOpen: false,
      ngModalIsOpen: false,
      naModalIsOpen: false,
      ahus2: [],
      ahus:[],
      ahu:{},
      ahuGroups:[],
      waitDivideAhuIds:[],
      ahuGroup:{},
      reportModalIsOpen: false,
      reportForm: {},
      reports: [],
      groupId:null,
      myModal5IsOpen: false,
      myModal6IsOpen: false,
      myModal7IsOpen: false,
      paneltypeIsZdy:false,
      inportFileType:1,
      moveAhuGroupState:false,
      moveAhuName:null,
      moveAhuId:null,
      moveGroupList:[],
      groupList:[],
      ahuPartList:[],
      newUnitNo:null,
      newDrawingNo:null,
      newAhuGroupId:null,
      newAhuGroupType:null,
      exPortState:false,
      groupTypeList:[],
      AhuInitList:[],
      loadingState:false,
      ahuList:[],
    };

    this.openBatchModal = this.openBatchModal.bind(this);
    this.afterOpenBatchModal = this.afterOpenBatchModal.bind(this);
    this.closeBatchModal = this.closeBatchModal.bind(this);
    this.saveBatchModal = this.saveBatchModal.bind(this);
    this.openExportModal = this.openExportModal.bind(this);
    this.closeExportModal = this.closeExportModal.bind(this);
    this.exportGroup = this.exportGroup.bind(this);

    this.openngModal = this.openngModal.bind(this);
    this.afterOpenngModal = this.afterOpenngModal.bind(this);
    this.closengModal = this.closengModal.bind(this);

    this.opennaModal = this.opennaModal.bind(this);
    this.afterOpennaModal = this.afterOpennaModal.bind(this);
    this.closenaModal = this.closenaModal.bind(this);

    this.pid = this.props.params.pid;
    this.getAhus = this.getAhus.bind(this);
    this.delete = this.delete.bind(this);
    this.getAhuGroups = this.getAhuGroups.bind(this);
    this.savengModal = this.savengModal.bind(this);
    this.handleGroupNameChange = this.handleGroupNameChange.bind(this);
    this.closeReportModal = this.closeReportModal.bind(this);

    this.openNgModal = this.openNgModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openMyModal5 = this.openMyModal5.bind(this);
    this.closeMyModal5 = this.closeMyModal5.bind(this);
    this.openMyModal6 = this.openMyModal6.bind(this);
    this.closeMyModal6 = this.closeMyModal6.bind(this);
    this.openMyModal7 = this.openMyModal7.bind(this);
    this.closeMyModal7 = this.closeMyModal7.bind(this);
    this.handlerInputChange = this.handlerInputChange.bind(this);
    this.delGroup = this.delGroup.bind(this);
    this.openMoveAhuGroup = this.openMoveAhuGroup.bind(this);
    this.closeMoveAhuGroup = this.closeMoveAhuGroup.bind(this);
    this.saveMoveAhuGroup = this.saveMoveAhuGroup.bind(this);
    this.ahuInitChange = this.ahuInitChange.bind(this);
    this.saveAhuinit = this.saveAhuinit.bind(this);
    this.saveQutSet = this.saveQutSet.bind(this);
  }

  openMoveAhuGroup(ahuId,ahuName){
  	http.get('group/lists/'+this.pid+'/'+ahuId).then(data => {
  		this.setState({moveGroupList:data.data,moveAhuId:ahuId,moveAhuName:ahuName,moveAhuGroupState:true});
  	});
  }
  closeMoveAhuGroup(){
  	this.setState({moveAhuGroupState:false});
  }
  saveMoveAhuGroup(){
  	var groupId = $("#groupSelect").val();
  	http.get('ahu/resetgroup/'+this.state.moveAhuId+'/'+groupId).then(data => {
  		if(data.code == 1){
				sweetalert({title:'Oops...', text: data.msg, type: 'error', timer: 3000});
				event.target.value = "";
			}else{
				sweetalert({title:'修改成功', type: 'success', timer: 3000});
				this.setState({moveAhuGroupState:false});
				this.getAhuGroups();
			}
  	});
  }

  ahuInitChange(name,value){
  	var AhuInitList = this.state.AhuInitList;
  	AhuInitList[name] = value;
		this.setState({AhuInitList:AhuInitList});
  }

  saveAhuinit(groupId){
		http.post('mc/savegc', {
      projid: this.props.params.pid,
      groupid: groupId,
      prokey:JSON.stringify(this.state.AhuInitList)
   }).then(data => {
	   	if(data.code == 0){
	   		this.getAhuGroups();
	   		sweetalert({title:'保存成功！', type: 'success', timer: 3000});
	   	}else{
	   		sweetalert({title:'保存失败！', type: 'error', timer: 3000});
	   	}
      this.closeMyModal5();
    })
  }

	saveQutSet(){
		var type = $("input[name=judge-select]:checked").attr("id");
		if(type != undefined){
			var text = '批量选型只适用于最初报价阶段！';
			this.setState({myModal5IsOpen: false});
			sweetalert({
	        title: '确定选型吗？',
	        text,
	        type: 'warning',
	        showCancelButton: true,
	        confirmButtonText: '确定',
	        cancelButtonText: '取消',
	        closeOnConfirm: true
	      }, isConfirm => {
	        if (isConfirm) {
	        	this.setState({loadingState:true});
						http.post('mcset/'+this.pid+'/'+this.state.ahuGroupId+'/'+type).then(data => {
							var successList="图纸编号为";
							var FailureList="图纸编号为";
							var returnType="error";
							if(data.data.Success.length == 0){
								successList="";
							}else{
								for(var i = 0; i<data.data.Success.length; i++ ){
									successList+=data.data.Success[i]+",";
								}
								successList=successList.substring(0,successList.length-1);
								successList+="的机组配置成功!";
								returnType='success';
							}
							if(data.data.Failure.length == 0){
								FailureList="";
							}else{
								for(var i = 0; i<data.data.Failure.length; i++ ){
									FailureList+=data.data.Failure[i]+",";
								}
								FailureList=FailureList.substring(0,FailureList.length-1);
								FailureList+="的机组配置失败!";
							}
							setTimeout(() => {
								this.setState({loadingState:false});
					      if(data.code == 1){
									sweetalert({title:'配置失败', text:'', type: 'error', timer: 3000});
								}else{
									setTimeout(function(){
										sweetalert({title:successList+'\n'+FailureList, type: returnType, timer: 3000});
									},1000)
									
								}
								this.getAhuGroups();
					    }, 500);
						});
					}
	    });
		}
  }

  openMyModal5(groupId,ahuId) {
    if (groupId) {
      http.get('ahu/getsection/'+ahuId).then(data => {
      	this.setState({ahuPartList:data.data,ahuGroupId:groupId});
      	http.get('mc/getgc/'+this.props.params.pid+'/'+groupId).then(data => {
		  		var obj = eval('(' + data.data.prokey + ')');
		  		this.setState({AhuInitList:obj,myModal5IsOpen: true});
		  	})
      });
    }
  }
	closeMyModal5() {
		this.setState({ahuGroupId: false});
		this.setState({myModal5IsOpen: false});
	}

	 openMyModal6(groupId) {
    if (groupId) {
    	this.setState({ahuGroupId:groupId});
      this.setState({myModal6IsOpen: true});
    }
  }
	closeMyModal6() {
		this.setState({ahuGroupId: false});
		this.setState({batchModalIsOpen: false});
		this.setState({myModal6IsOpen: false});
	}

	openMyModal7(groupId) {
    if (groupId) {

    	this.setState({ahuGroupId:groupId});
      this.setState({myModal7IsOpen: true});
    }
  }
	closeMyModal7() {
		this.setState({ahuGroupId: false});
		this.setState({myModal7IsOpen: false});
	}
  handlerInputChange(event){
    var ahu = this.state.ahu;
    var value = event.target.value;
    var name = event.target.name;
    ahu[name] = value;
    this.setState({ahu:ahu});
    if(name == "groupType"){
    	if(value == "自定义"){
    		this.setState({paneltypeIsZdy:true});
    	}else{
    		this.setState({paneltypeIsZdy:false});
    	}
    }
  }
  openNgModal(){
  	var ahuGroupList = this.state.ahuGroups;
  	var ahuGroup = null;
  	for(var i = 0; i < ahuGroupList.length; i++){
  		if(ahuGroupList[i].groupId == null){
  			ahuGroup = ahuGroupList[i];
  		}
  	}
  	var ahus = ahuGroup.ahus;
  	var ahuIds = [];
  	for(var i=0;i<ahus.length;i++){
	    ahuIds.push(ahus[i].unitid);
	  }
    this.openngModal(ahuIds,ahuGroup.groupId,ahuGroup.ahuGroupCode);
  }

  componentDidMount() {
    this.getSectionProps();
    this.getAhuGroups();
  }

	closeModal() {
    this.setState({modalIsOpen: false});
  }

   getSectionProps() {
    http.get('meta/layout').then(data => {
      for (let variable in data) {
        if (data.hasOwnProperty(variable)) data[variable].groups = data[variable].groups.sort((a, b) => a.paras.length - b.paras.length)
      }
      store.setLayout(data)
    })
  }
  // Start Batch Madal Declaration.
  openBatchModal(flg,groupId) {
  	this.setState({groupId:groupId})
  	this.setState({inportFileType:flg});
    this.setState({batchModalIsOpen: true});
  }

  afterOpenBatchModal() {
    // references are now sync'd and can be accessed.
    this.refs.subtitle.style.color = '#f00';
  }

  saveBatchModal(formData) {
    //Do create AHU from the configuration here
    var projectId = this.props.params.pid;
    var ajaxUrl = "";
    if(this.state.inportFileType == 1){
    	ajaxUrl = `${SERVICE_URL}import/ungroup/`;
    	ajaxUrl += projectId;
    }else if(this.state.inportFileType == 2){
    	ajaxUrl = `${SERVICE_URL}import/group/`;
    	ajaxUrl += projectId;
    	ajaxUrl += "/"+this.state.groupId;
    }
    
    const pageObj = this;
		$.ajax({
        url: ajaxUrl,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.code == "0") {
                sweetalert({title:'上传成功！', type: 'success', timer: 3000});
                pageObj.getAhuGroups();
            }
            if (data.code == "1") {
                sweetalert({title:data.msg, type: 'error', timer: 3000});
            }
            pageObj.closeBatchModal();
            pageObj.closeMyModal6();
        },
        error: function () {
            sweetalert({title:'上传失败！', type: 'error', timer: 3000});
            pageObj.closeBatchModal();
            pageObj.closeMyModal6();
        }
    });
  }

	openExportModal(){
		this.setState({exPortState:true});
	}
	closeExportModal(){
		this.setState({exPortState:false});
	}

	exportGroup(fileName){
		var pid = this.props.params.pid;
		var gid = this.state.ahuGroupId;
    window.location=`${SERVICE_URL}` + 'export/group/'+pid+'/'+gid+'?filename='+fileName;
    this.closeExportModal();
    this.closeMyModal6();
	}


  closeBatchModal() {
    this.setState({batchModalIsOpen: false});
  }

  // Start New group Modal declaration

  openngModal(ahuIds,ahuGroupId,ahuGroupCode) {
    var ahuGroup = {};
    ahuGroup.ahuIds = ahuIds.join(",");
    ahuGroup.groupId = ahuGroupId;
    ahuGroup.ahuGroupCode = ahuGroupCode;
    ahuGroup.projectId = this.pid;
    ahuGroup.groupName = this.state.groupName;
    http.get('grouptype/list').then(data =>{
    	this.setState({groupTypeList:data.data});
    })
    this.setState({ahuGroup: ahuGroup});
    this.setState({ngModalIsOpen: true});
  }

  handleGroupNameChange(name,value){
    var ahuGroup = this.state.ahuGroup;
    ahuGroup[name] = value;
    this.setState({ahuGroup: ahuGroup});
  }

  afterOpenngModal() {
    // references are now sync'd and can be accessed.
    this.refs.subtitle.style.color = '#f00';
  }

  savengModal() {
    http.post('group/add',this.state.ahuGroup).
    then(data=>{
      this.closengModal();
      this.getAhuGroups();
    });
  }

  handleGroupName(){
    var ahuGroup = this.state.ahuGroup;


  }

  closengModal() {
    this.setState({ngModalIsOpen: false});
  }

  // Start new AHU Modal declaration
  opennaModal(groupId,groupType) {
  	if(groupId != "" &&　groupType != ""){
      this.setState({newAhuGroupId:groupId,newAhuGroupType:groupType});
  	}
  	http.get('ahu/getusefulnum').then(data => {
  		this.setState({newUnitNo:data.data.unitNo,newDrawingNo:data.data.drawingNo});
  		this.setState(prevState => {
	      return {naModalIsOpen: !prevState.naModalIsOpen}
	    })
  	})
  }

  afterOpennaModal() {
    // references are now sync'd and can be accessed.
    this.refs.subtitle.style.color = '#f00';
  }

  savenaModal() {
    //Do create AHU from the configuration here
  }

  closenaModal() {
    this.setState({naModalIsOpen: false});
  }

  getAhuGroups(){
    http.get('group/lists/'+this.pid)
    .then(data=>{
    	this.setState({groupList:data.data});
      this.getAhus(data.data);
    });
  }

  getAhus(ahuGroups){
  	for (var i=0;i<ahuGroups.length;i++) {
     ahuGroups[i].ahus = [];
    }
    http.get('ahu/list',{projectId:this.pid,groupId:""})
    .then(data=>{
      var ahus = data.data;
      var notDivided = [];
      ahus.sort(function(a,b){
      	return a.drawingNo-b.drawingNo
      });
      for(var j=0;j<ahus.length;j++){
        if(ahus[j].groupId == null || ahus[j].groupId == ''){
          notDivided.push(ahus[j]);
          continue;
        }
        for (var i=0;i<ahuGroups.length;i++) {
          if(!ahuGroups[i].ahus){
            ahuGroups[i].ahus = [];
          }
          if(ahus[j].groupId == ahuGroups[i].groupId){
            ahuGroups[i].ahus.push(ahus[j]);
            break;
          }
        }
      }
      var ahuGroup = {};
      ahuGroup.ahus = notDivided;
      ahuGroup.groupName = '未分组';
      ahuGroup.groupId = null;
      ahuGroup.ahuGroupCode = '';
      ahuGroups.unshift(ahuGroup);
      this.setState({ahuGroups : ahuGroups,ahuList:ahus});
      $("#moreLoding").hide();
    });
  }

  delete(unitid){
    http.post('ahu/delete/'+unitid)
    .then(data=>{
      this.getAhuGroups();
    });
  }

	delGroup(ahuGroupId){
		 http.delete('group/delete/'+ahuGroupId)
    .then(data=>{
      this.getAhuGroups();
    });
	}

  closeReportModal() {
    this.setState({reportModalIsOpen: false})
  }

  openReportModal() {
    this.setState({reportModalIsOpen: true})
  }

  handleChange({target}) {
    const obj = {}
    obj[target.getAttribute('name')] = target.checked;
    this.setState(prevState => {
      const reportForm = Object.assign({}, prevState.reportForm, obj)
      return {reportForm}
    }, () => {
      console.log(this.state.reportForm);
    })
    
    var name = target.getAttribute('name');
    var value = target.checked;
    if(value && (name == "3view" || name == "fan" || name == "hanshi")){
    	$("input[name=techproj]").prop("checked","checked");
    	const newObj = {}
    	newObj['techproj'] = true;
    	this.setState(prevState => {
	      const reportForm = Object.assign({}, prevState.reportForm, newObj)
	      return {reportForm}
	    }, () => {
	      console.log(this.state.reportForm);
	    })
    }
    
  }

  handleCreateReport() {
  	var ahuIds = [];
  	$(".selAhuCheckBox").each(function(){
  		if($(this).is(':checked')){
  			ahuIds.push($(this).attr("id"))
  		}
  	});
  	if(ahuIds.length != 0){
	    let param = {
	      "ahuIds": ahuIds,
	      "projectId": this.props.params.pid,
	      "reports": [{
	        "type": "excel",
	        "name": "项目清单",
	        "output":false,
	        "path":"/asserts/report/pro1/项目清单.xlsx",
	        "items": [],
	        "classify":"prolist",
	      },{
	      	"type": "excel",
	        "name": "主要参数列表",
	        "output":false,
	        "path":"/asserts/report/pro1/主要参数列表.xlsx",
	        "items": [],
	        "classify":"paralist",
	      }, {
	      	"type": "excel",
	        "name": "长交货期选项",
	        "output":false,
	        "path":"/asserts/report/pro1/长交货期选项.xlsx",
	        "items": [],
	        "classify":"delivery",
	      }, {
            "type": "pdf", 
            "name": "奥雅纳工程格式报告",
            "output": false,
            "path": "/asserts/report/proj1/奥雅纳工程格式报告.pdf",
            "classify": "aruppf",
            "items": [ ]
        }, {
            "type": "pdf", 
            "name": "柏诚工程格式报告", 
            "output": false, 
            "path": "/asserts/report/proj1/柏诚工程格式报告.pdf", 
            "classify": "bochengpf", 
            "items": [ ]
        }, {
            "type": "pdf", 
            "name": "自定义格式报告", 
            "output": false, 
            "path": "/asserts/report/proj1/自定义格式报告.pdf", 
            "classify": "custompf", 
            "items": [ ]
        }, {
	        "type": "pdf", 
	        "name": "技术说明(工程版)", 
	        "output": false, 
	        "path": "/asserts/report/proj1/技术说明(工程版).pdf", 
	        "classify": "techproj",
	        "items": [
	          {"name":"三视图", "classify":"3view", "output":false},
	          {"name":"风机曲线", "classify":"fan", "output":false},
	          {"name":"焓湿图", "classify":"hanshi", "output":false}
	          ]
	      },{
            "type": "pdf", 
            "name": "技术说明(销售版)", 
            "output": false, 
            "path": "/asserts/report/proj1/技术说明(销售版).pdf", 
            "classify": "techsaler", 
            "items": [ ]
        }]
	    }
	    param.reports.forEach(report => {
	      report.items.forEach(item => {
	        if (typeof this.state.reportForm[item.classify] !== typeof undefined) item.output = this.state.reportForm[item.classify]
	      })
	      if (typeof this.state.reportForm[report.classify] !== typeof undefined) report.output = this.state.reportForm[report.classify]
	    })
	    http.post('report', {"reports": JSON.stringify(param)}).then(data => {
	    	if(data.code == 0){
	    		sweetalert({title:'生成成功！', type: 'success', timer: 3000});
	    		this.setState({reports: data.data.reportsObj.reports})
	    	}else{
	    		sweetalert({title:'生成失败！', type: 'error', timer: 3000});
	    	}
	    })
    }
  }

  render() {
    for (var index = 1; index <= 0; index++) {
      this.state.ahus2.push(
        {
          unitNo:"00" + index,
          drawingNo: index,
          name:"AHU" + index,
          series:"39CQ1317",
          customerName:"张江集团",
          mount:1,
          weight:1234,
          price:33224,
          updateTime:"2017-03-02 13:23:34",
          recordStatus:"正在选型"
        }
      );
    }

    return (
      <div className={style.page2}>
      {this.state.batchModalIsOpen && <ProjectBatchImportDiv closeBatchModal={this.closeBatchModal} saveBatchModal={this.saveBatchModal} batchModalIsOpen={this.state.batchModalIsOpen}/>}

			{this.state.exPortState && <ProjectBatchExportDiv closeExportModal={this.closeExportModal} exportFile={this.exportGroup} batchModalIsOpen={this.state.batchModalIsOpen}/>}

      <Header />
<div className="main promain clear compro_main">
  <div className="pro_con">
      <ul className="pro_cap clear com_cap">
          <li>
              <Link className="iconfont icon-fanhui" to="/">返回</Link>

          </li>
          <li data-toggle="modal" onClick={this.openNgModal}>
              <i className="iconfont icon-xinjiangroup"></i>
                新建group
          </li>
          <li data-toggle="modal" onClick={() => this.opennaModal()}>
              <i className="iconfont icon-xinzengAHU"></i>
                新增AHU
          </li>

          <li onClick={() => this.openReportModal()}>
              <i className="iconfont icon-baogao" style={{color:'white',fontSize:'14px'}}>生成报告</i>
          </li>
      </ul>
      
      {this.state.reportModalIsOpen && <div className="modal homModal" id="creatReport" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block'}}>
          <div style={{width:'700px',margin: '30px auto'}} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeReportModal}><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>生成报告</b></h4>
              </div>
              <div className="modal-body clear">
                <div onChange={e => this.handleChange(e)} style={{width:'698px',height:'600px',overflow:'auto'}}>
	                <div style={{padding:'20px'}}>
					        	<div style={{height:'32px'}}>
					        		<div style={{width:'100px',float:'left',color:'#3E3E3E',fontWeight:'bold',fontSize:'13px'}}>报告名称</div>
					        		<input type="text" style={{width:'200px',border:'1px solid #cccccc',float:'left',height:'32px',lineHeight:'32px'}}/>
					        	</div>
					        	<div style={{height:'32px',marginTop:'10px'}}>
					        		<div style={{width:'100px',float:'left',color:'#505050',fontWeight:'bold',fontSize:'13px'}}>报告路径</div>
					        		<input type="text" style={{width:'440px',border:'1px solid #cccccc',float:'left',height:'32px',lineHeight:'32px'}}/>
					        		<button type="button" style={{flot:'left',marginLeft:'20px',width:'80px',height:'32px',lineHeight:'32px',backgroundColor:'#f3f4f8',color:'#a6aeb9'}}>选择</button>
					        	</div>
					          <div style={{marginTop:'20px',marginBottom:'20px'}}>
					          	<div style={{width:'100px',float:'left',color:'#505050',fontWeight:'bold',fontSize:'13px'}}>报告内容</div>
					          	<div style={{float:'left'}}>
					          		<div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'0px 0px 15px 0px'}}>三方资讯文档</div>
					          		<label className="checkbox-inline" style={{marginLeft:'30px',color:'#666666'}}>
						              <input type="checkbox" name="aruppf" style={{height:'13px'}} /> 奥雅纳工程格式报告
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="bochengpf" style={{height:'13px'}} /> 柏诚工程格式报告
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="custompf" style={{height:'13px'}} /> 自定义格式报告
						            </label>
						            <div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'20px 0px 15px 0px'}}>技术说明</div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',fontWeight:'bold',fontSize:'12px',color:'#505050'}}>
						              <input type="checkbox" name="techproj" style={{height:'13px'}} /> 工程图
						            </label>
						            <div style={{marginLeft:'60px',marginBottom:'20px',marginTop:'20px'}}>
						            	<label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="3view" style={{height:'13px'}}/> 三视图
							            </label>
							            <label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="fan" style={{height:'13px'}}/> 风机曲线
							            </label>
							            <label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="hanshi" style={{height:'13px'}}/> 焓湿图
							            </label>
						            </div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',fontWeight:'bold',fontSize:'12px',color:'#505050'}}>
						              <input type="checkbox" name="techsaler" style={{height:'13px'}} /> 销售版
						            </label>
						            <div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'20px 0px 15px 0px'}}>相关文件</div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',color:'#666666'}}>
						              <input type="checkbox" name="prolist" style={{height:'13px'}} /> 项目清单
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="paralist" style={{height:'13px'}} /> 主要参数列表
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="delivery" style={{height:'13px'}} /> 长交货期选项
						            </label>
					          	</div>
					          	<div style={{clear:'both'}}></div>
					          </div>
					        </div>
					        <div style={{padding:'20px', borderTop:'1px solid #F0F0F0',backgroundColor:'#F9FAFC'}}>
					          <div className={classnames(style.groupDiv, style1.groupDiv, 'pro_cont clear com_cont')}>
					                {
					                    !this.state.collapse &&
					                    <table className="com_bottom" cellSpacing="0">
					                        <thead>
					                        <tr>
					                            <th>选择</th>
					                            <th>机组编号</th>
					                            <th>图纸编号</th>
					                            <th>机组名称</th>
					                            <th>机组型号</th>
					                            <th style={{width:'9%'}}>AHU状态</th>
					                        </tr>
					                        </thead>
					                        <tbody>
					                        {
											          		this.state.ahuList.map((ahu,index)=> {
														            return <AhuDiv flg={3} data={ahu}/>;
														        })
											          	}
					                        </tbody>
					                    </table>
					                }
					            </div>
					          </div>
				        </div>
              </div>
              <div className="modal-footer clear" style={{textAlign:'center'}}>
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.closeReportModal}style={{backgroundColor:'#25c2cb', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>取消</button>
                <button type="button" className="btn btn-primary" onClick={() => this.handleCreateReport()} style={{backgroundColor:'rgba(50,116,207,1)', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>生成报告</button>
              </div>
              <div style={{marginTop:'10px'}}>
			          {this.state.reports.map(report => <a href={report.path} target="_blank" style={{marginRight:'10px'}}>{report.name}</a>)}
			        </div>
            </div>
          </div>
        </div>
     	}
      
      
      
      <div className="pro_allcont">
{this.state.ngModalIsOpen && <NewGroupDiv closengModal={this.closengModal} groupTypeList = {this.state.groupTypeList} handleGroupNameChange={this.handleGroupNameChange} savengModal={this.savengModal} ngModalIsOpen={this.state.ngModalIsOpen}/>}
{this.state.naModalIsOpen && <NewAhuDiv getAhus={this.getAhuGroups} pid={this.pid} newUnitNo = {this.state.newUnitNo} newAhuGroupId = {this.state.newAhuGroupId} newAhuGroupType = {this.state.newAhuGroupType} newDrawingNo = {this.state.newDrawingNo} closenaModal={this.closenaModal} savenaModal={this.savenaModal} naModalIsOpen={this.state.naModalIsOpen}/>}

      {
        this.state.ahuGroups.map((ahuGroup,index)=> {
          if(ahuGroup.ahus){
            return <GroupDiv data={ahuGroup} flg={2} getAhuGroups={this.getAhuGroups} openMoveAhuGroup={this.openMoveAhuGroup} delGroup={this.delGroup} opennaModal={this.opennaModal} openngModal={this.openngModal} openMyModal5={this.openMyModal5} openMyModal6={this.openMyModal6} openMyModal7={this.openMyModal7} openBatchModal={this.openBatchModal} pid={this.pid} delete={this.delete} groupName={ahuGroup.groupName}/>;
          }
        })
       }
      </div>
      <div className="more" id="moreLoding">
          <span><i className="loding"><img src={loadingImg} /> </i>加载更多</span>
      </div>
  </div>
</div>
<div className="footer">
    <div className="fotop">
        <div className="fot_lef fl">
            <a href="#" className="">隐私声明</a>
            <a href="#" className="">使用条款</a>
            <a href="#" className="">联系我们</a>
        </div>
        <div className="fot_rig fr">
            <a href="#" className="">开利公司是 联合技术环境、控制与安防 的子公司，联合技术环境、控制与安防隶属于 联合技术公司</a>
        </div>
    </div>
    <p>© 开利公司 2017</p>
</div>
<div className="modal homModal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
  <div className="modal-dialog modal-group" role="document">
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title" id="myModalLabel"><b>新建group</b></h4>
          </div>
          <div className="modal-body clear">
              <ul className="group_cont">
                  <li className="grpcon_li">
                      <label className="tex_cap">group名称</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <label className="tex_cap">group备注</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <div className="select_box fl">
                          <label className="tex_cap">选择类型</label>
                          <div className="selected">
                              <select title="选择类型:" name="sType" className="single_select" style={{display:'none'}} >
                                  <option selected="selected" value="空白">空白</option>
                                  <option value="自定义">自定义</option>
                                  <option value="基本型">基本型</option>
                                  <option value="二管制舒适型">二管制舒适型</option>
                                  <option value="四管制舒适型">四管制舒适型</option>
                              </select>
                          </div>
                      </div>
                      <a href="#" className="zdy_btn">自定义</a>
                  </li>
              </ul>
          </div>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal">取消</button>
              <button type="button" className="btn btn-primary btn_add">创建</button>
          </div>
      </div>
  </div>
</div>
<div className="modal homModal fade" id="myModal2"  role="dialog" aria-labelledby="myModalLabel">
  <div className="modal-dialog modal-group new-group" role="document">
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title"><b>新建AHU</b></h4>
          </div>
          <div className="modal-body clear">
              <ul className="group_cont">
                  <li className="grpcon_li">
                      <label className="tex_cap">机组编号</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <label className="tex_cap">图纸编号</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <div className="select_box fl">
                          <label className="tex_cap">机组类型</label>
                          <div className="selected">
                              <select title="机组类型:" name="sType" className="single_select" style={{display:'none'}} >
                                  <option selected="selected" value="空白">空白</option>
                                  <option value="自定义">自定义</option>
                                  <option value="基本型">基本型</option>
                                  <option value="二管制舒适型">二管制舒适型</option>
                                  <option value="四管制舒适型">四管制舒适型</option>
                              </select>
                          </div>
                      </div>
                  </li>
                  <li className="grpcon_li">
                      <label className="tex_cap">机组名称</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <label className="tex_cap">机组数量</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
              </ul>
          </div>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal">取消</button>
              <button type="button" className="btn btn-primary btn_add">创建</button>
          </div>
      </div>
  </div>
</div>
<div className="modal homModal fade" id="myModal3"  role="dialog" aria-labelledby="myModalLabel" >
  <div className="modal-dialog modal-group new-group group-mease" role="document">
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title"><b>group信息</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
          </div>
          <div className="modal-body clear">
              <h3 className="time">
                  <span className="star_times">创建时间<em className="star_time">2017-03-01</em></span>
                  <span className="end_times">最后修改时间<em className="end_time">2017-03-01</em><em className="end_send">09:21:23</em></span>
              </h3>
              <ul className="group_cont">
                  <li className="grpcon_li">
                      <label className="tex_cap">group名称</label>
                      <input type="text" value="回风工况机组01" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li grpcon_two">
                      <label className="tex_cap">group备注</label>
                      <input type="text" value="" className="tex_inp" />
                  </li>
                  <li  className="grpcon_li">
                      <div className="select_box fl">
                          <label className="tex_cap">选择类型</label>
                          <div className="selected">
                              <select title="机组类型:" name="sType" className="single_select" style={{display:'none'}} >
                                  <option selected="selected" value="空白">空白</option>
                                  <option value="自定义">自定义</option>
                                  <option value="基本型">基本型</option>
                                  <option value="二管制舒适型">二管制舒适型</option>
                                  <option value="四管制舒适型">四管制舒适型</option>
                              </select>
                          </div>
                      </div>
                  </li>
              </ul>
          </div>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal">取消</button>
              <button type="button" className="btn btn-primary btn_add">创建</button>
          </div>
      </div>
  </div>
</div>
<div className="modal homModal fade" id="myModal4"  role="dialog" aria-labelledby="myModalLabel">
  <div className="modal-dialog modal-group new-group dr_group" role="document">
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title"><b>参数批量导入</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
          </div>
          <div className="modal-body clear">
              <div className="input_dr">
                  <label>选择文件</label>
                  <input type="text" value="" className="img_tex" />
                  <button type="button">浏览<input type="file" className="none_inpt" /> </button>

              </div>
          </div>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal">取消</button>
              <button type="button" className="btn btn-primary btn_add">打开文件</button>
          </div>
      </div>
  </div>
</div>
{this.state.myModal5IsOpen && <div className={classnames(style.myhomModal, 'modal homModal fade in')} id="myModal5" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',paddingRight:'17px'}} aria-hidden="false">
  <div className={classnames(style.myModal_group, 'modal-dialog modal-group new-group dr_group')} role="document" style={{width:'950px'}}>
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title"><b>批量处理</b><span className="iconfont icon-close" data-dismiss="modal" onClick={this.closeMyModal5}></span></h4>
          </div>

          <ul className={classnames(style.nav_tabs, 'nav nav-tabs')} role="tablist">
				    <li role="presentation" className="active"><a href="#xxplsz" aria-controls="xxplsz" role="tab" data-toggle="tab">选项批量设置</a></li>
				    <li role="presentation"><a href="#plxxsz" aria-controls="plxxsz" role="tab" data-toggle="tab">批量选型设置</a></li>
				  </ul>

				  <div className="tab-content">
				    <div role="tabpanel" className={classnames(style.tab_pane_ahuInit, 'tab-pane active')}  id="xxplsz">
				    	<AhuInit data={this.state.AhuInitList} saveAhuinit={this.saveAhuinit} ahuInitChange={this.ahuInitChange} id={this.state.ahuGroupId} onCloseModal={this.closeMyModal5} ahuPartList = {this.state.ahuPartList} flg={2}/>
				    </div>
				    <div role="tabpanel" className="tab-pane" id="plxxsz">
				    		<div className="modal-body clear checkbox">
			              <span className="check_cap"><b>配置规则</b></span>
			              <ul className="check_ul" style={{marginLeft:'20px',marginTop:'15px'}}>
			                  <li className="fl">
			                      <div className="label-checkbox fl">
			                          <input type="radio" name="judge-select" style={{verticalAlign: 'middle',marginTop: '0px',marginRight:'5px'}} id="1" />
			                          <label for="1" style={{display:'none'}}></label>
			                      </div>
			                      <span style={{verticalAlign:'middle'}}>最经济</span>
			                  </li>
			                  <li  className="fl">
			                      <div className="label-checkbox fl">
			                          <input type="radio" name="judge-select" style={{verticalAlign: 'middle',marginTop: '0px',marginRight:'5px'}}  id="2" />
			                          <label for="2" style={{display:'none'}}></label>
			                      </div>
			                      <span style={{verticalAlign:'middle'}}>性能最优</span>
			                  </li>
			              </ul>
			          </div>
			          <div className="modal-footer clear" style={{border:'1px solid #e5e5e5 !important;'}}>
			              <button type="button" className="btn btn-default btn_close" data-dismiss="modal" onClick={this.closeMyModal5}>取消</button>
			              <button type="button" className="btn btn-primary btn_add" onClick={this.saveQutSet}>快速配置</button>
			          </div>
				    </div>
				  </div>



      </div>
  </div>
</div>
}

{this.state.loadingState && <div className={classnames(style.myhomModal, 'modal homModal fade in')} role="dialog" aria-labelledby="myModalLabel" style={{display:'block',paddingRight:'17px'}} aria-hidden="false">
  <div className="modal-dialog modal-group new-group dr_group" role="document">
		<div className="modal-content" style={{width:'430px',height:'130px',marginLeft:'160px',marginTop:'160px'}}>
			<div style={{marginTop:'50px',marginLeft:'100px'}}>
				<img style={{width:'40px',height:'40px',float:'left'}} src={loadingImg}/><span style={{fontSize:'18px',color:'#999999',float:'left',marginTop:'7px',marginLeft:'26px'}}>正在配置...</span>
			</div>
		</div>
	</div>
</div>
}

{this.state.myModal6IsOpen && <div className={classnames(style.myhomModal, 'modal homModal fade in')} id="myModal6" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',paddingRight:'17px',zIndex:'99998'}} aria-hidden="false">
  <div className="modal-dialog modal-group new-group dr_group" role="document">
      <div className="modal-content">
          <div className="modal-header">
              <h4 className="modal-title"><b>参数批量导入</b><span className="iconfont icon-close" data-dismiss="modal" onClick={this.closeMyModal6}></span></h4>
          </div>
          <div className={classnames(style.btnGroupDiv)}>
          	<button className={classnames(style.drBtn)} onClick={() => this.openBatchModal(2,this.state.ahuGroupId)}><i className="iconfont icon-daoru" style={{marginRight:'5px'}}></i>导入</button>
          	<button className={classnames(style.dcBtn)} onClick={this.openExportModal}><i className="iconfont icon-daochu" style={{marginRight:'5px'}}></i>导出</button>
          </div>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal" onClick={this.closeMyModal6}>取消</button>
              <button type="button" className="btn btn-primary btn_add">保存</button>
          </div>
      </div>
  </div>
</div>
}



<Modal isOpen={this.state.moveAhuGroupState} onRequestClose={this.closeMoveAhuGroup}>
        <div style={{margin:'-20px'}}>
        <h2 style={{width:'600px',backgroundColor:'#4c74ac',padding:'10px 20px',color:'white'}}>移组
        	<span className="iconfont icon-close fr" data-dismiss="modal" style={{marginTop:'5px'}} onClick={this.closeMoveAhuGroup}></span>
        </h2>
        <div onChange={e => this.handleChange(e)} style={{padding:'20px',textAlign:'center'}}>
          将{this.state.moveAhuName}移动到

         	<select id="groupSelect" style={{marginLeft:'10px',width:'150px',height:'34px'}}>
          	{this.state.moveGroupList.map((group,index) => <option value={group.groupId}>{group.groupName}</option>)}
          </select>

        </div>
        <div className="modal-footer clear" style={{textAlign:'center'}}>
	          <button type="button" className="btn btn-default btn_close" onClick={this.closeMoveAhuGroup} style={{backgroundColor:'#25c2cb', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}} data-dismiss="modal">取消</button>
	          <button type="button" className="btn btn-primary btn_add" onClick={() => this.saveMoveAhuGroup()} style={{backgroundColor:'rgba(50,116,207,1)', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>确认移动</button>
	      </div>
    </div>
  </Modal>


{this.state.myModal7IsOpen && <div className={classnames(style.myhomModal, 'modal homModal fade in')} id="myModal7" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',paddingRight:'17px'}} aria-hidden="false">
  <div className="modal-dialog modal-group new-group dr_group" role="document">
      <div className="modal-content" style={{width:'800px'}}>
          <div className="modal-header">
              <h4 className="modal-title"><b>group信息</b><span className="iconfont icon-close" data-dismiss="modal" onClick={this.closeMyModal7}></span></h4>
          </div>
          <div className={classnames(style.createDate)}>
          	<span>创建时间</span>
          	<span>2017-03-01</span>
          	<span>|</span>
          	<span>修改时间</span>
          	<span>2017-03-01 09:21:23</span>
          </div>
          <ul className={classnames(style.group_cont, 'group_cont')}>
          	<li className="grpcon_li matbm20">
          		<label className="tex_cap">group名称</label>
          		<input className="tex_inp" name="groupName" type="text" onChange={this.handlerInputChange}/>
          	</li>
          	<li className="grpcon_li matbm20">
          		<label className="tex_cap">group备注</label>
          		<input className="tex_inp" name="groupMemo" type="text" onChange={this.handlerInputChange}/>
          	</li>
          	<li className="grpcon_li matbm20">

          		<div className="selected">
          		<label className="tex_cap">选择类型</label>
                  <select title="机组类型:" className="tex_inp" name="groupType" onChange={this.handlerInputChange} >
                      <option selected="selected" value="空白">空白</option>
                      <option value="自定义">自定义</option>
                      <option value="基本型">基本型</option>
                      <option value="二管制舒适型">二管制舒适型</option>
                      <option value="四管制舒适型">四管制舒适型</option>
                  </select>
                  {this.state.paneltypeIsZdy && <a href="#" className="zdy_btn">自定义</a>}
              </div>
          	</li>
          </ul>
          <div className="modal-footer clear">
              <button type="button" className="btn btn-default btn_close" data-dismiss="modal" onClick={this.closeMyModal7}>取消</button>
              <button type="button" className="btn btn-primary btn_add">保存</button>
          </div>
      </div>
  </div>
</div>
}





      </div>
    );
  }
}
