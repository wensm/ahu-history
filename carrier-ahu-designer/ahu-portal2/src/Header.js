/* eslint-disable */
import React from 'react';
import { Link } from 'react-router';


import logoImg from './images/LOGObg.png';
import personImg from './images/person.png';
import style from './header.css';

export default class Page2 extends React.Component {

  constructor() {
    super();

    this.state = {
    	ahus:[]
    };

  }


  render() {
    for (var index = 1; index <= 12; index++) {
      this.state.ahus.push(
        {
          ahuOrder:"00" + index,
          dwgOrder: index,
          ahuName:"AHU" + index,
          ahuSerial:"39CQ1317",
          customer:"张江集团",
          count:1,
          weight:1234,
          price:33224,
          modifyDate:"2017-03-02",
          modifyTime:"13:23:34",
          state:"正在选型"
        }
      );
    }

    return (
	      <div className="header clear">
	      	<div className="header-content">
				      <div className="logo">
				         <Link to="/project">
				           <img src={logoImg}/>
				         </Link>
				      </div>
				      <div className="logo_righ">
				          <div className="header-info">
				          		<nav>
					              <ul>
					                  <li className="">
					                      <Link to="/page1">
					                          <i className="iconfont icon-svg11"></i>系统
					                      </Link>
					                      <ul className={style.systemmenu}>
						                        <li><Link to="/defaultParameter">默认参数</Link></li>
						                        <li><Link to="/defaultParameter">语言</Link></li>
						                        <li><Link to="/defaultParameter"></Link></li>
					                      </ul>
					                  </li>
					                  <li className="">
					                      <a href="javascript:;">
					                          <i className="iconfont icon-gongju"></i>工具
					                      </a>
					                  </li>
					                  <li className="">
					                      <a href="javascript:;">
					                          <i className="iconfont icon-tishi1"></i>帮助
					                      </a>
					                  </li>
					              </ul>
				              </nav>
				          </div>
				          <div className="header-login">
				              <div className="seach">
				                  <i className="iconfont icon-sousuoicon"></i>
				              </div>
				              <dl>
				                  <dt className="fl"><img src={personImg}/> </dt>
				                  <dd className="fl">
				                      <p>Bruce Wang</p>
				                      <p>张江高科工厂</p>
				                  </dd>
				              </dl>
				              <div className="fr down">
				                  <i className="iconfont icon-touxiangzhankaibtn"></i>
				              </div>
				          </div>
				      </div>
				  </div>
				</div>
    );
  }
}
