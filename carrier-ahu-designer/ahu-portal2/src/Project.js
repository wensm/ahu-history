/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './components/Modal';
import AhuInit from './components/AhuInit.js';
import ProjectDiv from './components/ProjectDiv.js';
import { Link } from 'react-router';
import style from './Project.css';
import NewProject from './components/NewProject';
import http from './misc/http';
import moment from 'moment';
import store from './modules/ahu/modules/store'
import ProjectBatchImportDiv from './components/ProjectBatchImportDiv.js';
import ProjectBatchExportDiv from './components/ProjectBatchExportDiv.js';
import logoImg from './images/LOGObg.png';
import pro_icon1Img from './images/pro_icon1.png';
import pro_icon2Img from './images/pro_icon2.png';
import pro_icon3Img from './images/pro_icon3.png';
import pro_icon4Img from './images/pro_icon4.png';
import loadingImg from './images/loding.gif';
import personImg from './images/person.png';
import Header from './Header';
import SetDirectionDiv from './components/SetDirectionDiv.js'
import sweetalert from 'sweetalert'

const appElement = document.getElementById('your-app-element');

export default class Project extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      projectId: null,
      modalIsOpen: false,
      projectModalIsOpen:false,
      projects:[
      ],
      project:{},
      setDirectionState:false,
      getReportState:false,
      reportForm: {},
      reports: [],
      exPortState:false,
      AhuInitList:[],
    };

    this.openProjectModal = this.openProjectModal.bind(this);
    this.afterOpenProjectModal = this.afterOpenProjectModal.bind(this);
    this.closeProjectModal = this.closeProjectModal.bind(this);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.getProjects = this.getProjects.bind(this);
    this.newProject = this.newProject.bind(this);
    this.handleProjectDate = this.handleProjectDate.bind(this);
    this.handlerInputChange = this.handlerInputChange.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this.openBatchModal = this.openBatchModal.bind(this);
    this.afterOpenBatchModal = this.afterOpenBatchModal.bind(this);
    this.closeBatchModal = this.closeBatchModal.bind(this);
    this.saveBatchModal = this.saveBatchModal.bind(this);
    this.openSetDirection = this.openSetDirection.bind(this);
    this.closeSetDirection = this.closeSetDirection.bind(this);
    this.openGetReport = this.openGetReport.bind(this);
    this.closeGetReport = this.closeGetReport.bind(this);
    this.openExportModal = this.openExportModal.bind(this);
    this.closeExportModal = this.closeExportModal.bind(this);
    this.exportProject = this.exportProject.bind(this);
    this.saveAhuinit = this.saveAhuinit.bind(this);
    this.ahuInitChange = this.ahuInitChange.bind(this);
    this.handleCreateReport = this.handleCreateReport.bind(this);
  }

  // Start Batch Madal Declaration.
  openBatchModal() {
    this.setState({batchModalIsOpen: true});
  }
  afterOpenBatchModal() {
    // references are now sync'd and can be accessed.
    this.refs.subtitle.style.color = '#f00';
  }

  openExportModal(pid){
		this.setState({exPortState:true,projectId:pid});
	}
	closeExportModal(){
		this.setState({exPortState:false});
	}

	exportProject(fileName){
		var pid = this.state.projectId;
		window.location=`${SERVICE_URL}` + 'export/proj/'+pid+'?filename='+fileName;
		 this.closeExportModal();
	}

  saveBatchModal(formData) {
    //Do create project from the configuration here
    const projectObj = this;
    $.ajax({
        url: `${SERVICE_URL}import/proj`,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.code == "0") {
                sweetalert({title:'上传成功！', type: 'success', timer: 3000});
                projectObj.getProjects();
            }
            if (data.code == "1") {
                sweetalert({title:data.msg, type: 'error', timer: 3000});
            }
            projectObj.closeBatchModal();
        },
        error: function () {
            sweetalert({title:'上传失败！', type: 'error', timer: 3000});
            projectObj.closeBatchModal();
        }
    });

  }

  openSetDirection(projectId){
  	this.setState({projectId:projectId, setDirectionState: true});
  }
  closeSetDirection(){
  	this.setState({setDirectionState: false});
  }

  openGetReport(projectId){
  	this.setState({projectId:projectId, getReportState: true});
  }
  closeGetReport(){
  	this.setState({getReportState: false});
  }

	handleChange({target}) {
    const obj = {}
    obj[target.getAttribute('name')] = target.checked;
    this.setState(prevState => {
      const reportForm = Object.assign({}, prevState.reportForm, obj)
      return {reportForm}
    }, () => {
      console.log(this.state.reportForm);
    })
    
    var name = target.getAttribute('name');
    var value = target.checked;
    if(value && (name == "3view" || name == "fan" || name == "hanshi")){
    	$("input[name=techproj]").prop("checked","checked");
    	const newObj = {}
    	newObj['techproj'] = true;
    	this.setState(prevState => {
	      const reportForm = Object.assign({}, prevState.reportForm, newObj)
	      return {reportForm}
	    }, () => {
	      console.log(this.state.reportForm);
	    })
    }
    
  }

	handleCreateReport() {
    let param = {
	      "ahuIds": [],
	      "projectId": this.state.projectId,
	      "reports": [{
	        "type": "excel",
	        "name": "项目清单",
	        "output":false,
	        "path":"/asserts/report/pro1/项目清单.xlsx",
	        "items": [],
	        "classify":"prolist",
	      },{
	      	"type": "excel",
	        "name": "主要参数列表",
	        "output":false,
	        "path":"/asserts/report/pro1/主要参数列表.xlsx",
	        "items": [],
	        "classify":"paralist",
	      }, {
	      	"type": "excel",
	        "name": "长交货期选项",
	        "output":false,
	        "path":"/asserts/report/pro1/长交货期选项.xlsx",
	        "items": [],
	        "classify":"delivery",
	      }, {
            "type": "pdf", 
            "name": "奥雅纳工程格式报告",
            "output": false,
            "path": "/asserts/report/proj1/奥雅纳工程格式报告.pdf",
            "classify": "aruppf",
            "items": [ ]
        }, {
            "type": "pdf", 
            "name": "柏诚工程格式报告", 
            "output": false, 
            "path": "/asserts/report/proj1/柏诚工程格式报告.pdf", 
            "classify": "bochengpf", 
            "items": [ ]
        }, {
            "type": "pdf", 
            "name": "自定义格式报告", 
            "output": false, 
            "path": "/asserts/report/proj1/自定义格式报告.pdf", 
            "classify": "custompf", 
            "items": [ ]
        }, {
	        "type": "pdf", 
	        "name": "技术说明(工程版)", 
	        "output": false, 
	        "path": "/asserts/report/proj1/技术说明(工程版).pdf", 
	        "classify": "techproj",
	        "items": [
	          {"name":"三视图", "classify":"3view", "output":false},
	          {"name":"风机曲线", "classify":"fan", "output":false},
	          {"name":"焓湿图", "classify":"hanshi", "output":false}
	          ]
	      },{
            "type": "pdf", 
            "name": "技术说明(销售版)", 
            "output": false, 
            "path": "/asserts/report/proj1/技术说明(销售版).pdf", 
            "classify": "techsaler", 
            "items": [ ]
        }]
	    }
	    param.reports.forEach(report => {
	      report.items.forEach(item => {
	        if (typeof this.state.reportForm[item.classify] !== typeof undefined) item.output = this.state.reportForm[item.classify]
	      })
	      if (typeof this.state.reportForm[report.classify] !== typeof undefined) report.output = this.state.reportForm[report.classify]
	    })
	    http.post('report', {"reports": JSON.stringify(param)}).then(data => {
	    	if(data.code == 0){
	    		sweetalert({title:'生成成功！', type: 'success', timer: 3000});
	    		this.setState({reports: data.data.reportsObj.reports})
	    	}else{
	    		sweetalert({title:'生成失败！', type: 'error', timer: 3000});
	    	}
	    })
  }

  closeBatchModal() {
    this.setState({batchModalIsOpen: false});
  }

  componentDidMount() {
    this.getProjects();
    this.getSectionProps()
  }
  getSectionProps() {
    http.get('meta/layout').then(data => {
      for (let variable in data) {
        if (data.hasOwnProperty(variable)) data[variable].groups = data[variable].groups.sort((a, b) => a.paras.length - b.paras.length)
      }
      store.setLayout(data)
    })
  }

  ahuInitChange(name,value){
  	var AhuInitList = this.state.AhuInitList;
  	AhuInitList[name] = value;
		this.setState({AhuInitList:AhuInitList});
  }

  saveAhuinit(projectId){
		http.post('mc/savepc', {
      projid: projectId,
      prokey:JSON.stringify(this.state.AhuInitList)
   }).then(data => {
	   	if(data.code == 0){
	   		sweetalert({title:'保存成功！', type: 'success', timer: 3000});
	   	}else{
	   		sweetalert({title:'保存失败！', type: 'error', timer: 3000});
	   	}
      this.closeModal();
    })
  }

  openModal(pid) {
  	http.get('mc/getpc/'+pid).then(data => {
  		var obj = eval('(' + data.data.prokey + ')');
  		this.setState({AhuInitList:obj})
  	})
    this.setState({modalIsOpen: true, projectId: pid});
  }

  afterOpenModal() {
  }

  saveModal() {
    //Do create AHU from the configuration here
    alert("The new AHU has been created.");
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  openProjectModal(pid) {
    this.setState({projectModalIsOpen:true});
    if(pid == null){

    }else{
      http.get('project/'+pid)
      .then(data=>{
        var project = data.data;
        project.projectDate = moment(project.projectDate);
        this.setState({project:project});
      });
    }
  }

  newProject(){
    this.setState({project:{}});
    this.openProjectModal(null);
  }

  afterOpenProjectModal() {
    // references are now sync'd and can be accessed.
   // this.refs.subtitle.style.color = '#f00';
  }

  saveProjectModal() {
    //Do create AHU from the configuration here
    alert("saveProjectModal.");
  }

  closeProjectModal() {
    this.setState({projectModalIsOpen: false});
  }

  getProjects(){
    http.get('project/list')
    .then(data=>{
      this.setState({projects:data.data});
    });
    $("#moreLoding").hide();
  }

  handlerInputChange(event){
    var project = this.state.project;
    var value = event.target.value;
    var name = event.target.name;
    project[name] = value;
    this.setState({project:project});
  }
  handleProjectDate(date){
    var project = this.state.project;
    var projectDate = moment(date).format('YYYY-MM-DD');
    project.projectDate = projectDate;
    this.setState({project:project});
  }
  save(){
    console.log(this.state.project);
    http.post('project/save', this.state.project)
    .then(data=>{
      this.getProjects();
      this.closeProjectModal();
    });
  }

  delete(pid){
    http.post('project/delete/'+pid)
    .then(data=>{
      this.getProjects();
    });
  }

  render() {
    return (
      <div className={style.project}>
        <Header />
        <div className="main promain clear">
            <div className="pro_con">
                <ul className="pro_cap clear">
                    <li onClick={this.newProject}>
                        <i className="iconfont icon-xinjianxiangmu"></i>
                        新建项目
                    </li>
                    <li onClick={this.openBatchModal}>
                        <i className="iconfont icon-daoru"></i>
                       导入项目
                    </li>
                </ul>

                {this.state.projectModalIsOpen && <NewProject openProjectModal={this.openProjectModal} closeProjectModal={this.closeProjectModal}
                   projectModalIsOpen={this.state.projectModalIsOpen}  data={this.state.project}
                   handlerInputChange={this.handlerInputChange} save={this.save}
                   handleProjectDate={this.handleProjectDate}/>}

                <div className="pro_allcont">
                    {this.state.projects.map((project, index) => <ProjectDiv key={index} data={project} delete={this.delete} openExportModal={this.openExportModal} closeExportModal={this.closeExportModal} openModal={this.openModal} openGetReport={this.openGetReport} openSetDirection={this.openSetDirection} openProjectModal={this.openProjectModal} />)}
                </div>
                <div className="more" id="moreLoding">
                    <span><i className="loding"><img src={loadingImg}/> </i>加载更多</span>
                </div>
            </div>

        </div>

        {this.state.batchModalIsOpen && <ProjectBatchImportDiv closeBatchModal={this.closeBatchModal} saveBatchModal={this.saveBatchModal} batchModalIsOpen={this.state.batchModalIsOpen}/>}

        {this.state.exPortState && <ProjectBatchExportDiv closeExportModal={this.closeExportModal} exportFile={this.exportProject} batchModalIsOpen={this.state.batchModalIsOpen}/>}


        <div className="footer">
            <div className="fotop">
                <div className="fot_lef fl">
                    <a href="#" className="">隐私声明</a>
                    <a href="#" className="">使用条款</a>
                    <a href="#" className="">联系我们</a>
                </div>
                <div className="fot_rig fr">
                    <a href="#" className="">开利公司是 联合技术环境、控制与安防 的子公司，联合技术环境、控制与安防隶属于 联合技术公司</a>
                </div>
            </div>
            <p>© 开利公司 2017</p>
        </div>
        <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} contentLabel="Three Views Modal">
		      <AhuInit data={this.state.AhuInitList} saveAhuinit={this.saveAhuinit} id={this.state.projectId} onCloseModal={this.closeModal} ahuInitChange={this.ahuInitChange} flg={1}/>
		    </Modal>
		    {this.state.setDirectionState && <SetDirectionDiv closeSetDirection={this.closeSetDirection} projectId={this.state.projectId}/>}
		    {this.state.getReportState && <div className="modal homModal" id="creatReport" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block'}}>
          <div style={{width:'700px',margin: '30px auto'}} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeGetReport}><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>生成报告</b></h4>
              </div>
              <div className="modal-body clear">
                <div onChange={e => this.handleChange(e)} style={{width:'698px',overflow:'auto'}}>
	                <div style={{padding:'20px'}}>
					        	<div style={{height:'32px'}}>
					        		<div style={{width:'100px',float:'left',color:'#3E3E3E',fontWeight:'bold',fontSize:'13px'}}>报告名称</div>
					        		<input type="text" style={{width:'200px',border:'1px solid #cccccc',float:'left',height:'32px',lineHeight:'32px'}}/>
					        	</div>
					        	<div style={{height:'32px',marginTop:'10px'}}>
					        		<div style={{width:'100px',float:'left',color:'#505050',fontWeight:'bold',fontSize:'13px'}}>报告路径</div>
					        		<input type="text" style={{width:'440px',border:'1px solid #cccccc',float:'left',height:'32px',lineHeight:'32px'}}/>
					        		<button type="button" style={{flot:'left',marginLeft:'20px',width:'80px',height:'32px',lineHeight:'32px',backgroundColor:'#f3f4f8',color:'#a6aeb9'}}>选择</button>
					        	</div>
					          <div style={{marginTop:'20px',marginBottom:'20px'}}>
					          	<div style={{width:'100px',float:'left',color:'#505050',fontWeight:'bold',fontSize:'13px'}}>报告内容</div>
					          	<div style={{float:'left'}}>
					          		<div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'0px 0px 15px 0px'}}>三方资讯文档</div>
					          		<label className="checkbox-inline" style={{marginLeft:'30px',color:'#666666'}}>
						              <input type="checkbox" name="aruppf" style={{height:'13px'}} /> 奥雅纳工程格式报告
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="bochengpf" style={{height:'13px'}} /> 柏诚工程格式报告
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="custompf" style={{height:'13px'}} /> 自定义格式报告
						            </label>
						            <div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'20px 0px 15px 0px'}}>技术说明</div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',fontWeight:'bold',fontSize:'12px',color:'#505050'}}>
						              <input type="checkbox" name="techproj" style={{height:'13px'}} /> 工程图
						            </label>
						            <div style={{marginLeft:'60px',marginBottom:'20px',marginTop:'20px'}}>
						            	<label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="3view" style={{height:'13px'}}/> 三视图
							            </label>
							            <label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="fan" style={{height:'13px'}}/> 风机曲线
							            </label>
							            <label className="checkbox-inline" style={{color:'#666666'}}>
							              <input type="checkbox" name="hanshi" style={{height:'13px'}}/> 焓湿图
							            </label>
						            </div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',fontWeight:'bold',fontSize:'12px',color:'#505050'}}>
						              <input type="checkbox" name="techsaler" style={{height:'13px'}} /> 销售版
						            </label>
						            <div style={{width:'100px',color:'#505050',fontWeight:'bold',fontSize:'12px',margin:'20px 0px 15px 0px'}}>相关文件</div>
						            <label className="checkbox-inline" style={{marginLeft:'30px',color:'#666666'}}>
						              <input type="checkbox" name="prolist" style={{height:'13px'}} /> 项目清单
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="paralist" style={{height:'13px'}} /> 主要参数列表
						            </label>
						            <label className="checkbox-inline" style={{color:'#666666'}}>
						              <input type="checkbox" name="delivery" style={{height:'13px'}} /> 长交货期选项
						            </label>
					          	</div>
					          	<div style={{clear:'both'}}></div>
					          </div>
					        </div>
				        </div>
              </div>
              <div className="modal-footer clear" style={{textAlign:'center'}}>
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.closeGetReport}style={{backgroundColor:'#25c2cb', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>取消</button>
                <button type="button" className="btn btn-primary" onClick={() => this.handleCreateReport()} style={{backgroundColor:'rgba(50,116,207,1)', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>生成报告</button>
              </div>
              <div style={{marginTop:'10px'}}>
			          {this.state.reports.map(report => <a href={report.path} target="_blank" style={{marginRight:'10px'}}>{report.name}</a>)}
			        </div>
            </div>
          </div>
        </div>
     	}
      </div>
    );
  }
}
