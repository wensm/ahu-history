import ReactDataGrid from 'react-data-grid'
import { Editors, Toolbar, Formatters } from 'react-data-grid-addons'
import React from 'react';
import update from 'immutability-helper';
import classnames from 'classnames';
import http from './misc/http';
import store from './modules/ahu/modules/store';

const { AutoComplete: AutoCompleteEditor, DropDownEditor } = Editors

export default class BatchEditor extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      headers: [], rows: [],
    };

    this.rowGetter = this.rowGetter.bind(this);
    this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this);
  }

  componentDidMount() {
    this.getData()
  }

  getData() {
    const p1 = http.get('performance/ahus1',{projectId: this.props.params.projectId})
    const p2 = http.get('meta/layout')
    Promise.all([p1, p2]).then(values => {
      store.setLayout(values[1])
      const rows = values[0].data.values.map(value => {
        value.id = value.ahuId
        return value
      })
      this.setState({headers: this.getHeaders(values[0].data.headers), rows})
    })
  }

  getName(key) {
    for (var variable in store.layout) {
      if (store.layout.hasOwnProperty(variable)) {
        if (variable === key) {
          return store.layout[key].memo
        }
      }
    }
    return 'unknow'
  }

  getOptions(key) {
    for (var variable in store.layout) {
      if (store.layout.hasOwnProperty(variable)) {
        if (variable === key && store.layout[key].option) {
          return store.layout[key].option.options
        }
      }
    }
    return []
  }

  getHeaders(headers) {
    return headers.map(header => {
      header.width = 120
      header.editable = true
      header.resizable = true
      header.name = this.getPrefix(header.key) + '_' + this.getName(header.key)
      if (this.getOptions(header.key).length) {
        header.editor = <DropDownEditor options={this.getOptions(header.key)}/>
      }
      return header
    })
  }

  getPrefix(key) {
    let prefix = ''
    switch (true) {
      case key.includes('ahu'):
        prefix = 'ahu'
        break;
      case key.includes('combinedFilter'):
        prefix = 'com'
        break;
      case key.includes('coolingCoil'):
        prefix = 'coo'
        break;
      case key.includes('fan'):
        prefix = 'fan'
        break;
      case key.includes('filter'):
        prefix = 'fil'
        break;
      case key.includes('mix'):
        prefix = 'mix'
        break;
      default:

    }
    return prefix
  }

  rowGetter(i) {
    return this.state.rows[i];
  }

  handleGridRowsUpdated({ fromRow, toRow, updated }) {
    let rows = this.state.rows.slice();

    for (let i = fromRow; i <= toRow; i++) {
      let rowToUpdate = rows[i];
      let updatedRow = update(rowToUpdate, {$merge: updated});
      rows[i] = updatedRow;
    }

    this.setState({ rows });
  }

  handleClick() {
    console.log(this.state.rows);
    const rows = []
    this.state.rows.forEach(row => {
      rows.push({unitId: row.ahuId, jsonStr: JSON.stringify(row)});
    })
    http.post('performance/update', {str: JSON.stringify(rows)}).then(data => {
      console.log(data);
    })
  }

  render() {
    return (
      <div>
        <div style={{margin:'10px'}}>
          <button type="button" className="btn btn-primary" style={{width:'80px'}} onClick={() => this.handleClick()}>保 存</button>
        </div>
        <ReactDataGrid
          enableCellSelect={true}
          columns={this.state.headers}
          rowGetter={this.rowGetter}
          rowsCount={this.state.rows.length}
          minHeight={window.innerHeight}
          onGridRowsUpdated={this.handleGridRowsUpdated} />
      </div>
    );
  }

}
