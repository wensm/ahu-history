import React from 'react';
import Modal from 'react-modal';
import http from '../misc/http';
import pro_icon1Img from '../images/pro_icon1.png';
import pro_icon2Img from '../images/pro_icon2.png';
import pro_icon3Img from '../images/pro_icon3.png';
import pro_icon4Img from '../images/pro_icon4.png';
import { Link } from 'react-router';
import style from './dialog.css';
export default class NewGroupDiv extends React.Component {
 constructor(props) {
    super(props);
    this.state = {
     unitTypes:[],
     groupNamesIsHas:false,
     groupNameCheckNot:false,
     groupTypeList:[],
    };
    this.handleChange = this.handleChange.bind(this);
    this.saveGroup = this.saveGroup.bind(this);
    this.checkGroupName = this.checkGroupName.bind(this);
  }

  componentDidMount() {
  }

  getUnitTypes() {
   
  }
  
  saveGroup(event){
  	var groupType = $("[name='groupType']").val();
  	this.props.handleGroupNameChange("groupType",groupType);
  	if(this.state.groupNamesIsHas || this.state.groupNameCheckNot || groupType == ""){
  		return false;
  	}else{
  		var groupName = $("[name='groupName']").val();
  		$("[name='groupName']").val(groupName.trim());
  		this.props.savengModal();
  	}
  }
  
  checkGroupName(str){
  	var reg = '^[a-zA-Z0-9\\s\u4e00-\u9fa5]+$';
    if(str.match(reg)){
    	return true;
    }else{
    	return false;
    }
  }

  handleChange(event){
    var value = event.target.value;
    var name = event.target.name;
    if(name == "groupName"){
    	value = value.trim();
    	var msgEl = event.target.nextElementSibling;
		if(value != ''){
			if(this.checkGroupName(value)){
				this.setState({groupNameCheckNot:false})
				http.get('group/checkname/'+value).then(data => {
					if(data.data.type == 1){
						this.setState({groupNamesIsHas:true})
					}else{
						this.setState({groupNamesIsHas:false})
					}
				})
			}else{
	    		this.setState({groupNamesIsHas:false,groupNameCheckNot:true})
	    	}
		}else{
			event.target.value = value;
			this.setState({groupNamesIsHas:false,groupNameCheckNot:false})
		}
    }else if(name == "groupType"){
    	if(value == "tp0"){
    		this.setState({paneltypeIsZdy:true});
    	}else{
    		this.setState({paneltypeIsZdy:false});
    	}
    }
    this.props.handleGroupNameChange(name,value);
  }
  render() {
    const data = this.props.data;
    const groupTypeList = this.props.groupTypeList;
    return (
      <div className="modal-group newGroup newgrop_cont1">
		    <div className="negrp-header">
		        <h4 className="modal-title" ><b>新建Group</b></h4>
		    </div>
		    <div className="negrp-body clear">
		        <ul className="group_cont">
		            <li className="grpcon_li matbm20">
		                <label className="tex_cap">group名称</label>
		            	<p className="exi_tex" style={{width:'228px'}}>
                            <input className="tex_inp" name="groupName" onChange={this.handleChange} type="text"/>
                            {this.state.groupNamesIsHas && <i className="tex">* 已存在</i>}
                            {this.state.groupNameCheckNot && <i className="tex">* 请输入汉字,字母,数字或"_"</i>}
                        </p>
		            </li>
		            <li  className="grpcon_li matbm20">
		                <label className="tex_cap">group备注</label>
		                <input type="text" className="tex_inp" name="drawingNo"  onChange={this.handleChange} />
		            </li>
		            <li  className="grpcon_li matbm20">
		                  <label className="tex_cap">选择类型</label>
		                  
		                  <div className="selected">
		                  <select title="选择类型:" className="tex_inp" name="groupType" onChange={this.handleChange} >
			                  {
			                  	groupTypeList.map((groupType,index)=>{
			                  		if(index == 0){
			                  			return <option value={groupType.id}  selected="selected">{groupType.name}</option>
			                  		}else{
			                  			return <option value={groupType.id}>{groupType.name}</option>
			                  		}
			                  	})
			                  }
		                   </select>
		                      {this.state.paneltypeIsZdy && <a className="zdy_btn">正在建设中...</a>}
		                  </div>
		            </li>
		        </ul>
		    </div>
		    <div className="modal-footer clear">
		        <button type="button" className="btn btn-default btn-info btn_close newgrop_close1" onClick={() => this.props.closengModal()}>取消</button>
		        <button type="button" className="btn btn-primary btn_add" onClick={this.saveGroup}>保存</button>
		    </div>
		</div>
    );
  }
}