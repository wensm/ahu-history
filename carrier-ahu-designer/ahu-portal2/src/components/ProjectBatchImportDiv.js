import React from 'react';
import Modal from 'react-modal';
import pro_icon1Img from '../images/pro_icon1.png';
import pro_icon2Img from '../images/pro_icon2.png';
import pro_icon3Img from '../images/pro_icon3.png';
import pro_icon4Img from '../images/pro_icon4.png';
import { Link } from 'react-router';
import style from './dialog.css';
export default class ProjectBatchImportDiv extends React.Component {
	constructor(props) {
    super(props);
   	this.selFileBtn = this.selFileBtn.bind(this);
   	this.selFileChange = this.selFileChange.bind(this);
   	this.saveBatchModal = this.saveBatchModal.bind(this);
	}
	
	selFileBtn(){
		$("#configInputFile").click();
	}
	selFileChange(){
		var value = $("#configInputFile").val();
		var indenNum = value.lastIndexOf('\\');
		var fileName = value.substring((indenNum+1));
		$("#fileInput").val(fileName);
		return true;
	}
	
	saveBatchModal(){
		var formData = new FormData();
    	formData.append("myfile", document.getElementById("configInputFile").files[0]);
		this.props.saveBatchModal(formData);
	}
  render() {
    console.log(this.props);
    const data = this.props.data;
    return (

     
          <div className="modal homModal" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',marginTop:'200px'}}>
          <div style={{width:'700px',margin: '30px auto'}} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeBatchModal}><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>参数批量导入</b></h4>
              </div>
              <div className="modal-body clear">
                <div>
                  <form>
                   
                    <div className={style.form_group} style={{width:'100%'}}>
                      <label for="configInputFile">选择文件</label>
                      <input id="fileInput" value=""  style={{width:'450px',height:'34px', lineHeight:'34px',padding:'0 10px',border:'#cccccc 1px solid',marginLeft:'10px'}} name="trueattachment"  type="text"/>
                      <button type="button" onClick={this.selFileBtn} style={{width:'90px', height:'34px', lineHeight:'34px', backgroundColor:'#f3f4f8', color:'#b1b8c2', marginLeft:'10px', border:'1px solid #e8eaf1'}}>浏览</button>
                      <input style={{display:'none'}} type="file" id="configInputFile" onChange={this.selFileChange}/>
                    <p className="repot_tex" style={{marginTop:'10px', marginBottom:'10px', height:'32px', lineHeight:'32px'}}>
			                    <input name="" id="td-checkbox25" className="prev_choseAll" style={{verticalAlign:'top',marginRight:'5px'}} type="checkbox"/>
			                    覆盖现有的参数
			                </p>
                    </div>
                </form>
              </div>
              </div>
              <div className="modal-footer clear" style={{marginTop:'20px',textAlign:'center'}}>
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.props.closeBatchModal}style={{backgroundColor:'#25c2cb', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>取消</button>
                <button type="button" className="btn btn-primary" onClick={this.saveBatchModal}style={{backgroundColor:'rgba(50,116,207,1)', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>打开文件</button>
              </div>
            </div>
          </div>
        </div>

    );
  }
}