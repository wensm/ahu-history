import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './Modal';
import Element from '../modules/ahu/modules/Element';
import style from './SetDirectionDiv.css'
import style1 from '../page2.css'
import $ from 'jquery'
import classnames from 'classnames'
import http from '../misc/http';
import GroupDiv from '../modules/ahus/GroupDiv.js';

import pro_icon1Img from '../images/pro_icon1.png';


export default class AhuInit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      values: {},
      zktabState:false,
      ahuGroups:[]
    }
    this.handleChange = this.handleChange.bind(this);
    this.zktabClick = this.zktabClick.bind(this);
    this.tabItemtzkClick = this.tabItemtzkClick.bind(this);
    this.getAhus = this.getAhus.bind(this);
    this.getAhuGroups = this.getAhuGroups.bind(this);
  }

	componentDidMount() {
	    this.getAhuGroups();
	  }
	zktabClick(){
		$(".panel-collapse").removeAttr("style");
		if(this.state.zktabState){
			$('.panel-collapse').removeClass("in");
			$(".zktab").html('<em>全部展开</em><i class="iconfont icon-liebiaozhankai"></i>');
			$(".panel-title .iconfont").addClass("rotasty9");
			this.setState({zktabState: false});
		}else{
			$('.panel-collapse').addClass("in");
			$(".zktab").html('<em>全部收起</em><i class="iconfont icon-liebiaozhankai rotasty9"></i>');
			$(".panel-title .iconfont").removeClass("rotasty9");
			this.setState({zktabState: true});
		}
	}
	
	tabItemtzkClick(index){
		var obj = $("#heading"+index).find(".iconfont");
		$(obj).toggleClass("rotasty9");
		$(".panel-title").each(function(){
			if($(this).closest(".panel").index() != (index-1)){
				$(this).find(".iconfont").addClass("rotasty9");
			}
		});
	}
	
  handleChange({target}) {
    this.setState(prevState => {
      const value = {}
      value[$(target).attr('name')] = target.type === 'checkbox' ? target.checked : target.value
      return {values: Object.assign({}, prevState.values, value)}
    })
  }

  handleSave() {
    http.post('material/ahu/update', {
      projectId: this.props.projectId,
      groupId: this.props.groupId,
      materialStr:JSON.stringify(this.state.values)
    }).then(data => {
      this.props.onCloseModal()
    })
  }
  
  getAhuGroups(){
    http.get('group/lists/'+this.props.projectId)
    .then(data=>{
      this.getAhus(data.data);
    });
  }

  getAhus(ahuGroups){
    for (var i=0;i<ahuGroups.length;i++) {
     ahuGroups[i].ahus = [];
    }
    http.get('ahu/list',{projectId:this.props.projectId,groupId:""})
    .then(data=>{
      var ahus = data.data;
      var notDivided = [];
      for(var j=0;j<ahus.length;j++){
        if(ahus[j].groupId == null || ahus[j].groupId == ''){
          notDivided.push(ahus[j]);
          continue;
        }
        for (var i=0;i<ahuGroups.length;i++) {
          if(!ahuGroups[i].ahus){
            ahuGroups[i].ahus = [];
          }
          if(ahus[j].groupId == ahuGroups[i].groupId){
            ahuGroups[i].ahus.push(ahus[j]);
            break;
          }
        }
      }
      var ahuGroup = {};
      ahuGroup.ahus = notDivided;
      ahuGroup.groupName = '未分组';
      ahuGroup.groupId = null;
      ahuGroup.ahuGroupCode = '';
      ahuGroups.unshift(ahuGroup);
      this.setState({ahuGroups : ahuGroups});
    });
  }

  render() {
    const values = this.state.values
    return (
			<div className="modal homModal" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',marginTop:'100px'}}>
          <div style={{width:'1000px',margin: '0px auto'}} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeSetDirection}><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>方向设置</b></h4>
              </div>
              <div className="modal-body clear" style={{height:'600px',overflow:'auto'}}>
                <div className={classnames(style.groupListDiv)}>
                <form>
                   <div style={{margin:'20px 10px',height:'50px'}}>
                   		<div className={classnames(style.checkboxDiv, 'fl')}>
		                   	<input type="checkbox"/>
                			<span className={classnames(style.selAll)}>全选</span>
                			<span className={classnames(style.setDirection)}>方向设置</span>
		                </div>
	                   	<div className={classnames(style.inputDiv, 'fl')}>
	                   		<Element name="meta.ahu.pipeorientation" values={values} />
	                   	</div>
	                   	<div className={classnames(style.inputDiv, 'fl')}>
	                   		<Element name="meta.ahu.doororientation" values={values} />
	                   	</div>
	                   	<div className={classnames(style.inputDiv, 'fr')}>
	                   		<button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.props.closeSetDirection}style={{backgroundColor:'#727d8b', marginRight:'10px', width:'80px', height:'34px', border:'none',color:'#ffffff'}}>取消</button>
                			<button type="button" className="btn btn-primary" style={{backgroundColor:'#727d8b', marginRight:'10px', width:'80px', height:'34px', border:'none',color:'#ffffff'}}>保存</button>
	                   	</div>
                   </div>
                    {
				        this.state.ahuGroups.map((ahuGroup,index)=> {
				          if(ahuGroup.ahus){
				            return <GroupDiv flg={1} data={ahuGroup} delGroup={this.delGroup} opennaModal={this.opennaModal} openngModal={this.openngModal} openMyModal5={this.openMyModal5} openMyModal6={this.openMyModal6} openMyModal7={this.openMyModal7} openBatchModal={this.openBatchModal} pid={this.props.projectId} delete={this.delete} groupName={ahuGroup.groupName}/>;
				          }
				        })
				       }
                   
                </form>
              </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}
