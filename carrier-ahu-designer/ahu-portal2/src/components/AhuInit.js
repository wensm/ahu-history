import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './Modal';
import Element from '../modules/ahu/modules/Element';
import style from './AhuInit.css'
import style1 from '../page2.css'
import $ from 'jquery'
import classnames from 'classnames'
import http from '../misc/http';

import pro_icon1Img from '../images/pro_icon1.png';

import modal_icon1Img from '../images/modal_icon1.png';
import modal_icon2Img from '../images/modal_icon2.png';
import modal_icon3Img from '../images/modal_icon3.png';
import modal_icon4Img from '../images/modal_icon4.png';
import modal_icon5Img from '../images/modal_icon5.png';
import modal_icon6Img from '../images/modal_icon6.png';
import modal_icon7Img from '../images/modal_icon7.png';

export default class AhuInit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      values: {},
      zktabState:false,
      
    }
    this.handleChange = this.handleChange.bind(this);
    this.zktabClick = this.zktabClick.bind(this);
    this.tabItemtzkClick = this.tabItemtzkClick.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.valueChange = this.valueChange.bind(this);
    this.initElement = this.initElement(this);
  }
  
  initElement(){
  	this.setState({values:this.props.data});
  }

	zktabClick(){
		$(".panel-collapse").removeAttr("style");
		if(this.state.zktabState){
			$('.panel-collapse').removeClass("in");
			$(".zktab").html('<em>全部展开</em><i class="iconfont icon-liebiaozhankai"></i>');
			$(".panel-title .iconfont").addClass("rotasty9");
			this.setState({zktabState: false});
		}else{
			$('.panel-collapse').addClass("in");
			$(".zktab").html('<em>全部收起</em><i class="iconfont icon-liebiaozhankai rotasty9"></i>');
			$(".panel-title .iconfont").removeClass("rotasty9");
			this.setState({zktabState: true});
		}
	}
	
	valueChange(event){
		var name = event.target.name;
		var value = event.target.value;
		if(value == "on"){
			if($(event.target).is(":checked")){
				value = true;
			}else{
				value = false;
			}
		}
		this.props.ahuInitChange(name,value);
	}
	
	tabItemtzkClick(index){
		console.log(index)
		var obj = $("#heading"+index).find(".iconfont");
		$(obj).toggleClass("rotasty9");
		$(".panel-title").each(function(){
			if($(this).closest(".panel").index() != (index-1)){
				$(this).find(".iconfont").addClass("rotasty9");
			}
		});
	}
	
  handleChange({target}) {
    this.setState(prevState => {
      const value = {}
      value[$(target).attr('name')] = target.type === 'checkbox' ? target.checked : target.value
      return {values: Object.assign({}, prevState.values, value)}
    })
  }

  handleSave() {
    this.props.saveAhuinit(this.props.id);
  }
  
  render() {
    const values = this.props.data;
    const ahuPartList = this.props.ahuPartList;
    const flg = this.props.flg;
		const metaMix = false;
		const metaFilter = false;
		const metaCombinedFilter = false;
		const metaCoolingCoil = false;
		const metaFan = false;
    if(flg == 1){
    	this.metaMix = true
    	this.metaFilter = true
    	this.metaCombinedFilter = true
    	this.metaCoolingCoil = true
    	this.metaFan = true
    }else if(flg == 2){
    	for(var i = 0; i < ahuPartList.length; i++){
	    	if(ahuPartList[i].key == "ahu.mix"){
	    		this.metaMix = true
	    	}else if(ahuPartList[i].key == "ahu.filter"){
	    		this.metaFilter = true
	    	}else if(ahuPartList[i].key == "ahu.combinedFilter"){
	    		this.metaCombinedFilter = true
	    	}else if(ahuPartList[i].key == "ahu.coolingCoil"){
	    		this.metaCoolingCoil = true
	    	}else if(ahuPartList[i].key == "ahu.fan"){
	    		this.metaFan = true
	    	}
	    }
    }
    
    return (
				<div className={classnames(style.AhuInit)} id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" onChange={(e) =>this.valueChange(e)}>
		    	<div className="modal-dialog" role="document" style={{width:'935px',margin: '0px'}}>
		        <div className={classnames(style1.select_leng, 'modal-content select_leng')}>
	            <div className={classnames(style1.modal_header, style.modal_header, 'modal-header')}>
	                <h4 className={classnames(style1.modal_title, 'modal-title')} id="myModalLabel">
	                    <b>选项批量设置</b>
	                    <span className={classnames(style1.zktab_all, 'zktab zktab_all active')} onClick={this.zktabClick}><em>全部展开</em><i className="iconfont icon-liebiaozhankai"></i></span>
	                    <span className={classnames(style1.icon_close, 'iconfont icon-close fr')} data-dismiss="modal" onClick={this.props.onCloseModal}></span>
	                </h4>
	            </div>
							<div className={classnames(style.modal_body_pd0, 'modal-body clear')}>
						    <div className={classnames(style.modal_body_div, style1.modal_body_div_min)}>
						      <form>
						       <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								
										 <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading1">
										      <h4 className="panel-title">
										        <a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" onClick={() => this.tabItemtzkClick(1)}>
										        <img src={modal_icon1Img}/>
										          <b>机组配置</b>
										          <i className="iconfont icon-liebiaozhankai fr"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse1" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										      	<ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div>
	                             		<Element name="meta.ahu.product" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
								<ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}>送风机组件</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.sexternalstatic" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
	                          <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}>回风机组件</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.eexternalstatic" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
	                          <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}>外面板</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.exskinm" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.exskincolor" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.exskinw" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             	
	                             	</div>
	                             </li>
	                          </ol>
	                          <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}>内面板</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.inskinm" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.inskincolor" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	            										<Element name="meta.ahu.inskinw" values={values} />
	            									</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	            										<Element name="meta.ahu.panelinsulation" values={values} />
	            									</div>
	                             </li>
	                          </ol>
	                          <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}>出厂</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.delivery" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.package" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.voltage" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		 <Element name="meta.ahu.serial" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.ahu.baseType" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group, style.moda_cont_group_radio)}>
	                             	  <Element name="meta.ahu.ISPRERAIN" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
	                          <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}><Element name="meta.ahu.ISPRERAIN" values={values} /></div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>
		  
										 {this.metaMix && <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading2">
										      <h4 className="panel-title">
										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2" onClick={() => this.tabItemtzkClick(2)}>
										        	<img src={modal_icon2Img}/>
										          <b>混合段</b>
										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse2">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		 <Element name="meta.section.mix.DoorO" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.mix.damperOutlet" values={values} />
											          </div>
											          <div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.mix.damperMeterial" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group, style.moda_cont_group_radio)}>
											            <Element name="meta.section.mix.uvLamp" values={values} />
											            <Element name="meta.section.mix.style" values={values} />
											            <Element name="meta.section.mix.fixRepairLamp" values={values} />
											          </div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>}
			
										 {this.metaFilter && <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading3">
										      <h4 className="panel-title">
										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3" onClick={() => this.tabItemtzkClick(3)}>
										        	<img src={modal_icon3Img}/>
										          <b>单层过滤段</b>
										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse3">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.section.filter.rimMaterial" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.mediaLoading" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.rimThickness" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.filterEfficiency" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.pressureGuage" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.PlankM" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.filterOptions" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.bracketMeterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.filter.fitetF" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>}
			
										  {this.metaCombinedFilter && <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading4">
										      <h4 className="panel-title">
										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4" onClick={() => this.tabItemtzkClick(4)}>
										        	<img src={modal_icon4Img}/>
										          <b>综合过滤段</b>
										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse4">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.section.combinedFilter.LMaterialE" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.RMaterialE" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.rimThick" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.RMaterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.mediaLoading" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.LMaterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.bracketMeterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.pressureGuageB" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.filterF" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.pressureGuageP" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.combinedFilter.PlankM" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>}

											{this.metaCoolingCoil && <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading5">
										      <h4 className="panel-title">
										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5" onClick={() => this.tabItemtzkClick(5)}>
										        	<img src={modal_icon5Img}/>
										          <b>冷水盘管段</b>
										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse5" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse5">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.section.coolingCoil.finType" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.waterCollection" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.connections" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.coilFrameMaterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.coolingDetail.finDensity" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.uTrap" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.outletPipe" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.rows" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.finDensity" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.drainpanMaterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.baffleMaterial" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.tubeDiameter" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.winter.fluidVelocity" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.coolingDetail.rows" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.drainpanType" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.eliminator" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.circuit" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.coolingDetail.circuit" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.coilType" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.coolingCoil.returnrh" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>}
										  {this.metaFan && <div className="panel panel-default">
										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading6">
										      <h4 className="panel-title">
										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6" onClick={() => this.tabItemtzkClick(6)}>
										        	<img src={modal_icon6Img}/>
										          <b>风机段</b>
										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
										        </a>
										      </h4>
										    </div>
										    <div id="collapse6" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse6">
										      <div className={classnames(style.panel_body_div, 'panel-body')}>
										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
	                             <li className={classnames(style.moda_cont_li)}>
	                             	<div className={classnames(style.moda_cont_name)}></div>
	                             	<div className={classnames(style.moda_cont_group)}>
	                             		<Element name="meta.section.fan.power" values={values} />
	                             	</div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.fan.accessDoor" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.fan.type" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.fan.supplier" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.fan.startStyle" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group)}>
											            <Element name="meta.section.fan.outlet" values={values} />
											          </div>
											          <div className={classnames(style.moda_cont_group)}>
											          	<Element name="meta.section.fan.motorPosition" values={values} />
											          </div>
	                             	<div className={classnames(style.moda_cont_group, style.moda_cont_group_radio)}>
	                             		<Element name="meta.section.fan.seismicPringIsolator" values={values} />
											            <Element name="meta.section.fan.beltGuard" values={values} />
											            <Element name="meta.section.fan.doorOnBothSide" values={values} />
											            <Element name="meta.section.fan.fixRepairLamp" values={values} />
											            <Element name="meta.section.fan.para13" values={values} />
											            <Element name="meta.section.fan.ptc" values={values} />
											            <Element name="meta.section.fan.standbyMotor" values={values} />
	                             	</div>
	                             </li>
	                          </ol>
										      </div>
										    </div>
										  </div>}
										  
										</div>
						    </form>
							</div>
						</div>
						<div className={classnames(style.modal_footer, 'modal-footer clear')} style={{marginTop:'20px'}}>
						    <button type="button" className={classnames(style.cancelBtn, 'btn btn-default')} data-dismiss="modal" onClick={this.props.onCloseModal}>取消</button>
						    <button type="button" className={classnames(style.saveBtn, 'btn btn-primary')} onClick={() => this.handleSave()}>保存</button>
			      </div>
		    	</div>
    		</div>
		  </div>
    )
  }
}

//<div className="panel panel-default">
//										    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading7">
//										      <h4 className="panel-title">
//										        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7" onClick={() => this.tabItemtzkClick(7)}>
//										        	<img src={modal_icon7Img}/>
//										          <b>蒸器盘管段</b>
//										          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
//										        </a>
//										      </h4>
//										    </div>
//										    <div id="collapse7" className="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse7">
//										      <div className={classnames(style.panel_body_div, 'panel-body')}>
//										        <ol className={classnames(style.moda_cont_ol, 'clearfix')} >
//	                             <li className={classnames(style.moda_cont_li)}>
//	                             	<div className={classnames(style.moda_cont_name)}></div>
//	                             	<div className={classnames(style.moda_cont_group)}>
//	                             		
//	                             	</div>
//	                             </li>
//	                          </ol>
//										      </div>
//										    </div>
//										  </div>