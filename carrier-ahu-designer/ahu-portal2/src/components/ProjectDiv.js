import React from 'react';
import pro_icon1Img from '../images/pro_icon1.png';
import pro_icon2Img from '../images/pro_icon2.png';
import pro_icon3Img from '../images/pro_icon3.png';
import pro_icon4Img from '../images/pro_icon4.png';
import Element from '../modules/ahu/modules/Element';
import { Link } from 'react-router';
import DateUtil from '../misc/DateUtils';
import http from '../misc/http';
export default class ProjectDiv extends React.Component {
  constructor(props) {
    super(props);

    this.handlerEditClick = this.handlerEditClick.bind(this);
    this.handlerDeleteClick = this.handlerDeleteClick.bind(this);
    this.exportProject = this.exportProject.bind(this);
  }

  handlerEditClick(event){
    var pid = event.target.getAttribute("data-id");
    this.props.openProjectModal(pid);
  }
  
  exportProject(pid){
  	this.props.openExportModal(pid);
  }

  handlerDeleteClick(event){
    var pid = event.target.getAttribute("data-id");
    this.props.delete(pid);
  }

  longToDate(longDate){
    if(longDate ==null || longDate == '' ){
      return "";
    }
    // return 1;
    return DateUtil.getFormatDateByLong(longDate,'yyyy-MM-dd hh:mm:ss');
  }
  render() {
    const data = this.props.data;
    const openModal = this.props.openModal;
    const openSetDirection = this.props.openSetDirection;
    const openGetReport = this.props.openGetReport;
    return (

            <div className="pro_cont clear">
                        <div className="con_cap">
                          <b>项目编号:{data.no}</b>
                            <div className="fr conca_fr">
                                <ol className="contabs fl">
                                    <li><a onClick={() => openModal(data.pid)} className="active">选项批量设置</a></li>
                                    <li><a onClick={() => openGetReport(data.pid)}>生成报告</a></li>
                                    <li><a onClick={() => openSetDirection(data.pid)}>方向设置</a></li>
                                </ol>
                                <ol className="conchose concho_click fr">
                                    <li className="col1"><a href="#"><i className="iconfont icon-daochu" onClick={() => this.exportProject(data.pid)}></i></a></li>
                                    <li className="col2"><a href="#"><i className="iconfont icon-guidang"></i></a></li>
                                    <li className="col3"><a  href="#"><i onClick={this.handlerEditClick}  data-id={data.pid} className="iconfont icon-xiugaishijian"></i></a></li>
                                    <li className="col4"><a href="#"><i onClick={this.handlerDeleteClick} data-id={data.pid} className="iconfont icon-shanchu"></i></a></li>
                                </ol>
                            </div>
                        </div>
                        <div className="con_bottom">
                            <div className="botm_cent fl">
                                <table className="cen_top">
                                  <tbody>
                                  <tr>
                                    <td>
                                        <dl className="item1">
                                            <dt className="fl"><img src={pro_icon1Img}/></dt>
                                            <dd className="fl">
                                                <p>项目名称</p>
                                                <b>{data.name}</b>
                                            </dd>
                                        </dl>
                                    </td>
                                    <td>
                                      <dl className="item2">
                                          <dt className="fl"><img src={pro_icon2Img}/></dt>
                                          <dd className="fl">
                                              <p>日期</p>
                                              <b>{this.longToDate(data.createTime)}</b>
                                          </dd>
                                      </dl>
                                    </td>
                                    <td>
                                      <dl className="item3">
                                          <dt className="fl"><img src={pro_icon3Img}/></dt>
                                          <dd className="fl">
                                              <p>合同编号</p>
                                              <b>{data.contract}</b>
                                          </dd>
                                      </dl>
                                    </td>
                                  </tr>
                                </tbody>
                                </table>
                                <div className="cen_btm ">
                                    <ul className=" cenbtm_lef">
                                        <li className="">
                                            <em className=""><i className="iconfont icon-hetonghao"></i></em>
                                            <span>价格基准</span>
                                            <b>{data.priceBase}</b>
                                        </li>
                                        <li>
                                            <em className=""><i className="iconfont icon-feibiaoxunjiadanbianhao"></i></em>
                                            <span>非标询价单编号</span>
                                            <b>{data.enquiryNo}</b>
                                        </li>
                                        <li>
                                            <em className=""><i className="iconfont icon-xiugaishijian"></i></em>
                                            <span>修改时间</span>
                                            <b>{this.longToDate(data.updateTime)}</b>
                                        </li>
                                    </ul>
                                    <ul className=" cenbtm_lef cenbtm_rig">
                                      <li className="">
                                          <em className=""><i className="iconfont icon-xiangmudizhi"></i></em>
                                          <span>项目地址</span>
                                          <b>{data.address}</b>
                                      </li>
                                      <li>
                                          <em className=""><i className="iconfont icon-xiaoshouyuan"></i></em>
                                          <span>销售员</span>
                                          <b>{data.saler}</b>
                                      </li>
																			<li>
                                          <em className=""><i className="iconfont icon-hetonghao"></i></em>
                                          <span>客户名称</span>
                                          <b>{data.customerName}</b>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="botm_rig fr">
                                <div className="btmrig_cir">
                                    <h2>42<em>%</em></h2>
                                    <div className="echar_img"><img src={pro_icon4Img}/> </div>
                                    <p className="rig_tex">选型完成</p>
                                    <div className="rig_btn"><Link to={'/page2/'+data.pid} style={{color:'white', lineHeight:'40px'}}>进入项目</Link></div>
                                </div>
                            </div>
                        </div>
                    </div>
    );
  }
}
