import React from 'react';
import { Link } from 'react-router';
import DateUtil from '../misc/DateUtils';
import addImg from '../images/add_icon.png';
import style from './dialog.css';
import classnames from 'classnames'
import http from '../misc/http';
import sweetalert from 'sweetalert'
export default class AhuDiv extends React.Component {
  constructor(props) {
    super(props);
    this.handlerDeleteClick = this.handlerDeleteClick.bind(this);
    this.handleDivide = this.handleDivide.bind(this);
    this.insertAhu = this.insertAhu.bind(this);
    this.copyAhu = this.copyAhu.bind(this);
    this.changeDrawingNo = this.changeDrawingNo.bind(this);
    this.openMoveAhuGroup = this.openMoveAhuGroup.bind(this);
  }

  openMoveAhuGroup(ahuId,ahuName){
  	this.props.openMoveAhuGroup(ahuId,ahuName);
  }
  changeDrawingNo(event){
		var ahuId = event.target.getAttribute("data-id");
		var drawingNo = event.target.value;
		var msgEl = event.target.nextElementSibling;
		var tr = $(event.target).closest("tr");
		var addBox = $(tr).children().get(0);
		if(drawingNo == ""){
			msgEl.innerText = '* 1-999整数';
			msgEl.style.display = 'inline';
		}else{
			http.get('ahu/resetno/'+ahuId+'/'+drawingNo).then(data =>{
				if(data.data.type == 1){
					msgEl.innerText = '* '+data.data.errMsg;
				}else{
					msgEl.innerText = '* 修改成功';
					$(addBox).text(data.data.unitNo)
				}
				msgEl.style.display = 'inline';
			});
		}

  }

  longToDate(longDate){
    if(longDate ==null || longDate == '' ){
      return "";
    }
    // return 1;
    return DateUtil.getFormatDateByLong(longDate,'yyyy-MM-dd hh:mm:ss');
  }

  handlerDeleteClick(event){
     var unitid = event.target.getAttribute("data-id");
    this.props.delete(unitid);
  }

  insertAhu(ahuId){
  	http.get('ahu/insert/'+ahuId).then(data => {
  		if(data.code == 0){
  			sweetalert({title:'插入成功', type: 'success', timer: 3000},()=>{
	  			this.props.getAhuGroups();
	  		});
  		}else{
  			sweetalert({title:'插入失败', type: 'success', timer: 3000});
  		}
  	})
  }

  copyAhu(ahuId){
  	http.get('ahu/copy/'+ahuId).then(data => {
  		if(data.code == 0){
  			sweetalert({title:'复制成功', type: 'success', timer: 3000},()=>{
	  			this.props.getAhuGroups();
	  		});
  		}else{
  			sweetalert({title:'复制失败', type: 'success', timer: 3000});
  		}
  	})
  }

  handleDivide(){
    var ahus = this.props.ahuGroup.ahus;
    var ahuIds = [];
    for(var i=0;i<ahus.length;i++){
      if(ahus[i].ahuGroupCodeGroup == this.props.data.ahuGroupCode){
        ahuIds.push(ahus[i].unitid);
      }
    }
    this.props.divide(ahuIds,null,this.props.data.ahuGroupCode);
  }

  render() {
    const data = this.props.data;
    const ahuGroup = this.props.ahuGroup;
    const metaJson = eval('(' + data.metaJson + ')');
    const serial = "";
    if(metaJson != null){
    	this.serial = metaJson["meta.ahu.serial"];
    }else{
    	this.serial = "";
    }
    return(
    	<tr>
    			{this.props.flg == 1 && <td><input type="checkbox"/></td>}
    			{data.recordStatus == 3 && this.props.flg == 3 &&
    				<td><input id={data.unitid} className="selAhuCheckBox" type="checkbox"/></td>
    			}
    			{data.recordStatus != 3 && this.props.flg == 3 &&
    				<td><input id={data.unitid} className="selAhuCheckBox" type="checkbox" disabled="desabled"/></td>
    			}
    			
          <td className="add_box">{data.unitNo}
            <i className="addicon">
            {this.props.flg == 2 && <img src={addImg}/>}
            </i>
          </td>
          <td>

          	{this.props.flg == 1 && <b className="cir" style={{position:'relative'}}>{data.drawingNo}</b>}
          	{this.props.flg == 3 && <span>{data.drawingNo}</span>}
          	{this.props.flg == 2 && <b className="cir" style={{position:'relative'}}>
          		<input type="number" style={{border:'#cccccc 1px solid',borderRadius:'4px',width:'50px',textAlign:'center'}}data-id={data.unitid} defaultValue={data.drawingNo} onChange={this.changeDrawingNo}/>
          		<i className="tex" style={{width:'80px',color:'#fd4c4a',fontSize:'12px',fontStyle:'normal',fontWeight:'500',position:'absolute',display:'none',left:'65px'}}>* 已存在</i>
          		</b>
          	}
          </td>
          {this.props.flg != 3 && <td>{data.name}</td>}
          {this.props.flg == 3 && <td style={{color:'#4E78B3'}}>{data.name}</td>}
          <td>{this.serial}</td>
          {this.props.flg == 1 && <td className="num">{data.mount}</td>}
          {this.props.flg == 2 && <td className="num">{data.mount}</td>}
          {this.props.flg == 1 && <td>向左</td>}
          {this.props.flg == 1 && <td>向右</td>}
          {this.props.flg == 2 && <td>{data.weight}kg</td>}
          {this.props.flg == 2 && <td>￥{data.price}</td>}
          {this.props.flg == 2 && <td className="time">
              <p>{this.longToDate(data.updateTime)}</p>
          </td>}
         {this.props.flg == 2 &&  <td><span className="zt"><em className="rid  graycol"></em>
         			{data.recordStatus == 1 && <span>导入完成</span>}
         			{data.recordStatus == 2 && <span>正在选型</span>}
         			{data.recordStatus == 3 && <span>选型完成</span>}
         			{data.recordStatus == 4 && <span>项目完成</span>}
         			{data.recordStatus == 5 && <span>新建</span>}
         </span></td>}
         {this.props.flg == 3 &&  <td><span className="zt"><em className="rid  graycol"></em>
         			{data.recordStatus == 1 && <span>导入完成</span>}
         			{data.recordStatus == 2 && <span>正在选型</span>}
         			{data.recordStatus == 3 && <span>选型完成</span>}
         			{data.recordStatus == 4 && <span>项目完成</span>}
         			{data.recordStatus == 5 && <span>新建</span>}
         </span></td>}
         {this.props.flg == 2 &&  <td style={{whiteSpace: 'nowrap'}}>
              <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal3" title="详情">
                  <i className="iconfont icon-category-copy"></i>
                  <span className="tb_icon">
                       <em className="bg"></em>
                       <i></i>
                       <em className="tex">详情</em>
                  </span>
              </span>
               <span className="icon_img hover_icon blucor" title="非标配置">
                  <i className="iconfont icon-biaopei"></i>
                  <span className="tb_icon ta_iconbp">
                       <em className="bg bgwid"></em>
                       <i></i>
                       <em className="tex">非标配置</em>
                  </span>
              </span>
              <span className={classnames(style.tbmor_span_hover)}>
              <span className="icon_img hover_icon blucor"><i className="iconfont icon-fuzhi"></i>
			          <span className={classnames(style.tbmor_span, 'more_icon tbmor_icon')}>
	              		<em className="moricon-tex">
	              			<a className="moretex" onClick={() => this.copyAhu(data.unitid)}><span className="iconfont icon-fuzhi"></span>复制</a>
	              			<a className="moretex" onClick={() => this.openMoveAhuGroup(data.unitid,data.name)}><span className="iconfont icon-yizu"></span>移组</a>
					            <a className="moretex" onClick={() => this.insertAhu(data.unitid)}><span className="iconfont icon-xinzengAHU"></span>插入</a>
					            <a href="#" className="moretex"><span className="iconfont icon-guidang"></span>归档</a>
	              		</em>
	              		<i></i>
	              </span>
          		</span>
							</span>
              <span className="icon_img hover_icon redcor" title="删除">
                  <i className="iconfont icon-shanchu" onClick={this.handlerDeleteClick} data-id={data.unitid}></i>
                   <span className="tb_icon">
                       <em className="bg"></em>
                       <i></i>
                       <em className="tex">删除</em>
                  </span>
              </span>

              <a href={'ahu.html/#/' + 'projects/'+ this.props.pid +'/ahus/'+data.unitid}>
                  <span className="icon_img hover_icon blucor"><i className="iconfont icon-chakan"></i></span>
              </a>
          </td>}
      </tr>

    )

  }
}


//<span className="more_icon tbmor_icon" style="display: none;">
//   <span className="tbmor_span">
//         <em className="moricon-tex">
//             <a href="#" className="moretex"><span className="iconfont icon-fuzhi"></span>复制</a>
//             <a href="#" className="moretex"><span className="iconfont icon-yizu"></span>移组</a>
//             <a href="#" className="moretex"><span className="iconfont icon-xinzengAHU"></span>插入</a>
//             <a href="#" className="moretex"><span className="iconfont icon-guidang1" style="font-size: 12px;"></span>归档</a>
//         </em>
//         <i></i>
//   </span>
//</span>



//<span className="icon_img hover_icon blucor"><i className="iconfont icon-fuzhi"></i></span>

//    {
//
//              (ahuGroup.ahuGroupId == null)?(
//                <span className="icon_img" title="分组">
//                  <i onClick={this.handleDivide} className="iconfont icon-xinjiangroup"></i>
//                  <span className="tb_icon">
//                    <em className="bg"></em>
//                    <i></i>
//                    <em className="tex">分组</em>
//                  </span>
//                </span>):null
//
//            }
//
//
//            <span className="icon_img hover_icon blucor"><i className="iconfont icon-yizu"></i></span>
