import React from 'react';
import Modal from 'react-modal';
import pro_icon1Img from '../images/pro_icon1.png';
import pro_icon2Img from '../images/pro_icon2.png';
import pro_icon3Img from '../images/pro_icon3.png';
import pro_icon4Img from '../images/pro_icon4.png';
import { Link } from 'react-router';
import http from '../misc/http';
import style from './dialog.css';
import classnames from 'classnames'
export default class NewAhuDiv extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ahu:{pid:this.props.pid},
      paneltypeIsZdy:false,
      newUnitNo:this.props.newUnitNo,
      newDrawingNo:this.props.newDrawingNo,
      isCanSave:true,
    }
    this.handlerInputChange = this.handlerInputChange.bind(this);
    this.save = this.save.bind(this);
  }

  handlerInputChange(event){
    var ahu = this.state.ahu;
    var value = event.target.value;
    var name = event.target.name;
    ahu[name] = value;
    this.setState({ahu:ahu});
    if(name == "paneltype"){
    	if(value == "自定义"){
    		this.setState({paneltypeIsZdy:true});
    	}else{
    		this.setState({paneltypeIsZdy:false});
    	}
    }else if(name == "drawingNo"){
    	var msgEl = event.target.nextElementSibling;
    	if(value == ""){
				msgEl.innerText = '* 1-999整数';
				msgEl.style.display = 'inline';
				this.setState({isCanSave:false});
			}else{
				http.get('ahu/checkno/'+value).then(data =>{
					if(data.data.type == 1){
						msgEl.innerText = '* '+data.data.errMsg;
						msgEl.style.display = 'inline';
						this.setState({isCanSave:false});
					}else{
						msgEl.style.display = 'none';
						this.setState({newUnitNo:data.data.unitNo,newDrawingNo:data.data.drawingNo,isCanSave:true});
					}
				});
			}
    }
  }
  save(){
  	if(this.state.isCanSave){
  		var ahu = this.state.ahu;
	  	ahu['unitNo'] = this.state.newUnitNo;
	  	ahu['drawingNo'] = this.state.newDrawingNo;
	  	if(this.props.newAhuGroupId != null &&　this.props.newAhuGroupType != null){
	  		ahu['groupId'] = this.props.newAhuGroupId;
	    	ahu['paneltype'] = this.props.newAhuGroupType;
	  	}
	    http.post('ahu/save', ahu)
	    .then(data=>{
	      this.props.closenaModal();
	      this.props.getAhus();
	    });
  	}
  }
  render() {
    const data = this.props.data;
    return (
<div className="modal-group newGroup newgrop_cont1">
    <div className="negrp-header">
        <h4 className="modal-title" ><b>新建AHU</b></h4>
    </div>
    <div className="negrp-body clear">
        <ul className="group_cont">
            <li className="grpcon_li matbm20">
                <label className="tex_cap">机组编号</label>
                <span style={{color:'#a7a7a7'}}>{this.state.newUnitNo}</span>
            </li>
            <li  className="grpcon_li matbm20">
                <label className="tex_cap">图纸编号</label>
                <p className="exi_tex" style={{width:'228px'}}>
                    <input type="number" className="tex_inp" name="drawingNo" defaultValue={this.state.newDrawingNo} onChange={this.handlerInputChange} />
                    <i className="tex" style={{display:'none',right:'20px'}}>* 已存在</i>
                </p>
            </li>
            <li  className="grpcon_li matbm20">
                <label className="tex_cap">机组名称</label>
                <input type="text" className="tex_inp" name="name"  onChange={this.handlerInputChange} />
            </li>
            <li  className="grpcon_li matbm20">
                <label className="tex_cap">机组数量</label>
                <input type="number" className="tex_inp" name="mount"  onChange={this.handlerInputChange} />
            </li>
        </ul>
    </div>
    <div className="modal-footer clear">
        <button type="button" className="btn btn-default btn-info btn_close newgrop_close1" onClick={() => this.props.closenaModal()}>取消</button>
        <button type="button" className="btn btn-primary btn_add" onClick={this.save}>保存</button>
    </div>
</div>
    );
  }
}
