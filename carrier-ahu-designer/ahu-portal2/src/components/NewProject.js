import React from 'react';
import Modal from 'react-modal';
import http from '../misc/http';
import style from './dialog.css';
import DateUtil from '../misc/DateUtils';
import DatePicker from 'react-datepicker';
import moment from 'moment';

export default class NewProject extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      projectDate:moment()
    }

    this.handleProjectDate = this.handleProjectDate.bind(this);
  }

  handleProjectDate(date){
    this.setState({projectDate:date});
    this.props.handleProjectDate(date);
  }

  longToDate(longDate){
    if(longDate ==null || longDate == '' ){
      return "";
    }
    // return 1;
    return DateUtil.getFormatDateByLong(longDate,'yyyy-MM-dd hh:mm:ss');
  }


  render() {
    const data = this.props.data;
    return (
        <div className="modal-group newGroup newgrop_cont1">
				    <div className="negrp-header">
				        <h4 className="modal-title" ><b>新建项目</b></h4>
				    </div>
				    <div className="negrp-body clear">
				        <ul className="group_cont">
				            <li className="grpcon_li matbm20">
				                <label className="tex_cap">日期</label>
				                <DatePicker className="tex_inp" selected={moment(data.projectDate)} onChange={this.props.handleProjectDate}   dateFormat="YYYY/MM/DD" />
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">项目编号</label>
				                <input type="text" className="tex_inp" name="no" value={data.no} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                  <label className="tex_cap">项目名称</label>
				                  <input type="text" className="tex_inp" name="name" value={data.name} onChange={this.props.handlerInputChange} />
                  			<input type="hidden" value={data.pid}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">销售员</label>
				                <input type="text" className="tex_inp" name="saler" value={data.saler} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">项目地址</label>
				                <input type="text" className="tex_inp" name="address" value={data.address} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">非标询价单号</label>
				                <input type="text" className="tex_inp" name="enquiryNo" value={data.enquiryNo} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                  <label className="tex_cap">合同号</label>
				                  <input type="text" className="tex_inp" name="contract" value={data.contract} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">价格基准</label>
				                <input type="text" className="tex_inp" name="priceBase" value={data.priceBase} onChange={this.props.handlerInputChange}/>
				            </li>
				            <li  className="grpcon_li matbm20">
				                <label className="tex_cap">客户名称</label>
				                <input type="text" className="tex_inp" name="customerName" value={data.customerName} onChange={this.props.handlerInputChange}/>
				            </li>
				        </ul>
				    </div>
				    <div className="modal-footer clear">
				        <button type="button" className="btn btn-default btn-info btn_close newgrop_close1" onClick={() => this.props.closeProjectModal()}>取消</button>
				        <button type="button" className="btn btn-primary btn_add" onClick={this.props.save}>保存</button>
				    </div>
				</div>
    )
  }
}





//<div className="modal homModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block'}}>
//<div style={{width:'600px',margin: '30px auto'}} role="document">
//<div className="modal-content">
//<div className="modal-header">
//  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeProjectModal}><span aria-hidden="true">&times;</span></button>
//  <h4 className="modal-title" id="myModalLabel"><b>编辑项目</b></h4>
//</div>
//<div className="modal-body clear">
//  <div>
//    <form>
//     
//      <div className={style.form_group}>
//        <label for="name">项目名称</label>
//        <input type="text" className="form-control" name="name" value={data.name} onChange={this.props.handlerInputChange}/>
//        <input type="hidden" value={data.pid}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="projectDate">项目日期</label>
//        <div>
//          <DatePicker className="form-control" selected={moment(data.projectDate)} onChange={this.handleProjectDate}   dateFormat="YYYY/MM/DD" />
//        </div>
//      </div>
//      <div className={style.form_group}>
//        <label for="no">项目编号</label>
//        <input type="text" className="form-control" name="no" value={data.no} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="contract">合同号</label>
//        <input type="text" className="form-control" name="contract" value={data.contract} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="enquiryNo">非标询价单编号</label>
//        <input type="text" className="form-control" name="enquiryNo" value={data.enquiryNo} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="address">项目地址</label>
//        <input type="text" className="form-control" name="address" value={data.address} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="saler">销售员</label>
//        <input type="text" className="form-control" name="saler" value={data.saler} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="address">价格基准</label>
//        <input type="text" className="form-control" name="priceBase" value={data.priceBase} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div className={style.form_group}>
//        <label for="saler">客户名称</label>
//        <input type="text" className="form-control" name="customerName" value={data.customerName} onChange={this.props.handlerInputChange}/>
//      </div>
//      <div style={{clear:'both'}}></div>
//  </form>
//</div>
//</div>
//<div className="modal-footer clear" style={{marginTop:'20px'}}>
//  <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.props.closeProjectModal}>取消</button>
//  <button type="button" className="btn btn-primary" onClick={this.props.save}>保存</button>
//    </div>
//  </div>
//</div>
//</div>