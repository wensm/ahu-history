import React from 'react';
import Modal from 'react-modal';
import pro_icon1Img from '../images/pro_icon1.png';
import pro_icon2Img from '../images/pro_icon2.png';
import pro_icon3Img from '../images/pro_icon3.png';
import pro_icon4Img from '../images/pro_icon4.png';
import { Link } from 'react-router';
import style from './dialog.css';
export default class ProjectBatchImportDiv extends React.Component {
	constructor(props) {
    super(props);
   	this.exportFile = this.exportFile.bind(this);
	}
	
	exportFile(){
		var fileName = $("#fileName").val();
		this.props.exportFile(fileName);
	}
	
  render() {
    console.log(this.props);
    const data = this.props.data;
    return (

     
          <div className="modal homModal" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display:'block',marginTop:'200px'}}>
          <div style={{width:'700px',margin: '30px auto'}} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeExportModal}><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>参数批量导出</b></h4>
              </div>
              <div className="modal-body clear">
                <div style={{marginTop:'20px',height:'100px'}}>
                  <form>
                   	<div className={style.form_group} style={{width:'100%'}}>
                      <label for="configInputFile">文件名称</label>
                      <input id="fileName"  style={{width:'450px',height:'34px', lineHeight:'34px',padding:'0 10px',border:'#cccccc 1px solid',marginLeft:'10px'}} name="trueattachment"  type="text"/>
                    </div>
                </form>
              </div>
              </div>
              <div className="modal-footer clear" style={{marginTop:'20px',textAlign:'center'}}>
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.props.closeExportModal}style={{backgroundColor:'#25c2cb', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>取消</button>
                <button type="button" className="btn btn-primary" onClick={this.exportFile}style={{backgroundColor:'rgba(50,116,207,1)', marginRight:'10px', width:'105px', height:'41px', border:'none',color:'#ffffff'}}>导出</button>
              </div>
            </div>
          </div>
        </div>

    );
  }
}