/* eslint-disable */
import 'babel-polyfill'
import Es6Promise from 'es6-promise';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import {arrayFind} from './misc/polyfill';
import App from './App';
import Project from './Project';
import Page2 from './Page2';
import Ahus from './Ahus';
import Ahu from './Ahu';
import Report from './Report';
import BatchEditor from './BatchEditor';
import DefaultParameter from './DefaultParameter';
import Header from './Header';

import '!style-loader!css-loader!./css/font-awesome.css';
import '!style-loader!css-loader!./css/common.css';
import '!style-loader!css-loader!normalize.css';
import '!style-loader!css-loader!react-s-alert/dist/s-alert-default.css';
import '!style-loader!css-loader!react-s-alert/dist/s-alert-css-effects/slide.css';
import '!style-loader!css-loader!react-tabs/style/react-tabs.css';
import '!style-loader!css-loader!./css/bootstrap.css';
import '!style-loader!css-loader!./css/pro.css';
import '!style-loader!css-loader!./css/projects.css';
import '!style-loader!css-loader!./css/reset.css';
import '!style-loader!css-loader!./css/style.min.css';
import '!style-loader!css-loader!./css/table.css';
import '!style-loader!css-loader!./css/select.css';
import '!style-loader!css-loader!./fonts/iconfont.css';
import '!style-loader!css-loader!../node_modules/sweetalert/dist/sweetalert.css';
import '!style-loader!css-loader!./css/react-datepicker.min.css';

Es6Promise.polyfill();
arrayFind();

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Project} />
      <Route path="project" component={Project} />
      <Route path="page2/:pid" component={Page2} />
      <Route path="projects/:projectId/ahus" component={Ahus} />
      <Route path="projects/:projectId/ahus/:ahuId" component={Ahu} />
      <Route path="defaultParameter" component={DefaultParameter} />
      <Route path="header" component={Header} />
      <Route path="projects/:projectId/report" component={Report} />
      <Route path="projects/:projectId/batchEditor" component={BatchEditor} />
    </Route>
  </Router>
), window.document.getElementById('root'));

console.log(PRODUCTION); // eslint-disable-line
console.log(SERVICE_URL); // eslint-disable-line
