import React from 'react'
import $ from 'jquery'
import Alert from 'react-s-alert'
import { Link } from 'react-router'
import sweetalert from 'sweetalert'
import Modal from './components/modal'
import http from './misc/http'
import {bindthis, clone} from './misc/util'
import Sections from './modules/ahu/Sections'
import Templates from './modules/ahu/Templates'
import Dropzone from './modules/ahu/Dropzone'
import Form from './modules/ahu/Form'
import Detail from './modules/ahu/Detail'
import store from './modules/ahu/modules/store'
import style from './ahu.css'
import img1 from './images/LOGObg.png'
import img2 from './images/person.png'
export default class Ahu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sections: [], sectionProps: {}, sectionDefaultValues: {}, sectionValues: {}, sectionCalValues: {}, ahuSections: [], selectedSection: '', selectedDropZoneIndex: 0, modalIsOpen: false, threeViews: '', isSplit: false, separator: {},
      selectedCalRsltIdx: -1,
    }
    bindthis(this, ['handleSelectAhuTemplate', 'handleSelectedCalRsltIdxChange', 'handleComplete', 'handleCalculate', 'handleClickSeparator', 'handleSave', 'handleClick', 'handleChange', 'handleAhuDrop', 'handleClickAhu', 'handleDragStart', 'handleTrashDrop', 'handleAutoAlign', 'openModal', 'afterOpenModal', 'closeModal', 'handleClickFd',])
 		this.tabItemtzkClick = this.tabItemtzkClick.bind(this);
  }
  
  tabItemtzkClick(index){
		var obj = $("#heading"+index).find(".iconfont");
		$(obj).toggleClass("rotasty9");
		$(".panel-title").each(function(){
			if($(this).closest(".panel").index() != (index-1)){
				$(this).find(".iconfont").addClass("rotasty9");
			}
		});
	}
  
  componentDidMount() {
    this.getSections()
    this.getSectionProps()
    this.getDefaultValues()
    this.queryAhu(this.props.params.ahuId)
  }
  componentWillReceiveProps(nextProps) {
    this.queryAhu(nextProps.params.ahuId)
  }
  queryAhu(ahuId) {
    Promise.all([http.get(`ahu/${ahuId}`), http.get('section/list', {ahuId})]).then(values => {
      const sectionValues = {}
      sectionValues[999] = JSON.parse(values[0].data.metaJson)
      values[1].data.forEach(section => {sectionValues[section.position] = JSON.parse(section.metaJson)})
      this.setState({sectionValues, ahuSections: values[1].data})
    })
  }
  handleChange(key, value) {
    console.log(key, value)
    this.setState(prevState => {
      const sectionValues = $.extend(true, {}, prevState.sectionValues)
      if (!sectionValues[this.state.selectedDropZoneIndex]) sectionValues[this.state.selectedDropZoneIndex] = {}
      sectionValues[this.state.selectedDropZoneIndex][key] = value
      return {sectionValues}
    })
  }
  handleSave() {this.saveAhu(this.props.params.projectId, this.props.params.ahuId)}
  saveAhu() {
    const sections = $.extend(true, [], this.state.ahuSections)
    sections.forEach(section => {section.metaJson = JSON.stringify(this.state.sectionValues[section.position])})
    http.post('section/bacthsave', {pid: this.props.params.projectId, unitid: this.props.params.ahuId, ahuStr: window.JSON.stringify(this.state.sectionValues[999]), sectionStr: window.JSON.stringify(sections)}).then(data => {
      sweetalert({title:'保存成功', type: 'success', timer: 3000})
      this.queryAhu(this.props.params.ahuId)
    })
  }

  handleComplete(){
    console.log("TODO: set complete status")
  }
  handleCalculate() {
    const sections = $.extend(true, [], this.state.ahuSections)
    sections.forEach(section => {section.metaJson = JSON.stringify(this.state.sectionValues[section.position])})
    http.post('cal', {pid: this.props.params.projectId, unitid: this.props.params.ahuId, position: this.state.selectedDropZoneIndex, ahuStr: window.JSON.stringify(this.state.sectionValues[999]), sectionStr: window.JSON.stringify(sections)}).then(data => {
      const sectionCalValues = Object.assign({}, this.state.sectionCalValues);
      sectionCalValues[this.state.selectedDropZoneIndex] = data.data;
      this.setState({sectionCalValues}, () => {
        this.updateSectionValues(this.state.sectionCalValues)
      })
    })
  }
  updateSectionValues() {
    if (this.state.selectedSection === 'ahu.filter' || this.state.selectedSection === 'ahu.mix') {
      this.setState(prevState => {
        const sectionCalValues = clone(prevState.sectionCalValues)
        const sectionValues = clone(prevState.sectionValues)
        for (let variable in sectionCalValues) {
          if (sectionCalValues.hasOwnProperty(variable)) {
            sectionValues[variable] = Object.assign({}, sectionValues[variable], sectionCalValues[variable])
          }
        }
        return ({sectionValues})
      })
    }
  }
  handleSelectedCalRsltIdxChange(index) {
    this.setState(prevState => {
      const sectionCalValues = clone(prevState.sectionCalValues)
      const sectionValues = clone(prevState.sectionValues)
      for (let variable in sectionCalValues) {
        if (sectionCalValues.hasOwnProperty(variable)) {
          sectionValues[variable] = Object.assign({}, sectionValues[variable], sectionCalValues[variable][index])
        }
      }
      return ({selectedCalRsltIdx:index, sectionValues})
    })
  }
  getSections() {
    http.get('meta/sections').then(data => {this.setState({sections: data})})
  }
  getDefaultValues() {
    http.get('template').then(data => {this.setState({sectionDefaultValues: data})})
  }
  getSectionProps() {
    http.get('meta/layout').then(data => {
      for (let variable in data) {
        if (data.hasOwnProperty(variable)) data[variable].groups = data[variable].groups.sort((a, b) => a.paras.length - b.paras.length)
      }
      store.setLayout(data)
      this.setState({sectionProps: data, selectedDropZoneIndex: 999, selectedSection: 'ahu'}, () => {
        setTimeout(() => {this.handleClick(this.state.selectedDropZoneIndex, this.state.selectedSection)}, 500)
      })
    })
  }
  handleDragStart(section, position) {
    this.from = position
    this.section = section
  }
  handleDragOver(event) {event.preventDefault()}
  isNumeric(n) {return !isNaN(parseFloat(n)) && isFinite(n)}
  handleAhuDrop(position) {
    const to = Number(position)
    if (this.isNumeric(this.from)) {
      const from = Number(this.from)
      const newAhuSections = clone(this.state.ahuSections)
      newAhuSections.forEach(section => {if (section.position === from) section.position = to})
      this.checkAhuSections(newAhuSections).then(data => {
        if (data.pass) {
          this.setState(prevState => {
            const ahuSections = clone(prevState.ahuSections)
            const sectionValues = clone(prevState.sectionValues)
            const sectionCalValues = clone(prevState.sectionCalValues)
            ahuSections.forEach(section => {if (section.position === from) section.position = to})
            sectionValues[to] = sectionValues[from]
            sectionValues[from] = {}
            sectionCalValues[to] = sectionCalValues[from]
            sectionCalValues[from] = {}
            return {ahuSections, sectionValues, sectionCalValues, selectedDropZoneIndex: to, selectedSection: this.section}
          }, ()=> {this.handleClick(this.state.selectedDropZoneIndex, this.state.selectedSection)})
        } else sweetalert({title:'Oops...', text: data.msg, type: 'error', timer: 3000})
      })
    } else {
      const newAhuSections = this.state.ahuSections.concat({key: this.section, position: to})
      this.checkAhuSections(newAhuSections).then(data => {
        if (data.pass) {
          this.setState(prevState => {
            const ahuSections = prevState.ahuSections.concat({key: this.section, position: to})
            return {ahuSections, selectedDropZoneIndex: to, selectedSection: this.section}
          }, () => {this.handleClick(this.state.selectedDropZoneIndex, this.state.selectedSection)})
        } else sweetalert({title:'Oops...', text: data.msg, type: 'error', timer: 3000})
      })
    }
  }
  handleAutoAlign() {
$('[data-index]').find('i').remove()
    const begin = Math.floor((15 - this.state.ahuSections.length) / 2)
    const newAhuSections = this.sortAhuSections(clone(this.state.ahuSections))
    let k = begin
    newAhuSections.forEach(section => {
      section.position = k
      k++
    })
    this.checkAhuSections(newAhuSections).then(data => {
      if (data.pass) {
        this.setState(prevState => {
          let i = begin
          const sectionValues = {}
          const sectionCalValues = {}
          sectionValues[999] = prevState.sectionValues[999]
          const ahuSections = this.sortAhuSections(clone(prevState.ahuSections))
          ahuSections.forEach(section => {
            sectionValues[i] = prevState.sectionValues[section.position]
            sectionCalValues[i] = prevState.sectionCalValues[section.position]
            i++
          })
          let j = begin
          ahuSections.forEach(section => {
            section.position = j
            j++
          })
          return {ahuSections, sectionValues, sectionCalValues, selectedDropZoneIndex: 999, selectedSection: 'ahu'}
        }, () => {sweetalert({title:'自动连接完成', type: 'success', timer: 3000})})
      } else sweetalert({title:'Oops...', text: data.msg, type: 'error', timer: 3000})
    })
  }
  checkAhuSections(ahuSections) {
    if (ahuSections.length > 1) {
      const sortedAhuSections = this.sortAhuSections(ahuSections)
      const sections = []
      sortedAhuSections.forEach(section => {
        const sec = {}
        sec.fullName = section.key
        sec.position = section.position
        sec.cnName = this.getSectionCnName(section.key)
        sections.push(sec)
      })
      return http.get('sectionrelation', {sections: JSON.stringify(sections)})
    } else return Promise.resolve({pass: true})
  }
  sortAhuSections(ahuSections) {
    return ahuSections.sort((a, b) => {
      if (a.position > b.position) return 1
      if (a.position < b.position) return -1
      return 0
    })
  }
  getSectionCnName(key) {
    return  this.state.sections.filter(section => section.metaId === key)[0].cName
  }
  handleTrashDrop(event) {
    event.preventDefault()
    this.removeAhuSectionByPosition(this.from)
  }
  removeAhuSectionByPosition(position) {
    this.setState(prevState => {
      let ahuSections = $.extend(true, [], prevState.ahuSections)
      const from = Number(position)
      const sectionValues = $.extend(true, {}, prevState.sectionValues)
      ahuSections = ahuSections.filter(section => section.position !== from)
      sectionValues[from] = {}
      return {ahuSections, sectionValues, selectedDropZoneIndex: 999, selectedSection: 'ahu'}
    })
  }
  handleClick(index, section) {
    if (index < 100) $('#indicator').css('left', index * 50 + 38)
    this.setState(prevState => {
      const sectionDefaultValue = prevState.sectionDefaultValues[section]
      const sectionValues = $.extend(true, {}, prevState.sectionValues)
      if (!sectionValues[index]) sectionValues[index] = {}
      const sectionValue = sectionValues[index]
      for (let key in sectionDefaultValue) {
        if (typeof sectionValue[key] === typeof undefined) sectionValue[key] = sectionDefaultValue[key]
      }

        console.log(sectionValues[index].sectionL)
      return {selectedDropZoneIndex: index, selectedSection: section, sectionValues}
    })
  }
  handleClickAhu({target}) {
    if (!$(target).attr('draggable')) this.setState({selectedDropZoneIndex: 999, selectedSection: 'ahu'}, () => {
      $('#indicator').css('left', 10);
      this.handleClick(this.state.selectedDropZoneIndex, this.state.selectedSection)
    })
  }
  openModal() {this.setState({modalIsOpen: true})}
  afterOpenModal() {
    http.get(`ahu/cad/threeview/${this.props.params.ahuId}`).then(data => {
      this.setState({threeViews: data.data})
    })
  }
  closeModal() {this.setState({modalIsOpen: false})}
  handleClickFd() {
    console.log(this.state)
    this.setState({isSplit: !this.state.isSplit})
  }
  handleClickSeparator(position) {
    this.setState(prevState => {
      const separator = $.extend(true, {}, prevState.separator)
      if (typeof separator[position] === typeof undefined) separator[position] = true
      else separator[position] = !separator[position]
      return {separator}
    })
  }
  handleSelectAhuTemplate(data) {
    const sectionValues = {}
    sectionValues[999] = JSON.parse(JSON.parse(data.ahuContent).metaJson)
    JSON.parse(data.sectionContent).forEach(section => {sectionValues[section.position] = JSON.parse(section.metaJson)})
    this.setState({sectionValues, ahuSections: JSON.parse(data.sectionContent)})
  }
  render() {
    const section = this.state.sections.find(section => section.metaId === this.state.selectedSection)
    return (
      <div>
        <div className="header clear pro_head">
            <div className="header-content">
                <div className="logo">
                  <Link to="/project">
                    <img src={img1}/>
                  </Link>
                </div>
                <div className="logo_righ">
                    <div className="header-login">
                        <div className="seach">
                            <a href="javascript:void(0)" className="mar10">
                              <i className="iconfont icon-iconfontdayinji sizesty"></i>
                            </a>
                            <a href="javascript:void(0)" onClick={this.handleSave}>
                              <i className="iconfont icon-baocun"></i>
                            </a>
                        </div>
                        <dl>
                            <dt className="fl"><img src={img2}/> </dt>
                            <dd className="fl">
                                <p>Bruce Wang</p>
                                <p>张江高科工厂</p>
                            </dd>
                        </dl>
                        <div className="fr down">
                            <i className="iconfont icon-touxiangzhankaibtn"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={style.main} onDragOver={this.handleDragOver}>
            <div className={style.west}>
              <div className={style.goback}>
                <Link to={`page2/${this.props.params.projectId}`}>返回项目</Link> <i className="iconfont icon-fanhui" />
              </div>
              
              <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true" >
							  <div className="panel panel-default">
							    <div className="panel-heading" role="tab" id="heading1">
							      <h4 className="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" onClick={() => this.tabItemtzkClick(1)}>
							          段选择
							          <i className="iconfont icon-liebiaozhankai fr"></i>
							        </a>
							      </h4>
							    </div>
							    <div id="collapse1" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
							      <div className="panel-body">
												<Sections data={this.state.sections} onDragStart={this.handleDragStart} />
										</div>
							    </div>
							  </div>
							  <div className="panel panel-default">
							    <div className="panel-heading" role="tab" id="heading2">
							      <h4 className="panel-title">
							        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2" onClick={() => this.tabItemtzkClick(2)}>
							          行业类型
							          <i className="iconfont icon-liebiaozhankai fr rotasty9"></i>
							        </a>
							      </h4>
							    </div>
							    <div id="collapse2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
							      <div className="panel-body">
							        <Templates data={this.state.templates} onSelectAhuTemplate={this.handleSelectAhuTemplate} />
							      </div>
							    </div>
							  </div>
							  
							</div>
            </div>
            <div className={style.east}>
              <Detail data={section} ahuValues={this.state.sectionValues[999]} sectionValues={this.state.sectionValues[this.state.selectedDropZoneIndex]} />
            </div>
            <div className={style.center}>
                <div className="dep_cenleft" style={{width:'100%'}}>
                    <div className="dep_cen">
                      <div className="decen_btm" style={{padding:'10px 0px'}}>
                        <button type="button" className="debtm_btn2" onClick={this.handleClickFd}>分段</button>
                        <button type="button" className="debtm_btn2">焓湿图</button>
                        <button type="button" className="debtm_btn2" onClick={this.openModal}>三视图</button>
                        <button type="button" className="debtm_btn2">面板布置</button>
                        <button type="button" className="debtm_btn1">价格</button>
                        <button type="button" className="debtm_btn2" onClick={this.handleCalculate}>计算</button>
                      </div>
                        <div className="decen_botm" onClick={this.handleClickAhu}>
                          <Dropzone sections={this.state.ahuSections} dropzoneNum={15} onDragStart={this.handleDragStart} onAhuDrop={this.handleAhuDrop} onTrashDrop={this.handleTrashDrop} onClick={this.handleClick} onHandleAutoAlign={this.handleAutoAlign}
                            isSplit={this.state.isSplit} separator={this.state.separator} onClickSeparator={this.handleClickSeparator} />
                        </div>
                    </div>
                </div>
                <div className="dep_rig">
                  <Form section={this.state.selectedSection} groups={this.state.selectedSection ? this.state.sectionProps[this.state.selectedSection].groups : []}
                    sectionValues={this.state.sectionValues[this.state.selectedDropZoneIndex] ? this.state.sectionValues[this.state.selectedDropZoneIndex] : {}}
                    sectionCalValues={this.state.sectionCalValues[this.state.selectedDropZoneIndex] ? this.state.sectionCalValues[this.state.selectedDropZoneIndex] : {}}
                    onChange={this.handleChange}
                    selectedDropZoneIndex={this.state.selectedDropZoneIndex}
                    selectedCalRsltIdx={this.state.selectedCalRsltIdx} onSelectedCalRsltIdxChange={this.handleSelectedCalRsltIdxChange} />
                </div>
            </div>
        </div>
        <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} contentLabel="Three Views Modal">
          <div className={style.cad} style={{width:window.innerWidth-100,height:window.innerHeight-100}}>
            <img src={this.state.threeViews} />
          </div>
        </Modal>
      </div>
    )
  }
}
