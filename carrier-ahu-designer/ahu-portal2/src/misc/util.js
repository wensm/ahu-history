/* eslint-disable */
export function bindthis(self, funs) {
  funs.forEach(fun => {
    self[fun] = self[fun].bind(self);
  });
}

export function clone(obj) {
  return JSON.parse(JSON.stringify(obj));
}
