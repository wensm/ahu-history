/* eslint-disable */
import $ from 'jquery';

const http = {

  get(url, data) {
    return this.request(url, data, 'GET');
  },

  post(url, data) {
    return this.request(url, data, 'POST');
  },

  put(url, data) {
    return this.request(url, data, 'PUT');
  },

  delete(url, data) {
    return this.request(url, data, 'DELETE');
  },

  request(url, data, method) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${SERVICE_URL}${url}`,
        method: method,
        data: data ? data : {},
        cache: false,
      }).done(resp => {
        resolve(typeof(resp) === 'string' ? JSON.parse(resp) : resp);
      }).fail(response => {
        reject(typeof(response) === 'string' ? JSON.parse(response) : response);
      }).always(() => {

      });
    });
  },
};

export default http;
