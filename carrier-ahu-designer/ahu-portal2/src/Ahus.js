/* eslint-disable */
import React from 'react';
import { Link } from 'react-router';
import style from './ahus.css';
import img1 from './images/LOGObg.png';
import img2 from './images/person.png';
import img3 from './images/add_icon.png';
import img4 from './images/loding.gif';

export default class Ahus extends React.Component {
  render() {
    return (
<div className={style.ahus}>
      <div className="header clear">
          <div className="header-content">
              <div className="logo">
                  <a href="./projects.html"><img src={img1}/></a>
              </div>
              <div className="logo_righ">
                  <div className="header-info">
                      <ul>
                          <li className="">
                              <a href="./projects.html">
                                  <i className="iconfont icon-svg11"></i>系统
                              </a>
                          </li>
                          <li className="">
                              <a href="javascript:;">
                                  <i className="iconfont icon-gongju"></i>工具
                              </a>
                          </li>
                          <li className="">
                              <a href="javascript:;">
                                  <i className="iconfont icon-tishi1"></i>帮助
                              </a>
                          </li>
                      </ul>
                  </div>
                  <div className="header-login">
                      <div className="seach">
                          <i className="iconfont icon-sousuoicon"></i>
                      </div>
                      <dl>
                          <dt className="fl"><img src={img2}/> </dt>
                          <dd className="fl">
                              <p>Bruce Wang</p>
                              <p>张江高科工厂</p>
                          </dd>
                      </dl>
                      <div className="fr down">
                          <i className="iconfont icon-touxiangzhankaibtn"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div className="main promain clear compro_main">
          <div className="pro_con">
              <ul className="pro_cap clear com_cap">
                  <li>
                      <a href="projects.html">
                          <i className="iconfont icon-fanhui"></i>
                          返回
                      </a>
                  </li>
                  <li className="newgrop_btn">
                      <i className="iconfont icon-xinjiangroup"></i>
                      新建group
                  </li>
                  <li className="newgrop_btn1">
                      <i className="iconfont icon-xinzengAHU"></i>
                      新增AHU
                  </li>
                  <li data-toggle="modal" data-target="#myModal4">
                      <i className="iconfont icon-canshudaoru"></i>
                      参数批量导入
                  </li>
                  <li  data-toggle="modal" data-target="#myModal6">
                      <i className="iconfont icon-baogao"></i>
                      生成报告
                  </li>
              </ul>
              <div className=" modal-group newGroup hide newgrop_cont">
                  <div className="negrp-header">
                      <h4 className="modal-title"><b>新建group</b></h4>
                  </div>
                  <div className="negrp-body clear">
                      <ul className="group_cont">
                          <li className="grpcon_li">
                              <label className="tex_cap">group名称</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li">
                              <label className="tex_cap">group备注</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li">
                              <div className="label_select select_leng fl">
                                  <label className="tex_cap">选择类型</label>
                                  <input type="text" placeholder="选择类型" className="select-ture tex_inp" readOnly />
                                  <i className="icon_down hide"></i>
                                  <ul className="select-list nicescroll2 hide scroll_botm">
                                      <li>空白</li>
                                      <li>自定义</li>
                                      <li>基本型</li>
                                      <li>二管制舒适型</li>
                                      <li>四管制舒适型</li>
                                  </ul>
                              </div>
                              <a href="#" className="zdy_btn">自定义</a>
                          </li>
                      </ul>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close newgrop_close">取消</button>
                      <button type="button" className="btn btn-primary btn_add">创建</button>
                  </div>
              </div>
              <div className=" modal-group newGroup hide newgrop_cont1">
                  <div className="negrp-header">
                      <h4 className="modal-title" ><b>新建AHU</b></h4>
                  </div>
                  <div className="negrp-body clear">
                      <ul className="group_cont">
                          <li className="grpcon_li matbm20">
                              <label className="tex_cap">机组编号</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li matbm20">
                              <label className="tex_cap">图纸编号</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li matbm20">
                              <div className="label_select select_leng fl">
                                  <label className="tex_cap">机组类型</label>
                                  <input type="text" placeholder="机组类型" className="select-ture tex_inp" readOnly />
                                  <i className="icon_down hide"></i>
                                  <ul className="select-list nicescroll2 hide scroll_botm">
                                      <li>空白</li>
                                      <li>自定义</li>
                                      <li>基本型</li>
                                      <li>二管制舒适型</li>
                                      <li>四管制舒适型</li>
                                  </ul>
                              </div>
                          </li>
                          <li  className="grpcon_li matbm20">
                              <label className="tex_cap">机组名称</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li matbm20">
                              <label className="tex_cap">机组数量</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                      </ul>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close newgrop_close1">取消</button>
                      <button type="button" className="btn btn-primary btn_add">保存</button>
                  </div>
              </div>
              <div className="pro_allcont">
                  <div className="pro_cont clear com_cont">
                      <div className="con_cap">
                          <i className="line_left"></i>
                          <ul className="fl conca_fl">
                              <li>未分组</li>
                              <li>AHU</li>
                          </ul>
                          <div className="fr conca_fr down_btn">
                              <div className="iconzk">
                                  <i className="iconfont icon-liebiaozhankai fr"></i>
                              </div>
                          </div>
                      </div>
                      <table className="com_bottom" cellSpacing="0">
                          <thead>
                          <tr>
                              <th>机组编号</th>
                              <th>图纸编号</th>
                              <th>AHU名称</th>
                              <th>机组型号</th>
                              <th>客户名称</th>
                              <th>数量</th>
                              <th>重量</th>
                              <th>价格</th>
                              <th>修改时间</th>
                              <th>状态</th>
                              <th>操作</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td>001</td>
                              <td><b className="cir">1</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  graycol"></em>正在选型</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>002</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">2</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224></td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid blucol"></em>导出完成</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>003</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">3</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  grecol"></em>选型完成</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>004</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">4</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid yelcol"></em>导入完成</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>005</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">5</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  cyacol"></em>完成归档</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <div className="pro_cont clear com_cont">
                      <div className="con_cap">
                          <i className="line_left"></i>
                          <ul className="fl conca_fl">
                              <li>回风工况组</li>
                              <li>二管制舒适型</li>
                          </ul>
                          <div className="fr conca_fr down_btn">
                              <ol className="contabs fl">
                                  <li><a href="#" className="active">用材批量选择</a></li>
                                  <li data-toggle="modal" data-target="#myModal5"><a href="#">批量选型</a></li>
                                  <li data-toggle="modal" data-target="#myModal3"><a href="#">查看组信息</a></li>
                              </ol>
                              <ol className="conchose  fr">
                                  <li className=""><a href="#"><i className="iconfont icon-daochu"></i></a></li>
                                  <li className="" data-toggle="modal" data-target="#myModal4"><a href="#"><i className="iconfont icon-daoru"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-shanchu"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-tianjia"></i></a></li>

                              </ol>
                              <div className="iconzk">
                                  <i className="iconfont icon-liebiaozhankai fr"></i>
                              </div>

                          </div>
                      </div>
                      <table className="com_bottom" cellSpacing="0">
                          <thead>
                          <tr>
                              <th>机组编号</th>
                              <th>图纸编号</th>
                              <th>AHU名称</th>
                              <th>机组型号</th>
                              <th>客户名称</th>
                              <th>数量</th>
                              <th>重量</th>
                              <th>价格</th>
                              <th>修改时间</th>
                              <th>状态</th>
                              <th>操作</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td>006</td>
                              <td><b className="cir">6</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  graycol"></em>正在选型</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>007</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">7</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  graycol"></em>正在选型</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>008</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">8</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  grecol"></em>选型完成</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <div className="pro_cont clear com_cont">
                      <div className="con_cap">
                          <i className="line_left"></i>
                          <ul className="fl conca_fl">
                              <li>新风工况组</li>
                              <li>基础型</li>
                          </ul>
                          <div className="fr conca_fr down_btn">
                              <ol className="contabs fl">
                                  <li><a href="#" className="active">用材批量选择</a></li>
                                  <li data-toggle="modal" data-target="#myModal5"><a href="#">批量选型</a></li>
                                  <li><a href="javascript:;" data-toggle="modal" data-target="#myModal3">查看组信息</a></li>
                              </ol>
                              <ol className="conchose  fr">
                                  <li className=""><a href="#"><i className="iconfont icon-daochu"></i></a></li>
                                  <li className=""  data-toggle="modal" data-target="#myModal4"><a href="#"><i className="iconfont icon-daoru"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-shanchu"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-tianjia"></i></a></li>

                              </ol>
                              <div className="iconzk">
                                  <i className="iconfont icon-liebiaozhankai fr"></i>
                              </div>

                          </div>
                      </div>
                      <table className="com_bottom" cellSpacing="0">
                          <thead>
                          <tr>
                              <th>机组编号</th>
                              <th>图纸编号</th>
                              <th>AHU名称</th>
                              <th>机组型号</th>
                              <th>客户名称</th>
                              <th>数量</th>
                              <th>重量</th>
                              <th>价格</th>
                              <th>修改时间</th>
                              <th>状态</th>
                              <th>操作</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td>009</td>
                              <td><b className="cir">9</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid cyacol"></em>完成归档</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>009</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">9</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid cyacol"></em>完成归档</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <div className="pro_cont clear com_cont">
                      <div className="con_cap">
                          <i className="line_left"></i>
                          <ul className="fl conca_fl">
                              <li>回风工况组</li>
                              <li>二管制舒适型</li>
                          </ul>
                          <div className="fr conca_fr down_btn">
                              <ol className="contabs fl">
                                  <li><a href="#" className="active">用材批量选择</a></li>
                                  <li data-toggle="modal" data-target="#myModal5"><a href="#">批量选型</a></li>
                                  <li><a href="javascript:;" data-toggle="modal" data-target="#myModal3">查看组信息</a></li>
                              </ol>
                              <ol className="conchose  fr">
                                  <li className=""><a href="#"><i className="iconfont icon-daochu"></i></a></li>
                                  <li className=""  data-toggle="modal" data-target="#myModal4"><a href="#"><i className="iconfont icon-daoru"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-shanchu"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-tianjia"></i></a></li>

                              </ol>
                              <div className="iconzk">
                                  <i className="iconfont icon-liebiaozhankai fr"></i>
                              </div>

                          </div>
                      </div>
                      <table className="com_bottom" cellSpacing="0">
                          <thead>
                          <tr>
                              <th>机组编号</th>
                              <th>图纸编号</th>
                              <th>AHU名称</th>
                              <th>机组型号</th>
                              <th>客户名称</th>
                              <th>数量</th>
                              <th>重量</th>
                              <th>价格</th>
                              <th>修改时间</th>
                              <th>状态</th>
                              <th>操作</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td>006</td>
                              <td><b className="cir">6</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  graycol"></em>正在选型</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor" data-toggle="modal" data-target="#myModal8">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>007</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">7</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  graycol"></em>正在选型</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>008</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">8</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid  grecol"></em>选型完成</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <div className="pro_cont clear com_cont">
                      <div className="con_cap">
                          <i className="line_left"></i>
                          <ul className="fl conca_fl">
                              <li>新风工况组</li>
                              <li>基础型</li>
                          </ul>
                          <div className="fr conca_fr down_btn">
                              <ol className="contabs fl">
                                  <li><a href="#" className="active">用材批量选择</a></li>
                                  <li data-toggle="modal" data-target="#myModal5"><a href="#">批量选型</a></li>
                                  <li><a href="javascript:;" data-toggle="modal" data-target="#myModal3">查看组信息</a></li>
                              </ol>
                              <ol className="conchose  fr">
                                  <li className=""><a href="#"><i className="iconfont icon-daochu"></i></a></li>
                                  <li className=""  data-toggle="modal" data-target="#myModal4"><a href="#"><i className="iconfont icon-daoru"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-shanchu"></i></a></li>
                                  <li className=""><a href="#"><i className="iconfont icon-tianjia"></i></a></li>
                              </ol>
                              <div className="iconzk">
                                  <i className="iconfont icon-liebiaozhankai fr"></i>
                              </div>
                          </div>
                      </div>
                      <table className="com_bottom" cellSpacing="0">
                          <thead>
                          <tr>
                              <th>机组编号</th>
                              <th>图纸编号</th>
                              <th>AHU名称</th>
                              <th>机组型号</th>
                              <th>客户名称</th>
                              <th>数量</th>
                              <th>重量</th>
                              <th>价格</th>
                              <th>修改时间</th>
                              <th>状态</th>
                              <th>操作</th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>
                              <td>009</td>
                              <td><b className="cir">9</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid cyacol"></em>完成归档</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          <tr>
                              <td className="add_box">
                                  <em>009</em>
                                  <i className="addicon"><img src={img3}/> </i>
                              </td>
                              <td><b className="cir">9</b></td>
                              <td>AHU1</td>
                              <td>39CQ1317</td>
                              <td>天空</td>
                              <td className="num">1</td>
                              <td>1234kg</td>
                              <td>￥33224</td>
                              <td className="time">
                                  <p>2017-03-02</p>
                                  <p>13:23:34</p>
                              </td>
                              <td><span className="zt"><em className="rid cyacol"></em>完成归档</span></td>
                              <td>
                                  <span className="icon_img hover_icon blucor">
                                      <i className="iconfont icon-category-copy"></i>
                                      <span className="tb_icon" style={{display:'none'}}>
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">详情</em>
                                      </span>
                                  </span>
                                   <span className="icon_img hover_icon grecor">
                                      <i className="iconfont icon-biaopei"></i>
                                      <span className="tb_icon ta_iconbp">
                                           <em className="bg bgwid"></em>
                                           <i></i>
                                           <em className="tex">非标配置</em>
                                      </span>
                                  </span>
                                  <span className="icon_img"><i className="iconfont icon-fuzhi"></i></span>
                                  <span className="icon_img hover_icon redcor">
                                      <i className="iconfont icon-shanchu"></i>
                                       <span className="tb_icon">
                                           <em className="bg"></em>
                                           <i></i>
                                           <em className="tex">删除</em>
                                      </span>
                                  </span>
                                  <span className="icon_img" data-toggle="modal" data-target="#myModa20"><i className="iconfont icon-yizu"></i></span>
                                  <a href="./pro.html">
                                      <span className="icon_img"><i className="iconfont icon-chakan"></i></span>
                                  </a>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
              <div className="more">
                  <span><i className="loding"><img src={img4} /> </i>加载更多</span>
              </div>
          </div>
      </div>
      <div className="footer">
          <div className="fotop">
              <span>隐私条款 | 使用条款 | 网站地图 | 开利公司是联合技术环境、控制与安防的子公司，联合技术环境、控制与安防隶属于联合技术公司</span>
          </div>
          <p>© 开利公司 2017</p>
      </div>
      <div className="modal homModal fade" id="myModal3" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" >
          <div className="modal-dialog modal-group new-group group-mease news_group" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>group信息</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                      <h3 className="time">
                          <span className="star_times">创建时间<em className="star_time">2017-03-01</em></span>
                          <span className="end_times">最后修改时间<em className="end_time">2017-03-01</em><em className="end_send">09:21:23</em></span>
                      </h3>
                      <ul className="group_cont">
                          <li className="grpcon_li">
                              <label className="tex_cap">group名称</label>
                              <input type="text" value="回风工况机组01" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li grpcon_two">
                              <label className="tex_cap">group备注</label>
                              <input type="text" value="" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li">
                              <div className="label_select select_leng fl">
                                  <label className="tex_cap">选择类型</label>
                                  <input type="text" placeholder="选择类型" className="select-ture tex_inp bgposition" readOnly />
                                  <i className="icon_down hide"></i>
                                  <ul className="select-list nicescroll2 hide scroll_botm">
                                      <li>空白</li>
                                      <li>自定义</li>
                                      <li>基本型</li>
                                      <li>二管制舒适型</li>
                                      <li>四管制舒适型</li>
                                  </ul>
                              </div>
                          </li>
                      </ul>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">创建</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal4" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group new-group dr_group" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>参数批量导入</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                      <div className="input_dr">
                          <label>选择文件</label>
                          <input type="text" value="" className="img_tex" />
                          <button type="button">浏览<input type="file" value="浏览" className="none_inpt" /></button>

                      </div>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add" data-dismiss="modal" data-toggle="modal" data-target="#myModal7">打开文件</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal7" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group dr_group dr_exel" role="document">
              <div className="modal-content ">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>参数批量导入</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear  nano contentHolder ps-containe" id="Default">
                     <table className="dr_tables nano-content content">
                         <thead>
                              <tr>
                                  <th rowSpan="2"><a>机组名称</a></th>
                                  <th rowSpan="2"><a>机组名称</a></th>
                                  <th colSpan="2" className="drtab_top"><a>送风机组件</a></th>
                                  <th colSpan="2" className="drtab_top"><a>回风机组件</a></th>
                                  <th colSpan="2" className="drtab_top"><a>夏季回风参数／出风温度</a></th>
                                  <th colSpan="2" className="drtab_top"><a>冬季回风参数／出风温度</a></th>
                                  <th><a>新风要求</a></th>
                                  <th colSpan="3" className="drtab_top"><a>单层过滤器</a></th>
                                  <th colSpan="5" className="drtab_top"><a>综合过滤器</a></th>
                                  <th colSpan="" className="drtab_top"><a>盘管类型</a></th>
                                  <th colSpan="" className="drtab_top"><a>翅片材质</a></th>
                                  <th colSpan="6" className="drtab_top"><a>制冷参数输入</a></th>

                                  <th colSpan="" rowSpan="2" className="drtab_top"><a>水温升</a></th>
                                  <th colSpan="" rowSpan="2" className="drtab_top"><a>风机形式</a></th>
                                  <th colSpan="" rowSpan="2" className="drtab_top"><a>电机品牌</a></th>
                                  <th colSpan="" rowSpan="2" className="drtab_top"><a>电机类型</a></th>
                                  <th colSpan="" rowSpan="2" className="drtab_top"><a>其他压降</a></th>
                              </tr>
                              <tr>
                                  <th className="drtab_botm"><a>风量</a></th>
                                  <th className="drtab_botm"><a>机外静压</a></th>
                                  <th className="drtab_botm"><a>风量</a></th>
                                  <th className="drtab_botm"><a>机外静压</a></th>
                                  <th className="drtab_botm"><a>干球温度</a></th>
                                  <th className="drtab_botm"><a>湿球温度</a></th>
                                  <th className="drtab_botm"><a>干球温度</a></th>
                                  <th className="drtab_botm"><a>湿球温度</a></th>
                                  <th className="drtab_botm"><a>新风比</a></th>
                                  <th className="drtab_botm"><a>过滤器形式</a></th>
                                  <th className="drtab_botm"><a>过滤器选项</a></th>
                                  <th className="drtab_botm"><a>过滤效率</a></th>

                                  <th className="drtab_botm"><a>过滤器形式</a></th>
                                  <th className="drtab_botm"><a>板式过滤选项</a></th>
                                  <th className="drtab_botm"><a>板式过滤效率</a></th>
                                  <th className="drtab_botm"><a>袋式过滤选项</a></th>
                                  <th className="drtab_botm"><a>袋式过滤效率</a></th>

                                  <th className="drtab_botm"><a>冷水盘管</a></th>
                                  <th className="drtab_botm"><a>普通铝翅片</a></th>

                                  <th className="drtab_botm"><a>风量</a></th>
                                  <th className="drtab_botm"><a>进风干球温度</a></th>
                                  <th className="drtab_botm"><a>进风湿球温度</a></th>
                                  <th className="drtab_botm"><a>冷量</a></th>
                                  <th className="drtab_botm"><a>进水温度</a></th>
                                  <th className="drtab_botm"><a>最大水阻</a></th>

                              </tr>
                         </thead>
                         <tbody>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                  <div className="label_select select_leng">
                                      <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly />
                                      <i className="icon_down hide"></i>
                                      <ul className="select-list nicescroll2 hide scroll_botm">
                                          <li>袋式</li>
                                          <li>袋式</li>
                                          <li>袋式</li>
                                          <li>袋式</li>
                                          <li>袋式</li>
                                      </ul>
                                  </div>
                              </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly/>
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                              <tr>
                                  <td> AHU1 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="机组系列" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                              <li>39CQ</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td> 1800 </td>
                                  <td> 600 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td>30 </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                              <li>袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                              <li>过滤器选项</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="过滤器形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                              <li>板式+袋式</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="板式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤选项" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                              <li>无纱布过滤器</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="袋式过滤效率" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                              <li>C1</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="盘管类型" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                              <li>冷水盘管</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="普通铝翅片" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                              <li>普通铝翅片</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      30
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>
                                  <td className="">
                                      50
                                  </td>

                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="风机形式" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="默认" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className="">
                                      <div className="label_select select_leng">
                                          <input type="text" placeholder="二级能效电机" className="select-ture tex_inp" readOnly />
                                          <i className="icon_down hide"></i>
                                          <ul className="select-list nicescroll2 hide scroll_botm">
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯</li>
                                              <li>前弯前弯</li>
                                              <li>前弯</li>
                                          </ul>
                                      </div>
                                  </td>
                                  <td className=""> 50</td>



                              </tr>
                         </tbody>
                     </table>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-edit">编辑</button>
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">确认导入</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal5" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group new-group dr_group wid540" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>批量选型配置</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear checkbox row">
                      <span className="fl check_cap col-md-3"><b>配置规则</b></span>
                      <ul className="check_ul col-md-9">
                          <li className="fl news-list">
                              <label className="alrlist alwidth on_check">
                                  <input type="checkbox" name="newslist" value="1" className="radioclass" />
                              </label>
                              <span>最经济</span>
                          </li>
                          <li  className="fl news-list">
                              <label className="alrlist alwidth ">
                                  <input type="checkbox" name="newslist" value="2" className="radioclass" />
                              </label>
                              <span>性能最优</span>
                          </li>

                      </ul>


                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">快速配置</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal6" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group dr_group repa_group" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>生成报告</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                      <div className="body_top">
                          <div className="input_dr martp30">
                              <label className="fl cap">报告名称</label>
                              <input type="text" value="" className="img_tex fl" />
                          </div>
                          <div className="input_dr martp30">
                              <label className="fl cap">报告路径</label>
                              <input type="text" value="" className="img_tex fl" />
                              <button type="button">浏览<input type="file" value="浏览" className="none_inpt fl" /></button>
                          </div>
                          <div className="input_dr repor_cont">
                              <label className="fl cap">报告内容</label>
                              <ul className="check_box">
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="1" className="radioclass" />
                                      </label>
                                      <span>项目已清单</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="2" className="radioclass" />
                                      </label>
                                      <span>技术说明</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="3" className="radioclass" />
                                      </label>
                                      <span>三视图</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="3" className="radioclass" />
                                      </label>
                                      <span>风机曲线</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="3" className="radioclass" />
                                      </label>
                                      <span>焓湿图</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="3" className="radioclass" />
                                      </label>
                                      <span>主要参数列表</span>
                                  </li>
                                  <li className="news-list">
                                      <label className="alrlist alwidth ">
                                          <input type="checkbox" name="newslist" value="3" className="radioclass" />
                                      </label>
                                      <span>长交货期选项</span>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add" data-dismiss="modal" data-toggle="modal" data-target="#myModal9">确认生成</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal9" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group  repa_group conrepa" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>生成报告</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                     <b className="text">报告已生成完毕，是否前往查看？</b>
                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">前往查看</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModal8" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group new-group group-mease news_group wid700" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>AHU1信息</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                      <h3 className="time">
                          <span className="star_times">创建时间<em className="star_time">2017-03-01</em></span>
                          <span className="end_times">最后修改时间<em className="end_time">2017-03-01</em><em className="end_send">09:21:23</em></span>
                      </h3>
                      <ul className="group_cont">
                          <li className="grpcon_li">
                              <label className="tex_cap">图纸编号</label>
                              <input type="text" value="3" className="tex_inp" />
                          </li>
                          <li  className="grpcon_li grpcon_two">
                              <label className="tex_cap">机组类型</label>
                              <input type="text" value="基本型" className="tex_inp" readOnly/>
                          </li>
                          <li  className="grpcon_li">
                              <div className="label_select select_leng fl">
                                  <label className="tex_cap">机组名称</label>
                                  <input type="text" placeholder="机组名称" className="select-ture tex_inp bgposition" readOnly />
                                  <i className="icon_down hide"></i>
                                  <ul className="select-list nicescroll2 hide scroll_botm">
                                      <li>AHU</li>
                                      <li>AHU</li>
                                      <li>AHU</li>
                                      <li>AHU</li>
                                      <li>AHU</li>
                                  </ul>
                              </div>
                          </li>
                          <li  className="grpcon_li grpcon_two">
                              <label className="tex_cap">数量</label>
                              <input type="text" value="2" className="tex_inp" />
                          </li>
                      </ul>
                  </div>
                  <div className="modal-footer clear">

                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">创建</button>
                  </div>
              </div>
          </div>
      </div>
      <div className="modal homModal fade" id="myModa20" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog modal-group repa_group conrepa" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title"><b>移动AHU</b><span className="iconfont icon-close" data-dismiss="modal"></span></h4>
                  </div>
                  <div className="modal-body clear">
                      <ul className="group_cont">
                          <li className="grpcon_li">
                              <div className="label_select select_leng fl">
                                  <label className="tex_cap">将AHU123移动至</label>
                                  <input type="text" placeholder="" className="select-ture tex_inp bgposition" readOnly />
                                  <i className="icon_down hide"></i>
                                  <ul className="select-list nicescroll2 hide scroll_botm">
                                      <li>空白</li>
                                      <li>自定义</li>
                                      <li>基本型</li>
                                      <li>二管制舒适型</li>
                                      <li>四管制舒适型</li>
                                  </ul>
                              </div>
                          </li>
                      </ul>

                  </div>
                  <div className="modal-footer clear">
                      <button type="button" className="btn btn-default btn-info btn_close" data-dismiss="modal">取消</button>
                      <button type="button" className="btn btn-primary btn_add">确认移动</button>
                  </div>
              </div>
          </div>
      </div>
</div>
    );
  }
}
