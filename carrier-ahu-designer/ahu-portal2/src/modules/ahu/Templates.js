import React from 'react';
import Modal from '../../components/modal';
import http from '../../misc/http';
import style from './templates.css';
import tp1 from '../../images/template/tp1.png';
import tp2 from '../../images/template/tp2.png';
import tp3 from '../../images/template/tp3.png';
import tp4 from '../../images/template/tp4.png';
import tp5 from '../../images/template/tp5.png';
import tp6 from '../../images/template/tp6.png';
import classnames from 'classnames'
const imgs = {
  tp1, tp2, tp3, tp4, tp5, tp6,
}

export default class Templates extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false, templates: [],
    };

    this.afterOpenModal = this.afterOpenModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.getTemplates = this.getTemplates(this)
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }
  
  getTemplates(){
  	http.get('templates').then(data => {
      this.setState({templates: data});
    });
  }

  afterOpenModal() {
    http.get('templates').then(data => {
      this.setState({templates: data});
    });
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  handleClick(id) {
    http.get('ahutemplate/list').then(data => {
      this.props.onSelectAhuTemplate(data.data[0])
    });
    this.closeModal()
  }

  render() {
    return (
      <div>
        {this.state.templates.map((template, index) => <Template key={index} data={template} onClick={this.handleClick} />)}
      </div>
    );
  }
}

const Template = ({data, onClick}) => (
  <div className={classnames(style.templatesList)}>
  	<div><input name="judge-select" id={data.id} value="on" type="checkbox"/>{data.cnName}</div>
  	<img src={imgs[data.id]} />
  </div>
)

//<tr style={{cursor:'pointer'}} onClick={() => onClick(data.id)}>
//  <td style={{verticalAlign:'middle',textAlign:'right',fontSize:'16px'}}>{data.cnName}</td>
//  <td>
//    <img src={imgs[data.id]} />
//  </td>
//</tr>