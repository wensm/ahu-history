/* eslint-disable */
import React from 'react';
import classnames from 'classnames';
import Groups from './modules/Groups';
import Ahu from './modules/Ahu';
import CombinedFilter from './modules/CombinedFilter';
import CoolingCoil from './modules/CoolingCoil';
import Fan from './modules/Fan';
import Filter from './modules/Filter';
import Mix from './modules/Mix';
import $ from 'jquery'

import style from './form.css';

export default class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            layout: true,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
      $('#form').on('click', '[data-collapse]', event => {
        $(event.target).parent().siblings('[data-panel]').toggleClass('hidden')
      })

      $('#form').on('click', '[data-ok]', event => {
        $(event.target).attr('disabled', '')
        $(`[data-index="${this.props.selectedDropZoneIndex}"]`).append('<i class="fa fa-check-circle-o" style="font-size: 20px;color: green;position: absolute;top: 50px;left: 17px;" aria-hidden="true"></i>')
      })
    }

    handleChange(key, value) {
        this.props.onChange(key, value);
    }

    handleClick() {
        this.setState(prevState => {
            return {layout: !prevState.layout};
        });
    }

    render() {
        let section = null;
        switch (this.props.section) {
            case 'ahu':
                this.props.sectionValues.sectionL  = "??";
                section = <Ahu onChange={this.handleChange} values={this.props.sectionValues}
                               calValues={this.props.sectionCalValues}/>;
                break;
            case 'ahu.combinedFilter':
                this.props.sectionValues.sectionL = 11;
                section = <CombinedFilter onChange={this.handleChange} values={this.props.sectionValues}
                                          calValues={this.props.sectionCalValues}/>;
                break;
            case 'ahu.coolingCoil':
                this.props.sectionValues.sectionL = 12;
                section = <CoolingCoil onChange={this.handleChange} values={this.props.sectionValues}
                                       calValues={this.props.sectionCalValues}
                                       selectedCalRsltIdx={this.props.selectedCalRsltIdx}
                                       onSelectedCalRsltIdxChange={this.props.onSelectedCalRsltIdxChange}/>;
                break;
            case 'ahu.fan':
                this.props.sectionValues.sectionL = 13;
                section = <Fan onChange={this.handleChange} values={this.props.sectionValues}
                               calValues={this.props.sectionCalValues}
                               selectedCalRsltIdx={this.props.selectedCalRsltIdx}
                               onSelectedCalRsltIdxChange={this.props.onSelectedCalRsltIdxChange}/>;
                break;
            case 'ahu.filter':
                this.props.sectionValues.sectionL = 14;
                section = <Filter onChange={this.handleChange} values={this.props.sectionValues}
                                  calValues={this.props.sectionCalValues}/>;
                break;
            case 'ahu.mix':
                this.props.sectionValues.sectionL = 15;
                section = <Mix onChange={this.handleChange} values={this.props.sectionValues}
                               calValues={this.props.sectionCalValues}/>;
                break;
            default:

        }
        console.log(this.props.sectionValues)
        return (
            <div className={classnames(style.form, 'clearfix')} id="form">
                <button onClick={this.handleClick}>切换布局</button>
                {this.state.layout ? section :
                    <Groups groups={this.props.groups} sectionValues={this.props.sectionValues}
                            onChange={this.handleChange}/>}
            </div>
        );
    }
}
