/* eslint-disable */
import React from 'react';

import http from '../../misc/http';

import Section from './components/Section';

import style from './sections.css';

export default class Sections extends React.Component {
  render() {
    return (
      <ul className={style.sections}>
        {this.props.data.map((section, index) => <SectionWrap key={index} data={section} onDragStart={this.props.onDragStart} />)}
      </ul>
    );
  }
}

class SectionWrap extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <li>
        <Section title={data.name} section={data.metaId} onDragStart={this.props.onDragStart} />
      </li>
    );
  }
}
