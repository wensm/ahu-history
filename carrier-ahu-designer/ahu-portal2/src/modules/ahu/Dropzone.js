/* eslint-disable */
import React from 'react';
import classnames from 'classnames';
import $ from 'jquery';

import Section from './components/Section';

import style from './dropzone.css';

import img15 from '../../images/comf-gey.png';

export default class Dropzone extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleAhuDrop = this.handleAhuDrop.bind(this);
    this.handleAutoAlign = this.handleAutoAlign.bind(this);
    this.handleClickSeparator = this.handleClickSeparator.bind(this);
  }

  handleClick(position, section) {
    this.props.onClick(position, section);
  }

  handleAhuDrop(event) {
    event.preventDefault();
    const index = $(event.target).attr('data-index');
    $(event.target).removeClass(style.dragover);
    if (index) this.props.onAhuDrop(index);
    $('#indicator').css('left', index * 50 + 38)
  }

  handleDragEnter({target}) {
    if ($(target).attr('data-index')) $(target).addClass(style.dragover);
  }

  handleDragLeave({target}) {
    if ($(target).attr('data-index')) $(target).removeClass(style.dragover);
  }

  handleAutoAlign() {
    this.props.onHandleAutoAlign();
  }

  handleClickSeparator(position) {
    this.props.onClickSeparator(position);
  }

  render() {
    const dropzones = [];
    for (let i = 0; i < this.props.dropzoneNum; i++) {
      const section = this.props.sections.find(section => section.position === i);
      dropzones.push(
        <div key={i} data-index={i} className={style.dropzone} onDrop={this.handleAhuDrop} onDragEnter={this.handleDragEnter} onDragLeave={this.handleDragLeave}>
          {i > 0 && this.props.isSplit && <Separator position={i} isActive={this.props.separator[i]} onClick={this.handleClickSeparator} />}
          {section && <Section section={section.key} position={i} onClick={this.handleClick} onDragStart={this.props.onDragStart} />}
        </div>
      );
    }
    return (
      <div className="decen_btmtcont" style={{padding:'0px'}}>
        <h2 style={{padding:'0px 25px'}}>
          <i className="iconfont icon-tishi tis"></i>可将不用的段拖拽置垃圾桶
          <div className={classnames(style.autoAlign)}>
            <button type="button" className="debtm_btn2" onClick={this.handleAutoAlign}>自动连接</button>
          </div>
          <div className={classnames('deleicon', 'fr', style.trash)} style={{marginTop:'10px'}} onDrop={this.props.onTrashDrop}>
            <i className="iconfont icon-shanchu" onDrop={this.props.onTrashDrop} />
          </div>
        </h2>
        <p className="decen_img"><span><img src={img15}/></span>airflow</p>
        <div className={classnames(style.dropzones, 'clearfix')}>
          <i id="indicator" className={classnames('fa', 'fa-sort-desc', 'fa-5', style.indicator)} aria-hidden="true"></i>
          {dropzones}
        </div>
        <div className="decen_airflo">
          <p className="decen_img rota_decenimg fl"><span><img src={img15}/></span>airflow</p>
          <p className="decen_img rota_decenimg fr"><span><img src={img15}/></span>airflow</p>
        </div>
      </div>
    );
  }
}

class Separator extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.position);
  }

  render() {
    return (
      <div title="点击分段" className={this.props.isActive ? classnames(style.separator, style.active) : style.separator} onClick={this.handleClick}>
        <i className="fa fa-map-pin" aria-hidden="true"></i>
        <div>|</div><div>|</div>
      </div>
    );
  }
}
