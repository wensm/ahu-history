/* eslint-disable */
import React from 'react';
import style from './checkbox.css';

export default class Checkbox extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    // this.props.onChange(this.props.name, target.checked);
  }

  render() {
    const props = this.props;
    return (
      <label className={style.checkbox}>
        <input type="checkbox" name={this.props.name} checked={this.props.value} disabled={props.disabled} onChange={this.handleChange} />
        <span>{props.label}</span>
      </label>
    );
  }
}
