/* eslint-disable */
import React from 'react';
import style from './select.css';

export default class Select extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    // this.props.onChange(this.props.name, target.value);
  }

  render() {
    return (
      <div className={style.select}>
        <label className="overflow-ellipsis" title={this.props.label}>{this.props.label}{this.props.unit && ` (${this.props.unit})`}</label>
        <select name={this.props.name} value={this.props.value} disabled={this.props.disabled} onChange={this.handleChange} >
          {this.props.options && this.props.options.map((option, index) => <option key={index} value={option.option}>{option.clabel}</option>)}
        </select>
      </div>
    );
  }
}
