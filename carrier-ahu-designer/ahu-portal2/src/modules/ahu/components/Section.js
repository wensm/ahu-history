/* eslint-disable */
import React from 'react';
import classnames from 'classnames';

import style from './section.css';

import mix from '../../../images/pro_blu1.png';
import combinedFilter from '../../../images/pro_blu2.png';
import coolingCool from '../../../images/pro_blu3.png';
import filter from '../../../images/pro_blu6.png';
import fan from '../../../images/pro_blu7.png';

const MAP = {
  'ahu.filter': filter,
  'ahu.combinedFilter': combinedFilter,
  'ahu.mix': mix,
  'ahu.fan': fan,
  'ahu.coolingCoil': coolingCool,
};

export default class Section extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleDragStart = this.handleDragStart.bind(this);
  }

  handleDragStart(ev) {
    // ev.dataTransfer.setData('text/plain', null); // Add the target element's id to the data transfer object
    this.props.onDragStart(this.props.section, this.props.position);
  }

  handleClick() {
    if(this.props.onClick) this.props.onClick(this.props.position, this.props.section);
  }

  render() {
    const props = this.props;
    return (
      <img src={MAP[props.section]} title={props.title} draggable="true" onDragStart={this.handleDragStart} onClick={this.handleClick} className={classnames(style.section, 'imgResponsive')} />
    );
  }
}
