/* eslint-disable */
import React from 'react';
import classNames from 'classnames';

import style from './input.css';

export default class Input extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    // this.props.onChange(this.props.name, target.value);
  }

  render() {
    return (
      <div className={style.input}>
        <label className="overflow-ellipsis" title={this.props.label}>{this.props.label}{this.props.unit && ` (${this.props.unit})`}</label>
        {
          this.props.readOnly ?
            <input type="text" name={this.props.name} value={this.props.value ? this.props.value : ''} disabled={this.props.disabled} onChange={this.handleChange} />
              :
            <input type="text" name={this.props.name} value={this.props.value ? this.props.value : ''} disabled onChange={this.handleChange} />
        }
      </div>
    );
  }
}
