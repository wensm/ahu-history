import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';

import style from './fan.css';

export default class Fan extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelectedCalRsltIdxChange = this.handleSelectedCalRsltIdxChange.bind(this);
    }

    handleChange({target}) {
        this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
    }

    handleSelectedCalRsltIdxChange({target}) {
        if (target.checked) {
            this.props.onSelectedCalRsltIdxChange($(target).attr('name'))
        }
    }

    render() {
        const values = this.props.values;
        return (
            <div className={classnames(style.fan, 'clearfix')} onChange={this.handleChange}>
        <button type="button" className="btn btn-primary btn-block" data-ok>完 成</button>
{
this.props.calValues.length &&
<table className="_table" style={{backgroundColor: 'white', marginBottom: '4px'}}>
    <thead>
    <tr>
        <th></th>
        <th>风机型号</th>
        <th>出风口风速 (m/s)</th>
        <th>效率</th>
        <th>吸收功率</th>
        <th>转速</th>
        <th>A声压级噪音</th>
        <th>机座号</th>
        <th>电机功率</th>
        <th>极数</th>
    </tr>
    </thead>
    <tbody>
    {
         this.props.calValues.map((value, index) => (
            <tr key={index}>
                <td>
                    <input type="checkbox" name={index}
                           checked={index === Number(this.props.selectedCalRsltIdx)}
                           onChange={this.handleSelectedCalRsltIdxChange}/>
                </td>
                <td>{value['meta.section.fan.fanModel']}</td>
                <td>{value['meta.section.fan.outletVelocity'].toFixed(2)}</td>
                <td>{value['meta.section.fan.efficiency'].toFixed(2)}</td>
                <td>{value['meta.section.fan.absorbedPower'].toFixed(2)}</td>
                <td>{value['meta.section.fan.maxRPM'].toFixed(2)}</td>
                <td>{value['meta.section.fan.lp'].toFixed(2)}</td>
                <td>{value['']}</td>
                <td>{value['meta.section.fan.motorPower']}</td>
                <td>{value['meta.section.fan.pole']}</td>
            </tr>
        ))
    }
    </tbody>
</table>
}

                <div className={style.west}>
                    <div data-group>
                        <h4 className="overflow-ellipsis">风机选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                          <Element name="meta.section.fan.outletDirection" values={values}/>
                          <Element name="meta.section.fan.outlet" values={values}/>
                          <Element name="meta.section.fan.power" values={values}/>
                        </div>
                    </div>
                    <div data-group>
                        <h4 className="overflow-ellipsis">电机选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <Element name="meta.section.fan.supplier" values={values}/>
                            <Element name="meta.section.fan.type" values={values}/>
                            <Element name="meta.section.fan.pole" values={values}/>
                            <Element name="meta.section.fan.power" values={values}/>
                        </div>
                    </div>
                </div>
                <div className={style.center}>
                    <div data-group>
                        <h4 className="overflow-ellipsis">风机参数<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <Element name="meta.section.fan.airVolume" values={values}/>
                            <Element name="meta.section.fan.sysStatic" values={values}/>
                            <Element name="meta.section.fan.externalStatic" values={values}/>
                            <Element name="meta.section.fan.extraStatic" values={values}/>
                            <Element name="meta.section.fan.totalStatic" values={values}/>
                        </div>
                    </div>
                    <div data-group>
                        <h4 className="overflow-ellipsis">附件<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <div data-panel>
                                <Element name="meta.section.fan.doorOnBothSide" values={values} />
                                <Element name="meta.section.fan.fixRepairLamp" values={values}  />
                                <Element name="meta.section.fan.beltGuard" values={values} disabled={values['meta.section.fan.outlet'] == 'wwk'}/>
                                <Element name="meta.section.fan.seismicPringIsolator" values={values} disabled={values['meta.section.fan.outlet'] == 'wwk'} />
                                <Element name="meta.section.fan.standbyMotor" values={values} />
                                <Element name="meta.section.fan.para13" values={values} />
                            </div>
                        </div>
                    </div>
                    <div data-group>
                        <h4 className="overflow-ellipsis">附件<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <Element name="meta.section.fan.accessDoor" values={values}/>
                            <Element name="meta.section.fan.motorPosition" values={values}/>
                            <Element name="meta.section.fan.sendPosition" values={values}/>
                            <Element name="meta.section.fan.startStyle" values={values}/>
                            <Element name="meta.section.fan.ptc" values={values}/>
                        </div>
                    </div>
                </div>
                <div className={style.west}>
                    <div data-group>
                        <h4 className="overflow-ellipsis">风机性能及重量<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <div data-panel>
                                <Element name="meta.section.fan.fanModel" values={values} />
                                <Element name="meta.section.fan.airVolume" values={values}  />
                                <Element name="meta.section.fan.staticPressure" values={values} />
                                <Element name="meta.section.fan.totalPressure" values={values} />
                                <Element name="meta.section.fan.outletVelocity" values={values} />
                                <Element name="meta.section.fan.efficiency" values={values} />
                                <Element name="meta.section.fan.RPM" values={values} />
                                <Element name="meta.section.fan.absorbedPower" values={values} />
                                <Element name="meta.section.fan.motorPower" values={values} />
                                <Element name="meta.section.fan.maxRPM" values={values} />
                                <Element name="meta.section.fan.maxAbsorbedPower" values={values} />
                                <Element name="meta.section.fan.motorBaseNo" values={values} />
                            </div>
                        </div>
                    </div>
                    <div data-group>
                        <h4 className="overflow-ellipsis">风机曲线<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                        <div data-panel>
                            <div data-panel>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
