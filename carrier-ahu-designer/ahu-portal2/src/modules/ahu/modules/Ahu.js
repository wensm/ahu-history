import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';
import http from '../../../misc/http';
import Modal from '../../../components/modal';
import { getExskinwOptions } from './AhuConstraint'

import style from './ahu.css';

export default class Ahu extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ahuSeries: [], modalIsOpen: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleAhuSerieSelect = this.handleAhuSerieSelect.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    http.get(`ahuseries`).then(data => {
      this.setState({ahuSeries: data});
    });
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  handleChange({target}) {
    this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
  }

  handleClick() {
    this.openModal();
  }

  handleAhuSerieSelect(ahuSerie, checked, param) {
    if (checked) {
      this.props.onChange('meta.ahu.serial', ahuSerie);
      this.props.onChange('meta.ahu.width', param.width);
      this.props.onChange('meta.ahu.height', param.height);
      this.props.onChange('meta.ahu.svelocity', param.svelocity);
    }
  }

  render() {
    const values = this.props.values;
    return (
      <div className={classnames(style.ahu, 'clearfix')} onChange={this.handleChange}>
        <div className={style.west}>
          <div data-group>
            <h4 className="overflow-ellipsis">送风机组<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.sairvolume" values={values} />
              <Element name="meta.ahu.sexternalstatic" values={values} />
              <Element name="meta.ahu.svelocity" values={values} />
              <Element name="meta.ahu.inWindDirection" values={values} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">机组型号<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.product" values={values} />
              <Element name="meta.ahu.serial" values={values} />
              <Element name="meta.ahu.width" values={values} />
              <Element name="meta.ahu.height" values={values} />
            </div>
            <div className={style.smodelMask} onClick={this.handleClick}></div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">回风机组<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.eairvolume" values={values} disabled={true}/>
              <Element name="meta.ahu.eexternalstatic" values={values} disabled={true} />
              <Element name="meta.ahu.evelocity" values={values} disabled={true} />
            </div>
          </div>
        </div>
        <div className={style.center}>
          <div data-group>
            <h4 className="overflow-ellipsis">外面板<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.exskinm" values={values} />
              <Element name="meta.ahu.exskinw" values={values} options={getExskinwOptions(values)} />
              <Element name="meta.ahu.exskincolor" values={values} />
            </div>
          </div>
           <div data-group>
             <h4 className="overflow-ellipsis">内面板<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
              <div data-panel>
                <Element name="meta.ahu.inskinm" values={values} />
                <Element name="meta.ahu.panelinsulation" values={values} />
                <Element name="meta.ahu.inskinw" values={values} />
                <Element name="meta.ahu.inskincolor" values={values} />
              </div>
           </div>
          <div data-group>
            <h4 className="overflow-ellipsis">海拔高度<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.altitude" values={values} />
            </div>
          </div>
        </div>
        <div className={style.east}>
          <div data-group>
            <h4 className="overflow-ellipsis">选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.delivery" values={values} />
              <Element name="meta.ahu.voltage" values={values} />
              <Element name="meta.ahu.baseType" values={values} />
              <Element name="meta.ahu.package" values={values} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">方向<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.ahu.pipeorientation" values={values} />
              <Element name="meta.ahu.doororientation" values={values} />
              <Element name="meta.ahu.ISPRERAIN" values={values} />
            </div>
          </div>
        </div>
        <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} contentLabel="Ahu Series Modal">
           <table className="table" style={{marginBottom:'0px'}}>
             <thead>
               <tr>
                 <th>背景色</th>
                 <th style={{width:'155px'}}>风速 v</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <td>
                  <div className={classnames(style.indicator, style.white)}></div>
                  </td>
                 <td>{`v <= 2.65m/s`}</td>
               </tr>
               <tr>
                 <td>
                   <div className={classnames(style.indicator, style.yellow)}></div>
                 </td>
                 <td>{`2.65m/s < v <= 3.5m/s`}</td>
               </tr>
               <tr>
                 <td>
                  <div className={classnames(style.indicator, style.gray)}></div>
                 </td>
                 <td>{`v > 3.5m/s`}</td>
               </tr>
             </tbody>
           </table>
           <div className={style.ahuSeries} style={{maxHeight:window.innerHeight-300}}>
            <table className="table">
              <thead>
                <tr>
                  <th>选择</th>
                  <th>型号</th>
                  <th>宽度 (mm)</th>
                  <th>高度 (mm)</th>
                  <th>面风速 (m/s)</th>
                </tr>
              </thead>
              <tbody>
                {this.state.ahuSeries.map((ahuSerie, index) => <AhuSerie key={index} data={ahuSerie} sairvolume={values['meta.ahu.sairvolume']} serial={values['meta.ahu.serial']} onAhuSerieSelect={this.handleAhuSerieSelect} />)}
              </tbody>
            </table>
          </div>
        </Modal>
      </div>
    );
  }
}

class AhuSerie extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    const param = {
      width: this.props.data.width,
      height: this.props.data.height,
      svelocity: (this.props.sairvolume / (this.props.data.coilFaceArea * 3600)).toFixed(3),
    };
    this.props.onAhuSerieSelect(this.props.data.ahu, target.checked, param);
  }

  render() {
    const ahuSerie = this.props.data;
    const velocity = this.props.sairvolume / (ahuSerie.coilFaceArea * 3600);
    let className = style.white;
    switch (true) {
      case velocity <= 2.65:
        className = style.white;
        break;
      case velocity > 3.5:
        className = style.gray;
        break;
      default:
        className = style.yellow;
    }
    return (
      <tr className={className}>
        <td>
          <input type="checkbox" checked={this.props.serial === ahuSerie.ahu ? true : false} onChange={this.handleChange} />
        </td>
        <td>{ahuSerie.ahu}</td>
        <td>{ahuSerie.width}</td>
        <td>{ahuSerie.height}</td>
        <td>{velocity.toFixed(3)}</td>
      </tr>
    );
  }
}
