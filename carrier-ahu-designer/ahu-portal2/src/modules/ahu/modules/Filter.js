import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';

import style from './filter.css';

export default class Filter extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  // handleChange(name, value) {
  //   this.props.onChange(name, value);
  // }

  handleChange({target}) {
    this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
  }

  render() {
    const values = this.props.values;
    return (
      <div className={classnames(style.filter, 'clearfix')} onChange={this.handleChange}>
        <button type="button" className="btn btn-primary btn-block" data-ok>完 成</button>
        <div className={style.west}>
          <div data-group>
            <h4 className="overflow-ellipsis">参数输入<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.section.filter.fitetF" values={values} />
              <Element name="meta.section.filter.fitlerStandard" values={values} />
              <Element name="meta.section.filter.filterOptions" values={values} />
              <Element name="meta.section.filter.filterEfficiency" values={values} />
              <Element name="meta.section.filter.rimThickness" values={values} />
              <Element name="meta.section.filter.mediaLoading" values={values} />
            </div>
          </div>
        </div>
        <div className={style.center}>
          <div data-group>
            <h4 className="overflow-ellipsis">选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.section.filter.bracketMeterial" values={values} />
              <Element name="meta.section.filter.PlankM" values={values} />
              <Element name="meta.section.filter.pressureGuage" values={values} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">阻力<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.section.filter.everagePD" values={values} />
              <Element name="meta.section.filter.finalPD" values={values} />
              <Element name="meta.section.filter.Resistance" values={values} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">重量<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.section.filter.Weight" values={values} />
            </div>
          </div>
        </div>
        <div className={style.east}>
          <div data-group>
            <h4 className="overflow-ellipsis">阻力计算<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element name="meta.section.filter.everagePDN" values={values} />
              <Element name="meta.section.filter.finalPDN" values={values} />
              <Element name="meta.section.filter.Resistance" values={values} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
