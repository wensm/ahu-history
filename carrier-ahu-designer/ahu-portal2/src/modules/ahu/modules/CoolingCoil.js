import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import style from './coolingCoil.css';

export default class CoolingCoil extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapse: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSelectedCalRsltIdxChange = this.handleSelectedCalRsltIdxChange.bind(this);
    }

    handleChange({target}) {
        this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
    }

    handleSelectedCalRsltIdxChange({target}) {
        if (target.checked) {
            this.props.onSelectedCalRsltIdxChange($(target).attr('name'))
        }
    }

    handleClick() {
        this.setState(prevState => {
            return {collapse: !prevState.collapse}
        })
    }

    render() {
        const values = this.props.values;
        return (
            <div>
        <button type="button" className="btn btn-primary btn-block" data-ok>完 成</button>
                {
                    this.props.calValues.length > 0 &&
                    <div className={style.calResult}>
                        <div className={style.collapse} onClick={this.handleClick}>
                            <i className="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>
                        <div className={style.calResultTable}>
                            {
                                !this.state.collapse ?
                                    <h4 className="overflow-ellipsis" style={{width: '1200px'}}>计算结果</h4>
                                    :
                                    <h4 className="overflow-ellipsis">计算结果</h4>
                            }
                            {
                                !this.state.collapse &&
                                <table className="_table" style={{width: '1200px'}}>
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th colSpan="5">盘管信息</th>
                                        <th colSpan="3">进风</th>
                                        <th colSpan="3">出风</th>
                                        <th colSpan="1">风侧</th>
                                        <th colSpan="5">水侧</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td>排数</td>
                                        <td>片距</td>
                                        <td>回路</td>
                                        <td>迎风面积</td>
                                        <td>面风速</td>
                                        <td>干球温度</td>
                                        <td>湿球温度</td>
                                        <td>相对湿度</td>
                                        <td>干球温度</td>
                                        <td>湿球温度</td>
                                        <td>相对湿度</td>
                                        <td>空气阻力</td>
                                        <td>水阻力</td>
                                        <td>水流量</td>
                                        <td>进水温度</td>
                                        <td>水温升</td>
                                        <td>介质流速</td>
                                    </tr>
                                    {
                                        this.props.calValues.length && this.props.calValues.map((calValue, index) => (
                                            <tr key={index}>
                                                <td>
                                                    <input type="checkbox" name={index}
                                                           checked={index === Number(this.props.selectedCalRsltIdx)}
                                                           onChange={this.handleSelectedCalRsltIdxChange}/>
                                                </td>
                                                <td>{calValue['meta.section.coolingCoil.rows']}</td>
                                                <td>{calValue['meta.section.coolingCoil.finDensity']}</td>
                                                <td>{calValue['meta.section.coolingCoil.circuit']}</td>
                                                <td>{calValue['meta.section.coolingCoil.area'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.sensibleCapacity'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SInDryBulbT']}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SInWetBulb']}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SInRelativeT']}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SOutDryBulbT'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SOutWebBulbT'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SOutRelativeT']}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.SAirResistance'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.fluidvelocity']}</td>
                                                <td>{calValue['meta.section.coolingCoil.totalCapacity'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.inFlowT'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.WTAScend'].toFixed(3)}</td>
                                                <td>{calValue['meta.section.coolingCoil.summer.waterFlow'].toFixed(3)}</td>
                                            </tr>
                                        ))
                                    }
                                    </tbody>
                                </table>
                            }
                        </div>
                    </div>
                }
                <div className={classnames(style.coolingCoil, 'clearfix')} onChange={this.handleChange}>
                    <div className={style.west}>
                        <div data-group>
                            <h4 className="overflow-ellipsis">盘管信息<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                            <div data-panel>
                              <Element name="meta.section.coolingCoil.coilType" values={values}/>
                              <Element name="meta.section.coolingCoil.tubeDiameter" values={values}/>
                              <Element name="meta.section.coolingCoil.rows" values={values}/>
                              <Element name="meta.section.coolingCoil.circuit" values={values}/>
                              <Element name="meta.section.coolingCoil.finDensity" values={values}/>
                              <Element name="meta.section.coolingCoil.finType" values={values}/>
                            </div>
                        </div>
                        <div data-group>
                            <h4 className="overflow-ellipsis">重量及段长<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                            <div data-panel>
                              <Element name="meta.section.coolingCoil.driftEliminatorResistance" values={values}/>
                              <Element name="meta.section.coolingCoil.Weight" values={values}/>
                              <Element name="meta.section.coolingCoil.runningWeight" values={values}/>
                              <Element name="meta.section.coolingCoil.sectionL" values={values}/>
                            </div>
                        </div>
                    </div>
                    <div className={style.center}>
                        <div data-group>
                            <h4 className="overflow-ellipsis">盘管选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                            <div data-panel>
                              <Element name="meta.section.coolingCoil.connections" values={values}/>
                              <Element name="meta.section.coolingCoil.eliminator" values={values}/>
                              <Element name="meta.section.coolingCoil.waterCollection" values={values}/>
                              <Element name="meta.section.coolingCoil.waterCollection" values={values}/>
                              <Element name="meta.section.coolingCoil.drainpanType" values={values}/>
                              <Element name="meta.section.coolingCoil.drainpanMaterial" values={values}/>
                              <Element name="meta.section.coolingCoil.baffleMaterial" values={values}/>
                              <Element name="meta.section.coolingCoil.coilFrameMaterial" values={values}/>
                              <Element name="meta.section.coolingCoil.uTrap" values={values}/>
                            </div>
                        </div>
                    </div>
                    <div className={style.west}>


                        <Tabs>
                          <TabList>
                            <Tab>夏季</Tab>
                            <Tab>冬季</Tab>
                          </TabList>
                          <TabPanel>
                             <div data-group>
                                 <h4 className="overflow-ellipsis">进风温度 (夏季)<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                 <div data-panel>
                                   <Element name="meta.section.coolingCoil.summer.SInDryBulbT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.SInWetBulbT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.SInRelativeT" values={values}/>
                                 </div>
                             </div>
                             <div data-group>
                                 <h4 className="overflow-ellipsis">出风参数 (夏季)<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                 <div data-panel>
                                   <Element name="meta.section.coolingCoil.summer.SOutDryBulbT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.SOutWetBulbT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.SOutRelativeT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.SAirResistance" values={values}/>
                                 </div>
                             </div>
                             <div data-group>
                                 <h4 className="overflow-ellipsis">水侧参数 (夏季)<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                 <div data-panel>
                                   <Element name="meta.section.coolingCoil.summer.coldQ" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.inFlowT" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.WTAScend" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.waterResistance" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.waterFlow" values={values}/>
                                   <Element name="meta.section.coolingCoil.summer.fluidVelocity" values={values}/>
                                 </div>
                             </div>
                          </TabPanel>
                          <TabPanel>
                            <div data-group>
                                <h4 className="overflow-ellipsis">进风温度 (冬季) <i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                <div data-panel>
                                  <Element name="meta.section.coolingCoil.winter.WInDryBulbT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WInWetBulbT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WInRelativeT" values={values}/>
                                </div>
                            </div>
                            <div data-group>
                                <h4 className="overflow-ellipsis">出风参数 (冬季)<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                <div data-panel>
                                  <Element name="meta.section.coolingCoil.winter.WOutDryBulbT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WOutWetBulbT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WOutRelativeT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WAirResistance" values={values}/>
                                </div>
                            </div>
                            <div data-group>
                                <h4 className="overflow-ellipsis">水侧参数 (冬季)<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                                <div data-panel>
                                  <Element name="meta.section.coolingCoil.winter.coldQ" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.inFlowT" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.WTAScend" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.waterResistance" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.waterFlow" values={values}/>
                                  <Element name="meta.section.coolingCoil.winter.fluidVelocity" values={values}/>
                                </div>
                            </div>
                          </TabPanel>
                        </Tabs>
                    </div>
                </div>
            </div>
        );
    }
}
