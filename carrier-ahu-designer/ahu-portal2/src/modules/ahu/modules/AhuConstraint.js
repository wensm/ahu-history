import store from './store'

export function getExskinwOptions(values) {
	let newOptions = []
	const options = store.layout['meta.ahu.exskinw'].option.optionPairs
	switch (values['meta.ahu.exskinm']) {
		case 'prePaintedSteal':
			newOptions.push(options[1])
			newOptions.push(options[3])
			break
		case 'tainless':
			newOptions.push(options[0])
			newOptions.push(options[2])
			break
		default:
			newOptions = options
	}
	return newOptions
}
