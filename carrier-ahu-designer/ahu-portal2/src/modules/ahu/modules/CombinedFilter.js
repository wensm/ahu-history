import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';

import style from './combinedFilter.css';

export default class CombinedFilter extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
  }

  render() {
    const values = this.props.values;
    return (
      <div className={classnames(style.combinedFilter, 'clearfix')} onChange={this.handleChange}>
        <button type="button" className="btn btn-primary btn-block" data-ok>完 成</button>
          <div className={style.west}>
              <div data-group>
                  <h4 className="overflow-ellipsis">参数输入<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.filterF" values={values} />
                    <Element name="meta.section.combinedFilter.fitlerStandard" values={values} />
                    <Element name="meta.section.combinedFilter.rimThick" values={values} />
                    <Element name="meta.section.combinedFilter.mediaLoading" values={values} />
                  </div>
              </div>
              <div data-group>
                  <h4 className="overflow-ellipsis">板式<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.LMaterial" values={values} />
                    <Element name="meta.section.combinedFilter.LMaterialE" values={values} />
                  </div>
              </div>
              <div data-group>
                  <h4 className="overflow-ellipsis">板式阻力<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.initialPDP" values={values} />
                    <Element name="meta.section.combinedFilter.everagePDP" values={values} />
                    <Element name="meta.section.combinedFilter.finalPDP" values={values} />
                  </div>
              </div>
              <div data-group>
                  <h4 className="overflow-ellipsis">重量<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                      <Element name="meta.section.combinedFilter.Weight" values={values} />
                  </div>
              </div>
          </div>
          <div className={style.center}>
              <div data-group>
                  <h4 className="overflow-ellipsis">选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.PlankM" values={values} />
                    <Element name="meta.section.combinedFilter.bracketMeterial" values={values} />
                    <Element name="meta.section.combinedFilter.pressureGuageP" values={values} />
                    <Element name="meta.section.combinedFilter.pressureGuageB" values={values} />
                  </div>
              </div>
              <div data-group>
                  <h4 className="overflow-ellipsis">袋式<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.RMaterial" values={values} />
                    <Element name="meta.section.combinedFilter.RMaterialE" values={values} />
                    <Element name="meta.section.combinedFilter.sectionL" values={values} />
                  </div>
              </div>
              <div data-group>
                  <h4 className="overflow-ellipsis">袋式阻力<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.initialPDB" values={values} />
                    <Element name="meta.section.combinedFilter.everagePDB" values={values} />
                    <Element name="meta.section.combinedFilter.finalPDB" values={values} />
                  </div>
              </div>
          </div>
          <div className={style.east}>
              <div data-group>
                  <h4 className="overflow-ellipsis">过滤器布置<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                  <div data-panel>
                    <Element name="meta.section.combinedFilter.rimThick" values={values} />
                    <Element name="meta.section.combinedFilter.mediaLoading" values={values} />
                  </div>
              </div>
          </div>
      </div>
    );
  }
}
