/* eslint-disable */
import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import style from './groups.css';
import $ from 'jquery';

export default class Groups extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(key, value) {
    // this.props.onChange(key, value);
  }

  handleChange({target}) {
    this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
  }

  render() {
    return (
      <form className={classnames(style.groups, 'clearfix')} onChange={this.handleChange}>
        {this.props.groups.map((group, index) => <Group key={index} group={group} sectionValues={this.props.sectionValues} onChange={this.handleChange} />)}
      </form>
    );
  }
}

class Group extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(key, value) {
    this.props.onChange(key, value);
  }

  render() {
    const group = this.props.group;
    return (
      <div className={classnames(style.group, 'clearfix')}>
        <h4 className="overflow-ellipsis" title={group.cname}>{group.cname}</h4>
        {group.paras.map((param, index) => <Element key={index} name={param.key} values={this.props.sectionValues} onChange={this.handleChange} />)}
      </div>
    );
  }
}
