import React from 'react';
import classnames from 'classnames';
import Element from './Element';
import $ from 'jquery';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import style from './mix.css';

export default class Mix extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    this.props.onChange($(target).attr('name'), target.type === 'checkbox' ? target.checked : target.value);
  }

  getCheckedNum() {
    let i = 0
    const values = this.props.values
    if (values['meta.section.mix.returnTop']) i++
    if (values['meta.section.mix.returnBack']) i++
    if (values['meta.section.mix.returnLeft']) i++
    if (values['meta.section.mix.returnRight']) i++
    if (values['meta.section.mix.returnButtom']) i++
    return i
  }

  render() {
    const values = this.props.values;
    return (
      <div className={classnames(style.mix, 'clearfix')} onChange={this.handleChange}>
        <button type="button" className="btn btn-primary btn-block" data-ok>完 成</button>
        <div className={style.east}>
          <div data-group>
            <h4 className="overflow-ellipsis">混合形式<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element label="顶部回风" name="meta.section.mix.returnTop" values={values} disabled={!values['meta.section.mix.returnTop'] && this.getCheckedNum() > 1} />
              <Element label="后回风"  name="meta.section.mix.returnBack" values={values} disabled={!values['meta.section.mix.returnBack'] && this.getCheckedNum() > 1} />
              <Element label="左侧回风" name="meta.section.mix.returnLeft" values={values} disabled={!values['meta.section.mix.returnLeft'] && this.getCheckedNum() > 1} />
              <Element label="右侧回风" name="meta.section.mix.returnRight" values={values} disabled={!values['meta.section.mix.returnRight'] && this.getCheckedNum() > 1} />
              <Element label="底部回风" name="meta.section.mix.returnButtom" values={values} disabled={!values['meta.section.mix.returnButtom'] && this.getCheckedNum() > 1} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">新风要求<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element label="新风比" name="meta.section.mix.NARatio" values={values} disabled={this.getCheckedNum() != 2} />
              <Element label="新风量" name="meta.section.mix.NAVolume" values={values} disabled={this.getCheckedNum() != 2} />
            </div>
          </div>
          <div data-group>
            <h4 className="overflow-ellipsis">风阀选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
            <div data-panel>
              <Element label="风口接件" name="meta.section.mix.damperOutlet" values={values}  />
              <Element label="风阀材料" name="meta.section.mix.damperMeterial" values={values} />
              <Element label="执行器" name="meta.section.mix.damperExecutor" values={values} />
              <Element label="风口阻力" name="meta.section.mix.outletPressure" values={values}  />
            </div>
          </div>
        </div>
        <div className={style.center}>
           <div data-group>
             <h4 className="overflow-ellipsis">其他选项<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
             <div data-panel>
               <Element label="开门情况" name="meta.section.mix.DoorO" values={values} />
               <Element label="开门方向" name="meta.section.mix.DoorDirection" values={values} />
               <Element label="安装检修灯" name="meta.section.mix.fixRepairLamp" values={values} />
               <Element label="安装杀菌灯" name="meta.section.mix.uvLamp" values={values} />
             </div>
           </div>
           <div data-group>
             <h4 className="overflow-ellipsis">阻力及重量<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
             <div data-panel>
               <Element label="阻力" name="meta.section.mix.Resistance" values={values} />
               <Element label="重量" name="meta.section.mix.Weight" values={values}  />
             </div>
           </div>
        </div>
        <div className={style.west}>
          <Tabs>
            <TabList>
              <Tab>夏季</Tab>
              <Tab>冬季</Tab>
            </TabList>
            <TabPanel>
              <div data-group>
                <h4 className="overflow-ellipsis">回风参数[夏季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.SInDryBulbT" values={values} />
                  <Element label="湿球温度" name="meta.section.mix.SInWetBulbT" values={values} />
                  <Element label="相对湿度" name="meta.section.mix.SInRelativeT" values={values} />
                </div>
              </div>
              <div data-group>
                <h4 className="overflow-ellipsis">新风参数[夏季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.SNewDryBulbT" values={values} disabled={this.getCheckedNum() != 2}  />
                  <Element label="湿球温度" name="meta.section.mix.SNewWetBulbT" values={values} disabled={this.getCheckedNum() != 2}  />
                  <Element label="相对湿度" name="meta.section.mix.SNewRelativeT" values={values} disabled={this.getCheckedNum() != 2} />
                </div>
              </div>
              <div data-group>
                <h4 className="overflow-ellipsis">出风参数[夏季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.SOutDryBulbT" values={values}  />
                  <Element label="湿球温度" name="meta.section.mix.SOutWetBulbT" values={values}  />
                  <Element label="相对湿度" name="meta.section.mix.SOutRelativeT" values={values} />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div data-group>
                <h4 className="overflow-ellipsis">回风参数[冬季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.WInDryBulbT" values={values}  />
                  <Element label="湿球温度" name="meta.section.mix.WInWetBulbT" values={values}  />
                  <Element label="相对湿度" name="meta.section.mix.WInRelativeT" values={values} />
                </div>
              </div>
              <div data-group>
                <h4 className="overflow-ellipsis">新风参数[冬季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.WNewDryBulbT" values={values} disabled={this.getCheckedNum() != 2}  />
                  <Element label="湿球温度" name="meta.section.mix.WNewWetBulbT" values={values} disabled={this.getCheckedNum() != 2}  />
                  <Element label="相对湿度" name="meta.section.mix.WNewRelativeT" values={values} disabled={this.getCheckedNum() != 2} />
                </div>
              </div>
              <div data-group>
                <h4 className="overflow-ellipsis">出风参数[冬季]<i data-collapse className={classnames('fa', 'fa-sort-desc')} aria-hidden="true"></i></h4>
                <div data-panel>
                  <Element label="干球温度" name="meta.section.mix.WOutDryBulbT" values={values}  />
                  <Element label="湿球温度" name="meta.section.mix.WOutWetBulbT" values={values}  />
                  <Element label="相对湿度" name="meta.section.mix.WOutRelativeT" values={values} />
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    )
  }
}
