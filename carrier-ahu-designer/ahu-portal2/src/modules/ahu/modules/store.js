const store = {

  layout: {},

  setLayout(layout) {
    for (const variable in layout) {
      if (layout.hasOwnProperty(variable)) {
        layout[variable].groups.forEach(group => {
          group.paras.forEach(para => {
            this.layout[para.key] = para;
          })
        });
      }
    }
    console.log(this.layout);
  },

};

export default store;
