/* eslint-disable */
import React from 'react';
import Input from '../components/Input';
import Select from '../components/Select';
import Checkbox from '../components/Checkbox';
import store from './store';

const TYPE = {
  input: 'INPUT', select: 'SELECT', checkbox: 'CHECKBOX',
};

export default class Element extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(name, value) {
    this.props.onChange(name, value);
  }

  getType() {
    const prop = store.layout[this.props.name];
    if (!prop) console.error(`${this.props.name} not found in layout`);
    if (prop.valueType === 'BooleanV') return TYPE.checkbox;
    if (prop.option && prop.option.optionPairs.length >= 1) return TYPE.select;
    return TYPE.input;
  }

  render() {
    const props = this.props;
    const prop = store.layout[props.name];

    let key = ''
    let options = []
    if (prop.option) {
      prop.option.relatedKeys.forEach((relatedKey, index) => {
        key += this.props.values[relatedKey]
        if (index < prop.option.relatedKeys.length - 1) {
          key += '#'
        }
      })
      if (key !== '' && prop.option.valueOptionMap[key] && prop.option.valueOptionMap[key].length) {
        prop.option.valueOptionMap[key].forEach(idx => {
          options.push(prop.option.optionPairs[idx])
        })
      }
    }

    let isEnable = true
    if (prop.option && prop.option.enableExprs && prop.option.enableExprs.length) {
      prop.option.enableExprs.forEach(expr => {
        const arr = expr.split(':')
        const key = arr[0]
        let isEqual = true
        let value = ''
        if (arr[1].includes('!')) {
          isEqual = false
          value = arr[1].replace('!', '')
        } else {
          value = arr[1]
        }
        if (isEqual) {
          if (this.props.values[key] !== value) {
            isEnable = false
          }
        } else {
          if (this.props.values[key] === value) {
            isEnable = false
          }
        }
      })
    }

    let elem = <div>unknow type</div>;
    switch (this.getType()) {
      case TYPE.input:
        elem = <Input label={prop.memo} name={props.name} value={this.props.values[props.name]} unit={prop.unitMetricString} readOnly={prop.editable} disabled={props.disabled || !isEnable} onChange={this.handleChange} />;
        break;
      case TYPE.select:
        elem = <Select label={prop.memo} name={props.name} value={this.props.values[props.name]} unit={prop.unitMetricString} options={options.length ? options : prop.option.optionPairs} disabled={!isEnable} onChange={this.handleChange} />;
        break;
      case TYPE.checkbox:
        elem = <Checkbox label={prop.memo} name={props.name} value={this.props.values[props.name]} disabled={props.disabled || !isEnable} onChange={this.handleChange} />;
        break;
      default:
        elem = <Input label={prop.memo} name={props.name} value={this.props.values[props.name]} unit={prop.unitMetricString} readOnly={prop.editable} disabled={props.disabled || !isEnable} onChange={this.handleChange} />;
    }
    return elem;
  }
}
