/* eslint-disable */
import React from 'react';

import style from './detail.css';

import img12 from '../../images/comf.png';
import img13 from '../../images/icon1.png';
import img14 from '../../images/icon2.png';

export default class Detail extends React.Component {
    render() {
        console.log(this.props)
        return (
            <ul className={style.detail}>
                <li>
                    <table className="_table">
                        <thead>
                        <tr>
                            <th>段信息</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>部件名称</td>
                            <td>{this.props.data && this.props.data.cName}</td>
                        </tr>
                        <tr>
                            <td>段长</td>
                            <td>{ this.props.sectionValues && this.props.sectionValues.sectionL }</td>
                        </tr>
                        <tr>
                            <td>阻力</td>
                            <td>{ this.props.sectionValues && this.props.sectionValues.sectionL }</td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table className="_table">
                        <tbody>
                        <tr>
                            <td>干球(新风)</td>
                            <td>30.0℃</td>
                        </tr>
                        <tr>
                            <td>湿球(新风)</td>
                            <td>27.0℃</td>
                        </tr>
                        <tr>
                            <td>干球(回风)</td>
                            <td>30.0℃</td>
                        </tr>
                        <tr>
                            <td>湿球(回风)</td>
                            <td>27.0℃</td>
                        </tr>
                        <tr>
                            <td>干球(出风)</td>
                            <td>30.0℃</td>
                        </tr>
                        <tr>
                            <td>湿球(出风)</td>
                            <td>27.0℃</td>
                        </tr>
                        </tbody>
                    </table>
                </li>
                <li>
                    <table className="_table">
                        <thead>
                        <tr>
                            <th>机组</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td className="oldgeycol">机组型号</td>
                            <td><b>{this.props.ahuValues && this.props.ahuValues['meta.ahu.serial']}</b></td>
                        </tr>
                        <tr>
                            <td className="oldgeycol">风量</td>
                            <td><b>{this.props.ahuValues && this.props.ahuValues['meta.ahu.sairvolume']}</b><em
                                className="geycol">m3/h</em></td>
                        </tr>
                        <tr>
                            <td className="oldgeycol">机外静压</td>
                            <td><b>{this.props.ahuValues && this.props.ahuValues['meta.ahu.sexternalstatic']}</b><em
                                className="geycol">m3/h</em></td>
                        </tr>
                        </tbody>
                    </table>
                </li>
            </ul>
        );
    }
}
