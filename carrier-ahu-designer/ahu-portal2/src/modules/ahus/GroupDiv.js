import React from 'react';
import style from './groupDiv.css';
import style1 from '../../components/SetDirectionDiv.css';
import AhuDiv from '../../components/AhuDiv';
import Modal from '../../components/Modal';
import AhuInit from '../../components/AhuInit';
import classnames from 'classnames'
export default class GroupDiv extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            collapse: false,
        }
        this.handleDivided = this.handleDivided.bind(this);
        this.divide = this.divide.bind(this);
        this.ahuNum = this.props.data.ahus.length;
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this);
        this.opennaModal = this.opennaModal.bind(this);
        this.openBatchModal = this.openBatchModal.bind(this);
        this.selGroup = this.selGroup.bind(this);
        console.log("*****", this.props);
    }

    openBatchModal(groupId) {
        this.props.openBatchModal(1,groupId);
    }

    opennaModal(groupId,groupType) {
        this.props.opennaModal(groupId,groupType);
    }

    openModal(groupId) {
        if (groupId) {
            this.setState({modalIsOpen: true})
        }
    }

    selGroup(event) {

    }

    afterOpenModal() {

    }

    closeModal() {
        this.setState({modalIsOpen: false})
    }

    handleDivided() {
        var ahuGroup = this.props.data;
        var ahus = ahuGroup.ahus;
        var ahuIds = [];
        for (var i = 0; i < ahus.length; i++) {
            ahuIds.push(ahus[i].unitid);
        }
        this.props.openngModal(ahuIds, ahuGroup.groupId, ahuGroup.ahuGroupCode);
    }

    divide(ahuIds, ahuGroupId, ahuGroupCode) {
        this.props.openngModal(ahuIds, ahuGroupId, ahuGroupCode);
    }

    handleClick() {
        this.setState(prevState => {
            return {collapse: !prevState.collapse}
        })
    }

    render() {
        const ahuGroup = this.props.data;
        const ahus = ahuGroup.ahus;
        const ahuFirst = ahus[0];
        return (
            <div className={classnames(style.groupDiv, style1.groupDiv, 'pro_cont clear com_cont')}>

                <div className="con_cap">
                    <i className="line_left"></i>
                    <ul className="fl conca_fl">
                        <li>{this.props.groupName}</li>
                        <li>AHU</li>
                    </ul>
                    <div className="fr conca_fr down_btn">
                        {this.props.flg == 2 && <ol className="contabs fl">
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li><a className="active" data-toggle="modal" data-target="#myModal6"
                                            onClick={() => this.props.openMyModal6(ahuGroup.groupId)}>参数批量设置</a></li>)
                            }
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li data-toggle="modal"
                                         onClick={() => this.props.openMyModal5(ahuGroup.groupId, ahuFirst.unitid)}
                                         data-target="#myModal5">批量选型</li>)
                            }
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li data-toggle="modal" onClick={() => this.props.openMyModal7(ahuGroup.groupId)}
                                         data-target="#myModal7">查看组信息</li>)
                            }
                            {
                                (ahuGroup.groupId != null) ? null :
                                    (<li><i className="iconfont icon-daoru" style={{marginRight: '5px'}}></i><a
                                        onClick={() => this.openBatchModal(ahuGroup.groupId)}>参数批量导入</a></li>)

                            }

                        </ol>}
                        {this.props.flg == 2 && <ol className="conchose  fr">
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li data-toggle="modal"><i
                                        className="iconfont icon-daochu" style={{cursor: 'pointer'}}></i></li>)
                            }
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li><i className="iconfont icon-daoru" style={{cursor: 'pointer'}}></i></li>)
                            }
                            {
                                (ahuGroup.groupId == null) ? null :
                                    (<li onClick={() => this.props.delGroup(ahuGroup.groupId)}><i
                                        className="iconfont icon-shanchu" style={{cursor: 'pointer'}}></i></li>)
                            }
                            <li onClick={() => this.opennaModal(ahuGroup.groupId,ahuGroup.groupType)}><a><i className="iconfont icon-tianjia"
                                                                 style={{cursor: 'pointer'}}></i></a></li>

                        </ol>}
                        {!this.props.flg == 2 &&
                        <ol className="fr" style={{marginRight: '40px', marginTop: '20px'}}>
                            <li><input onClick={this.selGroup} type="checkbox"
                                       style={{verticalAlign: 'middle', marginTop: '-2px', marginRight: '3px'}}/>选择整租
                            </li>
                        </ol>
                        }
                        <div className="iconzk" onClick={() => this.handleClick()}>
                            <i className="iconfont icon-liebiaozhankai"></i>
                        </div>
                    </div>
                </div>
                {
                    !this.state.collapse &&
                    <table className="com_bottom" cellSpacing="0">
                        <thead>
                        <tr>
                            {this.props.flg == 1 && <th>选择</th>}
                            {this.props.flg == 3 && <th>选择</th>}
                            <th>机组编号</th>
                            <th>图纸编号</th>
                            <th>AHU名称</th>
                            <th>机组型号</th>
                            <th>数量</th>
                            {this.props.flg == 1 && <th>接管方向</th>}
                            {this.props.flg == 1 && <th>修建门方向</th>}
                            {this.props.flg == 2 && <th>重量</th>}
                            {this.props.flg == 2 && <th>价格</th>}
                            {this.props.flg == 2 && <th>修改时间</th>}
                            {this.props.flg == 2 && <th>状态</th>}
                            {this.props.flg == 2 && <th>操作</th>}
                        </tr>
                        </thead>
                        <tbody>
                        {ahus.map((ahu, index) => <AhuDiv key={index} divide={this.divide} flg={this.props.flg}
                        								  getAhuGroups={this.props.getAhuGroups}
                                                          openMoveAhuGroup={this.props.openMoveAhuGroup}
                                                          openngModal={this.props.openngModal} data={ahu}
                                                          ahuGroup={ahuGroup} delete={this.props.delete}
                                                          pid={this.props.pid}/>)}
                        </tbody>
                    </table>
                }

            </div>
        );
    }
}


//<Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} contentLabel="Three Views Modal">
//            <AhuInit projectId={null} onCloseModal={this.closeModal} groupId={ahuGroup.groupId} flg={2}/>
//          </Modal>