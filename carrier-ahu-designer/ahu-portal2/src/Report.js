import React from 'react';
import classnames from 'classnames';
import http from './misc/http';
import $ from 'jquery';
import { Link } from 'react-router';
import jsPDF from './vendors/jspdf.debug';

import style from './report.css';

export default class Report extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      ahus: [
        {code: '001', name: 'AHU1'},
        {code: '002', name: 'AHU2'},
        {code: '003', name: 'AHU3'},
      ],
      selectedAhu: {all: false}, selectedReport: 'Project List',
    };

    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleAllCheckboxChange = this.handleAllCheckboxChange.bind(this);
    this.getAhus = this.getAhus.bind(this);
    this.projectId = this.props.params.projectId;
  }

  componentDidMount() {
    this.getAhus();
  }

  handleSelectChange({target}) {
    this.setState({selectedReport: target.value});
  }

  handleCheckboxChange(code, checked) {
    this.setState(prevState => {
      const selectedAhu = $.extend(true, {}, prevState.selectedAhu);
      selectedAhu[code] = checked;
      return {selectedAhu};
    });
  }

  handleAllCheckboxChange({target}) {
    this.setState(prevState => {
      const selectedAhu = $.extend(true, {}, prevState.selectedAhu);
      selectedAhu.all = target.checked;
      this.state.ahus.forEach(ahu => {
        selectedAhu[ahu.code] = target.checked;
      });
      return {selectedAhu};
    });
  }

  getAhus(){
    http.get('ahu/list',{projectId:this.projectId,groupId:""})
    .then(data=>{
      this.setState({ahus:data.data});
    });
  }

  render() {
    const doc = new jsPDF();
    const strings = [];
    if (this.state.selectedReport) strings.push(this.state.selectedReport);
    this.state.ahus.forEach(ahu => {
      if (this.state.selectedAhu[ahu.code]) strings.push(`${ahu.code} ${ahu.name}`);
    });
    doc.text(strings, 10, 10);
    return (
      <div className={classnames(style.report, 'clearfix')}>
        <div className={style.west}>
          <Link to="/page2/1">Go back to page2</Link>
          <select className="select" value={this.state.selectedReport} onChange={this.handleSelectChange}>
            <option value="Project List">项目清单</option>
            <option value="Tech Instruction">技术说明</option>
            <option value="Three Views">三视图</option>
            <option value="Fan">风机曲线</option>
            <option value="Han Shi Tu">焓湿图</option>
            <option value="Parameter List">主要参数列表</option>
            <option value="What?!">长交货期选项</option>
          </select>
          <table className="table">
            <thead>
              <tr>
                <th style={{width:'20px'}}>
                  <input type="checkbox" checked={this.state.selectedAhu.all} onChange={this.handleAllCheckboxChange} />
                </th>
                <th>机组编号</th>
                <th>机组名称</th>
              </tr>
            </thead>
            <tbody>
              {this.state.ahus.map((ahu, index) => <Ahu key={index} data={ahu} checked={typeof this.state.selectedAhu[ahu.code] === typeof undefined ? false : this.state.selectedAhu[ahu.code]} onChange={this.handleCheckboxChange} />)}
            </tbody>
          </table>
        </div>
        <div className={style.east} style={{height:window.innerHeight}}>
          <object data={doc.output('datauristring')} type="application/pdf" width="100%" height="100%">
            <p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/pdf/sample-3pp.pdf">Download PDF</a>.</p>
          </object>
        </div>
      </div>
    );
  }
}

class Ahu extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange({target}) {
    this.props.onChange(this.props.data.code, target.checked);
  }

  render() {
    return (
      <tr>
        <td>
          <input type="checkbox" checked={this.props.checked} onChange={this.handleChange} />
        </td>
        <td>{this.props.data.code}</td>
        <td>{this.props.data.name}</td>
      </tr>
    );
  }
}
