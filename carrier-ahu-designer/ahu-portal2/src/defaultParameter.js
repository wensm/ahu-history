import React from 'react';
import Modal from 'react-modal';

import { Link } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import http from './misc/http';
import Header from './Header';
import style from './defaultParameter.css';

export default class DefaultParameter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      airParams:{}
    };
    this.handlerInputChange = this.handlerInputChange.bind(this);
    this.save = this.save.bind(this);
  }

  componentDidMount() {

  }

  handlerInputChange(event){
    var airParams = this.state.airParams;
    airParams.paramType = 'SUM';
    var value = event.target.value;
    var name = event.target.name;
    airParams[name] = value;
    this.setState({airParams:airParams});
    console.log(this.state.airParams);
  }

  save(){
    http.post('defaultparams', this.state.airParams)
    .then(data=>{
    
    });
  }

  render() {
    return (
      <div>
        <Header />
        

        <div style={{marginTop:'70px',width: '100%'}}>
          <span></span>
          <Tabs>
          <TabList>
            <Tab>空气参数设定</Tab>
            <Tab>默认值设定</Tab>
          </TabList>

          <TabPanel>
            <div className={style.layout}>
            
            <div className={style.season}>
              <div style={{marginLeft:'20px'}}>夏季</div>
              <div className={style.layout2}>
                <span className={style.title}>新风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input name="dryBulbTem" onChange={this.handlerInputChange}/>
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>
              <div className={style.layout2}>
                <span className={style.title}>回风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>

              <div className={style.layout2}>
                <span className={style.title}>出风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>
            </div>

            <div className={style.season}>
              <div style={{marginLeft:'20px'}}>冬季</div>

              <div className={style.layout2}>
                <span className={style.title}>新风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>
              <div className={style.layout2}>
                <span className={style.title}>回风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>

              <div className={style.layout2}>
                <span className={style.title}>出风参数</span>
                <div className={style.property}>
                  <label>干球温度</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>湿球温度</label>
                  <input />
                  <span className={style.unit}>dc</span>
                </div>
                <div className={style.property}>
                  <label>相对温度</label>
                  <input />
                  <span  className={style.unit}>%</span>
                </div>
              </div>

            </div>
            <div style={{clear:'both'}}></div>
            <div className={style.btnDiv}>
              <button type="button" onClick={this.save} className={style.button}>保存</button>
            </div>

          </div>
          </TabPanel>


          <TabPanel>
            <div className={style.layout} style={{backgroundColor:'#e3e5e6'}}>
            <div style={{clear:'both',marginTop:'20px'}}>
              {/*<span className={style.title}>外围面积</span>*/}

              <div className={style.property}>
                <div className={style.row}>
                  <label>最大水阻力</label>
                  <input />
                  <span  className={style.unit}>dc</span>
                </div>
                <div className={style.row}>
                  <label>风量单位</label>
                  <input />
                </div>
              </div>
              <div className={style.property}>
                <div className={style.row}>
                  <label>联箱材质</label>
                  <input />
                </div>
                <div className={style.row}>
                  <label>内测版厚度</label>
                  <input />
                </div>
              </div>
              <div className={style.property}>
                <div className={style.row}>
                  <label>外侧板厚度</label>
                  <input />
                </div>
                <div className={style.row}>
                  <label>电源</label>
                  <input />
                </div>
              </div>
              <div className={style.property}>
                <div className={style.row}>
                  <label>电机供应商</label>
                  <input />
                </div>
              </div>
            </div>
            <div style={{clear:'both'}}></div>
            <div className={style.btnDiv}>
              <button type="button" className={style.button}>保存</button>
            </div>
          </div>
          </TabPanel>
        </Tabs>
          

          


        </div>

      </div>
    );
  }
}