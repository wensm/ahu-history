/* eslint-disable */
import React from 'react';
import classnames from 'classnames';
import Alert from 'react-s-alert';
import $ from 'jquery';
import beep from './audio/beep.mp3'
import style from './app.css';
import img3 from './images/log1.png';

export default class App extends React.Component {

  componentDidMount() {
    $('[data-log]').click(() => {
      $('[data-console]').toggleClass(style.dn);
    });
  }

  render() {
    const childrenWithProps = React.Children.map(this.props.children, child => React.cloneElement(child, { data: 'React' }));
    return (
      <div className={style.app}>
        <div className={style.header}></div>
        <div className={style.main}>
          {childrenWithProps}
        </div>
        <div className={style.footer}>
          <Alert stack={{limit: 3}} position="bottom-left" effect="slide" timeout={3000} beep={beep} />
        </div>
        <div className={style.log} data-log><img src={img3}/></div>
        <div className={classnames(style.console, style.dn)} data-console>
          <ul>
            <li className={style.info}>当前价格规则版本：carrier_2017.03.10.</li>
            <li className={style.info}>11:01: 启动成功</li>
            <li className={style.warning}>11:02: AHU section配置冲突</li>
            <li className={style.info}>11:03: AHU 计算引擎完成计算</li>
            <li className={style.error}>11:05: AHU 选择了废弃的计算规则版本</li>
            <li className={style.info}>11:06: AHU 校验合法</li>
            <li className={style.info}>11:07: 项目保存成功</li>
            <li className={style.info}>11:11: 发现新的元数据更新</li>
          </ul>
        </div>
      </div>
    );
  }
}
