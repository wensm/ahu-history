package com.carrier.ahu.api;

import com.alibaba.fastjson.JSONObject;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Wen zhengtao on 2017/4/3.
 */
public class Api extends AbstractApi {
    public static void importData(String path,Map<String,Object> map){
        try {
            HttpRequest request = HttpRequest.post(URL);
            request.path(path);
            String body = JSONObject.toJSONString(map);
            request.body(body);
            HttpResponse response = request.send();
            byte[] bytes = response.bodyBytes();
            String result = new String(bytes,Charset.forName("UTF-8"));
            System.out.println(result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
