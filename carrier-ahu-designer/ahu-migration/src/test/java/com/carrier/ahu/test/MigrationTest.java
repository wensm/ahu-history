package com.carrier.ahu.test;

import com.carrier.ahu.migration.Migration;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Wen zhengtao on 2017/4/3.
 */
public class MigrationTest extends AbstractTest {
    @Autowired
    Migration migration;

    @Test
    public void migrationProject(){
        try {
            migration.migrationProject();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void migrationAhu(){
        try {
            migration.migrationAhu();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void migrationSection(){
        try {
            migration.migrationSection();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
