package com.carrier.ahu.service.impl;

import java.io.File;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.po.UnitType;
import com.carrier.ahu.report.ExportPart;
import com.carrier.ahu.report.ExportPartition;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.specification.PartitionSpecifications;
import com.carrier.ahu.specification.SectionSpecifications;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ValueFormatUtil;
import com.carrier.ahu.vo.SysConstants;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class AhuServiceImpl extends AbstractService implements AhuService {
	@Override
	public String addAhu(Unit unit, String userName) {
		unit.setCreateInfo(userName);
		try {
			String newUnitNO = ValueFormatUtil.getUsefulUnitNOWhenImport(findAllUnitNOInuse(unit.getPid()),
					Integer.parseInt(unit.getDrawingNo()));
			if (EmptyUtil.isEmpty(newUnitNO)) {
				return null;
			}
			unit.setUnitNo(newUnitNO);
			ahuDao.save(unit);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return unit.getUnitid();
	}

	@Override
	public String updateAhu(Unit unit, String userName) {
		unit.setUpdateInfo(userName);
		ahuDao.save(unit);
		return unit.getUnitid();
	}

	@Override
	public List<String> addOrUpdateAhus(List<Unit> units) {
		ahuDao.save(units);
		List<String> list = new ArrayList<>();
		for (Unit u : units) {
			list.add(u.getUnitid());
		}
		return list;
	}

	@Override
	public void deleteAhu(String ahuId) {
		ahuDao.delete(ahuId);
	}

	@Override
	public Unit findAhuById(String ahuId) {
		Unit ahu = ahuDao.findOne(ahuId);
		return ahu;
	}

	@Override
	public Page<Unit> findAhuList(String projectId, String groupId, Pageable pageable) {
		Page<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
				Predicate p2 = null;
				if (StringUtils.isEmpty(groupId)) {
					Predicate p = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), ServiceConstant.SYS_BLANK);
					Predicate p0 = cb.isNull(root.get(ServiceConstant.SYS_STRING_GROUPID));
					p2 = cb.or(p, p0);
				} else {
					p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
				}

				query.where(cb.and(p1, p2));
				return null;
			}
		}, pageable);
		return list;
	}

	@Override
	public List<Unit> findAhuList(String projectId, String groupId) {
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
				if (StringUtils.isNotBlank(groupId)) {
					Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
					query.where(cb.and(p1, p2));
				} else {
					query.where(p1);
				}
				return null;
			}
		});
		return list;
	}

	@Override
	public List<UnitType> getUnitTypes() {
		List<UnitType> unitTypes = new ArrayList<UnitType>();
		UnitType unitType1 = new UnitType();
		unitType1.setTypeName("39G1317");
		unitType1.setVelocity("2.97");
		unitTypes.add(unitType1);
		UnitType unitType2 = new UnitType();
		unitType2.setTypeName("39G1418");
		unitType2.setVelocity("2.63");
		unitTypes.add(unitType2);
		UnitType unitType3 = new UnitType();
		unitType3.setTypeName("39G1420");
		unitType3.setVelocity("2.34");
		unitTypes.add(unitType3);
		UnitType unitType4 = new UnitType();
		unitType4.setTypeName("39G1621");
		unitType4.setVelocity("1.91");
		unitTypes.add(unitType4);
		UnitType unitType5 = new UnitType();
		unitType5.setTypeName("39G1822");
		unitType5.setVelocity("1.53");
		unitTypes.add(unitType5);
		UnitType unitType6 = new UnitType();
		unitType6.setTypeName("39G1825");
		unitType6.setVelocity("1.33");
		unitTypes.add(unitType6);
		UnitType unitType7 = new UnitType();
		unitType7.setTypeName("39G2025");
		unitType7.setVelocity("1.24");
		unitTypes.add(unitType7);
		UnitType unitType8 = new UnitType();
		unitType8.setTypeName("39G2125");
		unitType8.setVelocity("1.15");
		unitTypes.add(unitType8);
		UnitType unitType9 = new UnitType();
		unitType9.setTypeName("39G2226");
		unitType9.setVelocity("1.04");
		unitTypes.add(unitType9);
		UnitType unitType10 = new UnitType();
		unitType10.setTypeName("39G2328");
		unitType10.setVelocity("0.93");
		unitTypes.add(unitType10);
		UnitType unitType11 = new UnitType();
		unitType11.setTypeName("39G2330");
		unitType11.setVelocity("0.88");
		unitTypes.add(unitType11);
		UnitType unitType12 = new UnitType();
		unitType12.setTypeName("39G2333");
		unitType12.setVelocity("0.78");
		unitTypes.add(unitType12);
		return unitTypes;
	}

	@Override
	public String getMaxAhuId() {
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return null;
			}
		});
		String max = "000";
		for (Unit u : list) {
			if (!ValueFormatUtil.isUnitNoLegal(u.getUnitNo())) {
				continue;
			}
			if (max.compareTo(u.getUnitNo()) < 0) {
				max = u.getUnitNo();
			}
		}
		return max;
	}

	@Override
	public String getMaxdrawingNo() {
		return Integer.parseInt(getMaxAhuId()) + ServiceConstant.SYS_BLANK;
	}

	@Override
	public Unit findAhuByDrawingNO(String projectid, String drawingNO) {
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectid);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO), drawingNO);
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		if (list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}
	
	@Override
	public Unit findAhuByDrawingNOAndGroupId(String projectid, String drawingNO, String groupId) {
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectid);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO), drawingNO);
				Predicate p3 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
				query.where(cb.and(p1, p2, p3));
				return null;
			}
		});
		if (list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}
	
	/**
	 * 获取所有在用的机组编号
	 * 
	 * @throws Exception
	 */
	@Override
	public List<String> findAllUnitNOInuse(String projectId) throws Exception {
		if (EmptyUtil.isEmpty(projectId)) {
			throw new Exception("查询机组编号，projectId Is null");
		}
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
				query.where(p1);
				return null;
			}
		});
		List<String> noList = new ArrayList<>();
		for (Unit u : list) {
			noList.add(u.getUnitNo());
		}
		return noList;
	}

	@Override
	public List<String> fixUnitNo(String projectId) {
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
				query.where(p1);
				return null;
			}
		});
		Collections.sort(list, new Comparator<Unit>() {
			public int compare(Unit o1, Unit o2) {
				return o1.getUnitNo().compareTo(o2.getUnitNo());
			}
		});
		List<Unit> toUpdateList = new ArrayList<>();
		for (int i = 1; i < list.size(); i++) {
			Unit unit = list.get(i - 1);
			unit.setUnitNo(ValueFormatUtil.transNumberic2String(i, 3));
			toUpdateList.add(unit);
		}
		List<String> lists = addOrUpdateAhus(toUpdateList);
		return lists;
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
			@Override
			public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (Unit bean : list) {
				idList.add(bean.getUnitid());
			}
		}
		return idList;
	}

    @Override
    public File exportUnit(Unit unit,String version) throws Exception {
        ExportUnit exportUnit = new ExportUnit();
        BeanUtils.copyProperties(exportUnit, unit);
        List<Part> partsOfUnit = sectionDao.findAll(SectionSpecifications.sectionsOfUnit(unit.getUnitid()));
        List<ExportPart> exportParts = new ArrayList<>();
        for (Part aPartOfUnit : partsOfUnit) {
            ExportPart exportPart = new ExportPart();
            BeanUtils.copyProperties(exportPart, aPartOfUnit);
            exportParts.add(exportPart);
        }
        exportUnit.setExportParts(exportParts);

        List<Partition> partitionsOfUnit = partitionDao.findAll(PartitionSpecifications.partitionsOfUnit(unit.getUnitid()));
        Partition partitionOfUnit = partitionsOfUnit.isEmpty() ? null : partitionsOfUnit.get(0);

        if (EmptyUtil.isNotEmpty(partitionOfUnit)) {
            ExportPartition exportPartition = new ExportPartition();
            BeanUtils.copyProperties(exportPartition, partitionOfUnit);
            exportUnit.setPartition(exportPartition);
        }
		exportUnit.setVersion(version);
        String fileName = MessageFormat.format(SysConstants.NAME_EXPORT_AHU,
                new String(unit.getUnitid().getBytes(Charset.forName(ServiceConstant.SYS_ENCODING_UTF8_UP)), ServiceConstant.SYS_ENCODING_ISO_8859_1));
        return FileUtil.writeFile(SysConstants.DIR_EXPORT + fileName, JSONObject.toJSONString(exportUnit));
    }

}
