package com.carrier.ahu.service.validate.impl;

import org.springframework.stereotype.Service;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.service.validate.ValidateService;

/**
 * 屏蔽所有校验，前端js已经做了控制。
 */
@Service
public class ValidateServiceImpl implements ValidateService {
	@Override
	public void compareT(double ParmT, double ParmTB, LanguageEnum languageEnum)
			throws TempCalErrorException {
		/*if (ParmTB > ParmT) {//湿球不能大于干球
			throw new TempCalErrorException(AHUContext.getIntlString(I18NConstants.WET_BULB_TEMP_NOT_EXCEED_DRY_BULB_TEMP));
		}*/
	}

	@Override
	public void compareHumidificationQ(double humidificationQ,
			LanguageEnum languageEnum) throws TempCalErrorException {
		/*if (humidificationQ < 0) {//加湿量不能 小于 0
			throw new TempCalErrorException(AHUContext.getIntlString(I18NConstants.HUMIDIFICATION_VOLUME_NOT_LESS_THAN_ZERO));
		}*/
	}

	@Override
	public void compareRelativeT(double relativeT, LanguageEnum languageEnum)
			throws TempCalErrorException {
		/*if (relativeT>100) {//相对湿度，计算结果不能大于100%
			throw new TempCalErrorException(AHUContext.getIntlString(I18NConstants.RELATIVE_HUMIDITY_NOT_EXCEED_HUNDRED_PERCENTAGE));
		}
		if (relativeT < 0) {//相对湿度，计算结果不能小于0
			throw new TempCalErrorException(AHUContext.getIntlString(I18NConstants.RELATIVE_HUMIDITY_NOT_LESS_THAN_ZERO));
		}*/
	}

}
