package com.carrier.ahu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.service.ACCalculatorService;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;

/**
 * 
 * @ClassName: ACCalculatorServiceImpl 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年1月29日 下午7:41:18 
 *
 */
@Service
public class ACCalculatorServiceImpl extends AbstractService implements ACCalculatorService {
	@Autowired
	private ValidateService validateService;
	@Override
	public AirConditionBean FAirParmCalculate1(double ParmT, double ParmTB,LanguageEnum languageEnum) throws TempCalErrorException {
		//湿球不能大于干球
		validateService.compareT(ParmT, ParmTB, languageEnum);
		
		AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(ParmT,
	                ParmTB);
		return bean;
	}

	@Override
	public HumidificationBean IsoenthalpyRHConvert(double E_Db, double E_Wb,
			double RH, int airV,LanguageEnum languageEnum) throws TempCalErrorException {
		HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyRHConvert(E_Db, E_Wb,
				RH, airV);
		return humidificationBean;
	}
	
	@Override
	public HumidificationBean IsothermalRHConvert(double E_Db, double E_Wb,
			double RH, int airV,LanguageEnum languageEnum) throws TempCalErrorException {
		HumidificationBean humidificationBean = PerformanceCalculator.IsothermalRHConvert(E_Db, E_Wb,
				RH, airV);
		return humidificationBean;
	}

	@Override
	public HumidificationBean IsoenthalpyHumidificationConvert(double E_Db, double E_Wb,
			double humidification, int airV,LanguageEnum languageEnum) throws TempCalErrorException {
		HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(E_Db, E_Wb,
				humidification, airV);
		return humidificationBean;
	}

	@Override
	public HumidificationBean IsothermalHumidificationConvert(double E_Db, double E_Wb,
			double humidification, int airV,LanguageEnum languageEnum) throws TempCalErrorException {
		HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(E_Db, E_Wb,
				humidification, airV);
		return humidificationBean;
	}

	
}
