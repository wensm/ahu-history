package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.RESISTANCE_CALCULATION_FAILED;
import static com.carrier.ahu.constant.CommonConstant.METACOMMON_POSTFIX_OUTLETPRESSURE;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

import cn.hutool.core.util.NumberUtil;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.MetaSelectUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.ResistanceCalcException;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.resistance.GetEntityResistance;
import com.carrier.ahu.resistance.count.WetFilmResultInfo;
import com.carrier.ahu.resistance.param.ResistanceBCVYBean;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.InvokeTool;

@Component
public class ResistanceCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(ResistanceCalEngine.class.getName());

    @Autowired
    private ResistanceService resistanceService;

    @Override
    public int getOrder() {
        return 6;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        try {
            logger.info("ResistanceCalEngine calculator line begin");
            ResistanceParam resistanceParam = new InvokeTool<ResistanceParam>().genInParamFromAhuParam(ahu, section, null, new ResistanceParam());
            SectionTypeEnum sectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(section.getKey());
            resistanceParam.setSectionType(sectionTypeEnum.getCode());
            logger.info("ResistanceCalEngine calculator param :" + JSONArray.toJSONString(resistanceParam));
            if ("BVY".contains(resistanceParam.getSectionType())) {// B:过滤段

                if("B".equals(resistanceParam.getSectionType()))
                    reSetFilterEfficiency(resistanceParam);

                ResistanceBCVYBean resistanceBCVYBean = resistanceService.getResistanceBean(resistanceParam);
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_DINITIALPD, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDinitialPD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_DEVERAGEPD, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_DFINALPD, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDfinalPD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, convert2Int(String.valueOf(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePD(), 1)))));
                logger.info("ResistanceCalEngine calculator resistanceBCVYBean :" + JSONArray.toJSONString(resistanceBCVYBean));
            } else if ("C".contains(resistanceParam.getSectionType())) {
                ResistanceBCVYBean resistanceBCVYBean = resistanceService.getResistanceBean(resistanceParam);
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_INITIALPDP, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDinitialPD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_EVERAGEPDP, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_FINALPDP, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDfinalPD(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_INITIALPDB, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDinitialPDB(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_EVERAGEPDB, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePDB(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_FINALPDB, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDfinalPDB(), 1))));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, convert2Int(String.valueOf(BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePDB(), 1) + BaseDataUtil.decimalConvert(resistanceBCVYBean.getDeveragePD(), 1))));
                logger.info("ResistanceCalEngine calculator resistanceBCVYBean :" + JSONArray.toJSONString(resistanceBCVYBean));
            } else {
                double resistance = resistanceService.getResistance(resistanceParam);
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, convert2Int(String.valueOf(resistance)));
                if (SectionTypeEnum.TYPE_COLD.getCode().equals(resistanceParam.getSectionType())) {// D:冷水盘管段 计算挡水器重量
                    GetEntityResistance getEntityResistance = new GetEntityResistance();
                    int airVolume = resistanceParam.getSairvolume();
                    if (AirDirectionEnum.RETURNAIR.getCode().equals(resistanceParam.getAirDirection())) {
                        airVolume = resistanceParam.getEairvolume();
                    }
                    String eliminator = resistanceParam.getEliminator();
                    double driftEliminatorResistance = 0.0;
                    if(!COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)){
                        driftEliminatorResistance = getEntityResistance.getEntityResistance(SectionTypeEnum.TYPE_COLD.getCode(),resistanceParam.getSerial(), airVolume, resistanceParam.getEliminator());
                    }
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_DRIFTELIMINATORRESISTANCE, convert2Int(String.valueOf(driftEliminatorResistance)));
                }
                if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// I:湿膜加湿段 计算类型（厚度）
                    WetFilmResultInfo resistanceBCVYBean = (WetFilmResultInfo) resistanceService.getResistanceBean(resistanceParam);
                    if (0 == resistanceBCVYBean.getThickness()) {
                        throw new ResistanceCalcException(ErrorCode.WET_FILM_NO_FOUND);
                    }
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_THICKNESS, String.valueOf(resistanceBCVYBean.getThickness()));
                }
                if (SectionTypeEnum.TYPE_MIX.getCode().equals(resistanceParam.getSectionType())) {
                    if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(resistanceParam.getDamperOutlet())) {//风口接件：(法兰,风口阻力0 ; 其他风口阻力10)
                        section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_OUTLETPRESSURE, "0");
                    }else{
                        section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_OUTLETPRESSURE, "10");
                    }
                }
                if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(resistanceParam.getSectionType())) {
                    if (SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE.equals(resistanceParam.getAInterface())) {//风口接件：(法兰,风口阻力0 ; 其他风口阻力5)
                        section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_AINTERFACER, "0");
                    }else{
                        section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_AINTERFACER, "5");
                    }
                }
                logger.info("ResistanceCalEngine calculator resistance :" + resistance);
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("ResistanceCalEngine calculator line end");
            return subc;
        } catch (ResistanceCalcException e) {
            String message = getIntlString(RESISTANCE_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("ResistanceCalEngine debug massage :" + message, e.getMessage());
            logger.error("ResistanceCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new ResistanceCalcException(e.getErrorCode());
        }
    }

    private void reSetFilterEfficiency(ResistanceParam resistanceParam) {
        if(EmptyUtil.isEmpty(resistanceParam.getFilterEfficiency())){
            resistanceParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_C2);
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = AhuSectionMetas.SECTIONTYPES_RESISTANCE;
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_RESISTANCE;
    }

}
