package com.carrier.ahu.service.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.ResistanceCalcException;
import com.carrier.ahu.length.util.ParamConvertFactory;
import com.carrier.ahu.metadata.entity.filter.ElectrostaticFilterResistance;
import com.carrier.ahu.metadata.entity.filter.HEPAFilterResistance;
import com.carrier.ahu.metadata.entity.filter.NWFSingleFilterResistance;
import com.carrier.ahu.resistance.ControlRes;
import com.carrier.ahu.resistance.GetEntityResistance;
import com.carrier.ahu.resistance.HeatRecoveryRes;
import com.carrier.ahu.resistance.RepeatingWindRes;
import com.carrier.ahu.resistance.SteamRes;
import com.carrier.ahu.resistance.WetFilmRes;
import com.carrier.ahu.resistance.count.WetFilmResultInfo;
import com.carrier.ahu.resistance.param.ResistanceBCVYBean;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/9/28.
 */
@Component
public class ResistanceServiceImpl implements ResistanceService {

	private static Logger logger = LoggerFactory.getLogger(ResistanceServiceImpl.class.getName());

	@Override
	public double getResistance(ResistanceParam resistanceParam) throws ResistanceCalcException {
		double resistance = 0;
		try {
			int airVolume = 0;
			if (AirDirectionEnum.RETURNAIR.getCode().equals(resistanceParam.getAirDirection())) {
				airVolume = resistanceParam.getEairvolume();
			} else {
				airVolume = resistanceParam.getSairvolume();
			}
			if (SectionTypeEnum.TYPE_MIX.getCode().equals(resistanceParam.getSectionType())) {// A:混合段
				resistance = SectionCount.sectionResistanceMix(airVolume, resistanceParam.getSerial(),
						resistanceParam.getSectionL(), resistanceParam.getDamperOutlet());
			} else if (SectionTypeEnum.TYPE_COLD.getCode().equals(resistanceParam.getSectionType())) {// D:冷水盘管段
				// 冷水盘管段阻力在DLL引擎中计算
				resistance = resistanceParam.getSAirResistance() + resistanceParam.getDriftEliminatorResistance();
			} else if (SectionTypeEnum.TYPE_HEATINGCOIL.getCode().equals(resistanceParam.getSectionType())) {// E:热水盘管段
				// 热水盘管段阻力在DLL引擎中计算
				resistance = resistanceParam.getWAirResistance() + resistanceParam.getDriftEliminatorResistance();
			} else if (SectionTypeEnum.TYPE_STEAMCOIL.getCode().equals(resistanceParam.getSectionType())) {// F:蒸汽盘管段
				SteamRes steamRes = new SteamRes();
				resistance = steamRes.getResistance(resistanceParam.getSerial(), airVolume);
			} else if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode().equals(resistanceParam.getSectionType())) {// G:电加热盘管段
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				resistance = getEntityResistance.getEntityResistance(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode(),
						resistanceParam.getSerial(), airVolume, null);
			} else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// H:干蒸汽加湿段
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				resistance = getEntityResistance.getResistance(resistanceParam.getSerial(),
						SectionTypeEnum.TYPE_ACCESS.getCode(), resistanceParam.getSectionL(), airVolume);
			} else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// I:湿膜加湿段
				WetFilmRes wetFilmRes = new WetFilmRes();
				WetFilmResultInfo wetFilmResultInfo = wetFilmRes.getResistance(resistanceParam.getSectionType(),
						resistanceParam.getSerial(), airVolume, resistanceParam.getSupplier(),
						resistanceParam.getSeason(), resistanceParam.getSHumidificationQ(),
						resistanceParam.getWHumidificationQ(), resistanceParam.getSInDryBulbT(),
						resistanceParam.getSInWetBulbT(), resistanceParam.getWInDryBulbT(),
						resistanceParam.getWInWetBulbT());
				resistance = wetFilmResultInfo.getResistance();
			} else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// J:高压喷雾加湿段
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				resistance = getEntityResistance.getEntityResistance(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode(),
						resistanceParam.getSerial(), airVolume, null);
			} else if (SectionTypeEnum.TYPE_FAN.getCode().equals(resistanceParam.getSectionType())) {// K:风机段

			} else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getCode().equals(resistanceParam.getSectionType())) {// L:新回排风段
				RepeatingWindRes repeatingWindRes = new RepeatingWindRes();
				resistance = repeatingWindRes.getResistance();
			} else if (SectionTypeEnum.TYPE_ATTENUATOR.getCode().equals(resistanceParam.getSectionType())) {// M:消音段
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				String deadening = resistanceParam.getDeadening();
				Object deadeningLevel = "";// 消音级数2，需要使用csv resistance2作为阻力列
				if (ServiceConstant.SYS_SEXON_ALIAS_FILTER.equals(deadening)) {
					deadeningLevel = ServiceConstant.SYS_STRING_NUMBER_2;
				}
				resistance = getEntityResistance.getEntityResistance(SectionTypeEnum.TYPE_ATTENUATOR.getCode(),
						resistanceParam.getSerial(), airVolume, deadeningLevel);
			} else if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(resistanceParam.getSectionType())) {// N:出风段
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				resistance = getEntityResistance.getResistance(resistanceParam.getSerial(),
						resistanceParam.getSectionType(), resistanceParam.getSectionL(), airVolume);
				resistance = getEntityResistance.packageN(resistanceParam.getSerial(), resistanceParam.getSectionL(),
						resistance, resistanceParam.getAInterface());
			} else if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(resistanceParam.getSectionType())) {// O:空段
				if (SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER.equals(resistanceParam.getFunction())) {// 均流
					GetEntityResistance getEntityResistance = new GetEntityResistance();
					resistance = getEntityResistance.getEntityResistance(SectionTypeEnum.TYPE_ACCESS.getCode(),
							resistanceParam.getSerial(), airVolume, null);
				} else {
					resistance = SectionCount.sectionResistanceAccess(airVolume, resistanceParam.getSerial(),
							resistanceParam.getSectionL());
				}
			} else if (SectionTypeEnum.TYPE_HEATRECYCLE.getCode().equals(resistanceParam.getSectionType())) {// W:热回收段
				HeatRecoveryRes heatRecoveryRes = new HeatRecoveryRes();
				resistance = heatRecoveryRes.getResistance();
			} else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// X:电极加湿
				GetEntityResistance getEntityResistance = new GetEntityResistance();
				resistance = getEntityResistance.getResistance(resistanceParam.getSerial(),
						resistanceParam.getSectionType(), resistanceParam.getSectionL(), airVolume);
			} else if (SectionTypeEnum.TYPE_CTR.getCode().equals(resistanceParam.getSectionType())) {// Z:控制段
				ControlRes controlRes = new ControlRes();
				resistance = controlRes.getResistance();
			} else if ((ServiceConstant.SYS_SEXON_ALIAS_FILTER + ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER
					+ ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER + ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER)
							.contains(resistanceParam.getSectionType())) {// B:过滤段
				resistanceParam = ParamConvertFactory.resistanceParam(resistanceParam);
				if (ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER.equals(resistanceParam.getSectionType())) {

					HEPAFilterResistance hepaFilterResistanceItem = SectionCount.sectionResistanceHEPAFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							resistanceParam.getFitetF(), resistanceParam.getFitlerStandard(),
							resistanceParam.getFilterEfficiency());

					resistance = hepaFilterResistanceItem.getAverResi();
					System.out.println("avg" + hepaFilterResistanceItem.getAverResi());
					System.out.println("begin" + hepaFilterResistanceItem.getBeginResi());
					System.out.println("end" + hepaFilterResistanceItem.getEndResi());
				} else if (ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER
						.equals(resistanceParam.getSectionType())) {
					ElectrostaticFilterResistance electostaticFilterResistanceItem = SectionCount
							.sectionResistanceElectostaticFilter(BaseDataUtil.integerConversionDouble(airVolume),
									resistanceParam.getSerial(), resistanceParam.getFilterEfficiency());
					resistance = electostaticFilterResistanceItem.getAverResi();
					System.out.println("avg" + electostaticFilterResistanceItem.getAverResi());
					System.out.println("begin" + electostaticFilterResistanceItem.getAverResi());
					System.out.println("windSpeed" + electostaticFilterResistanceItem.getWindSpeed());
				} else {
					NWFSingleFilterResistance filterResistanceItem = SectionCount.sectionResistanceSimpleFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							resistanceParam.getRimThickness(), resistanceParam.getFitetF(),
							resistanceParam.getSectionL(), resistanceParam.getFitlerStandard(),
							resistanceParam.getFilterEfficiency(), false, SectionTypeEnum.TYPE_SINGLE.getCode());
					if (EmptyUtil.isEmpty(filterResistanceItem)) {
						logger.warn("filterResistanceItem is null when confirm section");
					} else {
						resistance = filterResistanceItem.getAverResi();
						System.out.println("avg" + filterResistanceItem.getAverResi());
						System.out.println("begin" + filterResistanceItem.getBeginResi());
						System.out.println("end" + filterResistanceItem.getEndResi());
					}
				}

			} else {
				return -1;
			}
		} catch (Exception e) {
			logger.debug("resistance calculation error param:" + resistanceParam.toString());
			logger.error("resistance calculation error", e);
			throw new ResistanceCalcException("Resistance calculation error ", e);
			// TODO: handle exception
		}
		resistance = BaseDataUtil.decimalConvert(resistance,0);
		return resistance;
	}

	@Override
	public ResistanceBCVYBean getResistanceBean(ResistanceParam resistanceParam) throws ResistanceCalcException {
		int airVolume = 0;
		if (AirDirectionEnum.RETURNAIR.getCode().equals(resistanceParam.getAirDirection())) {
			airVolume = resistanceParam.getEairvolume();
		} else {
			airVolume = resistanceParam.getSairvolume();
		}
		ResistanceBCVYBean resistanceBCVYBean = new ResistanceBCVYBean();
		if ((ServiceConstant.SYS_SEXON_ALIAS_FILTER + ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER
				+ ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER + ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER)
						.contains(resistanceParam.getSectionType())) {// B:过滤段
			resistanceParam = ParamConvertFactory.resistanceParam(resistanceParam);
			if (ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER.equals(resistanceParam.getSectionType())) {
				if (SystemCalculateConstants.HEPAFILTER_SUPPLIER_NONE.equals(resistanceParam.getSupplier())) {// 高效过滤段供应商为无过滤器，阻力为0
					resistanceBCVYBean.setDinitialPD(0);
					resistanceBCVYBean.setDeveragePD(0);
					resistanceBCVYBean.setDfinalPD(0);
				} else {
					HEPAFilterResistance hepaFilterResistanceItem = SectionCount.sectionResistanceHEPAFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							resistanceParam.getFitetF(), resistanceParam.getFitlerStandard(),
							resistanceParam.getFilterEfficiency());
					resistanceBCVYBean.setDinitialPD(hepaFilterResistanceItem.getBeginResi());
					resistanceBCVYBean.setDeveragePD(hepaFilterResistanceItem.getAverResi());
					resistanceBCVYBean.setDfinalPD(hepaFilterResistanceItem.getEndResi());
					System.out.println("avg" + hepaFilterResistanceItem.getAverResi());
					System.out.println("begin" + hepaFilterResistanceItem.getBeginResi());
					System.out.println("end" + hepaFilterResistanceItem.getEndResi());
				}
			} else if (ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER.equals(resistanceParam.getSectionType())) {
				ElectrostaticFilterResistance electostaticFilterResistanceItem = SectionCount
						.sectionResistanceElectostaticFilter(BaseDataUtil.integerConversionDouble(airVolume),
								resistanceParam.getSerial(), resistanceParam.getFilterEfficiency());
				resistanceBCVYBean.setDeveragePD(electostaticFilterResistanceItem.getAverResi());
				System.out.println("avg" + electostaticFilterResistanceItem.getAverResi());
				System.out.println("begin" + electostaticFilterResistanceItem.getAverResi());
				System.out.println("windSpeed" + electostaticFilterResistanceItem.getWindSpeed());
			} else {
				int sectionL = resistanceParam.getSectionL();
				if (0 == sectionL
						&& ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER.equals(resistanceParam.getSectionType())) {// 综合过滤段段长默认值
					try {
						String sectionLdefaultValue = SectionMetaUtils
								.getDefaultValue("meta.section.combinedFilter.sectionL", null);
						sectionL = Integer.parseInt(sectionLdefaultValue);
					} catch (Exception e) {
						logger.error("resistance calculation filterResistance error cause by sectionL is 0", e);
					}
				}
				if (ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER.equals(resistanceParam.getSectionType())) {
					boolean pAl = false;
					if (SystemCalculateConstants.COMBINEDFILTER_LMATERIAL_ALMESH
							.equals(resistanceParam.getLMaterial())) {// 板式过滤选项 铝网过滤器
						pAl = true;
					}
					// 板式过滤器阻力
					String[] rimThinckness = resistanceParam.getRimThickness().split("\\+");
					NWFSingleFilterResistance filterResistanceItemP = SectionCount.sectionResistanceSimpleFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							rimThinckness[0], ServiceConstant.JSON_FILTER_FILTERF_P, sectionL,
							resistanceParam.getFitlerStandard(), resistanceParam.getLMaterialE(), pAl,
							SectionTypeEnum.TYPE_COMPOSITE.getCode());

					// 袋式过滤器阻力
					NWFSingleFilterResistance filterResistanceItemB = SectionCount.sectionResistanceSimpleFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							rimThinckness[1], ServiceConstant.SYS_SEXON_ALIAS_FILTER, sectionL,
							resistanceParam.getFitlerStandard(), resistanceParam.getRMaterialE(), false,
							SectionTypeEnum.TYPE_COMPOSITE.getCode());

					if (SystemCalculateConstants.COMBINEDFILTER_LMATERIAL_NO.equals(resistanceParam.getLMaterial())) {// 综合过滤段,板式过滤选项为无过滤器，阻力为0
						resistanceBCVYBean.setDinitialPD(0);
						resistanceBCVYBean.setDeveragePD(0);
						resistanceBCVYBean.setDfinalPD(0);
					} else {
						resistanceBCVYBean.setDinitialPD(filterResistanceItemP.getBeginResi());
						resistanceBCVYBean.setDeveragePD(filterResistanceItemP.getAverResi());
						resistanceBCVYBean.setDfinalPD(filterResistanceItemP.getEndResi());
					}

					if (SystemCalculateConstants.COMBINEDFILTER_RMATERIAL_NO_FILTER
							.equals(resistanceParam.getRMaterial())) {// 综合过滤段,袋式过滤选项为无过滤器，阻力为0
						resistanceBCVYBean.setDinitialPDB(0);
						resistanceBCVYBean.setDeveragePDB(0);
						resistanceBCVYBean.setDfinalPDB(0);
					} else {
						resistanceBCVYBean.setDinitialPDB(filterResistanceItemB.getBeginResi());
						resistanceBCVYBean.setDeveragePDB(filterResistanceItemB.getAverResi());
						resistanceBCVYBean.setDfinalPDB(filterResistanceItemB.getEndResi());
					}

				} else {
					boolean al = false;
					if (SystemCalculateConstants.FILTER_FILTEROPTIONS_ALMESH
							.equals(resistanceParam.getFilterOptions())) {// 板式过滤选项 铝网过滤器
						al = true;
					}
					NWFSingleFilterResistance filterResistanceItem = SectionCount.sectionResistanceSimpleFilter(
							BaseDataUtil.integerConversionDouble(airVolume), resistanceParam.getSerial(),
							resistanceParam.getRimThickness(), resistanceParam.getFitetF(), sectionL,
							resistanceParam.getFitlerStandard(), resistanceParam.getFilterEfficiency(), al,
							SectionTypeEnum.TYPE_SINGLE.getCode());
					if (EmptyUtil.isEmpty(filterResistanceItem)) {
						throw new ResistanceCalcException("filterResistanceItem is null when confirm section");
					} else {
						if (ServiceConstant.SYS_SEXON_ALIAS_FILTER.equals(resistanceParam.getSectionType())
								&& SystemCalculateConstants.FILTER_FILTEROPTIONS_NO
										.equals(resistanceParam.getFilterOptions())) {// 单层过滤段,过滤器选项为无过滤器，阻力为0
							resistanceBCVYBean.setDinitialPD(0);
							resistanceBCVYBean.setDeveragePD(0);
							resistanceBCVYBean.setDfinalPD(0);
						} else {
							resistanceBCVYBean.setDinitialPD(filterResistanceItem.getBeginResi());
							resistanceBCVYBean.setDeveragePD(filterResistanceItem.getAverResi());
							resistanceBCVYBean.setDfinalPD(filterResistanceItem.getEndResi());
							System.out.println("avg" + filterResistanceItem.getAverResi());
							System.out.println("begin" + filterResistanceItem.getBeginResi());
							System.out.println("end" + filterResistanceItem.getEndResi());
						}
					}
				}
			}
		} else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(resistanceParam.getSectionType())) {// I:湿膜加湿段
			WetFilmRes wetFilmRes = new WetFilmRes();
			WetFilmResultInfo wetFilmResultInfo = wetFilmRes.getResistance(resistanceParam.getSectionType(),
					resistanceParam.getSerial(), airVolume, resistanceParam.getSupplier(), resistanceParam.getSeason(),
					resistanceParam.getSHumidificationQ(), resistanceParam.getWHumidificationQ(),
					resistanceParam.getSInDryBulbT(), resistanceParam.getSInWetBulbT(),
					resistanceParam.getWInDryBulbT(), resistanceParam.getWInWetBulbT());
			resistanceBCVYBean = wetFilmResultInfo;
		}
		return resistanceBCVYBean;
	}

}
