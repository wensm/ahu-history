package com.carrier.ahu.service.cad.impl;

import java.io.File;

import com.carrier.ahu.constant.UtilityConstant;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.engine.cad.AhuExporter;
import com.carrier.ahu.engine.cad.CadUtils;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.ahu.AhuExporterUtils;
import com.carrier.ahu.vo.SysConstants;

import static com.carrier.ahu.vo.SysConstants.SLASH_LINUX;

/**
 * Created by liaoyw on 2017/4/9.
 */
@Service
public class AhuExporterServiceImpl implements AhuExporter {
	@Override
	public String createBmp(AhuParam param) {
		String fileName = ServiceConstant.SYS_BLANK + System.currentTimeMillis()+UtilityConstant.SYS_NAME_CAD_FLAG;
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName()+UtilityConstant.SYS_NAME_CAD_FLAG;
		}
		
		String destPath = AhuExporterUtils.getThreeViewPath(param.getPid(), param.getUnitid(), fileName);
		if(!param.getNeedReload3View() && new File(UtilityConstant.SYS_PATH_CAD_DEST_DIR + destPath).exists()){
			return destPath;
		}
		CadUtils.exportBmp(param, destPath);
		return destPath;
	}

	@Override
	public String createDwg(AhuParam param) {
		String fileName = ""+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		String destPath = String.format(
				SysConstants.CAD_DIR + SLASH_LINUX + "%s" + SLASH_LINUX + "%s" + SLASH_LINUX + "%s.dwg",
				param.getPid(), param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);
		return destPath;
		// return "asserts/cad/test.dwg";
	}

	@Override
	public String download(AhuParam param, String fileName) {
		String destPath = String.format(
				SysConstants.CAD_DIR + SLASH_LINUX + "%s" + SLASH_LINUX + "%s" + SLASH_LINUX + "%s",
				param.getPid(), param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);
		return destPath;
	}
}
