package com.carrier.ahu.service.cal;

import lombok.Data;

@Data
public class WeightBean {
    private String name;
    private double weight;
    private String key;
    private String id;
}