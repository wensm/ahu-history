package com.carrier.ahu.service.service.impl;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.SectionLengthCalcException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.length.*;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.length.util.ParamConvertFactory;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

import org.apache.catalina.Engine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by liangd4 on 2017/9/28.
 * 计算段长实现类
 */
@Service
public class LengthServiceImpl implements LengthService {

    private static Logger logger = LoggerFactory.getLogger(LengthServiceImpl.class.getName());
    private static double MIXLENGTHCAL = 0; 

    @Override
    public int getDefaultLength(LengthParam lengthParam) throws SectionLengthCalcException {
        double length = 0;
        String sectionType = lengthParam.getSectionType();
        String sectionId = lengthParam.getSectionId();
        try {
            int airVolume = 0;
            if (AirDirectionEnum.RETURNAIR.getCode().equals(lengthParam.getAirDirection())) {
                airVolume = lengthParam.getEairvolume();
            } else {
                airVolume = lengthParam.getSairvolume();
            }
            if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType)) {//A:混合段
                MixedLen mixedLen = new MixedLen();
                lengthParam = ParamConvertFactory.lengthParam(lengthParam);
                length = mixedLen.getLength(lengthParam.getSerial(), lengthParam.getMixType(), lengthParam.isUvLamp(), airVolume);
            } else if (SectionTypeEnum.TYPE_COLD.getId().equals(sectionId)) {//D:冷水盘管段
                CoilLen coilLen = new CoilLen();
                length = coilLen.getLength(lengthParam, SectionTypeEnum.TYPE_COLD.getId());
            } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionId)) {//D:直接蒸发式盘管
                CoilLen coilLen = new CoilLen();
                length = coilLen.getLength(lengthParam, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
            } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getCode().equals(sectionType)) {//E:热水盘管段
                CoilLen coilLen = new CoilLen();
                length = coilLen.getHLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_STEAMCOIL.getCode().equals(sectionType)) {//F:蒸汽盘管段
                SteamLen steamLen = new SteamLen();
                length = steamLen.getLength();
            } else if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode().equals(sectionType)) {//G:电加热盘管段
                ElectricityLen electricityLen = new ElectricityLen();
                length = electricityLen.getLength();
            } else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode().equals(sectionType)) {//H:干蒸汽加湿段
                DrySteamLen drySteamLen = new DrySteamLen();
                length = drySteamLen.getLength();
            } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(sectionType)) {//I:湿膜加湿段
                WetFilmLen wetFilmLen = new WetFilmLen();
                length = wetFilmLen.getLength();
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(sectionType)) {//J:高压喷雾加湿段
                HighPresueLen highPresueLen = new HighPresueLen();
                length = highPresueLen.getLength();
            } else if (SectionTypeEnum.TYPE_FAN.getCode().equals(sectionType)) {//K:风机段
                String ID = ServiceConstant.SYS_STRING_NUMBER_1;
                if (lengthParam.isVerticalUnit()) {
                    ID = ServiceConstant.SYS_STRING_NUMBER_2;
                }
                length = SectionCount.sectionLengthFan(lengthParam.getSerial(), lengthParam.getFanModel(),
                        lengthParam.getOutletDirection(), lengthParam.getOutlet(), lengthParam.getMotorPosition(),
                        lengthParam.isStandbyMotor(), ID, lengthParam.getMotorBaseNo());
            } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getCode().equals(sectionType)) {//L:新回排风段
                RepeatingWindLen repeatingWindLen = new RepeatingWindLen();
                length = repeatingWindLen.getLength(lengthParam.getSerial());
            } else if (SectionTypeEnum.TYPE_ATTENUATOR.getCode().equals(sectionType)) {//M:消音段
                ClearSoundLen clearSoundLen = new ClearSoundLen();
                length = clearSoundLen.getLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//N:出风段
                WindOutLen windOutLen = new WindOutLen();
                length = windOutLen.getLength(lengthParam.getSerial(), lengthParam.getOutletDirection(), lengthParam.getODoor());
            } else if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {//O:空段
                NullLen nullLen = new NullLen();
                length = nullLen.getLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_HEATRECYCLE.getCode().equals(sectionType)) {//W:热回收段
                //TODO 热回收分为板式和转轮
            } else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode().equals(sectionType)) {//X:电极加湿
                ElectricityHumidifierLen electricityHumidifierLen = new ElectricityHumidifierLen();
                length = electricityHumidifierLen.getLength();
            } else if (SectionTypeEnum.TYPE_CTR.getCode().equals(sectionType)) {//Z:控制段
                ControlLen controlLen = new ControlLen();
                length = controlLen.getLength(lengthParam.getSerial());
			} else if ((ServiceConstant.SYS_SEXON_ALIAS_FILTER + ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER
					+ ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER + ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER)
							.contains(sectionType)) {// B:过滤段 C:综合过滤段 V:高效过滤段 Y:静电过滤器
				if (ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER.equals(sectionType)) {
					length = 9;
				} else if (ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER.equals(sectionType)) {
					length = 4;
				} else if (ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER.equals(sectionType)) {
					length = 4;
				} else {
					length = SectionCount.sectionLengthSimpleFilter(lengthParam.getFitetF(),
							lengthParam.getMediaLoading(), lengthParam.getFilterEfficiency());
				}
			} else if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType) || SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getCode().equals(sectionType)) {
                HeatRecycleLen heatRecycleLen = new HeatRecycleLen();
                length = heatRecycleLen.getLength(lengthParam.getSerial(), sectionType);
                return BaseDataUtil.doubleConversionInteger(length);//如果是热回收段计算的段长是使用段长，不允许修改。
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.debug("Exception in fan seciton length calculation:", lengthParam);
            logger.error("Exception in fan seciton length calculation", e);
            throw new SectionLengthCalcException("Section length calculation error " + lengthParam.getSectionType(), e);
        }
        return BaseDataUtil.doubleConversionInteger(length);
    }


    @Override
    public int getCalLength(LengthParam lengthParam) throws SectionLengthCalcException {
        double calLength = 0;
        String sectionType = lengthParam.getSectionType();
        try {
            int airVolume = 0;
            if (AirDirectionEnum.RETURNAIR.getCode().equals(lengthParam.getAirDirection())) {
                airVolume = lengthParam.getEairvolume();
            } else {
                airVolume = lengthParam.getSairvolume();
            }
            /*混合段 空段 出风段 段长可以编辑*/
            if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType)) {//A:混合段
                MixedLen mixedLen = new MixedLen();
                lengthParam = ParamConvertFactory.lengthParam(lengthParam);
                calLength = mixedLen.getCalLength(lengthParam.getSerial(), lengthParam.getMixType(), lengthParam.isUvLamp(), airVolume);
                MIXLENGTHCAL = calLength;
            } else if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {//O:空段
                NullLen nullLen = new NullLen();
                calLength = nullLen.getCalLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//N:出风段
            	int height = SystemCountUtil.getUnitHeight(lengthParam.getSerial());
                int width = SystemCountUtil.getUnitWidth(lengthParam.getSerial());
                int serialV = height * 100 + width;
                if(serialV >= 2532 && serialV <= 4750) {//大型机组2532-4750的出风段最小段长与混合段一致
                	calLength = MIXLENGTHCAL;
                }else {
                    WindOutLen windOutLen = new WindOutLen();
                    calLength = windOutLen.getCalLength(lengthParam.getSerial(), lengthParam.getOutletDirection(), lengthParam.getODoor(), airVolume);	
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.debug("Exception in fan seciton callength calculation:", lengthParam);
            logger.error("Exception in fan seciton callength calculation", e);
            throw new SectionLengthCalcException("Section callength calculation error " + lengthParam.getSectionType(), e);
        }
        return BaseDataUtil.doubleConversionInteger(calLength);
    }

    @Override
    public int getShowLength(LengthParam lengthParam) throws SectionLengthCalcException {
        String sectionType = lengthParam.getSectionType();
        if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType) || SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType) || SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//A:混合段
            if (lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0 && lengthParam.getCalSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType) ) {
        	if (lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
        	if (lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        return BaseDataUtil.doubleConversionInteger(lengthParam.getDefaultSectionL());
    }
}
