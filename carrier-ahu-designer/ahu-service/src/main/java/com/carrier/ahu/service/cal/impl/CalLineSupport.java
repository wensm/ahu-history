package com.carrier.ahu.service.cal.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

public class CalLineSupport {

    // 如果当前机组只有一个风机，则机组为送风机组。
    // 如果当前机组包含新排段，则新排段前面部分为回风机组，后半部分为送风机组
    // 风机永远是则本组中的最后一位进行计算

    public static List<List<PartParam>> sortedParts(AhuParam ahu) {
        List<PartParam> params = ahu.getPartParams();
        List<List<PartParam>> sortedPartLinks = new LinkedList<List<PartParam>>();

        List<PartParam> sortedReturnParts = new LinkedList<PartParam>();
        List<PartParam> sortedSupplyParts = new LinkedList<PartParam>();
        int combinMixPos = 999;

        // 将机组拆分为送风段和回风段
        Collections.sort(params);
        Iterator<PartParam> it = params.iterator();
        while (it.hasNext()) {
            PartParam p = it.next();
            if (SectionTypeEnum.getSectionTypeFromId((p.getKey())) == SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER && params.size() > 1) {
                combinMixPos = p.getPosition();
                break;
            }
        }

        if (combinMixPos != 999) {
            sortedReturnParts.addAll(params.subList(0, combinMixPos));
            sortedSupplyParts.addAll(params.subList(combinMixPos, params.size()));
        } else {
            sortedSupplyParts.addAll(params);
        }

        sortList(ahu, sortedReturnParts, ServiceConstant.SYS_ALPHABET_R_UP);
        sortList(ahu, sortedSupplyParts, ServiceConstant.SYS_ALPHABET_S_UP);

        sortedPartLinks.add(sortedSupplyParts);
        sortedPartLinks.add(sortedReturnParts);
        return sortedPartLinks;

    }

    private static void sortList(AhuParam ahu, List<PartParam> sortedParts, String avdirection) {
        // 调整风机位置
        PartParam fan = null;

        Iterator<PartParam> returnList = sortedParts.iterator();
        while (returnList.hasNext()) {
            PartParam part = returnList.next();
            part.getParams().put(ServiceConstant.METASEXON_AIRDIRECTION, avdirection);
            if (SectionTypeEnum.getSectionTypeFromId((part.getKey())) == SectionTypeEnum.TYPE_FAN) {
                fan = part;
                fan.setParamValue(ServiceConstant.SYS_STRING_AIRVOLUME, ahu.getParams().get(ServiceConstant.METAHU_SAIRVOLUME));
                if (ServiceConstant.SYS_ALPHABET_R_UP.equals(avdirection)) {
                    fan.setParamValue(ServiceConstant.SYS_STRING_AIRVOLUME, ahu.getParams().get(ServiceConstant.METAHU_EAIRVOLUME));
                }
                returnList.remove();
            }
        }
        if (fan != null) {
            sortedParts.add(fan);
        }
    }

    /**
     * transfer the tempreture from last part to current part
     *
     * @param lastPart
     * @param currentPart
     * @return
     */
    public static PartParam transferTempreture(PartParam lastPart, PartParam currentPart) {

        if (lastPart == null) {
            return currentPart;
        }

        Double lastSOutDryBulbT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_SOUTDRYBULBT, Double.class);
        Double lastSOutRelativeT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_SOUTRELATIVET, Double.class);
        Double lastSOutWetBulbT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_SOUTWETBULBT, Double.class);
        Double lastWOutDryBulbT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_WOUTDRYBULBT, Double.class);
        Double lastWOutRelativeT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_WOUTRELATIVET, Double.class);
        Double lastWOutWetBulbT = lastPart.getParamValue(ServiceConstant.METACOMMON_STRING_WOUTWETBULBT, Double.class);

        if (EmptyUtil.isNotEmpty(lastSOutDryBulbT)) {
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SINDRYBULBT, lastSOutDryBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SINRELATIVET, lastSOutRelativeT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SINWETBULBT, lastSOutWetBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTDRYBULBT, lastSOutDryBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTRELATIVET, lastSOutRelativeT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTWETBULBT, lastSOutWetBulbT);

        }

        if (EmptyUtil.isNotEmpty(lastWOutDryBulbT)) {
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WINDRYBULBT, lastWOutDryBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WINRELATIVET, lastWOutRelativeT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WINWETBULBT, lastWOutWetBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTDRYBULBT, lastWOutDryBulbT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTRELATIVET, lastWOutRelativeT);
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTWETBULBT, lastWOutWetBulbT);

        }

        return currentPart;

    }

    /**
     * reset output tempreture to input tempreture if the part didn't calculate
     * tempreture.
     *
     * @param currentPart
     * @return
     */
    public static PartParam restPartTempreture(PartParam currentPart) {

        if (currentPart == null) {
            return currentPart;
        }

        Double currentSOutDryBulbT = currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_SOUTDRYBULBT, Double.class);
        Double currentWOutDryBulbT = currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_WOUTDRYBULBT, Double.class);

        if (EmptyUtil.isEmpty(currentSOutDryBulbT)) {
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTDRYBULBT, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_SINDRYBULBT, Double.class));
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTRELATIVET, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_SINRELATIVET, Double.class));
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_SOUTWETBULBT, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_SINWETBULBT, Double.class));
        }

        if (EmptyUtil.isEmpty(currentWOutDryBulbT)) {
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTDRYBULBT, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_WINDRYBULBT, Double.class));
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTRELATIVET, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_WINRELATIVET, Double.class));
            currentPart.setParamValue(ServiceConstant.METACOMMON_STRING_WOUTWETBULBT, currentPart.getParamValue(ServiceConstant.METACOMMON_STRING_WINWETBULBT, Double.class));
        }

        return currentPart;
    }

    /*总阻力计算：包含送风风机 回风风机*/
    public static void resetTotalResistance(AhuParam ahu) {
        String airDirection = ServiceConstant.METASEXON_AIRDIRECTION;//风向
        String eexternalstatic = ServiceConstant.METAHU_EEXTERNALSTATIC;//机外静压(回)
        String sexternalstatic = ServiceConstant.METAHU_SEXTERNALSTATIC;//机外静压(送)
        Map<String, Object> params = ahu.getParams();//机组信息
        List<PartParam> plist = ahu.getPartParams();
        Double rTotalResistance = 0.0;
        Double sTotalResistance = 0.0;
        Iterator<PartParam> iterator = plist.iterator();
        while (iterator.hasNext()) {
            PartParam part = iterator.next();
            Map<String, Object> parts = part.getParams();//获取段属性
            /*风机段：送风机、回风机 最后统一计算阻力*/
            if (SectionTypeEnum.getSectionTypeFromId((part.getKey())) == SectionTypeEnum.TYPE_FAN) {
                continue;
            } else if (SectionTypeEnum.getSectionTypeFromId((part.getKey())) == SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER) {/*新回排风段特殊计算*/
                Double rResistance = part.getParamValue(CommonConstant.SYS_STRING_RRESISTANCE, Double.class);
                if (null != rResistance) {
                    rTotalResistance += rResistance;
                }
                Double iResistance = part.getParamValue(CommonConstant.SYS_STRING_IRESISTANCE, Double.class);
                if (null != iResistance) {
                    sTotalResistance += iResistance;
                }
            } else {/*其他功能段追加阻力*/
                if (parts.containsKey(airDirection)) {
                    String value = BaseDataUtil.constraintString(parts.get(airDirection));
                    if (AirDirectionEnum.RETURNAIR.getCode().equals(value)) {//回风机组
                        Double resistance = part.getParamValue(CommonConstant.SYS_STRING_RESISTANCE_3, Double.class);
                        if (null != resistance) {
                            rTotalResistance += resistance;
                        }
                    } else {//送风机组
                        Double resistance = part.getParamValue(CommonConstant.SYS_STRING_RESISTANCE_3, Double.class);
                        if (null != resistance) {
                            sTotalResistance += resistance;
                        }
                    }
                }
            }

        }
        /*封装送风机、回风机 */
        Iterator<PartParam> partIterator = plist.iterator();
        while (partIterator.hasNext()) {
            PartParam part = partIterator.next();
            Map<String, Object> parts = part.getParams();//获取段属性
            if (parts.containsKey(airDirection)) {
                if (SectionTypeEnum.getSectionTypeFromId((part.getKey())) == SectionTypeEnum.TYPE_FAN) {
                    String airValue = BaseDataUtil.constraintString(parts.get(airDirection));
                    Double extraStatic = part.getParamValue(ServiceConstant.SYS_STRING_EXTRASTATIC, Double.class);//其他压降
                    if (null == extraStatic) {
                        extraStatic = new Double(0.0);
                    }
                    double eexternalstaticValue = 0.0;
                    if (params.containsKey(eexternalstatic)) {//获取机组 机外静压(回)
                        eexternalstaticValue = BaseDataUtil.objectToDouble(params.get(eexternalstatic));
                    }
                    double sexternalstaticValue = 0.0;
                    if (params.containsKey(sexternalstatic)) {//获取机组 机外静压(送)
                        sexternalstaticValue = BaseDataUtil.objectToDouble(params.get(sexternalstatic));
                    }
                    if (AirDirectionEnum.RETURNAIR.getCode().equals(airValue)) {
                        part.setParamValue(ServiceConstant.SYS_STRING_SYSSTATIC, BaseDataUtil.decimalConvert(rTotalResistance, 1));//系统压降
                        part.setParamValue(ServiceConstant.SYS_STRING_EXTERNALSTATIC, BaseDataUtil.decimalConvert(eexternalstaticValue, 1));//余压
                        part.setParamValue(ServiceConstant.SYS_STRING_TOTALSTATIC, BaseDataUtil.decimalConvert(eexternalstaticValue + extraStatic + rTotalResistance, 1));//总静压
                    } else {
                        part.setParamValue(ServiceConstant.SYS_STRING_SYSSTATIC, BaseDataUtil.decimalConvert(sTotalResistance, 1));//系统压降
                        part.setParamValue(ServiceConstant.SYS_STRING_EXTERNALSTATIC, BaseDataUtil.decimalConvert(sexternalstaticValue, 1));//余压
                        part.setParamValue(ServiceConstant.SYS_STRING_TOTALSTATIC, BaseDataUtil.decimalConvert(sexternalstaticValue + extraStatic + sTotalResistance, 1));//总静压
                    }
                } else {
                    continue;
                }
            }
        }
    }
}
