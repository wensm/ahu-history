package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.dao.ProjectDao;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.constant.ServiceConstant;

import com.carrier.ahu.util.EmptyUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_RECORDSTATUS_ARCHIEVED;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_RECORDSTATUS_UNARCHIEVED;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class ProjectServiceImpl extends AbstractService implements ProjectService {

	@Autowired
	ProjectDao projectDao;

	@Override
	public String addProject(Project project,String userName) {
		project.setCreateInfo(userName);
		projectDao.save(project);
		return project.getPid(); 
	}

	@Override
	public boolean exists(String no) {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> noPath = root.get(ServiceConstant.SYS_MAP_NO);
				query.where(cb.equal(noPath,no));
				return null;
			}
		});
		return CollectionUtils.isNotEmpty(list);
	}

	@Override
	public Project getProjectById(String pid) {
		return projectDao.findOne(pid);
	}

	@Override
	public List<Project> findProjectList() {
		List<Project> list = (List<Project>)projectDao.findAll();
		return list;
	}
	@Override
	public List<Project> findProjectListArchieved() {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_RECORDSTATUS), SYS_STRING_RECORDSTATUS_ARCHIEVED);
				query.where(p1);
				return null;
			}
		});
		return list;
	}
	@Override
	public List<Project> findProjectListUnArchieved() {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.notEqual(root.get(ServiceConstant.SYS_STRING_RECORDSTATUS), SYS_STRING_RECORDSTATUS_ARCHIEVED);
				query.where(p1);
				return null;
			}
		});
		return list;
	}

	@Override
	public String updateProject(Project project,String userName) {
		project.setUpdateInfo(userName);
		projectDao.save(project);
		return project.getPid();
	}

	@Override
	public void deleteProject(String pid) {
		Project project = projectDao.findOne(pid);;
		projectDao.delete(project);
	}
}
