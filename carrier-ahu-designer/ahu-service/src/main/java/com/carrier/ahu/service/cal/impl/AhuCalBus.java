package com.carrier.ahu.service.cal.impl;

import static com.carrier.ahu.common.intl.I18NConstants.AHU_CALCULATION_FINISHED;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.GET_ENGINE_LIST;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CALCULATION_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.TEMPERATURE_TRANSFER_FINISH;
import static com.carrier.ahu.common.intl.I18NConstants.TEMPERATURE_TRANSFER_START;
import static com.carrier.ahu.vo.SystemCalculateConstants.UNDERSCORE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.enums.*;
import com.carrier.ahu.unit.BaseDataUtil;
import org.apache.commons.lang3.ArrayUtils;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.IAhuCalBus;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AhuCalBus implements IAhuCalBus {

	private CalOperationEnum calOperation = CalOperationEnum.BATCH;
	@SuppressWarnings("unused")
	private CalcTypeEnum calcType = CalcTypeEnum.ECONOMICAL;
	private static AhuCalBus instance = new AhuCalBus();

	private List<ICalEngine> engineList = new ArrayList<>();

	public static AhuCalBus getInstance() {
		return instance;
	}

	// @Override
	// public void calculateLengthAndWeight(AhuParam ahu, PartJob partJob) {
	// Iterator<PartParam> it = ahu.getPartParams().iterator();
	// while (it.hasNext()) {
	// PartParam section = it.next();
	// LengthParam lengthParam =
	// CalculateParamFactory.generateLengthParam(section);
	// section.getParams().put("cal.length",
	// String.valueOf(lengthService.getLength(lengthParam)));
	// ResistanceParam resistanceParam =
	// CalculateParamFactory.generateResistanceParam(section);
	// section.getParams().put("cal.resistance",
	// String.valueOf(lengthService.getLength(lengthParam)));
	// }
	// }

	// Update Weight 属性到属性Map
	@SuppressWarnings("unused")
	private static void innerUpdatePartWeight(PartParam part, double total) {
		String key = part.getKey();
		part.getParams().put(MetaCodeGen.calculateAttributePrefix(key) + ServiceConstant.METACOMMON_POSTFIX_WEIGHT, String.valueOf(total));
	}

	// /**
	// * 段长和阻力计算 [单层过滤段]
	// *
	// * @param serial
	// * @param map
	// * @param result
	// * @throws BadRequestException
	// */
	// private void sectionCalculatorSingle(String serial, Map<String, String>
	// map, Map<String, Object> result)
	// throws BadRequestException {
	// // 过滤器形式(外抽、板式、袋式)
	// String filterF = map.get("meta.section.filter.fitetF");
	// // 过滤器抽取方式（正抽、侧抽）
	// String mediaLoading = map.get("meta.section.filter.mediaLoading");
	// // 过滤器效率
	// String filterEfficiency =
	// map.get("meta.section.filter.filterEfficiency");
	// // 风量
	// double airVolume = getAirVolume(map);
	//
	// if (null == filterF || null == mediaLoading || null == filterEfficiency)
	// {
	// throw new BadRequestException("过滤段：过滤器形式或抽取方式或过滤器效率为空");
	// }
	//
	// int itemLength = SectionCalculatorUtil.sectionLengthSimpleFilter(filterF,
	// mediaLoading, filterEfficiency);
	// result.put("meta.section.filter.sectionL", itemLength);
	//
	// String fitlerStandard = map.get("meta.section.filter.fitlerStandard");
	//
	// String bagLength = map.get("meta.section.filter.bagLength");
	//
	// FilterResistanceItem filterResistanceItem =
	// SectionCalculatorUtil.sectionResistanceSimpleFilter(airVolume, serial,
	// bagLength, filterF, itemLength, fitlerStandard, filterEfficiency);
	// if (EmptyUtil.isNotEmpty(filterResistanceItem)) {
	// result.put("meta.section.filter.dinitialPD",
	// filterResistanceItem.getBeginResi());
	// result.put("meta.section.filter.deveragePD",
	// filterResistanceItem.getAverResi());
	// result.put("meta.section.filter.dfinalPD",
	// filterResistanceItem.getEndResi());
	// } else {
	// result.put("meta.section.filter.dinitialPD", -1d);
	// result.put("meta.section.filter.deveragePD", -1d);
	// result.put("meta.section.filter.dfinalPD", -1d);
	// }
	// }

	@SuppressWarnings("unused")
	private double getAirVolume(Map<String, String> map) {
		double airVolume = 18000;
		String sAirVolume = map.get(ServiceConstant.METAHU_SAIRVOLUME);
		if (EmptyUtil.isNotEmpty(sAirVolume)) {
			airVolume = Double.valueOf(sAirVolume);
		}
		return airVolume;
	}

	// /**
	// * 段长和阻力计算 [综合过滤段]
	// *
	// * @param serial
	// * @param map
	// * @param result
	// */
	// private void sectionCalculatorComposite(String serial, Map<String,
	// String> map, Map<String, Object> result) {
	// String filterF = map.get("meta.section.combinedFilter.filterF");
	// String mediaLoading =
	// map.get("meta.section.combinedFilter.mediaLoading");
	// String RMaterialE = map.get("meta.section.combinedFilter.RMaterialE");//
	// 袋式过滤效率
	// int itemLength = SectionCalculatorUtil.sectionLengthSimpleFilter(filterF,
	// mediaLoading, RMaterialE);
	// // 综合过滤段 段长 = 袋式过滤器的袋长
	// result.put("meta.section.combinedFilter.sectionL", itemLength);
	// double airVolume = getAirVolume(map);
	// String fitlerStandard =
	// map.get("meta.section.combinedFilter.fitlerStandard");// 袋式过滤效率
	// String bagLength = map.get("meta.section.combinedFilter.rimThick");//
	// 边框厚度
	// FilterResistanceItem bagItem =
	// SectionCalculatorUtil.sectionResistanceSimpleFilter(airVolume, serial,
	// null, "bag",
	// itemLength, fitlerStandard, RMaterialE);
	// result.put("meta.section.combinedFilter.initialPDP",
	// bagItem.getBeginResi());
	// result.put("meta.section.combinedFilter.everagePDP",
	// bagItem.getAverResi());
	// result.put("meta.section.combinedFilter.finalPDP", bagItem.getEndResi());
	//
	// String LMaterialE = map.get("meta.section.combinedFilter.LMaterialE");//
	// 板式过滤效率
	// FilterResistanceItem panelItem =
	// SectionCalculatorUtil.sectionResistanceSimpleFilter(airVolume, serial,
	// bagLength, "panel",
	// itemLength, fitlerStandard, LMaterialE);
	// result.put("meta.section.combinedFilter.initialPDB",
	// panelItem.getBeginResi());
	// result.put("meta.section.combinedFilter.everagePDB",
	// panelItem.getAverResi());
	// result.put("meta.section.combinedFilter.finalPDB",
	// panelItem.getEndResi());
	// }

	/**
	 * 段长和阻力计算 [冷水盘管段]
	 *
	 * @param serial
	 * @param map
	 * @param result
	 */
	// private void sectionCalculatorCold(String serial, Map<String, String>
	// map, Map<String, Object> result) {
	// String eliminator = map.get("meta.section.coolingCoil.eliminator");
	// result.put("meta.section.coolingCoil.sectionL",
	// SectionCalculatorUtil.sectionLengthCoolingCoil(serial, eliminator));
	// // TODO -Simon 冷水盘管段 阻力确认
	// result.put("meta.section.coolingCoil.resistance", random());
	// // TODO -Simon 冷水盘管段 重量确认
	// result.put("meta.section.coolingCoil.runningWeight",
	// NumberUtil.scale(random(), 0));
	// // TODO -Simon 冷水盘管段 阻力确认
	// result.put("meta.section.coolingCoil.driftEliminatorResistance",
	// NumberUtil.scale(random(), 0));
	// }

	/**
	 * 段长和阻力计算 [风机]
	 *
	 * @param serial
	 * @param map
	 * @param result
	 */
	// private void sectionCalculatorFan(String serial, Map<String, String> map,
	// Map<String, Object> result) {
	// // TODO -Simon 风机阻力计算
	// result.put("meta.section.fan.Resistance", NumberUtil.scale(random(), 2));
	// String airdir = map.get("meta.section.fan.outletDirection");// 风机出风方向
	// // TODO -Simon 风机型号 是否是这个？？？？ 等待建峰答复
	// String fantype = map.get("meta.section.fan.fanModel");
	// String outlet = map.get("meta.section.fan.outlet");// 风机形式
	// String position = map.get("meta.section.fan.motorPosition");// 电机位置
	// boolean standbyMotorFlag =
	// Boolean.getBoolean(map.get("meta.section.fan.standbyMotor"));// 备用电机
	// String ID = "1";// id ：1.为标准供应商 属性来源-页面选择 2.占时没用到
	// result.put("meta.section.fan.sectionL",
	// SectionCalculatorUtil.sectionLengthFan(serial, fantype, airdir, outlet,
	// position, standbyMotorFlag, ID));
	// }

	/**
	 * 段长和阻力计算 [混合段]
	 *
	 * @param serial
	 * @param map
	 * @param result
	 */
	// private void sectionCalculatorMix(String serial, Map<String, String> map,
	// Map<String, Object> result) {
	// String returnTop = null == map.get("meta.section.mix.returnTop") ?
	// "false"
	// : String.valueOf(map.get("meta.section.mix.returnTop"));// 顶部回风
	// String returnBack = null == map.get("meta.section.mix.returnBack") ?
	// "false"
	// : String.valueOf(map.get("meta.section.mix.returnBack"));// 后回风
	// String returnLeft = null == map.get("meta.section.mix.returnLeft") ?
	// "false"
	// : String.valueOf(map.get("meta.section.mix.returnLeft"));// 左侧回风
	// String returnRight = null == map.get("meta.section.mix.returnRight") ?
	// "false"
	// : String.valueOf(map.get("meta.section.mix.returnRight"));// 右侧回风
	// String returnButtom = null == map.get("meta.section.mix.returnButtom") ?
	// "false"
	// : String.valueOf(map.get("meta.section.mix.returnButtom"));// 底部回风
	//
	// Integer sectionL = -1;
	// double airVolume = getAirVolume(map);
	//
	// if (Boolean.parseBoolean(returnTop) ||
	// Boolean.parseBoolean(returnButtom)) {
	// sectionL = SectionCalculatorUtil.SectionLengthMix(airVolume, serial, "1",
	// false);
	// }
	// if (Boolean.parseBoolean(returnBack)) {
	// int sectionL1 = SectionCalculatorUtil.SectionLengthMix(airVolume, serial,
	// "2", true);
	// if (sectionL1 != -1) {
	// if (sectionL > sectionL1) {
	// sectionL = sectionL1;
	// }
	// }
	// }
	// if (Boolean.parseBoolean(returnRight) ||
	// Boolean.parseBoolean(returnLeft)) {
	// int sectionL1 = SectionCalculatorUtil.SectionLengthMix(airVolume, serial,
	// "3", false);
	// if (sectionL1 != -1) {
	// if (sectionL > sectionL1) {
	// sectionL = sectionL1;
	// }
	// }
	// }
	// result.put("meta.section.mix.sectionL", sectionL);
	//
	// String damperOutlet = map.get("meta.section.mix.damperOutlet");//
	// 风口接件,风阀材料
	//
	// result.put("meta.section.mix.Resistance",
	// SectionCalculatorUtil.sectionResistanceMix(airVolume, serial, sectionL,
	// damperOutlet));
	// }

	/**
	 * 段长和阻力计算 [高效过滤段]
	 *
	 * @param serial
	 * @param map
	 * @param result
	 */
	// private void sectionCalculatorHEPAFilter(String serial, Map<String,
	// String> map, Map<String, Object> result) {
	// result.put("meta.section.HEPAFilter.sectionL", 9);
	//
	// String filterF = map.get("meta.section.HEPAFilter.FilterF");
	// String fitlerStandard = map.get("meta.section.HEPAFilter.MaterialE");
	// double airVolume = getAirVolume(map);
	// HEPAFilterResistanceItem item =
	// SectionCalculatorUtil.sectionResistanceHEPAFilter(airVolume, serial,
	// filterF,
	// fitlerStandard);
	// if (EmptyUtil.isEmpty(item)) {
	// result.put("meta.section.HEPAFilter.AverResi", -1d);
	// result.put("meta.section.HEPAFilter.BeginResi", -1d);
	// result.put("meta.section.HEPAFilter.EndResi", -1d);
	// } else {
	// result.put("meta.section.HEPAFilter.AverResi", item.getAverResi());
	// result.put("meta.section.HEPAFilter.BeginResi", item.getBeginResi());
	// result.put("meta.section.HEPAFilter.EndResi", item.getEndResi());
	// }
	//
	// }

	/**
	 * 段长和阻力计算 [静电过滤段]
	 *
	 */
	// private void sectionCalculatorElectrostaticFilter(String serial,
	// Map<String, String> map,
	// Map<String, Object> result) {
	// double airVolume = getAirVolume(map);
	// result.put("meta.section.electrostaticFilter.sectionL", 6);
	// // 查表[静电过滤器阻力]，根据风速WindSpeed为条件，大于等于步骤1中[velocity]
	// // 的最小值，即为平均阻力和终阻力值。不需要再进行后续计算。
	// ElectostaticFilterResistanceItem item =
	// SectionCalculatorUtil.sectionResistanceElectostaticFilter(airVolume,
	// serial);
	// if (EmptyUtil.isEmpty(item)) {
	// result.put("meta.section.electrostaticFilter.AverResi", -1d);
	// result.put("meta.section.electrostaticFilter.BeginResi", -1d);
	// result.put("meta.section.HEPAFilter.EndResi", -1d);
	// } else {
	// result.put("meta.section.electrostaticFilter.AverResi",
	// item.getAverResi());
	// result.put("meta.section.electrostaticFilter.BeginResi",
	// item.getAverResi());
	// result.put("meta.section.electrostaticFilter.EndResi",
	// item.getAverResi());
	// }
	// }

    // @formatter:off
    //	protected List<PartParam> tunePartOrder(AhuParam ahuParam) {
    //		List<PartParam> parts = ahuParam.getPartParams();
    //		if (parts.size() < 2) {
    //			return parts;
    //		}
    //		Layout layout = ahuParam.getLayout();
    //		if (EmptyUtil.isEmpty(layout)) {
    //			return parts;
    //		}
    //		if (layout == null || layout.getStyle() == Layout.STYLE_PLAIN) {
    //			return getPlainSequence(parts);
    //		} else if (layout.getStyle() == Layout.STYLE_PLATE) {
    //			return getPlateSequence(parts, layout);
    //		} else if (layout.getStyle() == Layout.STYLE_WHEEL) {
    //			return getWheelSequence(parts, layout);
    //		}
    //
    //		return parts;
    //
    //	}
	/**
	 * 轮式机组，计算顺序为：<br>
	 * 左上，右下；两组中的ahu.fan被调整到两组的最后<br>
	 * 右下,左上; 两组中的ahu.fan被调整到两组的最后<br>
	 * 
	 * @param parts
	 * @return
	 */
    //	private List<PartParam> getWheelSequence(List<PartParam> parts, Layout layout) {
    //		int[][] ldata = layout.getLayoutData();
    //		List<PartParam> result = new ArrayList<>();
    //		int[] sequence1 = new int[] { 0, 3 };
    //		int[] sequence2 = new int[] { 1, 2 };
    //		int[][] tmpArr = new int[][] { sequence1, sequence2 };
    //		for (int k = 0; k < tmpArr.length; k++) {
    //			int[] seq = tmpArr[k];
    //			List<PartParam> fanParts = new ArrayList<>();
    //			for (int i = 0; i < seq.length; i++) {
    //				int[] line = ldata[i];
    //				for (int j = 0; j < line.length; j++) {
    //					if (line[j] >= parts.size()) {
    //						log.error(String.format("Get wrong layout data, the position ? vs the part size ?", j,
    //								parts.size()));
    //						continue;
    //					}
    //					PartParam tmpPart = parts.get(j);
    //					if (SectionTypeEnum.TYPE_FAN.getId().equals(tmpPart.getKey())) {
    //						fanParts.add(tmpPart);
    //					} else {
    //						result.add(tmpPart);
    //					}
    //				}
    //			}
    //			result.addAll(fanParts);
    //		}
    //
    //		return result;
    //	}

	/**
	 * 板式机组，计算顺序为左上，右上，右下，左下
	 * 
	 * @param parts
	 * @return
	 */
    //	private List<PartParam> getPlateSequence(List<PartParam> parts, Layout layout) {
    //		int[][] ldata = layout.getLayoutData();
    //		List<PartParam> result = new ArrayList<>();
    //		int[] sequence = new int[] { 0, 1, 3, 2 };
    //		List<PartParam> fanParts = new ArrayList<>();
    //		for (int i = 0; i < sequence.length; i++) {
    //			int[] line = ldata[i];
    //			for (int j = 0; j < line.length; j++) {
    //				if (line[j] >= parts.size()) {
    //					log.error(
    //							String.format("Get wrong layout data, the position ? vs the part size ?", j, parts.size()));
    //					continue;
    //				}
    //				PartParam tmpPart = parts.get(j);
    //				if (SectionTypeEnum.TYPE_FAN.getId().equals(tmpPart.getKey())) {
    //					fanParts.add(tmpPart);
    //				} else {
    //					result.add(tmpPart);
    //				}
    //			}
    //		}
    //		result.addAll(fanParts);
    //		return result;
    //	}

	/**
	 * 按照业务逻辑，重新排序段的计算顺序。 普通机组，需要把ahu.fan放到最后计算
	 * 
	 * @param parts
	 * @return
	 */
    //	private List<PartParam> getPlainSequence(List<PartParam> parts) {
    //		List<PartParam> result = new ArrayList<>();
    //		List<PartParam> fanParts = new ArrayList<>();
    //		Iterator<PartParam> it = parts.iterator();
    //		while (it.hasNext()) {
    //			PartParam part = it.next();
    //			String key = part.getKey();
    //			if (SectionTypeEnum.TYPE_FAN.getId().equals(key)) {
    //				fanParts.add(part);
    //			} else {
    //				result.add(part);
    //			}
    //		}
    //		result.addAll(fanParts);
    //		return result;
    //	}

    // @formatter:on
	/**
	 * 注册一个新的计算引擎
	 * 
	 * @param calEngine
	 */
	public void registerCalEngine(ICalEngine calEngine) {

		this.engineList.add(calEngine);
		Collections.sort(this.engineList, new Comparator<ICalEngine>() {

			@Override
			public int compare(ICalEngine o1, ICalEngine o2) {
				return o1.getOrder() - o2.getOrder();
			}
		});

	}

	/**
	 * 输入段的类型，返回顺序的计算引擎
	 * 
	 * 过滤掉机组系列在Disabled列表里的引擎。
	 *
	 * @param sectionKey
	 * @param unitModel '39CQ'
	 * @param isNS
	 * @return
	 */
    public List<ICalEngine> getOrderedEngineArr(String sectionKey, String unitModel, String isNS) {
        String unitSeries = AhuUtil.getUnitSeries(unitModel);
        List<ICalEngine> list = new ArrayList<>();
        Iterator<ICalEngine> it = this.engineList.iterator();
        while (it.hasNext()) {
            ICalEngine engine = it.next();

			//点击前端非标计算按钮只进行非标计算引擎调用，其他引擎跳过
			if("true".equals(isNS)){
				//非标价格引擎
				if(ICalEngine.ID_NS_PRICE.equals(engine.getKey())) {
					list.add(engine);
					break;
				}
            }else{
				if (!ICalEngine.ID_NS_PRICE.equals(engine.getKey()) //非标价格引擎，不参与其他计算线路
						&& ArrayUtils.contains(engine.getEnabledSectionIds(), sectionKey)
						&& !ArrayUtils.contains(engine.getDisabledUnitSeries(), unitSeries)) {
					list.add(engine);
				}
            }
        }
        return list;
    }

	@Override
	public void calculate(AhuParam ahu, LanguageEnum language, ICalContext context) throws Exception {

        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(AHU_CALCULATION_STARTED, language, ahu.getUnitNo(), ahu.getDrawingNo()));
		DefaultCalContext subContext = new DefaultCalContext();
		subContext.setText(ICalContext.NAME_AHU);
		subContext.setName(ahu.getName());
		subContext.setCalOperation(context.getCalOperation());
		subContext.setCalcTypeEnum(context.getCalType());
		subContext.setUserConfigParam(context.getUserConfigParam());
		context.enter(subContext);
		if (this.calOperation == CalOperationEnum.BATCH) {
			List<List<PartParam>> list = CalLineSupport.sortedParts(ahu);
			for (List<PartParam> plist : list) {
				Iterator<PartParam> it = plist.iterator();
				PartParam lastPart = null;
				while (it.hasNext()) {
					PartParam param = it.next();
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                            I18NBundle.getString(TEMPERATURE_TRANSFER_START, language, ahu.getUnitNo()));
					UserMeta userMeta = AHUContext.getUserMeta(UserMetaEnum.TEMPERATURE_TRANSMIT);
					if (!EmptyUtil.isEmpty(userMeta)) {//根据用户设置是否启用温度传递
						if(BaseDataUtil.stringConversionBoolean(userMeta.getMetaValue())){
							CalLineSupport.transferTempreture(lastPart, param);
						}
					}
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                            I18NBundle.getString(TEMPERATURE_TRANSFER_FINISH, language, ahu.getUnitNo()));
					innerCallEngine(ahu, param, language, subContext);
					lastPart = param;
				}
			}
            CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                    I18NBundle.getString(AHU_CALCULATION_FINISHED, language, ahu.getUnitNo(), ahu.getDrawingNo()));
			subContext.setSuccess(true);
			return;
		}

		Iterator<PartParam> it = ahu.getPartParams().iterator();
		while (it.hasNext()) {
			PartParam param = it.next();
			innerCallEngine(ahu, param, language, subContext);
		}
	}

	private void innerCallEngine(AhuParam ahu, PartParam param, LanguageEnum language, ICalContext context) throws Exception {


		param.getParams().put(SystemCalculateConstants.META_SECTION_COMPLETED, true);// 每次计算重置状态
		DefaultCalContext subContext = new DefaultCalContext();
		subContext.setText(ICalContext.NAME_PART);
		subContext.setName(param.getKey());
		subContext.setCalcType(context.getCalType());
		subContext.setUserConfigParam(context.getUserConfigParam());
		context.enter(subContext);
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(SECTION_CALCULATION_STARTED, language, ahu.getUnitNo(), param.getKey(), param.getOrders()));
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(GET_ENGINE_LIST, language, ahu.getUnitNo()));
		Iterator<ICalEngine> it = getOrderedEngineArr(String.valueOf(param.getKey()), ahu.getSeries(),ahu.getIsNS()).iterator();
		boolean successful = true;

		while (it.hasNext()) {
			ICalEngine engine = it.next();
			try {
				// comfirm按钮时，不能进行的操作不加进计算bus
				if (this.calOperation.getId() < engine.getCalLevel()) {
					log.debug("Ignore batch calculation engine " + engine.getKey());
					continue;
				}
                CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                        I18NBundle.getString(ENGIN_CALCULATION_STARTED, language, ahu.getUnitNo(), engine.getKey()));
                engine.cal(ahu, param, subContext);
                CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                        I18NBundle.getString(ENGIN_CALCULATION_SUCCESS, language, ahu.getUnitNo(), engine.getKey()));
			} catch (AhuException e) {
                // 当一个engine计算失败时，退出当前的段的计算，因为前后引擎之间存在依赖关系
                successful = false;
                if (e.getMessage().contains(UNDERSCORE)) {
                    String message = I18NBundle.getString(e.getMessage(), language, e.getParams());
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(), message);
                    log.error(String.format("Failed to call %s", engine.getKey()), message);
                    if (this.calOperation.getId() == CalOperationEnum.COMFIRM.getId()) {
                        throw new AhuException(message);
                    }
                } else {
                    String message = I18NBundle.getString(ENGIN_CALCULATION_FAILED, language, ahu.getUnitNo(),
                            engine.getKey());
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(), message);
                    if (this.calOperation.getId() == CalOperationEnum.COMFIRM.getId()) {
                        throw new AhuException(message);
                    }
                    log.error(String.format("Failed to call %s", engine.getKey()), e);
                }
                break;// 批量计算遇到报错的引擎，后面的所有已经不用计算。
            }
		}
		// 完成状态到已完成
		subContext.setSuccess(successful);
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(SECTION_CALCULATION_SUCCESS, language, ahu.getUnitNo(), param.getKey(), param.getOrders()));
	}

	@Override
	public void setCalOperation(CalOperationEnum operation) {
		this.calOperation = operation;

	}

	@Override
	public void setCalType(CalcTypeEnum calType) {
		this.calcType = calType;
	}

}
