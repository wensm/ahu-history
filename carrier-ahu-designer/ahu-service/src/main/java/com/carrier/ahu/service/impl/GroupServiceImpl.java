package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wen zhengtao on 2017/3/25.
 */
@Service
public class GroupServiceImpl extends AbstractService implements GroupService {
	@Override
	public String addGroup(GroupInfo group, String userName) {
		group.setCreateInfo(userName);
		groupDao.save(group);
		return group.getGroupId();
	}

	@Override
	public List<String> addGroups(List<GroupInfo> groupList) {
		groupDao.save(groupList);
		List<String> list = new ArrayList<>();
		for (GroupInfo u : groupList) {
			list.add(u.getGroupId());
		}
		return list;
	}

	@Override
	public String updateGroup(GroupInfo group, String userName) {
		group.setUpdateInfo(userName);
		groupDao.save(group);
		return group.getGroupId();
	}

	@Override
	public void deleteGroup(String groupId) {
		groupDao.delete(groupId);
	}

	@Override
	public GroupInfo findGroupById(String groupId) {
		GroupInfo group = groupDao.findOne(groupId);
		return group;
	}

	@Override
	public List<GroupInfo> findGroupList(String projectId) {
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> projectIdPath = root.get(ServiceConstant.SYS_STRING_PROJECTID);// 项目ID
				query.where(cb.equal(projectIdPath, projectId));
				return null;
			}
		});
		return list;
	}

	@Override
	public boolean checkName(String groupName) {
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> groupNamePath = root.get(ServiceConstant.SYS_STRING_GROUPNAME);
				query.where(cb.equal(groupNamePath, groupName));
				return null;
			}
		});
		if (list.isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkName(String projectId, String groupName) {
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPNAME), groupName);
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		if (list.isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public Iterable<GroupInfo> findAll() {
		return groupDao.findAll();
	}

	@Override
	public List<GroupInfo> findGroupListByType(String projectId, String groupType) {
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPTYPE), groupType);
				if (EmptyUtil.isNotEmpty(groupType) && !GroupTypeEnum.TYPE_BLANK.getCnName().equals(groupType)) {
					query.where(cb.and(p1, p2));
				} else {
					query.where(p1);
				}
				return null;
			}
		});
		return list;
	}

	@Override
	public List<GroupInfo> findGroupListByCode(String projectId, String groupCode) {
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPCODE), groupCode);
				if (EmptyUtil.isEmpty(projectId)) {
					if (null == groupCode) {
						return null;
					} else {
						query.where(p2);
					}
				} else {
					if (null == groupCode) {
						query.where(p1);
					} else {
						query.where(cb.and(p1, p2));
					}
				}

				return null;
			}
		});
		return list;
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<GroupInfo> list = groupDao.findAll(new Specification<GroupInfo>() {
			@Override
			public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (GroupInfo bean : list) {
				idList.add(bean.getGroupId());
			}
		}
		return idList;
	}
}
