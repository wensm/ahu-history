package com.carrier.ahu.service.service.impl;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.price.*;
import com.carrier.ahu.metadata.entity.report.SKAbsorber;
import com.carrier.ahu.param.NSPriceParam;
import com.carrier.ahu.price.NSPriceUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.PriceService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class PriceServiceImpl implements PriceService {

    private static Logger logger = LoggerFactory.getLogger(PriceServiceImpl.class.getName());

    @Autowired
    private AhuService ahuService;

    @Autowired
    private PartitionService partitionService;

    @Override
    public double getPrice(LengthParam lengthParam) {
        return 0;
    }

    /**
     * 获取非标价格
     *
     * @param nsPriceParam
     * @return
     */
    @Override
    public Map<String, String> getNSPrice(NSPriceParam nsPriceParam) {
        Map<String, String> nsPriceMap = new HashMap<String, String>();
        String ahuId = nsPriceParam.getUnitid();
        String sectionType = nsPriceParam.getSectionType();
        String serial = AhuUtil.getUnitSeries(nsPriceParam.getSerial());
        try {
            NSPriceUtil nsPriceUtil = new NSPriceUtil();
            if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_section_wetfilmHumidifier_nsChangeSupplier())) {
//                    calAccessNsPrice();
                }
            } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(sectionType)) {
                if (ServiceConstant.SYS_ASSERT_TRUE
                        .equals(nsPriceParam.getNs_section_wetfilmHumidifier_nsChangeSupplier())) {
                    int thinkness = new Double(nsPriceParam.getThickness()).intValue();
                    NonstdWet nsWet = nsPriceUtil.getWetNsPrice(SystemCountUtil.getUnit(nsPriceParam.getSerial()),
                            ServiceConstant.SYS_BLANK + thinkness);
                    if (null != nsWet) {
                        double cj = Double.parseDouble(nsWet.getTp()) - Double.parseDouble(nsWet.getOldPrice());
                        nsPriceMap.put(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE, String.valueOf(cj));
                    }
                }
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(sectionType)) {
                if (ServiceConstant.SYS_ASSERT_TRUE
                        .equals(nsPriceParam.getNs_section_sprayHumidifier_nsChangeSupplier())) {
                    List<NonstdHigh> nshighs = nsPriceUtil.getAllHighNsPrice();
                    Collections.sort(nshighs, new Comparator<NonstdHigh>() {
                        public int compare(NonstdHigh o1, NonstdHigh o2) {
                            return new Integer(o1.getSprayV()).compareTo(new Integer(o2.getSprayV()));
                        }
                    });
                    for (NonstdHigh nshigh : nshighs) {
                        if (nshigh.getSprayV() >= nsPriceParam.getHq()) {
                            double cj = Double.parseDouble(nshigh.getTp()) - Double.parseDouble(nshigh.getOldPrice());
                            nsPriceMap.put(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE, String.valueOf(cj));
                            break;
                        }
                    }
                }
            } else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode().equals(sectionType)) {

            } else if (SectionTypeEnum.TYPE_FAN.getCode().equals(sectionType)) {
                double nsFanPrice = 0;
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_section_fan_nsChangeFan())) {
                    String oldUnit = nsPriceParam.getFanModel()
                            .replace(ServiceConstant.JSON_FAN_FANMODEL_FC, ServiceConstant.JSON_FAN_FANMODEL_KTQ)
                            .replace(ServiceConstant.JSON_FAN_FANMODEL_BC, ServiceConstant.JSON_FAN_FANMODEL_KTH);// 前弯fc后弯bc替换为ktq,kth;
                    String supplier = nsPriceParam.getNs_section_fan_nsFanSupplier();
                    String nsFanModel = nsPriceParam.getNs_section_fan_nsFanModel();
                    NonstdFan nonstdFan = AhuMetadata.findOne(NonstdFan.class, oldUnit, supplier, nsFanModel);
                    LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                    if (EmptyUtil.isNotEmpty(nonstdFan)) {
                        if (EmptyUtil.isNotEmpty(lpRate)) {
                            nsFanPrice = this.calculateNsPrice(nonstdFan, lpRate);
                        } else {
                            log.error("LPRate for - [{}] has not found.", serial);
                            throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                        }
                    } else {
                        log.error("Non-std fan price for - [{}, {}, {}] has not found.", oldUnit, supplier, nsFanModel);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_FAN_PRICE, String.valueOf(nsFanPrice));

                double nsMotorPrice = 0;
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_section_fan_nsChangeMotor())) {
                    String gl = nsPriceParam.getNs_section_fan_nsMotorPower();
                    String js = nsPriceParam.getNs_section_fan_nsMmotorPole();
                    String type = this.getMotorTypeCode(nsPriceParam.getType());
                    String supplier = this.getFanSupplierCode(nsPriceParam.getSupplier());
                    NonstdMotor nonstdMotor = null;
                    LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                    if (ServiceConstant.SYS_ALPHABET_C.equals(supplier)
                            && ServiceConstant.SYS_ALPHABET_D.equals(type)) {
                        nonstdMotor = AhuMetadata.findOne(NonstdMotor.class, gl, js, "IP55", "F",
                                ServiceConstant.SYS_ALPHABET_C, ServiceConstant.SYS_ALPHABET_B);
                    } else {
                        nonstdMotor = AhuMetadata.findOne(NonstdMotor.class, gl, js, "IP55", "F", supplier, type);
                    }
                    if (EmptyUtil.isNotEmpty(nonstdMotor)) {
                        if (EmptyUtil.isNotEmpty(lpRate)) {
                            nsMotorPrice = this.calculateNsPrice(nonstdMotor, lpRate);
                        } else {
                            log.error("LPRate for - [{}] has not found.", serial);
                            throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                        }
                    } else {
                        log.error("Non-std Motor price for - [{}, {}, {}, {}] has not found.", gl, js, type, supplier);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_MOTOR_PRICE, String.valueOf(nsMotorPrice));

                // ns spring price calculation
                double nsSpringtransformPrice = 0;
                String nsSpringtransform = nsPriceParam.getNs_section_fan_nsSpringtransform();
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsSpringtransform)) {
                    String absorberType = getAbsorberType(nsPriceParam.getFanModel());
                    String motorBaseNo = nsPriceParam.getMotorBaseNo();
//                    SKAbsorber skAbsorber = AhuMetadata.findOne(SKAbsorber.class, absorberType, motorBaseNo);
                    List<SKAbsorber> absorberList = AhuMetadata.findAll(SKAbsorber.class);
                    SKAbsorber skAbsorber = new SKAbsorber();
                    for (SKAbsorber skAbsorberTemp : absorberList) {
                        if (absorberType.equals(skAbsorberTemp.getFan()) && skAbsorberTemp.getMachineSiteNo().contains(motorBaseNo)) {
                            skAbsorber = skAbsorberTemp;
                            break;
                        }
                    }
                    if (EmptyUtil.isNotEmpty(skAbsorber)) {
                        List<String> absorbers = Arrays.asList(skAbsorber.getA(), skAbsorber.getB(), skAbsorber.getC(),
                                skAbsorber.getD(), skAbsorber.getE(), skAbsorber.getF());
                        double jdPrice = 0;
                        double hdPrice = 0;
                        for (String absorber : absorbers) {
                            if (EmptyUtil.isNotEmpty(absorber)) {
                                CsPrice csPrice = AhuMetadata.findOne(CsPrice.class, absorber);
                                if (EmptyUtil.isNotEmpty(csPrice)) {
                                    jdPrice += csPrice.getCost();
                                } else {
                                    log.error("Absorber price for code [{}] has not found", absorber);
                                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                                }
                                if (absorber.contains(ServiceConstant.SYS_STRING_SPRING_JD)) {
                                    String hdAbsorber = absorber.replace(ServiceConstant.SYS_STRING_SPRING_JD,
                                            ServiceConstant.SYS_STRING_SPRING_HD);
                                    csPrice = AhuMetadata.findOne(CsPrice.class, hdAbsorber);
                                    if (EmptyUtil.isNotEmpty(csPrice)) {
                                        hdPrice += csPrice.getCost();
                                    } else {
                                        log.error("Absorber price for code [{}] has not found", hdAbsorber);
                                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                                    }
                                }
                            }
                        }
                        LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                        if (EmptyUtil.isNotEmpty(lpRate)) {
                            nsSpringtransformPrice = (hdPrice - jdPrice) / lpRate.getMargin() / lpRate.getRate();
                        } else {
                            log.error("LPRate for - [{}] has not found.", serial);
                            throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                        }
                    } else {
                        log.error("S_K_Absorber for - [{}, {}] has not found.", absorberType, motorBaseNo);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_SPRING_TRANSFORM_PRICE,
                        String.valueOf(nsSpringtransformPrice));
            } else if (SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType)
                    || SectionTypeEnum.TYPE_HEPAFILTER.getCode().equals(sectionType)
                    || SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
                String nsPressureGuage = ServiceConstant.SYS_BLANK;
                String nsPressureGuageModel = ServiceConstant.SYS_BLANK;
                if (SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType)) {
                    nsPressureGuage = nsPriceParam.getNs_section_filter_nsPressureGuage();
                    nsPressureGuageModel = nsPriceParam.getNs_section_filter_nsPressureGuageModel();
                } else if (SectionTypeEnum.TYPE_HEPAFILTER.getCode().equals(sectionType)) {
                    nsPressureGuage = nsPriceParam.getNs_section_HEPAFilter_nsPressureGuage();
                    nsPressureGuageModel = nsPriceParam.getNs_section_HEPAFilter_nsPressureGuageModel();
                } else if (SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
                    nsPressureGuage = nsPriceParam.getNs_section_combinedFilter_nsPressureGuage();
                    nsPressureGuageModel = nsPriceParam.getNs_section_combinedFilter_nsPressureGuageModel();
                }
                double nsPressureGuagePrice = 0;
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPressureGuage)) {
                    NonstdPressure nonstdPressure = AhuMetadata.findOne(NonstdPressure.class, nsPressureGuageModel);
                    LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                    if (EmptyUtil.isNotEmpty(nonstdPressure)) {
                        if (EmptyUtil.isNotEmpty(lpRate)) {
                            nsPressureGuagePrice = this.calculateNsPrice(nonstdPressure, lpRate);
                        } else {
                            log.error("LPRate for - [{}] has not found.", serial);
                            throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                        }
                    } else {
                        log.error("Non-std pressure price for - [{}] has not found.", nsPressureGuageModel);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_PRESSURE_GUAGE_PRICE,
                        String.valueOf(nsPressureGuagePrice));
            } else if (EmptyUtil.isNotEmpty(ahuId)) { // calculate ahu ns price
                // calculate AHU channel steel price
                String nsChannelSteelBase = nsPriceParam.getNs_ahu_nsChannelSteelBase();
                String nsChannelSteelBaseModel = nsPriceParam.getNs_ahu_nsChannelSteelBaseModel();
                double nsChannelSteelBasePrice = 0;
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsChannelSteelBase)) {
                    Unit unit = ahuService.findAhuById(ahuId);
                    Partition partition = partitionService.findPartitionByAHUId(ahuId);
                    List<AhuPartition> ahuPartitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
                    for (AhuPartition ahuPartition : ahuPartitions) {
                        String baseCode = AhuPartitionGenerator.generateBaseForPrice(ahuPartition);
                        if (EmptyUtil.isNotEmpty(baseCode)) {
                            CsPrice csPrice = AhuMetadata.findOne(CsPrice.class, baseCode);
                            CsPriceRate csPriceRate = AhuMetadata.findOne(CsPriceRate.class, nsChannelSteelBaseModel
                                    .replaceAll(ServiceConstant.SYS_PUNCTUATION_POUND, ServiceConstant.SYS_BLANK));
                            if (EmptyUtil.isNotEmpty(csPrice)) {
                                if (EmptyUtil.isNotEmpty(csPriceRate)) {
                                    nsChannelSteelBasePrice += this.calculateNsPrice(csPrice, csPriceRate);
                                } else {
                                    log.error("Channel steel price rate for model [{}] has not found",
                                            nsChannelSteelBaseModel);
                                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                                }
                            } else {
                                log.error("Channel steel price for base code [{}] has not found", baseCode);
                                throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                            }
                        } else {
                            log.error("Channel steel base code has not found");
                            throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                        }
                    }
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_CHANNEL_STEEL_BASE_PRICE,
                        String.valueOf(nsChannelSteelBasePrice));

                double nsDeformationPrice = 0;
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_ahu_deformation())) {
                    int heightOfAhu = AhuUtil.getHeightOfAHU(nsPriceParam.getSerial());
                    int widthOfAhu = AhuUtil.getWidthOfAHU(nsPriceParam.getSerial());
                    nsDeformationPrice = nsPriceParam.getBoxPrice()
                            * (((nsPriceParam.getNsheight() * nsPriceParam.getNswidth()) / (heightOfAhu * widthOfAhu))
                            * 1.02 - 1);
                    nsPriceMap.put(ServiceConstant.SYS_STRING_DEFORMATION_PRICE, String.valueOf(nsDeformationPrice));
                }
                nsPriceMap.put(ServiceConstant.SYS_STRING_DEFORMATION_PRICE, String.valueOf(nsDeformationPrice));
            }
        } catch (Exception e) {
            logger.debug("Exception in fan seciton calNsPrice calculation:", nsPriceParam);
            logger.error("Exception in fan seciton calNsPrice calculation", e);
            throw new PriceCalcException("Section calNsPrice calculation error " + nsPriceParam.getSectionType(), e);
        }

        return nsPriceMap;
    }

    private String getMotorTypeCode(String type) {
        if (ServiceConstant.SYS_STRING_NUMBER_1.equals(type)) { // 二级能效电机, 高效电机
            return ServiceConstant.SYS_ALPHABET_C;
        } else if (ServiceConstant.SYS_STRING_NUMBER_2.equals(type)) { // 三级能效电机, 普通电机
            return ServiceConstant.SYS_ALPHABET_D;
        } else if (ServiceConstant.SYS_STRING_NUMBER_3.equals(type)) { // 防爆电机
            return ServiceConstant.SYS_ALPHABET_B;
        } else if (ServiceConstant.SYS_STRING_NUMBER_4.equals(type)) { // 双速电机
            return ServiceConstant.SYS_ALPHABET_A;
        } else if (ServiceConstant.SYS_STRING_NUMBER_5.equals(type)
                || ServiceConstant.SYS_STRING_NUMBER_6.equals(type)) { // 变频电机
            return ServiceConstant.SYS_ALPHABET_C;
        }
        log.debug("Motor type code for [{}] has not found.", type);
        return null;
    }

    private String getFanSupplierCode(String supplier) {
        if (ServiceConstant.JSON_FAN_SUPPLIER_DEFAULT.equals(supplier)) { // 卧龙
            return ServiceConstant.SYS_ALPHABET_C;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_ABB.equals(supplier)) { // ABB
            return ServiceConstant.SYS_ALPHABET_A;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_DONGYUAN.equals(supplier)) { // 东元 TECO
            return ServiceConstant.SYS_ALPHABET_E;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_SIMENS.equals(supplier)) { // GE
            return ServiceConstant.SYS_ALPHABET_A;
        }
        return null;
    }

    private String getAbsorberType(String fanModel) {
        String fanModelNo = NumberUtil.onlyNumbers(fanModel);
        if (fanModel.contains(ServiceConstant.JSON_FAN_FANMODEL_FC)) {
            return fanModelNo + ServiceConstant.JSON_FAN_FANMODEL_KTQ;
        } else if (fanModel.contains(ServiceConstant.JSON_FAN_FANMODEL_BC)) {
            return fanModelNo + ServiceConstant.JSON_FAN_FANMODEL_KTH;
        }
        return fanModel;
    }

    private double calculateNsPrice(NonstdPrice nonstdPrice, LpRate lpRate) {
        return (nonstdPrice.getTp() - nonstdPrice.getOldPrice()) / lpRate.getMargin() / lpRate.getRate();
    }

    private double calAccessNsPrice() {
        return 0.0;
    }

}
