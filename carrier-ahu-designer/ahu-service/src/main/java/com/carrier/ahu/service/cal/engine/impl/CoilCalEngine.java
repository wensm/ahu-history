package com.carrier.ahu.service.cal.engine.impl;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.engine.service.coil.CoilService;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.common.intl.I18NConstants.COIL_ENGINE_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2017/12/14. 盘管引擎计算 冷水盘管（冬夏） 热水盘管（冬） 直接蒸发式（冬夏）
 */
@Component
public class CoilCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(CoilCalEngine.class.getName());

    @Autowired
    private CoilService coilService;

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("CoilCalEngine calculator line begin");
        try {
            CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(ahu, section, null, new CoilInParam());
            PublicRuleParam publicRuleParam = new PublicRuleParam();
            publicRuleParam.setKey(context.getCalType().getId());
            UserConfigParam userConfigParam = context.getUserConfigParam();
            publicRuleParam.setUserConfigParam(userConfigParam);
            String coilType;
            CoilInfo coilInfoS = null;
            CoilInfo coilInfoW = null;
            if (CalcTypeEnum.PERFORMANCE_KEEP.getId().equals(publicRuleParam.getKey())) {//如果规则为性能保持引擎不参与计算
                logger.info("CoilCalEngine calculator Performance Keep Success");
            } else {//其他条件下根据规则引擎计算
                if (SectionTypeEnum.TYPE_COLD.getId().equals(section.getKey())) {
                    coilType = waterCoil;
                    coilInfoS = coilInParamS(coilType, ahu, section, publicRuleParam);
                    if (coilInParam.isEnableWinter()) {
                        coilInfoW = coilInParamW(coilType, ahu, section, publicRuleParam);
                    }
                } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(section.getKey())) {
                    coilType = DXCoil;
                    coilInfoS = coilInParamS(coilType, ahu, section, publicRuleParam);
                    if (coilInParam.isEnableWinter()) {
                        coilInfoW = coilInParamW(coilType, ahu, section, publicRuleParam);
                    }
                } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(section.getKey())) {
                    coilType = waterCoilH;
                    coilInfoW = coilInParamW(coilType, ahu, section, publicRuleParam);
                }
                section.getParams().putAll(getMap(section, coilInfoS, coilInfoW));// 转换计算
            }
            section.getParams().put(META_SECTION_COMPLETED, true);
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("CoilCalEngine calculator line end");
            return subc;
        } catch (Exception e) {
            String message = getIntlString(COIL_ENGINE_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("CoilCalEngine debug massage :" + message, e.getMessage());
            logger.error("CoilCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
        }
    }

    // 引擎结果转换
    private Map<String, Object> getMap(PartParam section, CoilInfo coilInfoS, CoilInfo coilInfoW) throws EngineException {
        Map<String, Object> map = new HashMap<String, Object>();
        if (EmptyUtil.isNotEmpty(coilInfoS)) {//封装夏季结果
            Map<String, Object> mapS = new InvokeTool<CoilInfo>().reInvoke(coilInfoS, seasonS, section.getKey());
            map.putAll(mapS);
        }
        if (EmptyUtil.isNotEmpty(coilInfoW)) {//封装冬季结果
            Map<String, Object> mapW = new InvokeTool<CoilInfo>().reInvoke(coilInfoW, seasonW, section.getKey());
            map.putAll(mapW);
        }
        if (EmptyUtil.isEmpty(coilInfoS) && EmptyUtil.isEmpty(coilInfoW)) {
            section.getParams().put(META_SECTION_COMPLETED, false);
            logger.error("CoilCalEngine calculator coilInfoW null");
            throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
        }
        return map;
    }

    // 夏季引擎计算
    private CoilInfo coilInParamS(String coilType, AhuParam ahu, PartParam section, PublicRuleParam publicRuleParam) {
        String season = seasonS;
        CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(ahu, section, season, new CoilInParam());
        /*批量计算特殊参数处理：计算是规则不同会影响机组型号、盘管、片距、回路 批量计算时全部使用AUTO重新计算*/
        coilInParam.setRows(COOLINGCOIL_ROWS_AUTO);
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_AUTO);
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_AUTO);
        coilInParam.setCoilType(coilType);
        coilInParam.setSeason(season);// 默认计算夏季
        CoilInfo coilInfo = null;
        try {
            double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
            double inWetBulbT = coilInParam.getInWetBulbT();//进风湿球温度
            logger.info("CoilCalEngine calculator param :" + JSONArray.toJSONString(coilInParam));
            if (inDryBulbT < 12 || inWetBulbT >= inDryBulbT) {
                throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
            } else {
                List<CoilInfo> coilInfoList = coilService.getCoil(coilInParam, publicRuleParam, coilType, AHUContext.getAhuVersion());
                if (!EmptyUtil.isEmpty(coilInfoList)) {
                    coilInfo = coilInfoList.get(0);
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, null == coilInfo.getAirResistance() ? ServiceConstant.SYS_BLANK : coilInfo.getAirResistance());
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_SINRELATIVET, coilInParam.getInRelativeT());
                }
            }
            logger.info("CoilCalEngine calculator coilInfo :" + JSONArray.toJSONString(coilInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CoilCalEngine error massage :" + e.getMessage());
        } catch (Error e) {
            logger.error("CoilCalEngine error massage :" + e.getMessage());
        }
        return coilInfo;
    }

    // 冬季引擎计算
    private CoilInfo coilInParamW(String coilType, AhuParam ahu, PartParam section, PublicRuleParam publicRuleParam) {
        String season = seasonW;
        CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(ahu, section, season, new CoilInParam());
        /*批量计算特殊参数处理：计算是规则不同会影响机组型号、盘管、片距、回路 批量计算时全部使用AUTO重新计算*/
        coilInParam.setRows(COOLINGCOIL_ROWS_AUTO);
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_AUTO);
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_AUTO);
        coilInParam.setSeason(season);
        CoilInfo coilInfo = null;
        try {
            logger.info("CoilCalEngine calculator param :" + JSONArray.toJSONString(coilInParam));
            /*冬季计算增加 进水温度 - 水温升 必须大于 进风干球温度*/
            double enteringFluidTemperature = coilInParam.getEnteringFluidTemperature();//进水温度
            double wTAScend = coilInParam.getWTAScend();//水温升
            double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
            double inWetBulbT = coilInParam.getInWetBulbT();//进风湿球温度
            if (enteringFluidTemperature - wTAScend <= inDryBulbT || inDryBulbT > 50 || inWetBulbT >= inDryBulbT) {
                throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
            } else {
                List<CoilInfo> coilInfoList = coilService.getCoil(coilInParam, publicRuleParam, coilType, AHUContext.getAhuVersion());
                if (!EmptyUtil.isEmpty(coilInfoList)) {
                    coilInfo = coilInfoList.get(0);
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, null == coilInfo.getAirResistance() ? ServiceConstant.SYS_BLANK : coilInfo.getAirResistance());
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WINRELATIVET, coilInParam.getInRelativeT());
                }
            }
            logger.info("CoilCalEngine calculator coilInfo :" + JSONArray.toJSONString(coilInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("CoilCalEngine error massage :" + e.getMessage());
        } catch (Error e) {
            logger.error("CoilCalEngine error massage :" + e.getMessage());
        }
        return coilInfo;
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{SectionTypeEnum.TYPE_COLD,
                SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL};
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_COIL;
    }

    @Override
    public int getCalLevel() {
        return CalOperationEnum.BATCH.getId();
    }

}
