package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.FAN_ENGINE_CALCULATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.RESET_TOTAL_RESISTANCE_FINISH;
import static com.carrier.ahu.common.intl.I18NConstants.RESET_TOTAL_RESISTANCE_START;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonS;

import java.util.Map;

import com.carrier.ahu.common.entity.UserConfigParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.engine.service.fan.FanService;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.cal.impl.CalLineSupport;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by LIANGD4 on 2017/12/14.
 * 风机引擎计算
 * 风机引擎包含：亿利达、谷科
 */
@Component
public class FanCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(FanCalEngine.class.getName());

    @Autowired
    private FanService fanService;

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("FanCalEngine calculator line begin");
        try {
            /* 总线阻力计算 */
            CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                    getIntlString(RESET_TOTAL_RESISTANCE_START, AHUContext.getLanguage(), ahu.getUnitNo()));
            CalLineSupport.resetTotalResistance(ahu);
            CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                    getIntlString(RESET_TOTAL_RESISTANCE_FINISH, AHUContext.getLanguage(), ahu.getUnitNo()));
            FanInParam fanInParam = new InvokeTool<FanInParam>().genInParamFromAhuParam(ahu, section, seasonS,
                    new FanInParam());
            logger.info("FanCalEngine calculator param :" + JSONArray.toJSONString(fanInParam));
            PublicRuleParam publicRuleParam = new PublicRuleParam();
            publicRuleParam.setKey(context.getCalType().getId());
            UserConfigParam userConfigParam = context.getUserConfigParam();
            publicRuleParam.setUserConfigParam(userConfigParam);
            if (CalcTypeEnum.PERFORMANCE_KEEP.getId().equals(publicRuleParam.getKey())) {//如果规则为性能保持引擎不参与计算
                logger.info("FanCalEngine calculator Performance Keep Success");
            } else {//其他条件下根据规则引擎计算
                FanInfo fanInfo = fanService.getFanEngineByRule(fanInParam, publicRuleParam, AHUContext.getAhuVersion());
                logger.info("FanCalEngine calculator fanInfo :" + JSONArray.toJSONString(fanInfo));
                if (null != fanInfo) {
                    Map<String, Object> map = new InvokeTool<FanInfo>().reInvoke(fanInfo, seasonS, section.getKey());
                    section.getParams().putAll(map);//转换计算
                } else {
                    logger.error("FanCalEngine fanService calculator error engine null");
                    throw new FanEngineException("FanEngine Calculator Result null");
                }
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("FanCalEngine calculator line end");
            return subc;
        } catch (Exception e) {
            String message = getIntlString(FAN_ENGINE_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("FanCalEngine debug massage :" + message, e.getMessage());
            logger.error("FanCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new FanEngineException(FAN_ENGINE_CALCULATION_FAILED);
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_FAN
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_FAN;
    }

    @Override
    public int getCalLevel() {
        return CalOperationEnum.BATCH.getId();
    }

}
