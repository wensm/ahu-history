package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO;
import static com.carrier.ahu.common.intl.I18NConstants.HUMIDIFICATION_VOLUME_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonS;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonW;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.DefaultValueValidationUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.exception.calculation.HumQCalcException;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.EngineConvertParam;
import com.carrier.ahu.param.HumQCalParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.util.InvokeTool;

/**
 * Created by LIANGD4 on 2017/12/13.
 * 计算出风温度
 * 包含:加湿段
 * 条件加湿量、相对湿度（加湿比）二选一
 */
@Component
public class HumQCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(HumQCalEngine.class.getName());
    @Autowired
    private ValidateService validateService;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("HumQCalEngine calculator line begin");
        Map<String, Object> mapAll = new HashMap<String, Object>();
        int airVolume = 0;
        String sectionKey = section.getKey();
        double sHumidificationQ = 0.00;
        double wHumidificationQ = 0.00;
        String supplier = ServiceConstant.SYS_BLANK;
        try {
            HumQCalParam humQCalParam = new InvokeTool<HumQCalParam>().genInParamFromAhuParam(ahu, section, seasonS, new HumQCalParam());
            boolean isSummer = humQCalParam.isEnableSummer();
            boolean isWinter = humQCalParam.isEnableWinter();
            if (isSummer) {
                sHumidificationQ = humQCalParam.getHumidificationQ();//夏季加湿量
                DefaultValueValidationUtil.zeroHumQ(sHumidificationQ);
                supplier = humQCalParam.getSupplier();
                //计算前温度条件校验
                //1:湿球不能大于干球
                validateService.compareT(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), null);
                //2:加湿量不能 小于 0
                validateService.compareHumidificationQ(humQCalParam.getHumidificationQ(), null);


                if (AirDirectionEnum.RETURNAIR.getCode().equals(humQCalParam.getAirDirection())) {
                    airVolume = humQCalParam.getEairvolume();//回风
                } else {
                    airVolume = humQCalParam.getSairvolume();//送风
                }

                logger.info("HumQCalEngine calculator paramS :" + JSONArray.toJSONString(humQCalParam));
                if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey)) {//（等温加湿）电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getHumidificationQ(), airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonS, section));
                    logger.info("HumQCalEngine calculator humidificationBeanS :" + JSONArray.toJSONString(humidificationBean));

                } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//（等焓加湿）湿膜加湿段(Wetfilm Humidifier)  /高压喷雾加湿段(Spray Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getHumidificationQ(), airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonS, section));
                    logger.info("HumQCalEngine calculator humidificationBeanS :" + JSONArray.toJSONString(humidificationBean));

                }
            }

            if (isWinter) {
                humQCalParam = new InvokeTool<HumQCalParam>().genInParamFromAhuParam(ahu, section, seasonW, new HumQCalParam());
                wHumidificationQ = humQCalParam.getHumidificationQ();//冬季加湿量
                DefaultValueValidationUtil.zeroHumQ(wHumidificationQ);
                //计算前温度条件校验
                //1:湿球不能大于干球
                validateService.compareT(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), null);
                //2:加湿量不能 小于 0
                validateService.compareHumidificationQ(humQCalParam.getHumidificationQ(), null);

                if (AirDirectionEnum.RETURNAIR.getCode().equals(humQCalParam.getAirDirection())) {
                    airVolume = humQCalParam.getEairvolume();//回风
                } else {
                    airVolume = humQCalParam.getSairvolume();//送风
                }
                logger.info("HumQCalEngine calculator paramW :" + JSONArray.toJSONString(humQCalParam));


                if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey)) {//（等温加湿）电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getHumidificationQ(), airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonW, section));
                    logger.info("HumQCalEngine calculator humidificationBeanW :" + JSONArray.toJSONString(humidificationBean));

                } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//（等焓加湿）湿膜加湿段(Wetfilm Humidifier)  /高压喷雾加湿段(Spray Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getHumidificationQ(), airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonW, section));
                    logger.info("HumQCalEngine calculator humidificationBeanW :" + JSONArray.toJSONString(humidificationBean));
                }
            }
            /*根据加湿段类型计算水量*/
            if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)) {//湿膜加湿段
                double waterQuantity = 0.00;
                //封装水量字段属性 水量=冬季加湿量*3 如果 冬季加湿量为0 使用夏季加湿量
                if (wHumidificationQ > 0.00) {
                    waterQuantity = wHumidificationQ * 3;
                } else {
                    waterQuantity = sHumidificationQ * 3;
                }
                mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WATERQUANTITY, waterQuantity);
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//高压喷雾加湿段
                double waterQuantity = 0.00;
                //封装水量字段属性 水量=冬季加湿量*4 如果 冬季加湿量为0 使用夏季加湿量
                if (wHumidificationQ > 0.00) {
                    waterQuantity = wHumidificationQ * 4;
                } else {
                    waterQuantity = sHumidificationQ * 4;
                }
                mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WATERQUANTITY, waterQuantity);
                mapAll.put(
                        MetaCodeGen.calculateAttributePrefix(section.getKey())
                                + ServiceConstant.METACOMMON_POSTFIX_WATERFLOW,
                        ServiceConstant.SYS_STRING_NUMBER_3 + ServiceConstant.SYS_STRING_NUMBER_0);
            }
        } catch (TempCalErrorException e) {
            String message = getIntlString(HUMIDIFICATION_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("HumQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HumQCalcException(HUMIDIFICATION_VOLUME_CALCULATION_FAILED);
        } catch (HumQCalcException e){
            String message = getIntlString(HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO, AHUContext.getLanguage());
            logger.debug("HumQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HumQCalcException(HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO);
        }catch (Exception e) {
            String message = getIntlString(HUMIDIFICATION_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("HumQCalEngine debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HumQCalcException(HUMIDIFICATION_VOLUME_CALCULATION_FAILED);
        }
        //返回结果更新到数据库中
        section.getParams().putAll(mapAll);
        context.setSuccess(true);
        ICalContext subc = super.cal(ahu, section, context);
        subc.setSuccess(true);
        logger.info("HumQCalEngine calculator line end");
        return subc;
    }

    private Map<String, Object> getMap(AhuParam ahu, HumidificationBean humidificationBean, String season, PartParam section) {
        EngineConvertParam engineConvertParam = new InvokeTool<EngineConvertParam>().genInParamFromAhuParam(ahu, section, season, new EngineConvertParam());
        engineConvertParam.setInRelativeT(humidificationBean.getInRelativeT());//进风相对湿度
        engineConvertParam.setHumidificationQ(humidificationBean.getHumidificationQ());//加湿量
        engineConvertParam.setJSBRelativeT(humidificationBean.getOutRelativeT());//相对湿度
        engineConvertParam.setOutDryBulbT(humidificationBean.getOutDryBulbT());//出风干球温度
        engineConvertParam.setOutWetBulbT(humidificationBean.getOutWetBulbT());//出风湿球温度
        engineConvertParam.setOutRelativeT(humidificationBean.getOutRelativeT());//出风相对湿度
        Map<String, Object> map = new InvokeTool<EngineConvertParam>().reInvoke(engineConvertParam, season, section.getKey());
        return map;
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
                SectionTypeEnum.TYPE_WETFILMHUMIDIFIER,
                SectionTypeEnum.TYPE_SPRAYHUMIDIFIER,
                SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_HUMQ;
    }
}
