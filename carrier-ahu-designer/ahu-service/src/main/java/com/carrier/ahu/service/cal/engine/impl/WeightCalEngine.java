package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.hutool.core.util.NumberUtil;
import com.carrier.ahu.util.EmptyUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carrier.ahu.calculator.CalculatorModel;
import com.carrier.ahu.calculator.CalculatorUtil;
import com.carrier.ahu.calculator.ExcelForPriceUtils;
import com.carrier.ahu.calculator.WeightCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.WeightCalcException;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.cal.WeightBean;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;

@Component
public class WeightCalEngine extends AbstractCalEngine {
    private static Logger logger = LoggerFactory.getLogger(WeightCalEngine.class.getName());
    @Autowired
    private WeightCalculator weightCalculator;

    private static CalculatorModel calculatorModel;

    public CalculatorModel getCalculatorModel() {
        if (null == calculatorModel) {
            calculatorModel = weightCalculator.getCalModelInstance();
        }
        return calculatorModel;
    }

    public List<CalculatorSpec> getSpec() {
        return getCalculatorModel().getSpec();
    }

    @Override
    public int getOrder() {
        return 8;
    }

    /**
     * 该计算默认以为Excel中section列只有一个段的信息
     * 
     * @throws Exception
     */
    @Override
    public ICalContext cal(AhuParam ahuParam, PartParam partParam, ICalContext context) throws Exception {
        logger.info("WeightCalEngine calculator line begin");
        ICalContext subc = super.cal(ahuParam, partParam, context);
        try {
            // calculateWeight(ahuParam, partParam);
            String smodel = ahuParam.getSeries();
            Map<String, Object> map = new HashMap<>();
            map.putAll(ahuParam.getParams());
            map.putAll(partParam.getParams());
            if (null != partParam.getNextParams()) {
                // avoid override parameter of same section type
                partParam.getNextParams().forEach((key, value) -> {
                    if (!map.containsKey(key)) {
                        map.put(key, value);
                    }
                });
            }
            populateAccessories(partParam.getKey(), map);

            String lowerSectionType = partParam.getKey().toLowerCase();// TODO
                                                                       // 风机前面的控制段，需要在风机确定提交后，再次进行确定控制段。不然控制段的重量计算错误@@@需要添加规则
            String wgtKey = MetaCodeGen.calculateAttributePrefix(partParam.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WEIGHT;
            partParam.getParams().put(wgtKey, 0);

            System.out.println("->->->->->->->->" + lowerSectionType);
            Iterator<CalculatorSpec> it = getSpec().iterator();
            boolean calResult = false;
            while (it.hasNext()) {
                CalculatorSpec spec = it.next();
                // 跳过分段
                if (ExcelForPriceUtils.PRICESPEC_AHUCAL_21.equals(spec.getAhucal())) {
                    continue;
                }
                String section = spec.getSection();
                // 跳过段类型不符合的计算
                // 不跳过附件的计算
                if (!ExcelForPriceUtils.PRICESPEC_AHUCAL_01.equals(spec.getAhucal())
                        && !lowerSectionType.equals(section.toLowerCase())) {
                    continue;
                }
                String item = spec.getName();
                // deal with coiling coil sub prices
                String ccstyle = ServiceConstant.SYS_STRING_NUMBER_3;
                if (item.startsWith(ServiceConstant.SYS_STRING_COOLINGCOIL_ACCESSES)) {
                    if (!item.endsWith(ccstyle)) {
                        // continue;
                    }
                }
                // 判断有效性
                if (ExcelForPriceUtils.PRICESPEC_AHUCAL_01.equals(spec.getAhucal())
                        || ExcelForPriceUtils.PRICESPEC_AHUCAL_11.equals(spec.getAhucal())
                        || ExcelForPriceUtils.PRICESPEC_AHUCAL_13.equals(spec.getAhucal())) {
                    if (!CalculatorUtil.isSpecEffective(spec, map)) {
                        logger.warn(String.format("Spec is not Effective Exception: %s @ %s ", spec.getName(),
                                spec.getTab()));
                        calResult = true;// 防止：如果只有一条需要计算的数据且不满足条件，前端报错
                        continue;
                    }
                } else if (ExcelForPriceUtils.PRICESPEC_AHUCAL_12.equals(spec.getAhucal())) {

                } else if (ExcelForPriceUtils.PRICESPEC_AHUCAL_13.equals(spec.getAhucal())) {

                }

                String simpleModel = smodel.substring(smodel.length() - 4);
                String unitSeries = AhuUtil.getUnitSeries(smodel);
                double itemWeight = weightCalculator.queryWeight(simpleModel, spec, map,
                        getCalculatorModel().getSpecData(unitSeries, spec), spec.getSection());
                if (itemWeight < 0) {
                    partParam.getParams().put(META_SECTION_COMPLETED, false);
                    String messageKey = AHUContext.getIntlString(I18NConstants.WEIGHT_ENGINE_CHECK_ABNORMAL);

                    // partParam.getParams().put(wgtKey, String.valueOf(0));
                    String message = MessageFormat.format(messageKey, itemWeight);
                    logger.error(message);
                    subc.error(message);
                    // TODO for debug
                    // context.setSuccess(false);
                    // throw new CalFailedException(message);
                } else {
                    Double pwgt = Double.parseDouble("" + partParam.getParams().get(wgtKey));
                    partParam.getParams().put(wgtKey, pwgt + itemWeight);
                    logger.info("WeightCalEngine calculator weight (" + wgtKey + "):" + pwgt + itemWeight);

                    // ahu重量保存时候统一计算
                    Double ahuWeight = ahuParam.getWeight();
                    if (null == ahuWeight) {
                        ahuWeight = 0d;
                        ahuParam.setWeight(ahuWeight);
                    }
                    ahuParam.setWeight(ahuParam.getWeight() + itemWeight);
                    logger.info("WeightCalEngine calculator weight (ahuParam.Weight):" + ahuParam.getWeight());

                    calResult = true;
                    // break;//TODO 补全逻辑。
                }
            }
            partParam.getParams().put(META_SECTION_COMPLETED, true);

            calculateRunningWeight(partParam); // running weight for humidifier

            convertWeight2Int(partParam);//重置段 重量、运行重量为int
            if(EmptyUtil.isNotEmpty(ahuParam.getWeight()))
                ahuParam.setWeight(Double.parseDouble(String.valueOf(NumberUtil.parseInt(String.valueOf(ahuParam.getWeight())))));//重置AHU重量去掉小数点

            if (calResult) {
                subc.setSuccess(true);
                return subc;
            } else {
                partParam.getParams().put(META_SECTION_COMPLETED, false);
                String message = AHUContext.getIntlString(I18NConstants.WEIGHT_ENGINE_CHECK_NO_DATA);
                logger.error(message);
                subc.error(message);
                context.setSuccess(false);
                throw new WeightCalcException(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            partParam.getParams().put(META_SECTION_COMPLETED, false);
            String message = AHUContext.getIntlString(I18NConstants.WEIGHT_CALCULATION_FAILED);
            logger.error(message);
            subc.error(message);
            context.setSuccess(false);
            throw new WeightCalcException(message);
        }
    }

    /**
     * Set accessory's property value</br>
     * 
     * accessaries.uvc </br>
     * accessaries.door </br>
     * accessaries.window </br>
     * accessaries.pswitch </br>
     * accessaries.pmeter </br>
     * accessaries.fac
     * 
     * @param key
     * @param map
     */
    private void populateAccessories(String sectionType, Map<String, Object> sectionMeta) {
        if (sectionHasDoor(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_DOOR, Boolean.TRUE.toString());
        }
        if (sectionHasTwoDoors(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_DOOR2, Boolean.TRUE.toString());
        }
        if (sectionHasWindow(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_WINDOW, Boolean.TRUE.toString());
        }
        if (sectionHasTwoWindows(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_WINDOW2, Boolean.TRUE.toString());
        }
        if (sectionHasUvLamp(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_UVC, Boolean.TRUE.toString());
        }
        if (sectionHasFixRepairLamp(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_MLAMP, Boolean.TRUE.toString());
        }
        if (sectionHasFac(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_FAC, Boolean.TRUE.toString());
        }
        if (sectionHasPSwitch(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_PSWITCH, Boolean.TRUE.toString());
        }
        if (sectionHasPSwitch2(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_PSWITCH2, Boolean.TRUE.toString());
        }
        if (sectionHasPMeter(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_PMETER, Boolean.TRUE.toString());
        }
        if (sectionHasPMeter2(sectionType, sectionMeta)) {
            sectionMeta.put(EngineConstant.SYS_WGT_ACCESSORIES_PMETER2, Boolean.TRUE.toString());
        }
    }

    private boolean sectionHasDoor(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_DOORO);
            return !SystemCalculateConstants.MIX_DOORO_NODOOR.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ODOOR);
            return !SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ODOOR);
            return !SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ISDOOR);
            return !SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ACCESSDOOR);
            return SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHOUTWINDOW.equals(sectionMeta.get(doorMetaKey));
        }
        return false;
    }

    private boolean sectionHasTwoDoors(String sectionType, Map<String, Object> sectionMeta) {
        if (Boolean.valueOf(String.valueOf(sectionMeta.get(EngineConstant.SYS_WGT_ACCESSORIES_DOOR)))) {
            if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)) {
                String door2MetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_DOOR_DIRECTION);
                return SystemCalculateConstants.MIX_DOORDIRECTION_BOTH.equals(sectionMeta.get(door2MetaKey));
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionType)) {
                String door2MetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_DOOR_DIRECTION);
                return SystemCalculateConstants.DISCHARGE_DOORDIRECTION_BOTH.equals(sectionMeta.get(door2MetaKey));
            } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionType)) {
                String door2MetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_DOOR_ON_BOTH_SIDE);
                return Boolean.valueOf(String.valueOf(sectionMeta.get(door2MetaKey)));
            }
        }
        if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_OSDOOR);
            return !SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMeta.get(doorMetaKey));
        }
        return false;
    }

    private boolean sectionHasWindow(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_DOORO);
            return SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ISDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMeta.get(doorMetaKey));
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ACCESSDOOR);
            return SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHWINDOW.equals(sectionMeta.get(doorMetaKey));
        }
        return false;
    }

    private boolean sectionHasTwoWindows(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionType)) {
            String doorMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_OSDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMeta.get(doorMetaKey));
        } else if (Boolean.valueOf(String.valueOf(sectionMeta.get(EngineConstant.SYS_WGT_ACCESSORIES_DOOR2)))
                && Boolean.valueOf(String.valueOf(sectionMeta.get(EngineConstant.SYS_WGT_ACCESSORIES_WINDOW)))) {
            return Boolean.TRUE;
        }
        return false;
    }

    private boolean sectionHasUvLamp(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)) {
            String uvLampMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_UV_LAMP);
            return Boolean.valueOf(String.valueOf(sectionMeta.get(uvLampMetaKey)));
        }
        return false;
    }

    private boolean sectionHasFixRepairLamp(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)
                || SectionTypeEnum.TYPE_FAN.getId().equals(sectionType)) {
            String fixLampMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_fIX_REPAIR_LAMP);
            return Boolean.valueOf(String.valueOf(sectionMeta.get(fixLampMetaKey)));
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionType)
                || SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionType)
                || SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionType)) {
            String fixLampMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_FIX_REPAIR_LAMP);
            return Boolean.valueOf(String.valueOf(sectionMeta.get(fixLampMetaKey)));
        }
        return false;
    }

    private boolean sectionHasFac(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionType)) {
            String facMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_FRESH_AIR_HOOD);
            return Boolean.valueOf(String.valueOf(sectionMeta.get(facMetaKey)));
        }
        return false;
    }

    private boolean sectionHasPSwitch(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE);
            return SystemCalculateConstants.FILTER_PRESSUREGUAGE_PRESSUREDIFFERENCESWITCH
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        } else if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE);
            return SystemCalculateConstants.FILTER_PRESSUREGUAGE_PRESSUREDIFFERENCESWITCH
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        } else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE_P);
            return SystemCalculateConstants.COMBINEDFILTER_PRESSUREGUAGEP_PRESSUREDIFFERENCESWITCH
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        }
        return false;
    }

    private boolean sectionHasPSwitch2(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE_B);
            return SystemCalculateConstants.COMBINEDFILTER_PRESSUREGUAGEB_PRESSUREDIFFERENCESWITCH
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        }
        return false;
    }

    private boolean sectionHasPMeter(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE);
            return SystemCalculateConstants.FILTER_PRESSUREGUAGE_POINT_DIFFERENTIAL_PRESSURE_GUAGE
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        } else if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE);
            return SystemCalculateConstants.FILTER_PRESSUREGUAGE_POINT_DIFFERENTIAL_PRESSURE_GUAGE
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        } else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE_P);
            return SystemCalculateConstants.COMBINEDFILTER_PRESSUREGUAGEP_POINT_DIFFERENTIAL_PRESSURE_GUAGE
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        }
        return false;
    }

    private boolean sectionHasPMeter2(String sectionType, Map<String, Object> sectionMeta) {
        if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionType)) {
            String pressureGuageMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_PRESSURE_GUAGE_B);
            return SystemCalculateConstants.COMBINEDFILTER_PRESSUREGUAGEB_POINT_DIFFERENTIAL_PRESSURE_GUAGE
                    .equals(sectionMeta.get(pressureGuageMetaKey));
        }
        return false;
    }

    private void calculateRunningWeight(PartParam partParam) {
        // calculate running weight
        if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(partParam.getKey())) {
            String weightKey = SectionMetaUtils.getMetaSectionKey(partParam.getKey(), MetaKey.KEY_WEIGHT);
            String waterQuantityKey = SectionMetaUtils.getMetaSectionKey(partParam.getKey(),
                    MetaKey.KEY_WATER_QUANTITY);
            double waterQuantity = NumberUtils.toDouble(String.valueOf(partParam.getParams().get(waterQuantityKey)));
            String runningWeightKey = SectionMetaUtils.getMetaSectionKey(partParam.getKey(),
                    MetaKey.KEY_RUNNING_WEIGHT);
            double partWeight = NumberUtils.toDouble(String.valueOf(partParam.getParams().get(weightKey)));
            partParam.getParams().put(runningWeightKey, waterQuantity + partWeight);
        }
    }

    /**
     * 转换计算后的重量、运行重量为int
     * @param partParam
     */
    private void convertWeight2Int(PartParam partParam) {
            String weightKey = SectionMetaUtils.getMetaSectionKey(partParam.getKey(), MetaKey.KEY_WEIGHT);
            String runningWeightKey = SectionMetaUtils.getMetaSectionKey(partParam.getKey(),
                    MetaKey.KEY_RUNNING_WEIGHT);

            if(EmptyUtil.isNotEmpty(partParam.getParams().get(weightKey))){
                int partWeight = NumberUtil.parseInt(String.valueOf(partParam.getParams().get(weightKey)));
                partParam.getParams().put(weightKey, partWeight);
            }
            if(EmptyUtil.isNotEmpty(partParam.getParams().get(runningWeightKey))){
                int runningWeight = NumberUtil.parseInt(String.valueOf(partParam.getParams().get(runningWeightKey)));
                partParam.getParams().put(runningWeightKey, runningWeight);
            }
    }
    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = AhuSectionMetas.SECTIONTYPES;
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_WEIGHT;
    }

    public Map<Short, List<WeightBean>> calculateWeight(AhuParam ahu, PartParam part) {
        Map<Short, List<WeightBean>> result = new HashMap<>();
        // 获取AHU参数
        List<String> sectionTypes = new ArrayList<>();
        sectionTypes.add(SectionTypeEnum.TYPE_AHU.getId());
        HashMap<String, Object> map = new HashMap<>();
        Map<String, Object> tmap = part.getParams();
        map.putAll(tmap);
        sectionTypes.add(part.getKey().toLowerCase());
        result.put(part.getPosition(), innerCalSectionWeight(ahu, part));
        return result;
    }

    /**
     * 计算AHU中单个段的重量信息
     *
     * @param ahu
     * @param part
     * @return
     */
    public List<WeightBean> innerCalSectionWeight(AhuParam ahu, PartParam part) {
        return Collections.emptyList();
    }

}
