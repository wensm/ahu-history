package com.carrier.ahu.service.service.impl;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.filter.ElectrostaticFilterResistance;
import com.carrier.ahu.metadata.entity.filter.HEPAFilterResistance;
import com.carrier.ahu.metadata.entity.access.AccessData;
import com.carrier.ahu.metadata.entity.fan.SFan;
import com.carrier.ahu.metadata.entity.fan.SFanLength;
import com.carrier.ahu.metadata.entity.fan.SFanLength1;
import com.carrier.ahu.metadata.entity.fan.SFanLength2;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SSectionKBIG;
import com.carrier.ahu.metadata.entity.filter.ALSingleFilterResistance;
import com.carrier.ahu.metadata.entity.filter.NWFSingleFilterResistance;
import com.carrier.ahu.metadata.entity.mix.MixSize;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.LineCoefficientUnit;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ValueFormatUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SectionCount implements SystemCalculateConstants {
	public static Logger logger = LoggerFactory.getLogger(SectionCount.class.getName());

	private static Map<String, NWFSingleFilterResistance> resistanceTable;
	private static Map<String, ALSingleFilterResistance> resistanceALTable;
	private static Map<String, HEPAFilterResistance> resistanceHEPATable;

	/**
	 * 单层过滤段段长计算
	 *
	 * @param filterF
	 *            过滤器形式(外抽、板式、袋式)
	 * @param mediaLoading
	 *            (正面抽、侧面抽)
	 * @param filterEfficiency
	 *            (袋式时PM2.5过滤器)
	 * @return
	 */
	public static Integer sectionLengthSimpleFilter(String filterF, String mediaLoading, String filterEfficiency) {
		if (FILTER_FITETF_X.equals(filterF)) {
			return 0;
		}
		if (FILTER_FITETF_P.equals(filterF)) {
			if (mediaLoading.equals(FILTER_MEDIALOADING_FRONTLOADING)) {
				return 0;
			}
			if (mediaLoading.equals(FILTER_MEDIALOADING_SIDELOADING)) {
				return 3;// 旧版本为3M 文档中4M
			}
		}
		if (FILTER_FITETF_B.equals(filterF)) {
			if (mediaLoading.equals(FILTER_MEDIALOADING_SIDELOADING)) {
				return 6;// 旧版本为6M 文档中8M
			} else {
				String[] pm25Array = new String[] { ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_C1,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_C2,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F5,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F6,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F7,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F8,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F9,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F7G,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F8G,
						ServiceConstant.JSON_FILTER_FILTEREFFICIENCY_F9G };// c1,c2 也默认6
				List<String> pm25List = new ArrayList<>();
				Collections.addAll(pm25List, pm25Array);
				if (pm25List.contains(filterEfficiency)) {
					return 6;
				} else {
					return 4;
				}
			}
		}
		return 0;
	}

	/**
	 * 单层过滤段阻力计算
	 *
	 * @param serial
	 *            序列号
	 * @param bagLength
	 *            边框厚度
	 * @param filterF
	 *            过滤器形式
	 * @param sectionLength
	 *            袋长
	 * @param fitlerStandard
	 *            过滤标准
	 * @param filterEfficiency
	 *            过滤器效率
	 * @return
	 */
	public static NWFSingleFilterResistance sectionResistanceSimpleFilter(Double airVolume, String serial,
			String bagLength, String filterF, int sectionLength, String fitlerStandard, String filterEfficiency,
			boolean al, String sectionType) {
		int itemLength = 1;// 边框厚度
		if (FILTER_RIMTHICKNESS_2.equals(bagLength)) {
			itemLength = 2;
		}
		SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
		double area = sectionArea.getBin();// mix迎风面积
		if (FILTER_FITETF_X.equals(filterF)) {
			area = sectionArea.getBout();
		}
		double velocity = airVolume / 3600.0 / area;// 风速 velocity = mathFloorBy025(velocity);
		String filterFKey = ServiceConstant.SYS_BLANK;
		String key = ServiceConstant.SYS_BLANK;
		if (FILTER_FITETF_X.equals(filterF)) {
			filterFKey = ServiceConstant.SYS_STRING_NUMBER_3;
			 key = filterFKey + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + itemLength + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN +
			 AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency);
			if (al) {// 如果为铝网过滤器
				return packageAl(itemLength, filterFKey, key, velocity, serial, fitlerStandard, filterEfficiency);
			}
		} else if (FILTER_FITETF_P.equals(filterF)) {
			filterFKey = ServiceConstant.SYS_STRING_NUMBER_2;
			key = filterFKey + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + itemLength + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency);
			if (al) {// 如果为铝网过滤器
				if (SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {// 如果是综合过滤段
					key = filterFKey + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + itemLength + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN
							+ AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency);
				}
				return packageAl(itemLength, filterFKey, key, velocity, serial, fitlerStandard, filterEfficiency);
			}
		} else if (FILTER_FITETF_B.equals(filterF)) {
			filterFKey = ServiceConstant.SYS_STRING_NUMBER_1;
			if (sectionLength > 6) {
				sectionLength = 6;
			}
			key = filterFKey + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + sectionLength + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN
					+ AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency);
		} else {
			return null;
		}
		return packageNon(itemLength, filterFKey, key, velocity, serial, fitlerStandard, filterEfficiency);
	}

	/* 封装无纺布阻力计算 */
	private static NWFSingleFilterResistance packageNon(int itemLength, String filterFKey, String key, double velocity,
			String serial, String fitlerStandard, String filterEfficiency) {
		Map<String, NWFSingleFilterResistance> resistanceTable = getResistanceTable(
				AhuMetadata.findAll(NWFSingleFilterResistance.class));
		List<NWFSingleFilterResistance> itemList = new ArrayList<>();
		for (Entry<String, NWFSingleFilterResistance> entry : resistanceTable.entrySet()) {
			if (entry.getKey().startsWith(key)) {
				NWFSingleFilterResistance filterResistanceItem = entry.getValue();
				if (filterResistanceItem.getWindSpeed() == 0) {
					continue;
				}
				itemList.add(filterResistanceItem);
			}
		}
		Collections.sort(itemList, new Comparator<NWFSingleFilterResistance>() {
			public int compare(NWFSingleFilterResistance sta1, NWFSingleFilterResistance sta2) {
				return new Double(sta1.getWindSpeed()).compareTo(new Double(sta2.getWindSpeed()));
			}
		});
		int len = itemList.size();
		double[] windspeedDouble = new double[len];
		double[] dinitialPDDouble = new double[len];
		double[] deveragePDDouble = new double[len];
		double[] dfinalPDDouble = new double[len];
		LinkedList<Double> windspeedList = new LinkedList<>();
		LinkedList<Double> dinitialPDList = new LinkedList<>();
		LinkedList<Double> deveragePDList = new LinkedList<>();
		LinkedList<Double> dfinalPDList = new LinkedList<>();
		NWFSingleFilterResistance returnItem = new NWFSingleFilterResistance();
		if (itemList.isEmpty()) {
			returnItem.setAhu(serial);
			returnItem.setAverResi(-1d);
			returnItem.setBeginResi(-1d);
			returnItem.setEndResi(-1d);
			returnItem.setFilterf(filterFKey);
			returnItem.setLength(itemLength);
			returnItem.setStandard(AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency));
			returnItem.setWindSpeed(velocity);
			return returnItem;
		}
		int i = 0;
		for (NWFSingleFilterResistance itm : itemList) {
			windspeedList.add(itm.getWindSpeed());
			dinitialPDList.add(itm.getBeginResi());
			deveragePDList.add(itm.getAverResi());
			dfinalPDList.add(itm.getEndResi());
			windspeedDouble[i] = itm.getWindSpeed();
			dinitialPDDouble[i] = itm.getBeginResi();
			deveragePDDouble[i] = itm.getAverResi();
			dfinalPDDouble[i] = itm.getEndResi();
			i++;
		}
		returnItem.setAhu(serial);
		// 3． 参考附件程序FilterUnit，计算初始压力，终压力，平均压力，计算得出的结果保留小数点后1位
		// a. 初始压力=qxnh2([velocity], [风速列表]，[初始压力列表]，2)
		// b. 终压力= qxnh2([velocity], [风速列表]，[终压力列表]，2)
		// c. 平均压力= qxnh2([velocity], [风速列表]，[平均压力列表]，2)
		double[] aver = LineCoefficientUnit.PolyFit(windspeedDouble, deveragePDDouble, new double[10], 2);
		double[] begin = LineCoefficientUnit.PolyFit(windspeedDouble, dinitialPDDouble, new double[10], 2);
		double[] end = LineCoefficientUnit.PolyFit(windspeedDouble, dfinalPDDouble, new double[10], 2);

		returnItem.setAverResi(LineCoefficientUnit.getY(velocity, windspeedDouble, aver, 2));
		returnItem.setBeginResi(LineCoefficientUnit.getY(velocity, windspeedDouble, begin, 2));
		returnItem.setEndResi(LineCoefficientUnit.getY(velocity, windspeedDouble, end, 2));

		returnItem.setFilterf(filterFKey);
		returnItem.setLength(itemLength);
		returnItem.setStandard(AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency));
		returnItem.setWindSpeed(velocity);
		return returnItem;
	}

	private static Map<String, NWFSingleFilterResistance> getResistanceTable(
			List<NWFSingleFilterResistance> resistances) {
		if (resistanceTable == null) {
			resistanceTable = new HashMap<>();
			for (NWFSingleFilterResistance resistance : resistances) {
				String key = resistance.getFilterf() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + resistance.getLength() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + resistance.getStandard()
						+ ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + ValueFormatUtil.transDouble2String(resistance.getWindSpeed());
				resistanceTable.put(key, resistance);
			}
		}
		return resistanceTable;
	}

	/* 封装铝网阻力计算 */
	private static NWFSingleFilterResistance packageAl(int itemLength, String filterFKey, String key, double velocity,
			String serial, String fitlerStandard, String filterEfficiency) {
		// 获取FilterResistanceAlInfo数据 板式 铝网过滤器 数据
		// 数据加载到当前方法内
		Map<String, ALSingleFilterResistance> resistanceTableAl = getALResistanceTable(
				AhuMetadata.findAll(ALSingleFilterResistance.class));

		List<ALSingleFilterResistance> itemList = new ArrayList<>();
		for (Entry<String, ALSingleFilterResistance> entry : resistanceTableAl.entrySet()) {
			if (entry.getKey().startsWith(key)) {
				ALSingleFilterResistance filterResistanceAlItem = entry.getValue();
				if (filterResistanceAlItem.getWindSpeed() == 0) {
					continue;
				}
				itemList.add(filterResistanceAlItem);
			}
		}
		Collections.sort(itemList, new Comparator<ALSingleFilterResistance>() {
			public int compare(ALSingleFilterResistance sta1, ALSingleFilterResistance sta2) {
				return new Double(sta1.getWindSpeed()).compareTo(new Double(sta2.getWindSpeed()));
			}
		});
		int len = itemList.size();
		double[] windspeedDouble = new double[len];
		double[] dinitialPDDouble = new double[len];
		double[] deveragePDDouble = new double[len];
		double[] dfinalPDDouble = new double[len];
		LinkedList<Double> windspeedList = new LinkedList<>();
		LinkedList<Double> dinitialPDList = new LinkedList<>();
		LinkedList<Double> deveragePDList = new LinkedList<>();
		LinkedList<Double> dfinalPDList = new LinkedList<>();
		NWFSingleFilterResistance returnItem = new NWFSingleFilterResistance();
		if (itemList.isEmpty()) {
			returnItem.setAhu(serial);
			returnItem.setAverResi(-1d);
			returnItem.setBeginResi(-1d);
			returnItem.setEndResi(-1d);
			returnItem.setFilterf(filterFKey);
			returnItem.setLength(itemLength);
			returnItem.setStandard(AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency));
			returnItem.setWindSpeed(velocity);
			return returnItem;
		}
		int i = 0;
		for (ALSingleFilterResistance itm : itemList) {
			windspeedList.add(itm.getWindSpeed());
			dinitialPDList.add(itm.getBeginResi());
			deveragePDList.add(itm.getAverResi());
			dfinalPDList.add(itm.getEndResi());
			windspeedDouble[i] = itm.getWindSpeed();
			dinitialPDDouble[i] = itm.getBeginResi();
			deveragePDDouble[i] = itm.getAverResi();
			dfinalPDDouble[i] = itm.getEndResi();
			i++;
		}
		double[] aver = LineCoefficientUnit.PolyFit(windspeedDouble, deveragePDDouble, new double[10], 2);
		double[] begin = LineCoefficientUnit.PolyFit(windspeedDouble, dinitialPDDouble, new double[10], 2);
		double[] end = LineCoefficientUnit.PolyFit(windspeedDouble, dfinalPDDouble, new double[10], 2);
		returnItem.setAverResi(LineCoefficientUnit.getY(velocity, windspeedDouble, aver, 2));
		returnItem.setBeginResi(LineCoefficientUnit.getY(velocity, windspeedDouble, begin, 2));
		returnItem.setEndResi(LineCoefficientUnit.getY(velocity, windspeedDouble, end, 2));
		returnItem.setAhu(serial);
		returnItem.setFilterf(filterFKey);
		returnItem.setLength(itemLength);
		returnItem.setStandard(AhuMetadata.getALStandardCode(fitlerStandard, filterEfficiency));
		returnItem.setWindSpeed(velocity);
		return returnItem;
	}

	private static Map<String, ALSingleFilterResistance> getALResistanceTable(
			List<ALSingleFilterResistance> resistances) {
		if (resistanceALTable == null) {
			resistanceALTable = new HashMap<>();
			for (ALSingleFilterResistance resistance : resistances) {
				String key = resistance.getFilterf() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + resistance.getRimThick() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + resistance.getMaterialE()
						+ ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + ValueFormatUtil.transDouble2String(resistance.getWindSpeed());
				resistanceALTable.put(key, resistance);
			}
		}
		return resistanceALTable;
	}

	/**
	 * 冷水盘管段段长计算
	 *
	 * @param serial
	 *            序列号
	 * @param eliminator
	 *            挡水器
	 * @return
	 */
	public static Integer sectionLengthCoolingCoil(String serial, String eliminator) {
		try {
			String cutSerial = serial.substring(serial.length() - 4, serial.length());
			Integer ser = Integer.parseInt(cutSerial);
			if (ser >= 2532) {
				return 12;
			} else {
				if (EmptyUtil.isEmpty(eliminator)) {
					return 6;
				} else {
					// return 5;
					double eli = Double.parseDouble(eliminator);
					if (eli > 0) {
						return 5;
					} else {
						return 6;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Failed to calculate sectionL for Cooling Coil: " + serial + " & " + eliminator, e);
			return 0;
		}
	}

	/**
	 * 风机段长计算
	 *
	 * @param serial
	 *            序列号
	 * @param fantype
	 *            风机类型
	 * @param airdir
	 *            出风方向
	 * @param outlet
	 *            风机形式
	 * @param position
	 *            电机位置
	 * @param standbyMotorFlag
	 *            备用电机
	 * @param ID
	 *            id ：1.为标准供应商 属性来源-页面选择  2.立式机组
	 * @return
	 */
	public static Integer sectionLengthFan(String serial, String fantype, String airdir, String outlet, String position,
			boolean standbyMotorFlag, String ID, String motorBase) {
		SectionLength sectionLength = AhuMetadata.findOne(SectionLength.class, serial);
		// AhuFanSectionLengthGen gen = new AhuFanSectionLengthGen();
		if (EmptyUtil.isNotEmpty(sectionLength)) {
			// 1. 初始默认段长为段长表中K列数据。
			Integer fanLength = sectionLength.getK();
			// 2. 查风机高度：风机表尺寸（S_FAN）中的H列
			SFan sfan = AhuMetadata.findOne(SFan.class, serial);
			if (EmptyUtil.isNotEmpty(sfan)) {
				Integer height = sfan.getH();
				if (height > 23) {
					// 3. 如果风机高度大于23模，则段长数据从风机段大型号段长（s_sectionK_BIG）表中取得
					// s_sectionK_BIG 表中，字段名[airdir]对应页面的出风方向，具体含义如下
					// 'A': 'THF';
					// 'B': BHF';
					// 'C': 'UBF';
					// 'D': 'UBR';
					SSectionKBIG sskbig = AhuMetadata.findOne(SSectionKBIG.class, serial, formatBigFanType(fantype),
							genAirdir(airdir));
					if (EmptyUtil.isNotEmpty(sskbig)) {
						return sskbig.getLen();
					} else {
						logger.warn("Failed to calculate sectionL for Fan - 0: " + serial);
						return -1;
					}
				} else {
					// 4. 如果风机高度小于23模，根据出风形式判断：
					// a. 如果是无蜗客风机，
					// 从表s_fanLength2中取数据，条件为UNITTYPE，id, type
					//
					// b. 其他出风形式
					// • 如果电机位置是后置
					// 从表s_fanLength1中取数据，条件为UNITTYPE，id, type
					//
					// • 如果电机位置是侧置
					// 从表s_fanLength中取数据，条件为id, type
					// **如果备用电机选中standbyMotorFlag，则段长为typeLen1的值，否则段长为typeLen的值

					if (FAN_OPTION_OUTLET_WWK.equals(outlet)) {
						SFanLength2 sfanLength2 = AhuMetadata.findOne(SFanLength2.class, serial, ID, fantype);
						if (EmptyUtil.isNotEmpty(sfanLength2)) {
							return sfanLength2.getTypeLen();
						} else {
							logger.warn("Failed to calculate sectionL for Fan - 1: " + serial);
							return -1;
						}
					} else {
						if (FAN_MOTORPOSITION_SIDE.equals(position)) {
							// String ID = ServiceConstant.SYS_BLANK;// TO DO what is this ?
							String regEx = "[^0-9]";
							Pattern p = Pattern.compile(regEx);
							Matcher m = p.matcher(fantype);

							SFanLength sfanLength = AhuMetadata.findOne(SFanLength.class, ID, m.replaceAll(ServiceConstant.SYS_BLANK).trim());
							if (EmptyUtil.isNotEmpty(sfanLength)) {
								if (standbyMotorFlag) {
									return sfanLength.getTypeLen1();
								} else {
									return sfanLength.getTypeLen();
								}
							} else {
								logger.warn("Failed to calculate sectionL for Fan - 2: " + serial);
								return -1;
							}
						}
						if (FAN_MOTORPOSITION_BACK.equals(position)) {
							// String ID = ServiceConstant.SYS_BLANK;// TO DO what is this ?
							// NOTE: It may be wrong substring here
							fantype = fantype.replaceAll("SYW", ServiceConstant.SYS_BLANK).replaceAll("RH", ServiceConstant.SYS_BLANK).replaceAll("R", ServiceConstant.SYS_BLANK)
									.replaceAll("BC", ServiceConstant.SYS_BLANK).replaceAll("FC", ServiceConstant.SYS_BLANK).replaceAll("K", ServiceConstant.SYS_BLANK);
							
							SFanLength1 sfanLength1 = null;
							SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, null,null,motorBase);
							if(skFanMotor!=null) {
								List<SFanLength1> lengthList = AhuMetadata.findList(SFanLength1.class, serial, ID, fantype);
								if(lengthList!=null && lengthList.size()>0) {
								for(SFanLength1 mortor:lengthList) {
									if(mortor.getSeq()>=skFanMotor.getSeq()) {
										sfanLength1=mortor;
										break;
									}
								}
								}
							}
							
							if (EmptyUtil.isNotEmpty(sfanLength1)) {
								if(SystemCalculateConstants.FAN_OUTLETDIRECTION_THF.equals(airdir)||SystemCalculateConstants.FAN_OUTLETDIRECTION_BHF.equals(airdir)){
									return sfanLength1.getThfLen();
								}
								
								return sfanLength1.getUbfLen();
								
							} else {
								logger.warn("Failed to calculate sectionL for Fan - 3: " + serial);
								return -1;
							}
						}
						logger.warn("Failed to calculate sectionL for Fan - 4: " + serial);
						return -1;
					}
				}
			} else {
				logger.warn("Failed to calculate sectionL for Fan - 5: " + serial);
				return -1;
			}
		} else {
			logger.warn("Failed to calculate sectionL for Fan - 6: " + serial);
			return -1;
		}
	}

	/**
	 * 混合段段长计算
	 *
	 * @param serial
	 *            机组序列号
	 * @param style
	 *            混合形式 1："顶部出风" 2："后出风" 3："左侧出风" 3："右侧出风" 1："底部出风"
	 * @return
	 */
	public static Integer SectionLengthMix(double airVolume, String serial, String style, boolean isReturnBack) {
		/*
		 * 数据准备： 1． 根据机组取到机组高度unitheight（06）和宽度unitwidth（08）的值
		 * 例：比如我们要选型的型号是39CQ0608，机组高度为06，机组宽度为08。 2．
		 * 根据[功能段段长信息表]取到段长数据，A列--〉[表段长]，AMIN列-〉[表最小段长] 3． 如果机组系列不同，则公式不同： •
		 * 若为39CQ系列：fC:=(unitwidth-1)*100+128-72 • 若为39G系列：fC := (unitwidth - 1) * 100 +
		 * 48 - 72; • 若为39XT系列：fC := (unitwidth - 1) * 100 + 76 - 72; 4．
		 * fa:=系统风量/(fc/1000)/8*1000 5．
		 * 从[混合段尺寸数据]表中按照机组型号，混合形式查找数据，选大于等于fa，最接近的一条数据。--〉[最小段长]
		 * 
		 * 混合形式代码： 1："顶部出风" 2："后出风" 3："左侧出风" 3："右侧出风" 1："底部出风" 段长计算： a.
		 * 若混合形式为：后回风，则段长数据为[表段长] b. 若为其他混合方式，则 • [最小段长]>[表最小段长]，则段长为[最小段长] •
		 * [表最小段长]>[表段长]，则段长为[表段长]
		 */
		// String hStr = serial.substring(serial.length() - 4, serial.length() -
		// 2);
		String wStr = serial.substring(serial.length() - 2);
		// Integer unitheight = Integer.parseInt(hStr);
		Integer unitwidth = Integer.parseInt(wStr);

		SectionLength sectionLength = AhuMetadata.findOne(SectionLength.class, serial);
		Integer mixA = sectionLength.getA();
		Integer mixAMIN = sectionLength.getAMIN();

		Integer fc = -1;
		if (serial.startsWith(ServiceConstant.SYS_UNIT_SERIES_39CQ)) {
			fc = (unitwidth - 1) * 100 + 128 - 72;
		} else if (serial.startsWith(ServiceConstant.SYS_UNIT_SERIES_39G)) {
			fc = (unitwidth - 1) * 100 + 488 - 72;
		} else if (serial.startsWith(ServiceConstant.SYS_UNIT_SERIES_39XT)) {
			fc = (unitwidth - 1) * 100 + 76 - 72;
		} else {
			return -1;
		}
		// Map<String, MetaValue> metaValues =
		// MetaValueGen.getInstance().getMetaValueMap();
		double fa = airVolume / (fc / 1000) / 8 * 1000;

		if (isReturnBack) {
			return mixA;
		} else {
			if (mixAMIN > mixA) {
				return mixA;
			} else {
				List<MixSize> mixSizeList = AhuMetadata.findList(MixSize.class, AhuUtil.getUnitSeries(serial), style);
				List<MixSize> llist = new ArrayList<>();
				for (MixSize mixSize : mixSizeList) {
					if (mixSize.getSectionL() >= fa) {
						llist.add(mixSize);
					}
				}
				Collections.sort(llist, new Comparator<MixSize>() {
					public int compare(MixSize o1, MixSize o2) {
						return new Integer(o1.getSectionL()).compareTo(new Integer(o2.getSectionL()));
					}
				});
				if (llist.isEmpty()) {
					return -1;
				} else {
					MixSize ms = llist.get(0);
					return ms.getSectionL();
				}

			}
		}
	}

	/**
	 * 混合段阻力计算
	 *
	 * @param serial
	 *            序列号
	 * @param sectionLength
	 *            实际段长
	 * @param damperOutlet
	 *            风阀材料
	 * @return
	 */
	public static Double sectionResistanceMix(double airVolume, String serial, Integer sectionLength,
			String damperOutlet) {
		/*
		 * a. 如果 [表段长]>[实际段长] 阻力=[空段阻力（表段长）] +（[表段长]-[实际段长]） b. 反之，则 阻力=[空段阻力（实际段长）] +
		 * 阀门阻力 阀门材料与阻力关系：法兰 / 0 Pa；其他 / 10 Pa
		 */
		SectionLength secLength = AhuMetadata.findOne(SectionLength.class, serial);
		Integer mixA = secLength.getA();// mix表段长

		if (mixA > sectionLength) {// 计算与旧软件不符合，暂时非法兰情况+10PA
			if (MIX_DAMPEROUTLET_FD.equals(damperOutlet)) {
				return sectionResistanceAccess(airVolume, serial, mixA) + mixA - new Double(sectionLength);
			} else {
				return sectionResistanceAccess(airVolume, serial, mixA) + mixA - new Double(sectionLength) + 10d;
			}
		} else {
			if (MIX_DAMPEROUTLET_FD.equals(damperOutlet)) {
				return sectionResistanceAccess(airVolume, serial, sectionLength) + 0d;
			} else {
				return sectionResistanceAccess(airVolume, serial, sectionLength) + 10d;
			}
		}
	}

	/**
	 * 空段阻力计算
	 *
	 * @param serial
	 *            序列号
	 * @param sectionLength
	 *            段长
	 * @return
	 */
	public static Double sectionResistanceAccess(double airVolume, String serial, Integer sectionLength) {

		/*
		 * 输入参数：段编号，段长
		 * 
		 * 1．实际风速计算： a. 根据[各功能段迎风面积]数据表，取得当前段的迎风面积area b. 取得系统风量airvolume c.
		 * 计算得出风速：velocity = airvolume / area
		 * 
		 * 2．最小风速（velmin）和最大风速（velmax）的取得：
		 * 在[s_resistance]表中，根据段长查找得到当前段长下的最大风速（velmax）和最 小风速（velmin）
		 * 3．查表[s_resistance]得到阻力的值： a. 如果实际风速（velocity）<最低风速（velmin），则阻力取最低风速 b.
		 * 如果实际风速（velocity）〉最高风速（velmax），则阻力取最高风速 c.
		 * 如果实际风速（velocity）介于最小风速（velmin）和最大风速（velmax）中间，则阻力取比它高一档的风速
		 */
		try {
			// Map<String, MetaValue> metaValues =
			// MetaValueGen.getInstance().getMetaValueMap();

			SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
			double area = sectionArea.getO();// mix迎风面积
			double velocity = airVolume / 3600.0 / area;
			List<AccessData> accessDataList = AhuMetadata.findList(AccessData.class, String.valueOf(sectionLength));
			if (accessDataList.isEmpty()) {
				logger.warn("计算空段阻力的时候--查表未得到数据");
				return -1d;
			}
			Collections.sort(accessDataList, new Comparator<AccessData>() {
				public int compare(AccessData o1, AccessData o2) {
					return new Double(o1.getVelocity()).compareTo(new Double(o2.getVelocity()));
				}
			});
			double velmin = accessDataList.get(0).getVelocity();
			double velmax = accessDataList.get(accessDataList.size() - 1).getVelocity();
			if (velocity < velmin) {
				return velmin;
			} else if (velocity > velmax) {
				return velmax;
			} else {
				for (AccessData accessData : accessDataList) {
					if (accessData.getVelocity() > velocity) {
						return accessData.getResistance();
					}
				}
			}
		} catch (Exception e) {
			logger.error("计算空段阻力的时候异常", e);
			return -1d;
		}
		return -1d;
	}

	/**
	 * 高效过滤段阻力计算
	 *
	 * @param serial
	 * @param filterF
	 * @param fitlerStandard
	 * @return
	 */
	public static HEPAFilterResistance sectionResistanceHEPAFilter(double airVolume, String serial, String filterF,
			String fitlerStandard, String filterEfficiency) {

		// Map<String, MetaValue> metaValues =
		// MetaValueGen.getInstance().getMetaValueMap();
		SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
		double area = sectionArea.getV();// 四分管下的迎风面积
		double velocity = airVolume / 3600.0 / area;

		String filterFKey = ServiceConstant.SYS_BLANK;
		// TODO -Simon v型箱型数据
		if (ServiceConstant.SYS_BLANK.equals(filterF) || ServiceConstant.JSON_FILTER_FILTERF_P.equals(filterF)) {// 箱型
			filterFKey = ServiceConstant.SYS_STRING_NUMBER_2;
		} else if ("V".equals(filterF)) {// V型
			filterFKey = ServiceConstant.SYS_STRING_NUMBER_1;
		} else {
			return null;
		}

		Map<String, HEPAFilterResistance> resistanceTable = getHEPAResistanceTable(
				AhuMetadata.findAll(HEPAFilterResistance.class));
		String key = filterFKey + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN + AhuMetadata.getHEPAStandardCode(fitlerStandard, filterEfficiency);

		List<HEPAFilterResistance> itemList = new ArrayList<>();
		for (Entry<String, HEPAFilterResistance> entry : resistanceTable.entrySet()) {
			if (entry.getKey().startsWith(key)) {
				itemList.add(entry.getValue());
			}
		}
		Collections.sort(itemList, new Comparator<HEPAFilterResistance>() {
			public int compare(HEPAFilterResistance sta1, HEPAFilterResistance sta2) {
				return new Double(sta1.getWindSpeed()).compareTo(new Double(sta2.getWindSpeed()));
			}
		});
		// double[] windspeedDouble = new double[12];
		// double[] dinitialPDDouble = new double[12];
		// double[] deveragePDDouble = new double[12];
		// double[] dfinalPDDouble = new double[12];

		LinkedList<Double> windspeedList = new LinkedList<>();
		LinkedList<Double> BeginResiList = new LinkedList<>();
		LinkedList<Double> AverResiList = new LinkedList<>();
		LinkedList<Double> EndResiList = new LinkedList<>();

		HEPAFilterResistance returnItem = new HEPAFilterResistance();
		returnItem.setAhu(serial);
		returnItem.setFilterF(filterFKey);
		returnItem.setMaterialE(AhuMetadata.getALStandardCode(filterF, fitlerStandard));
		returnItem.setWindSpeed(velocity);
		if (itemList.isEmpty()) {
			returnItem.setAverResi(-1d);
			returnItem.setBeginResi(-1d);
			returnItem.setEndResi(-1d);
			return returnItem;
		}
		for (HEPAFilterResistance itm : itemList) {
			if (itm.getWindSpeed() != 0) {
				windspeedList.add(itm.getWindSpeed());
			}
			if (itm.getBeginResi() != 0) {
				BeginResiList.add(itm.getBeginResi());
			}
			if (itm.getAverResi() != 0) {
				AverResiList.add(itm.getAverResi());
			}
			if (itm.getEndResi() != 0) {
				EndResiList.add(itm.getEndResi());
			}
			/*
			 * windspeedList.add(itm.getWindSpeed()); BeginResiList.add(itm.getBeginResi());
			 * AverResiList.add(itm.getAverResi()); EndResiList.add(itm.getEndResi());
			 */
		}

		Double[] x = (Double[]) windspeedList.toArray(new Double[windspeedList.size()]);
		Double[] ya = (Double[]) AverResiList.toArray(new Double[AverResiList.size()]);
		Double[] yb = (Double[]) BeginResiList.toArray(new Double[BeginResiList.size()]);
		Double[] ye = (Double[]) EndResiList.toArray(new Double[EndResiList.size()]);
		double[] a = new double[10];
		double[] aaa = LineCoefficientUnit.PolyFit(Double2double(x), Double2double(ya), a, 2);
		returnItem.setAverResi(LineCoefficientUnit.getY(velocity, Double2double(x), aaa, 2));

		double[] aab = LineCoefficientUnit.PolyFit(Double2double(x), Double2double(yb), a, 2);
		returnItem.setBeginResi(LineCoefficientUnit.getY(velocity, Double2double(x), aab, 2));

		double[] aae = LineCoefficientUnit.PolyFit(Double2double(x), Double2double(ye), a, 2);
		returnItem.setEndResi(LineCoefficientUnit.getY(velocity, Double2double(x), aae, 2));

		return returnItem;
	}

	private static Map<String, HEPAFilterResistance> getHEPAResistanceTable(List<HEPAFilterResistance> resistances) {
		if (resistanceHEPATable == null) {
			resistanceHEPATable = new HashMap<>();
			for (HEPAFilterResistance resistance : resistances) {
				String key = resistance.getFilterF() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN
						+ resistance.getMaterialE() + ServiceConstant.SYS_PUNCTUATION_LOW_HYPHEN
						+ ValueFormatUtil.transDouble2String(resistance.getWindSpeed());
				resistanceHEPATable.put(key, resistance);
			}
		}
		return resistanceHEPATable;
	}

	private static double[] Double2double(Double[] dArray) {

		double[] returnDarray = new double[dArray.length];
		for (int i = 0; i < dArray.length; i++) {
			returnDarray[i] = null == dArray[i] ? -1d : dArray[i];
		}
		return returnDarray;
	}

	/**
	 * 静电过滤段阻力计算
	 *
	 * @param serial
	 * @param filterEfficiency
	 * @return
	 */
	public static ElectrostaticFilterResistance sectionResistanceElectostaticFilter(double airVolume, String serial,
			String filterEfficiency) {

		SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
		double area = sectionArea.getY();
		// 风速
		double velocity = airVolume / 3600.0 / area;

		try {
			List<ElectrostaticFilterResistance> resistanceList = AhuMetadata
					.findAll(ElectrostaticFilterResistance.class);
			resistanceList = sortByWindSpeed(resistanceList);

			LinkedList<Double> windspeedList = new LinkedList<>();
			LinkedList<Double> averResiList = new LinkedList<>();
			LinkedList<Double> averResiList1 = new LinkedList<>();
			for (ElectrostaticFilterResistance item : resistanceList) {
				windspeedList.add(item.getWindSpeed());
				averResiList.add(item.getAverResi());
				averResiList1.add(item.getAverResi1());
			}
			Double[] x = (Double[]) windspeedList.toArray(new Double[windspeedList.size()]);
			Double[] y = (Double[]) averResiList.toArray(new Double[averResiList.size()]);
			Double[] y1 = (Double[]) averResiList1.toArray(new Double[averResiList1.size()]);
			double[] aa = LineCoefficientUnit.PolyFit(Double2double(x),
					Double2double(ELECTROSTATICFILTER_FILTERE_F7.equals(filterEfficiency) ? y : y1), new double[10], 2);// 过滤效率F7
																														// 使用averResi
																														// 拟合，G4+F7
																														// 使用averResi1
																														// 拟合
			double averResi = LineCoefficientUnit.getY(velocity, Double2double(x), aa, 2);

			ElectrostaticFilterResistance retRes = new ElectrostaticFilterResistance();
			retRes.setWindSpeed(BaseDataUtil.decimalConvert(velocity, 2));
			retRes.setAverResi(BaseDataUtil.decimalConvert(averResi, 2));
			return retRes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private static List<ElectrostaticFilterResistance> sortByWindSpeed(
			List<ElectrostaticFilterResistance> resistanceList) {
		List<ElectrostaticFilterResistance> resistances = new ArrayList<ElectrostaticFilterResistance>();
		resistances.addAll(resistanceList);
		Collections.sort(resistances, new Comparator<ElectrostaticFilterResistance>() {
			@Override
			public int compare(ElectrostaticFilterResistance arg0, ElectrostaticFilterResistance arg1) {
				if (arg0.getWindSpeed() > arg1.getWindSpeed()) {
					return 1;
				} else if (arg0.getWindSpeed() < arg1.getWindSpeed()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return resistances;
	}

	/**
	 * 风机段对应页面的出风方向，具体含义如下 'A': 'THF'; 'B': BHF'; 'C': 'UBF'; 'D': 'UBR';
	 *
	 * @param airdir
	 * @return
	 */
	private static String genAirdir(String airdir) {
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_THF.equals(airdir)) {
			return ServiceConstant.SYS_ALPHABET_A_UP;
		}
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_BHF.equals(airdir)) {
			return ServiceConstant.SYS_ALPHABET_B_UP;
		}
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(airdir)) {
			return ServiceConstant.SYS_ALPHABET_C_UP;
		}
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(airdir)) {
			return ServiceConstant.SYS_ALPHABET_D_UP;
		}
		return airdir;
	}

	/**
	 * 获取大机组风机key，用于定位csv 段长map数据
	 *
	 * @param fantype
	 * @return
	 */
	private static String formatBigFanType(String fantype) {
		String appendK = fantype.toUpperCase().endsWith("K") ? "K" : ServiceConstant.SYS_BLANK;
		fantype = fantype.replaceAll("[^0-9]", ServiceConstant.SYS_BLANK) + appendK;
		return fantype;
	}

	/**
	 * 按照0.25向下取值
	 * <p>
	 * 例：1.26-->>1.25
	 *
	 * @param d
	 * @return
	 */
	protected static double mathFloorBy025(double d) {
		double k = d % 0.25;
		if (k == 0) {
			return d;
		}
		double c = Math.floor(d / 0.25);
		return c * 0.25;
	}

	public static void main(String[] args) {
		String serial = "39CQ1428";
		System.out.println(serial.substring(serial.length() - 2));
		System.out.println(serial.substring(serial.length() - 4, serial.length() - 2));
		System.out.println(serial.substring(0, serial.length() - 4));
		Boolean b = Boolean.parseBoolean(null);
		System.out.println(b);
	}
}
