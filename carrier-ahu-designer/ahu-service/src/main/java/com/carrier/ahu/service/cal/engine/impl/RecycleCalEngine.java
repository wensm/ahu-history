package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.HEAT_RECYCLE_ENGINE_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonS;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.engine.HeatExchangeEngineException;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleBean;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.service.heatRecycle.HeatRecycleService;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;

/**
 * Created by LIANGD4 on 2018/01/12.
 * 热回收：转轮 板式
 */
@Component
public class RecycleCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(RecycleCalEngine.class.getName());

    @Autowired
    private HeatRecycleService heatRecycleService;

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        try {
            logger.info("RecycleCalEngine calculator line begin");
            HeatRecycleParam heatRecycleParam = new InvokeTool<HeatRecycleParam>().genInParamFromAhuParam(ahu, section, seasonS, new HeatRecycleParam());
            String heatRecycleType = "";
            if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(section.getKey())) {
                heatRecycleType = ServiceConstant.METASEXON_WHEELHEATRECYCLE_HEATWHEEL;
            } else if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(section.getKey())) {
                heatRecycleType = ServiceConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE;
            }
            heatRecycleParam.setHeatRecoveryType(heatRecycleType);
            List<HeatRecycleInfo> heatRecycleInfo = heatRecycleService.getHeatRecycleEngineByRule(heatRecycleParam, null, AHUContext.getAhuVersion());
            HeatRecycleBean heatRecycleBean = new HeatRecycleBean();
            heatRecycleBean.setHeatRecycleInfoList(heatRecycleInfo);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {//如果计算结果不为null封装热回收效率、阻力字段
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_HEATRECOVERYEFFICIENCY, BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(heatRecycleInfo.get(0).getReturnSEfficiency()), 1));//热回收效率
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_RESISTANCE, BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(heatRecycleInfo.get(0).getReturnSAirResistance()), 1));//阻力
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_HEATRECYCLEDETAIL,JSONArray.toJSON(heatRecycleInfo));//阻力
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_SECTIONSIDEL,JSONArray.toJSON(heatRecycleInfo.get(0).getSectionSideL()));//截面边长
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_HEATEXCHANGERL,JSONArray.toJSON(heatRecycleInfo.get(0).getHeatExchangerL()));//热交换器长度
                logger.info("RecycleCalEngine heatRecycleInfo json : " + JSONArray.toJSONString(heatRecycleInfo));
                Map<String, Object> map = new InvokeTool<HeatRecycleBean>().reInvoke(heatRecycleBean, seasonS, section.getKey());
                section.getParams().putAll(map);// 转换计算
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("RecycleCalEngine calculator line end");
            return subc;
        } catch (HeatExchangeEngineException e) {
            String message = getIntlString(HEAT_RECYCLE_ENGINE_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("RecycleCalEngine debug massage :" + message, e.getMessage());
            logger.error("RecycleCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HeatExchangeEngineException(HEAT_RECYCLE_ENGINE_CALCULATION_FAILED);
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{SectionTypeEnum.TYPE_WHEELHEATRECYCLE,
                SectionTypeEnum.TYPE_PLATEHEATRECYCLE};
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_RECYCLE;
    }

}
