package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.AhuTemplate;
import com.carrier.ahu.service.AhuTemplateService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AhuTemplateServiceImpl extends AbstractService implements AhuTemplateService {
	
	@Override
	public List<AhuTemplate> list() {
	   List<AhuTemplate> list = (List<AhuTemplate>)ahuTemplateDao.findAll();
       return list;
	}
	
	@Override
	public String addAhuTempate(AhuTemplate ahuTemplate) {
		ahuTemplateDao.save(ahuTemplate);
		return ahuTemplate.getId(); 
	}

	@Override
	public String updateAhuTempate(AhuTemplate ahuTemplate) {
		ahuTemplateDao.save(ahuTemplate);
		return ahuTemplate.getId(); 
	}

}
