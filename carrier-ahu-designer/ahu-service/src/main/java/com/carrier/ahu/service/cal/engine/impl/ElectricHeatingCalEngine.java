package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.length.param.ElectricHeatingParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.ECoilDimension;
import com.carrier.ahu.metadata.entity.validation.S4GRow;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.SystemCalculateConstants;

@Component
public class ElectricHeatingCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(ElectricHeatingCalEngine.class.getName());

    @Override
    public int getOrder() {
        return 5;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("ElectricHeatingCalEngine calculator line begin");
        CalOperationEnum calOperationEnum = context.getCalOperation();
        //转换计算使用参数
        ElectricHeatingParam electricHeatingParam = new InvokeTool<ElectricHeatingParam>().genInParamFromAhuParam(ahu, section, null, new ElectricHeatingParam());
        try {
            logger.info("ElectricHeatingCalEngine calculator param :" + JSONArray.toJSONString(electricHeatingParam));
            String rowNum = getRow(electricHeatingParam);
            section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_ROWNUM, rowNum);//添加排数
            List<String> groupCList = calGroupC(electricHeatingParam);//计算组数
            if (!EmptyUtil.isEmpty(groupCList)) {//加工计算结果
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_GROUPC, groupCList.get(0));//添加组数
            } else {//无法计算出结果
                throw new CalculationException(ErrorCode.QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("ElectricHeatingCalEngine calculator line end");
            return subc;
        } catch (CalculationException e) {
            String message = getIntlString(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN, AHUContext.getLanguage());
            logger.debug("ElectricHeatingCalEngine debug massage :" + message, e.getMessage());
            logger.error("ElectricHeatingCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new CalculationException(e.getErrorCode(), JSONArray.toJSONString((e.getParams())).replaceAll("\\[","").replaceAll("\\]","").replaceAll("\"",""));
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL};
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_ELECTRICHEATINGCOIL;
    }

    /*计算排数*/
    private String getRow(ElectricHeatingParam electricHeatingParam) {
        double WHeatQ = 0.00;
        double SHeatQ = electricHeatingParam.getSHeatQ();//夏季热量
        String serial = electricHeatingParam.getSerial();//机组型号
        double heatQ = electricHeatingParam.getSHeatQ();//默认取夏季热量
        if(electricHeatingParam.isEnableWinter()){//冬季生效
            WHeatQ = electricHeatingParam.getWHeatQ();//冬季热量
        }
        if (WHeatQ > SHeatQ) {
            heatQ = WHeatQ;
        }
        S4GRow s4GRow = AhuMetadata.findOne(S4GRow.class, serial);
        if (!EmptyUtil.isEmpty(s4GRow)) {
            if (heatQ < s4GRow.getRow1()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_1R;
            } else if (heatQ < s4GRow.getRow2()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_2R;
            } else if (heatQ < s4GRow.getRow3()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_3R;
            } else {
                throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
            }
        } else {
            throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
        }
    }

    /*计算组数*/
    private List<String> calGroupC(ElectricHeatingParam electricHeatingParam) {
        double WHeatQ = 0.00;//冬季热量
        double SHeatQ = electricHeatingParam.getSHeatQ();//夏季热量
        String serial = electricHeatingParam.getSerial();//机组型号
        double heatQ = electricHeatingParam.getSHeatQ();//默认取夏季热量
        if(electricHeatingParam.isEnableWinter()){//冬季生效
            WHeatQ = electricHeatingParam.getWHeatQ();//冬季热量
        }
        if (WHeatQ > SHeatQ) {
            heatQ = WHeatQ;
        }
//        serial = serial.replace("CQ", "G");//TODO 数据源为G数据
        ECoilDimension eCoilDimension = AhuMetadata.findOne(ECoilDimension.class, serial);
        if (!EmptyUtil.isEmpty(eCoilDimension)) {
            List<String> stringList = new ArrayList<String>();
            double minstage = Math.round(heatQ / 50 + 0.5);
            int n = BaseDataUtil.doubleConversionInteger((BaseDataUtil.stringConversionDouble(eCoilDimension.getN()) + 1) / 3);
            for (int i = 1; i <= n; i++)
                if ((BaseDataUtil.stringConversionDouble(eCoilDimension.getN()) + 1) % i == 0) {
                    if (i >= minstage) {
                        switch (i) {
                            case 1:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_1G);break;
                            case 2:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_2G);break;
                            case 3:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_3G);break;
                            case 4:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_4G);break;
                            case 5:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_5G);break;
                            case 6:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_6G);break;
                            case 7:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_7G);break;
                            case 8:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_8G);break;
                            case 9:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_9G);break;
                        }
                    }
                }
            return stringList;
        } else {
            throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
        }
    }

    @Override
    public int getCalLevel() {
        return CalOperationEnum.BATCH.getId();
    }
}
