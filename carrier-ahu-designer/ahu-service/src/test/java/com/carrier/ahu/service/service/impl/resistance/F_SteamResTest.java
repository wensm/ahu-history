package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 蒸汽盘管段段逻辑
 */
public class F_SteamResTest {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getSteamRes0608() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_STEAMCOIL.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getSteamRes3438() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_STEAMCOIL.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

}
