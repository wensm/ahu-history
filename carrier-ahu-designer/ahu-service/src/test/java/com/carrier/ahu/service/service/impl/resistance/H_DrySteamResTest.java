package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 干蒸汽加湿段
 */
public class H_DrySteamResTest implements SystemCalculateConstants {

    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getDrySteamRes0608() throws Exception {
       ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 0, 0.5);
    }

    @Test
    public void getDrySteamRes1317() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 1, 0.1);
    }

    @Test
    public void getDrySteamRes3438() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 1, 0.2);
    }
}
