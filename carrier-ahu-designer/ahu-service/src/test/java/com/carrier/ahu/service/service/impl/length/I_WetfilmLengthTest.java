package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 湿膜加湿段
 * 段长固定0
 */
public class I_WetfilmLengthTest {

    private static final String version = "CN-HTC-1.0";
    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getWetfilmLength() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode());
        lengthParam.setVersion(version);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }
}
