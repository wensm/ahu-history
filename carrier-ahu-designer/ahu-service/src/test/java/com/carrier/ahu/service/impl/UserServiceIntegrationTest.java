package com.carrier.ahu.service.impl;

import static org.junit.Assert.assertEquals;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.enums.FactoryEnum;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.service.IntegrationTestConfiguration;
import com.carrier.ahu.service.UserService;

/**
 * User Meta Service Integration Test.
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
@ActiveProfiles({ "test" })
@RunWith(SpringRunner.class)
@Import(IntegrationTestConfiguration.class)
public class UserServiceIntegrationTest {

    @Autowired
    private UserService userService;

    @Test
    public void testUserMetaCRUD() {
        UserMeta userMeta = UserMetaEnum.COLD_COOLING_SELECTION_SIZE.newUserMeta("test");

        // should use LicenseManager to get Factory id from license file
        // LicenseManager.getLicenseInfo().getFactory()
        userMeta.setFactoryId(FactoryEnum.THC.name());
//        userMeta.setMetaKey("meta.defaultPara.CalculationNumber");//个人信息-配置项标识
        userMeta.setMetaValue("5");

        this.userService.saveUserMeta(userMeta);

        UserMeta savedUserMeta = this.userService.findUserMetaById(userMeta.getUserMetaId());

        assertEquals(userMeta.toString(), savedUserMeta.toString());

        userMeta = UserMetaEnum.COLD_COOLING_SELECTION_SIZE.newUserMeta("test1");
        userMeta.setFactoryId(FactoryEnum.THC.name());
//        userMeta.setMetaKey("meta.defaultPara.CalculationNumber");//个人信息-配置项标识
        userMeta.setMetaValue("10");

        this.userService.saveUserMeta(userMeta);

        savedUserMeta = this.userService.findUserMetaById(userMeta.getUserMetaId());
        assertEquals(userMeta.toString(), savedUserMeta.toString());

        assertEquals(2, IterableUtils.size(this.userService.findAllUserMeta()));

        this.userService.deleteUserMeta(userMeta.getUserMetaId());

        assertEquals(1, IterableUtils.size(this.userService.findAllUserMeta()));
    }

}
