package com.carrier.ahu.service.impl;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.KrugerChartInfo;
import com.carrier.ahu.engine.fan.KrugerChartResult;
import com.carrier.ahu.engine.fan.KrugerCurvePoints;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.fan.KrugerSelection;
import com.carrier.ahu.engine.fan.KrugerSoundSpectrumEx;
import com.carrier.ahu.service.IntegrationTestConfiguration;

/**
 * Kruger Engine REST API Service Integration Test.
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
@ActiveProfiles({ "test" })
@RunWith(SpringRunner.class)
@Import(IntegrationTestConfiguration.class)
public class KrugerEngineServiceIntegrationTest {

    @Autowired
    private KrugerEngineService krugerEngineService;

    private KrugerSelectInfo getKrugerSelectInfo() {
        KrugerSelectInfo krugerSelectInfo = new KrugerSelectInfo();
        krugerSelectInfo.setVolume(50000);
        krugerSelectInfo.setPressure(500);
        krugerSelectInfo.setPressureType(0);
        krugerSelectInfo.setVolumeUnit(1);
        krugerSelectInfo.setPressureUnit(0);
        krugerSelectInfo.setCallType(0);
        krugerSelectInfo.setFanSize(0);
        krugerSelectInfo.setMinStyle(1);
        krugerSelectInfo.setMinClass(0);
        krugerSelectInfo.setSoundDistance(1);
        krugerSelectInfo.setHz(50);
        krugerSelectInfo.setTemperature(20);
        krugerSelectInfo.setTemperatureUnit(0);
        krugerSelectInfo.setSoundDistanceUnit(1);
        krugerSelectInfo.setAltitude(0);
        krugerSelectInfo.setAltitudeUnit(0);
        krugerSelectInfo.setServiceFactor(1.3f);
        krugerSelectInfo.setOutletType(0);
        krugerSelectInfo.setBladeType(0);
        krugerSelectInfo.setProductType(0);
        krugerSelectInfo.setFanCasing(2);
        krugerSelectInfo.setFanWidth(1);
        krugerSelectInfo.setVelocityUnit(0);
        krugerSelectInfo.setSoundCondition(0);
        krugerSelectInfo.setFanGroup(0);
        krugerSelectInfo.setFanArrangement(0);
        krugerSelectInfo.setDriveType(0);
        krugerSelectInfo.setDebug(0);
        krugerSelectInfo.setPhase(0);
        krugerSelectInfo.setPlenumChamberDesign(0);
        krugerSelectInfo.setPlenumOutletDesign(0);
        krugerSelectInfo.setVolts(0);
        krugerSelectInfo.setPlenumOutletDiameter(0);
        krugerSelectInfo.setPlenumChamberHeight(0);
        krugerSelectInfo.setPlenumChamberWidth(0);
        krugerSelectInfo.setPlenumChamberLength(0);
        krugerSelectInfo.setPlenumOutletType(0);
        krugerSelectInfo.setPlenumOutletWidth(0);
        krugerSelectInfo.setPlenumOutletHeight(0);
        krugerSelectInfo.setSpeed(0);
        krugerSelectInfo.setInverter(0);
        krugerSelectInfo.setDebugSize(0);
        krugerSelectInfo.setDebugType(null);
        return krugerSelectInfo;
    }

    @Test
    public void testKrugerSelect() {
        KrugerSelectInfo krugerSelectInfo = getKrugerSelectInfo();
        List<KrugerSelection> krugerSelections = krugerEngineService.select(krugerSelectInfo);
        for (KrugerSelection krugerSelection : krugerSelections) {
            System.out.println(krugerSelection.getProduct());
        }
        assertNotNull(krugerSelections);
    }

    @Test
    public void testCurvePoints() {
        KrugerSelectInfo krugerSelectInfo = getKrugerSelectInfo();
        List<KrugerSelection> krugerSelections = krugerEngineService.select(krugerSelectInfo);
        assertNotNull(krugerSelections);

        KrugerCurvePoints krugerCurvePoints = krugerEngineService.curvePoints(krugerSelections.get(0));
        assertNotNull(krugerCurvePoints);
    }

    @Test
    public void testKrugerSoundSpectrumEx() {
        KrugerSelectInfo krugerSelectInfo = getKrugerSelectInfo();
        List<KrugerSelection> krugerSelections = krugerEngineService.select(krugerSelectInfo);
        assertNotNull(krugerSelections);

        KrugerSoundSpectrumEx krugerSoundSpectrumEx = krugerEngineService.soundSpectrumEx(krugerSelections.get(0));
        assertNotNull(krugerSoundSpectrumEx);
    }

    @Test(expected = FanEngineException.class)
    public void testKrugerSelectException() {
        List<KrugerSelection> krugerSelections = krugerEngineService.select(null);
        assertNotNull(krugerSelections);
    }

    @Test
    public void testKrugerGenerateChart() {
        KrugerSelectInfo krugerSelectInfo = getKrugerSelectInfo();
        List<KrugerSelection> krugerSelections = krugerEngineService.select(krugerSelectInfo);
        assertNotNull(krugerSelections);

        KrugerChartInfo krugerChartInfo = new KrugerChartInfo(krugerSelections.get(0));
        krugerChartInfo.setWidth(500);
        krugerChartInfo.setHeight(500);
        krugerChartInfo.setChartName("chart");
        krugerChartInfo.setChartType(1);
        krugerChartInfo.setChartPath("C://CarrierAHU//");

        KrugerChartResult krugerChartResult = krugerEngineService.generateChart(krugerChartInfo);
        assertNotNull(krugerChartResult);
    }

}
