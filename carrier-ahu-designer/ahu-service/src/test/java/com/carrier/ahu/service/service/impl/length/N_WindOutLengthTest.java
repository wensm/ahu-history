package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class N_WindOutLengthTest {

    private static final String version = "CN-HTC-1.0";
    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getWindOutLengthTDN() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("T");
        lengthParam.setDoorType("DN");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("TDN"+length);
    }

    @Test
    public void getWindOutLengthTDY() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("T");
        lengthParam.setDoorType("DY");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("TDY"+length);
    }

    @Test
    public void getWindOutLengthTND() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("T");
        lengthParam.setDoorType("ND");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("TND"+length);
    }

    @Test
    public void getWindOutLengthADN() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("A");
        lengthParam.setDoorType("DN");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("ADN"+length);
    }

    @Test
    public void getWindOutLengthADY() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("A");
        lengthParam.setDoorType("DY");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("ADY"+length);
    }

    @Test
    public void getWindOutLengthAND() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("A");
        lengthParam.setDoorType("ND");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("AND"+length);
    }

    @Test
    public void getWindOutLengthLDN() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("L");
        lengthParam.setDoorType("DN");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("LDN"+length);
    }

    @Test
    public void getWindOutLengthLDY() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("L");
        lengthParam.setDoorType("DY");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("LDY"+length);
    }

    @Test
    public void getWindOutLengthLND() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("L");
        lengthParam.setDoorType("ND");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("LND"+length);
    }

    @Test
    public void getWindOutLengthRDN() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("R");
        lengthParam.setDoorType("DN");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("RDN"+length);
    }

    @Test
    public void getWindOutLengthRDY() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("R");
        lengthParam.setDoorType("DY");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("RDY"+length);
    }

    @Test
    public void getWindOutLengthRND() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("R");
        lengthParam.setDoorType("ND");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("RND"+length);
    }

    @Test
    public void getWindOutLengthFDN() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("F");
        lengthParam.setDoorType("DN");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("FDN"+length);
    }

    @Test
    public void getWindOutLengthFDY() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_DISCHARGE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2338");
        lengthParam.setOutType("F");
        lengthParam.setDoorType("DY");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("FDY"+length);
    }

}
