package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 消音段
 */
public class M_ClearSoundResTest {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getClearSoundRes() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ATTENUATOR.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
        resistanceParam.setSairvolume(2000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

}
