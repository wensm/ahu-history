package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 混合段
 */
public class A_MixedResTest implements SystemCalculateConstants {

    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getMixedRes() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
//        resistanceParam.setSectionLength(5);
        double resistance = resistanceService.getResistance(resistanceParam);
        Assert.assertEquals("0608_MixedRes_Error", resistance, 10, 2);//1.5
    }

    @Test
    public void getMixedResFlange() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
//        resistanceParam.setSectionLength(5);
        resistanceParam.setDamperOutlet(MIX_DAMPEROUTLET_FD);//法兰
        double resistance = resistanceService.getResistance(resistanceParam);
        Assert.assertEquals("0608_MixedRes_Error", resistance, 0, 2);//1.5
    }

    @Test
    public void getMixedRes3438() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
//        resistanceParam.setSectionLength(11);
        double resistance = resistanceService.getResistance(resistanceParam);
        Assert.assertEquals("3438_MixedRes_Error", resistance, 16, 0.25);//0.25
    }

    @Test
    public void getMixedResFlange3438() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
//        resistanceParam.setSectionLength(11);
        resistanceParam.setDamperOutlet(MIX_DAMPEROUTLET_FD);//法兰
        double resistance = resistanceService.getResistance(resistanceParam);
        Assert.assertEquals("3438_MixedRes_Error", resistance, 6, 0.25);//0.25
    }
}
