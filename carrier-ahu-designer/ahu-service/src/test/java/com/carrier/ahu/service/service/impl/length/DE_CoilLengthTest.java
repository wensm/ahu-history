package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 */
public class DE_CoilLengthTest implements SystemCalculateConstants {

    //冷水盘管、热水盘管同理
    //机组型号>=2532 应该是12 M
    //机组型号<2532 含档水器 5M 测试结果6M
    //机组型号<2532 不含档水器 6M


    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getCoilLength() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_COLD.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2328");
//        lengthParam.setEliminator(false);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getCoilLength1() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_COLD.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2328");
//        lengthParam.setEliminator(false);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getCoilLength2532() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_COLD.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2532");
//        lengthParam.setEliminator(false);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }
}
