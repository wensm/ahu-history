package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 电加热盘管段
 * 段长固定值为3M
 */
public class G_ElectricityLengthTest {

    private static final String version = "CN-HTC-1.0";
    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getElectricityLength() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode());
        lengthParam.setVersion(version);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }
}
