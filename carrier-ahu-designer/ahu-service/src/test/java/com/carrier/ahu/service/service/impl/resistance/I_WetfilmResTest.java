package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 湿膜加湿段
 */
public class I_WetfilmResTest implements SystemCalculateConstants {

    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getWetfilmRes1317S() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(0);
        resistanceParam.setSupplier("B");
        resistanceParam.setSeason(seasonS);
        resistanceParam.setSHumidificationQ(20);
        resistanceParam.setSInDryBulbT(40.0);
        resistanceParam.setSInWetBulbT(15.0);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 41, 0.5);
    }

    @Test
    public void getWetfilmRes1317W() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(0);
        resistanceParam.setSupplier("B");
        resistanceParam.setSeason(seasonW);
        resistanceParam.setWHumidificationQ(20);
        resistanceParam.setWInDryBulbT(10);
        resistanceParam.setWInWetBulbT(2);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 68, 0.5);
    }

    @Test
    public void getWetfilmRes1317All() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(0);
        resistanceParam.setSupplier("B");
        resistanceParam.setSeason(seasonAll);
        resistanceParam.setSHumidificationQ(20);
        resistanceParam.setWHumidificationQ(20);
        resistanceParam.setSInDryBulbT(30.0);
        resistanceParam.setSInWetBulbT(15.0);
        resistanceParam.setWInDryBulbT(10.0);
        resistanceParam.setWInWetBulbT(2.0);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 68, 0.5);
    }
}
