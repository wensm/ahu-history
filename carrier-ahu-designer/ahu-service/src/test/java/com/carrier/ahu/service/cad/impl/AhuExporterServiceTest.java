package com.carrier.ahu.service.cad.impl;

import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liaoyw on 2017/4/16.
 */
public class AhuExporterServiceTest {
    @Test
    public void testCreateBmp() {
        AhuParam ap = new AhuParam();
        ap.setUnitNo("115");
        ap.setPid("439");
        ap.setUnitid("554");
        ap.setProduct("cq-aq123");
        List<PartParam> list = new ArrayList<>();
        list.add(new PartParam((short)1));
        list.add(new PartParam((short)3));
        list.add(new PartParam((short)6));
        list.add(new PartParam((short)1));
        list.add(new PartParam((short)1));
        ap.setPartParams(list);
        new AhuExporterServiceImpl().createBmp(ap);
    }
}
