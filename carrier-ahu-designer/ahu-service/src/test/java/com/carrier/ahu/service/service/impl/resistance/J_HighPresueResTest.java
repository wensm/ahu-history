package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 高压喷雾加湿段
 */
public class J_HighPresueResTest {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getHighPresueRes() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

}
