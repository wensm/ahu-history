package com.carrier.ahu.engine.fan.factory;

import org.junit.Test;

import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx3ParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/7/6.
 */
public class WindMachineTest implements SystemCalculateConstants {

    @Test
    public void windMachineEngineCalculation() throws Exception {

        //1.供应商：A、风机形式：无蜗壳风机、风机高度：小于23
        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(8000);//海拔高度
//        fanInParam.setAirVolume(18000);//风量(m3/h)
        fanInParam.setTotalStatic(300);//风压（Pa）
        fanInParam.setSerial("39CQ0608");//机组型号 39G0608
        fanInParam.setFanSupplier("A");//供应商
        fanInParam.setOutlet(FAN_OPTION_OUTLET_WWK);//风机形式 0：前弯或后弯 1：前弯 2：后弯 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam package fanInParam success");

        //================================测试参数页面参数封装begin=================================================
//        FanYldDllInParam wmkWindMachineOutParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);
        //================================测试参数页面参数封装end=================================================

        //根据海拔高度计算空气密度 需要调用DLl完成后放入pAirDensity字段
//        wmkWindMachineOutParam.setPAirDensity("空气密度需要计算值");

        Double[] pDoubleData = new Double[50];//DLL返回值
        pDoubleData[0] = 1440.5;//风机转速
        pDoubleData[1] = 300.0;//全压内效率
        pDoubleData[3] = 17.5;//内功率
        pDoubleData[6] = 200.0;//静压
        pDoubleData[8] = 300.0;//全压
        pDoubleData[19] = 1.5;//实际噪声
        pDoubleData[38] = 18.9;//总A声压级
        pDoubleData[45] = 2000.8;//风机最高转速R/MIN

        String[][] pStrData = new String[5][255];//DLL返回值
        pStrData[1][0] = "R";
        pStrData[0][0] = "SYW";
        pStrData[3][0] = "B";
        pStrData[2][0] = "250";
        Integer tCount = 1;
        FanYldDllInfo windMachineDLLResult = new FanYldDllInfo();
//        windMachineDLLResult.setpStrData(pStrData);
//        windMachineDLLResult.setpDoubleData(pDoubleData);
        windMachineDLLResult.setTCount(tCount);
        windMachineDLLResult.setFanType("39CQ0608");
        FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();

        //================================测试根据DLL返回值封装页面返回值begin=================================================
//        List<FanInfo> fanInfoList = fanYldResultFactory.packageCountResultEx3(null);
        //================================测试根据DLL返回值封装页面返回值end=================================================
    }

}