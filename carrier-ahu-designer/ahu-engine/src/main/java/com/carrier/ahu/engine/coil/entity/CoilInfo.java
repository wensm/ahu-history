package com.carrier.ahu.engine.coil.entity;

import java.io.Serializable;

import lombok.Data;

import static com.carrier.ahu.vo.SystemCalculateConstants.SWEATCHECK_1;

//盘管计算总结果
@Data
public class CoilInfo implements Serializable {
    private int rows;//排数
    private String finDensity;//片距
    private String circuit;//回路
    private String area;//迎风面积
    private String velocity;//风面速
    private String InDryBulbT;//进风干球温度
    private String InWetBulbT;//进风湿球温度
    private String InRelativeT;//进风相对湿度
    private String OutDryBulbT;//出风干球温度
    private String OutWetBulbT;//出风湿球温度
    private String OutRelativeT;//出风相对湿度
    private String AirResistance;//空气阻力
    private String waterResistance;//水阻力
    private String returnWaterFlow;//水流量
    private String returnEnteringFluidTemperature;//进水温度
    private String returnWTAScend;//水温升
    private String fluidvelocity;//介质流速
    private String returnColdQ;//冷量
    private String returnHeatQ;//热量 冷水盘管冬季、热水盘管使用
    private String returnSensibleCapacity;//显冷
    private String returnBypass;//Bypass
    /*直接蒸发式盘管增加属性*/
    private String returnCond;//冷凝器进风温度
    private String returnSST;//饱和吸气温度
    private String returnSatCond;//饱和冷凝温度
    private String Unit;//unit
    private String ahriMessage;//认证提示消息
    private Boolean ahri = true;//认证标识

    private String price;//当前实体条件和风机段metajson merge 作为新段，计算价格，放到前端列表
    
    private String waterNoRAHIResistance;//水阻力
    private String SweatCheck = SWEATCHECK_1;//凝露校核

}
