package com.carrier.ahu.engine.fan.motor;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotorEff;
import com.carrier.ahu.metadata.entity.fan.SKFanVMotorEff;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

public class MortorDataParser implements IMotorDataParser {

    @Override
    public double getMotorEff(String supplier,String motorType, String power, String level) {

        /*A:三级能效电机、B:防爆电机、C:双速电机、D:变频电机、E:二级能效*/
        /*A:ABB、B:默认、C:东元、S:西门子*/
        /*A W T T*/
        //如果是双速电机
        if (SystemCalculateConstants.FAN_TYPE_4.equals(motorType)) {
            return 0;
        }
        //如果是变频电机
        if (SystemCalculateConstants.FAN_TYPE_5.equals(motorType) || SystemCalculateConstants.FAN_TYPE_6.equals(motorType)) {
            //TODO修改为S_K_FAN_VMOTOR_EFF
            List<SKFanVMotorEff> skFanVMotorEffList = AhuMetadata.findList(SKFanVMotorEff.class);
            Collections.sort(skFanVMotorEffList, Comparator.comparing(SKFanVMotorEff::getPower));
            for (SKFanVMotorEff sKFanVMotorEff : skFanVMotorEffList) {
                if (sKFanVMotorEff.getPower() >= BaseDataUtil.stringConversionDouble(power) && sKFanVMotorEff.getLevel() == BaseDataUtil.stringConversionInteger(level)) {
                    if(SystemCalculateConstants.FAN_SUPPLIER_ABB.equals(supplier)){//ABB A
                        return sKFanVMotorEff.getAeff();
                    }else if(SystemCalculateConstants.FAN_SUPPLIER_DEFAULT.equals(supplier)){//default W
                        return sKFanVMotorEff.getWeff();
                    }else if(SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(supplier)||SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(supplier)){//Dongyuan T Simens
                        return sKFanVMotorEff.getTeff();
                    }
                }
            }
        }
        List<SKFanMotorEff> skFanMotorEffList = AhuMetadata.findList(SKFanMotorEff.class);
        Collections.sort(skFanMotorEffList, Comparator.comparing(SKFanMotorEff::getPower));
        for (SKFanMotorEff skFanMotorEffPo : skFanMotorEffList) {
            if (skFanMotorEffPo.getPower() >= BaseDataUtil.stringConversionDouble(power) && skFanMotorEffPo.getLevel() == BaseDataUtil.stringConversionInteger(level)) {
                if(SystemCalculateConstants.FAN_TYPE_2.equals(motorType)||SystemCalculateConstants.FAN_TYPE_3.equals(motorType)){//三级能效电机 防爆电机
                    return skFanMotorEffPo.getV0E3A();
                }else if(SystemCalculateConstants.FAN_TYPE_1.equals(motorType)){//二级能效
                    return skFanMotorEffPo.getV0E2T();
                }else if(SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(supplier)||SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(supplier)){
                    return skFanMotorEffPo.getCEFF2();
                }
            }
        }
        return 0;
    }

}
