package com.carrier.ahu.engine.fan.entity;

import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

import lombok.Data;

//返回到页面结果
@Data
public class FanInfo {

    private String fanModel;//风机型号
    private String outletVelocity;//出风口风速 m/s
    private String efficiency;//效率 %
    private String motorEff = "";//电机效率 %
    private String frequencyRange;//变频范围
    private String motorEffPole;//电机效率/极数
    private String absorbedPower;//吸收功率 KW
    private String maxAbsorbedPower;//最大吸收功率 KW
    private String totalPressure;//全压
    private String RPM;//转速 RPM
    private String maxRPM;//最大转速
    private String noise;//噪音 dB(A)
    private String motorBaseNo;//机座号
    private String motorPower;//电机功率 KW
    private String pole;//极数
    private String curve;//图片路径
    private String engineData1;
    private String engineData2;
    private String engineData3;
    private String engineData4;
    private String engineData5;
    private String engineData6;
    private String engineData7;
    private String engineData8;
    private String engineData9;
    private String engineData10;
    private String engineData11;
    private String engineData12;
    private String engineData13;
    private String engineData14;
    private String engineData15;
    private String engineData16;
    private String outlet;
    private String model = "";
    private int series;
    private String k = "";
    private String hz = EngineConstant.SYS_BLANK;
    private String engineType;
    private String motorPosition;
    private String aPower;//A声功率级
    private String aSoundPressure;//A声声压级

    private String price;//当前实体条件和风机段metajson merge 作为新段，计算价格，放到前端列表

    public String getModel() {
        return fanModel.substring(0, 2).replaceAll(EngineConstant.JSON_FAN_FANMODEL_BC, EngineConstant.SYS_MAP_FAN_FANMODEL_ZC);
    }

    public int getSeries() {
        if (fanModel.contains(EngineConstant.SYS_MAP_FAN_FANMODEL_SYW)) {
            return BaseDataUtil.stringConversionInteger(fanModel.replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_SYW, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_RH, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_ALPHABET_R_UP, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_K, EngineConstant.SYS_BLANK));
        } else if (fanModel.contains(EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ)) {
            return BaseDataUtil.stringConversionInteger(fanModel.replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_RH, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_ALPHABET_R_UP, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_K, EngineConstant.SYS_BLANK));
        }else if(fanModel.contains("CM")||fanModel.contains("TM")||fanModel.contains("XM")||fanModel.contains("TL")){
            return BaseDataUtil.stringConversionInteger(fanModel.replaceAll("FDA", EngineConstant.SYS_BLANK).replaceAll("BDB", EngineConstant.SYS_BLANK).replaceAll("ADA", EngineConstant.SYS_BLANK).replaceAll("CM", EngineConstant.SYS_BLANK).replaceAll("XM", EngineConstant.SYS_BLANK).replaceAll("TM", EngineConstant.SYS_BLANK).replaceAll("TL", EngineConstant.SYS_BLANK));
        }
        return BaseDataUtil.stringConversionInteger(fanModel.replaceAll(EngineConstant.JSON_FAN_FANMODEL_BC, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.JSON_FAN_FANMODEL_FC, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_MAP_FAN_FANMODEL_K, EngineConstant.SYS_BLANK).replaceAll(EngineConstant.SYS_ALPHABET_R_UP, EngineConstant.SYS_BLANK));
    }

    public String getK() {
        if (fanModel.contains(EngineConstant.SYS_MAP_FAN_FANMODEL_K)) {
            return EngineConstant.SYS_MAP_FAN_FANMODEL_K;
        } else {
            return EngineConstant.SYS_BLANK;
        }
    }

    public String getOutlet() {
        if (fanModel.contains(EngineConstant.JSON_FAN_FANMODEL_FC)||fanModel.contains(EngineConstant.JSON_FAN_FANMODEL_FDA)) {
            return SystemCalculateConstants.FAN_OPTION_OUTLET_F;
        } else if (fanModel.contains(EngineConstant.JSON_FAN_FANMODEL_BC)||fanModel.contains(EngineConstant.JSON_FAN_FANMODEL_BDB)) {
            return SystemCalculateConstants.FAN_OPTION_OUTLET_B;
        } else if (fanModel.contains(EngineConstant.JSON_FAN_FANMODEL_ADA)) {
            return SystemCalculateConstants.FAN_OPTION_OUTLET_FOB;
        }else {
            return SystemCalculateConstants.FAN_OPTION_OUTLET_WWK;
        }
    }
}
