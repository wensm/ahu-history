package com.carrier.ahu.engine.coilDx4d;

import java.util.List;

import org.springframework.stereotype.Service;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;

/**
 * Created by liangd4 on 2017/9/8.
 */
@Service
public interface DXCoilService {
    List<CoilInfo> getDXCoil(CoilInParam coldWaterInParam);
}
