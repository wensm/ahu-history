package com.carrier.ahu.engine.fanYld4k;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.AFanEngine;
import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllEx3Factory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInExSYQKParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInExSYQRParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.util.EmptyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liangd4 on 2017/8/29. 风机引擎计算
 */
public class FanYldSYQServiceImpl extends AFanEngine {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanYldSYQServiceImpl.class.getName());
    private static final FanYldDllInExSYQKParamFactory fanYldDllInExSYQKParamFactory = new FanYldDllInExSYQKParamFactory();
    private static final FanYldDllInExSYQRParamFactory fanYldDllInExSYQRParamFactory = new FanYldDllInExSYQRParamFactory();
    private static final FanYldDllEx3Factory fanSelectionEx3 = new FanYldDllEx3Factory();


    private static final FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();

    // 风机引擎计算
    @Override
    public List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException {
        List<FanInfo> fanInfoList = new ArrayList<FanInfo>();
        FanYldDllInParam kFanYldDllInParam = fanYldDllInExSYQKParamFactory.packageParam(fanInParam);/* 封装YLDDll参数 */
        List<FanYldDllInfo> kFanYldDllInfoList = fanSelectionEx3.fanSelectionEx2(kFanYldDllInParam);/* 根据加工参数调用Dll */
        List<FanInfo> kFanInfoList = fanYldResultFactory.packageCountResultExSYQ(fanInParam,this.getFanConditions(),kFanYldDllInfoList,null); /* 根据Dll返回结果封装数据 */
        fanInfoList.addAll(kFanInfoList);
        FanYldDllInParam rFanYldDllInParam = fanYldDllInExSYQRParamFactory.packageParam(fanInParam);/* 封装YLDDll参数 */
        List<FanYldDllInfo> rFanYldDllInfoList = fanSelectionEx3.fanSelectionEx2(rFanYldDllInParam);/* 根据加工参数调用Dll */
        List<FanInfo> rFanInfoList = fanYldResultFactory.packageCountResultExSYQ(fanInParam,this.getFanConditions(),rFanYldDllInfoList,null); /* 根据Dll返回结果封装数据 */
        fanInfoList.addAll(rFanInfoList);
        return fanInfoList;
    }

    @Override
    public FanInfo calByRule(FanInParam fanInParam, PublicRuleParam publicRuleParam) throws FanEngineException {
        List<FanInfo> infoList = this.cal(fanInParam);
        if (EmptyUtil.isEmpty(infoList)) {
            return null;
        }
        return infoList.get(0);
    }
}
