package com.carrier.ahu.engine.coilWater4d.param;

/**
 * Created by liangd4 on 2017/7/31.
 */
public class CWDRuleParam {

    private String key;//最经济：economical、最性能：performance
    private String value;//1：生效 0：未生效

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
