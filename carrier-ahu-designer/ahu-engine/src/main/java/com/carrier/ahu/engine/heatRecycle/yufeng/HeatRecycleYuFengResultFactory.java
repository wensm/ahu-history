package com.carrier.ahu.engine.heatRecycle.yufeng;

import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.unit.BaseDataUtil;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleYuFengResultFactory {

    //引擎结果转换对象
    public HeatRecycleInfo packageHeatRecycleInfo(int rotorSize, String season, double supplyAirDB, double supplyAirWB, double supplyAirRH, double supplyAirPD, double supplyAirTotalEff,
                                                  double exhaustAirDB, double exhaustAirWB, double exhaustAirRH, double exhaustAirPD, double exhaustAirTotalEff, double totalCap, double sensible) {
        HeatRecycleInfo heatRecycleInfo = new HeatRecycleInfo();
        heatRecycleInfo.setReturnWheelSize(BaseDataUtil.integerConversionString(rotorSize));//转轮尺寸
        heatRecycleInfo.setReturnSeason(season);//季节
        heatRecycleInfo.setReturnSDryBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirDB,1)));//干球温度送风侧
        heatRecycleInfo.setReturnSWetBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirWB,1)));//湿球温度送风侧
        heatRecycleInfo.setReturnSRelativeT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirRH,1)));//相对湿度送风侧
        heatRecycleInfo.setReturnSAirResistance(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirPD,4)));//空气阻力送风侧
        heatRecycleInfo.setReturnSEfficiency(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirTotalEff,1)));//效率送风侧
        heatRecycleInfo.setReturnEDryBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirDB,1)));//干球温度进风侧
        heatRecycleInfo.setReturnEWetBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirWB,1)));//湿球温度进风侧
        heatRecycleInfo.setReturnERelativeT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirRH,1)));//相对湿度进风侧
        heatRecycleInfo.setReturnEAirResistance(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirPD,4)));//空气阻力进风侧
        heatRecycleInfo.setReturnEEfficiency(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirTotalEff,1)));//效率进风侧
        heatRecycleInfo.setReturnTotalHeat(BaseDataUtil.doubleConversionString(BaseDataUtil.doubleConvertABS(BaseDataUtil.decimalConvert(totalCap,2))));//全热
        heatRecycleInfo.setReturnSensibleHeat(BaseDataUtil.doubleConversionString(BaseDataUtil.doubleConvertABS(BaseDataUtil.decimalConvert(sensible,2))));//显热
        return heatRecycleInfo;
    }

}
