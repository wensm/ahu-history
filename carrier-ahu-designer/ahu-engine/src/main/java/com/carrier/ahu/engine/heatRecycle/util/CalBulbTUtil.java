package com.carrier.ahu.engine.heatRecycle.util;

import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;

public class CalBulbTUtil {

    //此计算为排风侧计算公式 如果计算 送风侧计算公式出入参数需要相反

    /**
     * @param NewInDryBulbT  新风干球温度
     * @param IndoorDryBulbT 回风干球温度
     * @param Efficiency     效率
     * @return 干球温度
     */
    public static double calPParamT(double NewInDryBulbT, double IndoorDryBulbT, double Efficiency) {
        return BaseDataUtil.decimalConvert(Math.abs((IndoorDryBulbT - Efficiency * (IndoorDryBulbT - NewInDryBulbT) / 100)), 1);
    }

    /**
     * @param NewInDryBulbT  新风干球温度
     * @param NewInWetBulbT  新风湿球温度
     * @param IndoorDryBulbT 回风干球温度
     * @param IndoorWetBulbT 回风湿球温度
     * @param Efficiency     效率
     * @param season     冬季：true
     * @return 含湿量
     */
    public static double calPParamD(double NewInDryBulbT, double NewInWetBulbT, double IndoorDryBulbT, double IndoorWetBulbT, double Efficiency,boolean season) {
        AirConditionBean NEWIN = AirConditionUtils.FAirParmCalculate1(NewInDryBulbT, NewInWetBulbT);
        AirConditionBean INDOOR = AirConditionUtils.FAirParmCalculate1(IndoorDryBulbT, IndoorWetBulbT);
        if(season){
            return INDOOR.getParamD() - Efficiency * (INDOOR.getParamD() - NEWIN.getParamD()) / 100;
        }
        return NEWIN.getParamD() - Efficiency * (NEWIN.getParamD() - INDOOR.getParamD()) / 100;
    }

    /**
     * @param paramT 干球温度
     * @param paramD 湿球温度
     * @return 相对湿度
     */
    public static double calPParamF(double paramT, double paramD) throws Exception {
        AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate2(paramT, paramD);
        return airConditionBean.getParmF();
    }
}
