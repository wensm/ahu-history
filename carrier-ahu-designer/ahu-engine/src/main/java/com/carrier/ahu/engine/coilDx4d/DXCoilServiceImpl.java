package com.carrier.ahu.engine.coilDx4d;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilDx4d.factory.DXCoilResultFactory;
import com.carrier.ahu.engine.coilWater4d.WaterCoilServiceImpl;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.factory.CoilWaterDllFactory;
import com.carrier.ahu.engine.coilWater4d.factory.CoilWaterDllInParamFactory;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.coilWater4d.util.CoilPackageUtil;
import com.carrier.ahu.util.DateUtil;

/**
 * Created by liangd4 on 2017/9/8.
 */
@Component
public class DXCoilServiceImpl implements DXCoilService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaterCoilServiceImpl.class.getName());
    private final static CoilWaterDllInParamFactory paramsFac = new CoilWaterDllInParamFactory();
    private final static CoilWaterDllFactory coilDllFac = new CoilWaterDllFactory();
    private final static DXCoilResultFactory dxCoilResultFactory = new DXCoilResultFactory();

    @Override
    public List<CoilInfo> getDXCoil(CoilInParam coilInParam) {
        LOGGER.info("DX coil calculator begin " + DateUtil.getStringDate());
        //入参数据加工&&包含多个CPC文件参数 包含DX参数封装
        List<CoilDllInParam> coldWaterOutParamList = paramsFac.packageParamHTC(coilInParam);
        LOGGER.info("DX coil calculator dll in param " + DateUtil.getStringDate());
        //根据out参数调用DLL
        List<CoilWaterDllInfo> coilWaterDllInfoList = coilDllFac.getDLLResultList(coldWaterOutParamList);
        LOGGER.info("DX coil calculator dll result " + DateUtil.getStringDate());
        //出参数据加工
        List<CoilInfo> coilInfoList = dxCoilResultFactory.packageCountResult(coilWaterDllInfoList);
        coilInfoList = CoilPackageUtil.packageUserConfig(coilInfoList, coilInParam);
        LOGGER.info("DX coil calculator end ");
        return coilInfoList;
    }
}
