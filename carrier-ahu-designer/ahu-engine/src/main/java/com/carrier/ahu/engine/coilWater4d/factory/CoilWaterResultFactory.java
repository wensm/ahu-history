package com.carrier.ahu.engine.coilWater4d.factory;

import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.util.FormulaUtil;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.unit.MapSortUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

import static com.carrier.ahu.vo.SystemCalculateConstants.seasonW;
import static com.carrier.ahu.vo.SystemCalculateConstants.waterCoilH;

@Component
public class CoilWaterResultFactory {

    // 根据DLL返回结果封装页面返回参数
    public List<CoilInfo> packageCountResult(List<CoilWaterDllInfo> coldWaterDLLResultList) {
        // 如果Dll返回为null 不参数数据转换
        if (EmptyUtil.isEmpty(coldWaterDLLResultList)) {
            return null;
        }
        List<CoilInfo> coilInfoList = new ArrayList<>();
        // 根据排数、片距、回路 计算：冷量、显冷、水流量
        Map<String, String> map = new HashMap<String, String>();
        for (CoilWaterDllInfo coldWaterDLLResult : coldWaterDLLResultList) {
            String rows = BaseDataUtil.integerConversionString(coldWaterDLLResult.getRows());// 排数
            String fpi = BaseDataUtil.integerConversionString(coldWaterDLLResult.getFpi());// 片距
            String cir = coldWaterDLLResult.getCir();// 回路
            double total = coldWaterDLLResult.getTotal();// 冷量
            double sensible = coldWaterDLLResult.getSensible();// 显冷
            double wpd = coldWaterDLLResult.getWpd();// 水阻
            int wpdI = 1;//cpc个数
            double waterflow = coldWaterDLLResult.getFlow();//*coldWaterDLLResult.getWaterFlowRate();
            String key = rows + UtilityConstant.SYS_PUNCTUATION_HYPHEN + fpi + UtilityConstant.SYS_PUNCTUATION_HYPHEN + cir;
            if (map.containsKey(key)) {
                String mapValue = map.get(key);
                double mapTotal = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[0]);
                double mapSensible = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[1]);
                double mapWpd = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[2]);
                int mapWpdI = BaseDataUtil.stringConversionInteger(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[3]);
                double mapWaterFlow = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[4]);
                total += mapTotal;
                sensible += mapSensible;
                wpd += mapWpd;
                wpdI += mapWpdI;
                waterflow += mapWaterFlow;
            }
            map.put(key, total + UtilityConstant.SYS_PUNCTUATION_HYPHEN + sensible + UtilityConstant.SYS_PUNCTUATION_HYPHEN + wpd + UtilityConstant.SYS_PUNCTUATION_HYPHEN + wpdI + UtilityConstant.SYS_PUNCTUATION_HYPHEN + waterflow);
        }
        Map<String, String> mapSort = MapSortUtil.sortMapByKey(map);// map排序
        // 根据map遍历排数、片距、回路 纬度的数据
        Iterator<Map.Entry<String, String>> iterator = mapSort.entrySet().iterator();
        while (iterator.hasNext()) {
            CoilInfo coilInfo = new CoilInfo();
            Map.Entry<String, String> entry = iterator.next();
            String mapKey = String.valueOf(entry.getKey());
            String mapValue = String.valueOf(entry.getValue());
            String rows = mapKey.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[0];// 排数
            String fpi = mapKey.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[1];// 片距
            String cir = mapKey.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[2];// 回路
            double mapTotal = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[0]);
            double mapSensible = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[1]);
            double mapWpd = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[2]);
            int mapWpdI = BaseDataUtil.stringConversionInteger(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[3]);
            double mapWaterFlow = BaseDataUtil.stringConversionDouble(mapValue.split(UtilityConstant.SYS_PUNCTUATION_HYPHEN)[4]);

            for (CoilWaterDllInfo coilWaterDllInfo : coldWaterDLLResultList) {
                // 满足三通阀条件 水温升+0.1 TODO
                // 排数\片距\回路 相同封装返回参数
                if (rows.equals(BaseDataUtil.integerConversionString(coilWaterDllInfo.getRows())) && fpi.equals(BaseDataUtil.integerConversionString(coilWaterDllInfo.getFpi())) && cir.equals(coilWaterDllInfo.getCir())) {
                    coilInfo.setRows(BaseDataUtil.stringConversionInteger(rows));// 排数
                    coilInfo.setFinDensity(fpi);// 片距
                    coilInfo.setCircuit(cir);// 回路
                    coilInfo.setArea(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getFaceArea()));// 迎风面积
                    coilInfo.setInRelativeT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaRH()));// 进风相对湿度
                    coilInfo.setOutRelativeT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaRH()));// 出风相对湿度
                    coilInfo.setReturnBypass(String.format(EngineConstant.SYS_FORMAT_4F, coilWaterDllInfo.getBypass()));// Bypass
                    if ("s_imperial".equals(EngineConstant.SYS_STRING_NUMBER_0)) {// 需要确认
                        coilInfo.setInDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEaDB(), true)));// 进风干球温度
                        coilInfo.setInWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEaWB(), true)));// 进风湿球温度
                        coilInfo.setOutDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getLaDB(), true)));// 出风干球温度
                        coilInfo.setOutWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getLaWB(), true)));// 出风湿球温度
                        coilInfo.setAirResistance(String.format(EngineConstant.SYS_FORMAT_3F, coilWaterDllInfo.getApd() / 249));// 空气阻力
                        coilInfo.setWaterResistance(String.format(EngineConstant.SYS_FORMAT_3F, coilWaterDllInfo.getWpd() / 2.99));// 水阻力
                        coilInfo.setReturnWaterFlow(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getFlow() * 4.40));// 水流量
                        coilInfo.setReturnEnteringFluidTemperature(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEwt(), true)));// 进水温度
                        coilInfo.setReturnWTAScend(String.format(EngineConstant.SYS_FORMAT_1F, FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getTr(), true) - FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEwt() - coilWaterDllInfo.getTr(), true)));// 水温升
                        coilInfo.setReturnColdQ(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getTotal() * 3414));// 冷量
                        coilInfo.setReturnSensibleCapacity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getSensible() * 3414));// 显冷
                        coilInfo.setVelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getVelocity() / 0.00508));// 风面速
                        if ((EngineConstant.JSON_AHU_SERIAL_0608).equals(coilWaterDllInfo.getUniType())) {// 机组型号
                            coilInfo.setFluidvelocity(String.format(EngineConstant.SYS_FORMAT_2F, 0.03 * 3.28));// 介质流速
                        } else {
                            coilInfo.setFluidvelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getFluidVelocity() * 3.28));// 介质流速
                        }
                    } else {
                        coilInfo.setInDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaDB()));// 进风干球温度
                        coilInfo.setInWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaWB()));// 进风湿球温度
                        coilInfo.setOutDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaDB()));// 出风干球温度
                        coilInfo.setOutWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaWB()));// 出风湿球温度
                        coilInfo.setAirResistance(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getApd()));// 空气阻力
                        coilInfo.setReturnEnteringFluidTemperature(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEwt()));// 进水温度
                        coilInfo.setReturnWTAScend(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getTr()));// 水温升
                        coilInfo.setReturnColdQ(String.format(EngineConstant.SYS_FORMAT_2F, mapTotal));// 冷量
                        coilInfo.setReturnSensibleCapacity(String.format(EngineConstant.SYS_FORMAT_2F, mapSensible));// 显冷
                        /*重新计算水阻力 当引擎计算的水阻小于最大水阻 根据水流量或者水温升重新封装*/
//                        double waterFlow = BaseDataUtil.decimalConvert(mapTotal / 4.18 / coilWaterDllInfo.getTr() * 3.6, 2);
//                        if (COOLINGCOIL_SCALCULATIONCONDITIONS_1.equals(coilWaterDllInfo.getCalculationConditions())) {
//                            coilInfo.setReturnWaterFlow(String.format(EngineConstant.SYS_FORMAT_1F, waterFlow));// 水温升计算水流量
//                        } else {
                        coilInfo.setReturnWaterFlow(String.format(EngineConstant.SYS_FORMAT_2F, mapWaterFlow));//水流量计算水流量
//                        }
                        coilInfo.setVelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getVelocity()));// 面风速
                        coilInfo.setReturnCond(null == coilWaterDllInfo.getCond() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getCond());
                        coilInfo.setReturnSatCond(null == coilWaterDllInfo.getSatCond() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getSatCond());
                        coilInfo.setUnit(null == coilWaterDllInfo.getCDU() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getCDU());
                        coilInfo.setReturnSST("8.5");
                        if ((EngineConstant.JSON_AHU_SERIAL_0608).equals(coilWaterDllInfo.getUniType())) {// 机组型号
                            coilInfo.setFluidvelocity("0.3");// 介质流速
                        } else {
                            coilInfo.setFluidvelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getFluidVelocity()));// 介质流速
                        }
                    }

                    coilInfo = packageAHRI(coilInfo, coilWaterDllInfo);

                    if (null != coilWaterDllInfo.getWpd()) {
                        double WTD = mapWpd;
                        if (coilWaterDllInfo.getFluid_ID() == 3) {
                            WTD = WTD * (0.98421618 - 2.6819146 * coilWaterDllInfo.getCONC1() + 8.9069170 * coilWaterDllInfo.getCONC1() * coilWaterDllInfo.getCONC1());
                        } else if (coilWaterDllInfo.getFluid_ID() == 4) {
                            WTD = WTD * (0.81352163 - 1.2655655 * coilWaterDllInfo.getCONC1() + 12.034736 * coilWaterDllInfo.getCONC1() * coilWaterDllInfo.getCONC1());
                        }

                        //如果不是AHRI认证的盘管，需要重新计算水阻
                        boolean isAHRI = coilInfo.getAhri();
                        double noRAHIWaterDrop = coilWaterDllInfo.getReturnNoAHRIWaterDrop();
                        if (!isAHRI && noRAHIWaterDrop != 0) {
                            WTD = Double.valueOf(noRAHIWaterDrop);
                        }
                        coilInfo.setWaterResistance(String.format(EngineConstant.SYS_FORMAT_1F, WTD / mapWpdI));//水阻力
                    } else {
                        coilInfo.setWaterResistance(EngineConstant.SYS_STRING_NUMBER_0);//水阻力
                    }
                    if (seasonW.equals(coilWaterDllInfo.getSeason()) || waterCoilH.equals(coilWaterDllInfo.getCoilType())) {
                        // 当为冬季时计算的冷量赋给热量值
                        coilInfo.setReturnHeatQ(coilInfo.getReturnColdQ());
                    }

                    coilInfoList.add(coilInfo);
                    break;
                }
            }
        }
        ListUtils.sort(coilInfoList, true, EngineConstant.SYS_MAP_ROWS, EngineConstant.SYS_MAP_FINDENSITY, "circuit");
        return coilInfoList;
    }

    private CoilInfo packageAHRI(CoilInfo coilInfo, CoilWaterDllInfo coilWaterDllInfo) {
        if (String.valueOf(coilWaterDllInfo.getFluid_ID()).equals(SystemCalculateConstants.COOLINGCOIL_SCOOLANT_1)) {//介质水
            double velocity = BaseDataUtil.stringConversionDouble(coilInfo.getVelocity());//风面速
            double inDryBulbT = BaseDataUtil.stringConversionDouble(coilInfo.getInDryBulbT());//进风干球温度
            double inWetBulbT = BaseDataUtil.stringConversionDouble(coilInfo.getInWetBulbT());//进风湿球温度
            double fluidvelocity = BaseDataUtil.stringConversionDouble(coilInfo.getFluidvelocity());//介质流速
            double returnEnteringFluidTemperature = BaseDataUtil.stringConversionDouble(coilInfo.getReturnEnteringFluidTemperature());//进水温度
            if(coilWaterDllInfo.getSeason().equals(seasonW)){
                if (1 > velocity || velocity > 8 || -18 > inDryBulbT || inDryBulbT > 38 || 0.1 > fluidvelocity || fluidvelocity > 2.4 ||
                        49 > returnEnteringFluidTemperature || returnEnteringFluidTemperature > 121) {
                    coilInfo.setAhriMessage(ErrorCode.COIL_NOT_CERTIFIED_AHRI_410.getMessage());
                    coilInfo.setAhri(false);
                    return coilInfo;
                }
            }else{
                if (1 > velocity || velocity > 4 || 18 > inDryBulbT || inDryBulbT > 38 || 16 > inWetBulbT || inWetBulbT > 29
                        || 0.3 > fluidvelocity || fluidvelocity > 2.4 || 1.7 > returnEnteringFluidTemperature || returnEnteringFluidTemperature > 18) {
                    coilInfo.setAhriMessage(ErrorCode.COIL_NOT_CERTIFIED_AHRI_410.getMessage());
                    coilInfo.setAhri(false);
                    return coilInfo;
                }
            }
        } else {
            coilInfo.setAhriMessage(ErrorCode.COIL_NOT_CERTIFIED_AHRI.getMessage());
            coilInfo.setAhri(false);
            return coilInfo;
        }
        return coilInfo;
    }

    public static void main(String[] args) {
        System.out.println(String.format(String.format(EngineConstant.SYS_FORMAT_2F, 0.03 * 3.28)));
        BigDecimal bg = new BigDecimal(0.03 * 3.28);
        System.out.println(bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

    }
}
