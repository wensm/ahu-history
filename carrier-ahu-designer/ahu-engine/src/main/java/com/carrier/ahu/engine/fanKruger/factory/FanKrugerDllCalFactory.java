package com.carrier.ahu.engine.fanKruger.factory;

import com.carrier.ahu.engine.fan.*;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineService;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineServiceImpl;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FanKrugerDllCalFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanKrugerDllCalFactory.class.getName());

    private static final KrugerEngineService krugerEngineService = new KrugerEngineServiceImpl();

    public List<KrugerSelection> calculation(KrugerSelectInfo krugerSelectInfo, FanInParam fanInParam) {
        List<KrugerSelection> krugerSelectionList = new ArrayList<KrugerSelection>();
        /*根据productType重新封装 FDA BDB ADA*/
        krugerSelectInfo.setProductType(3);
        List<KrugerSelection> krugerSelectionList3 = krugerEngineService.select(krugerSelectInfo);
        krugerSelectionList.addAll(krugerSelectionList3);
        krugerSelectInfo.setProductType(9);
        List<KrugerSelection> krugerSelectionList9 = krugerEngineService.select(krugerSelectInfo);
        krugerSelectionList.addAll(krugerSelectionList9);
        krugerSelectInfo.setProductType(5);
        List<KrugerSelection> krugerSelectionList5 = krugerEngineService.select(krugerSelectInfo);
        krugerSelectionList.addAll(krugerSelectionList5);
        return krugerSelectionList;
    }


}
