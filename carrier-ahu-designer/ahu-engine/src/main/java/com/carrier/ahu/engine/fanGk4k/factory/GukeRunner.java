package com.carrier.ahu.engine.fanGk4k.factory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanGk4k.param.FanGKDllInParam;

public class GukeRunner extends Thread {

	private static final Logger logger = LoggerFactory.getLogger(GukeRunner.class.getName());
	//接受风机的输入
	private final static BlockingQueue<GukeSingleMap<List<FanGKDllInParam>>> bq = new ArrayBlockingQueue<GukeSingleMap<List<FanGKDllInParam>>>(1);
	private final static BlockingQueue<Map<String,List<FanInfo>>> temQueue=new ArrayBlockingQueue<Map<String,List<FanInfo>>>(1);
	
	//接受风机的输出
//	private final static Map<String,BlockingQueue<List<FanInfo>>> retVal = new HashMap<String,BlockingQueue<List<FanInfo>>>();
	
	private boolean flg = true;
	private boolean runningFlg = false;
	private static final FanGkResultFactory fanGkResultFactory = new FanGkResultFactory();
	private static final FanGkDllFactory fanGkDllFactory = new FanGkDllFactory();

	private GukeRunner() {
	}

	private final static GukeRunner runner = new GukeRunner();

	public static GukeRunner getInstance() {
		return runner;
	}

	public void begin() {
		if (!GukeRunner.getInstance().runningFlg) {
			GukeRunner.getInstance().runningFlg = true;
			GukeRunner.getInstance().setDaemon(true);
			GukeRunner.getInstance().start();
		}

	}

	public void run() {
		Map<String,List<FanInfo>> map=null;
		while (GukeRunner.getInstance().flg) {
			try {
				map=new HashMap<String,List<FanInfo>>();
				GukeRunner.getInstance().runningFlg = true;
				GukeSingleMap<List<FanGKDllInParam>> input = bq.take();
				String key=input.getKey();
				List<FanInfo> result=fanGkResultFactory.packageResult(fanGkDllFactory.packageGKEngine(input.get(key)));
				map.put(key, result);
				GukeRunner.getInstance().temQueue.put(map);
			} catch (InterruptedException e) {
				logger.error("Error in GUKE RUNNER",e);
			}
		}
	}

	public boolean end() {
		GukeRunner.getInstance().flg = true;
		GukeRunner.getInstance().runningFlg = false;
		return true;
	}

	public void setInput(GukeSingleMap<List<FanGKDllInParam>> input) {
		try {
				GukeRunner.getInstance().bq.put(input);
		} catch (InterruptedException e) {
			logger.error("Error in GUKE RUNNER to set input",e);
		}
	}
	
	public List<FanInfo> getOutput(String key){
		try {
			Map tempValue = GukeRunner.getInstance().temQueue.poll(10, TimeUnit.SECONDS);
			if(tempValue!=null) {
				Object object = tempValue.get(key);
				List<FanInfo> fanInfoList  = (List<FanInfo>)object;
				return fanInfoList;
			}
			
		} catch (InterruptedException e) {
			logger.error("Error in GUKE RUNNER to get output",e);
		}
		return null;
	}
}
