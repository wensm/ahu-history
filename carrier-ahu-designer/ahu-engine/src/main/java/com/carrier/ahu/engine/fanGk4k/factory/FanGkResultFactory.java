package com.carrier.ahu.engine.fanGk4k.factory;

import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.motor.IMotorDataParser;
import com.carrier.ahu.engine.fan.motor.MortorDataParser;
import com.carrier.ahu.engine.fanGk4k.entity.FanGkDllInfo;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.unit.BaseDataUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LIANGD4 on 2017/12/17.
 */
public class FanGkResultFactory {

    public List<FanInfo> packageResult(List<FanGkDllInfo> fanGkDllInfoList) {
        Map<String, String> map = new HashMap<String, String>();
        List<FanInfo> fanInfoList = new ArrayList<FanInfo>();
        for (FanGkDllInfo fanGkDllInfo : fanGkDllInfoList) {
            double motor = fanGkDllInfo.getTpw();
            if (motor >= 0.55 && motor < 250) {
                FanInfo fanInfo = new FanInfo();
                String fanInModel = fanGkDllInfo.getFanModel();
                StringBuffer FanOutModel = new StringBuffer();
                FanOutModel.append(EngineConstant.JSON_FAN_FANMODEL_BC);
                FanOutModel.append(fanInModel.substring(6));
                if (fanInModel.startsWith(EngineConstant.JSON_FAN_FANMODEL_YFH_G)) {
                    FanOutModel.append(EngineConstant.JSON_FAN_FANMODEL_K);
                }
                fanInfo.setFanModel(FanOutModel.toString());
                fanInfo.setOutletVelocity(String.valueOf(BaseDataUtil.decimalConvert(fanGkDllInfo.getCo(), 1)));
                fanInfo.setEfficiency(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getEft() * 100, 1)));
                fanInfo.setAbsorbedPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getPwt(), 1)));
                fanInfo.setRPM(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getNst(), 1)));
                fanInfo.setNoise(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getRla(), 1)));
                String mapKey = EngineConstant.SYS_BLANK + fanGkDllInfo.getPower() + fanGkDllInfo.getPole();
                if (!map.containsKey(mapKey)) {
                    SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, String.valueOf(motor),
                            String.valueOf(BaseDataUtil.doubleConversionInteger(fanGkDllInfo.getPole())));
                    fanInfo.setMotorBaseNo(skFanMotor.getMachineSiteNo());
                } else {
                    fanInfo.setMotorBaseNo(map.get(mapKey));
                }
                fanInfo.setTotalPressure(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getPtt(), 1)));
                fanInfo.setMaxRPM(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getMaxspeed(), 1)));
                fanInfo.setMaxAbsorbedPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getMaxmotorpower(), 1)));
                fanInfo.setMotorPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(motor, 1)));
                fanInfo.setPole(String.format(EngineConstant.SYS_FORMAT_0F, fanGkDllInfo.getPole()));
                fanInfo.setCurve(fanGkDllInfoList.get(0).getCurve());
                fanInfo.setModel(fanInfo.getModel());
                fanInfo.setSeries(fanInfo.getSeries());
                fanInfo.setK(fanInfo.getK());
                IMotorDataParser parser = new MortorDataParser();
                fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(fanGkDllInfo.getSupplier(), fanGkDllInfo.getType(), fanInfo.getMotorPower(), fanInfo.getPole())) + "%");
                fanInfo.setEngineType(FanEnginesEnum.GK.getCode());
                packageFaninfoEngineData(fanInfo,fanGkDllInfo);
                fanInfoList.add(fanInfo);
            }
        }
        return fanInfoList;
    }

    private FanInfo packageFaninfoEngineData(FanInfo fanInfo, FanGkDllInfo fanGkDllInfo) {
        fanInfo.setEngineData1(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep1(), 0)));
        fanInfo.setEngineData2(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep2(), 0)));
        fanInfo.setEngineData3(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep3(), 0)));
        fanInfo.setEngineData4(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep4(), 0)));
        fanInfo.setEngineData5(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep5(), 0)));
        fanInfo.setEngineData6(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep6(), 0)));
        fanInfo.setEngineData7(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep7(), 0)));
        fanInfo.setEngineData8(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspep8(), 0)));

        fanInfo.setEngineData9(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe1(), 0)));
        fanInfo.setEngineData10(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe2(), 0)));
        fanInfo.setEngineData11(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe3(), 0)));
        fanInfo.setEngineData12(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe4(), 0)));
        fanInfo.setEngineData13(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe5(), 0)));
        fanInfo.setEngineData14(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe6(), 0)));
        fanInfo.setEngineData15(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe7(), 0)));
        fanInfo.setEngineData16(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getSouspe8(), 0)));

        fanInfo.setAPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getRla() + 7.0, 0)));
        fanInfo.setASoundPressure(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(fanGkDllInfo.getRla(), 1)));
        return fanInfo;
    }

}
