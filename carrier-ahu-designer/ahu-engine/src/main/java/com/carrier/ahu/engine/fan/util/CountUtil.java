package com.carrier.ahu.engine.fan.util;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanCountParam;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.metadata.entity.fan.SKFanType1;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/7/18.
 */
public class CountUtil {

    public FanInfo packageResult(FanCountParam countParam) {
        FanInfo engineCalculationResult = new FanInfo();
        String strTyper = EngineConstant.SYS_BLANK;
        @SuppressWarnings("unused")
        String machinesite = EngineConstant.SYS_BLANK;
        double motor = countParam.getMotor();
        //电机功率>= 0.55 && 电机功率<= 55
        if (motor >= 0.55 && motor <= 55) {
            SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, String.valueOf(countParam.getMotor()),String.valueOf(countParam.getJs()));
            if (EmptyUtil.isNotEmpty(skFanMotor)) {
                strTyper = skFanMotor.getMachineSiteNo();// 机器位置标号
                machinesite = skFanMotor.getMachineSite();// 机器位置
            }
        }
        //机器位置 > 查询出来的最大机器 并且 风机名称中不包含SYW
        /*if (Double.valueOf(machinesite) > Double.valueOf(countParam.getrMotorMaxMachineSite()) && countParam.getFanName().split("SYW").length == 0) {
            return engineCalculationResult;
        }*/
        //motor：电机功率 RMotorMaxpower：最大功率  并且 风机名称 = SYW
        /*if (Double.valueOf(countParam.getMotor()) > Double.valueOf(countParam.getrMotorMaxPower()) && countParam.getFanName().split("SYW").length == 0) {
            return engineCalculationResult;
        }*/

        //==========根据电机类型做判断=================================================================
        //电机类型为双速和变频
        if (countParam.getMotorType().equals(EngineConstant.SYS_ALPHABET_C_UP)) {
            STwoSpeedMotor sTwospeedmotorPo = AhuMetadata.findOne(STwoSpeedMotor.class,String.valueOf(countParam.getJs()), String.valueOf(countParam.getMotor()),countParam.getFanSupplier());
            if (EmptyUtil.isNotEmpty(sTwospeedmotorPo)) {// 根据电机级数、供应商、功率查询motorType、motorBase
                strTyper = sTwospeedmotorPo.getMotorBase();
            }
            engineCalculationResult.setFanModel(countParam.getFanName());//风机型号
            engineCalculationResult.setOutletVelocity(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getOutletVelocity())));//出风口风速
            engineCalculationResult.setEfficiency(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getEfficiency())));//效率
            engineCalculationResult.setAbsorbedPower(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getAbsorberPower())));//吸收功率
            engineCalculationResult.setRPM(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getRpm())));//转速
            engineCalculationResult.setNoise(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getNoise())));//噪音
            engineCalculationResult.setMotorBaseNo(strTyper);//机座号
            engineCalculationResult.setMotorPower(String.format(EngineConstant.SYS_FORMAT_2F, Double.valueOf(countParam.getMotor())));//电机功率
            engineCalculationResult.setPole(String.format(EngineConstant.SYS_FORMAT_0F, Double.valueOf(countParam.getJs().toString())));//极数
        } else {
            if (motor >= 0.75 && motor <= 45) {
                engineCalculationResult.setFanModel(countParam.getFanName());//风机型号
                engineCalculationResult.setOutletVelocity(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getOutletVelocity())));//出风口风速
                engineCalculationResult.setEfficiency(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getEfficiency())));//效率
                engineCalculationResult.setAbsorbedPower(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getAbsorberPower())));//吸收功率
                engineCalculationResult.setRPM(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getRpm())));//转速
                engineCalculationResult.setNoise(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getNoise())));//噪音
                engineCalculationResult.setMotorBaseNo(strTyper);//机座号
                engineCalculationResult.setMotorPower(String.format(EngineConstant.SYS_FORMAT_2F, Double.valueOf(countParam.getMotor())));//电机功率
                engineCalculationResult.setPole(String.format(EngineConstant.SYS_FORMAT_0F, Double.valueOf(countParam.getJs().toString())));//极数
            }
        }
        return engineCalculationResult;
    }

    public FanInfo packageResult2R(FanCountParam countParam, SKFanType skFanTypePo, FanInParam fanInParam) {
        FanInfo info = new FanInfo();
        int shape = Utils.getShape(fanInParam);
        String strTyper = EngineConstant.SYS_BLANK;
        String machinesite = EngineConstant.SYS_BLANK;
        double motor = countParam.getMotor();
        //电机功率>= 0.55 && 电机功率<= 55
        if (motor >= 0.55 && motor <= 55) {
            SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, String.valueOf(countParam.getMotor()),
                    String.valueOf(countParam.getJs()));
            if (EmptyUtil.isNotEmpty(skFanMotor)) {
                strTyper = skFanMotor.getMachineSiteNo();//机器位置标号
                machinesite = skFanMotor.getMachineSite();//机器位置
            }
        }
        //电机类型为双速和变频
        if (countParam.getMotorType().equals(EngineConstant.SYS_ALPHABET_C_UP) && !countParam.getMotorType().equals(EngineConstant.SYS_ALPHABET_D_UP)) {
            info.setFanModel(countParam.getFanName());//风机型号
            info.setOutletVelocity(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getOutletVelocity())));//出风口风速
            info.setEfficiency(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getEfficiency())));//效率
            info.setAbsorbedPower(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getAbsorberPower())));//吸收功率
            info.setRPM(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getRpm())));//转速
            info.setNoise(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getNoise())));//噪音
            info.setMotorBaseNo(strTyper);//机座号
            info.setMotorPower(String.format(EngineConstant.SYS_FORMAT_2F, Double.valueOf(countParam.getMotor())));//电机功率
            info.setPole(String.format(EngineConstant.SYS_FORMAT_0F, Double.valueOf(countParam.getJs().toString())));//极数
        } else {
            if (motor >= 0.75 && motor <= 45) {
                info.setFanModel(countParam.getFanName());//风机型号
                info.setOutletVelocity(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getOutletVelocity())));//出风口风速
                info.setEfficiency(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getEfficiency())));//效率
                info.setAbsorbedPower(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getAbsorberPower())));//吸收功率
                info.setRPM(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getRpm())));//转速
                info.setNoise(String.format(EngineConstant.SYS_FORMAT_1F, Double.valueOf(countParam.getNoise())));//噪音
                info.setMotorBaseNo(strTyper);//机座号
                info.setMotorPower(String.format(EngineConstant.SYS_FORMAT_2F, Double.valueOf(countParam.getMotor())));//电机功率
                info.setPole(String.format(EngineConstant.SYS_FORMAT_0F, Double.valueOf(countParam.getJs().toString())));//极数
            }
        }
        return info;
    }


    @SuppressWarnings("unused")
    private void findTwoSpeedMotor(FanCountParam countParam) {
        String sql = "select top 1 * from S_TWOSPEEDMOTOR where JS= " + countParam.getJs() + "  and supplier=" + countParam.getFanSupplier() + " and gl>=" + countParam.getMotor() + " order by js,gl";
        countParam.setMotorType("sql.KWP-sql.MEMO");
        countParam.setMotorBase("sql.MotorBase");
    }

    //计算最大功率、最大机器
    public FanCountParam calRearMotorMaxPower(FanInParam windMachineInParam, String fanName) {
        FanCountParam countParam = new FanCountParam();
        /*//根据条件查询机组型号、供应商 亿利达、风机形式
        //机组型号 供应商 亿利达
        StringBuffer sbf = new StringBuffer();
        sbf.append("select * from s_k_fan_type_1 where type=" + windMachineInParam.getSerial() + " and supplier=" + windMachineInParam.getFanSupplier());
        //如果风机形式不是前弯或后弯
        if (!windMachineInParam.getOutlet().equals("0")) {
            sbf.append(" and bend =" + windMachineInParam.getOutlet() + " and fan_query=" + fanName + " order by type,bend asc");
        }*/

        SKFanType1 skFanType1 = null;
        if (!windMachineInParam.getOutlet().equals("0")) {// 如果风机形式不为前弯或后弯
            skFanType1 = AhuMetadata.findOne(SKFanType1.class, windMachineInParam.getSerial(), windMachineInParam.getFanSupplier(), fanName, windMachineInParam.getOutlet()); //根据机组型号、供应商查询：机组型号、供应商、风机形式
        } else {
            skFanType1 = AhuMetadata.findOne(SKFanType1.class, windMachineInParam.getSerial(), windMachineInParam.getFanSupplier());
        }

        countParam.setrMotorMaxMachineSite(skFanType1.getMaxMachineSite());
        countParam.setrMotorMaxPower(skFanType1.getMaxPower());
        return countParam;
    }

    //计算电机级数
    public Integer packageCALJS(Double fanspeed) {
        if ((fanspeed >= 2880) && (fanspeed <= 4032)) {
            return 2;
        } else if ((fanspeed >= 1440) && (fanspeed < 2880)) {
            return 4;
        } else if ((fanspeed >= 960) && (fanspeed < 1440)) {
            return 6;
        } else {
            return 8;
        }
    }

    //计算电机级数
    public Integer packageCALJSEX2(Double fanspeed) {
        if ((fanspeed >= 2001) && (fanspeed <= 5100)) {
            return 2;
        } else if ((fanspeed >= 651) && (fanspeed < 2001)) {
            return 4;
        } else if ((fanspeed >= 401) && (fanspeed < 651)) {
            return 6;
        } else {
            return 8;
        }
    }
    //计算电机级数kruger
    public Integer packageCALJSEX3(Double fanspeed) {
        if ((fanspeed >= 2200)) {
            return 2;
        } else if ((fanspeed >= 750) && (fanspeed < 2200)) {
            return 4;
        } else if ((fanspeed >= 450) && (fanspeed < 750)) {
            return 6;
        } else {
            return 8;
        }
    }

}