package com.carrier.ahu.engine.fan.factory;

import java.util.Vector;

import com.carrier.ahu.metadata.entity.fan.SKFanType;

/**
 * Created by liangd4 on 2017/8/29.
 */
public abstract class AFanEngine implements IFanEngine {

	Vector<SKFanType> conditions = new Vector<SKFanType>();

	public void appendFanCondition(SKFanType po) {

		this.conditions.addElement(po);
	}

	public Vector<SKFanType> getFanConditions() {
		return this.conditions;
	}

	public void clearFanCondition() {

		this.conditions.clear();
	}
}
