package com.carrier.ahu.engine.service.heatRecycle;

import java.util.List;

import com.carrier.ahu.common.exception.engine.HeatExchangeEngineException;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
public interface HeatRecycleService {

    List<HeatRecycleInfo> getHeatRecycleEngine(HeatRecycleParam heatRecycleParam, String version) throws HeatExchangeEngineException;

    List<HeatRecycleInfo> getHeatRecycleEngineByRule(HeatRecycleParam heatRecycleParam, PublicRuleParam publicRuleParam, String version) throws Exception;
}
