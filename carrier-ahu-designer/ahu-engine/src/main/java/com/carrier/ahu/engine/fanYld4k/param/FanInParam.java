package com.carrier.ahu.engine.fanYld4k.param;

import com.carrier.ahu.common.entity.UserConfigParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class FanInParam extends UserConfigParam {

    /*机组信息*/
    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection;//风向

    //========================调用风机DLL页面传入参数=================//
    private double altitude;//海拔高度 老系统0-8000米
    private String serial;//机组型号 39G0608
    private String fanSupplier;//供应商 固定值：A
    private String outlet;//风机形式
    private String type;//电机类型 1:二级能效电机、2:三级能效电机、3:防爆电机、4:双速电机、5:变频电机、6:变频电机(直连)
    private String supplier;//电机品牌 default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
    private String power;//电源

    private String airTemperature;//GK DLL调用参数 暂不使用
    private String motorPosition;//电机位置
    private String outletDirection;//风机出风方向
    private double totalStatic;// 总静压 风压（Pa）风压=总静压 总静压=余压+其它压降 余压=机外静压
    private String projectId;


}
