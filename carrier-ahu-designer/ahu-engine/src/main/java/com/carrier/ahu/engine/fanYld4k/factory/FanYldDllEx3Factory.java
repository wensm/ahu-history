package com.carrier.ahu.engine.fanYld4k.factory;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.FanLib;
import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;
import com.carrier.ahu.unit.CCRDFileUtil;

/**
 * Created by liangd4 on 2017/7/3.
 * 风机调用DLL
 */
public class FanYldDllEx3Factory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanYldDllEx3Factory.class.getName());
    private Lock fanLock = new ReentrantLock();

    //封装fanSelectionEx3
    public List<FanYldDllInfo> fanSelectionEx3(FanYldDllInParam fanYldDllInParam,FanInParam fanInParam) throws FanEngineException {
        List<FanYldDllInfo> windMachineDLLResultList = new ArrayList<FanYldDllInfo>();
        try {
            fanLock.lock();
            FanLib fl = FanLib.INSTANCE;
            double density = fl.CalAirDensity2(fanYldDllInParam.getAltitude(), 21);
            LOGGER.info("pSeriesNames:" + fanYldDllInParam.getPSeriesNames() + " pSubTypeNames:" + fanYldDllInParam.getPSubTypeNames() + " pFlow:" + fanYldDllInParam.getPFlow() + " pPres:" + fanYldDllInParam.getPPres() + " pPresType:" + fanYldDllInParam.getPPresType());
            LOGGER.info("pOutFanType:" + fanYldDllInParam.getPOutFanType() + " pAirDesityType:" + fanYldDllInParam.getPAirDesityType() + " pAirDensity:" + density + " pB:" + fanYldDllInParam.getPB() + " pT:" + fanYldDllInParam.getPT());
            LOGGER.info("pV:" + fanYldDllInParam.getPV() + " pMotorSafeCoff:" + fanYldDllInParam.getPMotorSafeCoff() + " pUserSetMotorSafeCoff:" + fanYldDllInParam.getPUserSetMotorSafeCoff());
            int count = fl.FanSelectionEx3(fanYldDllInParam.getPSeriesNames(), fanYldDllInParam.getPSubTypeNames(), fanYldDllInParam.getPFlow(), fanYldDllInParam.getPPres(), Integer.parseInt(fanYldDllInParam.getPPresType()),
                    Integer.parseInt(fanYldDllInParam.getPOutFanType()), Integer.parseInt(fanYldDllInParam.getPAirDesityType()), density, Double.valueOf(fanYldDllInParam.getPB()), Double.valueOf(fanYldDllInParam.getPT()),
                    Double.valueOf(fanYldDllInParam.getPV()), Double.valueOf(fanYldDllInParam.getPMotorSafeCoff()), fanYldDllInParam.getPUserSetMotorSafeCoff());
            for (int i = 0; i < count; i++) {
                byte[] fanInfos = new byte[100 * 255];
                double[] fanValues = new double[500];
                fl.GetLargeResultData(i, fanInfos, fanValues);
                FanYldDllInfo fanYldDllInfo = new FanYldDllInfo();
                fanYldDllInfo.setTCount(i + 1);
                fanYldDllInfo.setBData(fanInfos);
                fanYldDllInfo.setPDoubleData(fanValues);
                fanYldDllInfo.setFanType(fanYldDllInParam.getFanType());// 机组型号 39CQ0608
                fanYldDllInfo.setFanSupplier("A");// 供应商 A：亿利达 + 谷科
                fanYldDllInfo.setMotorType(Utils.getFanType(fanInParam.getType()));// 页面选择值 电机类型 A：三级能效电机// B：防爆电机 C：双速电机// D：变频电机 E：二级能效电机
                /*调用风机曲线图*/
                String code = fanYldDllInParam.getPSeriesNames() + fanYldDllInParam.getPSubTypeNames() + fanYldDllInParam.getPFlow() + fanYldDllInParam.getPPres() + fanYldDllInParam.getPPresType() +
                        fanYldDllInParam.getPOutFanType() + fanYldDllInParam.getPAirDesityType() + density + fanYldDllInParam.getPB() + fanYldDllInParam.getPT() +
                        fanYldDllInParam.getPV() + fanYldDllInParam.getPMotorSafeCoff() + fanYldDllInParam.getPUserSetMotorSafeCoff();
                String path = EngineConstant.SYS_PATH_BMP_YLD + fanInParam.getProjectId() + EngineConstant.SYS_PUNCTUATION_SLASH + code.hashCode() + EngineConstant.SYS_PUNCTUATION_SLASH;
                if (CCRDFileUtil.fileExists(path)) {
                    path = path + EngineConstant.SYS_NAME_YLD_BMP;//亿利达需要命名图片名称
                    LOGGER.info("yld bmp files path exists share bmp");
                } else {
                    boolean b = CCRDFileUtil.createDir(path);// 调用方法创建目录
                    if (!b) {// 创建目录失败引擎无法调用成功
                        LOGGER.error("fan yld create bmp error param :" + path);
                    }
                    path = path + EngineConstant.SYS_NAME_YLD_BMP;//亿利达需要命名图片名称
                    fl.ExportPictureEx(fanYldDllInParam.getPFlow(), fanValues[8], fanInfos, fanValues, 345, 409, path, false);
                }
                path = path.replace("asserts", "files");//页面接收图片规则
                LOGGER.info("YYYYYYYYLLLLLLLLLLLLLLDDDDDDDDD BMP PATH" + path);
                fanYldDllInfo.setCurve(path);
                fanYldDllInfo.setType(fanYldDllInParam.getType());
                fanYldDllInfo.setSupplier(fanYldDllInParam.getSupplier());
                windMachineDLLResultList.add(fanYldDllInfo);
            }
            for(FanYldDllInfo info : windMachineDLLResultList){
                double[] fanValues = info.getPDoubleData();
                FanLib SYWFL = FanLib.INSTANCE;
                int SYWCount = SYWFL.SYWMotorSelection(2, Utils.getSupplierSYW(fanInParam.getSupplier()), fanValues[0], fanValues[3], fanValues[19]);
                for (int j = 0; j < SYWCount; j++) {
                    byte[] fanInfo1s = new byte[100 * 255];
                    double[] fanValue1s = new double[500];
                    SYWFL.GetLargeResultData(j, fanInfo1s, fanValue1s);
                    info.setPDoubleDataSYW(fanValue1s);
                }
            }
        } catch (Exception e) {
            LOGGER.error("fan engine failed exception massage : " + e.getMessage());
//            throw new FanEngineFailedException("fan engine failed exception massage : "+e.getMessage());
        } finally {
            fanLock.unlock();
        }
        return windMachineDLLResultList;
    }

    //封装fanSelectionEx2
    public List<FanYldDllInfo> fanSelectionEx2(FanYldDllInParam fanYldDllInParam) throws FanEngineException {
        LOGGER.info("YLDYLDYLD FanYldDllEx3Factory YLD fanSelectionEx2 " + DateUtil.getStringDate());
        List<FanYldDllInfo> fanYldDllInfoList = new ArrayList<FanYldDllInfo>();
        try {
            fanLock.lock();
            FanLib fl = FanLib.INSTANCE;
            double density = fl.CalAirDensity2(fanYldDllInParam.getAltitude(), 21);
            LOGGER.info("pSeriesNames:" + fanYldDllInParam.getPSeriesNames() + " pSubTypeNames:" + fanYldDllInParam.getPSubTypeNames() + " pFlow:" + fanYldDllInParam.getPFlow() + " pPres:" + fanYldDllInParam.getPPres() + " pPresType:" + fanYldDllInParam.getPPresType());
            LOGGER.info("pOutFanType:" + fanYldDllInParam.getPOutFanType() + " pAirDesityType:" + fanYldDllInParam.getPAirDesityType() + " pAirDensity:" + density + " pB:" + fanYldDllInParam.getPB() + " pT:" + fanYldDllInParam.getPT());
            LOGGER.info("pV:" + fanYldDllInParam.getPV() + " pMotorSafeCoff:" + fanYldDllInParam.getPMotorSafeCoff() + " pUserSetMotorSafeCoff:" + fanYldDllInParam.getPUserSetMotorSafeCoff());
            int count = fl.FanSelectionEx2(fanYldDllInParam.getPSeriesNames(), fanYldDllInParam.getPSubTypeNames(), fanYldDllInParam.getPFlow(), fanYldDllInParam.getPPres(), Integer.parseInt(fanYldDllInParam.getPPresType()),
                    Integer.parseInt(fanYldDllInParam.getPAirDesityType()), density, Double.valueOf(fanYldDllInParam.getPB()), Double.valueOf(fanYldDllInParam.getPT()),
                    Double.valueOf(fanYldDllInParam.getPV()), Double.valueOf(fanYldDllInParam.getPMotorSafeCoff()), fanYldDllInParam.getPUserSetMotorSafeCoff());
            for (int i = 0; i < count; i++) {
                byte[] fanInfos = new byte[100 * 255];
                double[] fanValues = new double[500];
                fl.GetLargeResultData(i, fanInfos, fanValues);
                FanYldDllInfo fanYldDllInfo = new FanYldDllInfo();
                fanYldDllInfo.setTCount(i + 1);
                fanYldDllInfo.setBData(fanInfos);
                fanYldDllInfo.setPDoubleData(fanValues);
                fanYldDllInfo.setFanType(fanYldDllInParam.getFanType());// 机组型号 39CQ0608
                fanYldDllInfo.setFanSupplier("A");// 供应商 A：亿利达 + 谷科
                fanYldDllInfo.setMotorType(EngineConstant.JSON_FAN_TYPE_C);// 页面选择值 电机类型 A：三级能效电机// B：防爆电机 C：双速电机// D：变频电机 E：二级能效电机
                /*调用风机曲线图*/
                String code = fanYldDllInParam.getPSeriesNames() + fanYldDllInParam.getPSubTypeNames() + fanYldDllInParam.getPFlow() + fanYldDllInParam.getPPres() + fanYldDllInParam.getPPresType() +
                        fanYldDllInParam.getPOutFanType() + fanYldDllInParam.getPAirDesityType() + density + fanYldDllInParam.getPB() + fanYldDllInParam.getPT() +
                        fanYldDllInParam.getPV() + fanYldDllInParam.getPMotorSafeCoff() + fanYldDllInParam.getPUserSetMotorSafeCoff();
                String path = EngineConstant.SYS_PATH_BMP_YLD + fanYldDllInParam.getProjectId() + EngineConstant.SYS_PUNCTUATION_SLASH + code.hashCode() + EngineConstant.SYS_PUNCTUATION_SLASH;
                if (CCRDFileUtil.fileExists(path)) {
                    path = path + EngineConstant.SYS_NAME_YLD_BMP;//亿利达需要命名图片名称
                    LOGGER.info("yld bmp files path exists share bmp");
                } else {
                    boolean b = CCRDFileUtil.createDir(path);// 调用方法创建目录
                    if (!b) {// 创建目录失败引擎无法调用成功
                        LOGGER.error("fan yld create bmp error param :" + path);
                    }
                    path = path + EngineConstant.SYS_NAME_YLD_BMP;//亿利达需要命名图片名称
                    fl.ExportPictureEx(fanYldDllInParam.getPFlow(), fanValues[8], fanInfos, fanValues, 345, 409, path, false);
                }
                path = path.replace("asserts", "files");//页面接收图片规则
                LOGGER.info("YYYYYYYYLLLLLLLLLLLLLLDDDDDDDDD BMP PATH" + path);
                fanYldDllInfo.setCurve(path);
                fanYldDllInfoList.add(fanYldDllInfo);
            }
        } catch (Exception e) {
            LOGGER.error("fan engine failed exception massage : " + e.getMessage());
//            throw new FanEngineFailedException("fan engine failed exception massage : "+e.getMessage());
        } finally {
            fanLock.unlock();
        }
        return fanYldDllInfoList;
    }

    //封装fanSelectionEx2+K
    @SuppressWarnings("unused")
    public FanYldDllInfo fanSelectionEx2K(FanYldDllInParam windMachineOutParam) {
        FanYldDllInfo windMachineDLLResult = new FanYldDllInfo();
        String fanName2 = EngineConstant.SYS_BLANK;
        //调用亿利达风机FanSelectionEx2并返回结果值
        //yilidafancal1("SYD", "R", fTPressure, fAirQ, S_unit.UnitPro.Attitude, js, ptt1, maxmotorpower,maxspeed, eff, power, nspeed, pt, co, rla, Motor, fanname2, Souspe, Souspep, totalSoue, totalsoupe, suc);
        Boolean suc = true;//接口返回
        Double motor = 0.00;//接口返回
        String js = EngineConstant.SYS_BLANK;//接口返回级数
        if (suc && motor >= 0.75 && motor <= 45) {
            //fanName2 = FC + fan字段返回值取右边三位
            fanName2 = EngineConstant.JSON_FAN_FANMODEL_FC + "450" + EngineConstant.JSON_FAN_FANMODEL_K;
            String sqlStr = "Select * from s_k_fan_motor where power=" + motor + " and level=" + js;
            Boolean flg = true;
            if (js.equals("1") || !flg || Double.valueOf("machinesite") > Double.valueOf("maxmachinesite")) {
                System.out.println(EngineConstant.SYS_BLANK);
            } else {
                //返回数据结果
                //YLDlistitemadd(fanname2, co, js, K, lQry.FieldByName("maxmachinesite").AsFloat, lQry.FieldByName("Maxpower").AsFloat, CBFS_MotorType.value, RMotorMaxpower, RMotormaxmachinesite);
            }
        }
        return windMachineDLLResult;
    }

    //计算空气密度 单位:kg/m3 //海拔高度 单位：m、大气温度 单位：℃
    public Double calAirDensity2(Double pH, Double pTemp) {
        //调用DLL文件
        return null;
    }
}
