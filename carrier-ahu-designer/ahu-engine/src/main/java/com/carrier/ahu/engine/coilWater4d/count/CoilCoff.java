package com.carrier.ahu.engine.coilWater4d.count;

import java.util.List;

import org.apache.catalina.Engine;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.SZua;

//盘管系数计算
public class CoilCoff {

    //盘管系数计算
    public static Double coilCoff(int tubeDiameter, String season, String uniTtype, double indb, double inwb, int rownum, boolean ahri) {
        uniTtype = uniTtype.substring(uniTtype.length()-4,uniTtype.length());
        Double coilCoff = 0.00;
        if (ahri) {//欧标
            coilCoff = coilCoffAHRI(tubeDiameter, season, uniTtype, indb, inwb, rownum);
        } else {//非欧标
            if (EngineConstant.JSON_AHU_SEASON_W.equals(season)) {//冬季
                return 1.13;
            } else {//夏季
                Double zua = 0.00;
                if(uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)){
                    zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
                }else{
                    zua = (double) (1 + (2 - Float.parseFloat(String.valueOf(rownum))) / 100);//1+(2-排数)/100
                }
                coilCoff = packageZua(indb, inwb, rownum, zua);
            }
        }
        return coilCoff;
    }

    //欧标
    public static Double coilCoffAHRI(int tubeDiameter, String season, String uniTtype, double indb, double inwb, int rownum) {
        Double coilCoff = 0.00;
        if (EngineConstant.JSON_AHU_SEASON_W.equals(season)) {//如果为冬季
            if (1 == tubeDiameter) {//管径为四分管
                coilCoff = 1.1;
            } else {//管径为三分管
                coilCoff = 1.02;
            }
        } else {
            if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
                coilCoff = packageZut(indb, inwb, rownum, EngineConstant.JSON_COIL_ZUA_0711);
            } else {
                coilCoff = packageZut(indb, inwb, rownum, null);
            }
        }

        return coilCoff;
    }

    //封装zua
    private static Double packageZua(double indb, double inwb, int rownum, Double zua) {
        if ((indb - 27 <= 0.0001) && (inwb - 19.5 <= 0.0001)) {
            return zua;//如果干球-27<=0.0001 并且 湿球-19.5<=0.0001 盘管系数=zua
        } else if ((indb - 35) < 0.0001 && inwb - 28 < 0.0001 && zua > 1) {
            return zua * 1.02;//如果干球-35<=0.0001 并且 湿球-28<=0.0001 并且 zua>1   盘管系数=zua*1.02
        } else if (((indb - 35) < 0.0001 && inwb - 28 < 0.0001) && zua < 1) {
            return zua / 1.02;//如果干球-35<=0.0001 并且 湿球-28<=0.0001 并且 zua<1   盘管系数=zua/1.02
        } else if (zua > 1) {
            return zua * 1.01;//如果zua>1 盘管系数=zua*1.01
        } else if (zua < 1) {
            return zua / 1.01;//如果zua<1 盘管系数=zua/1.01
        }
        return zua;
    }

    //封装zut
    private static Double packageZut(double indb, double inwb, int rownum, String zuafield) {
        double d = 0.00;
        if (EngineConstant.JSON_COIL_ZUA_0711.equals(zuafield)) {//机型为0711
            zuafield = EngineConstant.JSON_COIL_ZUA_0711;
        } else {
            if ((indb - 27 <= 0.0001) && (inwb - 19.5 <= 0.0001)) {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_R;
            } else if ((indb - 35) < 0.0001 && inwb - 28 < 0.0001) {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_F;
            } else {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_O;
            }
        }
        List<SZua> sZuaPoList = AhuMetadata.findList(SZua.class, String.valueOf(rownum));
        for (SZua sZuaPo : sZuaPoList) {
            if (EngineConstant.JSON_COIL_ZUA_0711.equals(zuafield)) {
                d = sZuaPo.getZua0711();
            } else if (EngineConstant.JSON_COIL_ZUA_C_R.equals(zuafield)) {
                d = sZuaPo.getZuaCR();
            } else if (EngineConstant.JSON_COIL_ZUA_C_F.equals(zuafield)) {
                d = sZuaPo.getZuaCF();
            } else if (EngineConstant.JSON_COIL_ZUA_C_O.equals(zuafield)) {
                d = sZuaPo.getZuaCO();
            }
        }
        return d;
    }
}

/*if (2 == tubeDiameter && "S".equals(season)) {//管径（三分管）并且季节（夏）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        } else {
        coilCoff = packageZut(indb, inwb, rownum, null);
        }
        } else if (2 == tubeDiameter && "W".equals(season)) {//管径（三分管）并且季节（冬）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        coilCoff = 1.13;
        } else {
        coilCoff = packageZut(indb, inwb, rownum, null);
        }
        } else if (1 == tubeDiameter && "S".equals(season)) {//管径（四分管）并且季节（夏）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        } else {
        zua = (double) (1 + (2 - Float.parseFloat(String.valueOf(rownum))) / 100);//1+(2-排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        }
        } else if (1 == tubeDiameter && "W".equals(season)) {//管径（四分管）并且季节（冬）
        coilCoff = 1.13;
        }*/
