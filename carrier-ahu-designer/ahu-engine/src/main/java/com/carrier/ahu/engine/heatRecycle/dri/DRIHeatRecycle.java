package com.carrier.ahu.engine.heatRecycle.dri;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycle;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.DRIBean;
import com.carrier.ahu.engine.heatrecycle.MS200Lib;
import com.carrier.ahu.engine.heatrecycle.MS270Lib;
import com.carrier.ahu.engine.heatrecycle.SG200Lib;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.EfficientParam;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by Braden Zhou on 2019/01/22.
 */
public class DRIHeatRecycle extends HeatRecycle {

    private enum DRIType {
        MOLECULAR("1"), SILICA_GEL("2"), PURE_ALUMINUM("3");

        private String code;

        private DRIType(String code) {
            this.code = code;
        }
    }

    public List<HeatRecycleInfo> getHeatRecyleInfo(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        String unitModel = heatRecycleParam.getSerial();// 机组型号
        double supplyair = heatRecycleParam.getNAVolume();// 新风风量
        double returnAir = heatRecycleParam.getRAVolume();// 回风风量
        double inputOAS_DBT = heatRecycleParam.getSNewDryBulbT();// 夏季新风干球温度
        double inputOAS_WBT = heatRecycleParam.getSNewWetBulbT();// 夏季新风湿球温度
        double inputRaS_DBT = heatRecycleParam.getSInDryBulbT();// 夏季回风干球温度
        double inputRaS_RH = heatRecycleParam.getSInRelativeT();// 夏季回风相对湿度
        boolean DRIunitSI = true;
        double DRIinputSupplyAir = heatRecycleParam.getNAVolume();// 新风
        if (Math.abs(supplyair - 3780) < 0.001) {
            DRIinputSupplyAir = 3790;
        } else {
            DRIinputSupplyAir = supplyair;
        }
        double DRIinputReturnAir = heatRecycleParam.getRAVolume();// 回风
        if (Math.abs(returnAir - 3780) < 0.001) {
            DRIinputReturnAir = 3790;
        } else {
            DRIinputReturnAir = returnAir;
        }
        double DRIinputOAS_DBT = inputOAS_DBT;
        double DRIinputOAS_WBT = inputOAS_WBT;
        double DRIinputRaS_DBT = inputRaS_DBT;
        double DRIinputRaS_RH = inputRaS_RH;
        boolean DRIwheelSensible = true;
        DRIBean driBean = null;
        if (DRIType.MOLECULAR.code.equals(heatRecycleParam.getModel())) {// 分子筛
            if (heatRecycleParam.getWheelDepth() == SystemCalculateConstants.MOLECULAR_200) {// 转轮厚度200
                driBean = MS200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir,
                        DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
            } else {
                driBean = MS270Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir,
                        DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
            }
        } else {
            driBean = SG200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT,
                    DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
        }
        String ECOFRESHModel = driBean.getDRIOutECOFRESHModel();
        double saEfficiencySummerTotal = driBean.getDRIoutSaEfficiencySummerTotal();
        double saEfficiencyWinterTotal = driBean.getDRIoutSaEfficiencyWinterTotal();
        double outSaPDropWinter = driBean.getDRIoutSaPDropWinter();
        double calwheeldia;
        if (ECOFRESHModel.length() == 13) {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        String efficiencyType = EngineConstant.SYS_STRING_NUMBER_1;
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, efficiencyType);
        double maxwheeldia;
        double wheeltype;
        if (heatRecycleParam.getSerial().contains(SystemCalculateConstants.AHU_PRODUCT_39CQ)) {
            maxwheeldia = partWWheelPo.getWheelDiaByAir39CQ();
            wheeltype = partWWheelPo.getWheelTypeByAir39CQ();
        } else {
            maxwheeldia = partWWheelPo.getWheelDiaByAir39G();
            wheeltype = partWWheelPo.getWheelTypeByAir39G();
        }
        int ecofreshmodel = BaseDataUtil.doubleConversionInteger(calwheeldia);
        if (calwheeldia > maxwheeldia) {
            double DRIinputModel = wheeltype;
            if (DRIType.MOLECULAR.code.equals(heatRecycleParam.getModel())) {// 分子筛
                if (heatRecycleParam.getWheelDepth() == SystemCalculateConstants.MOLECULAR_200) {// 转轮厚度200
                    driBean = MS200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir,
                            DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible,
                            DRIinputModel);
                } else {
                    driBean = MS270Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir,
                            DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible,
                            DRIinputModel);
                }
            } else {
                driBean = SG200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir,
                        DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible,
                        DRIinputModel);
            }
            saEfficiencySummerTotal = driBean.getDRIoutSaEfficiencySummerTotal();
            saEfficiencyWinterTotal = driBean.getDRIoutSaEfficiencyWinterTotal();
            outSaPDropWinter = driBean.getDRIoutSaPDropWinter();
            ECOFRESHModel = driBean.getDRIOutECOFRESHModel();
            ecofreshmodel = BaseDataUtil.doubleConversionInteger(maxwheeldia);
        }
        if (EmptyUtil.isNotEmpty(ECOFRESHModel)) {
            if (ECOFRESHModel.length() == 13) {
                ecofreshmodel = BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 7));
            } else {
                ecofreshmodel = BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 8));
            }
        }

        // add summer
        EfficientParam outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencySummerTotal, 1));

        EfficientParam exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencySummerTotal, 1));

        HeatRecycleInfo summerHRI = getSummerHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        summerHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_SUMMER);
        summerHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        heatRecycleInfoList.add(summerHRI);

        // add winter
        outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencyWinterTotal, 1));

        exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencyWinterTotal, 1));

        HeatRecycleInfo winterHRI = getWinterHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        winterHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_WINTER);
        winterHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        heatRecycleInfoList.add(winterHRI);
        return heatRecycleInfoList;
    }

}
