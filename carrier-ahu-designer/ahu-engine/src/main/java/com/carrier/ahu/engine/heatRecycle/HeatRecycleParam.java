package com.carrier.ahu.engine.heatRecycle;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
@Data
public class HeatRecycleParam {

    /*机组参数*/
    private String serial;//机组型号
    private double seaLevel;//海拔高度
    /*风量参数*/
    private double RAVolume;//回风量
    private double NAVolume;//新风量
    /*新风参数*/
    private double SNewDryBulbT;//夏季干球温度
    private double SNewWetBulbT;//夏季湿球温度
    private double SNewRelativeT;//夏季相对湿度
    private double WNewDryBulbT;//冬季干球温度
    private double WNewWetBulbT;//夏季湿球温度
    private double WNewRelativeT;//冬季相对湿度
    /*回风参数*/
    private double SInDryBulbT;//夏季干球温度
    private double SInWetBulbT;//夏季湿球温度
    private double SInRelativeT;//夏季相对湿度
    private double WInDryBulbT;//冬季干球温度
    private double WInWetBulbT;//冬季湿球温度
    private double WInRelativeT;//冬季相对湿度
    /*页面逻辑*/
    private String heatRecoveryType;//热回收形式 板换、heatWheel:转轮
    private String Brand;//品牌
    private String Model;//热回收型号 1：分子筛 2：硅胶
    private int WheelDepth;//转轮厚度
    private String EfficiencyType;//效率类型
    /*勾选值*/
    private boolean NSEnable;//非标无转轮
    private boolean EnableWinter;//冬季
    private boolean EnableSummer;//夏季


}
