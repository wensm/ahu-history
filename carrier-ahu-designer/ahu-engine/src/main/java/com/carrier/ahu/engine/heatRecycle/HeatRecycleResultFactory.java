package com.carrier.ahu.engine.heatRecycle;

/**
 * Created by LIANGD4 on 2018/2/5.
 */
public class HeatRecycleResultFactory {
    /*输出特殊字段加工*/
    public HeatRecycleInfo packageHeatRecycleInfo(HeatRecycleInfo heatRecycleInfo, HeatRecycleParam heatRecycleParam) {

        /*heatRecycleInfo.setSReturnSAirResistance(BaseDataUtil.doubleConversionString(airResistance));//夏季空气阻力送风侧
        heatRecycleInfo.setSReturnSEfficiency(BaseDataUtil.doubleConversionString(efficiency));//夏季效率送风侧
        heatRecycleInfo.setWReturnSAirResistance(BaseDataUtil.doubleConversionString(airResistance));//冬季空气阻力送风侧
        heatRecycleInfo.setWReturnSEfficiency(BaseDataUtil.doubleConversionString(efficiency));//冬季效率送风侧
        heatRecycleInfo.setWReturnEAirResistance(BaseDataUtil.doubleConversionString(airResistance));//冬季空气阻力进风侧
        heatRecycleInfo.setWReturnEEfficiency(BaseDataUtil.doubleConversionString(efficiency));//冬季效率进风侧*/
        return heatRecycleInfo;
    }
}
