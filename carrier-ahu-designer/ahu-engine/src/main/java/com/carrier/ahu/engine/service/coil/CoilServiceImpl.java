package com.carrier.ahu.engine.service.coil;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilDx4d.DXCoilService;
import com.carrier.ahu.engine.coilWater4d.WaterCoilService;
import com.carrier.ahu.engine.coilWater4d.util.CoilPackageUtil;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.S4FArea;
import com.carrier.ahu.metadata.entity.coil.S4FTHPress;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by liangd4 on 2017/9/8.
 */
@Component
public class CoilServiceImpl implements CoilService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoilServiceImpl.class.getName());

    @Autowired
    DXCoilService dxCoilService;
    @Autowired
    WaterCoilService waterCoilService;

    @Override
    public List<CoilInfo> getCoil(CoilInParam coilInParam, PublicRuleParam publicRuleParam, String coilType, String version) {
        LOGGER.info("CoilServiceImpl coil calculator begin" + JSONArray.toJSONString(coilInParam));
        boolean validationBoolean = CoilPackageUtil.coilInfoParamValidation(coilInParam);
        if (validationBoolean) {
            coilInParam = CoilPackageUtil.packageCoilInParam(coilInParam, publicRuleParam, coilType);
            List<CoilInfo> coilInfoList;
            if (!EmptyUtil.isEmpty(coilInParam)) {
                if (SystemCalculateConstants.DXCoil.equals(coilInParam.getCoilType())) {
                    coilInfoList = dxCoilService.getDXCoil(coilInParam);
                } else {
                    coilInfoList = waterCoilService.getWaterCoil(coilInParam);
                }
                coilInfoList = CoilPackageUtil.getWaterCoilByRule(coilInfoList, publicRuleParam, coilInParam);
                LOGGER.info("CoilServiceImpl coil calculator end");
                return coilInfoList;
            }
        }
        return null;
    }

    /**
     * 计算蒸汽盘管蒸汽压力
     *
     * @param parmTin
     * @param parmTout
     * @param parmAirFlow
     * @param serial
     * @param parmMFlag
     * @return
     */
    @Override
    public double getFFrm4fVaporPres(double parmTin, double parmTout, double parmAirFlow, String serial, String parmMFlag) {
        double ldVarFF = 0.00;
        double ldVarF = 0.00;
        double ldVarTH = 0.00;
        parmAirFlow = parmAirFlow / 3600.0;
        S4FArea s4FArea = AhuMetadata.findOne(S4FArea.class, serial);
        if (!EmptyUtil.isEmpty(s4FArea)) {
            ldVarF = s4FArea.getFArea();
            ldVarFF = s4FArea.getFfArea();
        }
        double ldVarDensity = AirConditionUtils.FFormulaDensity(parmTin, parmTout);
        double ldVarVr = parmAirFlow * ldVarDensity / ldVarFF;
        double ldVarQ = parmAirFlow * 1.01 * (parmTout - parmTin) * ldVarDensity * 1000.0;
        double ldVarK = 17.0 * Math.pow(ldVarVr, 0.608);
        if (parmMFlag.equals(EngineConstant.SYS_STRING_NUMBER_1)) {
            ldVarTH = ldVarQ * 1.2 / (ldVarF * ldVarK) + (parmTin + parmTout) / 2.0;
        } else {
            ldVarTH = ldVarQ / (ldVarF * ldVarK) + (parmTin + parmTout) / 2.0;
        }
        return FFrm4fSearchPress(ldVarTH) / 10.0;
    }

    private double FFrm4fSearchPress(double parmTH) {
        double LVarTH = 0.00;
        double retPressure = 0.00;
        double LVarTH1 = 0.00;
        double LVarPress1 = 0.00;
        double LVarTH2 = 0.00;
        double LVarPress2 = 0.00;
        if (parmTH < 100.0) {
            LVarTH = 100.0;
        } else {
            LVarTH = parmTH;
        }
        List<S4FTHPress> s4FTHPressList = AhuMetadata.findAll(S4FTHPress.class);
        ListUtils.sort(s4FTHPressList, false, EngineConstant.JSON_COIL_TH, EngineConstant.JSON_COIL_PRESSURE);
        S4FTHPress maxS4FTHPress = new S4FTHPress();
        for (S4FTHPress s4FTHPress : s4FTHPressList) {
            if (s4FTHPress.getTh() <= parmTH) {
                maxS4FTHPress = s4FTHPress;
                LVarTH1 = maxS4FTHPress.getTh();
                LVarPress1 = maxS4FTHPress.getPressure();
                break;
            }
        }
        if (Math.abs(BaseDataUtil.integerConversionDouble(maxS4FTHPress.getTh()) - LVarTH) < 0.00001) {
            retPressure = LVarPress1;
            return retPressure;
        }
        ListUtils.sort(s4FTHPressList, true, EngineConstant.JSON_COIL_TH, EngineConstant.JSON_COIL_PRESSURE);
        S4FTHPress minS4FTHPress = new S4FTHPress();
        for (S4FTHPress s4FTHPress : s4FTHPressList) {
            if (s4FTHPress.getTh() >= parmTH) {
                minS4FTHPress = s4FTHPress;
                break;
            }
        }
        LVarTH2 = minS4FTHPress.getTh();
        if (LVarTH2 < 0.00001) {
            retPressure = LVarPress1;
            return retPressure;
        }
        LVarPress2 = minS4FTHPress.getPressure();

        if (Math.abs(LVarTH - LVarTH2) < 0.00001) {
            retPressure = LVarPress2;
            return retPressure;
        }
        return AirConditionUtils.fInterpolation(LVarTH1, LVarTH, LVarTH2, LVarPress1, LVarPress2);
    }
}
