package com.carrier.ahu.engine.fan.util;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.motor.IMotorDataParser;
import com.carrier.ahu.engine.fan.motor.MortorDataParser;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.*;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.sun.jna.ptr.DoubleByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by liaoyw on 2017/3/28.
 */
public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class.getName());

    public static DoubleByReference dRef(double d) {
        return new DoubleByReference(d);
    }

    public static DoubleByReference dRef() {
        return new DoubleByReference();
    }

    static String readCpc(String cpcPath) {
        try {
            List<String> lines = Files.readAllLines(new File(cpcPath).toPath());
            return lines.stream().map(s -> s.trim()).collect(Collectors.joining(EngineConstant.SYS_PUNCTUATION_POUND));
        } catch (IOException e) {
            throw new CoilEngineException("Failed to read cpc", e);
        }
    }

    public static int getShape(FanInParam fanInParam) {
        int shape = -1;// 风机形式
        if (FAN_OPTION_OUTLET_FOB.equals(fanInParam.getOutlet())) {// 前弯或后弯
            shape = 0;
        } else if (FAN_OPTION_OUTLET_WWK.equals(fanInParam.getOutlet())) {// 无蜗壳风机
            shape = 7;
        } else if (FAN_OPTION_OUTLET_B.equals(fanInParam.getOutlet())) {// 后弯
            shape = 2;
        } else if (FAN_OPTION_OUTLET_F.equals(fanInParam.getOutlet())) {// 前弯
            shape = 1;
        }
        return shape;
    }

    //封装电机类型A：三级能效电机// B：防爆电机 C：双速电机// D：变频电机 E：二级能效电机
    public static String getFanType(String type) {
        String shape = JSON_FAN_TYPE_C;// 风机形式
        if (FAN_TYPE_1.equals(type)) {//二级能效电机
            shape = JSON_FAN_TYPE_E;
        } else if (FAN_TYPE_2.equals(type)) {//三级能效电机
            shape = JSON_FAN_TYPE_A;
        } else if (FAN_TYPE_3.equals(type)) {//防爆电机
            shape = JSON_FAN_TYPE_B;
        } else if (FAN_TYPE_4.equals(type)) {//双速电机
            shape = JSON_FAN_TYPE_C;
        } else if (FAN_TYPE_5.equals(type)) {//变频电机（IC416）
            shape = JSON_FAN_TYPE_D;
        } else if (FAN_TYPE_6.equals(type)) {//变频电机(直联)
            shape = JSON_FAN_TYPE_D;
        }
        return shape;
    }

    public static String getSupplier(String supplier) {//default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
        switch (supplier) {
            case FAN_SUPPLIER_DEFAULT:
                return DEFAULT;
            case FAN_SUPPLIER_DONGYUAN:
                return TECO;
            case FAN_SUPPLIER_ABB:
                return ABB;
            case FAN_SUPPLIER_SIMENS:
                return SIEMENS;
        }
        return EngineConstant.SYS_BLANK;
    }

    public static String getSupplierSYW(String supplier) {//default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
        switch (supplier) {
            case FAN_SUPPLIER_DEFAULT:
                return WOLONG_SYW;
            case FAN_SUPPLIER_DONGYUAN:
                return TECO_SYW;
            case FAN_SUPPLIER_ABB:
                return ABB_SYW;
        }
        return EngineConstant.SYS_BLANK;
    }

    public static String getFrequencyRange(String supplier, int js) {
        List<SVMotorRange> svMotorRangeList = AhuMetadata.findAll(SVMotorRange.class);
        if (!EmptyUtil.isEmpty(svMotorRangeList)) {
            for (SVMotorRange svMotorRange : svMotorRangeList) {
                if (svMotorRange.getMotorSupplier().equalsIgnoreCase(supplier) && svMotorRange.getJs() == js) {
                    return svMotorRange.getHZ50();
                }
            }
        }
        return EngineConstant.SYS_BLANK;
    }

    public static STwoSpeedMotor getSTwoSpeedMotor(String fanSupplier, double gl, int js) {
        List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findList(STwoSpeedMotor.class, fanSupplier);
        Collections.sort(sTwoSpeedMotorList, Comparator.comparing(STwoSpeedMotor::getGl));
        for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
            if (sTwoSpeedMotor.getGl() >= gl && sTwoSpeedMotor.getJs() == js) {
                return sTwoSpeedMotor;
            }
        }
        return null;
    }

    public static List<FanInfo> packageFanInfoList(List<FanInfo> fanInfoList, List<FanInfo> fanInfoListRep) {
        if (!EmptyUtil.isEmpty(fanInfoList) && EmptyUtil.isEmpty(fanInfoListRep)) {
            return fanInfoList;
        } else if (EmptyUtil.isEmpty(fanInfoList) && !EmptyUtil.isEmpty(fanInfoListRep)) {
            return fanInfoListRep;
        } else {
            Map<String, FanInfo> map = new HashMap<String, FanInfo>();
            List<FanInfo> newFanInfoList = new ArrayList<FanInfo>();
            for (FanInfo fanInfo : fanInfoList) {
                for (FanInfo fanInfoRep : fanInfoListRep) {
                    String fanModel = fanInfo.getFanModel();
                    String fanModelRep = fanInfoRep.getFanModel();
                    if (!fanModel.equals(fanModelRep) && !map.containsKey(fanModelRep) && !fanModelRep.contains(fanModel)) {
                        map.put(fanModelRep, fanInfoRep);
                        newFanInfoList.add(fanInfoRep);
                    }
                }
                newFanInfoList.addAll(fanInfoList);
                return newFanInfoList;
            }
        }
        return null;
    }

    /*===================================此处添加特殊逻辑================================================*/

    //封装变频范围到引擎结果中
    public static List<FanInfo> packageFrequencyRange(FanInParam fanInParam, List<FanInfo> fanInfoList) {
        IMotorDataParser parser = new MortorDataParser();
        if (!EmptyUtil.isEmpty(fanInfoList)) {//遍历结果封装变频范围
            for (FanInfo fanInfo : fanInfoList) {
                String HZ = Utils.getFrequencyRange(Utils.getSupplier(fanInParam.getSupplier()), BaseDataUtil.stringConversionInteger(fanInfo.getPole()));
                //此处封装支持功率、变频范围、马力
                //变频、变频直连
                if (fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_5) || fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_6)) {
                    if (!fanInParam.getSupplier().equals(SystemCalculateConstants.FAN_SUPPLIER_SIMENS)) {
                        fanInfo.setFrequencyRange(HZ);//变频范围
                    }
                } else if (fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_4)) {//双速
                    //增加双速逻辑
                    STwoSpeedMotor sTwoSpeedMotor = Utils.getSTwoSpeedMotor(fanInParam.getFanSupplier(), BaseDataUtil.stringConversionDouble(fanInfo.getMotorPower()), BaseDataUtil.stringConversionInteger(fanInfo.getPole()));
                    fanInfo.setMotorEff("");//双速电机 电机效率不赋值
                    if (sTwoSpeedMotor != null) {
                        fanInfo.setMotorEffPole(sTwoSpeedMotor.getKwp() + EngineConstant.SYS_PUNCTUATION_HYPHEN + sTwoSpeedMotor.getMemo());//电机效率/极数
                        fanInfo.setMotorBaseNo(sTwoSpeedMotor.getMotorBase());
                    }
                } else {//封装效率
                    if(FanEnginesEnum.KRUGER.getCode().equals(fanInParam.getFanSupplier())){
                        fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(fanInParam.getSupplier(), fanInParam.getType(), fanInfo.getMotorPower(), fanInfo.getPole())) + "%");
                    }
                }
            }
        }
        return fanInfoList;
    }

    public static List<FanInfo> packageMotorPosition(FanInParam fanInParam, List<FanInfo> fanInfoList) {
        for (FanInfo fanInfo : fanInfoList) {
            String motorPower = String.format(EngineConstant.SYS_FORMAT_1F, BaseDataUtil.stringConversionDouble(fanInfo.getMotorPower()));
            SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, motorPower, fanInfo.getPole());
            if (EmptyUtil.isEmpty(skFanMotor)) {
                continue;
            }
            String machineSite = skFanMotor.getMachineSite();
            String fanType = fanInParam.getSerial();//机组型号
            String fanSupplier = "A";//供应商
            int shape = Utils.getShape(fanInParam); //前弯或后弯
            int fanHeight = SystemCountUtil.getUnitHeight(fanType);// 风机高度
            SKFanType1 newSkFanType1 = null;
            List<SKFanType1> skFanType1List = AhuMetadata.findAll(SKFanType1.class);
            for (SKFanType1 skFanType1 : skFanType1List) {
                if (shape != 0) {
                    if (fanType.equals(skFanType1.getType()) && fanSupplier.equals(skFanType1.getSupplier()) && shape == skFanType1.getBend()) {
                        if (skFanType1.getFan().contains(BaseDataUtil.integerConversionString(fanInfo.getSeries()))) {
                            newSkFanType1 = skFanType1;
                            break;
                        }
                    }
                } else {
                    if (fanType.equals(skFanType1.getType()) && fanSupplier.equals(skFanType1.getSupplier())) {
                        if (skFanType1.getFan().contains(BaseDataUtil.integerConversionString(fanInfo.getSeries()))) {
                            newSkFanType1 = skFanType1;
                            break;
                        }
                    }
                }
            }
            if (EmptyUtil.isEmpty(newSkFanType1)) {
                LOGGER.error("Utils packageMotorPosition " + JSON.toJSONString(fanInfo));
                continue;
            }
            double RMotormaxmachinesite = newSkFanType1.getMaxMachineSite();
            double RMotorMaxpower = newSkFanType1.getMaxPower();
            double motor = BaseDataUtil.stringConversionDouble(fanInfo.getMotorPower());//功率
            if (fanHeight > 23) {
                if (SystemCalculateConstants.FAN_TYPE_5.equals(fanInParam.getType()) || SystemCalculateConstants.FAN_TYPE_6.equals(fanInParam.getType())) {
                    //大机组变频机组优先计算最新逻辑
                    String motorBaseNo = fanInfo.getMotorBaseNo();//机座号
                    ConverterMotorBig converterMotorBig = AhuMetadata.findOne(ConverterMotorBig.class, fanType, motorBaseNo);
                    if (!EmptyUtil.isEmpty(converterMotorBig)) {//如果新逻辑中没有查到符合条件的结果使用旧代码逻辑
                        String supplier = fanInParam.getSupplier();//电机品牌
                        if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
                            if (converterMotorBig.getWOLONG() > 100) {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            } else {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                            }
                        } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
                            if (converterMotorBig.getDONGYUAN() > 100) {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            } else {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                            }
                        } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
                            if (converterMotorBig.getABB() > 100) {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            } else {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                            }
                        } else {
                            LOGGER.error("fan supplier is simens " + JSON.toJSONString(fanInfo));
                        }
                    } else {
                        if ((BaseDataUtil.stringConversionDouble(machineSite) > 10000 && BaseDataUtil.stringConversionDouble(machineSite) <= RMotormaxmachinesite) ||
                                ((motor > 250) && (motor <= RMotorMaxpower)) ||
                                ((SystemCalculateConstants.FAN_TYPE_5.equals(fanInParam.getType()) || SystemCalculateConstants.FAN_TYPE_6.equals(fanInParam.getType())) &&
                                        (calRearMotor(motor, fanInfo.getPole(), fanInParam.getSupplier(), "D", BaseDataUtil.integerConversionString(fanInfo.getSeries()), 80, false, fanInParam).equals("B")))) {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                        } else {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                        }
                    }
                } else {
                    if (motor <= 250 && BaseDataUtil.stringConversionDouble(machineSite) <= 10000) {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                    } else {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    }
                }
            } else {
                double maxMachineSite = 0.0;
                double UnitMaxpower = 0.0;
                SKPlugFanType skPlugFanType = AhuMetadata.findOne(SKPlugFanType.class, SystemCountUtil.getUnit(fanInParam.getSerial()));
                if (EmptyUtil.isEmpty(skPlugFanType)) {
                    continue;
                } else {
                    maxMachineSite = skPlugFanType.getMaxMachineSite();
                    UnitMaxpower = skPlugFanType.getMaxPower();
                }

                if (SystemCalculateConstants.FAN_TYPE_4.equals(fanInParam.getType())) {//双速电机
                    if (fanInfo.getFanModel().contains("SYW")) {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    } else {
                        if ((BaseDataUtil.stringConversionDouble(machineSite) > maxMachineSite && BaseDataUtil.stringConversionDouble(machineSite) <= RMotormaxmachinesite) ||
                                ((motor > UnitMaxpower) && (motor <= RMotorMaxpower))) {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                        } else {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                        }
                    }
                } else if (SystemCalculateConstants.FAN_TYPE_5.equals(fanInParam.getType()) || SystemCalculateConstants.FAN_TYPE_6.equals(fanInParam.getType())) { //变频电机
                    if (fanInfo.getFanModel().contains("SYW")) {//SYW 直接返回后置
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    } else {
                        String fanTypeString = fanType.substring(fanType.length() - 4, fanType.length());
                        FrequencySmall frequencySmall = AhuMetadata.findOne(FrequencySmall.class, fanTypeString, String.valueOf(fanInfo.getSeries()), fanInfo.getMotorBaseNo());
                        if (EmptyUtil.isEmpty(frequencySmall)) {//如果变频效率表中没有对应数据默认使用后置
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                        } else {
                            int unitWidth = SystemCountUtil.getUnitWidth(fanInParam.getSerial());
                            if (fanInParam.getSerial().contains("39CQ")) {
                                unitWidth = unitWidth * 100 - 10;
                            } else {
                                unitWidth = unitWidth * 100;
                            }
                            int calculationValue = 0;
                            if (FAN_SUPPLIER_ABB.equals(fanInParam.getSupplier())) {
                                calculationValue = frequencySmall.getABB();
                            } else if (FAN_SUPPLIER_DEFAULT.equals(fanInParam.getSupplier())) {
                                calculationValue = frequencySmall.getWOLONG();
                            } else if (FAN_SUPPLIER_DONGYUAN.equals(fanInParam.getSupplier())) {
                                calculationValue = frequencySmall.getDONGYUAN();
                            } else if (FAN_SUPPLIER_SIMENS.equals(fanInParam.getSupplier())) {
                                calculationValue = frequencySmall.getSIEMENS();
                            }
                            if (unitWidth - calculationValue >= 0) {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            } else {
                                fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                            }
                        }
                    }
                } else {//不为变频 并且不为双速
                    if (motor >= RMotorMaxpower || BaseDataUtil.stringConversionDouble(machineSite) > RMotormaxmachinesite) {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    } else {
                        fanInfo.setMotorPosition(   SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                    }
                }
            }
        }
        return fanInfoList;
    }

    private static String calRearMotor(double motorPower, String js, String motorSupplier, String motorType, String fanType, int base, boolean vfd, FanInParam fanInParam) {
        int IVarWidth = SystemCountUtil.getUnitWidth(fanInParam.getSerial());
        SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, BaseDataUtil.doubleConversionString(motorPower), js);
        double base1 = skFanMotor.getMotorCode();
        List<SFanMotorBaseDimension> sFanMotorBaseDimensionList = AhuMetadata.findList(SFanMotorBaseDimension.class);
        SFanMotorBaseDimension newSFanMotorBaseDimension = null;
        for (SFanMotorBaseDimension sFanMotorBaseDimension : sFanMotorBaseDimensionList) {
            if (base1 >= sFanMotorBaseDimension.getBaseStart() && base1 <= sFanMotorBaseDimension.getBaseEnd()) {
                newSFanMotorBaseDimension = sFanMotorBaseDimension;
                break;
            }
        }
        double A = 0.0;
        double Q = 0.0;
        double C = 0.0;
        double INLET = 0.0;
        double MAXMOTOR = 0.0;
        double AMOTORDIMENSION = 0.0;
        double DMOTORDIMENSION = 0.0;
        double BASEDIMENSION = 0.0;
        double VFDdimension = 0.0;
        if (EmptyUtil.isEmpty(newSFanMotorBaseDimension)) {
            return "B";
        } else {
            A = newSFanMotorBaseDimension.getA();
            Q = newSFanMotorBaseDimension.getQ();
            C = newSFanMotorBaseDimension.getC();
            INLET = newSFanMotorBaseDimension.getInlet();
            MAXMOTOR = newSFanMotorBaseDimension.getMaxMotor();
        }

        SKFanMotorDimension skFanMotorDimension = AhuMetadata.findOne(SKFanMotorDimension.class, BaseDataUtil.doubleConversionString(motorPower), js, "D");
        if (!EmptyUtil.isEmpty(skFanMotorDimension)) {
            if (FAN_SUPPLIER_ABB.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getA();
            } else if (FAN_SUPPLIER_DEFAULT.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getB();
            } else if (FAN_SUPPLIER_DONGYUAN.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getC();
            } else if (FAN_SUPPLIER_SIMENS.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getS();
            }
        }

        if (fanInParam.getSerial().contains("39CQ")) {
            IVarWidth = IVarWidth * 100 - 10;
        } else {
            IVarWidth = IVarWidth * 100;
        }

        if (DMOTORDIMENSION < MAXMOTOR - Q) {
            BASEDIMENSION = A;
        } else {
            BASEDIMENSION = A + Q + DMOTORDIMENSION - MAXMOTOR;
        }

        BASEDIMENSION = BASEDIMENSION + 50 + INLET;
        if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanInParam.getSerial())) <= 811) {
            VFDdimension = 58.5;
        } else {
            VFDdimension = 93.5;
        }

        if (vfd) {
            VFDdimension = 0;
        }

        if (IVarWidth - BASEDIMENSION - VFDdimension < 0) {
            return "B";
        } else {
            return "R";
        }
    }
}