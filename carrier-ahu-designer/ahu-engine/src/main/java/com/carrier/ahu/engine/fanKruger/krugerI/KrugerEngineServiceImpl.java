package com.carrier.ahu.engine.fanKruger.krugerI;

import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.carrier.ahu.constant.CommonConstant.*;

/**
 * Created by Braden Zhou on 2019/02/20.
 */
@Service
public class KrugerEngineServiceImpl implements KrugerEngineService {

    private String krugerServiceUrl = SYS_REST_KRUGER_URL;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<KrugerSelection> select(KrugerSelectInfo krugerSelectInfo) throws FanEngineException {
        try {
            HttpEntity<KrugerSelectInfo> krugerEntity = new HttpEntity<KrugerSelectInfo>(krugerSelectInfo);
            String response = restTemplate.postForObject(krugerServiceUrl + SYS_REST_KRUGER_SELECT,
                    krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<List<KrugerSelection>>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_SELECT_FAILED, exp);
        }
    }

    @Override
    public KrugerCurvePoints curvePoints(KrugerSelection krugerSelection) throws FanEngineException {
        try {
            HttpEntity<KrugerSelection> krugerEntity = new HttpEntity<KrugerSelection>(krugerSelection);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_CURVE_POINTS, krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<KrugerCurvePoints>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_CURVE_POINTS_FAILED, exp);
        }
    }

    @Override
    public KrugerSoundSpectrumEx soundSpectrumEx(KrugerSelection krugerSelection) throws FanEngineException {
        try {
            HttpEntity<KrugerSelection> krugerEntity = new HttpEntity<KrugerSelection>(krugerSelection);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_SOUND_SPECTRUM_EX, krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<KrugerSoundSpectrumEx>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_SOUND_SPECTRUM_EX_FAILED, exp);
        }
    }

    @Override
    public KrugerChartResult generateChart(KrugerChartInfo krugerChartInfo) throws FanEngineException {
        try {
            HttpEntity<KrugerChartInfo> krugerChartEntity = new HttpEntity<KrugerChartInfo>(krugerChartInfo);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_GENERATE_CHART, krugerChartEntity, String.class);
            KrugerChartResult krugerChartResult = new KrugerChartResult();
            krugerChartResult.setSuccess(Boolean.valueOf(response));
            if (krugerChartResult.isSuccess()) {
                krugerChartResult.setChartFilePath(krugerChartInfo.getChartPath() + krugerChartInfo.getChartName()
                        + SYS_BMP_EXTENSION);
            }
            return krugerChartResult;
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_GENERATE_CHART_FAILED, exp);
        }
    }

}
