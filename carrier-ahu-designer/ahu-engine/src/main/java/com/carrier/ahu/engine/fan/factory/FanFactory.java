package com.carrier.ahu.engine.fan.factory;

import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.engine.fanGk4k.FanGkServiceImpl;
import com.carrier.ahu.engine.fanKruger.FanKrugerServiceImpl;
import com.carrier.ahu.engine.fanYld4k.FanYldEx2KServiceImpl;
import com.carrier.ahu.engine.fanYld4k.FanYldEx2RServiceImpl;
import com.carrier.ahu.engine.fanYld4k.FanYldSYQServiceImpl;
import com.carrier.ahu.engine.fanYld4k.FanYldServiceImpl;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanParams;
import com.carrier.ahu.engine.util.UserConfigUtil;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OPTION_OUTLET_WWK;

/**
 * Created by LIANGD4 on 2017/12/13.
 */
public class FanFactory {
    private static final Logger logger = LoggerFactory.getLogger(FanFactory.class.getName());

    private static final AFanEngine fanYldService = new FanYldServiceImpl();
    private static final AFanEngine FanYldEx2R = new FanYldEx2RServiceImpl();
    private static final AFanEngine FanYldEx2K = new FanYldEx2KServiceImpl();
    private static final AFanEngine fanGkService = new FanGkServiceImpl();
    private static final AFanEngine fanSYQService = new FanYldSYQServiceImpl();
    private static final AFanEngine fanKrugerService = new FanKrugerServiceImpl();

    /* 调用类别引擎计算 */
    public List<FanInfo> engineCal(FanInParam fanInParam, String version) throws FanEngineException {
        List<FanInfo> fans = new ArrayList<FanInfo>();
        Set<IFanEngine> engines = this.getEngine(fanInParam, version);
        if (engines == null) {
            return null;
        }
        for (IFanEngine engine : engines) {
            try {
                List<FanInfo> calResult = engine.cal(fanInParam);
                if (calResult == null) {
                    logger.debug("Calculate Result Count IS 0");
                    continue;
                }
                logger.debug("Calculate Result Count IS " + calResult);
                fans.addAll(calResult);
            } finally {
                if (engine instanceof AFanEngine) {
                    ((AFanEngine) engine).clearFanCondition();
                }
            }
        }
        //封装变频范围
        fans = Utils.packageFrequencyRange(fanInParam, fans);
        fans = UserConfigUtil.packageUserConfig(fanInParam, fans);
        fans = Utils.packageMotorPosition(fanInParam, fans);
        return fans;
    }

    // 判断风机引擎所属列表 YLD GK
    private Set<IFanEngine> getEngine(FanInParam fanInParam, String version) {
        /* 1.封装页面使用的参数 */
        Set<IFanEngine> engines = new LinkedHashSet<IFanEngine>();
        String fanType = fanInParam.getSerial();// 机组型号
        String fanSupplier = fanInParam.getFanSupplier() == null ? EngineConstant.SYS_ALPHABET_A_UP : fanInParam.getFanSupplier();// 供应商
        fanInParam.setFanSupplier(fanSupplier);
        int shape = Utils.getShape(fanInParam);
        /* 2.判断对应条件下使用哪种风机引擎 */
        int fanHeight = SystemCountUtil.getUnitHeight(fanType);// 风机高度
        FanParams fanParams = new FanParams();
        List<SKFanType> skFanTypePoList = null;
        // 根据条件查询结果
        if (shape == 7) {
            engines.add(fanYldService);
            return engines;
        } else {
            if (shape != 0) {// 如果风机形式不为前弯或后弯
                skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_A_UP, null, String.valueOf(shape));// 根据风机形式、供应商查询、风机形式
            } else {
                skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_A_UP);// 根据风机形式、供应商查询、风机形式
            }
        }
        if (!EmptyUtil.isEmpty(skFanTypePoList)) {
            for (SKFanType skFanTypePo : skFanTypePoList) {
                if (fanHeight > 23) { // 1.根据风机高度判断如果机组高度>23
                    fanParams.setFanEngineType(FanEnginesEnum.GK.getCode());// 风机类型
                    fanGkService.appendFanCondition(skFanTypePo);
                    engines.add(fanGkService);
                } else {
                    if (fanInParam.getFanSupplier().equals(EngineConstant.SYS_ALPHABET_A_UP) || fanInParam.getFanSupplier().equals(EngineConstant.SYS_ALPHABET_Y_UP)) {// 如果供应商：YLD Y
                        if (fanInParam.getOutlet().equals(FAN_OPTION_OUTLET_WWK)) {// 如果风机形式为无蜗壳风机
                            fanYldService.appendFanCondition(skFanTypePo);
                            engines.add(fanYldService);
                        } else {
                            String fanName = skFanTypePo.getFanQuery();
                            // 2.2.1 风机名称前三位 = ‘KTQ’ and 风机系列<=7.1
                            if (fanName.substring(0, 3).equals(EngineConstant.JSON_FAN_FANMODEL_KTQ) && Double.valueOf(fanName.substring(fanName.length() - 3, fanName.length())) <= 7.1) {
                                fanParams.setFanEngineType(FanEnginesEnum.YLD.getCode());// 风机类型
                                FanYldEx2R.appendFanCondition(skFanTypePo);
                                engines.add(FanYldEx2R);
                            } else {
                                if (fanName.substring(0, 3).equals(EngineConstant.JSON_FAN_FANMODEL_KTQ) && Double.valueOf(fanName.substring(fanName.length() - 3, fanName.length())) >= 8.0) {
                                    fanParams.setFanEngineType(FanEnginesEnum.YLD.getCode());// 风机类型
                                    FanYldEx2K.appendFanCondition(skFanTypePo);
                                    engines.add(FanYldEx2K);
                                } else {
                                    if (fanInParam.getFanSupplier().equals(EngineConstant.SYS_ALPHABET_A_UP)) {
                                        fanGkService.appendFanCondition(skFanTypePo);
                                        engines.add(fanGkService);
                                    } else if (fanInParam.getFanSupplier().equals(EngineConstant.SYS_ALPHABET_Y_UP)) {
                                        fanParams.setFanEngineType(FanEnginesEnum.YLD.getCode());// 风机类型
                                        fanSYQService.appendFanCondition(skFanTypePo);
                                        engines.add(fanSYQService);
                                    }
                                }
                            }
                        }
                    } else {// 供应商为B：kruger
                        fanParams.setFanEngineType(FanEnginesEnum.KRUGER.getCode());// 风机类型
                        engines.add(fanKrugerService);
                    }
                }
            }
        }
        return engines;
    }


}
