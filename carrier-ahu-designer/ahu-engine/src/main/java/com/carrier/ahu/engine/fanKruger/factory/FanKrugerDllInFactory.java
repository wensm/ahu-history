package com.carrier.ahu.engine.fanKruger.factory;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.unit.BaseDataUtil;

public class FanKrugerDllInFactory {

    public KrugerSelectInfo packageKrugerInParam(FanInParam fanInParam) {
        float altitude = BaseDataUtil.doubleConversionFloat(fanInParam.getAltitude()) * 1000;//海拔高度
        float volume = BaseDataUtil.intConversionFloat(fanInParam.getSairvolume());//送风量
        if (AirDirectionEnum.RETURNAIR.getCode().equals(fanInParam.getAirDirection())) {//风向
            volume = BaseDataUtil.intConversionFloat(fanInParam.getEairvolume());//回风量
        }
        double totalStatic = fanInParam.getTotalStatic();//总静压
        KrugerSelectInfo krugerSelectInfo = new KrugerSelectInfo();
        krugerSelectInfo.setVolume(volume);
        krugerSelectInfo.setAltitude(altitude);
        krugerSelectInfo.setPressure(BaseDataUtil.doubleConversionFloat(totalStatic));
        krugerSelectInfo.setPressureType(0);//ptStatic
        krugerSelectInfo.setVolumeUnit(1);//vuM3H
        krugerSelectInfo.setPressureUnit(0);//puPa
        krugerSelectInfo.setVelocityUnit(0);//vuMS
        krugerSelectInfo.setProductType(0);//cproducttype
        krugerSelectInfo.setMinStyle(0);//cMinstyle
        krugerSelectInfo.setMinClass(0);//FcI
        krugerSelectInfo.setSoundCondition(0);//scRoom
        krugerSelectInfo.setFanCasing(2);//fcsingleframe
        krugerSelectInfo.setFanWidth(0);//fwsingle
        krugerSelectInfo.setAltitudeUnit(0);//auM auFT
        krugerSelectInfo.setOutletType(0);//otducted
        krugerSelectInfo.setSoundDistanceUnit(1);//duM
        krugerSelectInfo.setTemperatureUnit(0);//degreeC
        /*固定值*/
        krugerSelectInfo.setCallType(0);
        krugerSelectInfo.setDebug(0);
        krugerSelectInfo.setHz(50);
        krugerSelectInfo.setFanSize(0);
        krugerSelectInfo.setTemperature(20);
        krugerSelectInfo.setServiceFactor(1.3f);
        krugerSelectInfo.setSoundDistance(1);
        /*待定*/
        krugerSelectInfo.setBladeType(0);//待定
        krugerSelectInfo.setFanGroup(0);//待定
        krugerSelectInfo.setFanArrangement(0);//待定
        krugerSelectInfo.setDriveType(0);//待定
        krugerSelectInfo.setPhase(0);//待定
        krugerSelectInfo.setPlenumChamberDesign(0);//待定
        krugerSelectInfo.setPlenumOutletDesign(0);//待定
        krugerSelectInfo.setVolts(0);//待定
        krugerSelectInfo.setPlenumOutletDiameter(0);//待定
        krugerSelectInfo.setPlenumChamberHeight(0);//待定
        krugerSelectInfo.setPlenumChamberWidth(0);//待定
        krugerSelectInfo.setPlenumChamberLength(0);//待定
        krugerSelectInfo.setPlenumOutletType(0);//待定
        krugerSelectInfo.setPlenumOutletWidth(0);//待定
        krugerSelectInfo.setPlenumOutletHeight(0);//待定
        krugerSelectInfo.setSpeed(0);//待定
        krugerSelectInfo.setInverter(0);//待定
        krugerSelectInfo.setDebugSize(0);//待定
        krugerSelectInfo.setDebugType(null);//待定
        return krugerSelectInfo;
    }


}
