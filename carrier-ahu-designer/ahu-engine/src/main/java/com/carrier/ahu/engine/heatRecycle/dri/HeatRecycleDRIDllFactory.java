package com.carrier.ahu.engine.heatRecycle.dri;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatRecycle.util.CalBulbTUtil;
import com.carrier.ahu.engine.heatrecycle.MS200Bean;
import com.carrier.ahu.engine.heatrecycle.MS200Lib;
import com.carrier.ahu.engine.heatrecycle.MS270Bean;
import com.carrier.ahu.engine.heatrecycle.MS270Lib;
import com.carrier.ahu.engine.heatrecycle.SG200Bean;
import com.carrier.ahu.engine.heatrecycle.SG200Lib;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleDRIDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleDRIDllFactory.class.getName());
    private static final HeatRecycleDRIResultFactory HEAT_RECYCLE_DRI_RESULT_FACTORY = new HeatRecycleDRIResultFactory();

    public List<HeatRecycleInfo> CALMS200ALL(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if (heatRecycleParam.isEnableWinter()) {
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = CALMS200(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("MS200 Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = CALMS200(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("MS200 Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    public List<HeatRecycleInfo> CALMS270All(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if (heatRecycleParam.isEnableWinter()) {
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = CALMS270(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("MS270 Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = CALMS270(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("MS270 Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    public List<HeatRecycleInfo> CALSG200All(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if (heatRecycleParam.isEnableWinter()) {
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = CALSG200(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("SG200 Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = CALSG200(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("SG200 Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }


    //分子筛 筛选MS200
    public HeatRecycleInfo CALMS200(HeatRecycleParam heatRecycleParam) throws Exception {
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double supplyair = heatRecycleParam.getNAVolume();//新风风量
//        double returnAir = heatRecycleParam.getRAVolume() * 3600;//回风风量
        double returnAir = heatRecycleParam.getRAVolume();//回风风量
        double inputOAS_DBT = heatRecycleParam.getSNewDryBulbT();//夏季新风干球温度
        double inputOAS_WBT = heatRecycleParam.getSNewWetBulbT();//夏季新风湿球温度
        double inputOAS_RH = heatRecycleParam.getSNewRelativeT();//夏季新风相对湿度
        double inputRaS_DBT = heatRecycleParam.getSInDryBulbT();//夏季回风干球温度
        double inputRaS_WBT = heatRecycleParam.getSInWetBulbT();//夏季回风湿球温度
        double inputRaS_RH = heatRecycleParam.getSInRelativeT();//夏季回风相对湿度
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;
        if (heatRecycleParam.isEnableWinter()) {
            inputOAS_DBT = heatRecycleParam.getWNewDryBulbT();//冬季新风干球温度
            inputOAS_WBT = heatRecycleParam.getWNewWetBulbT();//冬季新风湿球温度
            inputOAS_RH = heatRecycleParam.getWNewRelativeT();//冬季新风相对湿度
            inputRaS_DBT = heatRecycleParam.getWInDryBulbT();//冬季回风干球温度
            inputRaS_WBT = heatRecycleParam.getWInWetBulbT();//冬季回风湿球温度
            inputRaS_RH = heatRecycleParam.getWInRelativeT();//冬季回风相对湿度
        }
        boolean DRIunitSI = true;
        double DRIinputSupplyAir = heatRecycleParam.getNAVolume();//新风
        if (supplyair - 3780 < 0.001) {
            DRIinputSupplyAir = 3790;
        } else {
            DRIinputSupplyAir = supplyair;
        }
        double DRIinputReturnAir = heatRecycleParam.getRAVolume();//回风
        if (returnAir - 3780 < 0.001) {
            DRIinputReturnAir = 3790;
        } else {
            DRIinputReturnAir = returnAir;
        }
        double DRIinputOAS_DBT = inputOAS_DBT;
        double DRIinputOAS_WBT = inputOAS_WBT;
        double DRIinputRaS_DBT = inputRaS_DBT;
        double DRIinputRaS_RH = inputRaS_RH;
        boolean DRIwheelSensible = true;
        MS200Bean ms200Bean = MS200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
        String ECOFRESHModel = ms200Bean.getDRIOutECOFRESHModel();//引擎
        double saEfficiencySummerTotal = ms200Bean.getDRIoutSaEfficiencySummerTotal();//引擎
        double SaEfficiencyWinterTotal = ms200Bean.getDRIoutSaEfficiencyWinterTotal();//引擎
        double outSaPDropWinter = ms200Bean.getDRIoutSaPDropWinter();//引擎
        double calwheeldia;
        if (ECOFRESHModel.length() == 13) {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        String efficiencyType = EngineConstant.SYS_STRING_NUMBER_1;//TODO 需要确认
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, efficiencyType);
        double maxwheeldia;
        double wheeltype;
        //if ("AHU39CBF".equals(partWWheelPo.getProduct())) {
        //    maxwheeldia = partWWheelPo.getWheelDiaByAir39CQ();
        //    wheeltype = partWWheelPo.getWheelTypeByAir39CQ();
        //} else {
        maxwheeldia = partWWheelPo.getWheelDiaByAir39G();
        wheeltype = partWWheelPo.getWheelTypeByAir39G();
        //}
        double velface;
        if (calwheeldia > maxwheeldia) {
            velface = SystemCountUtil.velfacecal(returnAir, maxwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        } else {
            velface = SystemCountUtil.velfacecal(returnAir, calwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        }
        if (velface > 5.5) {
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        if (calwheeldia > maxwheeldia) {
            double DRIinputModel = wheeltype;
            ms200Bean = MS200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, wheeltype);
            saEfficiencySummerTotal = ms200Bean.getDRIoutSaEfficiencySummerTotal();//引擎
            SaEfficiencyWinterTotal = ms200Bean.getDRIoutSaEfficiencyWinterTotal();//引擎
            outSaPDropWinter = ms200Bean.getDRIoutSaPDropWinter();//引擎
        }
        double ecofreshmodel;
        if (ECOFRESHModel.length() == 13) {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        HeatRecycleInfo heatRecycleInfo = null;
        if (heatRecycleParam.isEnableWinter()) {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, SaEfficiencyWinterTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, true);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, SaEfficiencyWinterTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, SaEfficiencyWinterTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, SaEfficiencyWinterTotal, qt, qs);
        } else {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, saEfficiencySummerTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, saEfficiencySummerTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, saEfficiencySummerTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, saEfficiencySummerTotal, qt, qs);
        }
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            return heatRecycleInfo;
        }
        return null;
    }

    //MS270
    public HeatRecycleInfo CALMS270(HeatRecycleParam heatRecycleParam) throws Exception {
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double supplyair = heatRecycleParam.getNAVolume();//新风风量
//        double returnAir = heatRecycleParam.getRAVolume() * 3600;//回风风量
        double returnAir = heatRecycleParam.getRAVolume();//回风风量
        double inputOAS_DBT = heatRecycleParam.getSNewDryBulbT();//夏季新风干球温度
        double inputOAS_WBT = heatRecycleParam.getSNewWetBulbT();//夏季新风湿球温度
        double inputOAS_RH = heatRecycleParam.getSNewRelativeT();//夏季新风相对湿度
        double inputRaS_DBT = heatRecycleParam.getSInDryBulbT();//夏季回风干球温度
        double inputRaS_WBT = heatRecycleParam.getSInWetBulbT();//夏季回风湿球温度
        double inputRaS_RH = heatRecycleParam.getSInRelativeT();//夏季回风相对湿度
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;
        if (heatRecycleParam.isEnableWinter()) {
            inputOAS_DBT = heatRecycleParam.getWNewDryBulbT();//冬季新风干球温度
            inputOAS_WBT = heatRecycleParam.getWNewWetBulbT();//冬季新风湿球温度
            inputOAS_RH = heatRecycleParam.getWNewRelativeT();//冬季新风相对湿度
            inputRaS_DBT = heatRecycleParam.getWInDryBulbT();//冬季回风干球温度
            inputRaS_WBT = heatRecycleParam.getWInWetBulbT();//冬季回风湿球温度
            inputRaS_RH = heatRecycleParam.getWInRelativeT();//冬季回风相对湿度
        }
        boolean DRIunitSI = true;
        double DRIinputSupplyAir = supplyair;//新风
        double DRIinputReturnAir = returnAir;//回风
        double DRIinputOAS_DBT = inputOAS_DBT;
        double DRIinputOAS_WBT = inputOAS_WBT;
        double DRIinputRaS_DBT = inputRaS_DBT;
        double DRIinputRaS_RH = inputRaS_RH;
        boolean DRIwheelSensible = true;
        MS270Bean ms270Bean = MS270Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
        String ECOFRESHModel = ms270Bean.getDRIOutECOFRESHModel();//引擎
        double saEfficiencySummerTotal = ms270Bean.getDRIoutSaEfficiencySummerTotal();//引擎
        double SaEfficiencyWinterTotal = ms270Bean.getDRIoutSaEfficiencyWinterTotal();//引擎
        double outSaPDropWinter = ms270Bean.getDRIoutSaPDropWinter();//引擎
        double calwheeldia;
        if (ECOFRESHModel.length() == 13) {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        String efficiencyType = EngineConstant.SYS_STRING_NUMBER_1;//TODO 需要确认
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, efficiencyType);
        double maxwheeldia;
        double wheeltype;
        //if ("AHU39CBF".equals(partWWheelPo.getProduct())) {
        //    maxwheeldia = partWWheelPo.getWheelDiaByAir39CQ();
        //    wheeltype = partWWheelPo.getWheelTypeByAir39CQ();
        //} else {
        maxwheeldia = partWWheelPo.getWheelDiaByAir39G();
        wheeltype = partWWheelPo.getWheelTypeByAir39G();
        //}

        double velface;
        if (calwheeldia > maxwheeldia) {
            velface = SystemCountUtil.velfacecal(returnAir, maxwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        } else {
            velface = SystemCountUtil.velfacecal(returnAir, calwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        }
        if (velface > 5.5) {
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        if (calwheeldia > maxwheeldia) {
            double DRIinputModel = wheeltype;
            ms270Bean = MS270Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, wheeltype);
            saEfficiencySummerTotal = ms270Bean.getDRIoutSaEfficiencySummerTotal();//引擎
            SaEfficiencyWinterTotal = ms270Bean.getDRIoutSaEfficiencyWinterTotal();//引擎
            outSaPDropWinter = ms270Bean.getDRIoutSaPDropWinter();//引擎
        }
        double ecofreshmodel;
        if (ECOFRESHModel.length() == 13) {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        HeatRecycleInfo heatRecycleInfo = null;
        if (heatRecycleParam.isEnableWinter()) {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, SaEfficiencyWinterTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, true);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, SaEfficiencyWinterTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, SaEfficiencyWinterTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, SaEfficiencyWinterTotal, qt, qs);
        } else {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, saEfficiencySummerTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, saEfficiencySummerTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, saEfficiencySummerTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, saEfficiencySummerTotal, qt, qs);
        }
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            return heatRecycleInfo;
        }
        return null;
    }

    //SG200
    public HeatRecycleInfo CALSG200(HeatRecycleParam heatRecycleParam) throws Exception {
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double supplyair = heatRecycleParam.getNAVolume();//新风风量
//        double returnAir = heatRecycleParam.getRAVolume() * 3600;//回风风量
        double returnAir = heatRecycleParam.getRAVolume();//回风风量
        double inputOAS_DBT = heatRecycleParam.getSNewDryBulbT();//夏季新风干球温度
        double inputOAS_WBT = heatRecycleParam.getSNewWetBulbT();//夏季新风湿球温度
        double inputOAS_RH = heatRecycleParam.getSNewRelativeT();//夏季新风相对湿度
        double inputRaS_DBT = heatRecycleParam.getSInDryBulbT();//夏季回风干球温度
        double inputRaS_WBT = heatRecycleParam.getSInWetBulbT();//夏季回风湿球温度
        double inputRaS_RH = heatRecycleParam.getSInRelativeT();//夏季回风相对湿度
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;
        if (heatRecycleParam.isEnableWinter()) {
            inputOAS_DBT = heatRecycleParam.getWNewDryBulbT();//冬季新风干球温度
            inputOAS_WBT = heatRecycleParam.getWNewWetBulbT();//冬季新风湿球温度
            inputOAS_RH = heatRecycleParam.getWNewRelativeT();//冬季新风相对湿度
            inputRaS_DBT = heatRecycleParam.getWInDryBulbT();//冬季回风干球温度
            inputRaS_WBT = heatRecycleParam.getWInWetBulbT();//冬季回风湿球温度
            inputRaS_RH = heatRecycleParam.getWInRelativeT();//冬季回风相对湿度
        }
        boolean DRIunitSI = true;
        double DRIinputSupplyAir = supplyair;//新风
        double DRIinputReturnAir = returnAir;//回风
        double DRIinputOAS_DBT = inputOAS_DBT;
        double DRIinputOAS_WBT = inputOAS_WBT;
        double DRIinputRaS_DBT = inputRaS_DBT;
        double DRIinputRaS_RH = inputRaS_RH;
        boolean DRIwheelSensible = true;
        String ECOFRESHModel = "";//引擎
        //引擎计算
        SG200Bean sg200Bean = SG200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
        ECOFRESHModel = sg200Bean.getDRIOutECOFRESHModel();
        double saEfficiencySummerTotal = sg200Bean.getDRIoutSaEfficiencySummerTotal();//引擎
        double SaEfficiencyWinterTotal = sg200Bean.getDRIoutSaEfficiencyWinterTotal();//引擎
        double outSaPDropWinter = sg200Bean.getDRIoutSaPDropWinter();//引擎
        double calwheeldia;
        if (ECOFRESHModel.length() == 13) {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        String efficiencyType = EngineConstant.SYS_STRING_NUMBER_1;//TODO 需要确认
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, efficiencyType);
        double maxwheeldia;
        double wheeltype;
        //if ("AHU39CBF".equals(partWWheelPo.getProduct())) {
        //    maxwheeldia = partWWheelPo.getWheelDiaByAir39CQ();
        //    wheeltype = partWWheelPo.getWheelTypeByAir39CQ();
        //} else {
        maxwheeldia = partWWheelPo.getWheelDiaByAir39G();
        wheeltype = partWWheelPo.getWheelTypeByAir39G();
        //}
        double velface;
        if (calwheeldia > maxwheeldia) {
            velface = SystemCountUtil.velfacecal(returnAir, maxwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        } else {
            velface = SystemCountUtil.velfacecal(returnAir, calwheeldia, EngineConstant.SYS_STRING_NUMBER_1);
        }
        if (velface > 5.5) {
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        if (calwheeldia > maxwheeldia) {
            double DRIinputModel = wheeltype;
            sg200Bean = SG200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT, DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, DRIinputModel);
        }
        double ecofreshmodel;
        if (ECOFRESHModel.length() == 13) {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            ecofreshmodel = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }

        HeatRecycleInfo heatRecycleInfo = null;
        if (heatRecycleParam.isEnableWinter()) {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, SaEfficiencyWinterTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, true);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, SaEfficiencyWinterTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, SaEfficiencyWinterTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, SaEfficiencyWinterTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, SaEfficiencyWinterTotal, qt, qs);
        } else {
            //计算排风输出温度
            double pParamT = CalBulbTUtil.calPParamT(inputOAS_DBT, inputRaS_DBT, saEfficiencySummerTotal);
            double pParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double pParamF = CalBulbTUtil.calPParamF(pParamT, pParamD);
            AirConditionBean pAirConditionBean = AirConditionUtils.FAirParmCalculate3(pParamT, pParamF);

            //计算送风风输出温度
            double sParamT = CalBulbTUtil.calPParamT(inputRaS_DBT, inputOAS_DBT, saEfficiencySummerTotal);//送风计算和排风计算相反
            double sParamD = CalBulbTUtil.calPParamD(inputOAS_DBT, inputOAS_WBT, inputRaS_DBT, inputRaS_WBT, saEfficiencySummerTotal, false);
            double sParamF = CalBulbTUtil.calPParamF(sParamT, sParamD);
            AirConditionBean sAirConditionBean = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF);

            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(inputOAS_DBT, inputOAS_RH);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sParamT, sParamF); //送风干球温度 送风相对湿度
            double qt = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = DRIinputSupplyAir / 3600 * 1.27 * 273 / (273 + inputOAS_DBT) * (inputOAS_DBT - sParamT);

            heatRecycleInfo = HEAT_RECYCLE_DRI_RESULT_FACTORY.packageHeatRecycleInfo(BaseDataUtil.doubleConversionInteger(ecofreshmodel), season, sParamT, sAirConditionBean.getParmTb(), sParamF, outSaPDropWinter, saEfficiencySummerTotal,
                    pParamT, pAirConditionBean.getParmTb(), pParamF, outSaPDropWinter, saEfficiencySummerTotal, qt, qs);
        }
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            return heatRecycleInfo;
        }
        return null;
    }
}
