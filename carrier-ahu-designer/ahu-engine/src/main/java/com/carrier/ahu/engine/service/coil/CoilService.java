package com.carrier.ahu.engine.service.coil;

import java.util.List;

import org.springframework.stereotype.Service;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;

/**
 * Created by liangd4 on 2017/9/8.
 */
@Service
public interface CoilService {

    //CoilInParam：入参
    //coilType：盘管类型 dw 热水盘管 cw 冷水盘管
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    List<CoilInfo> getCoil(CoilInParam coilInParam, PublicRuleParam publicRuleParam, String coilType, String version);

    double getFFrm4fVaporPres(double parmTin, double parmTout, double parmAirFlow, String serial, String parmMFlag);

}
