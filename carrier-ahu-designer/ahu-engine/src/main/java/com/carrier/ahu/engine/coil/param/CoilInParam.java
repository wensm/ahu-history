package com.carrier.ahu.engine.coil.param;

import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CoilInParam extends UserConfigParam {
    /*机组参数*/
    private String serial;//机组型号
    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向

    /*盘管选项*/
    private String coilType;//盘管类型 D:冷水盘管、DX:直接蒸发式
    private String tubeDiameter;//管径
    private String rows;//排数
    private String circuit;//回路
    private String finDensity;//片距fpi
    private String finType;//翅片材质
    private String season;//季节 冬季:W、夏季:S 全选：All 页面为勾选框入参需要转换
    private double InDryBulbT;//进风干球温度
    private double InWetBulbT;//进风湿球温度
    private double InRelativeT;//进风相对湿度
    private int coolant;//介质
    private int concentration;//浓度
    private String calculationConditions;//水温升/水流量

    private String qualifiedConditions;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
    private double ScoldQ;//冷量(夏)
    private double SsensibleCapacity;//冷量(夏)
    private double WheatQ;//热量(冬)

    private double leavingAirDB;//出风温度

    /*凝露效核*/
    private double AmbientDryBulbT;//环境干球温度
    private double AmbientWetBulbT;//环境湿球温度
    private double AmbientRelativeT;//环境相对湿度

    private int WbalancedMethod;//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
    private double SbalanceMethodValue;//夏 限定条件值
    private String SbalanceMethodUnit;//夏 限定计量单位 暂时无使用
    private double WbalancedMethodValue;//冬 限定条件值
    private String WbalancedMethodUnit;//冬 限定计量单位 暂时无使用
    private String count;//计算条件 水温升、水流量 暂时无使用
    private String countValue;//计算条件对应值 暂时无使用
    private double WTAScend;//水温升值
    private double waterFlow;//水流量值
    private boolean calculateAllResults;//计算所有
    private boolean calculateOptimalResults;//计算最优结果
    private int language;//语言 0:中文 1：英文
    private double altitude;//海拔高度
    private double enteringFluidTemperature;//进水温度
    private double maxWPD;//最大水阻

    /*直接蒸发式盘管增加属性*/
    private String Cond;//冷凝器进风温度
    private String SatCond;//饱和冷凝温度
    private String SubClg;//过冷度
    private String ACCPressure;//饱和冷凝压力
    private String SuperHeat;//过热度
    private String CDU;//unit


    /*冷水、热水、直接蒸发*/
    private boolean EnableSummer;//夏季生效
    private boolean EnableWinter;//冬季生效


}
