package com.carrier.ahu.engine.service.fan;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.FanFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/8.
 */
@Service
public class FanServiceImpl implements FanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanServiceImpl.class.getName());
    private static final FanFactory fanFactory = new FanFactory();

    @Override
    public List<FanInfo> getFanEngine(FanInParam fanInParam, String version) throws FanEngineException {
        LOGGER.info("================== FanServiceImpl FanEngine begin ==================" + DateUtil.getStringDate());
        LOGGER.info("FanServiceImpl calculator param :" + JSONArray.toJSONString(fanInParam));
        if (fanInParam.getTotalStatic() == 0) {//风机总静压默认值为300
            fanInParam.setTotalStatic(300);
        }
        List<FanInfo> fanInfoList = fanFactory.engineCal(fanInParam, version);

        ListUtils.sort(fanInfoList, true, EngineConstant.JSON_FAN_SERIES, EngineConstant.JSON_FAN_MODEL, EngineConstant.SYS_ALPHABET_K);
        LOGGER.info("================== FanServiceImpl FanEngine end ==================" + DateUtil.getStringDate());
        return fanInfoList;
    }

    @Override
    public FanInfo getFanEngineByRule(FanInParam fanInParam, PublicRuleParam publicRuleParam, String version) throws FanEngineException {
        LOGGER.info("FanServiceImpl calculator param :" + JSONArray.toJSONString(fanInParam));
        if (fanInParam.getTotalStatic() == 0) {//风机总静压默认值为300
            fanInParam.setTotalStatic(300);
        }
        UserConfigParam userConfigParam = publicRuleParam.getUserConfigParam();
        if(!EmptyUtil.isEmpty(userConfigParam)){
            Map<String, Object> mapConfig = userConfigParam.getMapConfig();
            if(!EmptyUtil.isEmpty(mapConfig)){
                fanInParam.setMapConfig(mapConfig);
            }
        }
        List<FanInfo> fanInfoList = fanFactory.engineCal(fanInParam, version);
        if (!EmptyUtil.isEmpty(fanInfoList)) {
            if (null != publicRuleParam) {//如果规则不为空
                if (publicRuleParam.getKey().equals(CalcTypeEnum.OPTIMUM.getId())) {//性能最优
                    ListUtils.sort(fanInfoList, true, EngineConstant.JSON_FAN_EFFICIENCY);
                    return fanInfoList.get(fanInfoList.size() - 1);
                } else if(publicRuleParam.getKey().equals(CalcTypeEnum.ECONOMICAL.getId())){//最经济
                    ListUtils.sort(fanInfoList, true, EngineConstant.JSON_FAN_SERIES, EngineConstant.JSON_FAN_MODEL, EngineConstant.SYS_ALPHABET_K);
                    return fanInfoList.get(0);
                }else{
                    return fanInfoList.get(0);
                }
            } else {
                return fanInfoList.get(0);
            }
        } else {
            return null;
        }
    }
}
