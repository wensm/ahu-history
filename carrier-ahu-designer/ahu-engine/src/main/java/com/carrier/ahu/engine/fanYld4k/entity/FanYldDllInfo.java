package com.carrier.ahu.engine.fanYld4k.entity;

import lombok.Data;

//风机调用DLL结果
@Data
public class FanYldDllInfo {

    private Integer tCount;//fanSelectionEx3 返回结果数量 调用风机DLL接口返回值
    private byte[] bData;//GetLargeResultData 调用风机DLL接口返回值
    private double[] pDoubleData;//GetLargeResultData 调用风机DLL接口返回值
    private double[] pDoubleDataSYW;//GetLargeResultData 调用风机DLL接口返回值

    //计算使用查参数
    private String fanType;//机组型号 39CQ0608
    private String fanSupplier;//供应商 A：亿利达 + 谷科
    private String motorType;//页面选择值 电机类型 A：三级能效电机 B：防爆电机 C：双速电机 D：变频电机 E：二级能效电机
    private String curve;//图片路径
    private String type;//电机类型 1:二级能效电机、2:三级能效电机、3:防爆电机、4:双速电机、5:变频电机、6:变频电机(直连)
    private String supplier;//电机品牌 default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
}
