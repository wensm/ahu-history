package com.carrier.ahu.engine.coilWater4d.factory;

import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_FINDENSITY_AUTO;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_FINTYPE_COPPER;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_ROWS_AUTO;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_SCALCULATIONCONDITIONS_1;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_1_2;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_3_8;
import static com.carrier.ahu.vo.SystemCalculateConstants.DXCoil;
import static com.carrier.ahu.vo.SystemCalculateConstants.waterCoil;
import static com.carrier.ahu.vo.SystemCalculateConstants.waterCoilH;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.engine.coilWater4d.util.CoilPackageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilWater4d.count.CoilCoff;
import com.carrier.ahu.engine.coilWater4d.param.CWDCPCParam;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.SCalSeq;
import com.carrier.ahu.metadata.entity.coil.SCoilCal;
import com.carrier.ahu.metadata.entity.coil.SCoilCal1;
import com.carrier.ahu.metadata.entity.coil.SCoilCalEuro;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo1;
import com.carrier.ahu.metadata.entity.coil.SSurfaceGeometrics;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.CoilMetaUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

@Component
public class CoilWaterDllInParamFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoilWaterDllInParamFactory.class.getName());
    private static final String FFOULING = "F_fouling";
    private static final String CURVENUM = "CurveNum";
    private static final String TUBENUM = "TubeNum";
    private static final String FTHICK = "Fthick";
    private static final String FCOND = "Fcond";
    private static final String TCOND = "Tcond";
    private static final String RADMULT = "RADMULT";
    private static final String DPDMULT = "DPDMULT";
    private static final String RAWMULT = "RAWMULT";
    private static final String DPWMULT = "DPWMULT";
    private static final String PAIRIN = "P_air_in";
    private static final String SURFEFF = "Surf_eff";
    private static final boolean ahri = true;//欧标标识

    @SuppressWarnings("unused")
    public List<CoilDllInParam> packageParamHTC(CoilInParam coilInParam) {//盘管根据片距、回路、排数纬度分别封装调用DLL的参数
        coilInParam.setConcentration(coilInParam.getConcentration());
        LOGGER.info("CoilInParam param " + JSONArray.toJSONString(coilInParam));
        int tubeDiameter = 1;
        if (COOLINGCOIL_TUBEDIAMETER_3_8.equals(coilInParam.getTubeDiameter())) {
            tubeDiameter = 2;
        }
        int fpi = -1;
        if (!COOLINGCOIL_FINDENSITY_AUTO.equals(coilInParam.getFinDensity())) {
            fpi = BaseDataUtil.stringConversionInteger(coilInParam.getFinDensity());
        }
        int rows = -1;
        if (!COOLINGCOIL_ROWS_AUTO.equals(coilInParam.getRows())) {
            rows = BaseDataUtil.stringConversionInteger(coilInParam.getRows());
        }
        String unitNumber = coilInParam.getSerial().substring(coilInParam.getSerial().length() - 4, coilInParam.getSerial().length());
        List<CoilDllInParam> coldWaterOutParamList = new ArrayList<>();
        if (coilInParam.getCoolant() != 1) {//介质不是水
            coilInParam.setConcentration(Integer.valueOf(coilInParam.getConcentration()) / 100);//重新计算密度
        }
        int fintype = 1;//重新封装fintype值
        if (COOLINGCOIL_FINTYPE_COPPER.equals(coilInParam.getFinType())) {//翅片材质 铜翅片
            fintype = 2;
        }
        List<SCalSeq> sCalSeqPoList = CoilMetaUtils.getsCalSeqPoListByCFRU(coilInParam.getCircuit(), fpi, rows, coilInParam.getSerial(), coilInParam.getCoilType(), coilInParam.isCalculateOptimalResults());
        if (null == sCalSeqPoList) {
            LOGGER.error("coilWaterDllInParamFactory param error :" + JSONArray.toJSONString(coilInParam));
        } else {
            for (SCalSeq sCalSeqPo : sCalSeqPoList) {
                CoilDllInParam coldWaterOutParam = new CoilDllInParam();
                if (DXCoil.equals(coilInParam.getCoilType())) {//盘管类型：直接蒸发式
                    coldWaterOutParam = packageOutParamByDXCoilCal(sCalSeqPo, coilInParam, fintype, tubeDiameter);
                } else {//盘管类型：冷水盘管 热水盘管
                    coldWaterOutParam = packageOutParamByCoilCal(sCalSeqPo, coilInParam, fintype, tubeDiameter);
                }
                coldWaterOutParamList.add(coldWaterOutParam);
            }
        }
        return coldWaterOutParamList;
    }

    //计算迎风面积 serial: 机组型号
    private double getArea(String serial, int tubeDiameter, String coilType) {
        SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
        double area = 0.0;
        if (waterCoil.equals(coilType) || DXCoil.equals(coilType)) {
            if (tubeDiameter == 1) {//四分管
                area = sectionArea.getD();
            } else {
                area = sectionArea.getD1();
            }
        } else if (waterCoilH.equals(coilType)) {
            if (tubeDiameter == 1) {//四分管
                area = sectionArea.getE();
            } else {
                area = sectionArea.getE1();
            }
        }
        return area;
    }

    //计算面风速 airVolume:风量 area：迎风面积
    private double getVelocity(double airVolume, double area) {
        return airVolume / 3600.0 / area;
    }

    //盘管类型：直接蒸发式
    private CoilDllInParam packageOutParamByDXCoilCal(SCalSeq sCalSeqPo, CoilInParam coilInParam, int fintype, int tubeDiameter) {
        //=================根据 管径、回路、排数封装DLL调用参数=======================
        CoilDllInParam coldWaterOutParam = new CoilDllInParam();
        List<CWDCPCParam> cPCParamList = new ArrayList<CWDCPCParam>();
        int row = sCalSeqPo.getRow();
        int fpi = sCalSeqPo.getFpi();
        String cir = sCalSeqPo.getCircuit();
        List<SCoilCal> sCoilCalPoList = AhuMetadata.findList(SCoilCal.class, String.valueOf(fintype));
        this.packageOutParamBySCoilCalPoList(coldWaterOutParam, sCoilCalPoList);//根据SCoilCal表封装coldWaterOutParam
        List<SSurfaceGeometrics> sSurfaceGeometricsPoList = AhuMetadata.findList(SSurfaceGeometrics.class, String.valueOf(10));
        this.packageOutParamBySSurfaceGeometricsPoList(coldWaterOutParam, sSurfaceGeometricsPoList);//根据sSurfaceGeometrics表封装coldWaterOutParam
        this.packageOutParamByInParam(coilInParam, coldWaterOutParam, fpi, cir, row, tubeDiameter);//根据InParam表封装coldWaterOutParam
        //封装调用DLL路径参数
        List<SCoilInfo> sCoilinfoPoList = AhuMetadata.findList(SCoilInfo.class, coilInParam.getSerial(), String.valueOf(row), cir);
        List<CWDCPCParam> fourCPCParamList = this.packageCPCByFourTubeDiameter(sCoilinfoPoList, fpi, tubeDiameter, row, cir, coilInParam.getWaterFlow());//封装多个CPC路径参数
        if (null != fourCPCParamList) {
            cPCParamList.addAll(fourCPCParamList);
        }
        coldWaterOutParam.setCPCParamList(cPCParamList);
        return coldWaterOutParam;
    }

    //盘管类型：冷水盘管
    private CoilDllInParam packageOutParamByCoilCal(SCalSeq sCalSeqPo, CoilInParam coilInParam, int fintype, int tubeDiameter) {
        CoilDllInParam coilDllInParam = new CoilDllInParam();
        List<CWDCPCParam> cPCParamList = new ArrayList<CWDCPCParam>();
        int row = sCalSeqPo.getRow();
        int fpi = sCalSeqPo.getFpi();
        String cir = sCalSeqPo.getCircuit();
        //=================根据 管径、回路、排数封装DLL调用参数=======================
        List<SSurfaceGeometrics> sSurfaceGeometricsPoList = null;
        //根据数据库查询结果
        if (COOLINGCOIL_TUBEDIAMETER_1_2.equals(coilInParam.getTubeDiameter())) {//四分管
            if (ahri) {//欧标 --- 旧软件中为false  如果改成false 与旧软件引擎冷量计算相同
                List<SCoilCalEuro> sCoilCalEuroPoList = AhuMetadata.findList(SCoilCalEuro.class, String.valueOf(fintype));//封装sCoilCalEuro表相关字段
                this.packageOutParamBySCoilCalEuroPoList(coilDllInParam, sCoilCalEuroPoList);//根据SCoilCalEuro表封装coilDllInParam
            } else {//非欧标
                List<SCoilCal> sCoilCalPoList = AhuMetadata.findList(SCoilCal.class, String.valueOf(fintype));
                this.packageOutParamBySCoilCalPoList(coilDllInParam, sCoilCalPoList);//根据SCoilCal表封装coilDllInParam
            }
            sSurfaceGeometricsPoList = AhuMetadata.findList(SSurfaceGeometrics.class, String.valueOf(10));
            List<SCoilInfo> sCoilinfoPoList = AhuMetadata.findList(SCoilInfo.class, coilInParam.getSerial(), String.valueOf(row), cir);//封装调用DLL路径参数
            List<CWDCPCParam> fourCPCParamList = this.packageCPCByFourTubeDiameter(sCoilinfoPoList, fpi, tubeDiameter, row, cir, coilInParam.getWaterFlow());//封装多个CPC路径参数
            if (null != fourCPCParamList) {
                cPCParamList.addAll(fourCPCParamList);
            }
        } else {//三分管
            List<SCoilCal1> sCoilCal1PoList = AhuMetadata.findList(SCoilCal1.class, String.valueOf(fintype));
            this.packageOutParamBySCoilCal1PoList(coilDllInParam, sCoilCal1PoList);//根据SCoilCal1表封装coilDllInParam
            sSurfaceGeometricsPoList = AhuMetadata.findList(SSurfaceGeometrics.class, String.valueOf(3));
            List<SCoilInfo1> sCoilinfo1PoList = AhuMetadata.findList(SCoilInfo1.class, coilInParam.getSerial(), String.valueOf(row), cir);
            List<CWDCPCParam> thereCPCParamList = this.packageCPCByThereTubeDiameter(sCoilinfo1PoList, fpi, tubeDiameter, row, cir, coilInParam.getWaterFlow());//封装多个CPC路径参数
            if (null != thereCPCParamList) {
                cPCParamList.addAll(thereCPCParamList);
            }
        }
        this.packageOutParamBySSurfaceGeometricsPoList(coilDllInParam, sSurfaceGeometricsPoList);//根据sSurfaceGeometrics表封装coilDllInParam
        this.packageOutParamByInParam(coilInParam, coilDllInParam, fpi, cir, row, tubeDiameter);//根据InParam表封装coilDllInParam
        coilDllInParam.setCPCParamList(cPCParamList);//添加CPC到OutParam中
        if (COOLINGCOIL_SCALCULATIONCONDITIONS_1.equals(coilInParam.getCalculationConditions())) {
            coilDllInParam.setDtWater(0.0);//根据水温升计算，清空水流量值
            coilDllInParam.setTWaterIn(coilInParam.getWTAScend());//水温升
        } else {
            coilDllInParam.setTWaterIn(0.0);//根据水流量计算，清空水温升值
            coilDllInParam.setDtWater(coilInParam.getWaterFlow());//水流量
        }
        return coilDllInParam;
    }

    //根据InParam表封装coilDllInParam
    private void packageOutParamByInParam(CoilInParam coilInParam, CoilDllInParam coilDllInParam, int fpi, String cir, int row, int tubeDiameter) {
        double area = getArea(coilInParam.getSerial(), tubeDiameter, coilInParam.getCoilType());
        double airVolume = 0.00;
        if (AirDirectionEnum.SUPPLYAIR.getCode().equals(coilInParam.getAirDirection())) {
            airVolume = coilInParam.getSairvolume();
        } else {
            airVolume = coilInParam.getEairvolume();
        }
        double velocity = getVelocity(airVolume, area);
        coilDllInParam.setFaceArea(area);//迎风面积
        coilDllInParam.setVFace(BaseDataUtil.decimalConvert(velocity, 2));//面风速
        coilDllInParam.setTdbAirIn(coilInParam.getInDryBulbT());//进风干球温度
        coilDllInParam.setTwbAirIn(coilInParam.getInWetBulbT());//进风湿球温度
        coilDllInParam.setRtAirIn(coilInParam.getInRelativeT());//进风相对湿度
        coilDllInParam.setTWaterIn(coilInParam.getEnteringFluidTemperature());//水侧进水温度
        coilDllInParam.setDtWater(coilInParam.getWTAScend());//水温升
        coilDllInParam.setFluidId(BaseDataUtil.integerConversionLong(coilInParam.getCoolant()));//介质
        coilDllInParam.setFluidConcentration(BaseDataUtil.integerConversionDouble(coilInParam.getConcentration()));//浓度
        coilDllInParam.setHeight(coilInParam.getAltitude());//海拔高度
        coilDllInParam.setFPI(BaseDataUtil.integerConversionDouble(fpi));//片距
        coilDllInParam.setCIR(cir);//回路
        coilDllInParam.setEwt(coilInParam.getEnteringFluidTemperature());//进水温度
        coilDllInParam.setUnitType(coilInParam.getSerial());//机组型号
        //盘管系数
        coilDllInParam.setSurfEff(CoilCoff.coilCoff(tubeDiameter, coilInParam.getSeason(), coilInParam.getSerial(), coilInParam.getInDryBulbT(), coilInParam.getInWetBulbT(), row, ahri) * coilDllInParam.getSurfEff());
        coilDllInParam.setSurfEff(coilDllInParam.getSurfEff()*CoilPackageUtil.packageSurfEff(coilInParam));
        coilDllInParam.setCoilType(coilInParam.getCoilType());
        coilDllInParam.setTubeDiameter(tubeDiameter);
        coilDllInParam.setSeason(coilInParam.getSeason());
        coilDllInParam.setCalculationConditions(coilInParam.getCalculationConditions());//水温升、水流量标识
        if (SystemCalculateConstants.DXCoil.equals(coilInParam.getCoilType())) {
            /*直接蒸发式增加以下参数*/
            String satCond = coilInParam.getSatCond();//饱和冷凝温度
            String superHeat = coilInParam.getSuperHeat();//过热度
            String subClg = coilInParam.getSubClg();//过冷度
            coilDllInParam.setCond(null == coilInParam.getCond() ? EngineConstant.SYS_BLANK : coilInParam.getCond());
            coilDllInParam.setSatCond(null == satCond ? EngineConstant.SYS_BLANK : satCond);
            coilDllInParam.setCDU(null == coilInParam.getCDU() ? EngineConstant.SYS_BLANK : coilInParam.getCDU());
            coilDllInParam.setSst(4.0);//系数固定
            coilDllInParam.setSct(BaseDataUtil.stringConversionDouble(satCond));//饱和冷凝温度
            coilDllInParam.setDtsh(BaseDataUtil.stringConversionDouble(superHeat));//过热度
            coilDllInParam.setTliq(BaseDataUtil.stringConversionDouble(satCond) - BaseDataUtil.stringConversionDouble(subClg));////饱和冷凝温度-过冷度
            coilDllInParam.setSubClg(subClg);//过冷度
        }
    }

    //封装多个CPC路径参数 三分管
    private List<CWDCPCParam> packageCPCByThereTubeDiameter(List<SCoilInfo1> sCoilinfo1PoList, int fpi, Integer tubeDiameter, int rows, String cir, double flowValue) {
        List<CWDCPCParam> cwdcpcParamList = new ArrayList<CWDCPCParam>();
        if (null != sCoilinfo1PoList) {
            for (SCoilInfo1 sCoilinfo1Po : sCoilinfo1PoList) {
                CWDCPCParam cwdcpcParam = new CWDCPCParam();
                cwdcpcParam.setLBTS(BaseDataUtil.integerConversionDouble(sCoilinfo1Po.getTLen()));
                cwdcpcParam.setStrCpc(EngineConstant.SYS_PATH_CPC + tubeDiameter + EngineConstant.SYS_PUNCTUATION_SLASH + rows + "R " + cir + EngineConstant.SYS_PUNCTUATION_SLASH + rows + EngineConstant.SYS_ALPHABET_R_UP + sCoilinfo1Po.getTNo() + " " + cir + ".cpc");
                cwdcpcParam.setqWaterIn(flowValue * sCoilinfo1Po.getWaterFlowRate());//重新计算水流量
                cwdcpcParam.settId(sCoilinfo1Po.getTId());
                cwdcpcParam.setCirNo(sCoilinfo1Po.getCirNo());
                cwdcpcParam.settLen(sCoilinfo1Po.getTLen());
                cwdcpcParam.settNo(sCoilinfo1Po.getTNo());
                cwdcpcParam.settRow(sCoilinfo1Po.getRow());
                cwdcpcParam.setFlowRate(sCoilinfo1Po.getWaterFlowRate());
                cwdcpcParamList.add(cwdcpcParam);
            }
        }
        return cwdcpcParamList;
    }

    //封装多个CPC路径参数 四分管
    private List<CWDCPCParam> packageCPCByFourTubeDiameter(List<SCoilInfo> sCoilinfoPoList, int fpi, int tubeDiameter, int rows, String cir, double flowValue) {
        List<CWDCPCParam> cwdcpcParamList = new ArrayList<CWDCPCParam>();
        if (null != sCoilinfoPoList) {
            for (SCoilInfo sCoilinfoPo : sCoilinfoPoList) {
                CWDCPCParam cwdcpcParam = new CWDCPCParam();
                cwdcpcParam.setLBTS(BaseDataUtil.integerConversionDouble(sCoilinfoPo.getTLen()));
				cwdcpcParam.setStrCpc(
						EngineConstant.SYS_PATH_CPC + tubeDiameter + EngineConstant.SYS_PUNCTUATION_SLASH + rows
								+ "R " + cir + EngineConstant.SYS_PUNCTUATION_SLASH + rows + EngineConstant.SYS_ALPHABET_R_UP + sCoilinfoPo.getTNo() + " " + cir + ".cpc");
                cwdcpcParam.setqWaterIn(flowValue * sCoilinfoPo.getWaterFlowRate());//重新计算水流量
                cwdcpcParam.settId(sCoilinfoPo.getTId());
                cwdcpcParam.setCirNo(sCoilinfoPo.getCirNo());
                cwdcpcParam.settLen(sCoilinfoPo.getTLen());
                cwdcpcParam.settNo(sCoilinfoPo.getTNo());
                cwdcpcParam.settRow(sCoilinfoPo.getRow());
                cwdcpcParam.setFlowRate(sCoilinfoPo.getWaterFlowRate());
                cwdcpcParamList.add(cwdcpcParam);
            }
        }
        return cwdcpcParamList;
    }

    //根据sSurfaceGeometrics表封装coldWaterOutParam
    private void packageOutParamBySSurfaceGeometricsPoList(CoilDllInParam coldWaterOutParam, List<SSurfaceGeometrics> sSurfaceGeometricsPoList) {
        if (null != sSurfaceGeometricsPoList) {
            for (SSurfaceGeometrics sSurfaceGeometricsPo : sSurfaceGeometricsPoList) {
                coldWaterOutParam.setODEXP(sSurfaceGeometricsPo.getOdExp());
                coldWaterOutParam.setPT(sSurfaceGeometricsPo.getPt());
                coldWaterOutParam.setPR(sSurfaceGeometricsPo.getPr());
                coldWaterOutParam.setTW(sSurfaceGeometricsPo.getTw());
                coldWaterOutParam.setIdBend(sSurfaceGeometricsPo.getIdBend());
                coldWaterOutParam.setIdHairpin(sSurfaceGeometricsPo.getIdHairpin());
            }
        }
    }

    //根据SCoilCal表封装coldWaterOutParam
    private void packageOutParamBySCoilCal1PoList(CoilDllInParam coldWaterOutParam, List<SCoilCal1> sCoilCal1PoList) {

        if (null != sCoilCal1PoList) {
            for (SCoilCal1 sCoilCal1Po : sCoilCal1PoList) {
                if (FFOULING.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setFFouling(sCoilCal1Po.getValues());
                } else if (CURVENUM.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setCurveNum(BaseDataUtil.doubleConversionLong(sCoilCal1Po.getValues()));
                } else if (FTHICK.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setFThick(sCoilCal1Po.getValues());
                } else if (FCOND.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setFCond(sCoilCal1Po.getValues());
                } else if (TCOND.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setTCond(sCoilCal1Po.getValues());
                } else if (RADMULT.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setRADMULT(sCoilCal1Po.getValues());
                } else if (DPDMULT.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setDPDMULT(sCoilCal1Po.getValues());
                } else if (RAWMULT.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setRAWMULT(sCoilCal1Po.getValues());
                } else if (DPWMULT.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setDPWMULT(sCoilCal1Po.getValues());
                } else if (PAIRIN.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setPAirIn(sCoilCal1Po.getValues());
                } else if (SURFEFF.equals(sCoilCal1Po.getComments())) {
                    coldWaterOutParam.setSurfEff(sCoilCal1Po.getValues());
                }
            }
        }
    }

    //根据SCoilCal表封装coldWaterOutParam
    private void packageOutParamBySCoilCalPoList(CoilDllInParam coldWaterOutParam, List<SCoilCal> sCoilCalPoList) {
        if (null != sCoilCalPoList) {
            for (SCoilCal sCoilCalPo : sCoilCalPoList) {
                if (FFOULING.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setFFouling(sCoilCalPo.getValues());
                } else if (CURVENUM.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setCurveNum(BaseDataUtil.doubleConversionLong(sCoilCalPo.getValues()));
                } else if (TUBENUM.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setTubeNum(sCoilCalPo.getValues());
                } else if (FTHICK.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setFThick(sCoilCalPo.getValues());
                } else if (FCOND.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setFCond(sCoilCalPo.getValues());
                } else if (TCOND.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setTCond(sCoilCalPo.getValues());
                } else if (RADMULT.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setRADMULT(sCoilCalPo.getValues());
                } else if (DPDMULT.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setDPDMULT(sCoilCalPo.getValues());
                } else if (RAWMULT.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setRAWMULT(sCoilCalPo.getValues());
                } else if (DPWMULT.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setDPWMULT(sCoilCalPo.getValues());
                } else if (PAIRIN.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setPAirIn(sCoilCalPo.getValues());
                } else if (SURFEFF.equals(sCoilCalPo.getComments())) {
                    coldWaterOutParam.setSurfEff(sCoilCalPo.getValues());
                }
            }
        }
    }

    //根据SCoilCalEuro表封装coldWaterOutParam
    private void packageOutParamBySCoilCalEuroPoList(CoilDllInParam coldWaterOutParam, List<SCoilCalEuro> sCoilCalEuroPoList) {
        if (null != sCoilCalEuroPoList) {
            for (SCoilCalEuro sCoilCalEuroPo : sCoilCalEuroPoList) {
                if (FFOULING.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setFFouling(sCoilCalEuroPo.getValues());
                } else if (CURVENUM.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setCurveNum(BaseDataUtil.doubleConversionLong(sCoilCalEuroPo.getValues()));
                } else if (FTHICK.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setFThick(sCoilCalEuroPo.getValues());
                } else if (FCOND.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setFCond(sCoilCalEuroPo.getValues());
                } else if (TCOND.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setTCond(sCoilCalEuroPo.getValues());
                } else if (RADMULT.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setRADMULT(sCoilCalEuroPo.getValues());
                } else if (DPDMULT.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setDPDMULT(sCoilCalEuroPo.getValues());
                } else if (RAWMULT.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setRAWMULT(sCoilCalEuroPo.getValues());
                } else if (DPWMULT.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setDPWMULT(sCoilCalEuroPo.getValues());
                } else if (PAIRIN.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setPAirIn(sCoilCalEuroPo.getValues());
                } else if (SURFEFF.equals(sCoilCalEuroPo.getComments())) {
                    coldWaterOutParam.setSurfEff(sCoilCalEuroPo.getValues());
                }
            }
        }
    }
}