package com.carrier.ahu.engine.coilWater4d.count;
/*//---水阻计算
--------------------参数相关-----------------
相关数据表：S_COILINFO、S_COILINFO_1、s_waterDropfactor_AHRI、s_waterDropfactor (水阻计算数据)

计算参数：
T_ID：T_ID Cir_No：Cir_No T_len：T_len T_no：T_No
Waterflow：水流量*表字段（WATERFLOW）、pitch：0
Watertempin：进水温度、Watertempout：出水温度
rows：排数、unittype：机组型号、coildia：管径

参数赋值规则：
(T_ID：T_ID Cir_No：Cir_No T_len：T_len T_no：T_No Waterflow：水流量*表字段（WATERFLOW）)
四分管查询：S_COILINFO、三分管查询：S_COILINFO_1 查询条件：机型、排数、回路
---------------------------------------------
函数介绍：
例如Math.pow(3,4);{81}
例如Abs(-2.3);{2.3},Abs(-157);{157}
例如Round(1.4,'rounds to',Round(1.4));{1}四舍五入
PIdouble = 3.14;*/

import java.util.List;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.SWaterDensity;
import com.carrier.ahu.metadata.entity.coil.SWaterDropFactor;
import com.carrier.ahu.metadata.entity.coil.SWaterDropFactorAhri;
import com.carrier.ahu.unit.ListUtils;

public class CalWaterDrop {

    private static final double PI = 3.14;


    //非欧标
    public static Double calWaterDrop(double Watertemp, int T_ID, int Cir_no, int T_len, int T_no, double waterflow, int Rows, String unitType, int coildia, boolean ahri) {
        double T_len1 = 0.00;
        double T_len2 = 90;
        double W_vel1 = 0.00;
        double W_vel2 = 0.00;
        double W_vel3 = 0.00;
        double T_ID2 = 0.00;
        double T_ID3 = 0.00;
        double cir_len = 0.00;
        double T_waterdensity = 0.00;
        double T_waterdropfactor = 1.0;
        double coff=0.00;
        double coff1=0.00;
        double coff2=0.00;
        double coff3=0.00;
        Watertemp=20;
        List<SWaterDensity> densityList=AhuMetadata.findAll(SWaterDensity.class);
        ListUtils.sort(densityList,true,"waterTemp");
        for(SWaterDensity entity: densityList) {
        	if(entity.getWaterTemp()>=Watertemp) {
        		T_waterdensity=entity.getWaterDensity();
        		break;
        	}
        }
        
        
        if (ahri) {
            SWaterDropFactorAhri sWaterdropfactorAhriPo = AhuMetadata.findOne(SWaterDropFactorAhri.class, unitType);
            T_waterdropfactor=sWaterdropfactorAhriPo.getFactor();
        } else {
            SWaterDropFactor sWaterdropfactorPo = AhuMetadata.findOne(SWaterDropFactor.class, unitType);
            T_waterdropfactor=sWaterdropfactorPo.getFactor();
        }
        
        if (1 == coildia) {//如果管径为四分管
            T_len1 = 31.75 * T_no + 2 * 160 - 61 * 2;
        } else {
            T_len1 = 25.4 * T_no + 2 * 160 - 61 * 2;
        }
        W_vel1 = waterflow * 4 / T_ID / T_ID / 3.1415926 * 1000 * 1000 / 3600;
        T_len2 = 90;
        if (1 == coildia) {T_ID2 = 12;}
        else {T_ID2 = 8.92;} //added by yanxi 20140820
        W_vel2 = waterflow * 4 / T_ID2 / T_ID2 / 3.1415926 * 1000 * 1000 / Cir_no / 3600;
        if (1 == coildia) { T_ID3 = 12.5;}
        else {T_ID3 = 9.52;}
        
        W_vel3 = waterflow / T_ID3 / T_ID3 * 1000 * 1000 / 3.1415926 / Cir_no / 3600 * 4;
        if (1 == coildia) { //added by yanxi 20140820
          cir_len = T_len * T_no * Rows / Cir_no + 31.75 * (T_no * Rows / Cir_no - 1) * 3.1415926 / 2;
        }else {
          cir_len = T_len * T_no * Rows / Cir_no + 25.4 * (T_no * Rows / Cir_no - 1) * 3.1415926 / 2;
        }
        coff = 0.01775 / (1 + 0.0337 * Watertemp + 0.000221 * Math.pow(Watertemp, 2)) / 100 / 100;
        coff1 = W_vel1 * T_ID / coff / 1000;
        coff1 = 0.1 * Math.pow((100 / coff1), 0.25);
        coff2 = T_ID2 * W_vel2 / coff / 1000;
        coff2 = 0.3164 / Math.pow(coff2, 0.25);
        coff3 = T_ID3 * W_vel3 / coff / 1000;
        coff3 = 0.3164 / Math.pow(coff3, 0.25);
        double header_onway = coff1 * W_vel1 * W_vel1 / 2 * T_len1 * T_waterdensity / T_ID + coff2 * W_vel2 * W_vel2 * T_waterdensity * T_len2 / T_ID2;
        double header_local = 2 * 2 / 2 * T_waterdensity * W_vel1 * W_vel1 + 5 / 2 * T_waterdensity * Math.pow((W_vel2 + W_vel1) / 2, 2);
        double coil_onway = coff3 * W_vel3 * W_vel3 / 2 * cir_len / T_ID3 * T_waterdensity;
        double coil_local = (T_no * Rows / Cir_no - 1) * 0.246 * 2 / 2 * W_vel3 * W_vel3 * T_waterdensity;
        double waterdrop = (header_onway + header_local + coil_onway + coil_local) / 1000 * T_waterdropfactor;
        return waterdrop / 1.2;
    }

    //封装计算水阻 欧标
    @SuppressWarnings("unused")
    public static Double calWaterDropOne(int T_ID, int Cir_No, int T_len, int T_no, double Waterflow, int pitch, double Watertempin, double Watertempout, int rows, String unitType, int coildia, boolean ahri) {
    	
    	double coildia1 = 0.00;
        double Dis_T_ID = 0.00;
        double COIL_T_ID = 0.00;
        double COIL_W_Vel = 0.00;
        double Cir_len = 0.00;
        double headerlocal = 0.00;
        double T_waterdropfactor = 0.00;
        double avertemp = (Watertempin + Watertempout) / 2.0;//avertemp
        double density = -0.085866 * Math.pow(avertemp, 1.4802525) + 0.1655361 * Math.pow(avertemp, 1.1737852) + 999.81127;//density
        double W_Vel = Waterflow * 4.0 / T_ID / T_ID / PI * 1000 * 1000 / 3600;
        if (1 == coildia) {//如果管径为四分管
            coildia1 = 12.7;
            Dis_T_ID = coildia1 - 0.35 * 2.0;
        } else {
            coildia1 = 9.52;
            Dis_T_ID = coildia1 - 0.3 * 2.0;
        }
        double Dis_W_Vel = Waterflow * 4.0 / Dis_T_ID / Dis_T_ID / PI / Cir_No * 1000.0 * 1000.0 / 3600.0;
        if (1 == coildia) {//如果管径为四分管
            COIL_T_ID = coildia1 - 0.2;
            COIL_W_Vel = Waterflow / Math.pow(13.208 - 2.0 * 0.4064, 2.0) * 1000.0 * 1000.0 / PI / Cir_No / 3600.0 * 4.0;
            Cir_len = T_len * T_no * rows / Cir_No + 31.75 * (T_no * rows / Cir_No - 1) * PI / 2;
            headerlocal = 2.0 * density * W_Vel * W_Vel + 5.0 / 2.0 * density * ((W_Vel + Dis_W_Vel) / 2.0*(W_Vel + Dis_W_Vel) / 2.0);
        } else {//如果管径为三分管
            COIL_T_ID = coildia1 - 0.32;
            COIL_W_Vel = Waterflow / Math.pow(10.058 - 2.0 * 0.3175, 2.0) * 1000.0 * 1000.0 / PI / Cir_No / 3600.0 * 4.0;
            Cir_len = T_len * T_no * rows / Cir_No + 25.4 * (T_no * rows / Cir_No - 1) * PI / 2.0;
            headerlocal = 2.5 * 2.0 / 2.0 * density * W_Vel * W_Vel + 5.5 / 2.0 * density * (Math.pow((W_Vel + Dis_W_Vel) / 2.0, 2.0));
        }
        double coillocal = CalCoilLocal(avertemp, T_ID, Cir_No, T_len, T_no, Waterflow, rows, coildia1);
        double DPheader = CalDPHeader(Watertempin, Watertempout, T_ID, Cir_No, T_len, T_no, Waterflow, rows, coildia1);
        double DPheader1 = CalFrictionfDis(Watertempin, Watertempout, T_ID, Cir_No, T_len, T_no, Waterflow, rows, coildia1);
        DPheader = DPheader + DPheader1;
        double DPCoil = FrictionfCoil(avertemp, T_ID, Cir_No, T_len, T_no, Waterflow, rows, coildia1);
        if (ahri) {
            SWaterDropFactorAhri sWaterdropfactorAhriPo = AhuMetadata.findOne(SWaterDropFactorAhri.class, unitType);
            T_waterdropfactor = sWaterdropfactorAhriPo.getFactor();
            double result = (headerlocal + DPheader + coillocal + DPCoil) / 1000.0 * T_waterdropfactor;
            if (result <= 1) {
                result = result * 4.0;
            } else if (result <= 2 && result > 1) {
                result = result * 3.0;
            } else if (result <= 3 && result > 2) {
                result = result * 2.0;
            } else if (result <= 4 && result > 3) {
                result = result * 1.5;
            } else if (result <= 5 && result > 4) {
                result = result * 1.5;
            }
            return result;
        } else {
            SWaterDropFactor sWaterdropfactorPo = AhuMetadata.findOne(SWaterDropFactor.class, unitType);
            T_waterdropfactor = sWaterdropfactorPo.getFactor();
            return (headerlocal + DPheader + coillocal + DPCoil) / 1000.0 * T_waterdropfactor;
        }

    }

    //封装CalCoilLocal方法
    @SuppressWarnings("unused")
    private static Double CalCoilLocal(double avertemp, int T_ID, int Cir_No, int T_len, int T_no, double Waterflow, int rows, double coildia1) {
        double dt = coildia1;
        double tm = avertemp;
        int Row_No = rows;
        int Tube_No = T_no;
        double density = -0.085866 * Math.pow(tm, 1.4802525) + 0.1655361 * Math.pow(tm, 1.1737852) + 999.81127;
        double mu = 0.01775 / (1 + 0.0337 * tm + 0.000221 * Math.pow(tm, 2)) / 100 / 100;
        double Utotal = Waterflow;
        double d_id = 0.00;
        double d_pin = 0.00;
        double d_return = 0.00;
        double pitch = 0.00;
        if (Math.abs(dt - 9.52) < 0.0001) {
            d_id = 10.058 - 2 * 0.3175;
            d_pin = 9.43;
            d_return = 8.4;
            pitch = 25.4;
        } else {
            d_id = 13.208 - 2 * 0.4064;
            d_pin = 12.4104;
            d_return = 11.1506;
            pitch = 31.75;
        }
        double Len_pin = PI * pitch / 2 * getRoundNum(Tube_No / Cir_No * Row_No / 2);
        double Len_bend = PI * pitch / 2 * getRoundNum(Tube_No / Cir_No * Row_No / 2 - 1);
        double u = Utotal / (PI * Math.pow(d_id, 2)) * 4 * 1000 * 1000 / 3600 / Cir_No;
        double u_pin = u * Math.pow(d_id, 2) / Math.pow(d_pin, 2);
        double Re = u_pin * d_pin / mu / 1000;
        double Ratio = pitch / d_pin;
        double alpha = 1 + 116 * Math.pow(Ratio, -4.52);
        double alpha84 = Math.pow(Ratio, 0.84) * alpha * 0.00241 * 180;
        double Ratio_axi = Math.pow(d_pin, 2) / Math.pow(d_id, 2);
        double f = alpha84 * Math.pow(Re, -0.17);
        double DP1 = f * Math.pow(u_pin, 2) / 2 * density * getRoundNum(Tube_No / Cir_No * Row_No / 2);
        double u_return = u * Math.pow(d_id, 2) / Math.pow(d_return, 2);
        Re = u_return * d_return / mu / 1000;
        Ratio = pitch / d_return;
        alpha = 1 + 116 * Math.pow(Ratio, -4.52);
        alpha84 = Math.pow(Ratio, 0.84) * alpha * 0.00241 * 180;
        Ratio_axi = Math.pow(d_return, 2) / Math.pow(d_id, 2);
        f = alpha84 * Math.pow(Re, -0.17);
        double f_con = (0.1666667 * Ratio_axi - 0.6666667) * Ratio_axi + 0.5;
        double f_exp = Math.pow((1 - Ratio_axi), 2);
        double DP2 = (f + f_con + f_exp) * Math.pow(u_return, 2) / 2 * density * getRoundNum(Tube_No / Cir_No * Row_No / 2 - 1);
        return DP1 + DP2;
    }

    //封装CalDPHeader方法
    private static Double CalDPHeader(double Watertempin, double Watertempout, int T_ID, int Cir_No, int T_len, int T_no, double Waterflow, int rows, double coildia) {
        int NoCir = Cir_No;
        double tin = Watertempin;
        double tout = Watertempin;
        int Dh = T_ID;
        double u = Waterflow * 4 / Dh / Dh / PI * 1000 * 1000 / 3600;
        double LenH = 0.00;
        if (Math.abs(coildia) - 9.52 < 0.00001) {//例如Abs(-2.3);{2.3},Abs(-157);{157}
            LenH = 25.4 * T_no + 2 * 160 - 61 * 2;
        } else {
            LenH = 31.75 * T_no + 2 * 160 - 61 * 2;
        }

        double mu = 0.01775 / (1 + 0.0337 * tin + 0.000221 * Math.pow(tin, 2)) / 100 / 100;
        double Re = u * Dh / mu / 1000;
        double Roughness = 0.046;
        double density = -0.085866 * Math.pow(tin, 1.4802525) + 0.1655361 * Math.pow(tin, 1.1737852) + 999.81127;
        double f = 0.00;
        double f1 = 0.00;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            }
        }
        f = Math.pow(f, 2);
        double DP1 = 0.3 * LenH / Dh * f * Math.pow(u, 2) / 2 * density;
        double DP2 = 0.00;
        for (int i = 1; i < NoCir; i++) {
            Re = u * Dh / mu * (NoCir - i + 1) / NoCir / 1000;
            if (Re <= 1668) {
                f = 64 / Re;
            } else if (Re < 5000 && Re > 1668) {
                f = 0.316 / Math.pow(Re, 0.25);
            } else {
                f = Math.pow(64 / Re, 0.5);
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
                while (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 30;
                    f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
                }
            }
            f = Math.pow(f, 2);
            DP2 = 0.7 * LenH / Dh * f * Math.pow((u * (NoCir - i + 1) / NoCir), 2) / 2 / NoCir * density + DP2;
        }
        mu = 0.01775 / (1 + 0.0337 * tout + 0.000221 * Math.pow(tout, 2)) / 100 / 100;
        density = -0.085866 * Math.pow(tout, 1.4802525) + 0.1655361 * Math.pow(tout, 1.1737852) + 999.81127;
        Re = u * Dh / mu / 1000;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            }
        }
        f = Math.pow(f, 2);
        DP1 = 0.3 * LenH / Dh * f * Math.pow(u, 2) / 2 * density + DP1;
        double DP3 = 0.00;
        for (int i = 1; i < NoCir; i++) {
            Re = u * Dh / mu * (NoCir - i + 1) / NoCir / 1000;
            if (Re <= 1668) {
                f = 64 / Re;
            } else if (Re < 5000 && Re > 1668) {
                f = 0.316 / Math.pow(Re, 0.25);
            } else {
                f = Math.pow(64 / Re, 0.5);
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
                while (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 30;
                    f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
                }
            }
            f = Math.pow(f, 2);
            DP3 = 0.7 * LenH / Dh * f * Math.pow((u * (NoCir - i + 1) / NoCir), 2) / 2 / NoCir * density + DP3;
        }
        return DP1 + DP2 + DP3;
    }

    //封装CalFrictionfDis方法
    @SuppressWarnings("unused")
    private static Double CalFrictionfDis(double Watertempin, double Watertempout, int T_ID, int Cir_No, int T_len, int T_no, double Waterflow, int rows, double coildia) {
        int NoCir = Cir_No;
        double tin = Watertempin;
        double tout = Watertempin;
        int Dh = T_ID;
        double Ddis = 0.00;
        if (Math.abs(coildia) - 9.52 < 0.00001) {
            Ddis = 9.52 - 0.3 * 2;
        } else {
            Ddis = 12.7 - 0.35 * 2;
        }
        double u = Waterflow * 4 / Dh / Dh / PI * 1000 * 1000 / 3600;
        double Lendis = 90.00;
        double Roughness = 0.01;
        double mu = 0.01775 / (1 + 0.0337 * tin + 0.000221 * Math.pow(tin, 2)) / 100 / 100;
        double density = -0.085866 * Math.pow(tin, 1.4802525) + 0.1655361 * Math.pow(tin, 1.1737852) + 999.81127;
        double Re = u * Ddis / mu / 1000;
        double f = 0.00;
        double f1 = 0.00;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            }
            f = Math.pow(f, 2);
        }
        double DP1 = f * Lendis / Ddis * Math.pow(u, 2) / 2 * density;
        mu = 0.01775 / (1 + 0.0337 * tout + 0.000221 * tout * tout) / 100 / 100;
        density = -0.085866 * Math.pow(tout, 1.4802525) + 0.1655361 * Math.pow(tout, 1.1737852) + 999.81127;
        Re = u * Ddis / mu / 1000;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10.0));
            }
            f = Math.pow(f, 2);
        }
        double DP2 = f * Lendis / Ddis * u * u / 2 * density;
        return DP1 + DP2;
    }

    //封装FrictionfCoil方法
    private static Double FrictionfCoil(double watertemp, int T_ID, int Cir_No, int T_len, int T_no, double Waterflow, int rows, double coildia) {
        double dt = coildia;
        double tm = watertemp;
        int Row_No = rows;
        int Tube_No = T_no;
        double density = -0.085866 * Math.pow(tm, 1.4802525) + 0.1655361 * Math.pow(tm, 1.1737852) + 999.81127;   //Correlation regressed based on ASHRAE handbook data
        double Utotal = Waterflow;

        double pitch = 0.00;
        double d_id = 0.00;
        double d_pin = 0.00;
        double d_return = 0.00;
        if (Math.abs(dt - 9.52) < 0.0001) {
            d_id = 10.058 - 2 * 0.3175;
            d_pin = 9.43;
            d_return = 8.4;
            pitch = 25.4;
        } else {
            d_id = 13.208 - 2 * 0.4064;
            d_pin = 12.4104;
            d_return = 11.1506;
            pitch = 31.75;
        }
        double Len_pin = PI * pitch / 2 * getRoundNum(Tube_No / Cir_No * Row_No / 2);
        double Len_bend = PI * pitch / 2 * getRoundNum(Tube_No / Cir_No * Row_No / 2 - 1);

        int Len_tube = T_ID * Tube_No / Cir_No * Row_No;
        double mu = 0.01775 / (1 + 0.0337 * watertemp + 0.000221 * watertemp * watertemp) / 100 / 100;
        double u = Utotal / (PI * Math.pow(d_id, 2)) * 4 * 1000 * 1000 / 3600 / Cir_No;
        double Re = u * d_id / mu / 1000;
        double Roughness = 0.01;
        double f = 0.00;
        double f1 = 0.00;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            }
            f = Math.pow(f, 2);
        }
        double DP1 = f * Len_tube / d_id * Math.pow(u, 2) / 2 * density;
        double u_pin = u * Math.pow(d_id, 2) / Math.pow(d_pin, 2);
        Re = u_pin * d_pin / mu / 1000;
        Roughness = 0.01;


        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            }
            f = Math.pow(f, 2);
        }
        double DP2 = f * Len_pin / d_pin * Math.pow(u_pin, 2) / 2 * density;
        double u_return = u * Math.pow(d_id, 2) / Math.pow(d_return, 2);
        Re = u_return * d_return / mu / 1000;
        Roughness = 0.01;

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow(64 / Re, 0.5);
            f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            while (Math.abs(f - f1) > 0.0001) {
                f = f - (f - f1) / 30;
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10.0));
            }
            f = Math.pow(f, 2);
        }
        double DP3 = f * Len_bend / d_return * Math.pow(u_return, 2) / 2 * density;
        return DP1 + DP2 + DP3;
    }

    public static int getRoundNum(float num)
    {
        float n=(float)((int)num)+0.5f;
        if(num<n)
        {
            return (int)(n-0.5);
        }
        else
        {
            return (int)(n+0.5f);
        }
    }
   

}
