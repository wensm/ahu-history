package com.carrier.ahu.engine.fanYld4k.param;

import com.carrier.ahu.common.entity.UserConfigParam;

/**
 * Created by liangd4 on 2017/7/26.
 */
public class PublicRuleParam {

    private String key;//最经济：economical、最性能：performance
    private String value;//1：生效 0：未生效
    private UserConfigParam userConfigParam;

    public UserConfigParam getUserConfigParam() {
        return userConfigParam;
    }

    public void setUserConfigParam(UserConfigParam userConfigParam) {
        this.userConfigParam = userConfigParam;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
