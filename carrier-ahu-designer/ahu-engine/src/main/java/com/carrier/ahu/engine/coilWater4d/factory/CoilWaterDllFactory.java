package com.carrier.ahu.engine.coilWater4d.factory;

import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_SCALCULATIONCONDITIONS_1;
import static com.carrier.ahu.vo.SystemCalculateConstants.DXCoil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.engine.coilWater4d.count.CalWaterDrop;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.param.CWDCPCParam;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.dxcoil.DXCoilEngine;
import com.carrier.ahu.engine.dxcoil.DXCoilResult;
import com.carrier.ahu.engine.watercoil.WaterCoilEngine;
import com.carrier.ahu.engine.watercoil.WaterCoilResult;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/7. 根据out参数调用DL
 */
@Component
public class CoilWaterDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoilWaterDllFactory.class.getName());

    public List<CoilWaterDllInfo> getDLLResultList(List<CoilDllInParam> coldWaterOutParamList) {
        // 如果条件为null不调用Dll
        if (EmptyUtil.isEmpty(coldWaterOutParamList)) {
            return null;
        }
        List<CoilWaterDllInfo> coldWaterDLLResultList = new ArrayList<>();
        for (CoilDllInParam coilDllInParam : coldWaterOutParamList) {
            List<CWDCPCParam> cPCParamList = coilDllInParam.getCPCParamList();
            String coilType = coilDllInParam.getCoilType();
            for (CWDCPCParam cwdcpcParam : cPCParamList) {
                CoilWaterDllInfo coilWaterDllInfo = new CoilWaterDllInfo();
                if (DXCoil.equals(coilType)) {
                    coilWaterDllInfo = packageCoilWaterDllInfoByDX(coilDllInParam, cwdcpcParam);
                } else {
                    coilWaterDllInfo = packageCoilWaterDllInfoByDH(coilDllInParam, cwdcpcParam);
                }
                if (null == coilWaterDllInfo) {//如果调用DLL超时放弃计算结果
                    continue;
                }
                coilWaterDllInfo.setRows(cwdcpcParam.gettRow());// 排数
                coilWaterDllInfo.setFpi((new Double(coilDllInParam.getFPI())).intValue());// 片距
                coilWaterDllInfo.setCir(coilDllInParam.getCIR());// 回路
                coilWaterDllInfo.setVelocity(coilDllInParam.getVFace());// 面风速
                coilWaterDllInfo.setFaceArea(coilDllInParam.getFaceArea());// 迎风面积
                coilWaterDllInfo.setWaterFlowRate(cwdcpcParam.getFlowRate());
                coldWaterDLLResultList.add(coilWaterDllInfo);
            }
        }
        return coldWaterDLLResultList;
    }

    private CoilWaterDllInfo packageCoilWaterDllInfoByDH(CoilDllInParam coilDllInParam, CWDCPCParam cwdcpcParam) {
        LOGGER.info("coilDllInParam param" + JSONArray.toJSONString(coilDllInParam));
        LOGGER.info("cwdcpcParam param" + JSONArray.toJSONString(cwdcpcParam));
        CoilWaterDllInfo coilWaterDllInfo = new CoilWaterDllInfo();
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        Callable<WaterCoilResult> call = new Callable<WaterCoilResult>() {
            public WaterCoilResult call() throws Exception {
                //开始执行耗时操作
                long beginTime = System.currentTimeMillis();
                LOGGER.info(">>>>>>>>>>>>>" + beginTime + ">>>>>>>>>>>>>");
                WaterCoilResult r = WaterCoilEngine.calculate(cwdcpcParam.getStrCpc(), coilDllInParam.getVFace(),
                        coilDllInParam.getTdbAirIn(), coilDllInParam.getTwbAirIn(),
                        coilDllInParam.getPAirIn(), coilDllInParam.getFFouling(), coilDllInParam.getSurfEff(),
                        coilDllInParam.getRADMULT(), coilDllInParam.getDPDMULT(), coilDllInParam.getRAWMULT(),
                        coilDllInParam.getDPWMULT(), cwdcpcParam.getLBTS(), coilDllInParam.getODEXP(),
                        coilDllInParam.getTW(), coilDllInParam.getIdBend(), coilDllInParam.getIdHairpin(),
                        coilDllInParam.getPT(), coilDllInParam.getPR(), coilDllInParam.getTCond(),
                        Math.toIntExact(coilDllInParam.getCurveNum()), coilDllInParam.getFThick(),
                        coilDllInParam.getFCond(), coilDllInParam.getFPI(), coilDllInParam.getEwt(),
                        coilDllInParam.getTWaterIn(), coilDllInParam.getDtWater(),
                        Math.toIntExact(coilDllInParam.getFluidId()), coilDllInParam.getFluidConcentration(),
                        coilDllInParam.getHeight());
                long endTime = System.currentTimeMillis();
                LOGGER.info("<<<<<<<<<<<<<<<<" + endTime + "<<<<<<<<<<<<<<");
                LOGGER.info("==================" + (endTime - beginTime) + "================");
                return r;
            }
        };
        try {
            Future<WaterCoilResult> future = exec.submit(call);
            WaterCoilResult r = future.get(8000, TimeUnit.MILLISECONDS); //任务处理超时时间设为 3 秒
            coilWaterDllInfo.setLaDB(r.getDryBulbTemperatureOut());// 出风干球温度
            coilWaterDllInfo.setLaWB(r.getWetBulbTemperatureOut());// 出风湿球温度
            coilWaterDllInfo.setApd(r.getAirDrop());// 空气阻力
            coilWaterDllInfo.setWpd(CalWaterDrop.calWaterDropOne(cwdcpcParam.gettId(), cwdcpcParam.getCirNo(), cwdcpcParam.gettLen(), cwdcpcParam.gettNo(), r.getWaterFlowRate(), 0,
                    coilDllInParam.getEwt(), r.getWaterTemperatureOut(), cwdcpcParam.gettRow(), coilDllInParam.getUnitType(), coilDllInParam.getTubeDiameter(), true));// 水阻力
            double waterDropNoAHRI=0.0;
            		try {
//            			waterDropNoAHRI=CalWaterDrop.calWaterDropOne(cwdcpcParam.gettId(), cwdcpcParam.getCirNo(), cwdcpcParam.gettLen(), cwdcpcParam.gettNo(), r.getWaterFlowRate(), 0,
//            coilDllInParam.getTWaterIn(), r.getWaterTemperatureOut(), cwdcpcParam.gettRow(), coilDllInParam.getUnitType(), coilDllInParam.getTubeDiameter(), false);// 水阻力

            
//            CalWaterDrop.calWaterDrop(
//                        		(coilDllInParam.getTWaterIn()+r.getWaterTemperatureOut())/2,
//                        		cwdcpcParam.gettId(), 
//                        		cwdcpcParam.getCirNo(),
//                        		cwdcpcParam.gettLen(),
//                        		cwdcpcParam.gettNo(),
//                                r.getWaterFlowRate(),
//                                cwdcpcParam.gettRow(),
//                                coilDllInParam.getUnitType(), 
//                                coilDllInParam.getTubeDiameter(), 
//                                true);
            			}catch(Exception e) {
                    	e.printStackTrace();
                    }
            		
            coilWaterDllInfo.setReturnNoAHRIWaterDrop(waterDropNoAHRI);
            if (COOLINGCOIL_SCALCULATIONCONDITIONS_1.equals(coilDllInParam.getCalculationConditions())) {
                coilWaterDllInfo.setFlow(r.getWaterFlowRate());//水温升计算水流量
            } else {
                coilWaterDllInfo.setFlow(coilDllInParam.getDtWater());//水流量计算水流量
            }
            coilWaterDllInfo.setCalculationConditions(coilDllInParam.getCalculationConditions());
            coilWaterDllInfo.setTr(r.getWaterTemperatureDelta());// 水温升
            coilWaterDllInfo.setEwt(coilDllInParam.getEwt());// 进水温度
            coilWaterDllInfo.setFluidVelocity(r.getVwbrAve());// 介质流速
            coilWaterDllInfo.setTotal(r.getTotalCap());// 冷量
            coilWaterDllInfo.setSensible(r.getSensibleCap());// 显冷
            coilWaterDllInfo.setBypass(r.getBpf());// Bypass
            coilWaterDllInfo.setSeason(coilDllInParam.getSeason());// 季节
            coilWaterDllInfo.setCoilType(coilDllInParam.getCoilType());// 盘管类型
            coilWaterDllInfo.setUniType(coilDllInParam.getUnitType());// 机组型号
            coilWaterDllInfo.setFluid_ID(coilDllInParam.getFluidId());//Fluid_ID
            coilWaterDllInfo.setCONC1(coilDllInParam.getFluidConcentration());//浓度
            coilWaterDllInfo.setEaDB(coilDllInParam.getTdbAirIn());// 进风干球温度
            coilWaterDllInfo.setEaWB(coilDllInParam.getTwbAirIn());// 进风湿球温度
            coilWaterDllInfo.setEaRH(coilDllInParam.getRtAirIn());// 进风相对湿度
            AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(coilWaterDllInfo.getLaDB(), coilWaterDllInfo.getLaWB());
            coilWaterDllInfo.setLaRH(bean.getParmF());// 出风相对湿度
            LOGGER.info("==================任务成功完成==================");
        } catch (TimeoutException ex) {
            LOGGER.info("==================处理超时啦....================");
            ex.printStackTrace();
            return null;
        } catch (Exception e) {
            LOGGER.info("==================处理失败....================");
            e.printStackTrace();
            return null;
        } finally {
            // 关闭线程池
            exec.shutdown();
        }
        return coilWaterDllInfo;
    }

    private CoilWaterDllInfo packageCoilWaterDllInfoByDX(CoilDllInParam coilDllInParam, CWDCPCParam cwdcpcParam) {
        CoilWaterDllInfo coilWaterDllInfo = new CoilWaterDllInfo();
        DXCoilResult r = DXCoilEngine.DX_calculate(coilDllInParam.getVFace(), coilDllInParam.getTdbAirIn(),
                coilDllInParam.getTwbAirIn(), coilDllInParam.getPAirIn(), 1, coilDllInParam.getDtsh(),
                coilDllInParam.getSst(), coilDllInParam.getSct(), coilDllInParam.getTliq(), 1, 1.0,
                1.0, 1.0, 101, cwdcpcParam.gettNo(), coilDllInParam.getPT(), cwdcpcParam.gettLen(),
                coilDllInParam.getODEXP(), 0.3302, cwdcpcParam.gettRow(), coilDllInParam.getPR(),
                BaseDataUtil.longConversionInteger(coilDllInParam.getCurveNum()), coilDllInParam.getFPI(),
                coilDllInParam.getFThick(), cwdcpcParam.getCirNo(), cwdcpcParam.getCirNo(), 0, 0, 0);
        coilWaterDllInfo.setLaDB(r.getTdbAirOut());// 出风干球温度
        coilWaterDllInfo.setLaWB(r.getTwbAirOut());// 出风湿球温度
        coilWaterDllInfo.setApd(r.getDpAir());// 空气阻力
        coilWaterDllInfo.setTotal(r.getCapTotal());// 冷量
        coilWaterDllInfo.setSensible(r.getCapSensible());// 显冷
        coilWaterDllInfo.setCond(coilDllInParam.getCond());//冷凝器进风温度
        coilWaterDllInfo.setSatCond(coilDllInParam.getSatCond());//饱和冷凝温度
        coilWaterDllInfo.setCDU(coilDllInParam.getCDU());//unit
        coilWaterDllInfo.setEaDB(coilDllInParam.getTdbAirIn());// 进风干球温度
        coilWaterDllInfo.setEaWB(coilDllInParam.getTwbAirIn());// 进风湿球温度
        coilWaterDllInfo.setEaRH(coilDllInParam.getRtAirIn());// 进风相对湿度
        AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(coilWaterDllInfo.getLaDB(), coilWaterDllInfo.getLaWB());
        coilWaterDllInfo.setLaRH(bean.getParmF());// 出风相对湿度
        return coilWaterDllInfo;
    }
}
