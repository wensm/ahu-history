package com.carrier.ahu.engine.fanGk4k.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
@Data
public class FanGKDllInParam{

    private String fn;//风机型号
    private double pst;//总静压
    private double qvt;//风量
    private double nst;//转速
    private String Motorposition;//电机位置 C:侧置H:后置
    private String fanoutlet;//风机出风方向 A: 0 B:90A C:90B D:180
    private double tpw;//电机功率
    private boolean Etr;//转速,静压,全压判定
    private boolean Npt;//转速,静压,全压判定
    private double power;//
    private double maxPower;//
    private double maxMachineSite;//
    private String fanCode;//风机型号
    private String type;//电机类型 1:二级能效电机、2:三级能效电机、3:防爆电机、4:双速电机、5:变频电机、6:变频电机(直连)
    private String supplier;//电机品牌 default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
    private String projectId;

}
