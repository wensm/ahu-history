package com.carrier.ahu.engine.heatRecycle.yufeng;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycle;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.YuFengLib;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.EfficientParam;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.sun.jna.ptr.DoubleByReference;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class YuFengHeatRecycle extends HeatRecycle {

    private static final Logger LOGGER = LoggerFactory.getLogger(YuFengHeatRecycle.class.getName());

    @Override
    public List<HeatRecycleInfo> getHeatRecyleInfo(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        // 夏季默认封装，旧软件无论是单选冬季或者夏季 默认夏季都计算
        HeatRecycleInfo sHeatRecycleInfo = getYuFengEngineSummer(heatRecycleParam);
        if (!EmptyUtil.isEmpty(sHeatRecycleInfo)) {
            heatRecycleInfoList.add(sHeatRecycleInfo);
        } else {
            LOGGER.error("YuFeng Engine Cal Error Season S");
        }
        if (heatRecycleParam.isEnableWinter()) {
            HeatRecycleInfo wHeatRecycleInfo = getYuFengEngineWinter(heatRecycleParam);
            if (!EmptyUtil.isEmpty(wHeatRecycleInfo)) {
                heatRecycleInfoList.add(wHeatRecycleInfo);
            } else {
                LOGGER.error("YuFeng Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    // YuFeng夏季引擎调用
    private HeatRecycleInfo getYuFengEngineSummer(HeatRecycleParam heatRecycleParam) throws Exception {
        int nWorkStatus = 0;
        int nRotorType = 1;
        int nPurge = 0;
        String unitModel = heatRecycleParam.getSerial();// 机组型号
        double dPSI = heatRecycleParam.getSeaLevel();// 海拔高度
        double dBV21 = heatRecycleParam.getRAVolume();// 回风风量
        double dDB21 = heatRecycleParam.getSInDryBulbT();// 夏季回风干球温度
        double dRH21 = heatRecycleParam.getSInRelativeT();// 夏季回风相对湿度
        double dBV11 = heatRecycleParam.getNAVolume();// 新风风量
        double dDB11 = heatRecycleParam.getSNewDryBulbT();// 夏季新风干球温度
        double dRH11 = heatRecycleParam.getSNewRelativeT();// 夏季新风相对湿度
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(dBV21, EngineConstant.SYS_STRING_NUMBER_1,
                EngineConstant.JSON_HEATRECYCLE_POWER_EN);
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaYuFeng());// WHEELDIA_YUFENG
        int nDiameter;
        double velface=0;
        
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
        	rotordia=maxrotordia;
        }
        //从可选直径中选出一个满足当前面风速条件下，最小的一个数据
        double velfacemax = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(maxrotordia),
                EngineConstant.SYS_STRING_NUMBER_1);
        
        double velfacemin = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(rotordia),
                EngineConstant.SYS_STRING_NUMBER_1);
        
        
        if(velfacemin<=6) {
        	nDiameter = BaseDataUtil.stringConversionInteger(rotordia);
        	velface=velfacemin;
        }else if(velfacemax<=6) {
        	nDiameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        	velface=velfacemax;
        }else {
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        
       
        YuFengLib.SetSumParam(dBV21, dBV11, dDB21, dDB11, dRH21, dRH11);// 引擎参数入参
        YuFengLib.SetWorkStatus(nDiameter, nWorkStatus, dPSI, nRotorType, nPurge);
        int ret = YuFengLib.CalcSummerCondition();

        if (ret == 0) {
            double sumPreDropOA = YuFengLib.GetSumPreDropOA();// 新风压力降
            double sumPreDropRA = YuFengLib.GetSumPreDropRA();// 回风压力降
            DoubleByReference dXin = new DoubleByReference(0.0);
            DoubleByReference dHui = new DoubleByReference(0.0);
            YuFengLib.GetSumQuanreXiaolv(dXin, dHui);// 全热效率

            EfficientParam outEfficientParam = new EfficientParam();
            outEfficientParam.setN01(nDiameter);
            outEfficientParam.setR01(BaseDataUtil.decimalConvert(sumPreDropOA, 1));
            outEfficientParam.setR03(BaseDataUtil.decimalConvert(dXin.getValue(), 1));

            EfficientParam exhaustEfficientParam = new EfficientParam();
            exhaustEfficientParam.setN01(nDiameter);
            exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(sumPreDropRA, 1));
            exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(dHui.getValue(), 1));

            HeatRecycleInfo summerHRI = getSummerHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                    exhaustEfficientParam);
            summerHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_SUMMER);
            summerHRI.setReturnWheelSize(String.valueOf(nDiameter));
            return summerHRI;
        } else {
            LOGGER.error("YuFeng Engine Cal getYuFengEngineSummer Error Season S"
                    + JSONArray.toJSONString(heatRecycleParam));
        }
        return null;
    }

    // YuFeng冬季引擎调用
    private HeatRecycleInfo getYuFengEngineWinter(HeatRecycleParam heatRecycleParam) throws Exception {
        int nWorkStatus = 0;
        int nRotorType = 1;
        int nPurge = 0;
        String unitModel = heatRecycleParam.getSerial();// 机组型号
        double dPSI = heatRecycleParam.getSeaLevel();// 海拔高度
        double dBV21 = heatRecycleParam.getRAVolume();// 新风风量
        double dDB21 = heatRecycleParam.getWNewDryBulbT();// 冬季新风干球温度
        double dRH21 = heatRecycleParam.getWNewRelativeT();// 冬季新风相对湿度
        double dBV11 = heatRecycleParam.getNAVolume();// 回风风量
        double dDB11 = heatRecycleParam.getWInDryBulbT();// 冬季回风干球温度
        double dRH11 = heatRecycleParam.getWInRelativeT();// 冬季回风相对湿度
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(dBV21, EngineConstant.SYS_STRING_NUMBER_1,
                EngineConstant.JSON_HEATRECYCLE_POWER_EN);
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaYuFeng());// WHEELDIA_YUFENG
        double velface=0;
        int nDiameter;
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
        	rotordia=maxrotordia;
        }
        //从可选直径中选出一个满足当前面风速条件下，最小的一个数据
        double velfacemax = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(maxrotordia),
                EngineConstant.SYS_STRING_NUMBER_1);
        
        double velfacemin = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(rotordia),
                EngineConstant.SYS_STRING_NUMBER_1);
        
        
        if(velfacemin<=6) {
        	nDiameter = BaseDataUtil.stringConversionInteger(rotordia);
        	velface=velfacemin;
        }else if(velfacemax<=6) {
        	nDiameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        	velface=velfacemax;
        }else {
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        YuFengLib.SetWinParam(dBV21, dBV11, dDB21, dDB11, dRH21, dRH11);
        YuFengLib.SetWorkStatus(nDiameter, nWorkStatus, dPSI, nRotorType, nPurge);
        int ret = YuFengLib.CalcWinterCondition();
        if (ret == 0) {
            double winPreDropOA = YuFengLib.GetWinPreDropOA();
            double winPreDropRA = YuFengLib.GetWinPreDropRA();
            DoubleByReference dXin = new DoubleByReference(0.0);
            DoubleByReference dHui = new DoubleByReference(0.0);
            YuFengLib.GetWinQuanreXiaolv(dXin, dHui);

            EfficientParam outEfficientParam = new EfficientParam();
            outEfficientParam.setN01(nDiameter);
            outEfficientParam.setR01(BaseDataUtil.decimalConvert(winPreDropOA, 1));
            outEfficientParam.setR03(BaseDataUtil.decimalConvert(dXin.getValue(), 1));

            EfficientParam exhaustEfficientParam = new EfficientParam();
            exhaustEfficientParam.setN01(nDiameter);
            exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(winPreDropRA, 1));
            exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(dHui.getValue(), 1));

            HeatRecycleInfo winterHRI = getWinterHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                    exhaustEfficientParam);
            winterHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_WINTER);
            winterHRI.setReturnWheelSize(String.valueOf(nDiameter));
            return winterHRI;
        } else {
            LOGGER.error("YuFeng Engine Cal getYuFengEngineWinter Error Season W"
                    + JSONArray.toJSONString(heatRecycleParam));
        }
        return null;
    }
}
