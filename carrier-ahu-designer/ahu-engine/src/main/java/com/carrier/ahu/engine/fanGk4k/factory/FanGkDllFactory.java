package com.carrier.ahu.engine.fanGk4k.factory;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.GKFanLib;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.engine.fanGk4k.entity.FanGkDllInfo;
import com.carrier.ahu.engine.fanGk4k.param.FanGKDllInParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.Bdd;
import com.carrier.ahu.metadata.entity.fan.Bds;
import com.carrier.ahu.metadata.entity.fan.Noise;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.CCRDFileUtil;
import com.carrier.ahu.unit.CountUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
 * Created by liangd4 on 2017/9/1. 谷科风机不调用DLL文件 程序单独计算并返回结果
 */
public class FanGkDllFactory {

    protected static Logger logger = LoggerFactory.getLogger(FanGkDllFactory.class);

    protected String[] fanSizeGuke = new String[32];
    protected double[] fanProperty = new double[42];

    /**
     * 调用GKDLL引擎
     *
     * @return
     */
    public List<FanGkDllInfo> packageGKEngine(List<FanGKDllInParam> fanGKDllInParamList) {
        List<FanGkDllInfo> fanGkDllInfoList = new ArrayList<FanGkDllInfo>();
        GKFanLib instance = GKFanLib.GUKE_FAN_INSTANCE;
        // 封装标识路径
        long code = System.currentTimeMillis() + EngineConstant.SYS_BLANK.hashCode();
        String projectId = fanGKDllInParamList.get(0).getProjectId();
        String path = EngineConstant.SYS_PATH_BMP_GK + projectId + EngineConstant.SYS_PUNCTUATION_SLASH + code + EngineConstant.SYS_PUNCTUATION_SLASH;
        CCRDFileUtil.createDir(path);// 调用方法创建目录
        File file = new File(path);
        if (file.exists()) {
            logger.info("path exists please call");
        }
        for (FanGKDllInParam fanGKDllInParam : fanGKDllInParamList) {
            String fn = fanGKDllInParam.getFn();// fn 风机型号
            double pst = fanGKDllInParam.getPst();// pst 总静压
            double qvt = fanGKDllInParam.getQvt();// qvt 风量
            double nst = fanGKDllInParam.getNst();// nst 转速
            String Motorposition = fanGKDllInParam.getMotorposition();// Motorposition
            String fanoutlet = fanGKDllInParam.getFanoutlet();// fanoutlet
            double tpw = fanGKDllInParam.getTpw();// tpw 电机功率
            boolean Etr = fanGKDllInParam.isEtr();// Etr 转速,静压,全压判定
            boolean Npt = fanGKDllInParam.isNpt();// Npt 转速,静压,全压判定
            for (int k = 1; k <= 1; k++) {
                try {
                    double airtemperature = 293.0;
                    String record1 = path;// 曲线图路径
                    double Sealevel = 0;
                    double Souspep1 = 0;
                    double Souspep2 = 0;
                    double Souspep3 = 0;
                    double Souspep4 = 0;
                    double Souspep5 = 0;
                    double Souspep6 = 0;
                    double Souspep7 = 0;
                    double Souspep8 = 0;
                    double Souspe1 = 0;
                    double Souspe2 = 0;
                    double Souspe3 = 0;
                    double Souspe4 = 0;
                    double Souspe5 = 0;
                    double Souspe6 = 0;
                    double Souspe7 = 0;
                    double Souspe8 = 0;
                    double pwt = 0.0;
                    double eft = 0.0;
                    double co = 0.0;
                    double js = 0.0;
                    double rla = 0.0;
                    double ptt1 = 0.0;
                    double ptt = 0.0;
                    double maxmotorpower = 0.0;
                    double maxspeed = 0.0;
                    double outw = 0.0;
                    double outH = 0.0;
                    double fanL = 0.0;
                    double FanW = 0.0;
                    double fanH = 0.0;
                    double PL = 0.0;
                    PointerByReference nflg = new PointerByReference(new Memory(4 * Native.getNativeSize(Boolean.TYPE)));
                    DoubleByReference dr = new DoubleByReference();
                    DoubleByReference PL1 = Utils.dRef(PL);

                    DoubleByReference eft1 = Utils.dRef(eft);
                    IntByReference succ = new IntByReference();

                    DoubleByReference rla1 = Utils.dRef(rla);
                    DoubleByReference nst1 = Utils.dRef(nst);
                    DoubleByReference pttA = Utils.dRef(ptt);
                    DoubleByReference pwt1 = Utils.dRef(pwt);
                    DoubleByReference co1 = Utils.dRef(co);
                    DoubleByReference tpw1 = Utils.dRef(tpw);
                    DoubleByReference maxmotorpower1 = Utils.dRef(maxmotorpower);
                    DoubleByReference maxspeed1 = Utils.dRef(maxspeed);
                    DoubleByReference js1 = Utils.dRef(js);

                    DoubleByReference dSouspep1 = Utils.dRef(Souspep1);
                    DoubleByReference dSouspep2 = Utils.dRef(Souspep2);
                    DoubleByReference dSouspep3 = Utils.dRef(Souspep3);
                    DoubleByReference dSouspep4 = Utils.dRef(Souspep4);
                    DoubleByReference dSouspep5 = Utils.dRef(Souspep5);
                    DoubleByReference dSouspep6 = Utils.dRef(Souspep6);
                    DoubleByReference dSouspep7 = Utils.dRef(Souspep7);
                    DoubleByReference dSouspep8 = Utils.dRef(Souspep8);

                    DoubleByReference dSouspe1 = Utils.dRef(Souspe1);
                    DoubleByReference dSouspe2 = Utils.dRef(Souspe2);
                    DoubleByReference dSouspe3 = Utils.dRef(Souspe3);
                    DoubleByReference dSouspe4 = Utils.dRef(Souspe4);
                    DoubleByReference dSouspe5 = Utils.dRef(Souspe5);
                    DoubleByReference dSouspe6 = Utils.dRef(Souspe6);
                    DoubleByReference dSouspe7 = Utils.dRef(Souspe7);
                    DoubleByReference dSouspe8 = Utils.dRef(Souspe8);

                    instance.Calculate(record1, Sealevel, airtemperature, fn, pst, qvt,
                            nst1, pwt1, eft1, pttA, co1, tpw1,
                            js1, Motorposition, fanoutlet, dSouspe1, dSouspe2,
                            dSouspe3, dSouspe4, dSouspe5, dSouspe6,
                            dSouspe7, dSouspe8, dSouspep1, dSouspep2,
                            dSouspep3, dSouspep4, dSouspep5, dSouspep6,
                            dSouspep7, dSouspep8, Etr, succ, rla1, nflg,
                            Utils.dRef(ptt1), maxmotorpower1, maxspeed1, Utils.dRef(outw),
                            Utils.dRef(outH), Utils.dRef(fanL), Utils.dRef(FanW), Utils.dRef(fanH), PL1,
                            new PointerByReference(new Memory(k * Native.getNativeSize(Byte.TYPE))),
                            new PointerByReference(new Memory(k * Native.getNativeSize(Byte.TYPE))), dr);

                    String pathString = path + EngineConstant.SYS_NAME_GUKE_BMP;
                    pathString = pathString.replace("asserts", "files");
                    FanGkDllInfo fanGkDllInfo = new FanGkDllInfo();
                    fanGkDllInfo.setRla(rla1.getValue());
                    fanGkDllInfo.setNst(nst1.getValue());//转速
                    fanGkDllInfo.setPwt(pwt1.getValue());//轴功率
                    fanGkDllInfo.setEft(eft1.getValue());//效率
                    fanGkDllInfo.setPtt(pttA.getValue());//全压
                    fanGkDllInfo.setCo(co1.getValue());//全压-总静压
                    fanGkDllInfo.setTpw(tpw1.getValue());//电机功率
                    fanGkDllInfo.setSuc(true);//计算结果
                    fanGkDllInfo.setMaxmotorpower(maxmotorpower1.getValue());//最大电机功率
                    fanGkDllInfo.setMaxspeed(maxspeed1.getValue());//最大转速
                    fanGkDllInfo.setPL(PL1.getValue());//皮带长度
                    fanGkDllInfo.setPole(js1.getValue());//皮带长度
                    fanGkDllInfo.setFanModel(fn);//风机型号
                    fanGkDllInfo.setFanCode(fanGKDllInParam.getFanCode());//风机code
                    fanGkDllInfo.setPower(fanGKDllInParam.getPower());//power
                    fanGkDllInfo.setMaxPower(fanGKDllInParam.getMaxPower());//maxPower
                    fanGkDllInfo.setMaxMachineSite(fanGKDllInParam.getMaxMachineSite());//maxMachineSite
//					fanGkDllInfo.setFlcode();//风轮编码
//					fanGkDllInfo.setDlcode();//电轮编码
                    fanGkDllInfo.setImped(dr.getValue());//叶轮直径
                    fanGkDllInfo.setCurve(pathString);//图片路径
                    fanGkDllInfo.setSouspe1(dSouspe1.getValue());
                    fanGkDllInfo.setSouspe2(dSouspe2.getValue());
                    fanGkDllInfo.setSouspe3(dSouspe3.getValue());
                    fanGkDllInfo.setSouspe4(dSouspe4.getValue());
                    fanGkDllInfo.setSouspe5(dSouspe5.getValue());
                    fanGkDllInfo.setSouspe6(dSouspe6.getValue());
                    fanGkDllInfo.setSouspe7(dSouspe7.getValue());
                    fanGkDllInfo.setSouspe8(dSouspe8.getValue());

                    fanGkDllInfo.setSouspep1(dSouspep1.getValue());
                    fanGkDllInfo.setSouspep2(dSouspep2.getValue());
                    fanGkDllInfo.setSouspep3(dSouspep3.getValue());
                    fanGkDllInfo.setSouspep4(dSouspep4.getValue());
                    fanGkDllInfo.setSouspep5(dSouspep5.getValue());
                    fanGkDllInfo.setSouspep6(dSouspep6.getValue());
                    fanGkDllInfo.setSouspep7(dSouspep7.getValue());
                    fanGkDllInfo.setSouspep8(dSouspep8.getValue());
                    fanGkDllInfo.setType(fanGKDllInParam.getType());
                    fanGkDllInfo.setSupplier(fanGKDllInParam.getSupplier());
                    fanGkDllInfoList.add(fanGkDllInfo);
                    logger.info(MessageFormat.format("GUKE engine calculation successful.fn:{0}", fn));
                } catch (Error e) {
                    logger.error(MessageFormat.format("Error In run GUKE engine,fn:{0}", fn), e);
                } catch (Exception e) {
                    logger.error(MessageFormat.format("Exception In run GUKE engine,fn:{0}", fn), e.getMessage());
                }
            }
        }
        return fanGkDllInfoList;
    }

    // 封装谷科风机引擎 pPres:风压 qvt：风量
    public List<FanInfo> packageGKEngine1(String fanCode, double pPres, double qvt, double sealevel,
                                          double airtemperature) {
        for (int i = 0; i < 2; i++) {// 循环YFH(K)、YFH(G)两个系列
            String fanName = EngineConstant.SYS_BLANK;
            if (i == 0) {
                fanName = "YFH(K)" + fanCode;
            } else {
                fanName = "YFH(G)" + fanCode;
            }
            fanSizeGuke = getFanSizeGuke(fanName);
            fanProperty = getFanProperty(fanName);
            int spemrow = getSpemrow(fanName);
            // String record1 = ExtractFilePath(ParamStr(0)) + 'GUKE\';
            // String record1 = "GUKE";
            boolean etr = true;
            boolean npt = true;
            // String motorposition = "H";
            // String fanoutlet = "A";
            boolean b = GukeCalculate1(sealevel, airtemperature, etr, npt, pPres, qvt, spemrow);
            if (b) {
                logger.info("packageGKEngine1 true");
            }
        }
        return null;
    }

    // 页面显示结果封装
    public List<FanInfo> packageEngineResult(double motor, double maxmotorpower, int js, double maxmachinesite,
                                             double RMotormaxmachinesite) {
        if (motor >= 0.55 && motor <= maxmotorpower) {
            SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, String.valueOf(motor), String.valueOf(js));
            if (EmptyUtil.isNotEmpty(skFanMotor)) {
                // String strTyper = skFanMotorPo.getMachineSiteNo();
                // String machinesite = skFanMotorPo.getMachineSite();
                /*
                 * if (BaseDataUtil.stringConversionDouble(machinesite) >
				 * maxmachinesite &&
				 * BaseDataUtil.stringConversionDouble(machinesite) <=
				 * RMotormaxmachinesite) {
				 * 
				 * }
				 */
            }
            FanInfo fanInfo = new FanInfo();
            fanInfo.setFanModel(EngineConstant.SYS_BLANK);// 风机型号
            fanInfo.setOutletVelocity(EngineConstant.SYS_BLANK);// 出风口风速
            fanInfo.setEfficiency(EngineConstant.SYS_BLANK);// 效率
            fanInfo.setAbsorbedPower(EngineConstant.SYS_BLANK);// 吸收功率
            fanInfo.setRPM(EngineConstant.SYS_BLANK);// 转速
            fanInfo.setNoise(EngineConstant.SYS_BLANK);// 噪音
            fanInfo.setMotorBaseNo(EngineConstant.SYS_BLANK);// 机座号
            fanInfo.setMotorPower(EngineConstant.SYS_BLANK);// 电机功率
            fanInfo.setPole(EngineConstant.SYS_BLANK);// 极数
            // List<SKFanMotorPo> skFanMotorPoList =
            // skFanMotorDao.getSKFanMotorListByPowerAndLevel(motor, js);
        }

        return null;
    }

    // pst:风压 qvt：风量
    private boolean GukeCalculate1(double sealevel, double airtemperature, boolean etr, boolean npt, double pst,
                                   double qvt, int spemrow) {
        double ln10 = ln(10.0);
        double prp0 = Math.pow((1 - 0.02257 * sealevel), 5.256);
        // double trt0 = 293.0 / airtemperature;
        double ro0ro = airtemperature / 293.0 / prp0;
        // double maxMotorPower =
        // BaseDataUtil.stringConversionDouble(fanSizeGuke[28]);
        // double maxSpeed =
        // BaseDataUtil.stringConversionDouble(fanSizeGuke[27]);
        double co = 0.0;
        double pd = 0.0;
        double ptt = 0.0;
        double nst = 0.0;
        double eft = 0.0;
        double pwt = 0.0;
        double rla = 0.0;
        double ptt1 = 0.0;
        double tpw = 0.0;
        boolean suc = false;
        if (etr && npt) {
            if (fanSizeGuke[26].toLowerCase().equals("'ktq2'")) {// 如果值相同
                co = qvt / 3600.0 / (BaseDataUtil.stringConversionDouble(fanSizeGuke[3]) / 1000.
                        * BaseDataUtil.stringConversionDouble(fanSizeGuke[2]) / 1000.0);
            } else {
                co = qvt / 3600.0 / (BaseDataUtil.stringConversionDouble(fanSizeGuke[3]) / 1000
                        * BaseDataUtil.stringConversionDouble(fanSizeGuke[2]) / 1000);
            }
            pd = 0.6 * co * co;
            pst = pst * ro0ro;
            ptt = pst + pd;
            suc = thToThtGuke(CountUtil.lg(qvt), CountUtil.lg(ptt), nst, eft, pwt, rla, npt, ptt1);
            pwt = pwt / 1000.0 / ro0ro;
            pst = pst / ro0ro;
            pd = pd / ro0ro;
            ptt = ptt / ro0ro;
        }
        if (!etr && npt) {
            co = qvt / 3600.0 / (BaseDataUtil.stringConversionDouble(fanSizeGuke[3]) / 1000.
                    * BaseDataUtil.stringConversionDouble(fanSizeGuke[2]) / 1000.0);
            pd = 0.6 * co * co;
            suc = thToThtGuke(CountUtil.lg(qvt), CountUtil.lg(ptt), nst, eft, pwt, rla, npt, ptt1);
            pwt = pwt / 1000.0;
        }
        if (!etr && !npt) {
            co = qvt / 3600.0 / (BaseDataUtil.stringConversionDouble(fanSizeGuke[3]) / 1000.
                    * BaseDataUtil.stringConversionDouble(fanSizeGuke[2]) / 1000.0);
            suc = thToThtGuke(CountUtil.lg(qvt), ptt, nst, eft, pwt, rla, npt, ptt1);
            pd = 0.6 * co * co;
            ptt = ptt1;
            pwt = pwt / 1000.0;
        }
        // double nstl = BaseDataUtil.stringConversionDouble(fanSizeGuke[22]);
        // double pwtl = BaseDataUtil.stringConversionDouble(fanSizeGuke[23]);
        int spwl = 24;
        int spwi = 0;
        if (pwt <= 2.2) {
            tpw = pwt * 1.2;
        } else if (pwt > 2.2 && pwt <= 11) {
            tpw = pwt * 1.15;
        } else {
            tpw = pwt * 1.1;
        }

        double[] spw = {0.37, 0.55, 0.75, 1.1, 1.5, 2.2, 3.0, 4.0, 5.5, 7.5, 11.0, 15.0, 18.5, 22.0, 30.0, 37.0, 45.0,
                55.0, 75.0, 90, 110, 132, 160, 200, 250};
        // double[][] souspem = {{-7., -2., -2., -3., -5., -7., -9., -14.0},
        // {-2., -1., -2., -3., -5., -7., -9., -14.0}, {0., 0., -2., -3., -5.,
        // -7., -9., -14.0}, {3., 0., -2., -3, -5., -7., -9., -14.0}, {0., 3.,
        // 0., -1., -5., -9., -13., -20.0}, {5., 6., 0., -1., -5., -9., -13.,
        // -20.0}, {0., 0., 1., -2., -5., -9., -11., -16.0}};
        double[][] souspem1 = {{-7., -2., -2., -3., -5., -7., -9., -14.0}, {2., 0., 0., -2., -4., -6., -9., -13.0},
                {4., -1., -2., -4., -6., -6., -8., -13.0}, {2., 0., 0., -2., -4., -6., -9., -14.0},
                {5., -4., -3., -3., -7., -9., -11., -16.0}, {4., 1., -3., -4., -7., -10., -15., -19.0},
                {4., 2., -3., -7., -9., -10., -16., -20.0}};

        double[] qva = new double[50];
        double[] Pta = new double[50];
        double[] Efa = new double[50];
        double[] Pwa = new double[50];
        double[] Qva = new double[50];
        double[] souspe = new double[50];
        double[] souspep = new double[50];
        double dqva = 0.0;
        double lqva = 0.0;
        @SuppressWarnings("unused")
        int js = 0;
        while (spwi < spwl && tpw > spw[spwi]) {
            spwi = spwi + 1;
            tpw = spw[spwi];
            if (tpw > BaseDataUtil.stringConversionDouble(fanSizeGuke[28])
                    || nst > BaseDataUtil.stringConversionDouble(fanSizeGuke[27]) || tpw == 0) {
                tpw = 0;
                suc = false;
                return suc;
            }
            qva[0] = Math.pow(Math.E, fanProperty[19] * ln10);// e的x次幂
            qva[19] = Math.pow(Math.E, fanProperty[20] * ln10);// e的x次幂
            dqva = (qva[19] - qva[0]) / 39;
            for (int i = 1; i <= 39; i++) {
                qva[i] = qva[i - 1] + dqva;
            }
            for (int i = 0; i <= 39; i++) {
                lqva = CountUtil.lg(qva[i]);
                qva[i] = qva[i] * nst / fanProperty[2];
                Pta[i] = Math.pow(Math.E, per(lqva) * ln10) * Math.pow((nst / fanProperty[2]), 2);
                Efa[i] = eco(lqva) * 100;
                if (nst > 0.0) {
                    Pwa[i] = Qva[i] / 3600 * Pta[i] / Efa[i] / 10;
                }
            }
            if (nst >= 2200) {
                js = 2;
            } else if (nst < 2200 && nst >= 750) {
                js = 4;
            } else if (nst < 750) {
                js = 6;
            }
            for (int i = 0; i <= 7; i++) {
                souspe[i] = rla + souspem1[spemrow][i] - 7;
                souspep[i] = souspe[i] + 7.0;
            }
        }
        return suc;
    }

    private double eco(double qv) {
        qv = qv - fanProperty[21];
        double la = fanProperty[11] + (fanProperty[12] + (fanProperty[13] + fanProperty[14] * qv) * qv) * qv;
        return la;
    }

    private double per(double qv) {
        qv = qv - fanProperty[21];
        double p = fanProperty[3] + (fanProperty[4] + (fanProperty[5] + fanProperty[6] * qv) * qv) * qv;
        return p;
    }

    private double root(double qv, double pt) {
        double qtry = 0.0;
        double ptry = 0.0;
        double lq = fanProperty[19];
        double rq = fanProperty[20];
        double dq = (rq - lq) / 200.0;
        double p1 = per(lq) + 2. * (qv - lq);
        double p2 = per(rq) + 2. * (qv - rq);
        if (pt > p1 || pt < p2) {
            return -1.0;
        } else {// 满足 p2<pt<p1
            qtry = (pt - p1) / (p2 - p1) * (rq - lq) + lq;
            ptry = pt + 2. * (qtry - qv);
            if (per(qtry) > ptry) {
                while (per(qtry) > ptry) {
                    qtry = qtry + dq;
                    ptry = ptry + 2. * dq;
                }
            } else {
                while (per(qtry) < ptry) {
                    qtry = qtry + dq;
                    ptry = ptry + 2. * dq;
                }
            }
            return qtry - dq * 0.5;
        }
    }

    // 计算自然对数
    private double ln(double d) {
        return Math.log(d);
    }

    // 计算e的x幂
    private double exp(double d) {
        return Math.pow(Math.E, d);
    }

    // 封装sou
    private double sou(double qv) {
        qv = qv - fanProperty[21];
        return fanProperty[7] + (fanProperty[8] + (fanProperty[9] + fanProperty[10] * qv) * qv) * qv;
    }

    private boolean thToThtGuke(double q, double p, double rn, double reff, double rpw, double rla, boolean npt,
                                double ptt1) {
        double lqv = 0.0;
        double n = 0.0;
        double n1 = 0.0;
        if (npt) {
            lqv = root(q, p);
            if (lqv > 0.0) {
                n = q - lqv;
                rn = exp(ln(10.0) * n) * fanProperty[2];
                reff = eco(lqv);
                rpw = exp(ln(10.0) * (q + p)) / reff / 3600.0;
                rla = sou(lqv) + 60.0 * n;
                if (rn < fanProperty[18] && rn > fanProperty[17]) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            n1 = CountUtil.lg(rn / fanProperty[2]);
            lqv = q - n1;
            if (lqv >= fanProperty[19] && lqv <= fanProperty[20]) {
                ptt1 = exp(ln(10.0) * per(lqv)) * Math.pow(rn / fanProperty[2], 2);
                reff = eco(lqv);
                rpw = exp(ln(10.0) * (q + CountUtil.lg(ptt1))) / reff / 3600.0;
                rla = sou(lqv) + 60.0 * n1;
                return true;
            } else {
                return false;
            }
        }
    }

    private int getSpemrow(String fanName) {
        Noise noise = AhuMetadata.findOne(Noise.class, fanName);
        return noise.getNoise();
    }

    private String[] getFanSizeGuke(String fanName) {
        String[] fanSizeGuke = new String[32];
        Bds bds = AhuMetadata.findOne(Bds.class, fanName);
        if (EmptyUtil.isNotEmpty(bds)) {
            for (int i = 1; i <= fanSizeGuke.length; i++) {
                try {
                    fanSizeGuke[i - 1] = BeanUtils.getProperty(bds, EngineConstant.SYS_ALPHABET_N + i);
                } catch (Exception e) {
                    throw new EngineException("Failed to get fan size guke", e);
                }
            }
        }
        return fanSizeGuke;
    }

    private double[] getFanProperty(String fanName) {
        double[] fanProperty = new double[42];
        Bdd bdd = AhuMetadata.findOne(Bdd.class, fanName);
        if (EmptyUtil.isNotEmpty(bdd)) {
            for (int i = 1; i <= fanProperty.length; i++) {
                try {
                    String value = BeanUtils.getProperty(bdd, EngineConstant.SYS_ALPHABET_N + i);
                    fanProperty[i - 1] = BaseDataUtil.objectToDouble(value);
                } catch (Exception e) {
                    throw new EngineException("Failed to get fan property", e);
                }
            }
        }
        return fanProperty;
    }

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis() + EngineConstant.SYS_BLANK);
        System.out.println(String.format(EngineConstant.SYS_FORMAT_2F, 0.11111));
        System.out.println();
        int x = 5; // 次方数
        double e = Math.E;// 自然常数e的近似值
        double d = Math.pow(e, x);// e^x
        System.out.println("e^" + x + "=" + d);// 输出结果

    }

}
