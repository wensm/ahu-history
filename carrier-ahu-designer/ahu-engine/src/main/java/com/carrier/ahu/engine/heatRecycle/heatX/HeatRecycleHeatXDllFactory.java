package com.carrier.ahu.engine.heatRecycle.heatX;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.CaldensityLib;
import com.carrier.ahu.engine.heatrecycle.HeaterxLib;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleHeatXDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleHeatXDllFactory.class.getName());

    private static final HeatRecycleHeatXResultFactory HEAT_RECYCLE_HEAT_X_RESULT_FACTORY = new HeatRecycleHeatXResultFactory();

    //heatXDLL调用封装
    public List<HeatRecycleInfo> getHeatXDllAll(HeatRecycleParam heatRecycleParam) {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if(heatRecycleParam.isEnableWinter()){
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = getHeatXDll(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("HeatXDll Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = getHeatXDll(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("HeatXDll Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    //heatXDLL调用封装
    public HeatRecycleInfo getHeatXDll(HeatRecycleParam heatRecycleParam) {
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;//季节
        double E_NAVolume = heatRecycleParam.getRAVolume();//回风风量
        double warmTemp = heatRecycleParam.getSNewDryBulbT();//回风夏季干球温度
        double warmWetTemp = heatRecycleParam.getSNewWetBulbT();//回风夏季湿球温度
        double warmRT = heatRecycleParam.getSNewRelativeT();//回风夏季相对湿度
        double coldTemp = heatRecycleParam.getSInDryBulbT();//新风夏季干球温度
        double coldWetTemp = heatRecycleParam.getSInWetBulbT();//新风夏季湿球温度
        double coldRT = heatRecycleParam.getSInRelativeT();//回风夏季相对湿度
        if (heatRecycleParam.isEnableWinter()) {
            E_NAVolume = heatRecycleParam.getRAVolume();//回风风量
            warmTemp = heatRecycleParam.getWNewDryBulbT();//回风冬季干球温度
            warmWetTemp = heatRecycleParam.getWNewWetBulbT();//回风冬季湿球温度
            coldTemp = heatRecycleParam.getWInDryBulbT();//新风冬季干球温度
            warmRT = heatRecycleParam.getWNewRelativeT();//回风夏季相对湿度
            coldRT = heatRecycleParam.getWInRelativeT();//回风夏季相对湿度
        }
        String unitModel = heatRecycleParam.getSerial();//机组型号
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(E_NAVolume / 3600, EngineConstant.SYS_STRING_NUMBER_2, "POWER_HEATX");
        //查表计算
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaHeatX());//wheeldia_HeatX
        double Diameter;
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
            Diameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        } else {
            Diameter = BaseDataUtil.stringConversionInteger(rotordia);
        }
        String ECOFRESHModel = String.valueOf(Diameter);
        double velface = SystemCountUtil.velfacecal(E_NAVolume, Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if(velface > 6){
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        double depth = heatRecycleParam.getWheelDepth();//转轮厚度
        int mCode;//逻辑判断
        int rSpeed;//逻辑判断
        String model = heatRecycleParam.getModel();
        if (EngineConstant.SYS_STRING_NUMBER_1.equals(model)) {//热回收型号 1：分子筛 2：硅胶 3：显热
            mCode = 7;
            rSpeed = 25;
        } else if (EngineConstant.SYS_STRING_NUMBER_2.equals(model)) {
            mCode = 5;
            rSpeed = 12;
        } else {
            mCode = 0;
            rSpeed = 17;
        }
        double attitude = FAttitudeConvert(heatRecycleParam.getSeaLevel(), false);//海拔高度转换
        double temp = AirConditionUtils.FTemperatureConvert(warmTemp, true);//回风温度转换
        double density = CaldensityLib.CALDENSITY_INSTANCE.CaldensityBritish(attitude, warmTemp) * 1.2;//海拔高度和回风干球温度转换
        byte s1 = 0;
        byte s2 = 0;
        byte z1 = 0;
        byte z2 = 0;
//        String key = "E/0.200/" + getMaterial(mCode);
        String key = "H0750/6.0/E";
        Pointer in = new Memory(20 * Double.SIZE);
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), 2.5);//固定
        in.setDouble(2 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(3 * Native.getNativeSize(Double.TYPE), warmTemp);
        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE), rAirConditionBean.getParamD() / 1000);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), E_NAVolume / 3600 * density);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(7 * Native.getNativeSize(Double.TYPE), coldTemp);
        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp, coldWetTemp);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), nAirConditionBean.getParamD() / 1000);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE), mCode);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), Diameter / 1000);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE), rSpeed);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE), 0.002);//固定
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), depth / 1000);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0.00005);//固定
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 101325);//固定
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 1);//固定
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 3);//固定
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 5);//固定
        Pointer out = new Memory(50 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_CALCULATION(s1, s2, in, key, z1, z2, out);
        //TODO DLL参数返回值待确认
        int rotorSize = 1600;//转轮尺寸
        double sdbt = out.getDouble(23 * Native.getNativeSize(Double.TYPE));
        double swbt = out.getDouble(41 * Native.getNativeSize(Double.TYPE));
        AirConditionBean airConditionBeanS = null;
        try {
            airConditionBeanS = AirConditionUtils.FAirParmCalculate2(sdbt, swbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 S Error", e.getMessage());
        }
        double supplyAirD = sdbt;//干球温度送风侧
        double supplyAirWB = 0.00;//湿球温度送风侧
        double supplyAirRH = 0.00;//相对湿度送风侧
        if (null != airConditionBeanS) {
            supplyAirWB = airConditionBeanS.getParmTb();
            supplyAirRH = airConditionBeanS.getParmF();
        }
        double supplyAirPD = out.getDouble(22 * Native.getNativeSize(Double.TYPE));//空气阻力送风侧
        double supplyAirTotalEff = out.getDouble(31 * Native.getNativeSize(Double.TYPE));//效率送风侧

        AirConditionBean airConditionBeanE = null;
        double edbt = out.getDouble(27 * Native.getNativeSize(Double.TYPE));
        double ewbt = out.getDouble(28 * Native.getNativeSize(Double.TYPE));
        double exhaustAirDB = edbt;//干球温度进风侧
        double exhaustAirWB = 0.00;//湿球温度进风侧
        double exhaustAirRH = 0.00;//相对湿度进风侧
        try {
            airConditionBeanE = AirConditionUtils.FAirParmCalculate2(edbt, ewbt*100);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 E Error", e.getMessage());
        }
        if (null != airConditionBeanE) {
            exhaustAirWB = airConditionBeanE.getParmTb();//湿球温度进风侧
            exhaustAirRH = airConditionBeanE.getParmF();//相对湿度进风侧
        }
        double exhaustAirPD = out.getDouble(26 * Native.getNativeSize(Double.TYPE));//空气阻力进风侧
        double exhaustAirTotalEff = out.getDouble(32 * Native.getNativeSize(Double.TYPE));//效率进风侧
        double totalCap = out.getDouble(29 * Native.getNativeSize(Double.TYPE)) / 1000.0;//全热
        double sensible = out.getDouble(30 * Native.getNativeSize(Double.TYPE)) / 1000.0;//显热
/*        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        df.format(value);*/
        HeatRecycleInfo heatRecycleInfo = HEAT_RECYCLE_HEAT_X_RESULT_FACTORY.packageHeatRecycleInfo(rotorSize, season, coldTemp, coldWetTemp, coldRT, supplyAirPD, supplyAirTotalEff,
                warmTemp, warmWetTemp, warmRT, exhaustAirPD, exhaustAirTotalEff, totalCap, sensible);
        if(!EmptyUtil.isEmpty(heatRecycleInfo)){
            return heatRecycleInfo;
        }
        return null;
    }

    private String getMaterial(int material) {
        switch (material) {
            case 0:
                return "Al";
            case 1:
                return "Epoxy";
            case 6:
                return "Hybrid";
            case 5:
                return "Silica";
            case 7:
                return "MS";
            case 8:
                return "MS+";
        }
        return "";
    }

    private double FAttitudeConvert(double ParmCF, boolean ParmFlag) {
        if (ParmFlag) {
            return ParmCF / 3.281;
        } else {
            return ParmCF * 3.281;
        }
    }
}
