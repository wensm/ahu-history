package com.carrier.ahu.engine.coilWater4d;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilDx4d.DXCoilService;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.factory.CoilDataBaseFactory;
import com.carrier.ahu.engine.coilWater4d.factory.CoilWaterDllFactory;
import com.carrier.ahu.engine.coilWater4d.factory.CoilWaterDllInParamFactory;
import com.carrier.ahu.engine.coilWater4d.factory.CoilWaterResultFactory;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.coilWater4d.util.CoilPackageUtil;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

//盘管引擎计算
@Component
public class WaterCoilServiceImpl implements WaterCoilService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaterCoilServiceImpl.class.getName());

    @Autowired
    CoilWaterDllFactory coilWaterDllFactory;
    @Autowired
    DXCoilService dxCoilService;
    @Autowired
    CoilDataBaseFactory coilDataBaseFactory;
    @Autowired
    CoilWaterDllInParamFactory coilWaterDllInParamFactory;
    @Autowired
    CoilWaterResultFactory coilWaterResultFactory;

    public List<CoilInfo> getWaterCoil(CoilInParam coilInParam) {
        List<CoilInfo> coilInfoList = this.packageCoilDll(coilInParam);
        coilInfoList = CoilPackageUtil.packageCalculateResults(coilInfoList, coilInParam);
        coilInfoList = CoilPackageUtil.packageUserConfig(coilInfoList, coilInParam);
        coilInfoList = CoilPackageUtil.packageCondensation(coilInfoList, coilInParam);//凝露校核
        return coilInfoList;
    }

    private List<CoilInfo> packageCoilDll(CoilInParam coilInParam) {
        LOGGER.info("cooling coil calculator begin " + DateUtil.getStringDate());
        if (coilInParam.isCalculateOptimalResults()) {//计算5条
            List<CoilInfo> coilInfoListResult = new ArrayList<CoilInfo>();
            String row = CoilDataBaseFactory.getUnitTypeRow(coilInParam);//根据机组型号、盘管类型、管径获取排数 获取数组
            if (EmptyUtil.isEmpty(row)) {
                return null;
            }
            String[] rows = row.split(EngineConstant.SYS_PUNCTUATION_COMMA);
            String sysRow = coilInParam.getRows();
            for (int i = 0; i <= rows.length - 1; i++) {
                String calRow = rows[i];
                if (SystemCalculateConstants.COOLINGCOIL_ROWS_AUTO.equals(sysRow)) {
                    if (SystemCalculateConstants.waterCoil.equals(coilInParam.getCoilType()) && BaseDataUtil.stringConversionInteger(calRow) <= 2) {
                        continue;
                    } else {
                        coilInParam.setRows(calRow);
                    }
                    List<CoilInfo> newCoilInfoList = packageCalculateCoil(coilInParam);
                    coilInfoListResult.addAll(newCoilInfoList);
                    if (coilInfoListResult.size() >= 5) {
                        return coilInfoListResult;
                    }
                } else {
                    List<CoilInfo> newCoilInfoList = packageCalculateCoil(coilInParam);
                    return newCoilInfoList;
                }
            }
            return coilInfoListResult;
        } else {//计算所有
            coilInParam.setCalculateAllResults(true);
            List<CoilInfo> newCoilInfoList = packageCalculateCoil(coilInParam);
            return newCoilInfoList;
        }
    }

    private List<CoilInfo> packageCalculateCoil(CoilInParam coilInParam) {
        List<CoilDllInParam> coldWaterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);//入参数据加工&&包含多个CPC文件参数
        LOGGER.info("cooling coil calculator dll in param " + DateUtil.getStringDate());
        List<CoilWaterDllInfo> calCoilWaterDllInfoList = coilWaterDllFactory.getDLLResultList(coldWaterOutParamList);//根据out参数调用DLL
        LOGGER.info("cooling coil calculator dll result " + DateUtil.getStringDate());
        List<CoilInfo> coilInfoList = coilWaterResultFactory.packageCountResult(calCoilWaterDllInfoList); //引擎结果数据加工
        List<CoilInfo> newCoilInfoList = CoilPackageUtil.packageNewCoilInfoList(coilInParam, coilInfoList);//加工结果过滤
        return newCoilInfoList;
    }
}
