package com.carrier.ahu.engine.heatRecycle;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.dri.DRIHeatRecycle;
import com.carrier.ahu.engine.heatRecycle.heatX.HeatRecycleHeatXDllFactory;
import com.carrier.ahu.engine.heatRecycle.plate.PlateHeatRecycle;
import com.carrier.ahu.engine.heatRecycle.yufeng.YuFengHeatRecycle;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
public class HeatRecycleDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleDllFactory.class.getName());
    private static final HeatRecycleHeatXDllFactory HEAT_RECYCLE_HEAT_X_DLL_FACTORY = new HeatRecycleHeatXDllFactory();
    private static final YuFengHeatRecycle YUFENG_HEAT_RECYCLE = new YuFengHeatRecycle();
    private static final PlateHeatRecycle PLATE_HEAT_RECYCLE = new PlateHeatRecycle();
    private static final DRIHeatRecycle DRI_HEAT_RECYCLE = new DRIHeatRecycle();

    //调用热回收引擎
    public List<HeatRecycleInfo> heatRecycleDll(HeatRecycleParam heatRecycleParam) throws Exception {
        LOGGER.info("heatRecycleDll engine supper begin ");
        LOGGER.info("heatRecycleDll engine supper param:" + JSONArray.toJSONString(heatRecycleParam));
        heatRecycleParam = packageHeatRecycleParam(heatRecycleParam);//特殊字段加工
        List<HeatRecycleInfo> heatRecycleInfoList = null;
        if (heatRecycleParam.isNSEnable()) {//非标无转轮不参数计算
            LOGGER.info("heatRecycleDll TEP engine supper param:" + heatRecycleParam);
            return null;
        } else {
            if (heatRecycleParam.getBrand().equals("A")) {//A：标准供应商 B：百瑞 C:Enventus 毅科
                if (EngineConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE
                        .equals(heatRecycleParam.getHeatRecoveryType())) {
                    heatRecycleInfoList = PLATE_HEAT_RECYCLE.getHeatRecyleInfo(heatRecycleParam);
                } else {
                    heatRecycleInfoList = YUFENG_HEAT_RECYCLE.getHeatRecyleInfo(heatRecycleParam);
                }
            } else if (heatRecycleParam.getBrand().equals("C")) {//Enventus:毅科
                heatRecycleInfoList = HEAT_RECYCLE_HEAT_X_DLL_FACTORY.getHeatXDllAll(heatRecycleParam);
            } else {//如果是百瑞
                heatRecycleInfoList = DRI_HEAT_RECYCLE.getHeatRecyleInfo(heatRecycleParam);
            }
        }
        LOGGER.info("heatRecycleDll engine supper end ");
        return heatRecycleInfoList;
    }

    //特殊字段加工
    private HeatRecycleParam packageHeatRecycleParam(HeatRecycleParam heatRecycleParam) {
        if (1 == heatRecycleParam.getWheelDepth()) {//转轮厚度
            heatRecycleParam.setWheelDepth(200);
        }
        return heatRecycleParam;
    }

}
