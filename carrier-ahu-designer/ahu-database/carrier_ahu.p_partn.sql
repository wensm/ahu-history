CREATE TABLE `p_partn` (
  `PID` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `AirFlowO` varchar(1) DEFAULT NULL,
  `AInterface` varchar(1) DEFAULT NULL,
  `AInterfaceR` double DEFAULT NULL,
  `ODoor` varchar(1) DEFAULT NULL,
  `SectionL` smallint(5) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `DOORTYPE` varchar(1) DEFAULT NULL,
  `DOORDIRECTION` varchar(1) DEFAULT NULL,
  `VALVEM` varchar(1) DEFAULT NULL,
  `UVC` bit(1) NOT NULL,
  `ACTUATOR` varchar(1) DEFAULT NULL,
  KEY `partID` (`partID`),
  KEY `pid` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

