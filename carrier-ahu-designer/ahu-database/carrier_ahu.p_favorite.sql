CREATE TABLE `p_favorite` (
  `Pid` varchar(38) DEFAULT NULL,
  `UnitId` varchar(38) NOT NULL,
  `TTreeCode` varchar(4) DEFAULT NULL,
  `FTreeName` varchar(50) DEFAULT NULL,
  `FTreeMemo` varchar(255) DEFAULT NULL,
  KEY `Pid` (`Pid`),
  KEY `TTreeCode` (`TTreeCode`),
  KEY `UnitId` (`UnitId`),
  CONSTRAINT `pid` FOREIGN KEY (`Pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uid` FOREIGN KEY (`UnitId`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

