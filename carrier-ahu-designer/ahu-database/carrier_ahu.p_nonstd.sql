CREATE TABLE `p_nonstd` (
  `PID` varchar(38) NOT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `SEQ` int(10) NOT NULL,
  KEY `pid` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

