CREATE TABLE `p_party` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `FilterF` varchar(1) DEFAULT NULL,
  `MaterialE` varchar(2) DEFAULT NULL,
  `RimMaterial` varchar(1) DEFAULT NULL,
  `RimThick` varchar(1) DEFAULT NULL,
  `PlankM` varchar(1) DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `ResiFlag` varchar(1) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `BeginResi` double DEFAULT NULL,
  `AverResi` double DEFAULT NULL,
  `EndResi` double DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

