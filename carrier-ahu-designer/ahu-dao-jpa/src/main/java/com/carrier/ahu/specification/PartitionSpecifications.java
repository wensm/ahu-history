package com.carrier.ahu.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.carrier.ahu.common.entity.Partition;

/**
 * Created by Braden Zhou on 2018/09/29.
 */
public final class PartitionSpecifications {

    public static Specification<Partition> partitionsOfUnit(String unitid) {
        return new Specification<Partition>() {
            @Override
            public Predicate toPredicate(Root<Partition> partition, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<String> path = partition.get("unitid");
                query.where(cb.equal(path, unitid));
                return null;
            }
        };
    }

}
