package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.BatchCfgProgress;

/**
 * Created by Simon 2017/12/09.
 */
public interface BatchCfgProgressDao
		extends PagingAndSortingRepository<BatchCfgProgress, String>, JpaSpecificationExecutor<BatchCfgProgress> {
}
