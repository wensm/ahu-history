package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.GroupInfo;

/**
 * Created by Wen zhengtao on 2017/3/25.
 */
public interface GroupDao extends PagingAndSortingRepository<GroupInfo,String>,JpaSpecificationExecutor<GroupInfo> {
}
