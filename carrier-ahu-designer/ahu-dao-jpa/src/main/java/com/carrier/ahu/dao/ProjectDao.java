package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.Project;

/**
 * Created by Wen zhengtao on 2017/3/24.
 */
public interface ProjectDao extends PagingAndSortingRepository<Project,String>,JpaSpecificationExecutor<Project> {
}
