package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.MatericalConstance;

/**
 * Created by zhang zhiqiang on 2017/6/22.
 */
public interface MatericalConstanceDao
		extends PagingAndSortingRepository<MatericalConstance, String>, JpaSpecificationExecutor<MatericalConstance> {
}
