package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.AhuTemplate;

public interface AhuTemplateDao extends PagingAndSortingRepository<AhuTemplate, String>,JpaSpecificationExecutor<AhuTemplate> {
	
}
