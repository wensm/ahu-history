package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.LoginoutLog;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
public interface LogDao extends PagingAndSortingRepository<LoginoutLog,String>,JpaSpecificationExecutor<LoginoutLog> {
}
