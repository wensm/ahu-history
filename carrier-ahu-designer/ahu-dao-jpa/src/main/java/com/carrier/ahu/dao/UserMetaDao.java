package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.entity.UserMetaId;

/**
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
public interface UserMetaDao extends PagingAndSortingRepository<UserMeta, UserMetaId>, JpaSpecificationExecutor<UserMeta> {
}
