package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.Unit;

/**
 * Created by Wen zhengtao on 2017/3/24.
 */
public interface AhuDao extends PagingAndSortingRepository<Unit,String>,JpaSpecificationExecutor<Unit> {
}
