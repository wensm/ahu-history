package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.po.meta.AirParam;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Api(description = "系统默认参数")
@RestController
public class DefaultParamController extends AbstractController {

	@RequestMapping(value = "defaultparams", method = RequestMethod.GET)
	public Map<String, Object> getDefaultParams() {
		Map<String, Object> result = new HashMap<String, Object>();
		String filePath = System.getProperty(RestConstant.SYS_PROPERTY_USER_DIR).concat(RestConstant.SYS_PATH_MAIN_RESOURCES)
				.concat(FileNamesLoadInSystem.DEFAULT_PARAMS_JSON);
		File file = new File(filePath);
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)));
			StringBuffer buffer = new StringBuffer();
			String line = RestConstant.SYS_BLANK;
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			JSONObject jsonObject = JSON.parseObject(buffer.toString());
			result.put(RestConstant.SYS_STRING_DATA, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (EmptyUtil.isNotEmpty(in)) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		return result;
	}

	@RequestMapping(value = "defaultparams", method = RequestMethod.POST)
	public Map<String, Object> saveDefaultParams(AirParam airParam) {
		Map<String, Object> result = new HashMap<String, Object>();
		String filePath = System.getProperty(RestConstant.SYS_PROPERTY_USER_DIR).concat(FileNamesLoadInSystem.DEFAULT_AIR_PARAM_JSON);
		FileWriter fw = null;
		PrintWriter out = null;
		try {
			fw = new FileWriter(filePath);
			out = new PrintWriter(fw);
			JSONObject json = (JSONObject) JSONObject.toJSON(airParam);
			out.write(json.toString());
			out.println();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (fw!=null) {
					fw.close();
				}
				if (out!=null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return result;
	}
}
