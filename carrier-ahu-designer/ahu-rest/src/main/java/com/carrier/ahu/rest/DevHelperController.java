package com.carrier.ahu.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.metadata.section.ValueOption;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.layout.MetaLayout;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.section.meta.MetaLayoutGen.MetaParameterWithUnit;
import com.carrier.ahu.section.meta.devhelp.DevHelperInCommon;
import com.carrier.ahu.vo.ApiResult;

/**
 * Restful API接口清单 Created by Wen zhengtao on 2017/3/7.
 */
@RestController
public class DevHelperController extends AbstractController {

	/**
	 * 刷新属性值的元数据
	 */
	@RequestMapping(value = "/dev/reportintl", method = RequestMethod.GET)
	public ApiResult<List<Map<String, String>>> genReportIntl() {
		List<Map<String, String>> result = new ArrayList<>();
		result.add(DevHelperInCommon.getInstance().getCkMap());
		result.add(DevHelperInCommon.getInstance().getKcMap());
		result.add(DevHelperInCommon.getInstance().getKeMap());
		return ApiResult.success(result);
	}

	/**
	 * 刷新属性值的元数据
	 */
	@RequestMapping(value = "/dev/calbyunit", method = RequestMethod.GET)
	public ApiResult<String[]> calculateByUnit(String value, String parameterKey, UnitSystemEnum unitType) {
		String[] result = AhuSectionMetas.getInstance().getValueByUnit(value, parameterKey, unitType);
		return ApiResult.success(result);
	}
	
	
	/**
	 * 刷新属性值的元数据
	 */
	@RequestMapping(value = "/dev/refreshmeta", method = RequestMethod.GET)
	public ApiResult<String> refreshMetadata() {
		// MetaValueGen.reloadMeta();
		// AhuSectionMetas.reloadSectionMeta();
		return ApiResult.success();
	}
	
	

	/**
	 * 自动生成属性值的常量
	 * 
	 * @return
	 */
	@RequestMapping(value = "/dev/genmvalues", method = RequestMethod.GET)
	public ApiResult<List<String>> generateMetaValueConstances() {

		Map<String, MetaLayout> map = AhuSectionMetas.getInstance().getAhuAndSectionMetaLayoutWithDetail(null);
		List<String> list = new ArrayList<String>();
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			MetaLayout layout = map.get(key);
			Iterator<MetaParameter> it1 = layout.getGroups().get(0).getParas().iterator();
			while (it1.hasNext()) {
				MetaParameter para = it1.next();
				if (para instanceof MetaParameterWithUnit) {
					MetaParameterWithUnit parau = (MetaParameterWithUnit) para;
					ValueOption option = parau.getOption();
					if (option != null && option.getOptions() != null && option.getOptions().length > 0) {
						String[] options = option.getOptions();
						String optionKey = parau.getKey();

						for (int i = 0; i < options.length; i++) {
							String value = options[i];
							String outputStr = MetaCodeGen.getFieldDefStr(optionKey, value);
							System.out.println(outputStr);
							list.add(outputStr);
						}

					}
				}

			}
		}
		return ApiResult.success(list);
	}

}
