package com.carrier.ahu.util;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_SPRAYHUMIDIFIER_NOZZLEN;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_STEAMHUMIDIFIER_VAPORPRESSURE;

/**
 * Created by liangd4 on 2018/1/18.
 */
public class DataValidationUtil {

    public static void getPerformDataValidation(Map<String, Object> map, String sectionId, LanguageEnum languageEnum) {
        String fanModel = RestConstant.METASEXON_FAN_FANMODEL;//风机标识
        /*冷水盘管*/
        String coolingCoilRows = RestConstant.METASEXON_COOLINGCOIL_ROWS;//排数
        String coolingCoilCircuit = RestConstant.METASEXON_COOLINGCOIL_CIRCUIT;//回路
        String coolingCoilFinDensity = RestConstant.METASEXON_COOLINGCOIL_FINDENSITY;//片距
        String coolingCoilSReturnColdQ = RestConstant.METASEXON_COOLINGCOIL_SRETURNCOLDQ;//冷量
        String coolingCoilWreturnHeatQ = RestConstant.METASEXON_COOLINGCOIL_WRETURNHEATQ;//热量
        String coolingCoilEnableWinter = RestConstant.METASEXON_COOLINGCOIL_ENABLEWINTER;//冬季生效

        /*热水盘管*/
        String heatingCoilRows = RestConstant.METASEXON_HEATINGCOIL_ROWS;// 排数
        String heatingCoilCircuit = RestConstant.METASEXON_HEATINGCOIL_CIRCUIT;//回路
        String heatingCoilFinDensity = RestConstant.METASEXON_HEATINGCOIL_FINDENSITY;//片距
        String heatingCoilWreturnHeatQ = RestConstant.METASEXON_HEATINGCOIL_WRETURNHEATQ;//热量
        String heatingCoilSreturnColdQ = RestConstant.METASEXON_HEATINGCOIL_SRETURNCOLDQ;//冷量
        String heatingCoilEnableSummer = RestConstant.METASEXON_HEATINGCOIL_ENABLESUMMER;//夏季生效

        /*直接蒸发式*/
        String directExpensionCoilRows = RestConstant.METASEXON_DXCOIL_ROWS;// 排数
        String directExpensionCoilCircuit = RestConstant.METASEXON_DXCOIL_CIRCUIT;//回路
        String directExpensionCoilFinDensity = RestConstant.METASEXON_DXCOIL_FINDENSITY;//片距
        String directExpensionCoilSreturnColdQ = RestConstant.METASEXON_DXCOIL_SRETURNCOLDQ;//冷量

        ErrorCode message = ErrorCode.DO_CALCULATION_BEFORE_CONFIRM;
        ErrorCode messageSelect = ErrorCode.DO_CALCULATION_BEFORE_SELECT_CONFIRM;
        ErrorCode messageSummer = ErrorCode.DO_CALCULATION_SUMMER_BEFORE_CONFIRM;
        ErrorCode messageWinter = ErrorCode.DO_CALCULATION_WINTER_BEFORE_CONFIRM;
        if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {//风机效验
            if (map.containsKey(fanModel)) {
                Object value = map.get(fanModel);//获取风机型号
                if (EmptyUtil.isEmpty(value)) {
                    throw new ApiException(message);
                }
            }
        } else if (SectionTypeEnum.TYPE_COLD.getId().equals(sectionId)) {
            if (map.containsKey(coolingCoilRows)) {
                String valuerows = BaseDataUtil.constraintString(map.get(coolingCoilRows));
                if (valuerows.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(coolingCoilCircuit)) {
                String valueCircuit = BaseDataUtil.constraintString(map.get(coolingCoilCircuit));
                if (valueCircuit.equals(SystemCalculateConstants.COOLINGCOIL_CIRCUIT_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(coolingCoilFinDensity)) {
                String valueFinDensity = BaseDataUtil.constraintString(map.get(coolingCoilFinDensity));
                if (valueFinDensity.equals(SystemCalculateConstants.COOLINGCOIL_FINDENSITY_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(coolingCoilEnableWinter)) {//如果勾选冬季并
                String value = BaseDataUtil.constraintString(map.get(coolingCoilEnableWinter));
                if (RestConstant.SYS_ASSERT_TRUE.equals(value)) {//热量必须大于0
                    String valueWreturnHeatQ = BaseDataUtil.constraintString(map.get(coolingCoilWreturnHeatQ));
                    if (EmptyUtil.isEmpty(valueWreturnHeatQ) || BaseDataUtil.stringConversionInteger(valueWreturnHeatQ) < 0) {
                        throw new ApiException(messageWinter);
                    }
                }
            }
            if (map.containsKey(coolingCoilSReturnColdQ)) {
                String valueFinScoldQ = BaseDataUtil.constraintString(map.get(coolingCoilSReturnColdQ));
                if (EmptyUtil.isEmpty(valueFinScoldQ) || BaseDataUtil.stringConversionInteger(valueFinScoldQ) < 0) {
                    throw new ApiException(messageSummer);
                }
            }
        } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionId)) {
            if (map.containsKey(directExpensionCoilRows)) {
                String valuerows = BaseDataUtil.constraintString(map.get(directExpensionCoilRows));
                if (valuerows.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_ROWS_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(directExpensionCoilCircuit)) {
                String valueCircuit = BaseDataUtil.constraintString(map.get(directExpensionCoilCircuit));
                if (valueCircuit.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_CIRCUIT_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(directExpensionCoilFinDensity)) {
                String valueFinDensity = BaseDataUtil.constraintString(map.get(directExpensionCoilFinDensity));
                if (valueFinDensity.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_FINDENSITY_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(directExpensionCoilSreturnColdQ)) {
                String valueFinScoldQ = BaseDataUtil.constraintString(map.get(directExpensionCoilSreturnColdQ));
                if (EmptyUtil.isEmpty(valueFinScoldQ) || BaseDataUtil.stringConversionInteger(valueFinScoldQ) < 0) {
                    throw new ApiException(message);
                }
            }
        } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionId)) {
            if (map.containsKey(heatingCoilRows)) {
                String valuerows = BaseDataUtil.constraintString(map.get(heatingCoilRows));
                if (valuerows.equals(SystemCalculateConstants.HEATINGCOIL_ROWS_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(heatingCoilCircuit)) {
                String valueCircuit = BaseDataUtil.constraintString(map.get(heatingCoilCircuit));
                if (valueCircuit.equals(SystemCalculateConstants.HEATINGCOIL_CIRCUIT_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(heatingCoilFinDensity)) {
                String valueFinDensity = BaseDataUtil.constraintString(map.get(heatingCoilFinDensity));
                if (valueFinDensity.equals(SystemCalculateConstants.HEATINGCOIL_FINDENSITY_AUTO)) {
                    throw new ApiException(message);
                }
            }
            if (map.containsKey(heatingCoilEnableSummer)) {//如果勾选夏季
                String value = BaseDataUtil.constraintString(map.get(heatingCoilEnableSummer));
                if (RestConstant.SYS_ASSERT_TRUE.equals(value)) {//冷量必须大于0
                    String valueSreturnColdQ = BaseDataUtil.constraintString(map.get(heatingCoilSreturnColdQ));
                    if (EmptyUtil.isEmpty(valueSreturnColdQ) || BaseDataUtil.stringConversionInteger(valueSreturnColdQ) < 0) {
                        throw new ApiException(messageSummer);
                    }
                }
            }
            if (map.containsKey(heatingCoilWreturnHeatQ)) {
                String returnHeatQ = BaseDataUtil.constraintString(map.get(heatingCoilWreturnHeatQ));
                if (EmptyUtil.isEmpty(returnHeatQ) || BaseDataUtil.stringConversionInteger(returnHeatQ) < 0) {
                    throw new ApiException(messageWinter);
                }
            }
        } else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionId)) {
            if (map.containsKey(METASEXON_STEAMHUMIDIFIER_VAPORPRESSURE)) {
                Object value = map.get(METASEXON_STEAMHUMIDIFIER_VAPORPRESSURE);
                if (EmptyUtil.isEmpty(value)) {
                    throw new ApiException(messageSelect);
                }
            }
        } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionId)) {
            if (map.containsKey(METASEXON_SPRAYHUMIDIFIER_NOZZLEN)) {
                Object value = map.get(METASEXON_SPRAYHUMIDIFIER_NOZZLEN);
                if (EmptyUtil.isEmpty(value)) {
                    throw new ApiException(messageSelect);
                }
            }
        }
    }
}
