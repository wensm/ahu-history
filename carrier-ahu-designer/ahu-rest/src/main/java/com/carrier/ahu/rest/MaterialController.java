package com.carrier.ahu.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.MaterialVO;

import io.swagger.annotations.Api;

/**
 * 材料批量修改
 */
@Api(description = "材料批量修改rest接口")
@RestController
@RequestMapping(value = "material")
public class MaterialController extends AbstractController {

	@Autowired
	private AhuService ahuService;
	@Autowired
	private SectionService sectionService;

	@RequestMapping(value = "/ahu/update", method = RequestMethod.POST)
	@ResponseBody
	public ApiResult<Map<String, Object>> materailBatchUpdate(MaterialVO materialVO) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Unit> units = new ArrayList<Unit>();
		if (StringUtils.isNotBlank(materialVO.getProjectId())) {
			units = ahuService.findAhuList(materialVO.getProjectId(), null);
		} else {
			units = ahuService.findAhuList(null, materialVO.getGroupId());
		}
		Map<String, Object> materialMap = getJsonMap(materialVO.getMaterialStr());
		List<Part> parts = new ArrayList<Part>();
		for (Unit unit : units) {
			Map<String, Object> metaMap = getJsonMap(unit.getMetaJson());
			for (String materialKey : materialMap.keySet()) {
				if (metaMap.containsKey(materialKey)) {
					metaMap.put(materialKey, materialMap.get(materialKey));
				}
			}
			String ahuStr = JSONObject.toJSONString(metaMap);
			unit.setMetaJson(ahuStr);
			// unit = TemplateUtil.getTemplateUnit(unit);
			UnitUtil.genSerialAndProduct(unit);
			ahuService.updateAhu(unit, getUserName());
			List<Part> parts2 = sectionService.findSectionList(unit.getUnitid());
			if (parts2 != null && parts2.size() > 0) {
				parts.addAll(parts2);
			}

		}
		for (Part part : parts) {
			Map<String, Object> metaMap = getJsonMap(part.getMetaJson());
			for (String materialKey : materialMap.keySet()) {
				if (metaMap.containsKey(materialKey)) {
					metaMap.put(materialKey, materialMap.get(materialKey));
				}
			}
			String sectionStr = JSONObject.toJSONString(metaMap);
			part.setMetaJson(sectionStr);
			sectionService.updateSection(part, getUserName());
		}

		return ApiResult.success(result);
	}

	private Map<String, Object> getJsonMap(String json) {
		JSONObject jsonObject = JSONObject.parseObject(json);
		Map<String, Object> map = new HashMap<String, Object>();
		map = jsonObject;
		return map;
	}
}
