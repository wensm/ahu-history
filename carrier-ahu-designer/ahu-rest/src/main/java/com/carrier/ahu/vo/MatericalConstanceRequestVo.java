package com.carrier.ahu.vo;

import lombok.Data;

/**
 * Created by liujianfeng on 2017/6/20.
 */
@Data
public class MatericalConstanceRequestVo {
	private String pid;
	private String projid;
	private String groupid;
	private String prokey;

}
