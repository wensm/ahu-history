package com.carrier.ahu.rest.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import com.carrier.ahu.constant.RestConstant;

/**
 * 通过EnableWebSocketMessageBroker 开启使用STOMP协议来传输基于代理(messagebroker)的消息
 * 此时浏览器支持使用@MessageMapping 就像支持@RequestMapping一样。
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}

	/**
	 * endPoint 注册协议节点,并映射指定的URl
	 */
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(RestConstant.SYS_STOMP_ENDPOINT_ADD).withSockJS();// 注册一个Stomp 协议的endpoint,并指定 SockJS协议。
	}

	/**
	 * 配置消息代理(message broker)
	 */
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker(RestConstant.SYS_STOMP_SIMPLEBROKER_ENABLE); // 广播式应配置一个/topic 消息代理
		// 点对点式增加一个/queue 消息代理
		// registry.enableSimpleBroker("/queue", "/topic");
	}
}
