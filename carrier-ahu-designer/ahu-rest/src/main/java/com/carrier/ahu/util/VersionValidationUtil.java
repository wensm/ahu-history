package com.carrier.ahu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Value;

import com.carrier.ahu.unit.BaseDataUtil;

public class VersionValidationUtil {
	/**
	 * 读取yml文件中的版本信息
	 */
	@Value("${ahu.version}")
    private String ahuVersion;
	
    /**
     * 验证导入版本
     *
     * @param	version 导入版本
     * @param	supportVersion 支持版本区间
     * @return
     */
    public static boolean packageProjectValidation(String version, String supportVersion) {
    	if (version.length() >= 5) {//目前支持大版本号一致
    		int versionInt = BaseDataUtil.stringConversionInteger(version.replaceAll("\\.", ""));
            for (String supportString : supportVersion.split(",")) {
            	int beginInt = BaseDataUtil.stringConversionInteger(supportString.split("-")[0].replaceAll("\\.", ""));
            	int endInt = BaseDataUtil.stringConversionInteger(supportString.split("-")[1].replaceAll("\\.", ""));
                if (versionInt >= beginInt && versionInt <= endInt) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return true;
    }
    
    public static boolean TemplateValidation(String version, String supportVersion) {
    	if (version.length() >= 5) {//目前支持大版本号一致
    		int versionInt = BaseDataUtil.stringConversionInteger(version.replaceAll("\\.", ""));
            for (String supportString : supportVersion.split(",")) {
            	int beginInt = BaseDataUtil.stringConversionInteger(supportString.split("-")[0].replaceAll("\\.", ""));
            	int endInt = BaseDataUtil.stringConversionInteger(supportString.split("-")[1].replaceAll("\\.", ""));
                if (versionInt >= beginInt && versionInt <= endInt) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }
    
	/**
	 * 在excel模板中增加sheet页标记当前版本号。
	 * @param filePath
	 * @param fileName
	 * @param ahuVersion
	 */
	public static void markAtTemplate(String filePath, String fileName, String ahuVersion) {
		Workbook wb = null;
		File file = new File(filePath);
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(file);
			if (ExcelUtils.isExcel2003(fileName)) {
				wb = ExcelUtils.openExcelByPOIFSFileSystem(inputStream);
			} else {
				wb = ExcelUtils.openExcelByFactory(inputStream);
			}

			Sheet sheet = wb.getSheet("version_control");// 读取sheet页
			// if (EmptyUtil.isNotEmpty(sheet)) {//如果version_control的sheet存在，就读取并覆盖当前版本
			// Row row0 = sheet.getRow(0);// 获取第一行
			// row0.createCell(0).setCellValue(ahuVersion);//设置版本号
			// }else {//不存在，就新增
			Sheet sheet1 = wb.createSheet("version_control");
			Row row0 = sheet1.createRow(0);// 新增一行
			row0.createCell(0).setCellValue(ahuVersion);// 设置版本号
			// }
			FileOutputStream out = new FileOutputStream(filePath);
			wb.write(out);
			out.close();
			wb.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * 读取excel模板中版本sheet页中的版本信息。
     * @param inputStream
     * @param isExcel2003
     * @return
     * @throws Exception
     */
	public static String readMarkFromTemplate(InputStream inputStream, String fileName) {

		String version = "";
		Workbook wb = null;
		try {
			if (ExcelUtils.isExcel2003(fileName)) {
				wb = ExcelUtils.openExcelByPOIFSFileSystem(inputStream);
			} else {
				wb = ExcelUtils.openExcelByFactory(inputStream);
			}
			Sheet sheet = wb.getSheet("version_control");// 读取sheet页
			if (EmptyUtil.isEmpty(sheet)) {// 旧版本可能没有version_control的sheet页，默认返回1.0.0版本
				version = "1.0.0";
			} else {
				Row row0 = sheet.getRow(0);// 获取第一行
				String celll0 = ExcelCellReader.getCellContext(row0.getCell(0));// 获取第一个cell
				if (EmptyUtil.isNotEmpty(celll0)) {
					version = celll0;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return version;
	}
}
