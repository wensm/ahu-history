package com.carrier.ahu;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Map;

import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityFilterAutoConfiguration;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpEncodingAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebClientAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketMessagingAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.util.LOCALMAC;
import com.carrier.ahu.common.util.NetworkUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.interceptor.AhuLoggerInterceptor;
import com.carrier.ahu.license.LicenseRegister;
import com.carrier.ahu.listener.AhuUI;
import com.carrier.ahu.vo.SysConstants;

/**
 * ahu应用入口类 1,启动参数请加入-Djava.awt.headless=false <br>
 * 
 * <br>
 * Remove @SpringBootApplication and use @Configuraiton with @Import instead, so
 * that it will avoid other useless configuration scans when start up. To enable
 * H2 console and Swagger, use Program Argument: --spring.profiles.active=dev
 * when in development environment.
 */
//@formatter:off
@Configuration
@ComponentScan("com.carrier.ahu")
@EntityScan("com.carrier.ahu.common.entity")
@EnableJpaRepositories("com.carrier.ahu.dao")
@Import({
    DataSourceAutoConfiguration.class,
    DispatcherServletAutoConfiguration.class,
    ErrorMvcAutoConfiguration.class,
    EmbeddedServletContainerAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class,
    HttpEncodingAutoConfiguration.class,
    HttpMessageConvertersAutoConfiguration.class,
    JacksonAutoConfiguration.class,
    MultipartAutoConfiguration.class,
    SecurityAutoConfiguration.class,
    SecurityFilterAutoConfiguration.class,
    ServerPropertiesAutoConfiguration.class,
    WebClientAutoConfiguration.class,
    WebMvcAutoConfiguration.class,
    WebSocketAutoConfiguration.class,
    WebSocketMessagingAutoConfiguration.class,
    PropertyPlaceholderAutoConfiguration.class
})
//@formatter:on
@EnableAsync
public class AHUApplication extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        if (NetworkUtils.isPortInUse(RestConstant.SYS_DEFAULT_PORTAL)) {
            AhuUI.openFrontendWindow();
            AhuUI.showMessageAndExit(AHUContext.getIntlString(I18NConstants.AHU_HAS_STARTED, Locale.getDefault()));
        } else {
            try {
                String mac = LOCALMAC.getMAC();
                System.out.println("本机MAC地址：" + mac);
                Map<String, String> map = System.getenv();
                String userName = map.get(RestConstant.SYS_MAP_USERNAME);// 获取用户名
                String computerName = map.get(RestConstant.SYS_MAP_COMPUTERNAME);// 获取计算机名
                String userDomain = map.get(RestConstant.SYS_MAP_USERDOMAIN);// 获取计算机域名
                System.out.println("本机userName：" + userName);
                System.out.println("本机computerName：" + computerName);
                System.out.println("本机userDomain：" + userDomain);
                String localValue = LicenseRegister.getLocalValue(mac, userName, userDomain);
                // TODO LicenseRegister
                String target = LicenseRegister.publicKeyDecode(RestConstant.SYS_BLANK);
                System.out.println("注册验证是否通过：" + LicenseRegister.verify(target, localValue));
            } catch (UnknownHostException | SocketException e) {
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.setProperty(RestConstant.SYS_PROPERTY_JNA_DEBUG_LOAD, RestConstant.SYS_ASSERT_TRUE);
            // System.setProperty(RestConstant.SYS_PROPERTY_JNA_LIBRARY_PATH,
            // RestConstant.SYS_PROPERTY_ASSERTS_DLL);
            System.setProperty(RestConstant.SYS_PROPERTY_JAVA_AWT_HEADLESS, RestConstant.SYS_ASSERT_FALSE);

            SpringApplicationBuilder app = new SpringApplicationBuilder(AHUApplication.class);
            app.build().addListeners(new ApplicationPidFileWriter(SysConstants.SHUTDOWN_PID));
            app.run(args);
        }
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AhuLoggerInterceptor());
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer matcher) {
        matcher.setUseRegisteredSuffixPatternMatch(true);// 关闭@RequestMapping后缀模式匹配,防止url 尾部包含小数点被忽略问题
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // System.out.println("add rule");
        registry.addResourceHandler(RestConstant.SYS_PATH_REGISTRY_RESOURCE_HANDLER)
                .addResourceLocations(RestConstant.SYS_PATH_REGISTRY_RESOURCE_LOCATION);
    }

}