package com.carrier.ahu.rest;

import static com.carrier.ahu.common.intl.I18NConstants.COLLECTING;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATE_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATE_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATING;
import static com.carrier.ahu.common.intl.I18NConstants.REPORT_GENERATING;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_INFORMATION;
import static com.carrier.ahu.common.intl.I18NConstants.SPLIT_SECTION;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.vo.SysConstants.COLON;
import static com.carrier.ahu.vo.SystemCalculateConstants.PARTITOIN_POSITION_BOTTOM;
import static com.carrier.ahu.vo.SystemCalculateConstants.PARTITOIN_POSITION_TOP;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelCalculationObj;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.engine.cad.AhuExporter;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.Report;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.report.ReportExcelCRMList;
import com.carrier.ahu.report.ReportExcelDelivery;
import com.carrier.ahu.report.ReportExcelProject;
import com.carrier.ahu.report.ReportExcelProject.ProjAhu;
import com.carrier.ahu.report.ReportRequestVob;
import com.carrier.ahu.report.SAPFunction;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.Excel4ReportUtils;
import com.carrier.ahu.util.FileCopyUtils;
import com.carrier.ahu.util.MetaSelectUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.PDF4ReportUtils;
import com.carrier.ahu.util.ValueFormatUtil;
import com.carrier.ahu.util.Word4ReportUtils;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.SysConstants;

/**
 * Async generate report task.
 * 
 * @author Braden Zhou
 *
 */
@Component
public class GenerateReportTask {

    protected static Logger logger = LoggerFactory.getLogger(GenerateReportTask.class);

    @Autowired
    private AhuService ahuService;
    @Autowired
    private SectionService sectionService;
    @Autowired
    private PartitionService partitionService;
    @Autowired
    private AhuExporter exporter;

    @Async
    public void generateReport(ReportRequestVob reportRequest, Project project) {
        CalContextUtil.reload();
        CalContextUtil.sendMessage(project.getPid(), AHUContext.getIntlString(REPORT_GENERATING));

        ReportData reportData = new ReportData();
        reportData.setProject(project);
        populatePartition(project.getPid(), reportRequest.getAhuIds(), reportData);
        populatePartData(reportData);
        List<Report> generatedReports = generateReportFile(reportRequest.getReports(), reportData);
        reportGenerated(reportRequest.getProjectId(), reportRequest.getAhuIds(), generatedReports);
    }

    private void populatePartition(String projectId, String[] reportIds, ReportData reportData) {
        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                AHUContext.getIntlString(COLLECTING) + COLON + AHUContext.getIntlString(SPLIT_SECTION));

        List<Unit> projectUnitList = ahuService.findAhuList(projectId, null);
        Map<String, Partition> partitionMap = new HashMap<>();

        List<Unit> reportUnitList = new ArrayList<>();
        if (EmptyUtil.isEmpty(reportIds)) {
            for (Unit unit : projectUnitList) {// TODO 面板布置结束才可以导出
                if (AhuStatusEnum.SELECT_COMPLETE.getId().equals(unit.getRecordStatus())) {
                    reportUnitList.add(unit);
                    Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                    partitionMap.put(unit.getUnitid(), partition);
                }
            }
        } else {
            List<String> reportIdList = Arrays.asList(reportIds);
            for (Unit unit : projectUnitList) {
                if (reportIdList.contains(unit.getUnitid())
                        && AhuStatusEnum.SELECT_COMPLETE.getId().equals(unit.getRecordStatus())) {
                    reportUnitList.add(unit);
                    Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                    partitionMap.put(unit.getUnitid(), partition);
                }
            }
        }
        Collections.sort(reportUnitList, new Comparator<Unit>() {
            public int compare(Unit u1, Unit u2) {
                return u1.getUnitNo().compareTo(u2.getUnitNo());
            }
        });
        reportData.setUnitList(reportUnitList);
        reportData.setPartitionMap(partitionMap);
    }

    private void populatePartData(ReportData reportData) {
        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                AHUContext.getIntlString(COLLECTING) + COLON + AHUContext.getIntlString(SECTION_INFORMATION));

        Map<String, PartPO> partMap = new LinkedHashMap<String, PartPO>();
        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = sectionService.findLinkedSectionsList(unit.getUnitid());
            for (PartPO partPO : partList) {
                Part part = partPO.getCurrentPart();
                partMap.put(UnitConverter.genUnitKey(unit, part), partPO);
            }
        }
        reportData.setPartMap(partMap);
    }

    private List<Report> generateReportFile(Report[] reports, ReportData reportData) {
        List<Report> returnReportList = new ArrayList<>();
        String path = SysConstants.ASSERT_DIR + SysConstants.REPORT_DIR + reportData.getProject().getPid();
        for (Report report : reports) {
            String type = report.getType();
            if (report.isOutput()) {
                if (SysConstants.EXCEL.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generateExcel(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate excel report: " + report.getName(), e);
                    }
                } else if (SysConstants.PDF.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generatePdf(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate pdf report: " + report.getName(), e);
                    }
                } else if (SysConstants.WORD.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generateWord(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate pdf report: " + report.getName(), e);
                    }
                }
            }
        }
        return returnReportList;
    }

    private void generateExcel(Report report, String path, ReportData reportData) throws Exception {
        String filePath = path + "/" + report.getName() + SysConstants.EXCEL2007_EXTENSION;
        String excelClassify = report.getClassify();
        if ("prolist".equals(excelClassify)) {
            generateExcelProlist(filePath, reportData);
        } else if ("paralist".equals(excelClassify)) {
            generateExcelParalist(report, filePath, reportData);
        } else if ("delivery".equals(excelClassify)) {
            this.generateExcellDelivery(filePath, reportData);
        } else if ("crmlist".equals(excelClassify)) {
            this.generateExcellCrmList(filePath, reportData);
        } else if ("saplist".equals(excelClassify)) {
            filePath = path + "/" + report.getName() + SysConstants.ZIP_EXTENSION;
            this.generateExcelSAPPriceCode(filePath, reportData);
        } else if ("dwglist".equals(excelClassify)) {
            filePath = path + "/" + report.getName() + SysConstants.ZIP_EXTENSION;
            this.generateDWGDrawing(filePath, reportData);
        } else {
            logger.warn("Report Excel Document Classify Is Not Exist: " + excelClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void generateExcellCrmList(String filePath, ReportData reportData) throws IOException {
        List<ReportExcelCRMList> crmList = new ArrayList<>();
        for (Unit unit : reportData.getUnitList()) {
            ProjAhu proAhu = transUnit2ProjAhu(unit);
            try {
                ReportExcelCRMList reprotCrmList = new ReportExcelCRMList();
                reprotCrmList.setUnitId(unit.getUnitid());
                reprotCrmList.setUnitType(unit.getProduct());
                if("true".equals(proAhu.getDeformation())){//crm模板导出型号使用变形后型号
                    reprotCrmList.setSerial(proAhu.getDeformationSerial());
                }else{
                    reprotCrmList.setSerial(unit.getSeries());
                }

                reprotCrmList.setMount(unit.getMount());
                reprotCrmList.setDrawingNo(unit.getDrawingNo());
                reprotCrmList.setFacePrice(BaseDataUtil.doubleConversionInteger(unit.getPrice()));
                reprotCrmList.setProductionPlace(reportData.getProject().getAddress());
                String contract = reportData.getProject().getContract();
                if(EmptyUtil.isNotEmpty(contract)){
                    reprotCrmList.setContract(contract.substring(0, contract.length() - 1));
                    reprotCrmList.setSelectionTimes(contract.substring(contract.length() - 1));
                }
                crmList.add(reprotCrmList);
            } catch (Exception e) {
                logger.warn("报表-参数列表-CRM清单信息整合-异常, UnitId: " + unit.getUnitid(), e);
                continue;
            }
        }
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_CRMLIST
                : RestConstant.SYS_PATH_TEMPLATE_PATH_CRMLIST_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELLCRMLIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);

        Collections.sort(crmList, new Comparator<ReportExcelCRMList>() {
            public int compare(ReportExcelCRMList o1, ReportExcelCRMList o2) {
                return ListUtils.addZero2Str(Integer.parseInt(o1.getDrawingNo()),5).compareTo(ListUtils.addZero2Str(Integer.parseInt(o2.getDrawingNo()),5));
            }
        });

        for(ReportExcelCRMList crm : crmList) {
            int i = Integer.parseInt(crm.getDrawingNo());
            crm.setContractSerial(crm.getSerial()
                    +(EmptyUtil.isNotEmpty(crm.getContract())?crm.getContract():"")
                    +(EmptyUtil.isNotEmpty(crm.getSelectionTimes())?crm.getSelectionTimes():"")
                    +ListUtils.addZero2Str(i,3));
        }
        Excel4ReportUtils.generateExcelCRMList(tempDest, crmList);
    }

    private void generateExcelSAPPriceCode(String filePath, ReportData reportData) throws IOException {
        List<File> unitSAPFiles = new ArrayList<>();
        
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<Part> parts = sectionService.findSectionList(unit.getUnitid());
                Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                List<PanelCalculationObj> casingList = AhuPartitionGenerator.getCasinglist(unit.getSeries(),unit, partition,false);
                String partitionJson = partition.getPartitionJson();
                List<AhuPartition> partitions = JSONArray.parseArray(partitionJson, AhuPartition.class);

                List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, partitions);
                PriceCodePO priceCodePo = priceCodes.remove(0); // move box to the end
                priceCodes.add(priceCodePo);

                AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
                List<SAPFunction> sapFunctions = new ArrayList<>();
                for (int i = 0; i < priceCodes.size(); i++) {
                    if (i == priceCodes.size() - 1) {
                        // process box
                        PriceCodePO priceCode = priceCodes.get(priceCodes.size() - 1);
                        SAPFunction sapFunction = new SAPFunction();
                        sapFunction.setFuncName(priceCode.getFuncNameWithoutNo());
                        sapFunction.setFuncCode(priceCode.getPriceCode());
                        sapFunction.setFuncList(0);
                        sapFunction.setInstSection(0);
                        sapFunction.setSectionPosition(String.valueOf(0));
                        sapFunction.setSectionWeight(String.valueOf(0));
                        sapFunctions.add(sapFunction);
                    } else {
                        Part part = parts.get(i);
                        PriceCodePO priceCode = priceCodes.get(i);
                        SAPFunction sapFunction = new SAPFunction();
                        sapFunction.setFuncName(priceCode.getFuncName());
                        sapFunction.setFuncCode(priceCode.getPriceCode());
                        sapFunction.setFuncList(priceCode.getSectionList());
                        sapFunction.setSectionPosition(getLayoutPosition(part, ahuLayout));
                        sapFunction.setSectionWeight(part.getMetaJson());
                        populatePosAndWeightOfPartitionOfPart(part, partitions, sapFunction);
                        sapFunctions.add(sapFunction);
                    }
                }

                File tempSource = new File(RestConstant.SYS_PATH_TEMPLATE_PATH_SAP_PRICE_CODE);
                // use customer PO number as the file name
                File tempDest = new File(new File(filePath).getParentFile(),
                        (EmptyUtil.isEmpty(unit.getCustomerName()) ? unit.getUnitid() : unit.getCustomerName())
                                + SYS_EXCEL2003_EXTENSION);
                if (tempDest.exists() && BACKUPFILE_SAPPRICECODE) {
                    FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
                }
                FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
                Excel4ReportUtils.generateExcelSAPPriceCode(tempDest, sapFunctions, casingList);
                unitSAPFiles.add(tempDest);
            } catch (Exception e) {
                logger.error("Failed to generate sap price report for unit: " + unit.getUnitid(), e);
            }
        }

        // generate zip file
        File zipFile = new File(filePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        FileUtil.writeZipFile(filePath, unitSAPFiles);
    }

    private void generateDWGDrawing(String filePath, ReportData reportData) throws IOException {
        List<File> dwgFiles = new ArrayList<>();
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<PartPO> partList = sectionService.findLinkedSectionsList(unit.getUnitid());
                Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(unit, partList, partition);

                String fileName = unit.getDrawingNo()
						+ (EmptyUtil.isEmpty(unit.getName()) ? RestConstant.SYS_BLANK
								: RestConstant.SYS_PUNCTUATION_LOW_HYPHEN + unit.getName())
						+ RestConstant.SYS_DWG_EXTENSION;
                String dwgFilePath = exporter.download(ahuParam, fileName);
                dwgFiles.add(new File(RestConstant.SYS_ASSERT_DIR + dwgFilePath));
            } catch (Exception e) {
                logger.error("Failed to generate dwg file for unit: " + unit.getUnitid(), e);
            }
        }

        // generate zip file
        File zipFile = new File(filePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        FileUtil.writeZipFile(filePath, dwgFiles);
    }

    private static String getLayoutPosition(Part part, AhuLayout ahuLayout) {
        int[][] layoutData = ahuLayout.getLayoutData();
        if (layoutData.length == 4) { // wheel/plate heat recycle
            for (int i = 0; i < layoutData.length; i++) {
                int[] layout = layoutData[i];
                for (int pos : layout) {
                    if (pos == part.getPosition()) {
                        return (i == 0 || i == 1) ? PARTITOIN_POSITION_TOP : PARTITOIN_POSITION_BOTTOM;
                    }
                }
            }
        }
        // B as default position
        return PARTITOIN_POSITION_BOTTOM;
    }

    private static void populatePosAndWeightOfPartitionOfPart(Part part, List<AhuPartition> partitions, SAPFunction sapFunction) {
        for (AhuPartition partition : partitions) {
            LinkedList<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                short posOfSection = AhuPartition.getPosOfSection(section);
                if (part.getPosition().equals(posOfSection)) {
                    sapFunction.setInstSection(partition.getPos() + 1); // start from 1
                    sapFunction.setSectionWeight(String.valueOf(
                            NumberUtil.scale(AhuPartitionUtils.calculatePartitionSectionWeight(partition), 2)));
                    return;
                }
            }
        }
        sapFunction.setInstSection(-1);
        sapFunction.setSectionWeight(String.valueOf(0));
    }

    private void generateExcelProlist(String filePath, ReportData reportData) throws IOException {
        ReportExcelProject reportExcelProject = new ReportExcelProject();
        reportExcelProject.setProject(reportData.getProject());
        List<ProjAhu> projAhus = transUnit2ProjAhu(reportData.getUnitList());
        reportExcelProject.setProjAhus(projAhus);

        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_PROJ
                : RestConstant.SYS_PATH_TEMPLATE_PATH_PROJ_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELPROLIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
        try {
            Excel4ReportUtils.generateExcelProlist(tempDest, reportExcelProject, reportData);
        } catch (Exception e) {
            logger.warn("报表-创建-[项目清单]-报表内容-异常, UnitId: " + reportData.getUnitList(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<ProjAhu> transUnit2ProjAhu(List<Unit> ahuToOutputList) {
        List<ProjAhu> projAhus = new ArrayList<>();
        for (Unit unit : ahuToOutputList) {
            ProjAhu proAhu = transUnit2ProjAhu(unit);
            projAhus.add(proAhu);
        }
        Collections.sort(projAhus, new Comparator<ProjAhu>() {
            public int compare(ProjAhu o1, ProjAhu o2) {
                return o1.getUnitId().compareTo(o2.getUnitId());
            }
        });
        return projAhus;
    }

    /**
     * 转换单个ProjAhu
     * @param unit
     * @return
     */
    private ProjAhu transUnit2ProjAhu(Unit unit) {
        String ahuMetaJson = unit.getMetaJson();
        Map<String, String> metaMap = JSON.parseObject(ahuMetaJson, HashMap.class);
        ProjAhu ahu = new ProjAhu();
        ahu.setUnitId(unit.getUnitNo());
        ahu.setSerial(unit.getSeries());
        ahu.setAhuName(unit.getName());
        Short mount = EmptyUtil.isEmpty(unit.getMount()) ? -1 : unit.getMount();
        ahu.setAmount(mount);

        // 装箱单价: 价格引擎计算（包含非标）
        Double packingPrice = EmptyUtil.isEmpty(unit.getPrice()) ? -1 : unit.getPrice();
        ahu.setPackingPrice(BaseDataUtil.doubleConversionInteger(packingPrice));

        // 非标单价：非标价格引擎计算
        Double nspackingPrice = EmptyUtil.isEmpty(unit.getNsprice()) ? -1 : unit.getNsprice();
        ahu.setNStdpackingPrice(BaseDataUtil.doubleConversionInteger(nspackingPrice));

        // 标准单价：价格引擎计算（不包含非标）
        ahu.setStdPrice(BaseDataUtil.doubleConversionInteger(packingPrice-nspackingPrice));

        // 总价格：装箱单价*个数
        ahu.setTotalPrice(BaseDataUtil.doubleConversionInteger(packingPrice * mount));

        ahu.setWeight(EmptyUtil.isEmpty(unit.getWeight()) ? -1 : BaseDataUtil.doubleConversionInteger(unit.getWeight()));

        ahu.setFactoryForm(MetaSelectUtil.getTextForSelect("meta.ahu.delivery", metaMap.get("meta.ahu.delivery"),
                AHUContext.getLanguage()));
        ahu.setDeformation(String.valueOf(metaMap.get("ns.ahu.deformation")));
        ahu.setDeformationSerial(String.valueOf(metaMap.get("ns.ahu.deformationSerial")));
        return ahu;
    }

    private void generateExcelParalist(Report report, String filePath, ReportData reportData) throws IOException {
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_PARA
                : RestConstant.SYS_PATH_TEMPLATE_PATH_PARA_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_PARALIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
        try {
            Excel4ReportUtils.generateExcelParalist(tempDest, report, reportData, AHUContext.getUnitSystem());
        } catch (Exception e) {
            logger.warn("报表-创建-[参数汇总]-报表内容-异常, UnitId: " + reportData.getUnitList(), e);
        }
    }

    private void generateExcellDelivery(String filePath, ReportData reportData) throws IOException {
        List<ReportExcelDelivery> deliveryList = new ArrayList<>();
        // TODO -Simon gen deliveryList
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<Part> partList = sectionService.findSectionList(unit.getUnitid());
                ReportExcelDelivery reprotDelivery = Excel4ReportUtils.generateReportExcelDelivery(unit, partList);
                if (null != reprotDelivery) {
                    deliveryList.add(reprotDelivery);
                }
            } catch (Exception e) {
                logger.warn("报表-参数列表-延长交货周期选项列表信息整合-异常, UnitId: " + unit.getUnitid(), e);
                continue;
            }
        }
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_DELI
                : RestConstant.SYS_PATH_TEMPLATE_PATH_DELI_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELLDELIVERY) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);

        Collections.sort(deliveryList, new Comparator<ReportExcelDelivery>() {
            public int compare(ReportExcelDelivery o1, ReportExcelDelivery o2) {
                return o1.getUnitId().compareTo(o2.getUnitId());
            }
        });

        Excel4ReportUtils.generateExcelDelivery(tempDest, deliveryList);
    }

    private void generatePdf(Report report, String path, ReportData reportData) throws Exception {
        LanguageEnum language = AHUContext.getLanguage();
        UnitSystemEnum unitType = AHUContext.getUnitSystem();

        String filePath = path + "/" + report.getName() + SysConstants.PDF_EXTENSION;
        String pdfClassify = report.getClassify();

        File pdfFile = new File(filePath);
        if (!pdfFile.getParentFile().exists()) {
            pdfFile.getParentFile().mkdirs();
        }
        if (pdfFile.exists() && BACKUPFILE_PDF) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(pdfFile, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        if (PDF4ReportUtils.T_ARUPPF.equals(pdfClassify)) {// 奥雅纳工程格式报告
            PDF4ReportUtils.genPdf4Aruppf(pdfFile, report, reportData, language, unitType);
        } else if (PDF4ReportUtils.T_BOCHENGPF.equals(pdfClassify)) {// 柏诚工程格式报告
            PDF4ReportUtils.genPdf4Bochengpf(pdfFile, report, reportData, language, unitType);
        } else if (PDF4ReportUtils.T_TECHPROJ.equals(pdfClassify)) {// 技术说明(工程版)
            PDF4ReportUtils.genPdf4Techproj(pdfFile, report, reportData, language, unitType);
			report.setName(report.getName() + SysConstants.PDF_EXTENSION);
        } else if (PDF4ReportUtils.T_TECHSALER.equals(pdfClassify)) {// 技术说明(销售版)
            PDF4ReportUtils.genPdf4Techsaler(pdfFile, report, reportData, language, unitType);
        } else {
            logger.warn("Failed to generate pdf: Classify is undefined! >>PDFClassify: " + pdfClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void generateWord(Report report, String path, ReportData reportData) throws Exception {
        LanguageEnum language = AHUContext.getLanguage();
        UnitSystemEnum unitType = AHUContext.getUnitSystem();

        String filePath = path + "/" + report.getName() + SysConstants.WORD2007_EXTENSION;
        String wordClassify = report.getClassify();

        File wordFile = new File(filePath);
        if (!wordFile.getParentFile().exists()) {
            wordFile.getParentFile().mkdirs();
        }
        if (wordFile.exists() && BACKUPFILE_WORD) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(wordFile, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        if (Word4ReportUtils.T_SPECIALLIST.equals(wordClassify)) {// 非标清单
            Word4ReportUtils.genWord4Speciallist(wordFile, report, reportData, language, unitType);
        } else if (Word4ReportUtils.T_TECHPROJ.equals(wordClassify)) {// 技术说明(工程版)
            Word4ReportUtils.genWord4Techproj(wordFile, report, reportData, language, unitType);
            report.setName(report.getName() + SysConstants.WORD2007_EXTENSION);
        } else {
            logger.warn("Failed to generate word: Classify is undefined! >>wordClassify: " + wordClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void reportGenerated(String projectId, String[] ahuIdArray, List<Report> generatedReports) {
        ReportRequestVob responseB = new ReportRequestVob();
        responseB.setProjectId(projectId);
        responseB.setAhuIds(ahuIdArray);
        responseB.setReports(generatedReports.toArray(new Report[generatedReports.size()]));

        CalContextUtil.sendResultMessage(projectId, JSON.toJSONString(responseB));
        CalContextUtil.sendMessage(projectId, AHUContext.getIntlString(GENERATE_SUCCESS));
        CalContextUtil.sendFinishMessage(projectId);
    }

}
