package com.carrier.ahu.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.vo.*;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BeanTransferUtil {
	public static final short CAL_ALL_SECTION = -1;
	/**
	 * 转换前端传输的VO对象，为计算参数对象，以方便后续计算 本方法作为其它Service方法接受前端参数的入口
	 *
	 * Updated by JL 10/20
	 */
	public static AhuParam getAhuParam(AhuVO ahuVO) {
		String sectionStr = ahuVO.getSectionStr();
		if(EmptyUtil.isNotEmpty(ahuVO.getAllSectionsStr())){
			sectionStr = ahuVO.getAllSectionsStr();
		}
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {
		}.getType());
		AhuParam ahuParam = beanCopyFromAhuVoToAhuParam(ahuVO);
		
		// 拷贝当前选中的属性到
		List<PartParam> partParams = new ArrayList<PartParam>();
		for (int i=0;i<sectionVOs.size();i++) {
			
			SectionVO section = sectionVOs.get(i);
			if (ahuVO.getPosition() == CAL_ALL_SECTION
					|| (ahuVO.getPosition() != null && ahuVO.getPosition().equals(section.getPosition()))) {
				PartParam partTemp = beanCopyFromSectionVoToPartParam(section);

				//上下关系段添加
				if(EmptyUtil.isNotEmpty(ahuVO.getAllSectionsStr()) && EmptyUtil.isNotEmpty(ahuVO.getValidateSections())) {
					addPreNextPart(partTemp, ahuVO);
				}
				
				partParams.add(partTemp);
			}
		}
		ahuParam.setPartParams(partParams);
		ahuParam.setIsNS(ahuVO.getIsNS());
		return ahuParam;
	}

	private static void addPreNextPart(PartParam partCurrent, AhuVO ahuVO) {
		Gson gson = new Gson();
		List<AirDirectionVO> airDirectionVOList = gson.fromJson(ahuVO.getValidateSections(), new TypeToken<List<AirDirectionVO>>() {}.getType());
		if (!EmptyUtil.isEmpty(airDirectionVOList)) {
			boolean enabled = true;
			String msg = "";
			for (AirDirectionVO airDirectionVO : airDirectionVOList) {
				List<SectionRelationVO> relationVOs = airDirectionVO.getValue();
				//段前后循序校验
				for (int i = 0; i < relationVOs.size(); i++) {
					SectionRelationVO sectionCurrent = relationVOs.get(i);
					if(partCurrent.getPosition() == Integer.parseInt(sectionCurrent.getId())){
						if(i>0) {
							SectionRelationVO sectionPre = relationVOs.get(i - 1);
							partCurrent.setPreParams(getPreNextMetaJson(ahuVO,Integer.parseInt(sectionPre.getId())));
						}
						if(i<relationVOs.size()-1) {
							SectionRelationVO sectionNext = relationVOs.get(i + 1);
							partCurrent.setNextParams(getPreNextMetaJson(ahuVO,Integer.parseInt(sectionNext.getId())));
						}

					}
				}
			}
		}
	}
	private static Map<String, Object> getPreNextMetaJson(AhuVO ahuVO,int position){
		String sectionStr = ahuVO.getAllSectionsStr();
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {}.getType());
		for (SectionVO sectionVO : sectionVOs) {
			if(sectionVO.getPosition() == position ){
				Map<String, Object> preNextMetaMap = (Map<String, Object>) JSONObject.parseObject(sectionVO.getMetaJson());
				preNextMetaMap.put(RestConstant.SYS_MAP_AHU_KEY,sectionVO.getKey());
				return preNextMetaMap;
			}
		}
		return null;
	}
	private static PartParam beanCopyFromSectionVoToPartParam(SectionVO section) {
		PartParam partParam = new PartParam();
		partParam.setAvdirection(section.getAvdirection());
		partParam.setCost(section.getCost());
		partParam.setGroupi(section.getGroupi());
		partParam.setKey(section.getKey());
		partParam.setOrders(section.getOrders());
		partParam.setPartid(section.getPartid());
		partParam.setPic(section.getPic());
		partParam.setPid(section.getPid());
		partParam.setPosition(section.getPosition());
		partParam.setSetup(section.getSetup());
		partParam.setTypes(
				null == section.getTypes() ? MetaCodeGen.getSectionType(section.getKey()) : section.getTypes());
		partParam.setUnitid(section.getUnitid());
		Map<String, Object> map = (Map<String, Object>) JSONObject.parseObject(section.getMetaJson());
		partParam.setParams(map);
		return partParam;
	}

	private static AhuParam beanCopyFromAhuVoToAhuParam(AhuVO ahuVO) {
		AhuParam ahuParam = new AhuParam();
		ahuParam.setCost(ahuVO.getCost());
		ahuParam.setCustomerName(ahuVO.getCustomerName());
		ahuParam.setDrawing(ahuVO.getDrawing());
		ahuParam.setDrawingNo(ahuVO.getDrawingNo());
		ahuParam.setGroupId(ahuVO.getGroupId());
		ahuParam.setLb(ahuVO.getLb());
		ahuParam.setModifiedno(ahuVO.getModifiedno());
		ahuParam.setMount(ahuVO.getMount());
		ahuParam.setName(ahuVO.getName());
		ahuParam.setNsprice(ahuVO.getNsprice());
		ahuParam.setPaneltype(ahuVO.getPaneltype());
		ahuParam.setPid(ahuVO.getPid());
		ahuParam.setPrice(ahuVO.getPrice());
		ahuParam.setProduct(ahuVO.getProduct());
		ahuParam.setSeries(ahuVO.getSeries());
		ahuParam.setUnitid(ahuVO.getUnitid());
		ahuParam.setUnitNo(ahuVO.getUnitNo());
		ahuParam.setWeight(ahuVO.getWeight());
		if (StringUtils.isNotBlank(ahuVO.getAhuStr())) {
			JSONObject jasonObject = JSONObject.parseObject(ahuVO.getAhuStr());
			Map<String, Object> map = (Map<String, Object>) jasonObject;
			ahuParam.setParams(map);
		}
		return ahuParam;
	}
}
