package com.carrier.ahu.vo;

import java.util.Map;

import lombok.Data;

@Data
public class BatchCfgProgressVO {
	String text;
	Map<String, Object> data;
	Object children;
}
