package com.carrier.ahu.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.SectionTestCaseUtil;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.vo.AhuVO;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.SectionVO;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;

/**
 * 段业务接口 Created by Wen zhengtao on 2017/3/16.
 */
@Api(description = "段(section)相关接口")
@RestController
public class SectionController extends AbstractController {
	@Autowired
	SectionService sectionService;
	@Autowired
	AhuService ahuService;
	@Autowired
	GroupService groupService;
	@Autowired
	private AhuGroupBindService ahuGroupBindService;
	@Autowired
	private AhuStatusTool ahuStatusTool;
	@Autowired
	private PartitionService partitionService;
	@Autowired
	ProjectService projectService;

	private boolean toWrite = false;//（保存计算结果到excel test case）开关
	 
	@RequestMapping(value = "section/list", method = RequestMethod.GET)
	public ApiResult<List<Part>> list(String ahuId) {
		List<Part> result = sectionService.findSectionList(ahuId);
		Collections.sort(result, new Comparator<Part>() {
			public int compare(Part arg0, Part arg1) {
				return arg0.getPosition().compareTo(arg1.getPosition());
			}
		});
		return ApiResult.success(result);
	}

	@RequestMapping(value = "section/{sectionId}", method = RequestMethod.GET)
	public ApiResult<Part> findById(@PathVariable("sectionId") String sectionId) {
		Part section = sectionService.findSectionById(sectionId);
		return ApiResult.success(section);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "section/save", method = RequestMethod.POST)
	public ApiResult<String> save(Part section) {
		String pid = RestConstant.SYS_BLANK;
		String pMetaJson = section.getMetaJson();
		section.setRecordStatus(AhuStatusEnum.SELECTING.getId());
		if (EmptyUtil.isNotEmpty(pMetaJson)) {
			Map<String, Object> conMap = JSON.parseObject(pMetaJson, HashMap.class);
			if (EmptyUtil.isNotEmpty(conMap)) {
				Boolean isComplete = MapValueUtils.getBooleanValue(SystemCalculateConstants.META_SECTION_COMPLETED,
						conMap);
				if (EmptyUtil.isNotEmpty(isComplete)) {
					if (isComplete) {
						section.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
					}
				}
			}
		}
		if (StringUtils.isBlank(section.getPartid())) {
			User user = getUser();
			pid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
			section.setPartid(pid);
			sectionService.addSection(section, getUserName());
		} else {
			pid = sectionService.updateSection(section, getUserName());
		}
		ahuStatusTool.syncStatus(section.getPid(), getUserName());
		return ApiResult.success(pid);
	}

	/**
	 * 批量保存段信息</br>
	 * 项目状态变化</br>
	 * 
	 * @param ahuVO
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "section/bacthsave", method = RequestMethod.POST)
	public ApiResult<String> bacthSave(AhuVO ahuVO) throws Exception {
		Unit unit = ahuService.findAhuById(ahuVO.getUnitid());
		unit.setMetaJson(ahuVO.getAhuStr());
		unit.setLayoutJson(ahuVO.getLayoutStr());

		UnitUtil.genSerialAndProduct(unit);

		String groupCodeOld = unit.getGroupCode();
		boolean allSectionComplete = false;
		String sectionStr = ahuVO.getSectionStr();
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {
		}.getType());
		List<Part> parts = new ArrayList<Part>();
		List<Part> oldParts = sectionService.findSectionList(ahuVO.getUnitid());
		List<String> partIds = new ArrayList<String>();
		for (SectionVO sectionVO : sectionVOs) {
			Part part = new Part();
			part.setPartid(sectionVO.getPartid());
			String pMetaJson = sectionVO.getMetaJson();
			part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
			if (EmptyUtil.isNotEmpty(pMetaJson)) {
				Map<String, Object> conMap = JSON.parseObject(pMetaJson, HashMap.class);
				if (EmptyUtil.isNotEmpty(conMap)) {
					Boolean isComplete = MapValueUtils
							.getBooleanValue(SystemCalculateConstants.META_SECTION_COMPLETED, conMap);
					if (EmptyUtil.isNotEmpty(isComplete)) {
						if (isComplete) {
							part.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
							allSectionComplete = true;
						}else{
							allSectionComplete = false;//有没有完成选型的段
						}
					}
				}
			}
			part.setMetaJson(pMetaJson);
			part.setSectionKey(sectionVO.getKey());
			part.setCost(sectionVO.getCost());
			part.setPid(ahuVO.getPid());
			part.setUnitid(ahuVO.getUnitid());
			part.setPosition(sectionVO.getPosition());

			if (StringUtils.isBlank(part.getPartid())) {
				User user = getUser();
				String pid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
				part.setPartid(pid);
				sectionService.addSection(part, getUserName());
			} else {
				partIds.add(sectionVO.getPartid());
				sectionService.updateSection(part, getUserName());
			}
			parts.add(part);
		}
		// divide(parts, unit);

		/**
		 * 删除废弃的段
		 */
		for (Part oldPart : oldParts) {
			boolean deleteFlag = true;
			for (String partId : partIds) {
				if (oldPart.getPartid().equals(partId)) {
					deleteFlag = false;
				}
			}
			if (deleteFlag) {
				sectionService.deleteSection(oldPart.getPartid());
			}
		}

		/**
		 * ahu 附加属性
		 */
		// AHU段信息发生变化后，自动移动到未分组,变更组编码，删除组绑定
		List<Part> partList = sectionService.findSectionList(ahuVO.getUnitid());
		String groupCodeNew = TemplateUtil.getGroupCodeSectionKeysSorted(null, partList);
		if (!groupCodeNew.equals(groupCodeOld)) {
			// 机组编码发生变化时，移动到未分组
			unit.setGroupId(null);
			unit.setGroupCode(groupCodeNew);
			ahuGroupBindService.deleteBind(ahuVO.getUnitid());
		}
		if(!"true".equals(ahuVO.getIsNS())) {
			// 分段中
			unit.setRecordStatus(AhuStatusEnum.UNSPLIT.getId());
			// clear price and weight
			unit.setPrice(0d);
			unit.setWeight(0d);
		}
		ahuService.updateAhu(unit, getUserName());
		ahuStatusTool.syncStatus(ahuVO.getPid(), getUserName());


        // 如果机组正常结束，保存的时候，需要将默认分段的数据同时保存
        if (allSectionComplete) {
            Partition partition = AhuPartitionGenerator.generatePartition(ahuVO.getUnitid(), unit, partList);
            this.partitionService.savePartition(partition, getUserName());
        }

		/**
		 * 自动测试相关 should be deleted
		 */
        //this.generateTestData(ahuVO, sectionVOs, unit);

		return ApiResult.success("");
	}
	
	/**
	 * Should be deleted.
	 * 
	 * @param ahuVO
	 * @param sectionVOs
	 * @param unit
	 */
    private void generateTestData(AhuVO ahuVO, List<SectionVO> sectionVOs, Unit unit) {
        try {
            // 将正常机组最新段信息写入 testCase excel
			logger.error("**************" + unit.getUnitid() + " generateTestData start:" + toWrite);
			if (toWrite) {
                User user = getUser();
                LanguageEnum language = LanguageEnum.byId(user.getPreferredLocale());
                UnitSystemEnum unitType = UnitSystemEnum.byId(user.getUnitPreferCode());
                Project project = projectService.getProjectById(ahuVO.getPid());
                boolean writeRet = SectionTestCaseUtil.doWrite(unit, sectionVOs, language, unitType, project);
                logger.error("**************" + unit.getUnitid() + " write result:" + writeRet + "**************");
            }
        } catch (Exception e) {
            logger.error("Exception in generate new partition:" + e.getMessage(), e);
        }
    }

	// private String divide(List<Part> parts, Unit unit) {
	// LinkedList<String> keys = new LinkedList<String>();
	// for (Part part : parts) {
	// keys.add(part.getSectionKey());
	// }
	// String ahuGroupCode = MetaCodeGen.getAhuGroupCode(keys);
	// List<GroupInfo> ahuGroups = groupService.findGroupList(unit.getPid());
	// boolean exist = false;
	// if (EmptyUtil.isNotEmpty(ahuGroupCode)) {
	// for (GroupInfo ahuGroup : ahuGroups) {
	// if (ahuGroupCode.equals(ahuGroup.getGroupCode())) {
	// exist = true;
	// unit.setGroupId(ahuGroup.getGroupId());
	// break;
	// }
	// }
	// }
	// if (!exist) {
	// unit.setGroupId(null);
	// }
	// // if(!exist){
	// // AhuGroup ahuGroup = new AhuGroup();
	// // ahuGroup.setAhuGroupCode(ahuGroupCode);
	// // ahuGroup.setGroupName(ahuGroupCode);
	// // ahuGroup.setPid(unit.getPid());
	// // String ahuGroupId = ahuGroupService.addAhuGroup(ahuGroup);
	// // unit.setGroupId(ahuGroupId);
	// // }
	// ahuService.updateAhu(unit, getUserName());
	// return "";
	// }

	@RequestMapping(value = "section/delete/{sectionId}", method = RequestMethod.DELETE)
	public ApiResult<Boolean> delete(@PathVariable("sectionId") String sectionId) {
		Part part = sectionService.findSectionById(sectionId);
		if (EmptyUtil.isNotEmpty(part)) {
			sectionService.deleteSection(sectionId);
			ahuStatusTool.syncStatus(part.getPid(), getUserName());
		}
		return ApiResult.success(true);
	}
    @RequestMapping(value = "section/write2excel", method = RequestMethod.GET)
    public ApiResult<String> write2excel(String write) throws Exception {
    	if(RestConstant.SYS_ASSERT_TRUE.equals(write)){
    		toWrite = true;
    	}
        return ApiResult.success(String.valueOf(toWrite));
    }
}
