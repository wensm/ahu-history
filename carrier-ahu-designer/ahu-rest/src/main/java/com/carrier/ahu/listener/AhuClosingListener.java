package com.carrier.ahu.listener;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.ExecUtils;
import com.carrier.ahu.vo.SysConstants;

/**
 * Shutdown the application process.
 * 
 * Created by Braden Zhou on 2018/10/23.
 */
public class AhuClosingListener implements ApplicationListener<ContextClosedEvent> {

    private static Logger logger = LoggerFactory.getLogger(AhuClosingListener.class);

    public AhuClosingListener() {
        logger.debug(RestConstant.LOG_AHU_CLOSING_INIT);
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        if (SystemUtils.IS_OS_WINDOWS) {
            ExecUtils.executeCmd(new File(RestConstant.SYS_SHUTDOWN_BAT).getAbsolutePath());
        }
    }

}
