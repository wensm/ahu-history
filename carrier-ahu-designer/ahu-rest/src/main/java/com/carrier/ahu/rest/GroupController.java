package com.carrier.ahu.rest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.MatericalConstance;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.MatericalConstanceService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.ApiResult.ErrorCodeEnum;

import io.swagger.annotations.Api;
import lombok.Data;
import lombok.ToString;

import static com.carrier.ahu.common.intl.I18NConstants.UNCLASSIFIED;

/**
 * Created by Wen zhengtao on 2017/3/26.
 */
@Api(description = "组(group)相关接口")
@RestController
public class GroupController extends AbstractController {
	@Autowired
	GroupService groupService;
	@Autowired
	AhuService ahuService;
	@Autowired
	SectionService sectionService;
	@Autowired
	AhuGroupBindService ahuGroupBindService;
	@Autowired
	MatericalConstanceService matericalConstanceService;
	@Autowired
	AhuStatusTool ahuStatusTool;

	@RequestMapping(value = "group/list", method = RequestMethod.GET)
	public ApiResult<Iterable<GroupInfo>> list() {
		Iterable<GroupInfo> result = groupService.findAll();
		return ApiResult.success(result);
	}

	@RequestMapping(value = "group/lists/{projectId}", method = RequestMethod.GET)
	public ApiResult<List<GroupInfo>> lists(@PathVariable("projectId") String projectId) {
		List<GroupInfo> result = groupService.findGroupList(projectId);
		Collections.sort(result, new Comparator<GroupInfo>() {
			public int compare(GroupInfo o1, GroupInfo o2) {
				return o1.getGroupType().compareTo(o2.getGroupType());
			}
		});
		return ApiResult.success(result);
	}

	@RequestMapping(value = "group/lists/{projectId}/{ahuId}", method = RequestMethod.GET)
	public ApiResult<List<GroupInfo>> list(@PathVariable("projectId") String projectId,
			@PathVariable("ahuId") String ahuId) {
		Unit unit = ahuService.findAhuById(ahuId);
		String groupCode = unit.getGroupCode();
		// List<GroupInfo> result = groupService.findGroupListByType(projectId,
		// unit.getPaneltype());
		List<GroupInfo> result = groupService.findGroupListByCode(projectId, groupCode);
		List<GroupInfo> resultNoCode = groupService.findGroupListByCode(projectId, "");
		result.addAll(resultNoCode);
		if (StringUtils.isNotBlank(groupCode)) {
			GroupInfo ungrouped = new GroupInfo();
			ungrouped.setGroupId(GroupTypeEnum.TYPE_UNGROUP.getId());
			ungrouped.setGroupName(getIntlString(UNCLASSIFIED));
			ungrouped.setGroupType("0");
			result.add(ungrouped);
		}
		return ApiResult.success(result);
	}

	@RequestMapping(value = "group/{groupId}", method = RequestMethod.GET)
	public ApiResult<GroupInfo> findById(@PathVariable("groupId") String groupId) {
		GroupInfo Group = groupService.findGroupById(groupId);
		return ApiResult.success(Group);
	}

	@RequestMapping(value = "group/add", method = RequestMethod.POST)
	public ApiResult<String> add(GroupInfo group) throws Exception {
		if (EmptyUtil.isEmpty(group.getProjectId())) {
			throw new ApiException(ErrorCode.PROJECT_ID_IS_EMPTY);
		}
		boolean flag = groupService.checkName(group.getProjectId(), group.getGroupName());
		if (!flag) {
			throw new ApiException(ErrorCode.GROUP_NAME_IS_OCCUPIED);
		}
		String groupType = group.getGroupType();
		if (StringUtils.isBlank(groupType)) {
			throw new ApiException(ErrorCode.GROUP_TYPE_NOT_DEFINED);
		}
		group.setGroupCode(TemplateUtil.getGroupCodeSectionKeysSorted(groupType, null));//新增group 添加groupCode
		User user = getUser();
		String groupId = KeyGenerator.genGroupId(user.getUnitPreferCode(), user.getUserId());
		group.setGroupId(groupId);
		groupService.addGroup(group, getUserName());
		return ApiResult.success(groupId);
	}

	@RequestMapping(value = "group/update", method = RequestMethod.PUT)
	public ApiResult<String> update(GroupInfo group) {
		String groupid = group.getGroupId();
		if (EmptyUtil.isEmpty(groupid)) {
		    throw new ApiException(ErrorCode.GROUP_ID_IS_EMPTY);
		}
		GroupInfo g = groupService.findGroupById(groupid);
		if (EmptyUtil.isEmpty(g)) {
		    throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
		}

		String groupType = group.getGroupType();
		if (StringUtils.isBlank(groupType)) {
			throw new ApiException(ErrorCode.GROUP_TYPE_NOT_DEFINED);
		}
		group.setGroupCode(TemplateUtil.getGroupCodeSectionKeysSorted(groupType, null));//更新group 重置groupCode

		String groupId = groupService.updateGroup(group, getUserName());
		return ApiResult.success(groupId);
	}

	/**
	 * 删除分组<br>
	 * 1.删除分组信息<br>
	 * 2.删除分组和AHU的绑定<br>
	 * 3.删除分组下所有的机组<br>
	 * 4.删除分组下所有的机组的段<br>
	 * 5.删除分组的材料批量配置信息<br>
	 * 6.更新项目状态<br>
	 * 
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "group/delete/{groupId}", method = RequestMethod.DELETE)
	public ApiResult<Boolean> delete(@PathVariable("groupId") String groupId) {
		GroupInfo group = groupService.findGroupById(groupId);
		MatericalConstance constance = matericalConstanceService.getConstanceByProjidAndGroupid(group.getProjectId(),
				groupId);
		if (EmptyUtil.isNotEmpty(constance)) {
			matericalConstanceService.deleteConstance(constance.getPid());
		}
		groupService.deleteGroup(groupId);
		List<AhuGroupBind> list = ahuGroupBindService.findByGroup(groupId);
		for (AhuGroupBind bind : list) {
			ahuService.deleteAhu(bind.getUnitid());
			sectionService.deleteAHUSections(bind.getUnitid());
		}
		ahuGroupBindService.deleteGroup(groupId);
		ahuStatusTool.syncStatus(group.getProjectId(), getUserName());
		return ApiResult.success(true);
	}

	@RequestMapping(value = "group/checkname/{name}", method = RequestMethod.GET)
	public ApiResult<Map<String, String>> dcheckName(@PathVariable("name") String name)
			throws UnsupportedEncodingException {
		System.out.println(name);
		Map<String, String> map = new HashMap<>();
		// String groupName = new
		// String(name.getBytes(Charset.forName("UTF-8")), "iso-8859-1");
		// System.out.println(groupName);
		if (groupService.checkName(name)) {
			map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.SUCCESS.getCode());
			map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.AVAILABLE));
			return ApiResult.success(map);
		}
		map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.ERROR.getCode());
		map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.OCCUPIED));
		return ApiResult.success(map);
	}

	@RequestMapping(value = "group/checkname/{name}/{projectId}", method = RequestMethod.GET)
	public ApiResult<Map<String, String>> dcheckName(@PathVariable("name") String name,
			@PathVariable("projectId") String projectId) throws UnsupportedEncodingException {
		Map<String, String> map = new HashMap<>();
		if (EmptyUtil.isEmpty(projectId)) {
			map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.ERROR.getCode());
			map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.PROJECT_ID_IS_EMPTY));
			
		}
		if (groupService.checkName(projectId, name)) {
			map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.SUCCESS.getCode());
			map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.AVAILABLE));
			return ApiResult.success(map);
		}
		map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.ERROR.getCode());
		map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.OCCUPIED));
		return ApiResult.success(map);
	}

    @RequestMapping(value = "grouptype/list", method = RequestMethod.GET)
    public ApiResult<List<GroupType>> getGroupTypeList4SelectBox() {
        List<GroupType> list = new ArrayList<>();
        for (GroupTypeEnum groupType : GroupTypeEnum.values()) {
            if (groupType.isGroupable()) {
                list.add(new GroupType(groupType.getId(), groupType.getCnName()));
            }
        }
        return ApiResult.success(list);
    }

	@Data
	@ToString
	public static class GroupType {
		private String id;
		private String name;

		public GroupType(String id, String name) {
			this.id = id;
			this.name = name;
		}

		public GroupType() {

		}
	}
}
