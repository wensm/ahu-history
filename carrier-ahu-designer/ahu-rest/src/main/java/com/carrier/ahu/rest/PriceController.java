package com.carrier.ahu.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.PriceResultVO;
import com.carrier.ahu.vo.WeightResultVO;

import io.swagger.annotations.Api;

@Api(description = "价格接口")
@RestController
public class PriceController extends AbstractController {

    @Autowired
    AhuService ahuService;
    @Autowired
    private SectionService sectionService;
    @Autowired
    private CalculatorTool calculatorTool;
    @Autowired
    private PartitionService partitionService;

    @RequestMapping(value = "pricespec", method = RequestMethod.GET)
    public ApiResult<Object> getPriceSpec() throws Exception {
        return ApiResult.success(calculatorTool.getPriceCalculator().getModel());
    }

    @RequestMapping(value = "weightspec", method = RequestMethod.GET)
    public ApiResult<Object> getWeightSpec() throws Exception {
        return ApiResult.success(calculatorTool.getWeightCalculator().getModel());
    }

    @RequestMapping(value = "weight/{ahuId}", method = RequestMethod.GET)
    public ApiResult<WeightResultVO> calSectionWeight(@PathVariable("ahuId") String ahuId) {
        Unit unit = ahuService.findAhuById(ahuId);
        WeightResultVO vo = calculatorTool.calculateAHUWeight(unit);
        return ApiResult.success(vo);
    }

    @RequestMapping(value = "price/{ahuId}", method = RequestMethod.GET)
    public ApiResult<PriceResultVO> calSection(@PathVariable("ahuId") String ahuId) {
        try {
            if (EmptyUtil.isEmpty(ahuId)) {
                throw new ApiException(ErrorCode.UNIT_ID_IS_EMPTY);
            }

            // get AHU details
            Unit unit = ahuService.findAhuById(ahuId);
            List<Part> parts = sectionService.findSectionList(unit.getUnitid());
            Partition partition = partitionService.findPartitionByAHUId(ahuId);
            
            //国内版，强制
            PriceResultVO priceResultVo = calculatorTool.calculateAHUPrice(unit, partition, parts, getLanguage());

            // update part price and unit price
            sectionService.updateSections(parts);
            ahuService.updateAhu(unit, getUserName());

            return ApiResult.success(getIntlString(ErrorCode.PRICE_CALCULATION_WITHOUT_ERROR.getMessage()), priceResultVo);
        } catch (Exception e) {
            return ApiResult.error(getIntlString(ErrorCode.PRICE_CALCULATION_ERROR.getMessage()));
        }
    }

}
