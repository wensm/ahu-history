package com.carrier.ahu.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by liujianfeng on 2017/6/25.
 */
@Data
public class PriceResultVO {
	private List<PriceBean> details;
	private double total;
	private double weight;
	private String ahuId;
	private String priceSpecVersion;

	@Data
	public static class PriceBean {
		private String name;
		private double price;
		private String key;
		private double weight;
	}
}
