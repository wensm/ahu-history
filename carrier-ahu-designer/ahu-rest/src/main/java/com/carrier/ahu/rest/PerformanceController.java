package com.carrier.ahu.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.factory.PerformanceConstant;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.PerformanceVO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;

/**
 * 性能参数批量修改
 */
@Api(description = "性能参数批量修改")
@RestController
@RequestMapping(value = "performance")
public class PerformanceController extends AbstractController {

	@Autowired
	private AhuService ahuService;
	@Autowired
	private SectionService sectionService;

	@SuppressWarnings("unused")
	private List<String> getAhuGroupKeys(List<Unit> units) throws Exception {
		List<String> result = new ArrayList<String>();
		int size = 0;
		for (Unit unit : units) {
			List<Part> parts = sectionService.findSectionList(unit.getUnitid());
			for (Part part : parts) {
				if (!result.contains(part.getSectionKey())) {
					result.add(part.getSectionKey());
				}
			}
			if (size == 0) {
				size = result.size();
			} else {
				if (size != result.size()) {
					throw new CalculationException(ErrorCode.UNIT_SECTION_INFO_INCONSISTENT);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	private Map<String, Object> assemblyUnitV1(Unit unit, Map<String, String[]> propsMap) {
		Map<String, Object> valueMap = new HashMap<>();
		List<Part> parts = sectionService.findSectionList(unit.getUnitid());
		Map<String, Object> ahuMetaJson = getJsonMap(unit.getMetaJson());
		valueMap.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, unit.getDrawingNo());
		valueMap.put(RestConstant.SYS_UNIT_AHU_ID, unit.getUnitid());
		String[] tmpArr = propsMap.get(RestConstant.SYS_UNIT_SPEC_SECTION_AHU);
		for (int i = 0; i < tmpArr.length; i++) {
			String key = tmpArr[i];
			if (ahuMetaJson.containsKey(key)) {
				valueMap.put(key, (String) ahuMetaJson.get(key));
			} else {
				valueMap.put(key, RestConstant.SYS_BLANK);
			}
		}

		for (Part part : parts) {
			if (propsMap.containsKey(part.getSectionKey())) {
				tmpArr = propsMap.get(part.getSectionKey());
				Map<String, Object> sectionMetaMap = getJsonMap(part.getMetaJson());
				for (int i = 0; i < tmpArr.length; i++) {
					String key = tmpArr[i];
					if (sectionMetaMap.containsKey(key)) {
						valueMap.put(key, (String) sectionMetaMap.get(key));
					} else {
						valueMap.put(key, RestConstant.SYS_BLANK);
					}
				}
			}
		}
		return valueMap;
	}

	@RequestMapping(value = "/ahus", method = RequestMethod.GET)
	@ResponseBody
	public ApiResult<List<Map<String, Object>>> performanceBatchUpdate(
			@RequestParam(value = "projectId", required = false) String projectId,
			@RequestParam(value = "groupId", required = false) String groupId) {
		List<Unit> units = new ArrayList<Unit>();
		if (StringUtils.isNotBlank(projectId)) {
			units = ahuService.findAhuList(projectId, null);
		} else {
			units = ahuService.findAhuList(null, groupId);
		}
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
		for (Unit unit : units) {
			Map<String, Object> result = assemblyUnit(unit);
			results.add(result);
		}
		return ApiResult.success(results);
	}

	private Map<String, Object> assemblyUnit(Unit unit) {
		List<Part> parts = sectionService.findSectionList(unit.getUnitid());
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> ahuMetaJson = getJsonMap(unit.getMetaJson());
		result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, unit.getDrawingNo());
		result.put(RestConstant.SYS_UNIT_AHU_ID, unit.getUnitid());
		List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
		Map<String, String[]> propsMap = PerformanceConstant.getPropsMap();

		Map<String, Object> ahuResultMap = new HashMap<String, Object>();
		List<Map<String, Object>> ahuListMap = new ArrayList<Map<String, Object>>();
		String[] tmpArr = propsMap.get("ahu");
		for (String ahuPropKey : ahuMetaJson.keySet()) {
			if (ArrayUtils.contains(tmpArr, ahuPropKey)) {
				Map<String, Object> ahuMap = new HashMap<String, Object>();
				ahuMap.put(RestConstant.SYS_MAP_KEY, ahuPropKey);
				ahuMap.put(RestConstant.SYS_MAP_VALUE, ahuMetaJson.get(ahuPropKey));
				ahuListMap.add(ahuMap);
			}
		}
		ahuResultMap.put(RestConstant.SYS_MAP_NAME, RestConstant.SYS_UNIT_SPEC_SECTION_AHU);
		ahuResultMap.put(RestConstant.SYS_MAP_PROPS, ahuListMap);
		listMap.add(ahuResultMap);

		for (String propsMapKey : propsMap.keySet()) {
			if (!"ahu".equals(propsMapKey)) {
				for (Part part : parts) {
					if (propsMapKey.equals(part.getSectionKey())) {
						Map<String, Object> sectionResultMap = new HashMap<String, Object>();
						List<Map<String, Object>> sectionListMap = new ArrayList<Map<String, Object>>();
						Map<String, Object> sectionMetaMap = getJsonMap(part.getMetaJson());
						tmpArr = propsMap.get(propsMapKey);
						for (String sectionPropKey : sectionMetaMap.keySet()) {
							if (ArrayUtils.contains(tmpArr, sectionPropKey)) {
								Map<String, Object> sectionMap = new HashMap<String, Object>();
								sectionMap.put(RestConstant.SYS_MAP_KEY, sectionPropKey);
								sectionMap.put(RestConstant.SYS_MAP_VALUE, sectionMetaMap.get(sectionPropKey));
								sectionListMap.add(sectionMap);
							}
						}
						sectionResultMap.put(RestConstant.SYS_MAP_NAME, part.getSectionKey());
						sectionResultMap.put(RestConstant.SYS_MAP_PROPS, sectionListMap);
						listMap.add(sectionResultMap);
					}
				}
			}
		}
		result.put(RestConstant.SYS_MAP_SECTIONS, listMap);
		return result;
	}

	private Map<String, Object> getJsonMap(String json) {
		JSONObject jsonObject = JSONObject.parseObject(json);
		Map<String, Object> map = new HashMap<String, Object>();
		map = jsonObject;
		return map;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ApiResult<Map<String, Object>> update(String str) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		Gson gson = new Gson();
		List<PerformanceVO> performanceVOs = gson.fromJson(str, new TypeToken<List<PerformanceVO>>() {
		}.getType());
		for (PerformanceVO pv : performanceVOs) {
			Unit unit = ahuService.findAhuById(pv.getUnitId());
			Map<String, Object> jsonMap = getJsonMap(pv.getJsonStr());
			Map<String, Object> ahuMetaMap = getJsonMap(unit.getMetaJson());
			for (String key : jsonMap.keySet()) {
				if (ahuMetaMap.containsKey(key)) {
					ahuMetaMap.put(key, jsonMap.get(key));
				}
			}
			String ahuStr = JSONObject.toJSONString(ahuMetaMap);
			unit.setMetaJson(ahuStr);
			UnitUtil.genSerialAndProduct(unit);
			unit = TemplateUtil.getTemplateUnit(unit);
			ahuService.updateAhu(unit, getUserName());
			List<Part> parts = sectionService.findSectionList(unit.getUnitid());
			for (Part part : parts) {
				Map<String, Object> sectionMetaMap = getJsonMap(part.getMetaJson());
				for (String key : jsonMap.keySet()) {
					if (sectionMetaMap.containsKey(key)) {
						sectionMetaMap.put(key, jsonMap.get(key));
					}
				}
				String sectionStr = JSONObject.toJSONString(sectionMetaMap);
				part.setMetaJson(sectionStr);
				sectionService.updateSection(part, getUserName());
			}
		}
		return ApiResult.success(result);
	}
}
