package com.carrier.ahu.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by liangd4 on 2018/3/15.
 */
@Data
public class AirDirectionVO {
    private String airDirection;
    private List<SectionRelationVO> value;
}
