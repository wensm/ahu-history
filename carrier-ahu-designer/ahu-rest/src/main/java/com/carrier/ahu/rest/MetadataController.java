
package com.carrier.ahu.rest;

import static com.carrier.ahu.vo.SystemCalculateConstants.ALL_PRODUCT_TYPE;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.engine.price.AHUPriceCalFactory;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.RulesVO;
import com.carrier.ahu.po.meta.SectionMeta;
import com.carrier.ahu.po.meta.SerialVO;
import com.carrier.ahu.po.meta.layout.MetaLayout;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaLayoutGen.MetaParameterWithUnit;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.meta.SectionMetaService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.OptionVO;
import com.carrier.ahu.vo.UnitMetadataVO;

import io.swagger.annotations.Api;

@Api(description = "元数据管理")
@RestController
public class MetadataController extends AbstractController {

    @Autowired
    private SectionMetaService sectionMetaService;
    @Autowired
    AhuService ahuService;

    @RequestMapping(value = "meta/add", method = RequestMethod.POST)
    public Map<String, Object> createMetadata() {
        Map<String, Object> result = new HashMap<String, Object>();
        return result;
    }

    /**
     * 返回分段相关的元数据，该元数据会在分段（Partition）页面上调用
     *
     * @return
     */
    @RequestMapping(value = "meta/partition", method = RequestMethod.GET)
    public ApiResult<Map<String, Object>> getPartitionMetadata() {
        return ApiResult.success(AhuPartitionGenerator.getPartitionMeta());
    }

    @RequestMapping(value = "meta/{version}", method = RequestMethod.GET)
    public Map<String, Object> getMetadatas() {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, false);
        String configFilePath = System.getProperty(RestConstant.SYS_PROPERTY_USER_DIR)
                .concat(RestConstant.SYS_PATH_RESOURCES_SECTIONS_V1);
        File rootFile = new File(configFilePath);
        if (rootFile.exists()) {
            File[] files = rootFile.listFiles();
            List<SectionMeta> sectionMetas = sectionMetaService.getSectionMetas(files);
            result.put(RestConstant.SYS_STRING_DATA, sectionMetas);
            result.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, true);
        }
        return result;
    }

    @RequestMapping(value = "/meta/ahu", method = RequestMethod.GET)
    public SectionMeta getAhuMeta() {
        SectionMeta meta = AhuSectionMetas.getInstance().getAhuMeta();
        return meta;
    }

    @RequestMapping(value = "/meta/sections", method = RequestMethod.GET)
    public List<SectionMeta> getSectionMeta() {
        List<SectionMeta> sectionMetas = new ArrayList<SectionMeta>();
        sectionMetas.addAll(AhuSectionMetas.getInstance().getSectionMetas());
        return sectionMetas;
    }

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public Map<String, Map<String, Map<String, Object>>> getSectionTemplates() {
        return TemplateUtil.getAllAhuProTemplate();
    }

    @SuppressWarnings("unused")
    private void printProperties(SectionMeta meta) {
        Map<String, MetaParameter> map = meta.getParameters();
        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            MetaParameter para = map.get(key);
            String value = RestConstant.SYS_BLANK;
            if (para instanceof MetaParameterWithUnit) {
                MetaParameterWithUnit para1 = (MetaParameterWithUnit) para;
                if (EmptyUtil.isNotEmpty(para1.getDefaultValue())) {
                    value = para1.getDefaultValue();
                }
            }
        }
    }

    /**
     * 获取所有系列所有段的属性，下拉选项等信息。
     * 
     * @return
     */
    @RequestMapping(value = "/meta/layout", method = RequestMethod.GET)
    public Map<String, Map<String, MetaLayout>> getSectionMetaWithLayout() {
        Map<String, Map<String, MetaLayout>> allSectionMetas = new HashMap<String, Map<String, MetaLayout>>();
        for (String product : ALL_PRODUCT_TYPE) {
            Map<String, MetaLayout> sectionMetas = new HashMap<>();
            sectionMetas.putAll(AhuSectionMetas.getInstance().getAhuAndSectionMetaLayoutWithDetail(product));
            allSectionMetas.put(product, sectionMetas);
        }
        return allSectionMetas;
    }

    @RequestMapping(value = "/meta/unit", method = RequestMethod.GET)
    public ApiResult<UnitMetadataVO> getAhuPropUnit() {
        return ApiResult.success(new UnitMetadataVO(AhuSectionMetas.getInstance().getAhuPropUnitMeta(),
                AhuSectionMetas.getInstance().getUnitSetting()));
    }

    /**
     * Get section meta values base on unit series for current factory.
     * 
     * @param unitSeries - '39CQ', use factory metadata if not set
     * @return
     */
    @RequestMapping(value = "/meta/ahuSectionValues", method = RequestMethod.GET)
    public Map<String, MetaValue> getSectionValueMeta(String unitSeries) {
        return SectionMetaUtils.getSectionMetaValues(unitSeries);
    }

    /**
     * Get section meta validations base on unit series for current factory.
     * 
     * @param unitSeries - '39CQ', use factory metadata if not set
     * @return
     */
    @RequestMapping(value = "/meta/validations", method = RequestMethod.GET)
    public Map<String, List<RulesVO>> getValidations(String unitSeries, String unitid) {
        /*Map<String, List<RulesVO>> stringRulesVOMap = new HashMap<String, List<RulesVO>>();
        *//*风量添加*//*
        List<RulesVO> rulesVOListSairvolume = new ArrayList<RulesVO>();
        RulesVO rulesVORange = new RulesVO();//范围
        rulesVORange.setRule(RulesEnum.RANGE.getCode());
        Set<String> stringSetRange = new HashSet<String>();
        stringSetRange.add("2000-200000");
        rulesVORange.setRuleValueSet(stringSetRange);
        rulesVORange.setRuleMassage("default");
        rulesVOListSairvolume.add(rulesVORange);
        stringRulesVOMap.put("meta.ahu.sairvolume",rulesVOListSairvolume);

        *//*底座类型*//*
        List<RulesVO> rulesVOListBaseType = new ArrayList<RulesVO>();
        RulesVO rulesVONoEquals = new RulesVO();//不等于
        rulesVONoEquals.setRule(RulesEnum.NOEQUALS.getCode());
        Set<String> stringSetNoEquals = new HashSet<String>();
        stringSetNoEquals.add("powderCoatedCR");
        rulesVONoEquals.setRuleValueSet(stringSetNoEquals);
        rulesVONoEquals.setRuleMassage("the value can not be equal to the specified value");
        rulesVOListBaseType.add(rulesVONoEquals);
        stringRulesVOMap.put("meta.ahu.baseType",rulesVOListBaseType);

        *//*出厂形式*//*
        List<RulesVO> rulesVOListDelivery = new ArrayList<RulesVO>();
        RulesVO rulesVOEquals = new RulesVO();//等于
        rulesVOEquals.setRule(RulesEnum.EQUALS.getCode());
        Set<String> stringSetEquals = new HashSet<String>();
        stringSetEquals.add("Section");
        rulesVOEquals.setRuleValueSet(stringSetEquals);
        rulesVOEquals.setRuleMassage("the value must be equal to the specified value");
        rulesVOListDelivery.add(rulesVOEquals);
        stringRulesVOMap.put("meta.ahu.delivery",rulesVOListDelivery);*/

        // 获取系列
        if (EmptyUtil.isNotEmpty(unitid)) {
            Unit unit = ahuService.findAhuById(unitid);
            if (EmptyUtil.isNotEmpty(unit))
                unitSeries = unit.getProduct();
        }
        return SectionMetaUtils.getSectionMetaValidations(unitSeries);
    }

    /**
     * Get section meta serial base on unit series for current factory.
     * 
     * @return
     */
    @RequestMapping(value = "/meta/serial", method = RequestMethod.GET)
    public Map<String, Map<String, List<SerialVO>>> getSerial() {
        Map<String, Map<String, List<SerialVO>>> allSerials = new HashMap<String, Map<String, List<SerialVO>>>();
        for (String product : ALL_PRODUCT_TYPE) {
            Map<String, List<SerialVO>> serials = new HashMap<String, List<SerialVO>>();
            serials = SectionMetaUtils.getSectionMetaSerial(product);
            allSerials.put(product, serials);
        }
        return allSerials;
    }

    // @RequestMapping(value = "/ahu/sectionValues", method = RequestMethod.GET)
    // public MetaLayout getSectionLayout() {
    // MetaLayout result = AhuUtilMeta.getAhuMetaLayout();
    // return result;
    // }

    @RequestMapping(value = "/meta/price/versions", method = RequestMethod.GET)
    public ApiResult<List<OptionVO>> getAHUPriceCalVersions() {
        List<OptionVO> versionVos = new ArrayList<>();
        List<String> versions = AHUPriceCalFactory.getAHUPriceCalVersion();
        for (String version : versions) {
            versionVos.add(new OptionVO(version, version));
        }
        return ApiResult.success(versionVos);
    }

}
