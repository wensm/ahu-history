package com.carrier.ahu.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;

/**
 * 全局异常处理 1,自定义异常都在此类里分别做处理
 * 
 * Created by Wen zhengtao on 2017/2/3.
 */
@RestControllerAdvice
public class AhuExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(AhuExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ApiResult<String> handle(Exception exception) {
        String message = AHUContext.getIntlString(ErrorCode.INTERNAL_SERVER_ERROR.getMessage(), exception.getMessage());
        logger.error(message, exception);
        return ApiResult.error(ErrorCode.INTERNAL_SERVER_ERROR.getCode(), message);
    }

    @ExceptionHandler(AhuException.class)
    public ApiResult<String> handleAhuException(AhuException exception) {
        if (exception.getErrorCode() == null) {
            logger.error("", exception);
            return ApiResult.error(exception.getMessage());
        } else {
            String message = null;
            if (EmptyUtil.isEmpty(exception.getParams())) {
                message = AHUContext.getIntlString(exception.getErrorCode().getMessage());
            } else {
                message = AHUContext.getIntlString(exception.getErrorCode().getMessage(), exception.getParams());
            }
            logger.error(message, exception);
            return ApiResult.error(exception.getErrorCode().getCode(), message);
        }
    }

}
