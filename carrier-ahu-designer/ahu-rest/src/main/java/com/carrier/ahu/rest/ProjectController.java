package com.carrier.ahu.rest;

import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.MatericalConstance;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.MatericalConstanceService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_RECORDSTATUS_ARCHIEVED;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_RECORDSTATUS_UNARCHIEVED;

/**
 * 项目业务接口 Created by Wen zhengtao on 2017/3/16.
 */
@Api(description = "项目(project)相关接口")
@RestController
public class ProjectController extends AbstractController {

	@Autowired
	ProjectService projectService;
    @Autowired
    PartitionService partitionService;
	@Autowired
	GroupService groupService;
	@Autowired
	AhuService ahuService;
	@Autowired
	SectionService sectionService;
	@Autowired
	AhuGroupBindService ahuGroupBindService;
	@Autowired
	MatericalConstanceService matericalConstanceService;

	@RequestMapping(value = "project/list", method = RequestMethod.GET)
	public ApiResult<List<Project>> list(String recordStatus) {
		List<Project> result = null;

		if(SYS_STRING_RECORDSTATUS_UNARCHIEVED.equals(recordStatus)){//非未归档
			result = projectService.findProjectListUnArchieved();
		}else if(SYS_STRING_RECORDSTATUS_ARCHIEVED.equals(recordStatus)){//已归档
			result = projectService.findProjectListArchieved();
		}else{
			result = projectService.findProjectList();//所有
		}
		return ApiResult.success(result);
	}

	@RequestMapping(value = "project/{projectId}", method = RequestMethod.GET)
	public ApiResult<Project> findById(@PathVariable("projectId") String projectId) {
		Project project = projectService.getProjectById(projectId);
		return ApiResult.success(project);
	}

	@RequestMapping(value = "project/exists/{projectNo}", method = RequestMethod.GET)
	public ApiResult<Boolean> exists(@PathVariable("projectNo") String projectNo) {
		boolean isExists = projectService.exists(projectNo);
		return ApiResult.success(isExists);
	}

	@RequestMapping(value = "project/save", method = RequestMethod.POST)
	public ApiResult<String> add(Project project) {
		String pid = "";
		project.setRecordStatus(AhuStatusEnum.SELECTING.getId());
		if (StringUtils.isBlank(project.getPid())) {
			User user = getUser();
			pid = KeyGenerator.genProjectId(user.getUnitPreferCode(), user.getUserId());
			project.setPid(pid);
			projectService.addProject(project, getUserName());
			/*注释部分对项目ID进行控制*/
			/*boolean isExists = projectService.exists(project.getNo());
			if (isExists) {
				ApiResult.error(ControllerI18N.getInstance().getIntlString("项目编码已占用", getLanguage()));
			} else {

			}*/
		} else {
			pid = projectService.updateProject(project, getUserName());
		}
		return ApiResult.success(pid);
	}

	/**
	 * 项目删除<br>
	 * 1.删除项目信息<br>
	 * 2.删除分组信息<br>
	 * 3.删除机组和AHU的绑定<br>
	 * 4.删除分组下所有的机组<br>
	 * 5.删除分组下所有的机组的段<br>
	 * 6.删除分组的材料批量配置信息<br>
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "project/delete/{projectId}", method = RequestMethod.POST)
	public ApiResult<Boolean> delete(@PathVariable("projectId") String projectId) {
		projectService.deleteProject(projectId);
		List<GroupInfo> groupList = groupService.findGroupList(projectId);
		if (EmptyUtil.isNotEmpty(groupList)) {
			for (GroupInfo group : groupList) {
				groupService.deleteGroup(group.getGroupId());
				ahuGroupBindService.deleteGroup(group.getGroupId());
			}
		}
		List<Unit> unitList = ahuService.findAhuList(projectId, null);
		if (EmptyUtil.isNotEmpty(unitList)) {
			for (Unit unit : unitList) {
				ahuService.deleteAhu(unit.getUnitid());
				sectionService.deleteAHUSections(unit.getUnitid());
		        Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
		        if (partition != null) {
		            partitionService.deletePartition(partition.getPartitionid());
		        }
			}
		}
		List<MatericalConstance> constanceList = matericalConstanceService.findByProjectid(projectId);
		if (EmptyUtil.isNotEmpty(constanceList)) {
			for (MatericalConstance constance : constanceList) {
				matericalConstanceService.deleteConstance(constance.getPid());
			}
		}
		return ApiResult.success(true);
	}

	/**
	 * 项目归档
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "project/archieved/{projectId}", method = RequestMethod.POST)
	public ApiResult<Boolean> archieved(@PathVariable("projectId") String projectId) {
		Project project = projectService.getProjectById(projectId);
		project.setRecordStatus(AhuStatusEnum.ARCHIVED.getId());
		projectService.updateProject(project, getUserName());
		return ApiResult.success(true);
	}
}
