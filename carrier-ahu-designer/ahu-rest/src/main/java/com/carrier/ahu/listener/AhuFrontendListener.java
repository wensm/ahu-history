package com.carrier.ahu.listener;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import com.carrier.ahu.calculator.WeightCalculator;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.listener.AhuUI.AhuUIListener;

/**
 * 当spring boot应用启动完成后，执行某一件事情 1，打开frontend窗口 <br/>
 * 
 * Created by Wen zhengtao on 2017/3/28.
 */
public class AhuFrontendListener implements ApplicationListener<ApplicationReadyEvent> {

    private static Logger logger = LoggerFactory.getLogger(AhuFrontendListener.class);

    static ConfigurableApplicationContext context = null;

    public AhuFrontendListener() {
        logger.debug(RestConstant.LOG_AHU_FRONTEND_LISTENER_INIT);
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        AhuFrontendListener.context = event.getApplicationContext();
        String port = context.getEnvironment().getProperty(RestConstant.SYS_LOCAL_SERVER_PORT);
        AhuUI.showAhuUI(NumberUtils.toInt(port), new AhuUIListener() {

            @Override
            public void uiStarted() {
                // spring boot 启动之后加载重量cvs，保证启动速度不收影响。
                WeightCalculator weightCalculator = (WeightCalculator) context
                        .getBean(RestConstant.SYS_BEAN_WEIGHT_CALCULATOR);
                weightCalculator.reloadCalModeInstance();
            }

        });

    }

}
