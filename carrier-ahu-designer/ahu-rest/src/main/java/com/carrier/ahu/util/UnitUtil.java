package com.carrier.ahu.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.constant.RestConstant;

public class UnitUtil {

	/**
	 * 绑定产品系列和序列号 <br>
	 * 如果两处的序列号值不一样，采用Bean服从metaJson方式，即保留metaJson中的值
	 * 
	 * @param unit
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Unit genSerialAndProduct(Unit unit) {
		String metaJson = unit.getMetaJson();
		if (StringUtils.isBlank(metaJson)) {
			return unit;
		}
		Map<String, String> metaMap = new HashMap<>();
		try {
			metaMap = JSON.parseObject(metaJson, HashMap.class);
		} catch (Exception e) {
			return unit;
		}

		if (metaMap.isEmpty()) {
			return unit;
		}

		String serial = unit.getSeries();
		String product = unit.getProduct();
		String serialMeta = metaMap.get(RestConstant.METAHU_SERIAL);
		String productMeta = metaMap.get(RestConstant.METAHU_PRODUCT);

		if (EmptyUtil.isEmpty(serial)) {
			if (EmptyUtil.isEmpty(serialMeta)) {
			} else {
				unit.setSeries(serialMeta);
			}
		} else {
			if (StringUtils.isBlank(serialMeta)) {
				metaMap.put(RestConstant.METAHU_SERIAL, serial);
			} else {
				if (serialMeta.equals(serial)) {

				} else {
					// 如果两处的序列号值不一样，采用Bean服从metaJson方式，即保留metaJson中的值
					unit.setSeries(serialMeta);
				}
			}
		}
		if (EmptyUtil.isEmpty(product)) {
			if (EmptyUtil.isEmpty(productMeta)) {
			} else {
				unit.setProduct(productMeta);
			}
		} else {
			if (StringUtils.isBlank(productMeta)) {
				metaMap.put(RestConstant.METAHU_PRODUCT, product);
			} else {
				if (productMeta.equals(product)) {

				} else {
					// 如果两处的机组系列值不一样，采用Bean服从metaJson方式，即保留metaJson中的值
					unit.setProduct(productMeta);
				}
			}
		}

		unit.setMetaJson(JSON.toJSONString(metaMap));

		return unit;
	}
	
	public static <T extends Serializable> T clone(T obj) {
		T clonedObj = null;
		try {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(obj);
		oos.close();
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		clonedObj = (T) ois.readObject();
		ois.close();

		} catch (Exception e) {
		e.printStackTrace();
		}

		return clonedObj;
		}
}
