package com.carrier.ahu.vo;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
@Data
public class HeatQVO {

    private double OutDryBulbT;//干球温度
    private double OutWetBulbT;//湿球温度
    private double OutRelativeT;//相对湿度
    private double HumDryBulbT;//加湿比干球温度
    private double HeatQ;//热量
    private String sectionKey;//段Key

}
