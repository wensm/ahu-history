package com.carrier.ahu.rest;

import java.io.IOException;
import java.util.*;

import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.service.constant.ServiceConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.CalculatorModel;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.calculator.PriceCalculator;
import com.carrier.ahu.calculator.WeightCalculator;
import com.carrier.ahu.calculator.price.CalPriceTool;
import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.calculator.price.PriceCodeXSLXPO;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelCalculationObj;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.cal.WeightBean;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.PriceResultVO;
import com.carrier.ahu.vo.PriceResultVO.PriceBean;
import com.carrier.ahu.vo.SysConstants;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.carrier.ahu.vo.WeightResultVO;
import com.google.gson.Gson;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_TRUE;
import static com.carrier.ahu.constant.CommonConstant.SYS_PANEL_18;

@Component
@Data
@Slf4j
public class CalculatorTool {

    private static WeightCalculator weightCalculator = new WeightCalculator();
    @Autowired
    private PriceCalculator priceCalculator;
    private Gson gson = new Gson();
    @Autowired
    PartitionService partitionService;
    @Autowired
    ProjectService projectService;
    private static CalculatorModel calculatorModel;
    @Value("${price.code.test.output}")
    private String priceCodeTestOutput;

    public CalculatorModel getCalculatorModel() {
        if (null == calculatorModel) {
            calculatorModel = weightCalculator.getCalModelInstance();
        }
        return calculatorModel;
    }

    public List<CalculatorSpec> getSpec() {
        return getCalculatorModel().getSpec();
    }

    public WeightCalculator getWeightCalculator() {
        return weightCalculator;
    }

    /**
     * 计算AHU的总重量信息
     *
     * @param unit
     * @param parts
     * @return
     */
    public WeightResultVO calculateAHUWeight(Unit unit) {
        return calculateAHUWeight(unit, null);
    }
    
    /**
     * 计算AHU的总重量信息
     *
     * @param unit
     * @param partition
     * @return
     */
    public WeightResultVO calculateAHUWeight(Unit unit, Partition partition) {
        if (StringUtils.isEmpty(unit.getSeries())) {
            throw new CalculationException(ErrorCode.UNIT_MODEL_NOT_SELECTED);
        }

        List<WeightBean> weightBeans = new ArrayList<>();
        double total = 0;

        // 计算分段的面板重量
        log.info("Start to calculate partition weight ...");
        if (EmptyUtil.isEmpty(partition)) {
            partition = partitionService.findPartitionByAHUId(unit.getUnitid());
        }

        if (EmptyUtil.isNotEmpty(partition)) {
            List<AhuPartition> ahuPartitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
            Iterator<AhuPartition> ahuPartitionItr = ahuPartitions.iterator();
            while (ahuPartitionItr.hasNext()) {
                AhuPartition ahuPartition = ahuPartitionItr.next();
                double partitionWeight = 0;
                if (AhuPartitionUtils.hasEndFacePanel(ahuPartition, ahuPartitions)) {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ahuPartition, ahuPartitions.size(), true);
                } else {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ahuPartition, ahuPartitions.size(), false);
                }

                WeightBean weightBean = new WeightBean();
                String partitionKey = RestConstant.SYS_WGT_PARTITION_HYPHEN + ahuPartition.getPos();
                weightBean.setKey(partitionKey);
                weightBean.setName(RestConstant.SYS_WGT_PARTITION_WEIGHT + ahuPartition.getPos());
                weightBean.setWeight(partitionWeight);
                weightBean.setId(partitionKey);
                weightBeans.add(weightBean);

                total += partitionWeight;
            }
        }
        unit.setWeight(total); // update weight for late save

        WeightResultVO weightResultVo = new WeightResultVO();
        weightResultVo.setAhuId(unit.getUnitid());
        weightResultVo.setTotal(total);
        weightResultVo.setDetails(weightBeans);
        weightResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION);
        return weightResultVo;
    }

    /**
     * 计算整个ahu所有段价格
     * @param unit
     * @param partition
     * @param parts
     * @param language
     * @return
     */
    public PriceResultVO calculateAHUPrice(Unit unit, Partition partition, List<Part> parts, LanguageEnum language) {
        PriceResultVO priceResultVo = null;
        try {
        	//所有价格相关的计算中，都采用39G的面板布置原则去计算
            List<PanelCalculationObj> casingList = AhuPartitionGenerator.getCasinglist(getSeries(unit.getSeries()),unit, partition, true);
            String partitionJson = partition.getPartitionJson();
            List<AhuPartition> partitions = JSONArray.parseArray(partitionJson, AhuPartition.class);

            Project project = this.projectService.getProjectById(unit.getPid());
            List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, partitions);
            CalPriceTool.output2File(project.getPriceBase(), getPriceCodeOutputString(priceCodes, casingList));

            generatePriceCodeForTest(priceCodes);

            Map<String, Double> prices = CalPriceTool.calPrice(project.getPriceBase(), language);
            
            if(prices.get(CommonConstant.SYS_MAP_RESULT)==0) {
            	//TODO 暂时忽略价格报错
            	throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED); 	
            }
            priceResultVo = this.generatePriceResultVO(unit, partition, priceCodes, prices);
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        }
        return priceResultVo;
    }

    /**
     * 获取价格系列，39CQ 使用G的面板切割做价格，非CQ不变
     * @param series
     * @return
     */
    private String getSeries(String series){
        if(series.contains(RestConstant.SYS_UNIT_SERIES_39CQ)){
            return RestConstant.SYS_UNIT_SERIES_39G+AhuUtil.getUnitNo(series);
        }else{
            return series;
        }
    }
    /**
     * 计算单个段不同配置参数结果的价格
     * @param unit
     * @param parts
     * @param language
     * @return
     */
    public PriceResultVO calculatePartPrice(Unit unit, List<Part> parts, LanguageEnum language) {
        PriceResultVO priceResultVo = null;
        try {
            Project project = this.projectService.getProjectById(unit.getPid());
            List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, null);
            CalPriceTool.output2File(project.getPriceBase(), getPriceCodeOutputString(priceCodes, null));

            generatePriceCodeForTest(priceCodes);

            Map<String, Double> prices = CalPriceTool.calPrice(project.getPriceBase(), language);
            priceResultVo = this.generatePriceResultVO(unit, null, priceCodes, prices);
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        }
        return priceResultVo;
    }
    private void generatePriceCodeForTest(List<PriceCodePO> priceCodes) {
        try {
            if (Boolean.valueOf(priceCodeTestOutput)) {
                FileUtil.writeFile(SysConstants.TEST_PRICE_CODE_OUTPUT, getPriceCodeTestOutputString(priceCodes));
            }
        } catch (IOException e) {
            log.error("Failed to generate price code for test", e);
        }
    }

    private String getPriceCodeOutputString(List<PriceCodePO> priceCodes, List<PanelCalculationObj> casingList) {
        String lineSeparator = System.getProperty(RestConstant.SYS_PROPERTY_LINE_SEPARATOR);
        StringBuilder outputString = new StringBuilder();
        for (PriceCodePO priceCode : priceCodes) {
            if (EmptyUtil.isNotEmpty(priceCode.getPriceCode())) { // skip empty price code
                outputString.append(String.join(RestConstant.SYS_PUNCTUATION_COMMA, priceCode.getFuncName(),
                        priceCode.getPriceCode(), priceCode.getPartitionNo().toString()));
                outputString.append(lineSeparator);
            }
        }

        // uncomment when casing list is ready
        // @formatter:off
        // generate casing list
        if(EmptyUtil.isNotEmpty(casingList))
        for (PanelCalculationObj casingObj : casingList) {
        	//TODO 忽略底座的计算，每次加入底座后，计算价格报错
        	/*if(casingObj.getCategory().contains(RestConstant.SYS_STRING_BASE)) {
        		continue;
        	}*/
            outputString.append(
                    String.format("%s,%s,%s,%s,%s", EmptyUtil.toString(casingObj.getCategory(), StringUtils.EMPTY),
                            EmptyUtil.toString(casingObj.getPartLM(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getPartWM(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getQuantity(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getMemo(), StringUtils.EMPTY)));
            outputString.append(lineSeparator);
        }
        // @formatter:on
        return outputString.toString();
    }

    private String getPriceCodeTestOutputString(List<PriceCodePO> priceCodes) {
        String lineSeparator = System.getProperty(RestConstant.SYS_PROPERTY_LINE_SEPARATOR);
        StringBuilder outputString = new StringBuilder();
        for (PriceCodePO priceCode : priceCodes) {
            outputString.append(priceCode.getFuncName());
            for (PriceCodeXSLXPO codeRule : priceCode.getCodeRules()) {
                outputString.append(RestConstant.SYS_PUNCTUATION_COMMA);
                outputString
                        .append(String.join(RestConstant.SYS_PUNCTUATION_LOW_HYPHEN, codeRule.getNo(), codeRule.getDescription(), codeRule.getValueDesp()));
            }
            outputString.append(lineSeparator);
        }
        return outputString.toString();
    }
    private PriceResultVO generateEmptyPriceResultVO(Unit unit) {
        PriceResultVO priceResultVo = new PriceResultVO();
        priceResultVo.setWeight(calculateAHUWeight(unit).getTotal());
        priceResultVo.setTotal(0);
        priceResultVo.setDetails(null);
        priceResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION); // not sure about the version
        return priceResultVo;
    }
    @SuppressWarnings("unchecked")
    private PriceResultVO generatePriceResultVO(Unit unit, Partition partition, List<PriceCodePO> priceCodes,
            Map<String, Double> prices) {

        //设计院角色不用计算价格
        if(LicenseManager.isDesign()){
            return generateEmptyPriceResultVO(unit);
        }

        PriceResultVO priceResultVo = new PriceResultVO();
        List<PriceBean> priceBeans = new ArrayList<>();
        Double totalPrice = 0d;
        Double NStdpackingPrice = 0d;//非标总价格
        List<String> multiSectionCodes = getMultiSectionCode(priceCodes);
        Map<String, Object> unitMeta = JSON.parseObject(unit.getMetaJson(), Map.class);
        
        boolean hasW = false;
        Iterator<String> pcs = prices.keySet().iterator();
        while (pcs.hasNext()){
            String pc = pcs.next();
            if(pc.contains("_FUNC_W_")){
                hasW = true;
            }
        }
        for (PriceCodePO priceCode : priceCodes) {
            String funcName = priceCode.getFuncName();
            if (SystemCalculateConstants.BOX_SECTION_CODE.equals(priceCode.getSectionCode())) {
                funcName = priceCode.getFuncNameWithoutNo();
            } else {
                funcName = priceCode.getFuncName();
            }
            double sectionPrice = 0;
            if (prices.containsKey(funcName)) {
                sectionPrice = NumberUtil.scale(prices.get(funcName), 2); // two decimal
                if(funcName.contains("_FUNC_0") && hasW){
                    sectionPrice= AhuUtil.getHeightOfAHU(unit.getSeries())>=18?sectionPrice*SYS_PANEL_18:sectionPrice;
                }
            }
            PriceBean priceBean = new PriceResultVO.PriceBean();
            if (SystemCalculateConstants.BOX_SECTION_CODE.equals(priceCode.getSectionCode())) {
                priceBean.setKey(SystemCalculateConstants.SECTION_BOX);
                priceBean.setName(AHUContext.getIntlString(SystemCalculateConstants.SECTION_BOX));
                priceBean.setWeight(AhuPartitionUtils.calculateAHUContainerWeight(unit, partition));
                unitMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_BOX_PRICE), sectionPrice);
            } else {
                SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeByCode(priceCode.getSectionCode());
                priceBean.setKey(sectionType.getId());
                String name = AHUContext.getIntlString(sectionType.getCnName())
                        + (multiSectionCodes.contains(priceCode.getSectionCode())
                                ? String.valueOf(priceCode.getSectionNo())
                                : StringUtils.EMPTY);
                priceBean.setName(name);
            }

            Part part = priceCode.getPart();
            if (EmptyUtil.isNotEmpty(part)) { // update price of part for late save
                Map<String, Object> partMeta = JSON.parseObject(part.getMetaJson(), Map.class);
                partMeta.put(SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), MetaKey.KEY_PRICE), sectionPrice);
                part.setMetaJson(JSON.toJSONString(partMeta));

                // add ns price of part
                Double tempPartNsPrice = getNSPriceOfPart(part, partMeta);
                sectionPrice += tempPartNsPrice;
                NStdpackingPrice += tempPartNsPrice;

                priceBean.setWeight(AhuUtil.getWeight(part));
            } else { // update box price for late save
                if (EmptyUtil.isNotEmpty(unit)) {
                    unitMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRICE), priceResultVo.getTotal());
                    unit.setMetaJson(JSON.toJSONString(unitMeta));

                    // add ns price of unit
                    Double tempAhuNsPrice = getNSPriceOfUnit(unitMeta);
                    sectionPrice += tempAhuNsPrice;
                    NStdpackingPrice += tempAhuNsPrice;

                }
            }
            priceBean.setPrice(sectionPrice);
            priceBeans.add(priceBean);
            totalPrice += sectionPrice;
        }
        if(EmptyUtil.isNotEmpty(unit)) {
            unit.setPrice(totalPrice); // set total price for late save
            unit.setNsprice(NStdpackingPrice);// 设置非标总价格
        }
        priceResultVo.setWeight(calculateAHUWeight(unit).getTotal());
        priceResultVo.setTotal(totalPrice);
        priceResultVo.setDetails(priceBeans);
        priceResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION); // not sure about the version
        return priceResultVo;
    }

    /**
     * 段非标价格
     * @param part
     * @param partMeta
     * @return
     */
    private double getNSPriceOfPart(Part part, Map<String, Object> partMeta) {
        String sectionId = part.getSectionKey();
        double nsPriceOfPart = 0;

        String nsPriceEnable = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_ENABLE)));
        if(SYS_ASSERT_TRUE.equals(nsPriceEnable)){//允许非标
            String nsPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_PRICE);
            nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsPriceKey)));
        }


        if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionId)) {
            String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)){//湿膜加湿段，变更供应商
                String nsSupplierPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSUPPLIERPRICE);
                nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsSupplierPriceKey)));
            }
        } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionId)) {
            String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)){//高压喷雾加湿段，变更供应商
                String nsSupplierPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSUPPLIERPRICE);
                nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsSupplierPriceKey)));
            }
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
            String nsChangeFan = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEFAN)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeFan)){//风机段，变更风机供应商
                String nsFanPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSFANPRICE);
                nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsFanPriceKey)));
            }

            String nsChangeMotor = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEMOTOR)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeMotor)){//风机段，变更电机功率
                String nsMotorPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSMOTORPRICE);
                nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsMotorPriceKey)));
            }

        }

        return nsPriceOfPart;
    }

    /**
     * ahu非标价格
     * @param unitMeta
     * @return
     */
    private double getNSPriceOfUnit(Map<String, Object> unitMeta) {
        double nsPriceOfUnit = 0;
        String nsPriceEnable = String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_ENABLE)));
        if (SYS_ASSERT_TRUE.equals(nsPriceEnable)){//允许非标
            String nsPriceKey = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_PRICE);
            nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(unitMeta.get(nsPriceKey)));
        }

        String nsPriceDeformation = String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATION)));
        if (SYS_ASSERT_TRUE.equals(nsPriceDeformation)){//ahu变形
            String nsDeformationPrice = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_DEFORMATIONPRICE);
            nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(unitMeta.get(nsDeformationPrice)));
        }

        return nsPriceOfUnit;
    }

    private List<String> getMultiSectionCode(List<PriceCodePO> priceCodes) {
        Map<String, Integer> sectionCodeMap = new HashMap<>();
        for (PriceCodePO priceCode : priceCodes) {
            String sectionCode = priceCode.getSectionCode();
            if (sectionCodeMap.containsKey(sectionCode)) {
                int count = sectionCodeMap.get(sectionCode);
                sectionCodeMap.put(sectionCode, count + 1);
            } else {
                sectionCodeMap.put(sectionCode, 1);
            }
        }
        List<String> multiSectionCodes = new ArrayList<>();
        sectionCodeMap.forEach((sectionCode, count) -> {
            if (count > 1) {
                multiSectionCodes.add(sectionCode);
            }
        });
        return multiSectionCodes;
    }

}
