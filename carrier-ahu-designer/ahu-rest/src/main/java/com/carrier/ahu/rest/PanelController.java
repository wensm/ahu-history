package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.ApiResult;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;

@Api(description = "面板接口")
@RestController
public class PanelController extends AbstractController {
    @Autowired
    AhuService ahuService;
    @Autowired
    PartitionService partitionService;

    @ApiOperation(value="初始化面板切割")
    @RequestMapping(value = "/panel/init/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Object> init(@PathVariable("ahuId") String ahuId,String panelSeries) throws Exception {
        Unit unit = ahuService.findAhuById(ahuId);
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
//        List<AhuPartition> ahuPartitionList = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit,partition);
        Gson gson = new Gson();
        String series = unit.getSeries();
        boolean isChange=false;
        //面板切割页面变形处理。
        if(EmptyUtil.isNotEmpty(panelSeries) && !series.equals(panelSeries)){
            series = panelSeries;
            isChange = true;
            for (AhuPartition ahuPartition : ahuPartitionList) {
                int[] dimension = AhuUtil.getHeightAndWidthOfAHU(panelSeries);
                ahuPartition.setHeight(dimension[0]);
                ahuPartition.setWidth(dimension[1]);

                //清空切割
                //panel
                ahuPartition.setPanels(null);
                //底座
                ahuPartition.setBase("");

                //写入型号变形
                unit.setPanelSeries(panelSeries);
                ahuService.updateAhu(unit, getUserName());
            }
        }

        if(ahuPartitionList.size()>0 && EmptyUtil.isEmpty(ahuPartitionList.get(0).getPanels())) {
            try {
                AhuPartitionGenerator.populatePartitionPanel(series,isChange, ahuPartitionList);
                partition.setPartitionJson(gson.toJson(ahuPartitionList));
                //XT系列处理面板箱体零部件清单统计
                if(series.contains(AHU_PRODUCT_39XT)) {
                    AhuPartitionGenerator.initXTSummary(series,unit,partition);
                }
                this.partitionService.savePartition(partition, getUserName());
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApiException(ErrorCode.PANEL_INIT_ERROR);
            }
        }
        return ApiResult.success();
    }

    /**
     * 初始化面板AB
     * @param panelJson 当前分段面板Json:panelJson
     * @return
     * @throws Exception
     */
    @ApiOperation(value="初始化面板AB")
    @RequestMapping(value = "/panel/initab", method = RequestMethod.POST)
    public ApiResult<Object> initab(String panelJson) throws Exception {
        String initedJson = CommonConstant.SYS_BLANK;
        try {
            initedJson = AhuPartitionGenerator.initab(panelJson);
        } catch (Exception e) {
            String message = getIntlString(e.getMessage());
            if(message.equals(e.getMessage())) {
                throw new ApiException(ErrorCode.PANEL_INITAB_ERROR);
            }else{
                throw new ApiException(message);
            }
        }
        return ApiResult.success(initedJson);
    }

    /**
     * 面板合并
     * @param panel1 第一个面板id
     * @param panel2 第二个面板id
     * @param panelJson 当前操作的面Json
     * @return
     * @throws Exception
     */
    @ApiOperation(value="面板合并")
    @RequestMapping(value = "/panel/mergePanel", method = RequestMethod.POST)
    public ApiResult<Object> mergePanel(String panel1,String panel2,String panelJson,String product) throws Exception {
        String mergedJson = CommonConstant.SYS_BLANK;
        try {
            mergedJson = AhuPartitionGenerator.mergePanel(panel1, panel2,panelJson,product);
        } catch (Exception e) {
            String message = getIntlString(e.getMessage());
            if(message.equals(e.getMessage())) {
                throw new ApiException(ErrorCode.PANEL_MERGEPANEL_ERROR);
            }else{
                throw new ApiException(message);
            }
        }
        return ApiResult.success(mergedJson);
    }
}
