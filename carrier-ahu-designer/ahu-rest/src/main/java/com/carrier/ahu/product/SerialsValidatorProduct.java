package com.carrier.ahu.product;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.util.EmptyUtil;

public class SerialsValidatorProduct implements ValidatorProduct {
	
	@Autowired
	private AhuService ahuService;
	
	public SerialsValidatorProduct(AhuService ahuService) {
		this.ahuService = ahuService;
	}

	public SerialsValidatorProduct() {
	
	}
	
	/**
	 * 机组系列判空。
	 */
	@Override
	public boolean validate(ExportUnit eUnit, Project project, String groupId) {
		boolean result = true;

		String product = eUnit.getProduct();
		if (EmptyUtil.isEmpty(product)) {
			result = false;
		}
	
		return result;
	}

	@Override
	public Map<String, Object> validate1(ExportUnit eUnit, Project project, String groupId) {
		
		return null;
	}

	@Override
	public Map<String, Object> validate2(ExportUnit eUnit, Project project, String groupId) {
		
		return null;
	}
}