package com.carrier.ahu.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.entity.UserMeta;

import lombok.Data;

/**
 * Created by Braden Zhou on 2018/08/14.
 */
@Data
public class UserMetaVO {

    private String userId;
    private String factoryId;
    private Map<String, String> metaData;

    public UserMetaVO() {
    }

    public UserMetaVO(String userId, String factoryId) {
        this.userId = userId;
        this.factoryId = factoryId;
        this.metaData = new HashMap<>();
    }

    public void addUserMeta(List<UserMeta> userMetas) {
        for (UserMeta userMeta : userMetas) {
            this.metaData.put(userMeta.getUserMetaId().getMetaKey(), userMeta.getMetaValue());
        }
    }

}
