package com.carrier.ahu.rest;

import static com.carrier.ahu.rest.BatchCalculateAndConfigTask.getNoDotDouble;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.MatericalConstance;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.engine.cad.AhuExporter;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.metadata.entity.coil.CoilSize;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.UnitType;
import com.carrier.ahu.psychometric.PsyCalBean;
import com.carrier.ahu.psychometric.PsychometricDrawer;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.MatericalConstanceService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.util.ValueFormatUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.ahu.AhuParamUtils;
import com.carrier.ahu.util.meta.AhuMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionValidator;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.ApiResult.ErrorCodeEnum;
import com.carrier.ahu.vo.PartitionEditorVO;
import com.carrier.ahu.vo.ProjectRequestVo;
import com.carrier.ahu.vo.SysConstants;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;

import io.swagger.annotations.Api;
import lombok.Data;

/**
 * ahu单元业务接口 Created by Wen zhengtao on 2017/3/16.
 */
@Api(description = "机组(ahu)相关接口")
@RestController
public class AhuController extends AbstractController {

    @Autowired
    AhuService ahuService;
    @Autowired
    PartitionService partitionService;
    @Autowired
    SectionService sectionService;
    @Autowired
    GroupService groupService;
    @Autowired
    private AhuGroupBindService ahuGroupBindService;
    @Autowired
    MatericalConstanceService matericalConstanceService;
    @Autowired
    CalculatorTool calculatorTool;
    @Autowired
    private AhuStatusTool ahuStatusTool;

    @Autowired
    private AhuExporter exporter;

    /***
     * 分页查询机组列表
     *
     * @param projectId
     * @param groupId
     * @param page
     * @param count
     * @return
     */
    @RequestMapping(value = "ahu/listinpage", method = RequestMethod.GET)
    public ApiResult<List<Page<Unit>>> list(String projectId, String groupId, int page, int count) {
        List<String> gidList = new ArrayList<>();
        if (StringUtils.isEmpty(groupId)) {
            gidList.add(null);// 用于查询为分组的机组，分页方法和未分页方法，参数意义不一致，
            List<GroupInfo> groupInfo = groupService.findGroupList(projectId);
            if (groupInfo != null && !groupInfo.isEmpty()) {
                Iterator<GroupInfo> it = groupInfo.iterator();
                while (it.hasNext()) {
                    gidList.add(it.next().getGroupId());
                }
            }
        } else {
            gidList.add(groupId);
        }
        List<Page<Unit>> pageList = new ArrayList<>();
        Iterator<String> it = gidList.iterator();
        while (it.hasNext()) {
            String theId = it.next();
            Sort sort = new Sort(Direction.ASC, RestConstant.SYS_MSG_RESPONSE_RESULT_UNITNO);
            Pageable pageable = new PageRequest(page - 1, count, sort);
            Page<Unit> list = ahuService.findAhuList(projectId, theId, pageable);
            pageList.add(list);
        }
        return ApiResult.success(pageList);
    }

    /***
     * 不分页查询机组列表
     *
     * @param projectId
     * @param groupId
     * @return
     */
    @RequestMapping(value = "ahu/list", method = RequestMethod.GET)
    public ApiResult<List<Unit>> list(String projectId, String groupId, boolean all) {
        List<Unit> unitAll = new ArrayList<Unit>();
        if (all) {//根据项目ID获取对应项目ID的组列表
            List<Unit> unitList = ahuService.findAhuList(projectId, "");
            if(!EmptyUtil.isEmpty(unitList)){
                unitAll.addAll(unitList);
            }
            if(!EmptyUtil.isEmpty(unitAll)){
                List<Unit> newUnitAll = new ArrayList<Unit>();
                for(Unit unit : unitAll){
                    unit.setLayoutJson(null);
                    unit.setParts(null);
                    newUnitAll.add(unit);
                }
                unitAll = newUnitAll;
            }
        } else {
            unitAll = ahuService.findAhuList(projectId, groupId);
        }
        ListUtils.sort(unitAll,true,"unitNo","drawingNo");
        return ApiResult.success(unitAll);
    }

    /**
     * 根据机组编号查询分段信息
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/partition/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Partition> findPartitionByAhuId(@PathVariable("ahuId") String ahuId) {
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
        // if (EmptyUtil.isEmpty(partition)) {
        // Unit unit = ahuService.findAhuById(ahuId);
        // partition = newPartition(ahuId, unit);
        // }
        return ApiResult.success(partition);
    }

    /**
     * 新增或修改分段
     *
     * @param partition
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "ahu/partition", method = RequestMethod.POST)
    public ApiResult<Partition> saveOrUpdatePartitions(Partition partition) {
        Unit unit = ahuService.findAhuById(partition.getUnitid());

        // 分段保存，进行面板数据清除，保证面板切割每次用最新分段进行初始切割；
        if (EmptyUtil.isNotEmpty(partition.getSaveFrom())
                && partition.getSaveFrom().equals(AhuPartitionGenerator.SAVEFROMPANELPARTION)) {
            List<AhuPartition> ahuPartitionList = JSONArray.parseArray(partition.getPartitionJson(),
                    AhuPartition.class);
            String partitionsJson = AhuPartitionGenerator.clearPanels(ahuPartitionList,unit.getSeries());
            if (EmptyUtil.isNotEmpty(partitionsJson)) {
                partition.setPartitionJson(partitionsJson);
            }
            //需要重新生成三视图
            unit.setNeedReloadThreeView("true");
            //需要重置型号变形
            unit.setPanelSeries(unit.getSeries());
        }
        Partition result = this.partitionService.savePartition(partition, getUserName());

        // 手工分段结束，更新选型状态为完成
        unit.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());

        // calculate price
        List<Part> partList = sectionService.findSectionList(unit.getUnitid());
        calculatorTool.calculateAHUWeight(unit);
//        calculatorTool.calculateAHUPrice(unit, partition, partList, getLanguage());

        //结果转int
        if (EmptyUtil.isNotEmpty(unit.getWeight()))
            unit.setWeight(getNoDotDouble(unit.getWeight()));

        //分段清空价格
        if (EmptyUtil.isNotEmpty(partition.getSaveFrom())
                && partition.getSaveFrom().equals(AhuPartitionGenerator.SAVEFROMPANELPARTION)) {
            unit.setPrice(0d);
        }

        sectionService.updateSections(partList);
        ahuService.updateAhu(unit, getUserName());

        return ApiResult.success(result);
    }

    /**
     * 分段后保存的校验接口
     *
     * @param partition
     * @return
     */
    @RequestMapping(value = "ahu/partition/validate", method = RequestMethod.POST)
    public Map<String, Object> validatePartition(Partition partition) {
        Unit unit = ahuService.findAhuById(partition.getUnitid());
        List<Part> pList = sectionService.findSectionList(partition.getUnitid());
        AhuParam ahuParam = AhuParamUtils.getAhuParam(unit, pList, RestConstant.SYS_BLANK);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, true);
        String message = null;
        try {
            AhuPartitionValidator.validate(partition, unit, ahuParam);
        } catch (ApiException exp) {
            message = AHUContext.getIntlString(exp.getErrorCode().getMessage(), exp.getParams());
            if (!ErrorCode.PARTITION_VALIDATE_LEN.equals(exp.getErrorCode())) { // allow to save, but give warning message
                result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, false);
            }
        }
        result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_MSG, message);
        return result;
    }

    /**
     * 分段后保存的校验接口,废弃！！！
     * 校验分段是否符合规则 1:每个分好的段长不能大于3000，2：两两强关联的段不能拆开
     *
     * @param partition
     * @return
     * @deprecated use "ahu/partition/validate" service instead of.
     */
    @Deprecated
    @RequestMapping(value = "ahu/partitionValidate", method = RequestMethod.POST)
    public Map<String, Object> partitionValidate(Partition partition) {
        Map<String, Object> result = new HashMap<String, Object>();
        // 39CQ
        boolean is39CQ = false;
        boolean is39XT = false;
        Unit unit = ahuService.findAhuById(partition.getUnitid());
        String product = unit.getProduct();
        if (SystemCalculateConstants.AHU_PRODUCT_39CQ.equals(product)) {
            is39CQ = true;
        }
        if (SystemCalculateConstants.AHU_PRODUCT_39XT.equals(product)) {
            is39XT = true;
        }
        boolean enabled = true;
        String msg = RestConstant.SYS_BLANK;
        List<AhuPartition> ahuPartitionList = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        int maxLength = AhuPartitionGenerator.PARTITION_MAX_LENGTH * 100;
        int minAccessLength = AhuPartitionGenerator.ACCESS_MIN_LENGTH * 100;
        int minPartitionWithDoorLength = AhuPartitionGenerator.PARTITION_WITHDOOR_MIN_LENGTH * 100;

        List<Part> pList = sectionService.findSectionList(partition.getUnitid());
        AhuParam aParm = AhuParamUtils.getAhuParam(unit, pList, RestConstant.SYS_BLANK);

        String prePartLastKey = RestConstant.SYS_BLANK;
        // LanguageEnum language = getLanguage();
        int totalPartitionLen = 0;
        for (AhuPartition ap : ahuPartitionList) {

            // 规则:
            if (ap.getLength() > maxLength && !validateOnePartUnsplitable(ap.getSections())) {
                enabled = false;
                msg += getIntlString(I18NConstants.THE) + (ap.getPos() + 1) + getIntlString(I18NConstants.SECTION_LENGTH_SHOULD_NOT_EXCEED)
                        + maxLength;
            }

            Map<String, Object> sectionFirst = ap.getSections().get(0);
            String nextPartFirstKey = String.valueOf(sectionFirst.get(RestConstant.SYS_MAP_METAID));// 当前分段块里面的第一个段

            // 规则:
            if (!RestConstant.SYS_BLANK.equals(prePartLastKey) && AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(nextPartFirstKey)
                    && AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(prePartLastKey)) {
                enabled = false;
                msg += getIntlString(SectionTypeEnum.getSectionTypeFromId(prePartLastKey).getCnName()) + "、"
                        + getIntlString(SectionTypeEnum.getSectionTypeFromId(nextPartFirstKey).getCnName())
                        + getIntlString(I18NConstants.CANNOT_BE_DISMANTLED);
            }

            // 规则: 39CQ机组【空段】选择了门选项且段长小于5M，不能独立作为分成一段出厂，也就是空段的前面或者后面都必须有其他功能段！
            if (is39CQ && ap.getSections().size() == 1 && sectionFirst.get(RestConstant.METACOMMON_SECTIONL).toString().compareTo(RestConstant.SYS_BLANK + minAccessLength) == -1
                    && sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_ACCESS.getId())) {

                String oDoor = getParamsValByKey(sectionFirst, aParm, SectionTypeEnum.TYPE_ACCESS, RestConstant.METASEXON_ACCESS_ODOOR);
                if (!SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(oDoor)) {// 开门
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_ACCESS_LENGH_MSG);
                }

            }

            // 规则: 装箱段不能只有一个【控制段】
            if (ap.getSections().size() == 1 && sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_CTR.getId())) {
                enabled = false;
                msg += getIntlString(I18NConstants.PARTITION_VALIDATE_CTR_EXIST_MSG);
            }

            // 规则: 带顶棚选项且有顶部风阀（【混合段】【出风段】【风机段】），机组的宽度大于等于18M, 整机出厂时，英文版时
            // 需要提示,出厂方式自动默认为开顶箱或者CKD。
            String isprerain = aParm.getParams().get(RestConstant.METAHU_ISPRERAIN).toString();
            String serial = aParm.getParams().get(RestConstant.METAHU_SERIAL).toString();
            String serialWidth = serial.substring(serial.length() - 2);
            String serialWidthHeight = serial.substring(serial.length() - 4);
            if (RestConstant.SYS_ASSERT_TRUE.equals(isprerain) && serialWidth.compareTo("18") > -1) {
                for (Map<String, Object> sec : ap.getSections()) {
                    if (sec.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_MIX.getId())) {
                        String returnTop = getParamsValByKey(sec, aParm, SectionTypeEnum.TYPE_MIX, RestConstant.METASEXON_MIX_RETURNTOP);
                        if (RestConstant.SYS_ASSERT_TRUE.equals(returnTop)) {
                            enabled = false;
                            msg += getIntlString(I18NConstants.PARTITION_VALIDATE_RETURN_TOP_MSG);
                            break;
                        }
                    } else if (sec.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_DISCHARGE.getId())) {
                        String returnTop = getParamsValByKey(sec, aParm, SectionTypeEnum.TYPE_MIX,
                                RestConstant.METASEXON_DISCHARGE_OUTLETDIRECTION);
                        if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(returnTop)) {
                            enabled = false;
                            msg += getIntlString(I18NConstants.PARTITION_VALIDATE_RETURN_TOP_MSG);
                            break;
                        }
                    } else if (sec.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_FAN.getId())) {
                        String returnTop = getParamsValByKey(sec, aParm, SectionTypeEnum.TYPE_MIX, RestConstant.METASEXON_FAN_RETURNPOSITION);
                        String sendTop = getParamsValByKey(sec, aParm, SectionTypeEnum.TYPE_MIX, RestConstant.METASEXON_FAN_SENDPOSITION);

                        if (RestConstant.SYS_ASSERT_TOP.equals(returnTop) || RestConstant.SYS_ASSERT_TOP.equals(sendTop)) {
                            enabled = false;
                            msg += getIntlString(I18NConstants.PARTITION_VALIDATE_RETURN_TOP_MSG);
                            break;
                        }
                    }
                }
            }

            // 规则: 装箱段段长大于等于18M同时机组的宽度大于等于18M，如果选择顶棚，需要提示段长超过集装箱限制，顶棚需现场安装！
            if (RestConstant.SYS_ASSERT_TRUE.equals(isprerain) && serialWidth.compareTo("18") > -1 && ap.getLength() >= 18 * 100) {
                enabled = false;
                msg += getIntlString(I18NConstants.PARTITION_VALIDATE_LEN);
            }

            // 规则: 39CQ0608~1015机组的新回排段不能单独分段。
            if (is39CQ && serialWidthHeight.compareTo("0608") > -1 && serialWidthHeight.compareTo("1015") < 1) {
                if (ap.getSections().size() == 1 && sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())) {
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_COMBINEDMIXINGCHAMBER_SPLIT_MSG);
                }
            }

            // 规则: 39XT系列小于5M段长的带检修门功能段（混合段，出风段，风机段，空段，二次回风段）不能单独分段。
            if (is39XT && ap.getSections().size() == 1
                    && sectionFirst.get(RestConstant.METACOMMON_SECTIONL).toString().compareTo(RestConstant.SYS_BLANK + minPartitionWithDoorLength) == -1) {
                if (sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_MIX.getId())) {
                    // String door =
                    // getParamsValByKey(sectionFirst,aParm,SectionTypeEnum.TYPE_MIX,"meta.section.mix.DoorO");
                    // if(!SystemCalculateConstants.MIX_DOORO_NODOOR.equals(door)){
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_DOOR_MSG);
                    break;
                    // }
                } else if (sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_DISCHARGE.getId())) {
                    // String door =
                    // getParamsValByKey(sectionFirst,aParm,SectionTypeEnum.TYPE_DISCHARGE,"meta.section.discharge.ODoor");
                    // if(!SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(door)){
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_DOOR_MSG);
                    break;
                    // }
                } else if (sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_FAN.getId())) {// 风机必须开门
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_DOOR_MSG);
                    break;
                } else if (sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_ACCESS.getId())) {
                    // String door =
                    // getParamsValByKey(sectionFirst,aParm,SectionTypeEnum.TYPE_ACCESS,"meta.section.access.ODoor");
                    // if(!SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(door)){
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_DOOR_MSG);
                    break;
                    // }
                } else if (sectionFirst.get(RestConstant.SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())) {
                    // //新风侧开门
                    // String doorIS =
                    // getParamsValByKey(sectionFirst,aParm,SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER,"meta.section.combinedMixingChamber.ISDoor");
                    // //排风侧开门
                    // String doorOS =
                    // getParamsValByKey(sectionFirst,aParm,SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER,"meta.section.combinedMixingChamber.OSDoor");
                    // if(!"ND".equals(doorIS) || !"ND".equals(doorOS)){
                    enabled = false;
                    msg += getIntlString(I18NConstants.PARTITION_VALIDATE_DOOR_MSG);
                    break;
                    // }
                }
            }

            // 规则: 由于现场段连接操作困难,两个都不带检修门功能段之间禁止分段!
            if (!RestConstant.SYS_BLANK.equals(prePartLastKey) && !AhuPartitionGenerator.SECTION_WITH_DOOR.contains(nextPartFirstKey)
                    && !AhuPartitionGenerator.SECTION_WITH_DOOR.contains(prePartLastKey)) {
                enabled = false;
                msg += getIntlString(SectionTypeEnum.getSectionTypeFromId(prePartLastKey).getCnName()) + "、"
                        + getIntlString(SectionTypeEnum.getSectionTypeFromId(nextPartFirstKey).getCnName())
                        + getIntlString(I18NConstants.CANNOT_BE_DISMANTLED);
            }

            // 规则: 由于现场段连接操作困难,过滤段前或者侧抽过滤段前后禁止分段!
            if (!RestConstant.SYS_BLANK.equals(prePartLastKey) && (SectionTypeEnum.TYPE_SINGLE.getId().equals(nextPartFirstKey)
                    || (SystemCalculateConstants.FILTER_MEDIALOADING_SIDELOADING.equals(getParamsValByKey(sectionFirst, aParm,
                    SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, RestConstant.METASEXON_FILTER_MEDIALOADING))
                    && (SectionTypeEnum.TYPE_SINGLE.getId().equals(prePartLastKey)
                    || SectionTypeEnum.TYPE_SINGLE.getId().equals(nextPartFirstKey))))) {
                enabled = false;
                msg += getIntlString(SectionTypeEnum.getSectionTypeFromId(prePartLastKey).getCnName()) + "、"
                        + getIntlString(SectionTypeEnum.getSectionTypeFromId(nextPartFirstKey).getCnName())
                        + getIntlString(I18NConstants.CANNOT_BE_DISMANTLED);
            }

            // 规则: 控制段需要与风机段作为一段出厂，不能单独分段！
            if (!RestConstant.SYS_BLANK.equals(prePartLastKey) && SectionTypeEnum.TYPE_CTR.getId().equals(prePartLastKey)
                    && SectionTypeEnum.TYPE_FAN.getId().equals(nextPartFirstKey)) {
                enabled = false;
                msg += getIntlString(SectionTypeEnum.getSectionTypeFromId(prePartLastKey).getCnName()) + "、"
                        + getIntlString(SectionTypeEnum.getSectionTypeFromId(nextPartFirstKey).getCnName())
                        + getIntlString(I18NConstants.CANNOT_BE_DISMANTLED);
            }

            totalPartitionLen += ap.getLength();
            prePartLastKey = String.valueOf(ap.getSections().get(ap.getSections().size() - 1).get(RestConstant.SYS_MAP_METAID));// 上个分段块里面的最后一个段
        }
        // 规则: 39CQ机组单独分段长度小于6M, 建议改成CKD出厂。
        if (enabled && is39CQ && totalPartitionLen < 6 * 100) {// ?是否加入enabled？
            enabled = false;
            msg += getIntlString(I18NConstants.PARTITION_VALIDATE_LEN_TOTAL);
        }

        result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, enabled);
        result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_MSG, msg);
        return result;
    }

    private String getParamsValByKey(Map<String, Object> sec, AhuParam aParm, SectionTypeEnum typeMix, String key) {
        for (int i = 0; i < aParm.getPartParams().size(); i++) {
            PartParam tempAccess = aParm.getPartParams().get(i);
            if (tempAccess.getKey().equals(typeMix.getId()) // 段type相同
                    && tempAccess.getPosition() - 1 == Short.parseShort(sec.get(RestConstant.SYS_MAP_POS).toString())) {// 位置相等
                return String.valueOf(tempAccess.getParams().get(key));
            }
        }
        return null;
    }

    /**
     * 校验分段块里面的所有段是否都是不允许拆开的段
     *
     * @param sections
     * @return
     */
    private boolean validateOnePartUnsplitable(List<Map<String, Object>> sections) {
        boolean ret = true;
        for (Map<String, Object> map : sections) {
            String partKey = String.valueOf(map.get(RestConstant.SYS_MAP_METAID));
            if (!RestConstant.SYS_BLANK.equals(partKey) && !AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(partKey)) {
                ret = false;
            }
        }
        return ret;
    }

    /**
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/fullpartition/{ahuId}", method = RequestMethod.GET)
    public ApiResult<PartitionEditorVO> findFullPartitionInfoByAhuId(@PathVariable("ahuId") String ahuId) {
        Unit unit = ahuService.findAhuById(ahuId);
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
        List<Part> partList = sectionService.findSectionList(ahuId);
        if (EmptyUtil.isEmpty(partition)) {
            partition = AhuPartitionGenerator.generatePartition(ahuId, unit, partList);
        }
        PartitionEditorVO result = new PartitionEditorVO();
        result.setParts(partList);
        result.setUnit(unit);
        result.setPartition(partition);
        return ApiResult.success(result);
    }

    /**
     * 查看机组下段详细信息
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/{ahuId}", method = RequestMethod.GET)
    public ApiResult<AhuResponseVO> findById(@PathVariable("ahuId") String ahuId) {
        AhuResponseVO vo = new AhuResponseVO();
        Unit unit = ahuService.findAhuById(ahuId);
        List<Part> parts = sectionService.findSectionList(ahuId);
        Collections.sort(parts, new Comparator<Part>() {
            public int compare(Part arg0, Part arg1) {
                return arg0.getPosition().compareTo(arg1.getPosition());
            }
        });
        // set template id
        if (EmptyUtil.isNotEmpty(unit.getGroupId())) {
            GroupInfo groupInfo = groupService.findGroupById(unit.getGroupId());
            if (groupInfo != null) {
                vo.setTemplateId(groupInfo.getGroupType());
            }
        } else { // find a match group type based on unit layout
            GroupTypeEnum groupType = AhuLayoutUtils.findGroupType(unit);
            vo.setTemplateId(groupType.getId());
        }
        vo.setUnit(unit);
        vo.setParts(parts);
        return ApiResult.success(vo);
    }

    /**
     * 新增或修改AHU</br>
     * 变更项目状态</br>
     *
     * @param unitPara
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "ahu/save", method = RequestMethod.POST)
    public ApiResult<String> saveOrUpdate(Unit unitPara) throws Exception {

        if (EmptyUtil.isEmpty(unitPara.getMount()) || unitPara.getMount() < 1) {
            throw new ApiException(ErrorCode.UNIT_QUANTITY_IS_ILLEGAL);
        }
        if (EmptyUtil.isEmpty(unitPara.getPid())) {
            throw new ApiException(ErrorCode.UNIT_PROJECT_NUMBER_IS_ILLEGAL);
        }

        if (EmptyUtil.isEmpty(unitPara.getUnitNo())) {
            throw new ApiException(ErrorCode.UNIT_DRAWING_NO_ILLEGAL);
        }

        String unitId = RestConstant.SYS_BLANK;
        // 新增：判断groupId，不为空--修改AHU的groupCode
        String groupId = unitPara.getGroupId();

        if (StringUtils.isBlank(unitPara.getUnitid())) {
            Unit unit = TemplateUtil.getTemplateUnit(unitPara);
            UnitUtil.genSerialAndProduct(unit);

            User user = getUser();
            unitId = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
            // 新增AHU, 不需要进行重量和价钱计算
            unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
            if (StringUtils.isNotBlank(groupId)) {
                GroupInfo group = groupService.findGroupById(groupId);
                unit.setGroupCode(group.getGroupCode());
                unit.setUnitid(unitId);
                unit.setLayoutJson(TemplateUtil.getTemplateLayoutString(group.getGroupType()));
                ahuService.addAhu(unit, getUserName());
                MatericalConstance con = matericalConstanceService.getConstanceByProjidAndGroupid(unit.getPid(), groupId);
                List<Part> pList = getNewGroupSections(unit, groupId, group.getGroupCode());
                if (EmptyUtil.isEmpty(con)) {
                    sectionService.addSections(pList);
                } else {
                    try {
                        Map<String, String> conMap = JSON.parseObject(con.getProkey(), HashMap.class);
                        TemplateUtil.getTemplateUnitFromConstance(unit, conMap);
                        ahuService.updateAhu(unit, getUserName());

                        pList = TemplateUtil.getTemplateParts(pList, conMap);
                        List<Part> toAddPartList = new ArrayList<>();
                        for (Part part : pList) {
                            String partid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
                            part.setPartid(partid);
                            part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
                            toAddPartList.add(part);
                        }
                        sectionService.addSections(toAddPartList);
                    } catch (Exception e) {
                        logger.error("新建AHU的时候，根据批量配置格式化创建段信息出错了");
                    }
                }

                AhuGroupBind bind = new AhuGroupBind();
                bind.setUnitid(unitId);
                bind.setGroupid(groupId);
                ahuGroupBindService.addBind(bind, getUserName());
            } else {
                unit.setGroupId(null);
                unit.setUnitid(unitId);
                ahuService.addAhu(unit, getUserName());
            }
        } else {
            //修改值维护基本的ahu信息即可
            Unit unit = ahuService.findAhuById(unitPara.getUnitid());
            unit.setName(unitPara.getName());
            unit.setDrawingNo(unitPara.getDrawingNo());
            unit.setMount(unitPara.getMount());
            unit.setUnitNo(unitPara.getUnitNo());
            String ahuJsonStr=unit.getMetaJson();
           
            String oldSeries=unit.getSeries();
            String oldProduct=unit.getProduct();
            String newProduct=unitPara.getProduct();
            
            if(!oldProduct.equals(newProduct)) {
            	
            	if(StringUtils.isNotEmpty(oldSeries)) {
                	String newSeries=oldSeries.replace(oldProduct, newProduct);
                	unit.setSeries(newSeries);
                }
                
                unit.setProduct(newProduct);
                
                JSONObject ahuJson=(JSONObject) JSON.parse(ahuJsonStr);
                ahuJson.put(CommonConstant.METAHU_PRODUCT, newProduct);
                ahuJson.put(CommonConstant.METAHU_SERIAL, "");
                ahuJson.put(CommonConstant.METAHU_WIDTH, "");
                ahuJson.put(CommonConstant.METAHU_HEIGHT, "");
                ahuJson.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);
                unit.setMetaJson(JSON.toJSONString(ahuJson));
                unit.setPrice(0d);
                unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
            	
            }
            
            
            unitId = ahuService.updateAhu(unit, getUserName());

            /*if (StringUtils.isBlank(groupId)) {
                unitPara.setGroupId(null);//切换默认组更新gcode
                ahuGroupBindService.deleteBind(unitPara.getUnitid());
                unitId = ahuService.updateAhu(unitPara, getUserName());
            } else {
                GroupInfo group = groupService.findGroupById(groupId);
                unitPara.setGroupCode(group.getGroupCode());//切换组更新gcode
                unitId = ahuService.updateAhu(unitPara, getUserName());

                //不需要修改进行清空
                sectionService.deleteAHUSections(unitId);
                sectionService.addSections(getNewGroupSections(unit, groupId, group.getGroupCode()));
            }*/
        }

        ahuStatusTool.syncStatus(unitPara.getPid(), getUserName());
        return ApiResult.success(unitId);
    }

    /**
     * 删除机组</br>
     * 项目状态变动</br>
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/delete/{ahuId}", method = RequestMethod.POST)
    public ApiResult<Boolean> delete(@PathVariable("ahuId") String ahuId) {
        Unit unit = ahuService.findAhuById(ahuId);
        if (EmptyUtil.isNotEmpty(unit)) {
            ahuService.deleteAhu(ahuId);
            ahuGroupBindService.deleteBind(ahuId);
            sectionService.deleteAHUSections(ahuId);
            Partition partition = partitionService.findPartitionByAHUId(ahuId);
            if (partition != null) {
                partitionService.deletePartition(partition.getPartitionid());
            }
            ahuStatusTool.syncStatus(unit.getPid(), getUserName());
        } else {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        return ApiResult.success(true);
    }

    /**
     * AHU 复制，编号追加<br>
     * 项目状态不变</br>
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/copy/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Boolean> copy(@PathVariable("ahuId") String ahuId) {
        Unit ahu = ahuService.findAhuById(ahuId);

        Unit ahuNew = new Unit();
        ahuNew.setCreateInfo(getUserName());
        BeanUtils.copyProperties(ahu, ahuNew);
        try {
            String newUnitNO = ValueFormatUtil.getUsefulUnitNO(ahuService.findAllUnitNOInuse(ahu.getPid()), -1);
            if (EmptyUtil.isEmpty(newUnitNO)) {
                throw new ApiException(ErrorCode.UNIT_DRAWING_NO_ILLEGAL);
            }
            ahuNew.setUnitNo(newUnitNO);
        } catch (Exception e) {
            throw new ApiException(ErrorCode.UNIT_DRAWING_NO_ILLEGAL, e);
        }

        User user = getUser();
        String unitId = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
        ahuNew.setUnitid(unitId);
        ahuNew.setName(RestConstant.SYS_UNIT_NAME_COPY + ahu.getName());

        List<Part> partList = sectionService.findSectionList(ahu.getUnitid());
        if (EmptyUtil.isNotEmpty(partList)) {
            List<Part> partListCopied = new ArrayList<>();
            for (Part part : partList) {
                Part partCopied = new Part();
                BeanUtils.copyProperties(part, partCopied);
                String partId = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
                partCopied.setPartid(partId);
                partCopied.setUnitid(unitId);
                partCopied.setCreateInfo(getUserName());
                partListCopied.add(partCopied);
            }
            sectionService.addSections(partListCopied);
        }
        ahuService.addAhu(ahuNew, getUserName());
        String groupId = ahu.getGroupId();
        if (EmptyUtil.isNotEmpty(groupId)) {
            AhuGroupBind bind = new AhuGroupBind();
            bind.setUnitid(ahuNew.getUnitid());
            bind.setGroupid(groupId);
            ahuGroupBindService.addBind(bind, getUserName());
        }

        return ApiResult.success(true);
    }

    @RequestMapping(value = "ahu/type", method = RequestMethod.GET)
    public ApiResult<List<UnitType>> getAhuTypes() {
        List<UnitType> unitTypes = ahuService.getUnitTypes();
        return ApiResult.success(unitTypes);
    }

    @RequestMapping(value = "ahu/psychometric/{ahuId}", method = RequestMethod.GET)
    public ApiResult<String> getPsychometricThreeView(@PathVariable("ahuId") String ahuId) {
        Unit ahu = ahuService.findAhuById(ahuId);
        List<Part> pList = sectionService.findSectionList(ahuId);
        AhuParam aParm = AhuParamUtils.getAhuParam(ahu, pList, RestConstant.SYS_BLANK);
        List<PartParam> ppList = aParm.getPartParams();
        Iterator<PartParam> it = ppList.iterator();
        List<PsyCalBean> datas = PsychometricDrawer.drawPsy(it);

        String path = PsychometricDrawer.genPsychometric(ahu.getPid(), ahu.getUnitid(), datas);
        return ApiResult.success(RestConstant.SYS_PATH_FILES + path);
    }

    /**
     * 创建三视图
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/cad/threeview/{ahuId}", method = RequestMethod.GET)
    public ApiResult<String> getCadThreeView(@PathVariable("ahuId") String ahuId) {
        getLanguage();// 执行getLanguage()获取最新语言
        Unit ahu = ahuService.findAhuById(ahuId);
        List<PartPO> partList = sectionService.findLinkedSectionsList(ahuId);
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
        AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(ahu, partList, partition);

        //获取分段保存后,ahu的panelThreeView 是否为true
        if(EmptyUtil.isNotEmpty(ahu.getNeedReloadThreeView())
                && ahu.getNeedReloadThreeView().equals("true")){
            ahuParam.setNeedReload3View(true);
            ahu.setNeedReloadThreeView("false");
            ahuService.updateAhu(ahu, getUserName());
        }

        String url = exporter.createBmp(ahuParam);
        return ApiResult.success(RestConstant.SYS_PATH_FILES + url);
    }

    /**
     * 下载三视图
     *
     * @param ahuId
     * @param type  类型:dwg 其他全为bmp
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "ahu/cad/threeview/export/{ahuId}/{type}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> groupExport(@PathVariable("ahuId") String ahuId, @PathVariable("type") String type) throws Exception {
        String fileName = String.valueOf(System.currentTimeMillis());
        if (RestConstant.SYS_DWG.equals(type)) {
            fileName = fileName + RestConstant.SYS_DWG_EXTENSION;
        } else {
            fileName = fileName + RestConstant.SYS_BMP_EXTENSION;
        }

        Unit ahu = ahuService.findAhuById(ahuId);
        List<PartPO> partList = sectionService.findLinkedSectionsList(ahuId);
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
        AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(ahu, partList, partition);

        String filePath = exporter.download(ahuParam, fileName);
        return download(SysConstants.ASSERT_DIR + filePath, fileName);
    }

    /**
     * Return sorted AhuSizeDetail list and move unselectable item to the bottom.
     *
     * @param airVolume
     * @return
     */
    @RequestMapping(value = "ahuseries", method = RequestMethod.GET)
    public List<AhuSizeDetail> getAhuSerialList(String unitSeries, int airVolume) {
        List<AhuSizeDetail> ahuSizeDetails = AhuMetaUtils.getAhuSizeDetails(unitSeries);
        if (airVolume > 0) {
            List<AhuSizeDetail> sortedAhuSizeDetails = new ArrayList<>();
            List<AhuSizeDetail> unSelectableAhuSizeDetails = new ArrayList<>();
            for (AhuSizeDetail ahuSerialD : ahuSizeDetails) {
                double velocity = airVolume / (ahuSerialD.getCoilFaceArea() * 3600);
                if (velocity > SystemCalculateConstants.UNSELECTABLE_AHU_FACE_VOLUME_THRESHOLD) {
                    unSelectableAhuSizeDetails.add(ahuSerialD);
                } else {
                    sortedAhuSizeDetails.add(ahuSerialD);
                }
            }
            sortedAhuSizeDetails.addAll(unSelectableAhuSizeDetails);
            return sortedAhuSizeDetails;
        }
        return ahuSizeDetails;
    }

    @RequestMapping(value = "ahuserialcoil", method = RequestMethod.GET)
    public List<CoilSize> getAhuSerialCoilList() {
        return AhuMetadata.findAll(CoilSize.class);
    }

    /**
     * 插入AHU</br>
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/insert/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Boolean> insert(@PathVariable("ahuId") String ahuId) {
        Unit ahu = ahuService.findAhuById(ahuId);
        if (EmptyUtil.isEmpty(ahu)) {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        User user = getUser();
        String originUnitNo = ahu.getUnitNo();
        Unit ahuNew = new Unit();// 新增的AHU
        BeanUtils.copyProperties(ahu, ahuNew);
        ahuNew.setName(RestConstant.SYS_UNIT_NAME_BREAKIN + ahu.getName());
        ahuNew.setCreateInfo(getUserName());
        String newUnitNo = ValueFormatUtil.transNumberic2String(1 + Integer.parseInt(originUnitNo), 3);
        ahuNew.setUnitNo(newUnitNo);
        String unitId = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
        ahuNew.setUnitid(unitId);

        List<Unit> originUnitList = ahuService.findAhuList(ahu.getPid(), null);

        Collections.sort(originUnitList, new Comparator<Unit>() {
            public int compare(Unit o1, Unit o2) {
                return o1.getUnitNo().compareTo(o2.getUnitNo());
            }
        });
        List<Unit> toUpdateList = new ArrayList<>();

        int tempNo = Integer.parseInt(originUnitNo) + 1;
        for (Unit unit : originUnitList) {
            int unitNo = Integer.parseInt(unit.getUnitNo());
            if (unitNo == tempNo) {
                Unit ahuNeedUpdateNo = new Unit();
                BeanUtils.copyProperties(unit, ahuNeedUpdateNo);
                ahuNeedUpdateNo.setUnitNo(ValueFormatUtil.transNumberic2String(1 + tempNo, 3));
                ahuNeedUpdateNo.setUpdateInfo(getUserName());
                toUpdateList.add(ahuNeedUpdateNo);
                tempNo++;
                continue;
            } else if (unitNo < tempNo) {
                continue;
            } else {
                break;
            }
        }
        ahuService.addOrUpdateAhus(toUpdateList);

        String groupId = ahu.getGroupId();
        if (EmptyUtil.isEmpty(groupId)) {
            String groupCode = ahu.getGroupCode();
            if (StringUtils.isNotBlank(groupCode)) {
                List<Part> partList = getNewGroupSections(ahuNew, groupId, groupCode);
                sectionService.addSections(partList);
            }
        } else {
            AhuGroupBind bind = new AhuGroupBind();
            bind.setUnitid(ahuNew.getUnitid());
            bind.setGroupid(groupId);
            ahuGroupBindService.addBind(bind, getUserName());

            GroupInfo group = groupService.findGroupById(groupId);
            List<Part> partList = getNewGroupSections(ahuNew, groupId, group.getGroupCode());
            sectionService.addSections(partList);
        }
        ahuNew.setRecordStatus(AhuStatusEnum.SELECTING.getId());
        ahuService.addAhu(ahuNew, getUserName());// 插入的ahu状态为选型中

        return ApiResult.success(true);
    }

    /**
     * 根据ahuId查询所有段信息
     *
     * @param ahuId
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "ahu/getsection/{ahuId}", method = RequestMethod.GET)
    public ApiResult<List<Part>> getsections(@PathVariable("ahuId") String ahuId) {
        List<Part> parts = sectionService.findSectionList(ahuId);
        if (EmptyUtil.isEmpty(parts)) {
            parts = new ArrayList<>();
        }
        Map<String, Object> materialDefaultValueMap = TemplateUtil.getAllMaterialDefaultValue(parts);
        for (Part part : parts) {
            Map<String, Object> partMaterialMap = new LinkedHashMap<>();
            Map<String, Object> partMetaJsonMap = JSON.parseObject(part.getMetaJson(), HashMap.class);
            materialDefaultValueMap.forEach((key, value) -> {
                if (partMetaJsonMap.containsKey(key)) { // is material property
                    partMaterialMap.put(key, partMetaJsonMap.get(key));
                }
            });
            part.setMetaJson(JSON.toJSONString(partMaterialMap));
        }
        return ApiResult.success(parts);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "ahu/metadata/material/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Unit> getMaterialMetadataOfUnitAndParts(@PathVariable("ahuId") String ahuId) {
        Unit unit = this.ahuService.findAhuById(ahuId);
        if (unit == null) {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        // handle material property for unit
        Map<String, Object> allMaterialDefaultValueMap = AhuSectionMetas.getInstance().getAllMaterialDefaultValue();
        Map<String, Object> unitMaterialMap = new LinkedHashMap<>();
        Map<String, Object> unitMetaJsonMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
        allMaterialDefaultValueMap.forEach((key, value) -> {
            if (unitMetaJsonMap.containsKey(key)) { // is material property
                unitMaterialMap.put(key, unitMetaJsonMap.get(key));
            }
        });
        unit.setMetaJson(JSON.toJSONString(unitMaterialMap));

        // handle material property for parts
        List<Part> parts = sectionService.findSectionList(ahuId);
        if (EmptyUtil.isEmpty(parts)) {
            parts = new ArrayList<>();
        }
        Map<String, Object> materialDefaultValueMap = TemplateUtil.getAllMaterialDefaultValue(parts);
        for (Part part : parts) {
            Map<String, Object> partMaterialMap = new LinkedHashMap<>();
            Map<String, Object> partMetaJsonMap = JSON.parseObject(part.getMetaJson(), HashMap.class);
            materialDefaultValueMap.forEach((key, value) -> {
                if (partMetaJsonMap.containsKey(key)) { // is material property
                    partMaterialMap.put(key, partMetaJsonMap.get(key));
                }
            });
            part.setMetaJson(JSON.toJSONString(partMaterialMap));
        }
        unit.setParts(parts);

        return ApiResult.success(unit);
    }

    /**
     * AHU移组</br>
     * 项目状态不变</br>
     * 若目标分组没有段序，则以当前机组的段序为改分组的段序</br>
     *
     * @param ahuId
     * @param newgroupid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "ahu/resetgroup/{ahuId}/{newgroupid}", method = RequestMethod.GET)
    public ApiResult<Boolean> resetGroup(@PathVariable("ahuId") String ahuId, @PathVariable("newgroupid") String newgroupid)
            throws Exception {
        Unit unit = ahuService.findAhuById(ahuId);

        if (GroupTypeEnum.TYPE_UNGROUP.getId().equals(newgroupid)) {
            // 移动到未分组
            if (EmptyUtil.isNotEmpty(unit.getGroupId())) {
                ahuGroupBindService.deleteBind(ahuId);
            }
            unit.setGroupId(null);
            unit.setPaneltype(null);
            ahuService.updateAhu(unit, getUserName());
            return ApiResult.success(true);
        }
        GroupInfo group = groupService.findGroupById(newgroupid);
        if (EmptyUtil.isEmpty(group)) {
            throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
        } else {
            AhuGroupBind bind = new AhuGroupBind();
            bind.setUnitid(ahuId);
            bind.setGroupid(newgroupid);
            if (StringUtils.isNotBlank(unit.getGroupId())) {
                ahuGroupBindService.deleteBind(ahuId);
            }
            unit.setGroupId(group.getGroupId());

            // 分组下的组编码
            String groupCodeFromGroup = group.getGroupCode();
            // AHU下的组编码
            String groupCodeFromUnit = unit.getGroupCode();
            // 若为空，则将AHU的组编码赋值给分组
            if (EmptyUtil.isEmpty(groupCodeFromGroup)) {
                if (EmptyUtil.isEmpty(groupCodeFromUnit)) {

                } else {
                    group.setGroupCode(groupCodeFromUnit);
                    groupService.updateGroup(group, getUserName());
                }
            } else {
                unit.setGroupCode(group.getGroupCode());
            }

            ahuService.updateAhu(unit, getUserName());
            ahuGroupBindService.addBind(bind, getUserName());

            // 创建信的段
            // sectionService.deleteAHUSections(ahuId);
            // sectionService.addSections(getNewGroupSections(unit,
            // group.getGroupId(), group.getGroupCode()));

            // 升级所有的段的分组Id
            List<Part> sectionList = sectionService.findSectionList(ahuId);
            if (EmptyUtil.isNotEmpty(sectionList)) {
                List<Part> sectionListToUpdate = new ArrayList<>();
                for (Part part : sectionList) {
                    part.setGroupi(newgroupid);
                    sectionListToUpdate.add(part);
                }
                sectionService.addSections(sectionListToUpdate);
            }

            return ApiResult.success(true);
        }
    }

    /**
     * 更新机组客户PO编号
     *
     * @param ahuId
     * @param customerPO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "ahu/resetCustomerPO", method = RequestMethod.POST)
    public ApiResult<Boolean> resetCustomerPO(String ahuId, String customerPO) throws Exception {
        Unit unit = null;
        if (EmptyUtil.isNotEmpty(ahuId)) {
            unit = ahuService.findAhuById(ahuId);
        }
        if (unit != null) {
            unit.setCustomerName(customerPO);
            this.ahuService.updateAhu(unit, getUserName());
        } else {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        return ApiResult.success(true);
    }

    /**
     * 设置段连接清单类型
     *
     * @param ahuId
     * @param connexonlistType
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "ahu/setConnexonListType", method = RequestMethod.POST)
    public ApiResult<Boolean> setConnexonListType(String ahuId, String paneltype) throws Exception {
        Unit unit = null;
        if (EmptyUtil.isNotEmpty(ahuId)) {
            unit = ahuService.findAhuById(ahuId);
        }
        if (unit != null) {
            if (EmptyUtil.isEmpty(paneltype)) {
                paneltype = RestConstant.JSON_UNIT_PANELTYPE_STANDARD;//默认为标准
            }
            unit.setPaneltype(paneltype);
            this.ahuService.updateAhu(unit, getUserName());
        } else {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        return ApiResult.success(true);
    }

    /**
     * 根据组编码 生成新的段信息
     *
     * @param groupCode
     * @param unit
     * @param groupId
     * @return
     */
    private List<Part> getNewGroupSections(Unit unit, String groupId, String groupCode) {
        if (EmptyUtil.isEmpty(unit)) {
            return null;
        }
        if (EmptyUtil.isEmpty(unit.getUnitid())) {
            return null;
        }
        if (EmptyUtil.isEmpty(unit.getPid())) {
            return null;
        }
        if (EmptyUtil.isEmpty(unit.getGroupCode())) {
            return null;
        }
        if (StringUtils.isBlank(groupCode)) {
            groupCode = unit.getGroupCode();
        } else {
            if (!groupCode.equals(unit.getGroupCode())) {
                return null;
            }
        }

        List<String> sectionIds = MetaCodeGen.translateGroupCodeToIds(groupCode);
        List<Part> parts = new ArrayList<>();
        Iterator<String> it = sectionIds.iterator();
        int pos = 1;
        User user = getUser();
        while (it.hasNext()) {
            String partid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
            String key = it.next();
            Part p = new Part();
            p.setPartid(partid);
            p.setPid(unit.getPid());
            p.setUnitid(unit.getUnitid());
            try {
                p.setGroupi(groupId);
            } catch (Exception e) {
                p.setGroupi(null);
            }
            p.setSectionKey(key);
            p.setPosition((short) (pos));
            p.setCreateInfo(getUserName());
            p.setRecordStatus(AhuStatusEnum.SELECTING.getId());
            p.setMetaJson(JSON.toJSONString(TemplateUtil.genAhuSectionTemplate(key)));
            parts.add(p);
            pos = pos + 1;
        }
        return parts;
    }

    /**
     * 变更图纸编号
     *
     * @param ahuId
     * @return
     */
    @RequestMapping(value = "ahu/resetno/{ahuId}/{drawingNo}", method = RequestMethod.GET)
    public ApiResult<Map<String, String>> resetdrawingno(@PathVariable("ahuId") String ahuId, @PathVariable("drawingNo") String drawingNo) {
        Map<String, String> map = new HashMap<String, String>();

        int drawingNO = -1;
        try {
            drawingNO = Integer.parseInt(drawingNo);
            if (drawingNO > 999 || drawingNO < 1) {
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.NUMBER_1_999));
                return ApiResult.success(map);
            }
        } catch (Exception e) {
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.NUMBER_1_999));
            return ApiResult.success();
        }
        Unit tochangeUnit = ahuService.findAhuById(ahuId);
        Unit unit = ahuService.findAhuByDrawingNO(tochangeUnit.getPid(), drawingNo);
        if (EmptyUtil.isNotEmpty(unit)) {
            if (unit.getUnitid().equals(ahuId)) {
                logger.info("unitId : " + unit.getUnitid() + " equals ahuId : " + ahuId);
                return ApiResult.success(map);
            } else {
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.OCCUPIED));
                return ApiResult.success(map);
            }
        }
        try {
            tochangeUnit.setUnitNo(ValueFormatUtil.getUsefulUnitNO(ahuService.findAllUnitNOInuse(tochangeUnit.getPid()), drawingNO));
        } catch (Exception e) {
            e.printStackTrace();
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.ILLEGAL));
            return ApiResult.success(map);
        }
        ahuService.updateAhu(tochangeUnit, getUserName());
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.SUCCESS.getCode());
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_UNITNO, tochangeUnit.getUnitNo());
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, drawingNo);
        return ApiResult.success(map);
    }

    /**
     * 检查图纸编号是否可用 <br>
     *
     * @param drawingNo
     * @return
     */
    @RequestMapping(value = "ahu/checkno/{drawingNo}/{projectid}", method = RequestMethod.GET)
    public ApiResult<Map<String, String>> checkdrawingno(@PathVariable("drawingNo") String drawingNo,
                                                         @PathVariable("projectid") String projectid) {
        Map<String, String> map = new HashMap<String, String>();
        int drawingNO = -1;
        try {
            drawingNO = Integer.parseInt(drawingNo);
            if (drawingNO > 999 || drawingNO < 1) {
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
                map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.NUMBER_1_999));
                return ApiResult.success(map);
            }
        } catch (Exception e) {
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.NUMBER_1_999));
            return ApiResult.success(map);
        }
        Unit unit = ahuService.findAhuByDrawingNO(projectid, drawingNo);
        if (EmptyUtil.isNotEmpty(unit)) {
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.OCCUPIED));
            return ApiResult.success(map);
        }
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.SUCCESS.getCode());
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_UNITNO, ValueFormatUtil.formatUnitNO(drawingNo));
        map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, drawingNo);
        return ApiResult.success(map);
    }

    /**
     * 获取当前可用机组编号图纸编号 <br>
     *
     * @return
     */
    @RequestMapping(value = "ahu/getusefulnum/{projectid}", method = RequestMethod.GET)
    public ApiResult<Map<String, String>> getUsefulNO(@PathVariable("projectid") String projectid) {
        Map<String, String> map = new HashMap<String, String>();
        try {
            String unitNO = ValueFormatUtil.getUsefulUnitNO(ahuService.findAllUnitNOInuse(projectid), -1);
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_UNITNO, unitNO);
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, RestConstant.SYS_BLANK + Integer.parseInt(unitNO));
            return ApiResult.success(map);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_UNITNO, RestConstant.SYS_BLANK);
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO, RestConstant.SYS_BLANK);
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_TYPE, ErrorCodeEnum.ERROR.getCode());
            map.put(RestConstant.SYS_MSG_RESPONSE_RESULT_ERRMSG, getIntlString(I18NConstants.CALCULATION_ABNORMAL));
            return ApiResult.success(map);
        }
    }

    /**
     * 项目上全局配置检修门方向、接管方向<br>
     * l、left、L、Left >> left<br>
     * r、right、R、Right >> right<br>
     *
     * @param vo
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "project/setdirection", method = RequestMethod.POST)
    public ApiResult<Boolean> setDirection(ProjectRequestVo vo) {
        String projectId = vo.getProjectId();
        if (StringUtils.isBlank(projectId)) {
            throw new ApiException(ErrorCode.PROJECT_ID_IS_EMPTY);
        }
        String door = vo.getDoororientation();
        String pipe = vo.getPipeorientation();
        String ahuId[] = vo.getAhuIds();

        List<Unit> unitList = ahuService.findAhuList(projectId, null);
        List<Unit> unitList2Update = new ArrayList<>();

        for (Unit unit : unitList) {
            if (isNeedSetDirection(ahuId, unit.getUnitid())) {
                Gson gson = new Gson();
                Map<String, Object> map = gson.fromJson(unit.getMetaJson(), Map.class);
                if ("l".equalsIgnoreCase(door) || "left".equalsIgnoreCase(door)) {
                    map.put(RestConstant.METAHU_DOORORIENTATION, SystemCalculateConstants.AHU_DOORORIENTATION_LEFT);
                } else if ("r".equalsIgnoreCase(door) || "right".equalsIgnoreCase(door)) {
                    map.put(RestConstant.METAHU_DOORORIENTATION, SystemCalculateConstants.AHU_DOORORIENTATION_RIGHT);
                }
                if ("l".equalsIgnoreCase(pipe) || "left".equalsIgnoreCase(pipe)) {
                    map.put(RestConstant.METAHU_PIPEORIENTATION, SystemCalculateConstants.AHU_PIPEORIENTATION_LEFT);
                } else if ("r".equalsIgnoreCase(pipe) || "right".equalsIgnoreCase(pipe)) {
                    map.put(RestConstant.METAHU_PIPEORIENTATION, SystemCalculateConstants.AHU_PIPEORIENTATION_RIGHT);
                }
                unit.setMetaJson(gson.toJson(map));
                unitList2Update.add(unit);
            }
        }
        ahuService.addOrUpdateAhus(unitList2Update);
        return ApiResult.success(true);
    }

    private boolean isNeedSetDirection(String ahuId[], String unitid) {
        if (EmptyUtil.isEmpty(ahuId)) {
            return true;
        }
        List<String> ahuIdList = Arrays.asList(ahuId);
        return ahuIdList.contains(unitid);
    }

    @Data
    public static class AhuResponseVO {
        Unit unit;
        List<Part> parts;
        String templateId;
    }

    /**
     * 重置项目内所有AHU机组编号<br>
     * exp: 1 2 3 6 8 9 11 重置后 1 2 3 4 5 6 7
     *
     * @return
     */
    @RequestMapping(value = "ahu/resetnos/{projectid}", method = RequestMethod.GET)
    public ApiResult<List<String>> resetUnitNos(@PathVariable("projectid") String projectid) {
        List<String> list = ahuService.fixUnitNo(projectid);
        return ApiResult.success(list);
    }
}
