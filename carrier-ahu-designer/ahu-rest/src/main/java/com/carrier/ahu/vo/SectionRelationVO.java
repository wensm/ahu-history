package com.carrier.ahu.vo;

import lombok.Data;

@Data
public class SectionRelationVO {

	private String id;
	private String name;
	private Integer line;
	private Integer position;

}
