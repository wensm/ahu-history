package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.AhuTemplate;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.AhuTemplateService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Api(description = "ahu模板ctroller")
@RestController
public class AhuTemplateController extends AbstractController {

	@Autowired
	private AhuTemplateService templateService;
	@Autowired
	private AhuService ahuService;

	@Autowired
	private SectionService sectionService;

	@RequestMapping(value = "ahutemplate/list", method = RequestMethod.GET)
	public ApiResult<List<AhuTemplate>> list() {
		/*
		 * ---------------------------------------------------------------------
		 * ------- // List<AhuTemplate> result = templateService.list();
		 * List<AhuTemplate> result = new ArrayList<AhuTemplate>(); //
		 * this.getClass().getClassLoader().getResource(FileNamesLoadInSystem.
		 * AHU_TEMPLATE_JSON); // File file = new File(url.getFile());
		 * BufferedReader in = null; try { in = new BufferedReader(new
		 * InputStreamReader(
		 * this.getClass().getClassLoader().getResourceAsStream(
		 * FileNamesLoadInSystem.AHU_TEMPLATE_JSON), Charset.forName("UTF-8")));
		 * StringBuffer buffer = new StringBuffer(); String line = ""; while
		 * ((line = in.readLine()) != null) { buffer.append(line); } JSONArray
		 * jsonArray = JSONArray.parseArray(buffer.toString()); result =
		 * jsonArray.toJavaList(AhuTemplate.class); } catch (Exception e) {
		 * e.printStackTrace(); } finally { if (in != null) { try { in.close();
		 * } catch (IOException e) { e.printStackTrace(); } }
		 * 
		 * }
		 * ---------------------------------------------------------------------
		 * --------------------
		 */

		return ApiResult.success(TemplateUtil.getPredefinedTemplate());
	}

	@RequestMapping(value = "ahutemplate/save", method = RequestMethod.POST)
	public ApiResult<String> save(@RequestBody AhuTemplate ahuTemplate) {
		String id = "";
		if (StringUtils.isEmpty(ahuTemplate.getId())) {
			id = templateService.addAhuTempate(ahuTemplate);
		} else {
			id = templateService.updateAhuTempate(ahuTemplate);
		}
		return ApiResult.success(id);
	}

	@RequestMapping(value = "ahutemplate/generate/{ahuId}", method = RequestMethod.POST)
	public ApiResult<String> generate(@PathVariable("ahuId") String ahuId) {
		Unit unit = ahuService.findAhuById(ahuId);
		JSONObject object = (JSONObject) JSONObject.toJSON(unit);
		List<Part> parts = sectionService.findSectionList(ahuId);
		AhuTemplate ahuTemplate = new AhuTemplate();
		ahuTemplate.setTemplateName(GroupTypeEnum.TYPE_BASIC.getId());
		ahuTemplate.setCnName(GroupTypeEnum.TYPE_BASIC.getCnName());
		ahuTemplate.setAhuContent(object.toJSONString());
		ahuTemplate.setSectionContent(JSONArray.toJSONString(parts));
		List<AhuTemplate> ahuTemplates = new ArrayList<AhuTemplate>();
		ahuTemplates.add(ahuTemplate);
		// String filePath =
		// System.getProperty("user.dir").concat("/ahu_template.json");
		URL url = AhuTemplateController.class.getClassLoader().getResource(FileNamesLoadInSystem.AHU_TEMPLATE_JSON);
		File file = new File(url.getFile());
		FileWriter fw = null;
		PrintWriter out = null;
		try {
			fw = new FileWriter(file.getPath());
			out = new PrintWriter(fw);
			// JSONObject json = (JSONObject) JSONObject.toJSON(ahuTemplates);
			out.write(JSONArray.toJSONString(ahuTemplates).toString());
			out.println();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (fw!=null) {
					fw.close();
				}
				if (out!=null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return ApiResult.success();
	}

	@SuppressWarnings("unused")
	private List<AhuTemplate> getT() {
		List<AhuTemplate> result = new ArrayList<AhuTemplate>();
		URL url = AhuTemplateController.class.getClassLoader().getResource(FileNamesLoadInSystem.AHU_TEMPLATE_JSON);
		File file = new File(url.getFile());
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			JSONArray jsonArray = JSONArray.parseArray(buffer.toString());
			result = jsonArray.toJavaList(AhuTemplate.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (EmptyUtil.isNotEmpty(in)) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		return result;
	}
}
