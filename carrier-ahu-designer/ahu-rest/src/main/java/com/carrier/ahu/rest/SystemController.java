package com.carrier.ahu.rest;

import static com.carrier.ahu.vo.SysConstants.ASSERT_DIR;
import static com.carrier.ahu.vo.SysConstants.DIR_EXPORT;
import static com.carrier.ahu.vo.SysConstants.ERROR_FILE_PREFIX;
import static com.carrier.ahu.vo.SysConstants.FILES_DIR;
import static com.carrier.ahu.vo.SysConstants.ZIP_EXTENSION;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.log.LogService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;

import io.swagger.annotations.Api;

/**
 * Provide system related information.
 * 
 * Created by Braden Zhou on 2018/09/29.
 */
@Api(description = "系统相关接口")
@RestController
public class SystemController extends AbstractController {

    @Value("${ahu.version}")
    private String ahuVersion;

    @Value("${system.admin.email}")
    private String adminEmail;

    @Autowired
    private LogService logService;

    @Autowired
    private AhuService ahuService;

    @RequestMapping(value = "log/error", method = RequestMethod.GET)
    public ApiResult<Map<String, String>> reportError(String ahuId) throws Exception {
        // add slf4j log files
        List<File> errorFiles = new ArrayList<>();
        errorFiles.addAll(logService.getSystemLogFiles());

        // add unit export file
        Unit unit = ahuService.findAhuById(ahuId);
        if (EmptyUtil.isEmpty(unit)) {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        errorFiles.add(ahuService.exportUnit(unit,ahuVersion));

        // write all into zip file
        String errorZipPath = DIR_EXPORT + ERROR_FILE_PREFIX + ahuId + ZIP_EXTENSION;
        FileUtil.writeZipFile(errorZipPath, errorFiles);

        Map<String, String> error = new HashMap<>();
        error.put(RestConstant.SYS_MSG_RESPONSE_ERROR_FILE, errorZipPath.replace(ASSERT_DIR, FILES_DIR));
        error.put(RestConstant.SYS_MSG_RESPONSE_EMAIL, adminEmail);
        return ApiResult.success(error);
    }

}
