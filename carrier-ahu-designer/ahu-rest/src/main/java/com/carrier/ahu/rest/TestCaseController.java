package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.report.ReportExcelCRMList;
import com.carrier.ahu.service.*;
import com.carrier.ahu.service.log.LogService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.SectionTestCaseUtil;
import com.carrier.ahu.vo.AhuVO;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.SectionVO;
import com.carrier.ahu.vo.TestCaseVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.*;

import static com.carrier.ahu.constant.CommonConstant.SYS_MSG_RESPONSE_OK;
import static com.carrier.ahu.vo.SysConstants.*;

/**
 * 自动测试接口
 */
@Api(description = "TestCase接口")
@RestController
public class TestCaseController extends AbstractController {
    @Autowired
    SectionService sectionService;
    @Autowired
    AhuService ahuService;
    @Autowired
    GroupService groupService;
    @Autowired
    ProjectService projectService;
    @RequestMapping(value = "testCase/getAllAhu", method = RequestMethod.GET)
    public ApiResult<List<TestCaseVO>> getAllAhu() throws Exception {
        List<Project> projectList = projectService.findProjectList();
        List<TestCaseVO> allAhuTree = new ArrayList<>();

        for (Project project : projectList) {
            TestCaseVO tsvPrj = new TestCaseVO();
            tsvPrj.setKey(project.getPid());
            tsvPrj.setTitle(project.getName());
            List<String> units = ahuService.getAllIds(project.getPid());
            List<TestCaseVO> children = new ArrayList<>();
            for (String unit : units) {
                Unit unitEntity = ahuService.findAhuById(unit);
                TestCaseVO tsvUnit = new TestCaseVO();
                tsvUnit.setKey(unitEntity.getUnitid());
                tsvUnit.setTitle("{ "+unitEntity.getUnitid()+" } " + unitEntity.getName());
                children.add(tsvUnit);
            }
            tsvPrj.setChildren(children);
            allAhuTree.add(tsvPrj);
        }
        return ApiResult.success(allAhuTree);
    }
    @RequestMapping(value = "testCase/doTest", method = RequestMethod.POST)
    public ApiResult<String> doTest(String selectedAhu) throws Exception {
        JSONArray ja = JSONArray.parseArray(selectedAhu);
        JSONArray retja = new JSONArray();
        for (Object o : ja) {
            String ahuId = String.valueOf(o);
            if(ahuId.startsWith("A")){
                JSONObject retObj = new JSONObject();
                retObj.put("unitId",ahuId);
                retObj.put("result",generateTestData(ahuId));
                retja.add(retObj);
            }
        }
        Map<String, String> allAhu = null;
        return ApiResult.success(retja.toString());
    }

    /**
     * 执行写入case
     * @param unitid
     * @return
     */
    private boolean generateTestData(String unitid) {
        boolean ret = false;
        try {
            Unit unit = ahuService.findAhuById(unitid);
            String pid = unit.getPid();
            List<Part> partList = sectionService.findSectionList(unitid);
            List<SectionVO> sectionVOs = new ArrayList<>();
            for (Part part : partList) {
                SectionVO svo = new SectionVO();
                svo.setPid(part.getPid());
                svo.setPartid(part.getPartid());
                svo.setUnitid(part.getUnitid());
                svo.setKey(part.getSectionKey());
                svo.setPosition(part.getPosition());
                svo.setMetaJson(part.getMetaJson());
                sectionVOs.add(svo);
            }
            Collections.sort(sectionVOs, new Comparator<SectionVO>() {
                public int compare(SectionVO o1, SectionVO o2) {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            });


            logger.error("**************" + unit.getUnitid() + " generateTestData start:");
            if (true) {
                User user = getUser();
                LanguageEnum language = LanguageEnum.byId(user.getPreferredLocale());
                UnitSystemEnum unitType = UnitSystemEnum.byId(user.getUnitPreferCode());
                Project project = projectService.getProjectById(pid);
                ret = SectionTestCaseUtil.doWrite(unit, sectionVOs, language, unitType, project);
                logger.error("**************" + unit.getUnitid() + " write result:" + ret + "**************");
            }
        } catch (Exception e) {
            logger.error("Exception in generate new partition:" + e.getMessage(), e);
        }
        return ret;
    }

}
