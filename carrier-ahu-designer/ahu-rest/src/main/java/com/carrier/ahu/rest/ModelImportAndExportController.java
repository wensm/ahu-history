package com.carrier.ahu.rest;

import static com.carrier.ahu.common.util.MapRemoveNullUtil.removeNullEntry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.factory.GroupValidatorFactory;
import com.carrier.ahu.factory.SerialsValidatorFactory;
import com.carrier.ahu.factory.TemperatureValidatorFactory;
import com.carrier.ahu.factory.ValidatorFactory;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.product.GroupValidatorProduct;
import com.carrier.ahu.product.SerialsValidatorProduct;
import com.carrier.ahu.product.TemperatureValidatorProduct;
import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.report.ExportGroup;
import com.carrier.ahu.report.ExportPart;
import com.carrier.ahu.report.ExportPartition;
import com.carrier.ahu.report.ExportProject;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.Excel4ModelInAndOutUtils;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.util.VersionValidationUtil;
import com.carrier.ahu.util.ahu.AhuExporterUtils;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.SysConstants;
import com.carrier.ahu.vo.TemplateImportRstVO;

import io.swagger.annotations.Api;

/**
 * 模型导入导出业务接口 Created by zhiqiang.zhang on 2017/3/16.
 */
@Api(description = "模型导入导出相关接口")
@RestController
public class ModelImportAndExportController extends AbstractController {
	@Value("${ahu.version}")
	private String ahuVersion;
	@Value("${ahu.export.support.version}")
	private String exportSupportVersion;
	@Value("${ahu.template.support.version}")
	private String supportVersionT;
	@Autowired
	AhuService ahuService;
	@Autowired
	ProjectService projectService;
	@Autowired
	SectionService sectionService;
	@Autowired
	GroupService groupService;
	@Autowired
	AhuGroupBindService ahuGroupBindService;
	@Autowired
	PartitionService partitionService;
	@Autowired
	CalculatorTool calculatorTool;
	@Autowired
	AhuStatusTool ahuStatusTool;

	/**
	 * 项目导入<br>
	 * 导入时需要保证：所有的项目组成元素的ID都存在，相互依存关系也存在，采用树形结构的方式进行约束<br>
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "import/proj", method = RequestMethod.POST)
	public ApiResult<Project> projImport(HttpServletRequest request, String projectId) throws Exception {
		Project selectedProject = null;
		boolean hasProject2CoverFlag = false;
		if (EmptyUtil.isNotEmpty(projectId)) {
			hasProject2CoverFlag = true;
			selectedProject = projectService.getProjectById(projectId);
			if (EmptyUtil.isEmpty(selectedProject)) {
				throw new ApiException(ErrorCode.TARGET_PROJECT_NOT_EXIST);
			}
		}
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		// 判断 request 是否有文件上传,即多部分请求
		if (multipartResolver.isMultipart(request)) {
			// 转换成多部分request
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			// 取得request中的所有文件名
			Iterator<String> iter = multiRequest.getFileNames();
			while (iter.hasNext()) {
				// 取得上传文件
				MultipartFile uploadFile = multiRequest.getFile(iter.next());
				if (EmptyUtil.isNotEmpty(uploadFile)) {
                    InputStream in = null;
                    if (uploadFile.getOriginalFilename().endsWith(SysConstants.PRJ_EXTENSION)) {
                        in = uploadFile.getInputStream();
                    } else if (uploadFile.getOriginalFilename().endsWith(SysConstants.ZIP_EXTENSION)) {
                        AhuExporterUtils.cleanImportFilePath();
                        String importFilePath = SysConstants.DIR_IMPORT
                                + DateUtil.getUserDate(RestConstant.SYS_FORMAT_yyMMddHHmmss) + SysConstants.SLASH_LINUX;
                        try {
                            AhuExporterUtils.importImgFile(importFilePath, uploadFile.getInputStream());
                        } catch (Exception exp) {
                            throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                        }
                        File projectFile = AhuExporterUtils.findProjectFile(importFilePath);
                        if (projectFile == null) {
                            throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                        }
                        in = new FileInputStream(projectFile);
                    } else {
                        throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                    }

                    // read project json string from .prj file
                    StringBuffer projectJsonString = new StringBuffer();
                    InputStreamReader reader = new InputStreamReader(in, Charset.forName("UTF-8"));
                    BufferedReader br = new BufferedReader(reader);
                    String s = null;
                    while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
                        projectJsonString.append(s);
                    }
                    br.close();
                    in.close();

					// 解析上传内容，序列化后保存到数据库
					ExportProject eproject = JSONObject.parseObject(projectJsonString.toString(), ExportProject.class);
					/*增加版本验证*/
					if(!EmptyUtil.isEmpty(eproject.getVersion())&&!EmptyUtil.isEmpty(exportSupportVersion)){
						boolean bol = VersionValidationUtil.packageProjectValidation(eproject.getVersion(),exportSupportVersion);
						if(!bol){
							throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
						}
					}
					if (!hasProject2CoverFlag) {
						if (StringUtils.isBlank(eproject.getPid())) {
							throw new ApiException(ErrorCode.IMPORT_PROJECT_ID_EMPTY);
						}
						Project projectSrc = projectService.getProjectById(eproject.getPid());
                        if (EmptyUtil.isNotEmpty(projectSrc)) {
                            return ApiResult.error(String.valueOf(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getCode()),
                                    getIntlString(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getMessage()), projectSrc);
                        }
						Project project = new Project();
						BeanUtils.copyProperties(project, eproject);
						projectService.addProject(project, getUserName());
					}

					List<ExportGroup> groups = eproject.getGroups();
					List<GroupInfo> groupList = new ArrayList<>();
					List<Unit> unitList = new ArrayList<>();
					List<Part> partList = new ArrayList<>();
					List<Partition> partitionList = new ArrayList<>();
					List<AhuGroupBind> bindList = new ArrayList<>();
					for (ExportGroup egroup : groups) {
						if (StringUtils.isBlank(egroup.getGroupId())) {
							throw new ApiException(ErrorCode.IMPORT_GROUP_ID_EMPTY);
						}
						GroupInfo group = new GroupInfo();
						BeanUtils.copyProperties(group, egroup);
						if (hasProject2CoverFlag) {
							group.setProjectId(projectId);
						}
						groupList.add(group);
						List<ExportUnit> units = egroup.getUnits();
						for (ExportUnit eunit : units) {
							if (StringUtils.isBlank(eunit.getUnitid())) {
								throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
							}
							Unit unit = new Unit();
							BeanUtils.copyProperties(unit, eunit);
							if (hasProject2CoverFlag) {
								unit.setPid(projectId);
							}
							unitList.add(unit);

							if (EmptyUtil.isNotEmpty(eunit.getPartition())) {
								Partition partition = new Partition();
								BeanUtils.copyProperties(partition, eunit.getPartition());
								partition.setPartitionid(null); // clear partition id, let system to generate
								if (hasProject2CoverFlag) {
									partition.setPid(projectId);
								}
								partitionList.add(partition);
							}

							AhuGroupBind bind = new AhuGroupBind();
							bind.setUnitid(unit.getUnitid());
							bind.setGroupid(unit.getGroupId());
							bindList.add(bind);

							List<ExportPart> parts = eunit.getExportParts();
							for (ExportPart epart : parts) {
								if (StringUtils.isBlank(epart.getPartid())) {
									throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
								}
								Part part = new Part();
								BeanUtils.copyProperties(part, epart);
								if (hasProject2CoverFlag) {
									part.setPid(projectId);
								}
								partList.add(part);
							}
						}
					}
					List<ExportUnit> units = eproject.getUnits();
					for (ExportUnit eunit : units) {
						if (StringUtils.isBlank(eunit.getUnitid())) {
							throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
						}
						Unit unit = new Unit();
						BeanUtils.copyProperties(unit, eunit);
						if (hasProject2CoverFlag) {
							unit.setPid(projectId);
						}
						unitList.add(unit);

						if (EmptyUtil.isNotEmpty(eunit.getPartition())) {
							Partition partition = new Partition();
							BeanUtils.copyProperties(partition, eunit.getPartition());
							partition.setPartitionid(null); // clear partition id, let system to generate
							if (hasProject2CoverFlag) {
								partition.setPid(projectId);
							}
							partitionList.add(partition);
						}

						List<ExportPart> parts = eunit.getExportParts();
						for (ExportPart epart : parts) {
							if (StringUtils.isBlank(epart.getPartid())) {
								throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
							}
							Part part = new Part();
							BeanUtils.copyProperties(part, epart);
							if (hasProject2CoverFlag) {
								part.setPid(projectId);
							}
							partList.add(part);
						}
					}
					// should be in the same transaction
					groupService.addGroups(groupList);
					ahuService.addOrUpdateAhus(unitList);
					// should delete old parts from unit
					unitList.forEach(unit -> {
						sectionService.deleteAHUSections(unit.getUnitid());
					});
					sectionService.addSections(partList);
					ahuGroupBindService.addBinds(bindList, getUserName());
					partitionService.addOrUpdateAhus(partitionList);
					if (hasProject2CoverFlag) {
						ahuStatusTool.syncStatus(projectId, getUserName());
					} else {
						ahuStatusTool.syncStatus(eproject.getPid(), getUserName());
					}
				}
			}
		}
		return ApiResult.success(null);
	}

	/**
	 * 项目导出
	 *
	 * @return
	 */
	@RequestMapping(value = "export/proj/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> projExport(@PathVariable("projectId") String projectId, String filename)
			throws Exception {
		// Start generate export Project
		ExportProject eProject = new ExportProject();
		Project project = projectService.getProjectById(projectId);
		if (EmptyUtil.isEmpty(project)) {
			logger.error("Project is not exist, projectID:" + projectId);
			throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
		}
		BeanUtils.copyProperties(eProject, project);
		List<GroupInfo> groupList = groupService.findGroupList(projectId);
		List<ExportGroup> groups = new ArrayList<>();
		for (GroupInfo group : groupList) {
			ExportGroup eGroup = new ExportGroup();
			BeanUtils.copyProperties(eGroup, group);
			List<Unit> ahuList = ahuService.findAhuList(projectId, group.getGroupId());
			List<ExportUnit> units = new ArrayList<>();
			for (Unit ahu : ahuList) {
				ExportUnit eAhu = new ExportUnit();
				BeanUtils.copyProperties(eAhu, ahu);
				List<Part> partList = sectionService.findSectionList(ahu.getUnitid());
				List<ExportPart> parts = new ArrayList<>();
				for (Part part : partList) {
					ExportPart ePart = new ExportPart();
					BeanUtils.copyProperties(ePart, part);
					parts.add(ePart);
				}
				eAhu.setExportParts(parts);
				Partition partition = partitionService.findPartitionByAHUId(ahu.getUnitid());
				if (EmptyUtil.isNotEmpty(partition)) {
					ExportPartition ePartition = new ExportPartition();
					BeanUtils.copyProperties(ePartition, partition);
					eAhu.setPartition(ePartition);
				}
				units.add(eAhu);
			}
			eGroup.setUnits(units);
			groups.add(eGroup);
		}
		List<Unit> ahuList = ahuService.findAhuList(projectId, null);
		List<ExportUnit> units = new ArrayList<>();
		for (Unit ahu : ahuList) {
			ExportUnit eAhu = new ExportUnit();
			BeanUtils.copyProperties(eAhu, ahu);
			List<Part> partList = sectionService.findSectionList(ahu.getUnitid());
			List<ExportPart> parts = new ArrayList<>();
			for (Part part : partList) {
				ExportPart ePart = new ExportPart();
				BeanUtils.copyProperties(ePart, part);
				parts.add(ePart);
			}
			eAhu.setExportParts(parts);
			Partition partition = partitionService.findPartitionByAHUId(ahu.getUnitid());
			if (EmptyUtil.isNotEmpty(partition)) {
				ExportPartition ePartition = new ExportPartition();
				BeanUtils.copyProperties(ePartition, partition);
				eAhu.setPartition(ePartition);
			}
			units.add(eAhu);
		}
		eProject.setUnits(units);
		eProject.setGroups(groups);
		eProject.setVersion(ahuVersion);
		// Complete generate export Project

		String toWritttenStr = JSONObject.toJSONString(eProject);

		// generate project .prj file
		String projectExportFilePath = SysConstants.DIR_EXPORT + projectId + SysConstants.PRJ_EXTENSION;
		File projectExportFile = new File(projectExportFilePath);
		if (!projectExportFile.getParentFile().exists()) {
			projectExportFile.getParentFile().mkdirs();
		}
		if (!projectExportFile.exists()) {
			projectExportFile.createNewFile();
		}
		FileOutputStream o = new FileOutputStream(projectExportFile);
		o.write(toWritttenStr.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)));
		o.close();

        // prepare generating zip file
        List<File> exportFiles = new ArrayList<>();
        exportFiles.add(projectExportFile);
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_YLD + projectId));
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_GK + projectId));
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_KRUGER + projectId));
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_CAD + projectId));

        // generate zip file
        String zipFileName = "";
        String dateString = DateUtil.getStringDateYYMMDD();
        if (StringUtils.isBlank(filename)) {
            zipFileName = format(SysConstants.NAME_EXPORT_PROJECT, project.getPid(), dateString);
        } else {
            zipFileName = format(SysConstants.NAME_EXPORT_PROJECT,
                    new String(filename.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)),
                            RestConstant.SYS_ENCODING_ISO_8859_1),
                    dateString);
        }
        String zipFilePath = SysConstants.DIR_EXPORT + zipFileName;
        File zipFile = new File(zipFilePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        FileUtil.zipFiles(exportFiles, zipFilePath);

        return download(zipFilePath, zipFileName);
    }

	/**
	 * AHU导出
	 *
	 * @return
	 */
	@RequestMapping(value = "export/unit/{ahuId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> unitExport(@PathVariable("ahuId") String ahuId) throws Exception {
		Unit unit = ahuService.findAhuById(ahuId);
		if (EmptyUtil.isEmpty(unit)) {
			logger.error("ahu is not exist, ahuID:" + ahuId);
			throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
		}
		File unitExportFile = ahuService.exportUnit(unit,ahuVersion);
		return download(unitExportFile, unitExportFile.getName());
	}

	/**
	 * AHU导入
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "import/unit", method = RequestMethod.POST)
	public ApiResult<String> unitImport(HttpServletRequest request, String ahuId) throws Exception {
		Unit selectedAhu = null;
		Partition selectedPartition = null;
		if (EmptyUtil.isNotEmpty(ahuId)) {
			selectedAhu = ahuService.findAhuById(ahuId);
			selectedPartition = partitionService.findPartitionByAHUId(ahuId);
			if (EmptyUtil.isEmpty(selectedAhu)) {
				throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
			}
			sectionService.deleteAHUSections(ahuId);
			if (EmptyUtil.isNotEmpty(selectedPartition)) {
			    partitionService.deletePartition(selectedPartition.getPartitionid());
			}
		} else {
			throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
		}
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		// 判断 request 是否有文件上传,即多部分请求
		if (multipartResolver.isMultipart(request)) {
			// 转换成多部分request
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			// 取得request中的所有文件名
			Iterator<String> iter = multiRequest.getFileNames();
			while (iter.hasNext()) {
				// 取得上传文件
				MultipartFile f = multiRequest.getFile(iter.next());
				if (EmptyUtil.isNotEmpty(f)) {
					StringBuffer result = new StringBuffer();
					FileInputStream in = (FileInputStream) f.getInputStream();
					InputStreamReader reader = new InputStreamReader(in, Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP));
					BufferedReader br = new BufferedReader(reader);
					String s = null;
					while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
						result.append(s);
					}
					br.close();
					in.close();
					// 解析上传内容，序列化后选择关键字段更新保存到数据库
					List<Unit> unitList = new ArrayList<>();
					List<Part> partList = new ArrayList<>();
					List<Partition> partitionList = new ArrayList<>();

					ExportUnit eunit = JSONObject.parseObject(result.toString(), ExportUnit.class);
					//增加版本验证
					if(!EmptyUtil.isEmpty(eunit.getVersion())&&!EmptyUtil.isEmpty(exportSupportVersion)){
						boolean bol = VersionValidationUtil.packageProjectValidation(eunit.getVersion(),exportSupportVersion);
						if(!bol){
							throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
						}
					}
					if (StringUtils.isBlank(eunit.getUnitid())) {
						throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
					}

					// 机组信息
					Unit unit = new Unit();
					BeanUtils.copyProperties(unit, eunit);
					selectedAhu.setName(eunit.getName());
					selectedAhu.setLayoutJson(eunit.getLayoutJson());
					selectedAhu.setMetaJson(eunit.getMetaJson());
					selectedAhu.setPrice(eunit.getPrice());
					selectedAhu.setProduct(eunit.getProduct());
					selectedAhu.setRecordStatus(eunit.getRecordStatus());
					selectedAhu.setSeries(eunit.getSeries());
					selectedAhu.setWeight(eunit.getWeight());
					selectedAhu.setGroupCode(eunit.getGroupCode());
					unitList.add(selectedAhu);

					// 分段信息
					ExportPartition partition = eunit.getPartition();
					if (EmptyUtil.isNotEmpty(partition)) {
						if (EmptyUtil.isEmpty(selectedPartition)) {
							selectedPartition = new Partition();
							BeanUtils.copyProperties(selectedPartition, partition);
						} else {
							selectedPartition.setMetaJson(partition.getMetaJson());
							selectedPartition.setPartitionJson(partition.getPartitionJson());
							selectedPartition.setRecordStatus(partition.getRecordStatus());
						}
						selectedPartition.setPartitionid(RestConstant.SYS_BLANK); // regenerate partition id
						selectedPartition.setPid(selectedAhu.getPid());
						selectedPartition.setUnitid(selectedAhu.getUnitid());
						partitionList.add(selectedPartition);
					}
					User user = getUser();
					// 段详细信息
					List<ExportPart> parts = eunit.getExportParts();
					for (ExportPart epart : parts) {
						if (StringUtils.isBlank(epart.getPartid())) {
							throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
						}
						Part part = new Part();
						BeanUtils.copyProperties(part, epart);
						String partId = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
						part.setPartid(partId);
						part.setPid(selectedAhu.getPid());
						part.setUnitid(selectedAhu.getUnitid());
						partList.add(part);
					}
					ahuService.addOrUpdateAhus(unitList);
					partitionService.addOrUpdateAhus(partitionList);
					sectionService.addSections(partList);
				}
			}
		}
		return ApiResult.success(RestConstant.SYS_MSG_RESPONSE_SUCCESS);
	}

	/**
	 * 分组导入<br>
	 * 分组导入时，Excel中的所有AHU在数据库上新建，使用树形结构约束<br>
	 * model 为1时即覆盖现有参数<br>
	 * model 为0时，只新建AHU和段，对原有数据不做操作<br>
	 *
	 * @param request
	 * @param projectId
	 * @param groupId
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "import/group/{projectId}/{groupId}", method = RequestMethod.POST)
	public ApiResult<TemplateImportRstVO> groupImport(HttpServletRequest request, @PathVariable("projectId") String projectId,
			@PathVariable("groupId") String groupId, String model) throws Exception {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		
		//初始化返回给前端的实体对象
		TemplateImportRstVO resultvo = new TemplateImportRstVO();
//		String success = I18NBundle.getString(I18NConstants.IMPORT_SUCCESS, getLanguage());
//		String groupPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_GROUP, getLanguage());
//		String existPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage());
//		String tempPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage());
		Map<String, String> returnMap = new HashMap<String, String>();
		returnMap.put("success", I18NBundle.getString(I18NConstants.IMPORT_SUCCESS, getLanguage()));
		returnMap.put("groupPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_GROUP, getLanguage()));
		returnMap.put("existPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage()));
		returnMap.put("tempPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_TEMP, getLanguage()));
		returnMap.put("serialsPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_SERIALS, getLanguage()));
		if (!(multipartResolver.isMultipart(request))) {
			throw new ApiException(ErrorCode.REQUEST_DATA_ABNORMAL);
		}
		Project project = projectService.getProjectById(projectId);
		if (EmptyUtil.isEmpty(project)) {
			throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
		}
		GroupInfo group = groupService.findGroupById(groupId);
		if (EmptyUtil.isEmpty(group)) {
			throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
		}
		String groupType = group.getGroupType();
		String groupCode = group.getGroupCode();
		if (EmptyUtil.isEmpty(groupCode) && !RestConstant.SYS_STRING_GROUPTYPE_TP_0.equals(groupType)) {
			throw new ApiException(ErrorCode.GROUP_CODE_NOT_FOUND);
		}

		MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		Iterator<String> iter = multiRequest.getFileNames();
		while (iter.hasNext()) {
			String fileName = iter.next();
			MultipartFile f = multiRequest.getFile(fileName);
			if (EmptyUtil.isNotEmpty(f)) {
				if (!ExcelUtils.isExcelFileName(f.getOriginalFilename())) {
					throw new ApiException(ErrorCode.WRONG_FILE_FORMAT);
				}
				InputStream in = f.getInputStream();
				/*增加版本控制------start------*/
				String version = VersionValidationUtil.readMarkFromTemplate(in, fileName);
				if(!EmptyUtil.isEmpty(version)&&!EmptyUtil.isEmpty(supportVersionT)){
					boolean bol = VersionValidationUtil.TemplateValidation(version, supportVersionT);
					if(!bol){
						throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
					}
				}
				/*增加版本控制------end------*/
				in = f.getInputStream();
				List<ExportUnit> units = Excel4ModelInAndOutUtils.importExcelGroup(in, ExcelUtils.isExcel2003(fileName),
						groupCode);
				if (EmptyUtil.isEmpty(units)) {
					throw new ApiException(ErrorCode.RETURN_NULL_AFTER_EXCEL_ANALYSIS);
				}
				// 解析上传内容，序列化后保存到数据库
				String pid = project.getPid();
				if (model.equals(RestConstant.SYS_STRING_NUMBER_1)) {
					for (ExportUnit eUnit : units) {
						//机组系列判空
						ValidatorFactory factory0 = new SerialsValidatorFactory();
						ValidatorProduct product0 = (SerialsValidatorProduct)factory0.getValidator();
						if (!product0.validate(eUnit, project, groupId)) {
							String a = returnMap.get("serialsPRB");
							a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
							returnMap.put("serialsPRB", a);
							continue;		
						}
						
						//校验温度
						ValidatorFactory factory = new TemperatureValidatorFactory();
						ValidatorProduct product = (TemperatureValidatorProduct)factory.getValidator();
						if (!product.validate(eUnit, project, groupId)) {//校验未通过
							String a = returnMap.get("tempPRB");
							a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
							returnMap.put("tempPRB", a);
							continue;
						}
						//分组校验
						ValidatorFactory factory2 = new GroupValidatorFactory();
						ValidatorProduct product2 = (GroupValidatorProduct)factory2.getValidator(ahuService);
						Map<String, Object> rstMap2 = product2.validate1(eUnit, project, groupId);
						takeAction(rstMap2, eUnit, pid, groupId, groupCode, returnMap);
						
//						String drawingNo = eUnit.getDrawingNo();
//						// Unit unitBefore = ahuService.findAhuByDrawingNO(pid, drawingNo);
//						Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);
//						if (null == unitBefore) {
//							if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空就不新增
//								continue;
//							}
//							Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);
//							if (eUnit.getDrawingNo().equals(unitBefore1!=null?unitBefore1.getDrawingNo():null)) {//机组编号已存在于其他分组，不新增
//								groupPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//								continue;
//							}
//							importAddNewAhu(eUnit, pid, groupId, groupCode);
//							success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//						} else {
//							if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空就不覆盖
//								continue;
//							}
//							//清空覆盖所有参数后，老unit 价格、重量
//							unitBefore.setPrice(0.0);
//							unitBefore.setWeight(0.0);
//							importCoverAhu(eUnit, unitBefore, pid, groupId, groupCode);
//							success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//						}
					}
				} else {
					for (ExportUnit eUnit : units) {
						//机组系列判空
						ValidatorFactory factory0 = new SerialsValidatorFactory();
						ValidatorProduct product0 = (SerialsValidatorProduct)factory0.getValidator();
						if (!product0.validate(eUnit, project, groupId)) {
							String a = returnMap.get("serialsPRB");
							a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
							returnMap.put("serialsPRB", a);
							continue;		
						}
						//校验温度
						ValidatorFactory factory = new TemperatureValidatorFactory();
						ValidatorProduct product = (TemperatureValidatorProduct)factory.getValidator();
						if (!product.validate(eUnit, project, groupId)) {//校验未通过
							String a = returnMap.get("tempPRB");
							a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
							returnMap.put("tempPRB", a);
							continue;
						}
						//分组校验
						ValidatorFactory factory3 = new GroupValidatorFactory();
						ValidatorProduct product3 = (GroupValidatorProduct)factory3.getValidator(ahuService);
						Map<String, Object> rstMap3 = product3.validate2(eUnit, project, groupId);
						takeAction(rstMap3, eUnit, pid, groupId, groupCode, returnMap);
//						String drawingNo = eUnit.getDrawingNo();
//						Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);
//						Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);
//						if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空 或者机组编号已存在就不新增
//							continue;
//						}else if ((eUnit.getDrawingNo()).equals(unitBefore != null ? unitBefore.getDrawingNo() : null)) {//机组编号已存在于本组
//							existPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//							continue;
//						}else if ((eUnit.getDrawingNo()).equals(unitBefore1 != null ? unitBefore1.getDrawingNo() : null)) {//机组编号已存在于其他组
//							groupPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//							continue;
//						}
//						importAddNewAhu(eUnit, pid, groupId, groupCode);
//						success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
					}
				}
			}
		}
		ahuStatusTool.syncStatus(projectId, getUserName());
//		return ApiResult.success(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1);
		wrapResultVO(returnMap, resultvo);
		
		return ApiResult.success(resultvo);
	}
	
	/**
	 * 根据校验结果进行处理
	 */
	private void takeAction(Map validationMap, ExportUnit eUnit, String pid, String groupId, String groupCode, Map returnMap)
			throws Exception {
		String action = (String) validationMap.get(RestConstant.SYS_STRING_ACTION);
		String result = (String) validationMap.get(RestConstant.SYS_STRING_RESULT);
		Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, eUnit.getDrawingNo(), groupId);
		if (RestConstant.SYS_STRING_ACTION_ADD.equals(action)) {
			importAddNewAhu(eUnit, pid, groupId, groupCode);
			String temp = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
			temp += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
			returnMap.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, temp);
		} else if (RestConstant.SYS_STRING_ACTION_COVER.equals(action)) {
			unitBefore.setPrice(0.0);
			unitBefore.setWeight(0.0);
			importCoverAhu(eUnit, unitBefore, pid, groupId, groupCode);
			String temp = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
			temp += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
			returnMap.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, temp);
		} else if (RestConstant.SYS_STRING_ACTION_SKIP.equals(action)) {
//			String tempRst = String.valueOf(returnMap.get(RestConstant.SYS_STRING_RESULT));
			if ("groupPRB".equals(result)) {
				String a = String.valueOf(returnMap.get("groupPRB"));
				a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
				returnMap.put("groupPRB", a);
			}else if ("existPRB".equals(result)) {
				String a = String.valueOf(returnMap.get("existPRB"));
				a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
				returnMap.put("existPRB", a);
			}else if ("tempPRB".equals(result)) {
				String a = String.valueOf(returnMap.get("tempPRB"));
				a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
				returnMap.put("tempPRB", a);
			}
			
		}
	}
	
	/**
	 * 封装返回给前端的vo对象
	 * @param returnMap
	 * @param resultvo
	 */
	private void wrapResultVO(Map returnMap, TemplateImportRstVO resultvo) {
		String success = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
		String groupPRB = String.valueOf(returnMap.get("groupPRB"));
		String existPRB = String.valueOf(returnMap.get("existPRB"));
		String tempPRB = String.valueOf(returnMap.get("tempPRB"));
		String serialsPRB = String.valueOf(returnMap.get("serialsPRB"));
		
		// 若最后有逗号，就去掉最后的逗号
		resultvo.setSuccess(success.substring(0,
				(success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
						? success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
						: success.length())));
		resultvo.setGroupPRB(groupPRB.substring(0,
				(groupPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
						? groupPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
						: groupPRB.length())));
		resultvo.setExistPRB(existPRB.substring(0,
				(existPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
						? existPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
						: existPRB.length())));
		resultvo.setTempPRB(tempPRB.substring(0,
				(tempPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
						? tempPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
						: tempPRB.length())));
		resultvo.setSerialsPRB(serialsPRB.substring(0,
				(serialsPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
						? serialsPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
						: serialsPRB.length())));
	}
	
	/**
	 * 分组导出
	 *
	 * @return
	 */
	@RequestMapping(value = "export/group/{projectId}/{groupId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> groupExport(@PathVariable("projectId") String projectId,
			@PathVariable("groupId") String groupId, String filename) throws Exception {

		// 获取全局配置参数：导出模式 0-全量 1-配置
		String exportMode = new UserConfigParam().getExportMode();
		// 默认为根据配置导出
		if (EmptyUtil.isEmpty(exportMode)) {
			exportMode = RestConstant.SYS_STRING_NUMBER_1;
		}

		// Start generate result
		ExportProject eProject = new ExportProject();
		Project project = projectService.getProjectById(projectId);
		if (EmptyUtil.isEmpty(project)) {
			logger.error("Project is not exist, projectID:" + projectId);
			throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
		}
		BeanUtils.copyProperties(eProject, project);
		GroupInfo group = groupService.findGroupById(groupId);
		if (EmptyUtil.isEmpty(group)) {
			logger.error("GroupInfo is not exist, groupId:" + groupId);
			throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
		}
		ExportGroup eGroup = new ExportGroup();
		BeanUtils.copyProperties(eGroup, group);
		List<Unit> unitList = ahuService.findAhuList(projectId, groupId);
		if (EmptyUtil.isEmpty(unitList)) {
			logger.error("UnitList is not exist, groupId:" + groupId);
			throw new ApiException(ErrorCode.NO_AHU_IN_GROUP);
		}
		List<ExportUnit> units = new ArrayList<>();
		for (Unit unit : unitList) {
			ExportUnit eUnit = new ExportUnit();
			BeanUtils.copyProperties(eUnit, unit);
			List<Part> partList = sectionService.findSectionList(unit.getUnitid());
			if (EmptyUtil.isEmpty(partList)) {
				logger.error("PartList is not exist, unitid:" + unit.getUnitid());
				throw new ApiException(ErrorCode.NO_SECTION_UNDER_UNIT);
			}
			List<ExportPart> parts = new ArrayList<>();
			for (Part part : partList) {
				ExportPart ePart = new ExportPart();
				BeanUtils.copyProperties(ePart, part);
				ePart.setMetaJson(filter4MetaJson(part.getMetaJson(), TemplateUtil.getPerformanceExportKeyList()));
				parts.add(ePart);
			}
			eUnit.setMetaJson(filter4MetaJson(unit.getMetaJson(), TemplateUtil.getPerformanceExportKeyList()));
			eUnit.setExportParts(parts);
			eUnit.setVersion(ahuVersion);
			units.add(eUnit);
		}
		eGroup.setUnits(units);
		List<ExportGroup> groups = new ArrayList<>();
		groups.add(eGroup);
		eProject.setGroups(groups);
		eProject.setVersion(ahuVersion);
		// Complete generate result

		String fileName = "";
		if (StringUtils.isBlank(filename)) {
			fileName = format(SysConstants.NAME_EXPORT_GROUP, group.getGroupId());
		} else {
			fileName = format(SysConstants.NAME_EXPORT_GROUP,
					new String(filename.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)), RestConstant.SYS_ENCODING_ISO_8859_1));
		}

		String filePath = SysConstants.DIR_EXPORT + fileName;

		if (RestConstant.SYS_STRING_NUMBER_0.equals(exportMode)) {
			Excel4ModelInAndOutUtils.exportExcelGroup(filePath, eProject, getLanguage());
		} else if (RestConstant.SYS_STRING_NUMBER_1.equals(exportMode)) {
			Excel4ModelInAndOutUtils.exportExcelGroupByTemplet(filePath, eProject, getLanguage());// 读取导入模板文件，将数据填充到模板中，并导出文件。
		}
		/*标记版本信息*------start------*/
		VersionValidationUtil.markAtTemplate(filePath, fileName, ahuVersion);
		/*标记版本信息*------end------*/
		return download(filePath, fileName);
	}

	/**
	 * 未分组导入
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "import/ungroup/{projectId}", method = RequestMethod.POST)
	public ApiResult<String> unGroupImport(HttpServletRequest request, @PathVariable("projectId") String projectId)
			throws Exception {
		return ApiResult.success("developing......");
	}

	@SuppressWarnings("unchecked")
	private static String filter4MetaJson(String resourceJson, List<String> keepKeys) {
		try {
			Map<String, String> m = JSON.parseObject(resourceJson, Map.class);
			Map<String, String> rm = new HashMap<>();
			for (String key : keepKeys) {
				if (m.containsKey(key)) {
					rm.put(key, Excel4ModelInAndOutUtils.getStringValue(m.get(key)));
				}
			}
			String returnString = JSON.toJSONString(rm);
			return returnString;
		} catch (Exception e) {
			return resourceJson;
		}
	}

	@SuppressWarnings("unchecked")
	private void importAddNewAhu(ExportUnit eUnit, String pid, String groupId, String groupCode) throws Exception {
		User user = getUser();
		Unit unit = new Unit();
		String sVolume = "";/* 获取机组送风风量 */
		String eVolume = "";/* 获取机组回风风量 */
		String sAirVolume = RestConstant.METAHU_SAIRVOLUME;// 机组送风风量
		String eAirVolume = RestConstant.METAHU_EAIRVOLUME;// 机组回风风量
		BeanUtils.copyProperties(unit, eUnit);
		String unitid = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
		unit.setUnitid(unitid);
		unit.setProduct(eUnit.getProduct());
		unit.setPid(pid);
		unit.setGroupId(groupId);
		unit.setMount(eUnit.getMount());
		unit.setName(eUnit.getName());
		unit.setGroupCode(groupCode);
		AhuLayout layout = new AhuLayout();
		GroupInfo group = groupService.findGroupById(groupId);
		// 将group code转为layout
		int sectionNum = group.getSectionsNum();
		int[][] sections = new int[1][group.getSectionsNum()];
		for (int i = 0; i < sectionNum; i++) {
			sections[0][i] = i + 1;
		}
		layout.setStyle(LayoutStyleEnum.COMMON.style());
		layout.setLayoutData(sections);
		unit.setLayoutJson(JSON.toJSONString(layout));
		unit.setRecordStatus(AhuStatusEnum.IMPORT_COMPLETE.getId());
		unit = TemplateUtil.getTemplateUnit(unit);//此处导入unit属性，并对unit机组进行默认值设置。
		
		//如果回风风量为空，则将回风量默认设为送风量
		String eVolume1 = "";
		String sVolume1 = "";
		Map<String, String> tempMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
		Map<String, String> tempSourceMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
		if (tempSourceMap!=null && tempSourceMap.containsKey(UtilityConstant.METAHU_EAIRVOLUME)) {
			eVolume1 = BaseDataUtil.constraintString(tempSourceMap.get(UtilityConstant.METAHU_EAIRVOLUME));
		}
		if (tempSourceMap!=null && tempSourceMap.containsKey(UtilityConstant.METAHU_SAIRVOLUME)) {
			sVolume1 = BaseDataUtil.constraintString(tempSourceMap.get(UtilityConstant.METAHU_SAIRVOLUME));
		}		
		if (EmptyUtil.isEmpty(eVolume1) || UtilityConstant.SYS_STRING_NUMBER_0.equals(eVolume1)) {
			tempMap.put(UtilityConstant.METAHU_EAIRVOLUME, sVolume1);
		}
		unit.setMetaJson(JSON.toJSONString(tempMap));
		
		// List<String> ss = ahuService.findAllUnitNOInuse(pid);
		// String unitNo = ValueFormatUtil.getUsefulUnitNO(ss, -1);
		// unit.setUnitNo(unitNo);
		unit.setUnitNo(eUnit.getDrawingNo());
		Map<String, String> unitMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
		if (unitMap.containsKey(sAirVolume)) {
			sVolume = BaseDataUtil.constraintString(unitMap.get(sAirVolume));// 获取机组送风风量
		}
		if (unitMap.containsKey(eAirVolume)) {
			eVolume = BaseDataUtil.constraintString(unitMap.get(eAirVolume));// 获取机组回风风量
		}
		ahuService.addAhu(unit, getUserName());

		AhuGroupBind bind = new AhuGroupBind();
		bind.setUnitid(unitid);
		bind.setGroupid(groupId);
		ahuGroupBindService.addBind(bind, getUserName());

		List<ExportPart> parts = eUnit.getExportParts();
		List<Part> partList = new ArrayList<>();
		for (ExportPart ePart : parts) {
			Part part = new Part();
			BeanUtils.copyProperties(part, ePart);
			String partid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
			part.setPartid(partid);
			part.setPid(pid);
			part.setUnitid(unitid);
			Map<String, Object> mapTemplate = new HashMap<>(TemplateUtil.genAhuSectionTemplate(ePart.getSectionKey()));
			removeNullEntry(mapTemplate);
			Map<String, String> map = JSON.parseObject(ePart.getMetaJson(), HashMap.class);
			/* 如果风机段风量字段为空默认使用机组送风风量 */
			if (SectionTypeEnum.TYPE_FAN.getId().equals(ePart.getSectionKey())) {
				String fanAirVolume = RestConstant.METASEXON_FAN_AIRVOLUME;

				if (!map.containsKey(fanAirVolume)) {// 如果风机字段没有定义风量，风机风量默认使用机组风量
					map.put(fanAirVolume, sVolume);
				}
				/*
				 * String airDirection = "meta.section.airDirection";
				 *//* 如果风向是回风风向 *//*
									 * if(map.containsKey(airDirection)){ String airDirectionValue =
									 * BaseDataUtil.constraintString(airDirection);
									 * if(AirDirectionEnum.RETURNAIR.getCode().equals(airDirectionValue)){
									 * if(!map.containsKey(fanAirVolume)){//如果风机字段没有定义风量，风机风量默认使用机组风量
									 * map.put(fanAirVolume,eVolume); } }else{
									 * if(!map.containsKey(fanAirVolume)){//如果风机字段没有定义风量，风机风量默认使用机组风量
									 * map.put(fanAirVolume,sVolume); } } }
									 */
			}
			removeNullEntry(map);// 过滤excel中的空数据
			mapTemplate.putAll(map);
			part.setMetaJson(JSON.toJSONString(mapTemplate));
			partList.add(part);
		}
		sectionService.addSections(partList);
	}

	@SuppressWarnings("unchecked")
	private void importCoverAhu(ExportUnit eUnit, Unit unitbefore, String pid, String groupId, String groupCode)
			throws Exception {
		unitbefore.setRecordStatus(AhuStatusEnum.IMPORT_COMPLETE.getId());
		Map<String, String> beforeUnitMap = JSON.parseObject(unitbefore.getMetaJson(), HashMap.class);
		if (EmptyUtil.isEmpty(beforeUnitMap)) {
			beforeUnitMap = new HashMap<>();
		}
		Map<String, String> eUnitMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
		if (EmptyUtil.isNotEmpty(eUnitMap)) {
			beforeUnitMap.putAll(eUnitMap);
		}
		unitbefore.setMetaJson(JSON.toJSONString(beforeUnitMap));

		List<Part> poList = sectionService.findSectionList(unitbefore.getUnitid());
		List<ExportPart> parts = eUnit.getExportParts();

		List<Part> toUpdateList = new ArrayList<>();
		for (Part part : poList) {
			String sectionKey = part.getSectionKey();
			Part imptPart = getEPart(sectionKey, parts);
			if (null == imptPart) {
				continue;
			} else {
				Map<String, String> mapSource = JSON.parseObject(part.getMetaJson(), HashMap.class);
				Map<String, String> map = JSON.parseObject(imptPart.getMetaJson(), HashMap.class);
				mapSource.putAll(map);
				mapSource.put(RestConstant.METASEXON_COMPLETED, RestConstant.SYS_BLANK);//设置为正在选型状态
				part.setMetaJson(JSON.toJSONString(mapSource));
				part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
				toUpdateList.add(part);
			}
		}
		ahuService.updateAhu(unitbefore, getUserName());
		sectionService.updateSections(toUpdateList);
	}

	private static Part getEPart(String sectionKey, List<ExportPart> parts)
			throws IllegalAccessException, InvocationTargetException {
		for (ExportPart ePart : parts) {
			if (sectionKey.equals(ePart.getSectionKey())) {
				Part part = new Part();
				BeanUtils.copyProperties(part, ePart);
				part.setMetaJson(ePart.getMetaJson());
				return part;
			}
		}
		return null;
	}

	private static String format(String pattern, Object... arguments) {
		return MessageFormat.format(pattern, arguments);
	}
}
