package com.carrier.ahu.factory;

import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.service.AhuService;

/**
 * 校验器接口（工厂模式-工厂接口）
 * @author wangd1
 *
 */
public interface ValidatorFactory {

	public ValidatorProduct getValidator();
	
	public ValidatorProduct getValidator(AhuService ahuService);
}