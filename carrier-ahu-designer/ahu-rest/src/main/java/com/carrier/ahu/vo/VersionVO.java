package com.carrier.ahu.vo;

import lombok.Data;

/**
 * Created by Braden Zhou on 2019/02/15.
 */
@Data
public class VersionVO {
    private String ahuVersion;
    private String ahuPatchVersion;
}
