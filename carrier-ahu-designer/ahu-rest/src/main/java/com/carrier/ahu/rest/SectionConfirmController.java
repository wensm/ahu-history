package com.carrier.ahu.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.filter.FilterComposing;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.NSPriceParam;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.impl.AhuCalBus;
import com.carrier.ahu.service.cal.impl.DefaultCalContext;
import com.carrier.ahu.service.service.PriceService;
import com.carrier.ahu.util.BeanTransferUtil;
import com.carrier.ahu.util.DataValidationUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.AhuVO;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.SystemCalculateConstants;

import cn.hutool.core.util.NumberUtil;
import io.swagger.annotations.Api;

@Api(description = "段信息确认接口")
@RestController
public class SectionConfirmController extends AbstractController {

    @Autowired
    AhuService ahuService;

    @Autowired
    PriceService priceService;

	/**
	 * 确认段数据
	 *
	 * @param ahuVO
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "sec/confirm", method = RequestMethod.POST)
	public ApiResult<Object> sectionConfirm(AhuVO ahuVO) throws Exception {
		AhuParam para = BeanTransferUtil.getAhuParam(ahuVO);
		Map<String, Object> map = para.getParams();
		para.setLayout(AhuLayoutUtils.parse(ahuVO.getLayoutStr()));

		// 获取AHU的序列号
		String serialMeta = map.get(RestConstant.METAHU_SERIAL).toString();
		String serial = para.getSeries(); // serial = "39CQ1480";
		if (StringUtils.isEmpty(serial)) {
			if (StringUtils.isEmpty(serialMeta)) {
				throw new ApiException(ErrorCode.UNIT_MODEL_NOT_SELECTED);
			} else {
				serial = serialMeta;
				para.setSeries(serialMeta);
			}
		}
		// 获取AHU参数
		List<String> sectionTypes = new ArrayList<>();
		sectionTypes.add(SectionTypeEnum.TYPE_AHU.getId());

		// 遍历第一个段的信息
		PartParam part = para.getPartParams().get(0);
		Map<String, Object> tmap = part.getParams();
		map.putAll(tmap);
		sectionTypes.add(part.getKey().toLowerCase());
		DefaultCalContext rootContext = new DefaultCalContext();
		rootContext.setText(ICalContext.NAME_ROOT);
		rootContext.setName(para.getName());
		LanguageEnum languageEnum = getLanguage();

		//点击前端非标计算按钮只进行非标计算引擎调用，其他引擎跳过
		if(!RestConstant.SYS_ASSERT_TRUE.equals(para.getIsNS())){
			DataValidationUtil.getPerformDataValidation(tmap, part.getKey(), languageEnum);
		}

		//confirm 计算级别
		AhuCalBus.getInstance().setCalOperation(CalOperationEnum.COMFIRM);
		AhuCalBus.getInstance().calculate(para, getLanguage(), rootContext);
		Map<String, Object> result = part.getParams();
		result.put(SystemCalculateConstants.META_SECTION_COMPLETED, true);// 页面选择确定时修改当前段的计算状态
		// If it is for confirmation operation, only one part needed to be
		// returned.
		System.out.println(JSON.toJSONString(rootContext));
		return ApiResult.success(result);
	}

	/**
	 * 计算AHU非标价格。
	 * 
	 * @param ahuVO
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "ahu/nsprice", method = RequestMethod.POST)
    public ApiResult<Object> ahuNsPrice(AhuVO ahuVO) throws Exception {
        AhuParam ahuParam = BeanTransferUtil.getAhuParam(ahuVO);
        Map<String, Object> result = ahuParam.getParams();
        Map<String, Object> ahuMetaMap = ahuParam.getParams();

        String nsEnable = String.valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_ENABLE)));
        if (RestConstant.SYS_ASSERT_TRUE.equals(nsEnable)) {
            NSPriceParam nsPriceParam = new NSPriceParam();
            nsPriceParam.setSerial(String.valueOf(ahuMetaMap.get(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_SERIAL))));
            nsPriceParam.setUnitid(ahuParam.getUnitid());
            // channel steel base price
            nsPriceParam.setNs_ahu_nsChannelSteelBase(
                    String.valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE))));
            nsPriceParam.setNs_ahu_nsChannelSteelBaseModel(String
                    .valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_MODEL))));
            // deformation price
            nsPriceParam.setBoxPrice(NumberUtils
                    .toDouble(String.valueOf(ahuMetaMap.get(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_BOX_PRICE)))));
            nsPriceParam.setNs_ahu_deformation(
                    String.valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATION))));
            nsPriceParam.setNswidth(NumberUtils
                    .toInt(String.valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_WIDTH)))));
            nsPriceParam.setNsheight(NumberUtils
                    .toInt(String.valueOf(ahuMetaMap.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_HEIGHT)))));

            Map<String, String> nsPriceMap = this.priceService.getNSPrice(nsPriceParam);
            Iterator<Map.Entry<String, String>> nsPriceIterator = nsPriceMap.entrySet().iterator();

            while (nsPriceIterator.hasNext()) {
                Map.Entry<String, String> entry = nsPriceIterator.next();
                String mapKey = String.valueOf(entry.getKey());
                String mapValue = String.valueOf(entry.getValue());
                result.put(SectionMetaUtils.getNSAHUKey(mapKey), NumberUtil.parseInt(mapValue));
            }
        } else { // clear ns price if disabled
            result.put(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_PRICE), 0);
            result.put(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATION), 0);
        }
        result.put(SystemCalculateConstants.META_SECTION_COMPLETED, true);
        return ApiResult.success(result);
    }

	/**
	 * 根据风机类型和过滤器抽取形式获得过滤器大小和个数
	 *
	 * @param fanType
	 * @param filterFormat
	 *            2，正抽 3，外抽 5，侧抽
	 * @return
	 */
	@RequestMapping(value = "sec/filtercomposings/{fanType}/{filterFormat}", method = RequestMethod.POST)
	public ApiResult<List<FilterComposing>> findComposings(@PathVariable("fanType") String fanType,
			@PathVariable("filterFormat") String filterFormat) {
		if (StringUtils.isNotBlank(fanType)) {
		    throw new ApiException(ErrorCode.FAN_TYPE_IS_EMPTY);
		}
		if (StringUtils.isBlank(filterFormat)) {
		    throw new ApiException(ErrorCode.FAN_FILTER_TYPE_IS_EMPTY);
		}
		int filterFormatV = 0;
		try {
			filterFormatV = Integer.parseInt(filterFormat);
		} catch (Exception e) {

		}
		if (RestConstant.SYS_STRING_MEDIALOADING_FRONTLOADING.equals(filterFormat)
				|| RestConstant.SYS_STRING_MEDIALOADING_FRONTLOADING_CN.equals(filterFormat)) {
			filterFormatV = 2;
		}
		if (RestConstant.SYS_STRING_MEDIALOADING_SIDELOADING.equals(filterFormat)
				|| RestConstant.SYS_STRING_MEDIALOADING_SIDELOADING_CN.equals(filterFormat)) {
			filterFormatV = 5;
		}
		if (RestConstant.SYS_STRING_MEDIALOADING_EXTERNALLOADING_CN.equals(filterFormat)) {
			filterFormatV = 3;
		}
		List<FilterComposing> list = new ArrayList<>();
		List<FilterComposing> listTable = AhuMetadata.findAll(FilterComposing.class);
		for (FilterComposing item : listTable) {
			if (item.getFilterFormat() == filterFormatV && item.getFanType().equals(fanType)) {
				list.add(item);
				continue;
			}
		}

		return ApiResult.success(list);
	}

}
