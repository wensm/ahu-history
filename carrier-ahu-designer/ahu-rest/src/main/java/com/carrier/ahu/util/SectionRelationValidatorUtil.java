package com.carrier.ahu.util;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.section.SectionRelationUtils;
import com.carrier.ahu.vo.AirDirectionVO;
import com.carrier.ahu.vo.SectionRelationVO;

import java.text.MessageFormat;
import java.util.List;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER;
import static com.carrier.ahu.common.intl.I18NConstants.CAN_NOT_BE_FOLLOWED_BY;

/**
 * 段关系校验
 */
public final class SectionRelationValidatorUtil {

    /**
     * 前台拖拽段顺序后，校验最新顺序是否符合规则
     * @param airDirectionVOList
     * @throws ApiException
     */
    public static void dragDropValidate(List<AirDirectionVO> airDirectionVOList) throws ApiException {
        boolean hasCombinedmixingChamber = hasCombinedmixingChamber(airDirectionVOList);
        //规则：新回排两侧必须个包含一个风机
        if(hasCombinedmixingChamber)
            validateCombinedmixingChamber(airDirectionVOList);

        if(hasCombinedmixingChamber && airDirectionVOList.size()==2){
            airDirectionVOList.get(1).getValue().addAll(airDirectionVOList.get(0).getValue());
            airDirectionVOList.remove(0);

        }


        for (AirDirectionVO airDirectionVO : airDirectionVOList) {
            List<SectionRelationVO> relationVOs = airDirectionVO.getValue();
            /**
             * 1：段前后关系规则
             * 按照 section_relation.csv 第一行、左边的第一列分别作为横纵坐标轴；
             * 横坐标为前个段，纵坐标为后个段
             * 横纵坐标确定的点不为空，则允许此前后段关系。
             */
            for (int i = 0; i < relationVOs.size(); i++) {
                SectionRelationVO relationUp = relationVOs.get(i);
                if(relationUp.getName().equals(TYPE_COMBINEDMIXINGCHAMBER.getId())){
                    hasCombinedmixingChamber = true;
                }

                if (i < relationVOs.size() - 1) {
                    SectionRelationVO relationDown = relationVOs.get(i + 1);
                    //左、右必须是：1/2/3
                    if (!SectionRelationUtils.isEnabled(relationUp.getName(), relationDown.getName())) {
                        if (i < relationVOs.size() - 2) {
                            SectionRelationVO relationAfterDown = relationVOs.get(i + 2);
                            if (SectionRelationUtils.isEnabledAfterDown(relationDown.getName(), relationAfterDown.getName())) {
                                continue;
                            }
                        }
                        String upCnName = getIntlString(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()).getCnName());
                        String downCnName = getIntlString(SectionTypeEnum.getSectionTypeFromId(relationDown.getName()).getCnName());

                        Object[] params = {upCnName,downCnName};
                        StringBuffer errorStr = new StringBuffer(MessageFormat.format(getIntlString(CAN_NOT_BE_FOLLOWED_BY), params));//默认提醒
                        errorStr = appendErrorStr(errorStr,relationUp.getName(),relationDown.getName());//详细提醒
                        throw new ApiException(errorStr.toString());
                    }

                }
            }


            /**
             * 2：特殊关系规则
             */

            //规则：判断第一个段的有效性
            validateFirstSection(relationVOs);
            //规则：一个风向里面不可以超过两个风机
            if(!hasCombinedmixingChamber)
            validateFanCount(airDirectionVO, relationVOs);
            //规则：有控制段，必须有风机
            validateFanCount1(airDirectionVO, relationVOs);
            //规则：高效过滤段需要在同一风向包含单层（袋式：暂时不考虑必须是袋式）过滤段或者综合过滤段可选。
            validateHepaFilter(airDirectionVO, relationVOs);

        }
        //规则：热回收不能删除风机段
        validateHeatRecycle(airDirectionVOList);
    }

    /**
     * 详细段前后关系提醒信息
     * ：对csv 配置关系的进一步描述
     * @param errorStr
     * @param relationUpName
     * @param relationDownName
     * @return
     */
    private static StringBuffer appendErrorStr(StringBuffer errorStr, String relationUpName, String relationDownName) {
        String[] typeNames = {relationUpName,relationDownName};
        for(String typeName:typeNames) {
            switch (SectionTypeEnum.getSectionTypeFromId(typeName)) {
                case TYPE_CTR: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_CTR1));
                    break;
                }
                case TYPE_FAN: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_FAN1));
                    break;
                }
                case TYPE_COLD: {
                    errorStr.append("");
                    break;
                }
                case TYPE_HEATINGCOIL: {
                    errorStr.append("");
                    break;
                }
                case TYPE_SINGLE: {
                    errorStr.append("");
                    break;
                }
                case TYPE_COMPOSITE: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_COMBINEDFILTER1));
                    break;
                }
                case TYPE_HEPAFILTER: {
                    errorStr.append("");
                    break;
                }
                case TYPE_ELECTROSTATICFILTER: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_ELECTROSTATICFILTER1));
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_ELECTROSTATICFILTER2));
                    break;
                }
                case TYPE_WETFILMHUMIDIFIER: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_WETFILMHUMIDIFIER1));
                    break;
                }
                case TYPE_STEAMHUMIDIFIER: {
                    errorStr.append("");
                    break;
                }
                case TYPE_SPRAYHUMIDIFIER: {
                    errorStr.append(";"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_SPRAYHUMIDIFIER1));
                    break;
                }
                case TYPE_ELECTRODEHUMIDIFIER: {
                    errorStr.append("");
                    break;
                }
                case TYPE_ATTENUATOR: {
                    errorStr = new StringBuffer("");
                    errorStr.append(";-"+getIntlString(I18NConstants.PARTITION_POS_VALIDATE_ATTENUATOR1));
                    break;
                }
            }
        }


        return errorStr;
    }

    /**
     * 热回收验证
     * @param airDirectionVOList
     */
    private static void validateHeatRecycle(List<AirDirectionVO> airDirectionVOList) {
        /*根据前段提供的集合顺序排序*/
        for (AirDirectionVO airDirectionVO : airDirectionVOList) {
            List<SectionRelationVO> sectionRelationVOList = airDirectionVO.getValue();
            for (int i = 0; i < sectionRelationVOList.size(); i++) {
                SectionRelationVO sectionRelationVO = sectionRelationVOList.get(i);
                sectionRelationVO.setPosition(i);
            }
        }
        boolean isWheelHeatRecycle = false;
        boolean isPlatHeatRecycle = false;
        boolean isSFan = false;
        boolean isRFan = false;
        int sPosition = 0;
        int rPosition = 0;
        boolean isSFilter = false;
        boolean isRFilter = false;
        boolean isSCoil = false;
        boolean isRCoil = false;
        for (AirDirectionVO airDirectionVO : airDirectionVOList) {
            String airDirection = airDirectionVO.getAirDirection();
            List<SectionRelationVO> sectionRelationVOList = airDirectionVO.getValue();
            for (SectionRelationVO sectionRelationVO : sectionRelationVOList) {
                if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId())) {
                    isWheelHeatRecycle = true;
                }
                if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())) {
                    isPlatHeatRecycle = true;
                }
                if (isWheelHeatRecycle || isPlatHeatRecycle) {//板式或者转轮 验证风机
                    if (airDirection.equals(AirDirectionEnum.SUPPLYAIR.getCode())) {
                        if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())) {
                            sPosition = sectionRelationVO.getPosition();
                        }
                        if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_FAN.getId())||sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_WWKFAN.getId())) {
                            isSFan = true;
                        }

                    } else if (airDirection.equals(AirDirectionEnum.RETURNAIR.getCode())) {
                        if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())) {
                            rPosition = sectionRelationVO.getPosition();
                        }
                        if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_FAN.getId())||sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_WWKFAN.getId())) {
                            isRFan = true;
                        }
                    }
                }

            }
        }
        if (isWheelHeatRecycle || isPlatHeatRecycle) {
            if (!isSFan || !isRFan) {//风机不能删除
                throw new ApiException(ErrorCode.RECYCLE_INCLUDE_FAN);
            }
        }
        if (isWheelHeatRecycle || isPlatHeatRecycle) {//转轮 单独验证过滤段
            for (AirDirectionVO airDirectionVO : airDirectionVOList) {
                String airDirection = airDirectionVO.getAirDirection();
                List<SectionRelationVO> sectionRelationVOList = airDirectionVO.getValue();
                for (SectionRelationVO sectionRelationVO : sectionRelationVOList) {
                    if (airDirection.equals(AirDirectionEnum.SUPPLYAIR.getCode())) {
                        //转轮 热回收段前缺少过滤段
                        if (sectionRelationVO.getPosition() < sPosition) {
                            if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_SINGLE.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_COMPOSITE.getId())
                                    || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_HEPAFILTER.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId())) {
                                isSFilter = true;
                            }
                            if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_COLD.getId())) {
                                isSCoil = true;
                            }
                        }
                    } else if (airDirection.equals(AirDirectionEnum.RETURNAIR.getCode())) {
                        if (sectionRelationVO.getPosition() < rPosition) {
                            if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_SINGLE.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_COMPOSITE.getId())
                                    || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_HEPAFILTER.getId()) || sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId())) {
                                isRFilter = true;
                            }
                            if (sectionRelationVO.getName().equals(SectionTypeEnum.TYPE_COLD.getId())) {
                                isRCoil = true;
                            }
                        }
                    }
                }
            }
            if (!isSFilter || !isRFilter) {
                throw new ApiException(ErrorCode.RECYCLE_INCLUDE_FILTER);
            }
            if (isSCoil || isRCoil) {//冷水盘管段需要放置热回收段后
                throw new ApiException(ErrorCode.COIL_NEED_AFTER_RECYCLE);
            }
        }
    }
    /**
     * 新回排校验
     * @param airDirectionVOList
     */
    private static void validateCombinedmixingChamber(List<AirDirectionVO> airDirectionVOList) {
        for (AirDirectionVO airDirectionVO : airDirectionVOList) {
            List<SectionRelationVO> relationVOs = airDirectionVO.getValue();

            int fanCount = 0;
            fanCount = getFanCount(relationVOs, fanCount);

            if (fanCount == 0) {
                StringBuffer msg = new StringBuffer(RestConstant.SYS_BLANK);
                msg.append(RestConstant.SYS_ALPHABET_S_UP.equals(airDirectionVO.getAirDirection()) ? getIntlString(I18NConstants.SUPPLY_WIND) : getIntlString(I18NConstants.RETURN_AIR));
                msg.append(RestConstant.SYS_PUNCTUATION_COMMA);
                msg.append(getIntlString(I18NConstants.FAN_NUMBER_NOT_LESS_THAN_ONE));
                msg.append(RestConstant.SYS_PUNCTUATION_CN_DOT);
                throw new ApiException(msg.toString());
            }
            if (fanCount > 1) {
                StringBuffer msg = new StringBuffer(RestConstant.SYS_BLANK);
                msg.append(RestConstant.SYS_ALPHABET_S_UP.equals(airDirectionVO.getAirDirection()) ? getIntlString(I18NConstants.SUPPLY_WIND) : getIntlString(I18NConstants.RETURN_AIR));
                msg.append(RestConstant.SYS_PUNCTUATION_COMMA);
                msg.append(getIntlString(I18NConstants.FAN_NUMBER_NOT_GREATER_THAN_ONE));
                msg.append(RestConstant.SYS_PUNCTUATION_CN_DOT);
                throw new ApiException(msg.toString());
            }
        }
    }
    /**
     * 1:高效过滤段需要在同一风向包含单层（袋式：暂时不考虑必须是袋式）过滤段或者综合过滤段可选。
     * 2:同一风向第一个过滤段一定不能是高效过滤段
     * 3:同一风向第一个过滤段一定不能是静电过滤段
     * @param airDirectionVO
     * @param relationVOs
     * @throws ApiException
     */
    private static void validateHepaFilter(AirDirectionVO airDirectionVO, List<SectionRelationVO> relationVOs) throws ApiException {
        //1:判断是否包含高效过滤段
        int hepaFilterPos = -1;
        for (int i = 0; i < relationVOs.size(); i++) {
            SectionRelationVO relationUp = relationVOs.get(i);
            if (SectionTypeEnum.TYPE_HEPAFILTER.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                hepaFilterPos = i;
            }
        }
        if (hepaFilterPos > -1) {
            boolean canHepa = false;
            for (int i = 0; i < relationVOs.size(); i++) {
                SectionRelationVO relationUp = relationVOs.get(i);
                if (SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))
                        || SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                    //有单层或综合过滤段则高效过滤段可以出现
                    canHepa = true;
                    break;
                }
            }
            if (!canHepa) {
                throw new ApiException(";-"+getIntlString(I18NConstants.SECTION_HEPA_AFTER_FILTER_CFILTER));
            }
        }


        //2同一风向第一个过滤段一定不能是高效过滤段
        if (hepaFilterPos > -1) {
            boolean canHepa = false;
            for (int i = 0; i < relationVOs.size(); i++) {
                SectionRelationVO relationUp = relationVOs.get(i);
                if (SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))
                        || SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                    //有单层或综合过滤段在高效过滤段前面
                    if(i<hepaFilterPos) {
                        canHepa = true;
                        break;
                    }
                }
            }
            if (!canHepa) {
                throw new ApiException(getIntlString(I18NConstants.SECTION_HEPA_AFTER_FILTER_FIRST));
            }
        }

        //3同一风向第一个过滤段一定不能是静电过滤段
        int electrostaticFilterPos = -1;
        for (int i = 0; i < relationVOs.size(); i++) {
            SectionRelationVO relationUp = relationVOs.get(i);
            if (SectionTypeEnum.TYPE_ELECTROSTATICFILTER.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                electrostaticFilterPos = i;
            }
        }
        if (electrostaticFilterPos > -1) {
            boolean canHepa = false;
            for (int i = 0; i < relationVOs.size(); i++) {
                SectionRelationVO relationUp = relationVOs.get(i);
                if (SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))
                        || SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                    //有单层或综合过滤段在静电过滤段前面
                    if(i<electrostaticFilterPos) {
                        canHepa = true;
                        break;
                    }
                }
            }
            if (!canHepa) {
                throw new ApiException(getIntlString(I18NConstants.SECTION_ELECTROSTATIC_FILTER_FIRST));
            }
        }

    }

    /**
     * 一个风向里面不可以超过两个风机
     * @param airDirectionVO
     * @param relationVOs
     * @throws ApiException
     */
    private static void validateFanCount(AirDirectionVO airDirectionVO, List<SectionRelationVO> relationVOs) throws ApiException {
        int fanCount = 0;
        fanCount = getFanCount(relationVOs, fanCount);
        if (fanCount > 1) {
            StringBuffer msg = new StringBuffer(RestConstant.SYS_BLANK);
            msg.append(RestConstant.SYS_ALPHABET_S_UP.equals(airDirectionVO.getAirDirection()) ? getIntlString(I18NConstants.SUPPLY_WIND) : getIntlString(I18NConstants.RETURN_AIR));
            msg.append(RestConstant.SYS_PUNCTUATION_COMMA);
            msg.append(getIntlString(I18NConstants.FAN_NUMBER_NOT_GREATER_THAN_ONE));
            msg.append(RestConstant.SYS_PUNCTUATION_CN_DOT);
            throw new ApiException(msg.toString());
        }
    }

    /**
     * 有控制段，必须有风机
     * @param airDirectionVO
     * @param relationVOs
     * @throws ApiException
     */
    private static void validateFanCount1(AirDirectionVO airDirectionVO, List<SectionRelationVO> relationVOs) throws ApiException {

        //判断是否包含控制单段
        int ctrPos = -1;
        for (int i = 0; i < relationVOs.size(); i++) {
            SectionRelationVO relationUp = relationVOs.get(i);
            if (SectionTypeEnum.TYPE_CTR.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                ctrPos = i;
            }
        }


        int fanCount = 0;
        fanCount = getFanCount(relationVOs, fanCount);
        if (ctrPos >-1 && fanCount == 0) {
            throw new ApiException(getIntlString(I18NConstants.SECTION_FAN_CANNOT_BE_DEL_CAUSE_CTRL));
        }
    }

    /**
     * 获取风机个数
     * @param relationVOs
     * @param fanCount
     * @return
     */
    private static int getFanCount(List<SectionRelationVO> relationVOs, int fanCount) {
        for (int i = 0; i < relationVOs.size(); i++) {
            SectionRelationVO relationUp = relationVOs.get(i);
            if (SectionTypeEnum.TYPE_FAN.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))||SectionTypeEnum.TYPE_WWKFAN.equals(SectionTypeEnum.getSectionTypeFromId(relationUp.getName()))) {
                fanCount++;
            }
        }
        return fanCount;
    }

    /**
     * 判断第一个段的有效性
     * @param relationVOs
     * @throws ApiException
     */
    private static void validateFirstSection(List<SectionRelationVO> relationVOs) throws ApiException {
        /*User u = getUser();
        String r = u.getUserRole();*/
        if (relationVOs.size() > 0 && false) {// TODO等面板稳定后开启第一个段的验证
            SectionRelationVO firstVO = relationVOs.get(0);
            if (null != firstVO) {
                if (!SectionRelationUtils.isFirstRelationEnabled(firstVO.getName())) {
                    String tranName = getIntlString(SectionTypeEnum.getSectionTypeFromId(firstVO.getName()).getCnName());
                    throw new ApiException(ErrorCode.CAN_NOT_BE_AS_FIRST_SECTION, tranName);
                }
            }
        }
    }
    /**
     * 是否新回排机组
     * @param airDirectionVOList
     * @return
     */
    private static boolean hasCombinedmixingChamber(List<AirDirectionVO> airDirectionVOList) {
        boolean hasCombinedmixingChamber = false;
        for (AirDirectionVO airDirectionVO : airDirectionVOList) {
            List<SectionRelationVO> relationVOs = airDirectionVO.getValue();
            /**
             * 1：段前后关系规则
             * 按照 section_relation.csv 第一行、左边的第一列分别作为横纵坐标轴；
             * 横坐标为前个段，纵坐标为后个段
             * 横纵坐标确定的点不为空，则允许此前后段关系。
             */
            for (int i = 0; i < relationVOs.size(); i++) {
                SectionRelationVO relationUp = relationVOs.get(i);
                if (relationUp.getName().equals(TYPE_COMBINEDMIXINGCHAMBER.getId())) {
                    hasCombinedmixingChamber = true;
                }
            }
        }
        return hasCombinedmixingChamber;
    }
}
