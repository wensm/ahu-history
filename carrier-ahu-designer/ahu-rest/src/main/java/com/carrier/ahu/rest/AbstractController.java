package com.carrier.ahu.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.UserService;
import com.carrier.ahu.util.upgrade.UpgradeUtils;

/**
 * Created by Wen zhengtao on 2017/3/16.
 */
public abstract class AbstractController {

    protected static Logger logger = LoggerFactory.getLogger(AbstractController.class);

    @Autowired
    HttpServletRequest request;

    @Autowired
    HttpServletResponse response;

    @Autowired
    UserService userService;

    /**
     * 获取用户名
     *
     * @return
     */
    protected String getUserName() {
        User user = getUser();
        return user.getUserName();
    }

    /**
     * 获取用户对象更丰富的信息
     *
     * @return
     */
    protected User getUser() {
        User user = AHUContext.getUser();
        if (user == null) {
            user = new User();
            user.setPreferredLocale(LanguageEnum.Chinese.getId());
            user.setUnitPreferCode(UnitSystemEnum.M.getId());
            user.setUnitPrefer(AhuSectionMetas.getInstance().getUnitSetting().get(UnitSystemEnum.M.getId()));
            user.setDefaultParaPrefer(TemplateUtil.getDefaultParameterFromMeta());
        } else if (user.getUnitPrefer() == null) {
            user.setUnitPrefer(AhuSectionMetas.getInstance().getUnitSetting().get(user.getUnitPreferCode()));
        }
        return user;
    }

    /**
     * 获取用户选中的语言
     *
     * @return
     */
    protected LanguageEnum getLanguage() {
        return AHUContext.getLanguage();
    }

    public String getIntlString(String resourceId) {
        return AHUContext.getIntlString(resourceId);
    }

    /**
     * 获取用户选中的单位制
     *
     * @return
     */
    protected UnitSystemEnum getUnitSystem() {
        return AHUContext.getUnitSystem();
    }

    protected ResponseEntity<byte[]> download(File file, String fileName) throws IOException {
        byte[] documentBody = FileUtils.readFileToByteArray(file);
        HttpHeaders headers = new HttpHeaders();
        // // 修复IE下载异常问题
        // headers.setContentDispositionFormData("attachment", fileName);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, RestConstant.SYS_PATH_ATTACHMENT + RestConstant.SYS_PUNCTUATION_DB_QUOTATION + fileName + RestConstant.SYS_PUNCTUATION_DB_QUOTATION);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentLength(documentBody.length);
        return new ResponseEntity<byte[]>(documentBody, headers, HttpStatus.OK);
    }

    /**
     * 下载文件
     *
     * @return
     * @throws IOException
     */
    protected ResponseEntity<byte[]> download(String filePath, String fileName) throws IOException {
        return download(new File(filePath), fileName);
    }

    protected ResponseEntity<byte[]> download(byte[] body, String fileName) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, RestConstant.SYS_PATH_ATTACHMENT + RestConstant.SYS_PUNCTUATION_DB_QUOTATION + fileName + RestConstant.SYS_PUNCTUATION_DB_QUOTATION);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentLength(body.length);
        return new ResponseEntity<byte[]>(body, headers, HttpStatus.OK);
    }

    protected void unzipUpgradeFile(InputStream zipInStream) throws Exception {
        ZipInputStream zipIn = null;
        FileOutputStream out = null;
        try {
            zipIn = new ZipInputStream(zipInStream);
            ZipEntry entry = zipIn.getNextEntry();
            if (entry == null) {
                throw new ApiException(ErrorCode.INVALID_UPGRADE_FILE);
            }
            while (entry != null) {
                if (!entry.isDirectory()) { // ignore directory
                    if (UpgradeUtils.isValidJarVersion(entry.getName(), AHUContext.getAhuVersion())) {
                        File file = new File(RestConstant.SYS_UPGRADE_AHU_LIB_FOLDER + entry.getName());
                        file.createNewFile();
                        out = new FileOutputStream(file);
                        int b = 0;
                        while ((b = zipIn.read()) != -1) {
                            out.write(b);
                        }
                        out.close();
                    } else {
                        throw new ApiException(ErrorCode.INVALID_UPGRADE_FILE);
                    }
                }
                entry = zipIn.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zipIn);
            IOUtils.closeQuietly(out);
        }
    }

}
