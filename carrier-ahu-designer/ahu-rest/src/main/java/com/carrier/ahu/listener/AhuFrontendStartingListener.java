package com.carrier.ahu.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;

import com.carrier.ahu.constant.RestConstant;

/**
 * 当spring boot应用开始启动的时候，打开浏览器 1，打开frontend窗口 <br/>
 * 
 * Created by Wen zhengtao on 2017/3/28.
 */
public class AhuFrontendStartingListener implements ApplicationListener<ApplicationStartingEvent> {
    private static Logger logger = LoggerFactory.getLogger(AhuFrontendStartingListener.class);

    @Autowired
    Environment environment;

    public AhuFrontendStartingListener() {
        logger.debug(RestConstant.LOG_AHU_FRONTED_STARTING_LISTENER_INIT);
    }

    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        try {
            if (isWindowsSystem()) {
                AhuUI.openFrontendWindow();
            } else {
                logger.warn(RestConstant.LOG_SKIP_OPENNING_CLIENT_SINCE_SYSTEM_IS + System.getProperty(RestConstant.SYS_PROPERTY_OS_NAME));
            }
        } catch (Exception e) {
            logger.error(RestConstant.LOG_BLANK, e);
        }
    }

    public static final boolean isWindowsSystem() {
        String os = System.getProperty(RestConstant.SYS_PROPERTY_OS_NAME);
        return os.toLowerCase().contains(RestConstant.SYS_PROPERTY_WINDOWS);
    }
 
}
