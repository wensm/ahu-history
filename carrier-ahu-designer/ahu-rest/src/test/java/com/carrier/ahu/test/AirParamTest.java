package com.carrier.ahu.test;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.po.meta.AirParam;
import com.carrier.ahu.util.AirParamGen;
import com.carrier.ahu.util.EmptyUtil;

import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AirParamTest extends AbstractTest {

	@Test
	public void testCalculatePrice() {
		List<AirParam> airParams = AirParamGen.getAirParam();
		String filePath = "/data/default_air_param.json";
		FileWriter fw = null;
		PrintWriter out = null;
		try {
			fw = new FileWriter(filePath);
			out = new PrintWriter(fw);
			JSONObject json = (JSONObject) JSONObject.toJSON(airParams);
			out.write(json.toString());
			out.println();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (fw!=null) {
					fw.close();
				}
				if (out!=null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
