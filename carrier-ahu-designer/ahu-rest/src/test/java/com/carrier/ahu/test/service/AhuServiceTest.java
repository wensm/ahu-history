package com.carrier.ahu.test.service;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.test.AbstractTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Wen zhengtao on 2017/3/28.
 */
public class AhuServiceTest extends AbstractTest {
    @Autowired
    AhuService ahuService;

    @Test
    public void addAhuTest(){
        try {
            Unit unit = new Unit();
            unit.setPid("34");
            unit.setGroupId("1");
            unit.setUnitNo("123456");
            unit.setDrawingNo("1234");
            unit.setSeries("abc");
            unit.setMount((short)1);
            unit.setName("测试名称");
            unit.setWeight(10D);
            unit.setPrice(10D);
            unit.setPaneltype("测试类型");
            unit.setCustomerName("开利");
            String ahuId = ahuService.addAhu(unit,"admin");
            System.out.println(ahuId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void updateAhuTest(){
        try {
            String ahuId = "1";
            Unit unit = ahuService.findAhuById(ahuId);
            if(null != unit){
                unit.setName("测试名称update");
                ahuService.updateAhu(unit,"admin");
                System.out.println(unit.getUnitid());
            }
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deleteAhuTest(){
        try {
            String ahuId = "1";
            ahuService.deleteAhu(ahuId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findAhuByIdTest(){
        try {
            String ahuId = "1";
            Unit unit = ahuService.findAhuById(ahuId);
            System.out.println(unit);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findAhuListTest(){
        try {
            String projectId = "34";
            String groupId = "1";
            List<Unit> unitList = ahuService.findAhuList(projectId,groupId);
            System.out.println(unitList);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
