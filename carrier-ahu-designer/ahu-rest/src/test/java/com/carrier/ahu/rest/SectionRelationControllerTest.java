package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.vo.AirDirectionVO;
import com.carrier.ahu.vo.SectionRelationVO;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liangd4 on 2018/3/15.
 */
public class SectionRelationControllerTest {
    @Test
    public void validate() throws Exception {
        List<AirDirectionVO> airDirectionVOList = new ArrayList<AirDirectionVO>();
        List<SectionRelationVO> sectionRelationVOS = new ArrayList<SectionRelationVO>();
        List<SectionRelationVO> sectionRelationVOR = new ArrayList<SectionRelationVO>();
        SectionRelationVO sectionRelationVO1 = new SectionRelationVO();
        sectionRelationVO1.setId("1");
        sectionRelationVO1.setName("ahu.mix1");
        sectionRelationVO1.setLine(1);
        SectionRelationVO sectionRelationVO2 = new SectionRelationVO();
        sectionRelationVO2.setId("2");
        sectionRelationVO2.setName("ahu.mix2");
        sectionRelationVO2.setLine(2);
        sectionRelationVOS.add(sectionRelationVO1);
        sectionRelationVOS.add(sectionRelationVO2);
        sectionRelationVOR.add(sectionRelationVO1);
        sectionRelationVOR.add(sectionRelationVO2);
        AirDirectionVO airDirectionVOR = new AirDirectionVO();
        airDirectionVOR.setAirDirection("R");
        airDirectionVOR.setValue(sectionRelationVOS);
        airDirectionVOList.add(airDirectionVOR);
        AirDirectionVO airDirectionVOS = new AirDirectionVO();
        airDirectionVOS.setAirDirection("S");
        airDirectionVOS.setValue(sectionRelationVOR);
        airDirectionVOList.add(airDirectionVOS);
        System.out.println(JSONArray.toJSONString(airDirectionVOList));
    }
}