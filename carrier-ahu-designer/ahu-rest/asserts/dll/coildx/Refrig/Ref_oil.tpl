//14 "Ordinary Refrigerants: first one is the default; all properties from NIST RefProp 7.0"
115  R11     generic  
117  R113    generic  
118  R114    generic  
102  R12     generic  
116  R123    generic  
121  R13     generic  
103  R134a   generic  
123  R142b   generic  
124  R152a   generic  
101  R22     generic  
111  R245fa  generic  
104  R290    generic  
122  R32     generic  
105  R404A   generic  "44% R125/52% R143a/4% 134a"
130  R407A   generic  "20% R32/40% R125/40% R134a"
106  R407C   generic  "23% R32/25% R1225/52% R134a"
107  R410A   generic  "50% R32/50% R125"
128  R410C   generic  "60% R32/40% R125"
126  R417A   generic  "46.6% R125/50% R134a/3.4% R600"
129  R422D   generic  "65.1% R125/31.5% R134a/3.4% 600a"
119  R500    generic  "73.8% R12/26.2% R152a"
120  R502    generic  "48.8% R22/51.2% R115"
108  R507A   generic  "50% R125/50% R143a"
112  R6A290  generic  "50% isobutane/ 50% propane"
125  R600a   generic
109  R717    generic  "Ammonia"
113  R718    generic  "Water"
114  R744    generic  "Carbon Dioxide"
110  RFx220  generic  "21.5% R32/74% R134a/4.5% R23"
127  RF_H    generic
//11 "Solubility and viscosity - given (T,Fr) or (T,P)"
17   R134a   RL32H       "Emkarate RL 32H          "
4    R134a   SW20        "Castrol SW20             "
13   R134a   SW220       "Castrol SW220            "
6    R22     AB300       "AB-300 (Zerol & Zephron) "
16   R22     MO          "Mineral Oil 3GS          "
2    R404A   EAL68       "Mobil EAL Arctic 68      "
15   R404A   PVE68DX3    "IK PVE68DX3              "
1    R404A   Solest37    "CPI EXP 12476 (Solest-37)"
14   R404A   Solest120   "CPI Solest 120           "
7    R410A   EAL22cc     "Mobil EAL Arctic 22cc    "
8    R410A   EAL68       "Mobil EAL Arctic 68      "
11   R410A   PVE68       "PVE-ET/TB68              "
10   R410A   RL22H       "ICI RL 22H               "
18   R410A   RL32H       "Emkarate RL 32H          "
//81 "Solubility and viscosity - given (T,Fr) NOT (T,P)"
//null category now
//4 "Viscosity only"
//12	R134a	SW68        "Castrol SW68             "
// 3	R134a	EAL68       "Mobil EAL Arctic 68      "
// 5	R22	AB150       "AB-150 (Zerol & Zephron) "
//1 "Solubility Only"
// 9	R410A	POE         "Generalized POE          "
