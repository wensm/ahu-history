import React from 'react'
import {Field as FD, reduxForm} from 'redux-form'
import {formatValueForUnit, syncAhuComponentValues} from '../../actions/ahu'


export default class DateField extends React.Component {


    renderSelectField = ({
                             input,
                             label,
                             meta: {touched, error},
                             children,
                             ...custom
                         }) =>
        <SelectField
            floatingLabelText={label}
            errorText={touched && error}
            {...input}
            onChange={(event, index, value) => input.onChange(value)}
            children={children}
            {...custom}
        />

    render() {
        elem = (
            <FD
                component={this.renderSelectField}
                className="form-control"
            />)
        return elem
    }
}


