/* eslint-disable */
import React from 'react';
import ReactModal from 'react-modal';

const customStyles = {
  overlay : {
    zIndex: 99999,
    backgroundColor   : 'rgba(0, 0, 0, 0.75)',
  },
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
  }
};

export default class Modal extends React.Component {
  render() {
    const props = this.props;
    return (
      <ReactModal isOpen={props.isOpen} onAfterOpen={props.onAfterOpen} onRequestClose={props.onRequestClose} style={customStyles} contentLabel={props.contentLabel}>
        {this.props.children}
      </ReactModal>
    );
  }
}