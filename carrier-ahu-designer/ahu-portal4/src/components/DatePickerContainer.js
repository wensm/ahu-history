import { connect } from 'react-redux'
import DatePicker from './DatePicker'

const mapStateToProps = (state, ownProps) => {
  return {
    label: ownProps.label,
    name: ownProps.name,
    value: ownProps.value,
    : ownProps.onSynDateToReduxState,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSynDateToReduxState: (field, value) => {
      dispatch({
        type: '@@redux-form/CHANGE',
        payload: value,
        meta: {
          field,
          form: ownProps.form,
          persistentSubmitErrors: false,
          touch: false,
        }
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker)
