import React from 'react';
import NProgress from 'nprogress'
import swal from 'sweetalert'
import _ from "lodash";
import Header from '../pages/ahu/Header'
import intl from 'react-intl-universal'
import style from './app.css';
import { SupportedLocales } from '../actions/general'
import { Spin, Icon } from 'antd'
import {
    PANEL_NAMES,
    LINE_NAMES,
    PANEL_NAMES_G,
    PANEL_NAMES_XT,
    LINE_NAMES_XT
} from '../pages/partition/DivideConstants'
import {
    OK,
    CANCEL,
    PRIVACY_NOTICE,
    TERMS_OF_USE,
    CONTACT_US,
    CARRIER_OF_UTC_DESCRIPTION,
    CARRIER_CORPORATION,
    CARRIER_AHU_SYSTEM
} from '../pages/intl/i18n'
import UpgradeReminderContainer from './UpgradeReminderContainer'

export default class App extends React.Component {

    currentLocale = "";

    constructor(props) {
        super(props);
        this.state = { initDone: false }
        this.initIntl = this.initIntl.bind(this)
        this.initSweetAlert = this.initSweetAlert.bind(this)
        this.initPanelNames = this.initPanelNames.bind(this)
        this.showSystemUpdate = this.showSystemUpdate.bind(this)
    }
    componentWillMount() {
    }
    componentDidMount() {
        this.props.onFetchMetadata();
        const { locale } = this.props;
        // console.log(this.currentLocale)
        this.currentLocale = intl.determineLocale({
            urlLocaleKey: "lang",
            cookieLocaleKey: "lang"
        });
        if (!_.find(SupportedLocales, { value: this.currentLocale })) {
            this.currentLocale = locale;
        }
        this.props.onFetchUserInfoAndIntl(this.currentLocale, this.initIntl);
    }

    initIntl(locales) {
        // init method will load CLDR locale data according to currentLocale
        // react-intl-universal is singleton, so you should init it only once in your app
        intl.init({
            currentLocale: this.currentLocale,
            locales,
        })
            .then(() => {
                // After loading CLDR locale data, start to render
                document.title = intl.get(CARRIER_AHU_SYSTEM);
                this.initPanelNames();
                this.setState({ initDone: true });
                this.initSweetAlert();
            });
        this.props.onCheckSystemUpdate(this.currentLocale, this.showSystemUpdate);
    }

    initSweetAlert() {
        // set default button text from i18n
        swal.setDefaults({
            confirmButtonText: intl.get(OK),
            cancelButtonText: intl.get(CANCEL),
        });
    }

    initPanelNames() {
        // this is temporary solution
        for (var PANEL_NAME in PANEL_NAMES) {
            PANEL_NAMES[PANEL_NAME] = intl.get(PANEL_NAMES[PANEL_NAME]);
        }
        for (var LINE_NAME in LINE_NAMES) {
            LINE_NAMES[LINE_NAME] = intl.get(LINE_NAMES[LINE_NAME]);
        }

        for (var PANEL_NAME in PANEL_NAMES_G) {
            PANEL_NAMES_G[PANEL_NAME] = intl.get(PANEL_NAMES_G[PANEL_NAME]);
        }
        for (var LINE_NAME in PANEL_NAMES_XT) {
            PANEL_NAMES_XT[LINE_NAME] = intl.get(PANEL_NAMES_XT[LINE_NAME]);
        }
        for (var LINE_NAME in LINE_NAMES_XT) {
            LINE_NAMES_XT[LINE_NAME] = intl.get(LINE_NAMES_XT[LINE_NAME]);
        }
        
    }

    showSystemUpdate(updateInfo) {
        this.setState({ updateInfo: updateInfo });
    }

    render() {
        // this.loadLocales()
        const childrenWithProps = React.Children.map(this.props.children, child => React.cloneElement(child, { data: 'react-redux-starter' }));
        let currentYear = new Date().getFullYear();
        const antIcon = !this.state.initDone ? <Icon type="sync" style={{ fontSize: 48 }} spin /> : ''
        return (
            <Spin
                spinning={!this.state.initDone}
                size="large" style={{ position: 'absolute', top: '25%', left: '50%', fontSize: 48 }}
            //  indicator={antIcon}
            >
                {this.state.initDone &&
                    <div className={style.app}>
                        <div className={style.header} style={{ height: '60px' }}>
                            <Header />
                        </div>
                        <div className={style.main}>
                            {childrenWithProps}
                        </div>
                        <div className={style.footer}>
                            <div className="fotop">
                                <div className="fot_lef fl">
                                    <a href="#" className="">{intl.get(PRIVACY_NOTICE)}  |   </a>
                                    <a href="#" className="">{intl.get(TERMS_OF_USE)}  |  </a>
                                    <a href="#" className="">{intl.get(CONTACT_US)}</a>
                                </div>
                                <div className="fot_rig fr">
                                    <a href="#" className="">{intl.get(CARRIER_OF_UTC_DESCRIPTION)}</a>
                                </div>
                            </div>
                            <p>© {intl.get(CARRIER_CORPORATION)} {currentYear}</p>
                        </div>
                        {
                            this.state.updateInfo &&
                            <UpgradeReminderContainer updateInfo={this.state.updateInfo} />
                        }

                    </div>}
            </Spin>
        )
    }
}

