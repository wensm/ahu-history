import {connect} from 'react-redux'
import UpgradeReminder from './UpgradeReminder'

const mapStateToProps = (state,owProps) => {
    return {
       updateInfo: owProps.updateInfo
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onDownloadUpgradeFile(){
            owProps.onDownloadUpgradeFile();
        },
        onDownloadReleaseFile(){
            owProps.onDownloadReleaseFile()
        },
        onInstallUpgradeFile(){
            owProps.onInstallUpgradeFile();
        },
        onInstallReleaseFile(reports){
            owProps.onInstallReleaseFile();
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpgradeReminder)
