import {
    PARAMETER_BATCH_CONFIG,
    UNIT_CONFIGURATION,
    SUPPLY_FAN_COMPONENT,
    RETURN_FAN_COMPONENT,
    INTERNAL_PANEL,
    OUTSIDE_PANEL,
    EX_FACTORY,
    MIX_SECTION,
    SINGLE_FILTRATION_SECTION,
    COMBINED_FILTRATION_SECTION,
    COOLING_COIL_SECTION,
    FAN_SECTION,
    CANCEL,
    SAVE,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { reduxForm } from 'redux-form'
import classnames from 'classnames'
import style from './AhuInit.css'
import Field from '../pages/ahu/Field'
import DateUtil from '../misc/DateUtils';

import modal_icon1Img from '../images/modal_icon1.png';
import modal_icon2Img from '../images/modal_icon2.png';
import modal_icon3Img from '../images/modal_icon3.png';
import modal_icon4Img from '../images/modal_icon4.png';
import modal_icon5Img from '../images/modal_icon5.png';
import modal_icon6Img from '../images/modal_icon6.png';
import modal_icon7Img from '../images/modal_icon7.png';

class AhuInit extends React.Component{
	constructor(props) {
	    super(props);
	    this.saveAhuInitValues = this.saveAhuInitValues.bind(this);
	}
	saveAhuInitValues(){
		this.props.onSaveAhuInit(this.props.projectId,this.props.AhuInitForm);
	}

    tabItemtzkClick(index) {

    }

    render() {
    return (
			<div className="modal fade" id="AhuInit" data-name="AhuInit" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div className="modal-dialog" role="document">
			    <div className="modal-content" style={{width:'700px'}}>
			      <div className="modal-header">
			        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 className="modal-title" id="myModalLabel"><b>{intl.get(PARAMETER_BATCH_CONFIG)}</b></h4>
			      </div>
			      <div className="modal-body" style={{minHeight:'20vh',maxHeight:'60vh', overflow:'auto'}}>
			      	<form data-name="AhuInit">
				      	<div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading1">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" onClick={() => this.tabItemtzkClick(1)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(UNIT_CONFIGURATION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse1" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.product"/>
							        		</div>
							        	</div>
							        	<div className="row col-lg-6">
							        		<div className="col-lg-12" style={{padding:'0'}}>{intl.get(SUPPLY_FAN_COMPONENT)}</div>
							        		<div className="col-lg-12" style={{padding:'0'}}>
							        			<Field id="meta.ahu.sexternalstatic" />
							        		</div>
							        	</div>
							        	<div className="row col-lg-6" style={{marginLeft:'15px'}}>
							        		<div className="col-lg-12">{intl.get(RETURN_FAN_COMPONENT)}</div>
							        		<div className="col-lg-12">
							        			<Field id="meta.ahu.eexternalstatic" />
							        		</div>
							        	</div>
							        	<div className="row">
							        		<div className="col-lg-12">{intl.get(INTERNAL_PANEL)}</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.inskinm" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.inskincolor" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.inskinw" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.panelinsulation" />
							        		</div>
							        	</div>
							        	<div className="row">
							        		<div className="col-lg-12">{intl.get(OUTSIDE_PANEL)}</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.exskinm" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.exskincolor" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.exskinw" />
							        		</div>
							        	</div>
							        	<div className="row">
							        		<div className="col-lg-12">{intl.get(EX_FACTORY)}</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.delivery" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.package" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.voltage" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.serial" />
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.ahu.baseType" />
							        		</div>
							        	</div>
							        	<div className="row">
							        		<div className="col-lg-12">
							        			<Field id="meta.ahu.ISPRERAIN" />
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading2">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2" onClick={() => this.tabItemtzkClick(2)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(MIX_SECTION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.section.mix.DoorO"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.mix.damperOutlet"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.mix.damperMeterial"/>
							        		</div>
							        		<div className="col-lg-12">
							        			<Field id="meta.section.mix.uvLamp"/>
							        			<Field id="meta.section.mix.style"/>
							        			<Field id="meta.section.mix.fixRepairLamp"/>
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading3">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3" onClick={() => this.tabItemtzkClick(3)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(SINGLE_FILTRATION_SECTION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.BaffleM"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.filterEfficiency"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.filterOptions"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.fitetF"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.fitlerStandard"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.mediaLoading"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.BracketM"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.pressureGuage"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.filter.rimMaterial"/>
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading4">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4" onClick={() => this.tabItemtzkClick(4)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(COMBINED_FILTRATION_SECTION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.LMaterialE"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.RMaterialE"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.rimThickness"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.RMaterial"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.mediaLoading"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.LMaterial"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.BracketM"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.pressureGuageB"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.fitetF"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.pressureGuageP"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.BaffleM"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.combinedFilter.fitlerStandard"/>
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading5">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5" onClick={() => this.tabItemtzkClick(5)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(COOLING_COIL_SECTION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse5" className="panel-collapse collapse" role="tabpane5" aria-labelledby="heading5">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.finType"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.connections"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.eliminator"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.pipe"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.waterCollection"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.drainpanType"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.drainpanMaterial"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.baffleMaterial"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.coilFrameMaterial"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.uTrap"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.coolingCoil.uTrapNum"/>
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
							<div className="panel panel-default">
							    <div className={classnames(style.panel_heading, 'panel-heading')} role="tab" id="heading6">
							        <h4 className="panel-title">
							        	<a id="a1" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6" onClick={() => this.tabItemtzkClick(6)}>
								        	<img src={modal_icon1Img}/>
								            <b>{intl.get(FAN_SECTION)}</b>
								            <i className="iconfont icon-liebiaozhankai pull-right" style={{marginTop:'10px'}}></i>
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapse6" className="panel-collapse collapse" role="tabpane6" aria-labelledby="heading6">
							        <div className="panel-body">
							        	<div className="row">
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.accessDoor"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.insulation"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.motorPosition"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.outlet"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.power"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.startStyle"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.supplier"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.type"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.beltGuard"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.doorOnBothSide"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.fixRepairLamp"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.standbyMotor"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.para13"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.seismicPringIsolator"/>
							        		</div>
							        		<div className="col-lg-6">
							        			<Field id="meta.section.fan.ptc"/>
							        		</div>
							        	</div>
							        </div>
							    </div>
							</div>
				      	</div>
			      	</form>
			      	
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
			        <button type="button" className="btn btn-primary" onClick={this.saveAhuInitValues}>{intl.get(SAVE)}</button>
			      </div>
			    </div>
			  </div>
			</div>
    );
  }
}

export default reduxForm({
  form: 'AhuInitForm', // a unique identifier for this form
  enableReinitialize: true,
})(AhuInit)
