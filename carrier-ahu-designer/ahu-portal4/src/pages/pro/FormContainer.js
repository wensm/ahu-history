import { connect } from 'react-redux'
import Form from './Form'
import {
  setCurrentProjectId,
} from '../../actions/pro'

const mapStateToProps = state => {
  return {
    // todos: getVisibleTodos(state.todos, state.visibilityFilter)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetCurrentProjectId: projectId => {
      dispatch(setCurrentProjectId(projectId))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
