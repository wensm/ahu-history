import { connect } from 'react-redux'
import Table from './Table'
import {
  fetchProjects,
  setCurrentProjectId,
  deleteProject,
} from '../../actions/pro'

function getProjects(projects, pro) {
  if (pro.values && pro.values.filterText) {
    return projects.filter(project => project.name.toLowerCase().includes(pro.values.filterText))
  } else {
    return projects
  }
}

const mapStateToProps = state => {
  return {
    projects: getProjects(state.pro.projects, state.form.pro),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDelete: projectId => {
      dispatch(deleteProject(projectId))
    },
    onSetCurrentProjectId: projectId => {
      dispatch(setCurrentProjectId(projectId))
    },
    onFetchProjects: () => {
      // dispatch(fetchProjects())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table)
