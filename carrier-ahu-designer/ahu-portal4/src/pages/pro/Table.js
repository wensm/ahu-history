import {
    PROJECT_NO,
    PROJECT_NAME,
    CONTRACT_NUMBER,
    CUSTOMER_NAME,
    SALES,
    OPERATION,
    ENTER_PROJECT,
    MODIFY,
    DELETE,
    BATCH_CONFIG,
    BATCH_EXPORT,
    GENERATE_REPORT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'

export default class Table extends React.Component {

  componentDidMount() {
    this.props.onFetchProjects()
  }

  render() {
    const {
      projects,
      onSetCurrentProjectId,
      onDelete,
    } = this.props
    return (
      <div data-name="Table">
        <table className="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>{intl.get(PROJECT_NO)}</th>
              <th>{intl.get(PROJECT_NAME)}</th>
              <th>{intl.get(CONTRACT_NUMBER)}</th>
              <th>{intl.get(CUSTOMER_NAME)}</th>
              <th>{intl.get(SALES)}</th>
              <th style={{width:'120px'}}>{intl.get(OPERATION)}</th>
              <th style={{width:'80px'}}>{intl.get(ENTER_PROJECT)}</th>
            </tr>
          </thead>
          <tbody>
            {projects.map(project => <Project key={project.pid} project={project} onSetCurrentProjectId={onSetCurrentProjectId} onDelete={onDelete} />)}
          </tbody>
        </table>
      </div>
    )
  }
}

class Project extends React.Component {
  render() {
    const {
      project,
      onSetCurrentProjectId,
      onDelete,
    } = this.props
    return (
      <tr>
        <td>{project.no}</td>
        <td>{project.name}</td>
        <td>{project.contract}</td>
        <td>{project.customerName}</td>
        <td>{project.saler}</td>
        <td>
          <div className="dropdown">
            <a id="dLabel" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" onClick={() => onSetCurrentProjectId(project.pid)}>{intl.get(OPERATION)}<span className="caret"></span></a>
            <ul className="dropdown-menu" aria-labelledby="dLabel" style={{minWidth:'0px'}}>
              <li>
                <a role="button" data-toggle="modal" data-target="#proFormModal">{intl.get(MODIFY)}</a>
              </li>
              <li>
                <a role="button" onClick={() => onDelete(project.pid)}>{intl.get(DELETE)}</a>
              </li>
              <li role="separator" className="divider"></li>
              <li>
                <a role="button">{intl.get(BATCH_CONFIG)}</a>
              </li>
              <li>
                <a role="button">{intl.get(BATCH_EXPORT)}</a>
              </li>
              <li role="separator" className="divider"></li>
              <li>
                <a role="button">{intl.get(GENERATE_REPORT)}</a>
              </li>
            </ul>
          </div>
        </td>
        <td>
          <button type="button" className="btn btn-primary btn-xs">{intl.get(ENTER_PROJECT)}</button>
        </td>
      </tr>
    )
  }
}
