import {
    PLEASE_ENTER_PROJECT_NAME,
    IMPORT_PROJECT,
    NEW_PROJECT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { Field, reduxForm } from 'redux-form'

class Form extends React.Component {
  render() {
    const {
      onSetCurrentProjectId
    } = this.props
    return (
      <div data-name="Form" className="row" style={{margin:'20px 0px'}}>
        <div className="col-lg-3">
          <Field name="filterText" className="form-control" component="input" type="text" placeholder={intl.get(PLEASE_ENTER_PROJECT_NAME)} />
        </div>
        <div className="col-lg-6"></div>
        <div className="col-lg-3">
          <button type="button" className="btn btn-primary" style={{float:'right'}}>{intl.get(IMPORT_PROJECT)}</button>
          <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#proFormModal" style={{float:'right',marginRight:'20px'}} onClick={() => onSetCurrentProjectId(null)}>{intl.get(NEW_PROJECT)}</button>
        </div>
      </div>
    )
  }
}

export default reduxForm({
  form: 'pro',
  enableReinitialize: true,
})(Form)
