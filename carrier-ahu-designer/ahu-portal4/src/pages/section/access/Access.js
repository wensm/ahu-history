import {
    OPTION,
    OPERATION,
    CONFIRM,
    INVALID,
    PERFORMANCE
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Icon, Tooltip, Button } from 'antd';


class Access extends React.Component {
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            // onCalcWetBulbTemperature,
            onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        return (
            <form data-name="Access" >
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <Group title={intl.get(OPTION)} id="group2">
                        <div className="col-lg-4">
                            <Field id="meta.section.access.Function" />
                        </div>

                        <div className="col-lg-4">
                            <Field id="meta.section.access.ODoor" />
                        </div>
                        <div className="col-lg-4">
                            <Field id="meta.section.access.uvModel" />
                        </div>
                        <div className="col-lg-4">
                            <Field id="meta.section.access.FixRepairLamp" />
                        </div>
                        <div className="col-lg-2">
                            <Field id="meta.section.access.DoorOnBothSide" />
                        </div>
                        <div className="col-lg-2">
                            <Field id="meta.section.access.PositivePressureDoor" />
                        </div>
                        <div className="col-lg-4">
                            <Field id="meta.section.access.uvLamp" onChange={(e) => { this.props.onchangeUvLamp(e.target.value) }} />
                        </div>
                    </Group>
                    <Group title={intl.get(PERFORMANCE)} id="group2">
                        <div className="col-lg-3">
                            <Field id="meta.section.access.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.access.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.access.Weight" />
                        </div>
                    </Group>

                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                // type="button" className="btn btn-primary" 
                                disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => { onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout) }}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'Access', // a unique identifier for this form
    enableReinitialize: true,
})(Access)
