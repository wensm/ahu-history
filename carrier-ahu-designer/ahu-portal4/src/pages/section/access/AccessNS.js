import {
  NON_STANDARD_OPTION,
  OPERATION,
  CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Button } from 'antd';

class AccessNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="AccessNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.access.enable" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.access.Price" />
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.access.MEMO" />
            </div>
              <div className="col-lg-3">
                  <Field id="meta.section.access.nsDrainpanType" />
              </div>
              <div className="col-lg-3">
                  <Field id="meta.section.access.nsDrainpanPrice" />
              </div>
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
            <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
              <Button
                // type="button" className="btn btn-primary"
                disabled={!this.props.canConfirm || this.props.invalid}
                // style={{marginLeft: '10px'}}
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                onClick={() => { onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true) }}>
                {intl.get(CONFIRM)}
              </Button>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'AccessNS', // a unique identifier for this form
  enableReinitialize: true,
})(AccessNS)
