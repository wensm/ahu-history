import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { confirmSection, changeUvLamp } from '../../../actions/ahu'
import Access from './Access'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onchangeUvLamp:(value)=>{
      dispatch(changeUvLamp(value))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Access)
