import { connect } from 'react-redux'
import CoolingCoilNS from './CoolingCoilNS'
import { calcComponentValue, confirmSection, fetchRelativeHumidity, fetchWetBulbTemperature } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoolingCoilNS)
