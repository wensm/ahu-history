import {
    CONDENSER_INLET_AIR_TEMPERATURE,
    SATURATED_CONDENSATION_TEMPERATURE,
    SATURATED_SUCTION_TEMPERATURE,
    WATER_RESISTANCE,
    WATER_FLOW_VOLUME,
    INLET_WATER_TEMPERATURE,
    WATER_TEMPERATURE_RISE,
    MEDIUM_FLOW_RATE,
    COIL_SECTION_CALCULATION_RESULT,
    COIL_INFORMATION,
    INLET_WIND,
    OUTLET_AIR,
    AIR_SIDE,
    WATER_SIDE,
    PERFORMANCE,
    ROW_NUMBER,
    FIN_DISTANCE,
    CIRCUIT,
    WINDWARD_AREA,
    FACE_WIND_SPEED,
    DB_TEMPERATURE,
    WET_BULB_TEMPERATURE,
    RELATIVE_HUMIDITY,
    AIR_RESISTANCE,
    REQUEST_DATA_IS_ZERO,
    CLOSE,
    OK,
    COOLING_VOLUME,
    SHOW_COLD,
    HEATING_VOLUME,
    PRICE,
    COIL_NOT_CERTIFIED_AHRI
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { updateCoolingCoilByIndex } from '../../../actions/ahu'
import style from '../../ahu/AHUPage.css'
import loadingImg from '../../../images/loding.gif'
import isEmpty from 'lodash/isEmpty'
import { Tooltip, Icon } from 'antd'

class CoolingCoilModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentSelect: 0
        }
        this.updateSelectedIndex = this.updateSelectedIndex.bind(this)
    }

    updateSelectedIndex(index) {
        this.setState({ currentSelect: index })
    }

    render() {
        const { coolingCoilCalcs, onSelectCoolingCoilCalc, props, unitPreferCode, unitSetting, metaUnit, metaLayout } = this.props
        let lseason = 'S',
            arrlse = []
        if (coolingCoilCalcs) {
            lseason = coolingCoilCalcs.season
        }
        if (props == "ahu.directExpensionCoil") {
            arrlse =
                {
                    1: [CONDENSER_INLET_AIR_TEMPERATURE, SATURATED_CONDENSATION_TEMPERATURE, SATURATED_SUCTION_TEMPERATURE, 'title.moon_intl_str_0698'],
                    2: ['title.moon_intl_str_0664', 'title.moon_intl_str_0664', 'title.moon_intl_str_0664', ' ']
                }
        } else {
            arrlse = {
                1: [WATER_RESISTANCE, WATER_FLOW_VOLUME, INLET_WATER_TEMPERATURE, WATER_TEMPERATURE_RISE, MEDIUM_FLOW_RATE],
                2: ['title.moon_intl_str_0667', 'title.moon_intl_str_0668', 'title.moon_intl_str_0664', 'title.moon_intl_str_0664', 'title.moon_intl_str_0663']
            }
        }
        let modalErrorStyle = {
            minHeight: '200px',
            lineHeight: '200px',
            textAlign: 'center',
            color: 'red'
        }

        return (
            <div data-name="CoolingCoilModal">
                <div className="modal fade" id="coolingCoilModal" tabIndex="-1" role="dialog"
                     aria-labelledby="coolingCoilModalLabel">
                    <div className="modal-dialog modal-lg" role="document" style={{width:'1200px'}}>
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title"
                                    id="coolingCoilModalLabel">{intl.get(COIL_SECTION_CALCULATION_RESULT)}</h4>
                            </div>
                            {coolingCoilCalcs == 'clean' || (typeof coolingCoilCalcs != 'string' && !isEmpty(coolingCoilCalcs) && coolingCoilCalcs.resultData.length > 0) ?
                                <div className="modal-body">
                                    <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                        {coolingCoilCalcs == 'clean' && <img src={loadingImg} style={{ position: 'absolute', width: '40px', top: '90%', left: '45%' }} />}
                                        <table className="table table-bordered table-condensed" style={{ width: '1200px' }}>
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th colSpan="5"
                                                    style={{ textAlign: 'center' }}>{intl.get(COIL_INFORMATION)}</th>
                                                <th colSpan="3"
                                                    style={{ textAlign: 'center' }}>{intl.get(INLET_WIND)}</th>
                                                <th colSpan="3"
                                                    style={{ textAlign: 'center' }}>{intl.get(OUTLET_AIR)}</th>
                                                <th colSpan="1"
                                                    style={{ textAlign: 'center' }}>{intl.get(AIR_SIDE)}</th>
                                                {props == "ahu.directExpensionCoil" ?
                                                    <th colSpan="4"
                                                        style={{ textAlign: 'center' }}>{intl.get(WATER_SIDE)}</th> :
                                                    <th colSpan="5"
                                                        style={{ textAlign: 'center' }}>{intl.get(WATER_SIDE)}</th>}

                                                {lseason == "S" && props != "ahu.directExpensionCoil"?
                                                    <th colSpan="3"
                                                        style={{ textAlign: 'center' }}>{intl.get(PERFORMANCE)}</th>:
                                                    <th
                                                        style={{ textAlign: 'center' }}>{intl.get(PERFORMANCE)}</th>
                                                }
                                                <th
                                                    style={{ textAlign: 'center' }}>{intl.get(PRICE)}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td></td>
                                                <td>{intl.get(ROW_NUMBER)}</td>
                                                <td>{intl.get(FIN_DISTANCE)}</td>
                                                <td>{intl.get(CIRCUIT)}</td>
                                                <td>{intl.get(WINDWARD_AREA)}</td>
                                                <td>{intl.get(FACE_WIND_SPEED)}</td>
                                                <td>{intl.get(DB_TEMPERATURE)}</td>
                                                <td>{intl.get(WET_BULB_TEMPERATURE)}</td>
                                                <td>{intl.get(RELATIVE_HUMIDITY)}</td>
                                                <td>{intl.get(DB_TEMPERATURE)}</td>
                                                <td>{intl.get(WET_BULB_TEMPERATURE)}</td>
                                                <td>{intl.get(RELATIVE_HUMIDITY)}</td>
                                                <td>{intl.get(AIR_RESISTANCE)}</td>
                                                {arrlse[1].map((v, key) => {
                                                    return <td key={key}>{intl.get(v)}</td>
                                                })}
                                                {
                                                    props == "ahu.directExpensionCoil" &&
                                                    <td>{intl.get(COOLING_VOLUME)}</td>
                                                }
                                                {
                                                    props == "ahu.directExpensionCoil" &&
                                                    <td>{intl.get(SHOW_COLD)}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get(COOLING_VOLUME)}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get(SHOW_COLD)}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_2046')}</td>
                                                }
                                                {
                                                    lseason == "W" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get(HEATING_VOLUME)}</td>
                                                }
                                                <td>LP</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>{intl.get('title.moon_intl_str_0661')}</td>
                                                <td></td>
                                                <td>{intl.get('title.moon_intl_str_0662')}</td>
                                                <td>{intl.get('title.moon_intl_str_0663')}</td>
                                                <td>{intl.get('title.moon_intl_str_0664')}</td>
                                                <td>{intl.get('title.moon_intl_str_0664')}</td>
                                                <td>{intl.get('title.moon_intl_str_0665')}</td>
                                                <td>{intl.get('title.moon_intl_str_0664')}</td>
                                                <td>{intl.get('title.moon_intl_str_0664')}</td>
                                                <td>{intl.get('title.moon_intl_str_0665')}</td>
                                                <td>{intl.get('title.moon_intl_str_0666')}</td>
                                                {arrlse[2].map((v, key) => {
                                                    return <td key={key}>{intl.get(v)}</td>
                                                })}
                                                {
                                                    props == "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                }
                                                {
                                                    props == "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                }
                                                {
                                                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                                                    <td> </td>
                                                }
                                                {
                                                    lseason == "W" && props != "ahu.directExpensionCoil" &&
                                                    <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                }
                                                <td>RMB</td>
                                            </tr>
                                            {!(coolingCoilCalcs == 'clean') && coolingCoilCalcs.resultData && coolingCoilCalcs.resultData.map((coolingCoilCalc, index) =>
                                                <CoolingCoilCalc key={index} index={index} coolingCoilCalc={coolingCoilCalc}
                                                                 lseason={lseason} currentSelect={this.state.currentSelect}
                                                                 updateSelectedIndex={this.updateSelectedIndex} props={props} />)}
                                            </tbody>
                                        </table>
                                        {!(coolingCoilCalcs == 'clean') && coolingCoilCalcs.resultData.length == 0 &&
                                        <p style={{ textAlign: 'center' }}>{intl.get(REQUEST_DATA_IS_ZERO)}</p>
                                        }
                                    </div>
                                </div>
                                : <div className="modal-error" style={modalErrorStyle}>
                                    {coolingCoilCalcs}
                                </div>}
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default"
                                        data-dismiss="modal">{intl.get(CLOSE)}</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal"
                                        onClick={() => onSelectCoolingCoilCalc(unitPreferCode, unitSetting, metaUnit, metaLayout)}>{intl.get(OK)}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class CoolingCoilCalc extends React.Component {
    render() {
        const { coolingCoilCalc, index, lseason, currentSelect, updateSelectedIndex, props } = this.props
        return (
            <tr onClick={() => updateSelectedIndex(index)} className={index == currentSelect && style.SelectedTR}>
                <td style={{ cursor: 'pointer' }}>
                    <input type="radio" name="coolingCoilCalcRadio" checked={index == currentSelect} data-index={index} />
                </td>
                <td style={{ cursor: 'pointer' }}>
                    {coolingCoilCalc['rows']}
                    {
                        coolingCoilCalc.ahriMessage ? <Tooltip placement="topLeft" title={intl.get(`label.${coolingCoilCalc.ahriMessage}`)}>
                            <Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} />
                        </Tooltip> : ''
                    }
                </td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['finDensity']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['circuit']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['area']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['velocity']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['InDryBulbT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['InWetBulbT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['InRelativeT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['OutDryBulbT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['OutWetBulbT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['OutRelativeT']}</td>
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['AirResistance']}</td>
                {props == "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnCond']}</td>}
                {props == "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnSatCond']}</td>}
                {props == "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnSST']}</td>}
                {props == "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['Unit']}</td>}
                {props != "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['waterResistance']}</td>}
                {props != "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnWaterFlow']}</td>}
                {props != "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnEnteringFluidTemperature']}</td>}
                {props != "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['returnWTAScend']}</td>}
                {props != "ahu.directExpensionCoil" && <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['fluidvelocity']}</td>}
                {
                    props == "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnColdQ']}</td>
                }

                {
                    props == "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnSensibleCapacity']}</td>
                }
                {
                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnColdQ']}</td>
                }

                {
                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnSensibleCapacity']}</td>
                }
                {
                    lseason == "S" && props != "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnBypass']}</td>
                }

                {
                    lseason == "W" && props != "ahu.directExpensionCoil" &&
                    <td>{coolingCoilCalc['returnHeatQ']}</td>
                }
                <td style={{ cursor: 'pointer' }}>{coolingCoilCalc['price']}</td>
            </tr>
        )
    }
}

const mapStateToProps = state => {
    return {
        coolingCoilCalcs: state.ahu.coolingCoilCalcs,
        props: state.ahu.selectedComponent.name,
        unitPreferCode: state.general.user.unitPreferCode,
        unitSetting: state.metadata.metaUnitSetting,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onSelectCoolingCoilCalc: (unitPreferCode, unitSetting, metaUnit, metaLayout) => {
            const index = $('#coolingCoilModal').find('input[type="radio"]:checked').attr('data-index')
            if (typeof index !== typeof undefined) {
                dispatch(updateCoolingCoilByIndex(index, unitPreferCode, unitSetting, metaUnit, metaLayout))
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoolingCoilModal)
