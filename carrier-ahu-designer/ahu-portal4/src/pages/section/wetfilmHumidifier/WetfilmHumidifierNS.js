import {
  NON_STANDARD_OPTION,
  OPERATION,
  CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class WetfilmHumidifierNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="WetfilmHumidifierNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.wetfilmHumidifier.enable" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.wetfilmHumidifier.Price" />
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.wetfilmHumidifier.MEMO" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.wetfilmHumidifier.nsChangeSupplier" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.wetfilmHumidifier.nsSupplier" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.wetfilmHumidifier.nsSupplier1" />
            </div>
            <div className="col-lg-2">
              <Field id="meta.section.wetfilmHumidifier.thickness" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.wetfilmHumidifier.nsSupplierPrice" />
            </div>
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
            <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
              <Button
                // type="button" className="btn btn-primary"
                disabled={!this.props.canConfirm || this.props.invalid}
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}>
                {intl.get(CONFIRM)}
              </Button>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'WetfilmHumidifierNS', // a unique identifier for this form
  enableReinitialize: true,
})(WetfilmHumidifierNS)
