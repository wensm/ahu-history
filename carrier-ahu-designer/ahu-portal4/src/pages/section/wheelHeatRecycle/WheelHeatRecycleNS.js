import {
  NON_STANDARD_OPTION,
  CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Tooltip, Icon, Button } from 'antd'

class WheelHeatRecycleNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="WheelHeatRecycleNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="meta.section.wheelHeatRecycle.NSEnable" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.wheelHeatRecycle.Price" />
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.wheelHeatRecycle.MEMO" />
            </div>
            <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
              <Button disabled={!this.props.canConfirm || this.props.invalid}
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                onClick={() => {
                  onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)
                  this.onForm(this.props)
                }
                }>
                {intl.get(CONFIRM)}
              </Button>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'WheelHeatRecycleNS', // a unique identifier for this form
  enableReinitialize: true,
})(WheelHeatRecycleNS)
