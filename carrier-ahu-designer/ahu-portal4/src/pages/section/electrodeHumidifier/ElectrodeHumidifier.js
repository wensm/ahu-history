import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    HUMIDIFIER_OPTION,
    PARAMETER,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength5, minValue0 } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class ElectrodeHumidifier extends React.Component {
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalIsothermalJSBHumidificationVolume,
            componentValue,
            onCalIsothermalcJSBRelativeHumidity, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_electrodeHumidifier_EnableWinter) : false

        return (
            <form data-name="ElectrodeHumidifier">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                            role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a></li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="summer">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.electrodeHumidifier.EnableSummer" />
                            </div>
                            <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SInDryBulbT" 
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_electrodeHumidifier_SInWetBulbT', newValue, 'meta_section_electrodeHumidifier_SInRelativeT', 'meta_section_electrodeHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                            />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_electrodeHumidifier_SInDryBulbT', newValue, 'meta_section_electrodeHumidifier_SInRelativeT', 'meta_section_electrodeHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_electrodeHumidifier_SInDryBulbT', newValue, 'meta_section_electrodeHumidifier_SInWetBulbT', 'meta_section_electrodeHumidifier_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalcJSBRelativeHumidity(
                                                'meta_section_electrodeHumidifier_SInDryBulbT',
                                                'meta_section_electrodeHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_electrodeHumidifier_SOutDryBulbT',
                                                    'meta_section_electrodeHumidifier_SOutWetBulbT',
                                                    'meta_section_electrodeHumidifier_SOutRelativeT',
                                                    'meta_section_electrodeHumidifier_SJSBRelativeT',
                                                    'meta_section_electrodeHumidifier_SHumidificationQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalJSBHumidificationVolume(
                                                'meta_section_electrodeHumidifier_SInDryBulbT',
                                                'meta_section_electrodeHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_electrodeHumidifier_SOutDryBulbT',
                                                    'meta_section_electrodeHumidifier_SOutWetBulbT',
                                                    'meta_section_electrodeHumidifier_SOutRelativeT',
                                                    'meta_section_electrodeHumidifier_SHumidificationQ',
                                                    'meta_section_electrodeHumidifier_SJSBRelativeT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.SOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group>
                        </div>
                        <div role="tabpanel" className="tab-pane " id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.electrodeHumidifier.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WInDryBulbT" 
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_electrodeHumidifier_WInWetBulbT', newValue, 'meta_section_electrodeHumidifier_WInRelativeT', 'meta_section_electrodeHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                            />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_electrodeHumidifier_WInDryBulbT', newValue, 'meta_section_electrodeHumidifier_WInRelativeT', 'meta_section_electrodeHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_electrodeHumidifier_WInDryBulbT', newValue, 'meta_section_electrodeHumidifier_WInWetBulbT', 'meta_section_electrodeHumidifier_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalcJSBRelativeHumidity(
                                                'meta_section_electrodeHumidifier_WInDryBulbT',
                                                'meta_section_electrodeHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_electrodeHumidifier_WOutDryBulbT',
                                                    'meta_section_electrodeHumidifier_WOutWetBulbT',
                                                    'meta_section_electrodeHumidifier_WOutRelativeT',
                                                    'meta_section_electrodeHumidifier_WJSBRelativeT',
                                                    'meta_section_electrodeHumidifier_WHumidificationQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalJSBHumidificationVolume(
                                                'meta_section_electrodeHumidifier_WInDryBulbT',
                                                'meta_section_electrodeHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_electrodeHumidifier_WOutDryBulbT',
                                                    'meta_section_electrodeHumidifier_WOutWetBulbT',
                                                    'meta_section_electrodeHumidifier_WOutRelativeT',
                                                    'meta_section_electrodeHumidifier_WHumidificationQ',
                                                    'meta_section_electrodeHumidifier_WJSBRelativeT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electrodeHumidifier.WOutRelativeT"
                                        />
                                    </div>
                                </div>

                            </Group>:''}
                        </div>

                    </div>


                    <Group title={intl.get(HUMIDIFIER_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.electrodeHumidifier.Supplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrodeHumidifier.Material" />
                        </div>
                    </Group>
                    <Group title={intl.get(PARAMETER)} id="group8">
                        <div className="col-lg-3">
                            <Field id="meta.section.electrodeHumidifier.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrodeHumidifier.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrodeHumidifier.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'ElectrodeHumidifier', // a unique identifier for this form
    enableReinitialize: true,
})(ElectrodeHumidifier)
