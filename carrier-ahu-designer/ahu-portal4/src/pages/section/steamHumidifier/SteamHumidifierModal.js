import {
    CALCULATION_RESULT,
    CLOSE,
    OK,
    TYPE,
    STEAM_PRESSURE,
    HUMIDIFICATION_VOLUME,
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { updateSteamHumidifierByIndex } from '../../../actions/ahu'
import style from '../../ahu/AHUPage.css'
class SteamHumidifierModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentSelect: 0
        }
        this.updateSelectedIndex = this.updateSelectedIndex.bind(this)
    }

    updateSelectedIndex(index, resultData) {
        this.setState({currentSelect: index})
    }
    render() {
        const { steamHumidifierCalcs, onSelectCoolingCoilCalc, onCalIsothermalcJSBRelativeHumidity, unitSetting, metaUnit, metaLayout, propUnit} = this.props

        return (
            <div data-name="SteamHumidifierModal">
                <div className="modal fade" id="SteamHumidifierModal" tabIndex="-1" role="dialog" aria-labelledby="SteamHumidifierModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="SteamHumidifierModalLabel">{intl.get(CALCULATION_RESULT)}</h4>
                            </div>
                            <div className="modal-body">
                                <div style={{maxHeight:window.innerHeight-220,overflow:'auto'}}>
                                    <table className="table table-bordered table-condensed" style={{width:'500px'}}>
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th style={{textAlign:'center'}}>{intl.get(TYPE)}</th>
                                            <th style={{textAlign:'center'}}>{intl.get(STEAM_PRESSURE)}</th>
                                            <th style={{textAlign:'center'}}>{intl.get(HUMIDIFICATION_VOLUME)}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {steamHumidifierCalcs.resultData && steamHumidifierCalcs.resultData.map((steamHumidifierCalc, index) => <CoolingCoilCalc  key={index} index={index} steamHumidifierCalc={steamHumidifierCalc} currentSelect={this.state.currentSelect} updateSelectedIndex={this.updateSelectedIndex}/>)}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => onSelectCoolingCoilCalc(this.state.currentSelect, steamHumidifierCalcs.resultData, ()=>{
                                    onCalIsothermalcJSBRelativeHumidity(
                                        'meta_section_steamHumidifier_SInDryBulbT',
                                        'meta_section_steamHumidifier_SInWetBulbT',
                                        undefined,
                                        'meta_ahu_eairvolume',
                                        ['meta_section_steamHumidifier_SOutDryBulbT',
                                            'meta_section_steamHumidifier_SOutWetBulbT',
                                            'meta_section_steamHumidifier_SOutRelativeT',
                                            'meta_section_steamHumidifier_SJSBRelativeT',
                                            'meta_section_steamHumidifier_SHumidificationQ'
                                        ],
                                        ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit, 'meta_section_steamHumidifier_humidificationQ')

                                        onCalIsothermalcJSBRelativeHumidity(
                                            'meta_section_steamHumidifier_WInDryBulbT',
                                            'meta_section_steamHumidifier_WInWetBulbT',
                                            undefined,
                                            'meta_ahu_eairvolume',
                                            ['meta_section_steamHumidifier_WOutDryBulbT',
                                                'meta_section_steamHumidifier_WOutWetBulbT',
                                                'meta_section_steamHumidifier_WOutRelativeT',
                                                'meta_section_steamHumidifier_WJSBRelativeT',
                                                'meta_section_steamHumidifier_WHumidificationQ'
                                            ],
                                            ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit, 'meta_section_steamHumidifier_humidificationQ')





                                })}>{intl.get(OK)}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
class CoolingCoilCalc extends React.Component {

    render() {
        const { steamHumidifierCalc, index, currentSelect, updateSelectedIndex} = this.props
        return (
            <tr onClick={() => updateSelectedIndex(index)} className={index == currentSelect && style.SelectedTR}>
                <td style={{textAlign:'center',cursor:'pointer'}}>
                    <input type="radio" name="coolingCoilCalcRadio" checked={index == currentSelect} />
                </td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{steamHumidifierCalc['HTypes']}</td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{steamHumidifierCalc['VaporPressure']}</td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{steamHumidifierCalc['humidificationQ']}</td>
            </tr>
        )
    }
}
const mapStateToProps = state => {
    return {
        steamHumidifierCalcs: state.ahu.steamHumidifierCalcs
    }
}
const mapDispatchToProps = dispatch => {

    return {
        onSelectCoolingCoilCalc: (currentSelect, resultData, callBack) => {
            // const index = $('#SteamHumidifierModal').find('input[type="radio"]:checked').attr('data-index')
            
            dispatch(updateSteamHumidifierByIndex(resultData[currentSelect]))//edit by zzf change index to currentSelect 干蒸加湿段
            callBack && callBack()
            // if (typeof index !== typeof undefined) {
            // }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SteamHumidifierModal)
