import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    OTHER_WINTER_PAREN,
    HUMIDIFIER_OPTION,
    ATTACHMENT,
    PARAMETER,
    OPERATION,
    CHOOSE_MODEL,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';
// import Form from '../../ahu/Form'

class SteamHumidifier extends React.Component {
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalIsothermalJSBHumidificationVolume,
            onCalIsothermalcJSBRelativeHumidity,
            componentValue,
            onSteamHumidifierChooseModel, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        // console.log('FormContainer', Form)
        let enableWinter = componentValue ? eval(componentValue.meta_section_steamHumidifier_EnableWinter) : false


        return (
            <form data-name="SteamHumidifier">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                            role="tab"
                            data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation" ><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="summer">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.steamHumidifier.EnableSummer" />
                            </div>

                            <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.SInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_steamHumidifier_SInWetBulbT', newValue, 'meta_section_steamHumidifier_SInRelativeT', 'meta_section_steamHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.SOutDryBulbT"
                                        />
                                    </div>
                                </div>

                                <div className="col-lg-4">

                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.SInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_steamHumidifier_SInDryBulbT', newValue, 'meta_section_steamHumidifier_SInRelativeT', 'meta_section_steamHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.SOutWetBulbT"

                                        />
                                    </div>
                                </div>

                                <div className="col-lg-4">

                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.SInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_steamHumidifier_SInDryBulbT', newValue, 'meta_section_steamHumidifier_SInWetBulbT', 'meta_section_steamHumidifier_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.SOutRelativeT"

                                        />
                                    </div>

                                </div>
                                {/* <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.SHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalcJSBRelativeHumidity(
                                                'meta_section_steamHumidifier_SInDryBulbT',
                                                'meta_section_steamHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_steamHumidifier_SOutDryBulbT',
                                                    'meta_section_steamHumidifier_SOutWetBulbT',
                                                    'meta_section_steamHumidifier_SOutRelativeT',
                                                    'meta_section_steamHumidifier_SJSBRelativeT',
                                                    'meta_section_steamHumidifier_SHumidificationQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.SJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalJSBHumidificationVolume(
                                                'meta_section_steamHumidifier_SInDryBulbT',
                                                'meta_section_steamHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_steamHumidifier_SOutDryBulbT',
                                                    'meta_section_steamHumidifier_SOutWetBulbT',
                                                    'meta_section_steamHumidifier_SOutRelativeT',
                                                    'meta_section_steamHumidifier_SHumidificationQ',
                                                    'meta_section_steamHumidifier_SJSBRelativeT'

                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div> */}
                            </Group>
                        </div>
                        <div role="tabpanel" className="tab-pane" id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.steamHumidifier.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(OTHER_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.WInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_steamHumidifier_WInWetBulbT', newValue, 'meta_section_steamHumidifier_WInRelativeT', 'meta_section_steamHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.WOutDryBulbT"
                                        />
                                    </div>
                                </div>

                                <div className="col-lg-4">

                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_steamHumidifier_WInDryBulbT', newValue, 'meta_section_steamHumidifier_WInRelativeT', 'meta_section_steamHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.WOutWetBulbT"

                                        />
                                    </div>
                                </div>

                                <div className="col-lg-4">

                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.WInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_steamHumidifier_WInDryBulbT', newValue, 'meta_section_steamHumidifier_WInWetBulbT', 'meta_section_steamHumidifier_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamHumidifier.WOutRelativeT" />
                                    </div>
                                </div>
                                {/* <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.WHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalcJSBRelativeHumidity(
                                                'meta_section_steamHumidifier_WInDryBulbT',
                                                'meta_section_steamHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_steamHumidifier_WOutDryBulbT',
                                                    'meta_section_steamHumidifier_WOutWetBulbT',
                                                    'meta_section_steamHumidifier_WOutRelativeT',
                                                    'meta_section_steamHumidifier_WJSBRelativeT',
                                                    'meta_section_steamHumidifier_WHumidificationQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamHumidifier.WJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsothermalJSBHumidificationVolume(
                                                'meta_section_steamHumidifier_WInDryBulbT',
                                                'meta_section_steamHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_steamHumidifier_WOutDryBulbT',
                                                    'meta_section_steamHumidifier_WOutWetBulbT',
                                                    'meta_section_steamHumidifier_WOutRelativeT',
                                                    'meta_section_steamHumidifier_WHumidificationQ',
                                                    'meta_section_steamHumidifier_WJSBRelativeT'

                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div> */}
                                <div className="col-lg-4">



                                </div>
                            </Group> : ''}
                        </div>
                    </div>
                    <Group title={intl.get(HUMIDIFIER_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.inputVaporPressure" />
                        </div>
                    </Group>
                    <Group title={intl.get(HUMIDIFIER_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.Supplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.ControlM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.HTypes" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.Material" />
                        </div>
                    </Group>
                    <Group title={intl.get(PARAMETER)} id="group8">

                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.VaporPressure" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.steamHumidifier.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-3">
                            <Button
                                data-toggle="modal"
                                data-target="#SteamHumidifierModal"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onSteamHumidifierChooseModel(this.props.componentValues, this.props.selectedComponent)}>
                                {intl.get(CHOOSE_MODEL)}
                            </Button>
                        </div>

                        <div className="col-lg-3">
                            <Button disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'SteamHumidifier', // a unique identifier for this form
    enableReinitialize: true,
})(SteamHumidifier)
