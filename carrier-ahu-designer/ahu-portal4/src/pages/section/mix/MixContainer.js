import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import Mix from './Mix'
import { confirmSection, fetchOutletParams } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCalcSummerOutletParams: () => {
      const airFlow = ownProps.componentValue.meta_section_mix_NAVolume
      const freshAirRate = ownProps.componentValue.meta_section_mix_NARatio
      const freshAirTIN = ownProps.componentValue.meta_section_mix_SInDryBulbT
      const freshAirTbIN = ownProps.componentValue.meta_section_mix_SInWetBulbT
      const returnAirTIN = ownProps.componentValue.meta_section_mix_SNewDryBulbT
      const returnTbIN = ownProps.componentValue.meta_section_mix_SNewWetBulbT
      dispatch(fetchOutletParams('summer', airFlow, freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN))
    },
    onCalcWinterOutletParams: () => {
      const airFlow = ownProps.componentValue.meta_section_mix_NAVolume
      const freshAirRate = ownProps.componentValue.meta_section_mix_NARatio
      const freshAirTIN = ownProps.componentValue.meta_section_mix_WInDryBulbT
      const freshAirTbIN = ownProps.componentValue.meta_section_mix_WInWetBulbT
      const returnAirTIN = ownProps.componentValue.meta_section_mix_WNewDryBulbT
      const returnTbIN = ownProps.componentValue.meta_section_mix_WNewWetBulbT
      dispatch(fetchOutletParams('winter', airFlow, freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mix)
