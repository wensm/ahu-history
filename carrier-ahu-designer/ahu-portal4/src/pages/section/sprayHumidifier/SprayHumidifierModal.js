import {
    CALCULATION_RESULT,
    CLOSE,
    OK,
    NOZZLE_NUMBER,
    BORE_DIAMETER,
    MAX_SPRAY_VOLUME_KG_H,
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { updateCoolingCoilByIndex } from '../../../actions/ahu'
import style from '../../ahu/AHUPage.css'
import loadingImg from '../../../images/loding.gif'

class SprayHumidifierModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentSelect: 0
        }
        this.updateSelectedIndex = this.updateSelectedIndex.bind(this)
    }

    updateSelectedIndex(index) {
        this.setState({currentSelect: index})
    }
    render() {
        const { humidifierCalcs, onSelectCoolingCoilCalc, sectionPass, validationP, unitPreferCode, unitSetting, metaUnit, metaLayout} = this.props
        const {currentSelect} = this.state
        let sectionMsgc = validationP ? validationP.sectionMsg : ''
        return (
            <div data-name="SprayHumidifierModal">
                <div className="modal fade" id="SprayHumidifierModal" tabIndex="-1" role="dialog" aria-labelledby="SprayHumidifierModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="SprayHumidifierModalLabel">{intl.get(CALCULATION_RESULT)}</h4>
                            </div>
                            <div className="modal-body">
                                <div style={{maxHeight:window.innerHeight-220,overflow:'auto', textAlign:'center'}}>
                                {(sectionMsgc=='' && !sectionPass)&& <img src={loadingImg} style={{position:'absolute',width:'40px',top:'90%',left:'45%'}}/>}
                                    {sectionPass ? <table className="table table-bordered table-condensed" style={{width:'500px'}}>
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th style={{textAlign:'center'}}>{intl.get(NOZZLE_NUMBER)}</th>
                                            <th style={{textAlign:'center'}}>{intl.get(BORE_DIAMETER)}</th>
                                            <th style={{textAlign:'center'}}>{intl.get(MAX_SPRAY_VOLUME_KG_H)}</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        {humidifierCalcs.resultData && humidifierCalcs.resultData.map((humidifierCalc, index) => <CoolingCoilCalc key={index} index={index} humidifierCalc={humidifierCalc} currentSelect={this.state.currentSelect} updateSelectedIndex={this.updateSelectedIndex}/>)}
                                        </tbody>
                                    </table>
                                    :<span style={{color:'red'}}>
                                        {sectionMsgc}
                                    </span>}
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => onSelectCoolingCoilCalc( unitPreferCode, unitSetting, metaUnit, metaLayout, currentSelect)}>{intl.get(OK)}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
class CoolingCoilCalc extends React.Component {
    render() {
        const { humidifierCalc, index, currentSelect, updateSelectedIndex} = this.props

        return (
            <tr onClick={() => updateSelectedIndex(index)} className={index == currentSelect && style.SelectedTR}>
                <td style={{textAlign:'center',cursor:'pointer'}}>
                    <input type="radio" name="coolingCoilCalcRadio" checked={index == currentSelect}/>
                </td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{humidifierCalc['nozzleN']}</td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{humidifierCalc['aperture']}</td>
                <td style={{textAlign:'center',cursor:'pointer'}}>{humidifierCalc['hq']}</td>
            </tr>
        )
    }
}

const mapStateToProps = state => {
    return {
        humidifierCalcs: state.ahu.humidifierCalcs,
        sectionPass:state.ahu.sectionPass,
        validationP:state.ahu.validationP,
        unitPreferCode: state.general.user.unitPreferCode,        
        unitSetting: state.metadata.metaUnitSetting,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onSelectCoolingCoilCalc: ( unitPreferCode, unitSetting, metaUnit, metaLayout, currentSelect) => {
            const index = $('#SprayHumidifierModal').find('input[type="radio"]:checked').attr('data-index')

            // if (typeof index !== typeof undefined) {// zzf 高压喷雾段
                dispatch(updateCoolingCoilByIndex(currentSelect, unitPreferCode, unitSetting, metaUnit, metaLayout, 'SprayHumidifier'))
            // }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SprayHumidifierModal)
