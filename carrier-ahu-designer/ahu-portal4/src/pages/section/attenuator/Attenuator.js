import {
    OPTION,
    WEIGHT_AND_RESISTANCE,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Field as FDAtt, reduxForm } from 'redux-form'
import { confirmSection } from '../../../actions/ahu'
import { required } from '../../ahu/Validate'
import AttenuatorLost from './AttenuatorLost'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class Attenuator extends React.Component {
    getArr(type, deadening) {
        let arra = []
        if (deadening == 'A') {
            if (type == 'RS') {
                arra = ['5', '10', '13', '14', '16', '16', '12', '10']
            } else {
                arra = ['6.5', '9', '12', '22', '30', '29.5', '21.5', '12.5']
            }
        } else if (deadening == 'B') {
            if (type == 'RS') {
                arra = ['7', '13', '21', '23', '26', '25', '20', '15']
            } else {
                arra = ['7.5', '15', '20', '34', '40', '40.5', '34.5', '21.5']
            }
        }
        return arra
    }
    render() {
        const { isCompleted, onCompleteSection, componentValue, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        let arr = [];
        arr = this.getArr(componentValue.meta_section_attenuator_Type, componentValue.meta_section_attenuator_Deadening)
        return (
            <form data-name="Attenuator">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(OPTION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.attenuator.Type" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.attenuator.Deadening" />
                        </div>
                    </Group>
                    <Group title={intl.get('title.moon_intl_str_0333')} id="group1">
                        <div className="col-lg-12">{intl.get('title.moon_intl_str_0333')}
                            <AttenuatorLost arr={arr} deadening={componentValue.meta_section_attenuator_Deadening} />
                        </div>
                    </Group>
                    <Group title={intl.get(WEIGHT_AND_RESISTANCE)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.attenuator.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.attenuator.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.attenuator.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                //  type="button" className="btn btn-primary"
                                disabled={!this.props.canConfirm || this.props.invalid}
                                //    style={{ marginLeft: '10px' }}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}


export default reduxForm({
    form: 'Attenuator', // a unique identifier for this form
    enableReinitialize: true,
})(Attenuator)
