import React from 'react'
import {connect} from 'react-redux'
import AHUFormNS from './AHUFormNS'
import { reduxForm } from 'redux-form'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUFormNS)
