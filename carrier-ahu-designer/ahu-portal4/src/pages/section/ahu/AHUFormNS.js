import {
	NON_STANDARD_PARAMETER,
	GENERAL_NON_STANDARD,
	OPERATION,
	CONFIRM,
	INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Button, Tooltip, Icon } from 'antd'

class AHUFormNS extends React.Component {

	handleClickAhuWH() {


	}

	render() {
		const { onSairvolumeChange, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout, onCalculateNsPrice, componentValues, ahuId } = this.props
		return (
			<form data-name="AHUSectionFormNS">
				<div className="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

					<Group title={intl.get(NON_STANDARD_PARAMETER)} id="group1">
						<div className="col-lg-12">
							<Field id="ns.ahu.enable" />
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.Price" />
						</div>
						<div className="col-lg-9">
							<Field id="ns.ahu.MEMO" />
						</div>

					</Group>

					<Group title={intl.get(GENERAL_NON_STANDARD)} id="group1">
						<div className="col-lg-12">
							<Field id="ns.ahu.deformation" />
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.nsheight"
								onBlurChange={(event, newValue, previousValue) => onCalculateNsPrice('ns.ahu.enable', 'ns.ahu.nsChannelSteelBase', newValue, this.props)}
							/>
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.nswidth"
								onBlurChange={(event, newValue, previousValue) => onCalculateNsPrice('ns.ahu.enable', 'ns.ahu.nsChannelSteelBase', newValue, this.props)}
							/>
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.deformationSerial" />
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.deformationPrice" />
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.nsChannelSteelBase"
								diyAction='doCalculateAction'
								allValue={componentValues[0]}
								ahuId={ahuId}
							// onClick={(event, newValue, previousValue) => onCalculateNsPrice('ns.ahu.enable', 'ns.ahu.nsChannelSteelBaseModel', newValue, this.props)}
							/>
						</div>
						<div className="col-lg-6">
							<Field id="ns.ahu.nsChannelSteelBaseModel"
								onBlurChange={(event, newValue, previousValue) => onCalculateNsPrice('ns.ahu.enable', 'ns.ahu.nsChannelSteelBase', newValue, this.props)}
							/>
						</div>
						<div className="col-lg-3">
							<Field id="ns.ahu.nsChannelSteelBasePrice" />
						</div>
					</Group>

				</div>
			</form>
		)
	}
}

export default reduxForm({
	form: 'AHUSectionFormNS', // a unique identifier for this form
	enableReinitialize: true,
})(AHUFormNS)
