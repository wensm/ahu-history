import {
    WINTER_SLASH_SUMMER,
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    OPTION,
    ATTACHMENT,
    OPERATION,
    WHEEL_SIZE,
    SUPPLY_WIND_SIDE,
    EXHAUST_SIDE,
    HEAT_RECYCLE,
    DB_TEMPERATURE,
    WET_BULB_TEMPERATURE,
    RELATIVE_HUMIDITY,
    AIR_RESISTANCE,
    EFFICIENCY,
    TOTAL_HEATING,
    SHOW_HEAT,
    CONFIRM,
    SEASON,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';


class PlateHeatRecycle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: false
        }
        this.onForm = this.onForm.bind(this)
    }
    onForm() {
        this.setState({
            form: true
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.standard) {
            $("input[name='meta_section_plateHeatRecycle_Resistance']").attr("disabled", "disabled");//禁用input标签
            $('input[name=meta_section_plateHeatRecycle_Resistance]').attr("readonly", "readonly")//将input元素设置为readonly
        } else {
            $("input[name='meta_section_plateHeatRecycle_Resistance']").attr("disabled", false);//禁用input标签
            $('input[name=meta_section_plateHeatRecycle_Resistance]').removeAttr("readonly");//去除input元素的readonly属性

        }
    }
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            componentValue,
            PlateHeatRecycleform, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let line = []
        if (PlateHeatRecycleform) {
            line = PlateHeatRecycleform.values.meta_section_plateHeatRecycle_HeatRecycleDetail
        }
        let enableWinter = componentValue ? eval(componentValue.meta_section_plateHeatRecycle_EnableWinter) : false

        return (
            <form data-name="PlateHeatRecycle">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{intl.get(WINTER_SLASH_SUMMER)}</h3>
                        </div>
                        <div className="panel-body" style={{ padding: '15px 0' }}>
                            <div>
                                <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                                    <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                        role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                                    </li>
                                    <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                        data-toggle="tab">{intl.get(WINTER)}</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="summer">
                                        <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SNewDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_plateHeatRecycle_SNewWetBulbT', newValue, 'meta_section_plateHeatRecycle_SNewRelativeT', 'meta_section_plateHeatRecycle_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SNewWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_plateHeatRecycle_SNewDryBulbT', newValue, 'meta_section_plateHeatRecycle_SNewRelativeT', 'meta_section_plateHeatRecycle_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_plateHeatRecycle_SNewDryBulbT', newValue, 'meta_section_plateHeatRecycle_SNewWetBulbT', 'meta_section_plateHeatRecycle_SNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_plateHeatRecycle_SInWetBulbT', newValue, 'meta_section_plateHeatRecycle_SInRelativeT', 'meta_section_plateHeatRecycle_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_plateHeatRecycle_SInDryBulbT', newValue, 'meta_section_plateHeatRecycle_SInRelativeT', 'meta_section_plateHeatRecycle_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.SInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_plateHeatRecycle_SInDryBulbT', newValue, 'meta_section_plateHeatRecycle_SInWetBulbT', 'meta_section_plateHeatRecycle_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.NAVolume" />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.RAVolume" />
                                                </div>
                                            </div>
                                        </Group>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="winter">
                                        <div style={{ marginBottom: '5px' }}>
                                            <Field id="meta.section.plateHeatRecycle.EnableWinter" />
                                        </div>
                                        {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WNewDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_plateHeatRecycle_WNewWetBulbT', newValue, 'meta_section_plateHeatRecycle_WNewRelativeT', 'meta.section.plateHeatRecycle.WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WNewWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_plateHeatRecycle_WNewDryBulbT', newValue, 'meta_section_plateHeatRecycle_WNewRelativeT', 'meta.section.plateHeatRecycle.WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_plateHeatRecycle_WNewDryBulbT', newValue, 'meta_section_plateHeatRecycle_WNewWetBulbT', 'meta.section.plateHeatRecycle.WNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_plateHeatRecycle_WInWetBulbT', newValue, 'meta_section_plateHeatRecycle_WInRelativeT', 'meta.section.plateHeatRecycle.WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_plateHeatRecycle_WInDryBulbT', newValue, 'meta_section_plateHeatRecycle_WInRelativeT', 'meta.section.plateHeatRecycle.WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.WInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_plateHeatRecycle_WInDryBulbT', newValue, 'meta_section_plateHeatRecycle_WInWetBulbT', 'meta.section.plateHeatRecycle.WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.NAVolume" />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.plateHeatRecycle.RAVolume" />
                                                </div>
                                            </div>
                                        </Group> : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Group title={intl.get(OPTION)} id="group1">
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.Brand" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.HeatRecoveryType" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.EfficiencyType" />
                        </div>}
                        <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.BracketM" />
                        </div>
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.Installation" />
                        </div>}

                    </Group>
                    <Group title={intl.get(ATTACHMENT)} id="group8">
                        <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.SectionSideL" />
                        </div>
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.HeatExchangerL" />
                        </div>}
                        <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.HeatRecoveryEfficiency" />
                        </div>
                    </Group>
                    <Group title={intl.get(ATTACHMENT)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.sectionL" />
                        </div>
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.plateHeatRecycle.Weight" />
                        </div>}
                    </Group>

                    <Group title={intl.get(OPERATION)} id="group10">
                        {this.props && this.props.standard && <div className="modal-body">
                            <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                <table className="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th rowSpan="2"
                                                style={{ textAlign: 'center', paddingBottom: '26px' }}>{intl.get(WHEEL_SIZE)}</th>
                                            <th rowSpan="2"
                                                style={{ textAlign: 'center', paddingBottom: '26px' }}>{intl.get(SEASON)}</th>
                                            <th colSpan="5"
                                                style={{ textAlign: 'center' }}>{intl.get(SUPPLY_WIND_SIDE)}</th>
                                            <th colSpan="5"
                                                style={{ textAlign: 'center' }}>{intl.get(EXHAUST_SIDE)}</th>
                                            <th colSpan="2"
                                                style={{ textAlign: 'center' }}>{intl.get(HEAT_RECYCLE)} </th>
                                        </tr>
                                        <tr>
                                            <td> {intl.get(DB_TEMPERATURE)} </td>
                                            <td> {intl.get(WET_BULB_TEMPERATURE)} </td>
                                            <td> {intl.get(RELATIVE_HUMIDITY)} </td>
                                            <td> {intl.get(AIR_RESISTANCE)} </td>
                                            <td> {intl.get(EFFICIENCY)} </td>

                                            <td> {intl.get(DB_TEMPERATURE)} </td>
                                            <td> {intl.get(WET_BULB_TEMPERATURE)} </td>
                                            <td> {intl.get(RELATIVE_HUMIDITY)} </td>
                                            <td> {intl.get(AIR_RESISTANCE)} </td>
                                            <td> {intl.get(EFFICIENCY)} </td>

                                            <td> {intl.get(TOTAL_HEATING)} </td>
                                            <td> {intl.get(SHOW_HEAT)} </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{intl.get('meta.moon_intl_str_1145')}</td>

                                            <td></td>

                                            <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1051')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1024')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1051')}</td>

                                            <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1051')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1024')}</td>
                                            <td>{intl.get('meta.moon_intl_str_1051')}</td>

                                            <td>{intl.get('title.moon_intl_str_0669')}</td>
                                            <td>{intl.get('title.moon_intl_str_0669')}</td>
                                        </tr>
                                        {line && line.map((line, index) => <WheelmentLine key={index} index={index} line={line} />)}
                                    </tbody>
                                </table>
                            </div>
                        </div>}

                        <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
                            <Button disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => {
                                    onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, !this.props.standard)
                                    this.onForm(this.props)
                                }
                                }>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}
class WheelmentLine extends React.Component {

    render() {
        const { line, index } = this.props
        return (
            <tr>
                <td>{line['returnWheelSize']}</td>
                <td>{line['returnSeason']}</td>

                <td>{line['returnSDryBulbT']}</td>
                <td>{line['returnSWetBulbT']}</td>
                <td>{line['returnSRelativeT']}</td>
                <td>{line['returnSAirResistance']}</td>
                <td>{line['returnSEfficiency']}</td>

                <td>{line['returnEDryBulbT']}</td>
                <td>{line['returnEWetBulbT']}</td>
                <td>{line['returnERelativeT']}</td>
                <td>{line['returnEAirResistance']}</td>
                <td>{line['returnEEfficiency']}</td>

                <td>{line['returnTotalHeat']}</td>
                <td>{line['returnSensibleHeat']}</td>
            </tr>
        )
    }
}
export default reduxForm({
    form: 'PlateHeatRecycle', // a unique identifier for this form
    enableReinitialize: true,
})(PlateHeatRecycle)
