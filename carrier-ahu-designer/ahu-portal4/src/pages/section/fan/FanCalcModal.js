import {
    FAN_CALCULATION_RESULT,
    FAN_MODEL,
    ABSORBED_POWER,
    EFFICIENCY,
    MAX_RPM,
    MOTOR_BASE_NO,
    MOTOR_POWER,
    OUTLET_WIND_SPEED,
    MOTOR_POLE_NUMBER,
    PRICE,
    CANCEL,
    OK,
    NOISE,
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from '../../ahu/AHUPage.css'
import loadingImg from '../../../images/loding.gif'
import {isArray} from 'lodash'
export default class FanCalcModal extends React.Component {
	constructor(props) {
        super(props)
        this.state = {
            currentSelect: 0
        }
        this.updateSelectedIndex = this.updateSelectedIndex.bind(this)
    }

  updateSelectedIndex(index) {
      this.setState({currentSelect: index})
      
  }
  handleConfirm() {
    const index = $('#fanCalcModal').find('input[type="radio"]:checked').attr('data-index')
    if (typeof index !== typeof undefined) {
      this.props.onUpdateFanValue(index)
    }
  }

  render() {
    const { fanCalcs } = this.props
    let modalErrorStyle = {
      minHeight:'200px',
      lineHeight:'200px',
      textAlign:'center',
      color:'red'
  }
    return (
      <div data-name="FanCalcModal">
        <div className="modal fade" id="fanCalcModal" tabIndex="-1" role="dialog" aria-labelledby="fanCalcModalLabel">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="fanCalcModalLabel">{intl.get(FAN_CALCULATION_RESULT)}</h4>
              </div>
              {fanCalcs == 'clean' ||  (isArray(fanCalcs) && fanCalcs.length>0) ?
              <div className="modal-body">
                {fanCalcs == 'clean' && <img src={loadingImg} style={{position:'absolute',width:'40px',top:'90%',left:'45%'}}/>}
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>{intl.get(FAN_MODEL)}</th>
                      <th>{intl.get(ABSORBED_POWER)}</th>
                      <th>{intl.get(EFFICIENCY)}</th>
                      <th>{intl.get(NOISE)}</th>
                      <th>{intl.get(MAX_RPM)}</th>
                      <th>{intl.get(MOTOR_BASE_NO)}</th>
                      <th>{intl.get(MOTOR_POWER)}</th>
                      <th>{intl.get(OUTLET_WIND_SPEED)}</th>
                      <th>{intl.get(MOTOR_POLE_NUMBER)}</th>
                      <th>{intl.get(PRICE)}</th>
                    </tr>
                    <tr className={style.Faninittr}>
                      <td></td>
                      <td></td>
                      <td>{intl.get('meta.moon_intl_str_1366')}</td>
                      <td>{intl.get('title.moon_intl_str_0665')}</td>
                      <td>{intl.get('title.moon_intl_str_0669')}</td>
                      <td>{intl.get('title.moon_intl_str_0670')}</td>
                      <td></td>
                      <td>{intl.get('meta.moon_intl_str_1366')}</td>
                      <td>{intl.get('title.moon_intl_str_0663')}</td>
                      <td></td>
                      <td>RMB(LP)</td>
                    </tr>
                  </thead>
                  <tbody>
                    {fanCalcs && isArray(fanCalcs) && fanCalcs.map((fanCalc, index) =>
                    	<FanCalc key={index} index={index} fanCalc={fanCalc} currentSelect={this.state.currentSelect} updateSelectedIndex={this.updateSelectedIndex}/>)}
                  </tbody>
                </table>
              </div>
              :<div className = "modal-error" style={modalErrorStyle}>
                                {fanCalcs}
                            </div>}
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
                <button
                  type="button"
                  className="btn btn-primary"
                  data-dismiss="modal"
                  onClick={() => this.handleConfirm()}
                >
                  {intl.get(OK)}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class FanCalc extends React.Component {
  render() {
    const { index, fanCalc,updateSelectedIndex,currentSelect } = this.props
    return (
      <tr onClick={() => updateSelectedIndex(index)}>
        <td>
          <input type="radio" name="FanCalc" checked={index == currentSelect} data-index={index} />
        </td>
        <td>{fanCalc['meta.section.fan.fanModel']}</td>
        <td>{fanCalc['meta.section.fan.absorbedPower']}</td>
        <td>{fanCalc['meta.section.fan.efficiency']}</td>
        <td>{fanCalc['meta.section.fan.noise']}</td>
        <td>{fanCalc['meta.section.fan.RPM']}</td>
        <td>{fanCalc['meta.section.fan.motorBaseNo']}</td>
        <td>{fanCalc['meta.section.fan.motorPower']}</td>
        <td>{fanCalc['meta.section.fan.outletVelocity']}</td>
        <td>{fanCalc['meta.section.fan.pole']}</td>
        <td>{fanCalc['meta.section.fan.price']}</td>
      </tr>
    )
  }
}
