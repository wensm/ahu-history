import {
  NON_STANDARD_OPTION,
  CALCULATION,
  CONFIRM,
  OPERATION
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class FanNS extends React.Component {
  render() {
    const { onCalcFan, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props

    return (
      <form data-name="FanNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.fan.enable" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.fan.Price" />
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.fan.MEMO" />
            </div>
          </Group>
          <Group id="group1">
            <div className="col-lg-2">
              <Field id="ns.section.fan.nsChangeFan" />
            </div>
            <div className="col-lg-4">
              <Field id="ns.section.fan.nsFanSupplier" />
            </div>
            <div className="col-lg-4">
              <Field id="ns.section.fan.nsFanModel" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.fan.nsFanPrice" />
            </div>

            <div className="col-lg-2">
              <Field id="ns.section.fan.nsChangeMotor" />
            </div>
            <div className="col-lg-4">
              <Field id="ns.section.fan.nsMmotorPole" />
            </div>
            <div className="col-lg-4">
              <Field id="ns.section.fan.nsMotorPower" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.fan.nsMotorPrice" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.fan.nsSpringtransform" />
            </div>
            <div className="col-lg-4">
              
            </div>
            <div className="col-lg-4">
              
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.fan.nsSpringtransformPrice" />
            </div>
            
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
            <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
              {/*<button//非标下暂时把计算注释掉zzf
                // disabled={!this.props.canConfirm ||this.props.invalid}
                type="button" className="btn btn-primary"
                data-toggle="modal" data-target="#fanCalcModal"
                onClick={() => onCalcFan()}
              >
                {intl.get(CALCULATION)}
              </button>*/}
              <Button
                disabled={!this.props.canConfirm || this.props.invalid}
                // type="button"
                // className="btn btn-primary"
                // style={{marginLeft:'10px'}}
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}
              >
                {intl.get(CONFIRM)}
              </Button>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'FanNS',
  enableReinitialize: true,
})(FanNS)
