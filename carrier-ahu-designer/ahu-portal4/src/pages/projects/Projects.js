import React from 'react'
import Header from '../ahu/Header'
import {Link} from 'react-router'
import ProjectListContainer from './ProjectListContainer'
import ProjectFormContainer from './ProjectFormContainer'
import BatchImport from '../../components/BatchImport'
import BatchExport from '../../components/BatchExport'
import AhuInitContainer from '../../components/AhuInitContainer'
import CreateReportContainer from '../../components/CreateReportContainer'
import Buttons from './Buttons'
import SetDirectionContainer from './SetDirectionContainer'
import style from './Projects.css'

export default class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.saveBatchImport = this.saveBatchImport.bind(this);
    this.saveBatchExport = this.saveBatchExport.bind(this);
  }

  componentDidMount() {
    const {onFetchProjects, archived} = this.props
    onFetchProjects();
    $("a").on("click", function () {
      $("a").find(".active").removeClass("active");
      $(this).parent().addClass("active");
      // console.log("click")
    });

  }

  saveBatchImport(formData, groupId, coverId, callBack) {
    this.props.onSaveBatchImport(formData,coverId, callBack, this.props.archived);
  }

  saveBatchExport(projectId, fileName) {
    this.props.onSaveBatchExport(projectId, fileName);
  }

  render() {
    const {projectId, onSaveCreateReport, onClearReports, onReportGenerated, ahuGroupList, projectList, onFetchProjects} = this.props;
    return (
      <div data-name="Projects">
        <Buttons onFetchProjects={onFetchProjects}/>
        <ProjectFormContainer/>
        <ProjectListContainer/>
        <BatchImport coverList={projectList} coverId="pid" coverName="name" saveBatchImport={this.saveBatchImport}/>
        <BatchExport saveBatchExport={this.saveBatchExport}/>
        <AhuInitContainer/>
        <CreateReportContainer flg="projects"
                               onSaveCreateReport={onSaveCreateReport}
                               onReportGenerated={onReportGenerated}
                               onClearReports={onClearReports}/>
        <SetDirectionContainer ahuGroupList={ahuGroupList}/>
      </div>
    )
  }
}
