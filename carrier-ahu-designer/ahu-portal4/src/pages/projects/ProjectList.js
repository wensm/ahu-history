import {
    PROJECT_NO,
    SELECTION_BATCH_SETTING,
    GENERATE_REPORT,
    DIRECTION_SETTING,
    EXPORT,
    ARCHIVE,
    EDIT,
    DELETE,
    PROJECT_NAME,
    DATE,
    CONTRACT_NUMBER,
    PROJECT_ADDRESS,
    MODIFY_TIME,
    PRICE_BASELINE,
    NON_STANDARD_QUOTATION_NUMBER,
    SALES,
    CUSTOMER_NAME,
    ENTER_PROJECT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import ReactDOM from 'react-dom'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import pro_icon1Img from '../../images/pro_icon1.png'
import pro_icon2Img from '../../images/pro_icon2.png'
import pro_icon3Img from '../../images/pro_icon3.png'
import pro_icon4Img from '../../images/pro_icon4.png'
import DateUtil from '../../misc/DateUtils'
import { getStatus } from '../../actions/projects'
import style from './ProjectList.css'
import {
    Table, Input, Button, Icon, Divider,
} from 'antd';
import Highlighter from 'react-highlight-words'

export default class ProjectList extends React.Component {
    constructor(props) {
        super()
        this.doFilter = this.doFilter.bind(this)
    }

    doFilter(project) {
        const { projectFilterStr } = this.props
        let tempFilter = $.trim(projectFilterStr).toLowerCase()
        if (tempFilter == "" || tempFilter == null || tempFilter == undefined) {
            return true
        } else {
            if ((project.customerName && project.customerName.toLowerCase().includes(tempFilter)) ||//客户名称
                (project.contract && project.contract.toLowerCase().includes(tempFilter)) ||//合同号
                (project.projectDate && project.projectDate.toLowerCase().includes(tempFilter)) ||//日期
                (project.name && project.name.toLowerCase().includes(tempFilter))) {//项目名称
                return true
            }
            return false
        }
        // if (!project.customerName || project.customerName.toLowerCase().includes(tempFilter)) {
        //     return true
        // }
        // if (!project.contract || project.contract.toLowerCase().includes(tempFilter)) {
        //     return true
        // }
        // if (!project.name || project.name.toLowerCase().includes(tempFilter)) {
        //     return true
        // }
        // return false

        return true
    }

    render() {
        const { projectList, templates, onDelProject, onFetchProkey, onUpdateProjectPre, onFetchAhuGroupList, archived, onToArchieved } = this.props
        let newProjectList = projectList.filter(this.doFilter)
        return (
            <div className={style.projectDiv} data-name="ProjectList">
                {/* {
                    newProjectList.map((project, index) =>
                        <Project key={index} {...project}
                            onDelProject={onDelProject}
                            onFetchProkey={onFetchProkey}
                            projectId={project.pid}
                            onUpdateProjectPre={onUpdateProjectPre}
                            onFetchAhuGroupList={onFetchAhuGroupList}
                        />
                    )
                } */}
                <ProjectList2 data={newProjectList}
                    onFetchAhuGroupList={onFetchAhuGroupList}
                    onUpdateProjectPre={onUpdateProjectPre}
                    onDelProject={onDelProject}
                    archived={archived}
                    onToArchieved={onToArchieved}
                />
            </div>
        )
    }
}




class ProjectList2 extends React.Component {
    constructor(props, context) {
        super(props, context)
        // this.doFilter = this.doFilter.bind(this)
        this.router = context.router;

        this.state = {
            searchText: '',
        }
    }


    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
        setSelectedKeys, selectedKeys, confirm, clearFilters,
      }) => (
                <div style={{ padding: 8 }}>
                    <Input
                        ref={node => { this.searchInput = node; }}
                        placeholder={`Search ${dataIndex}`}
                        value={selectedKeys[0]}
                        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                        onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                        style={{ width: 188, marginBottom: 8, display: 'block' }}
                    />
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm)}
                        icon="search"
                        size="small"
                        style={{ width: 90, marginRight: 8 }}
                    >
                        Search
          </Button>
                    <Button
                        onClick={() => this.handleReset(clearFilters)}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Reset
          </Button>
                </div>
            ),
        filterIcon: (filtered) => {
            return <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        },
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: (text) => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text && text.toString()}
            />
        ),
    })

    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = (clearFilters) => {
        clearFilters();
        this.setState({ searchText: '' });
    }
    openCreateReport(projectId) {
        $('#CreateReport').data("projectId", projectId)
        // $('#CreateReport').modal('show')
        $('#CreateReport').modal({ backdrop: 'static', keyboard: false })

    }

    setDirection(projectId) {
        this.props.onFetchAhuGroupList(projectId)
        $('#SetDirection').data("projectId", projectId)
        $('#SetDirection').modal('show')
    }
    openExportProject(projectId) {
        $('#BatchExport').data("projectId", projectId)
        $('#BatchExport').modal('show')
    }
    toArchieved(projectId, archived) {
        this.props.onToArchieved(projectId, archived)
    }

    // openAhuInit(projectId) {
    //     this.props.onFetchProkey(projectId)
    // }

    render() {
        const columns = [{
            title: intl.get(PROJECT_NAME),
            dataIndex: 'name',
            key: 'name',
            width: '12%',
            ...this.getColumnSearchProps('name'),
            // }, {
            //     title: intl.get(PROJECT_NO),
            //     dataIndex: 'no',
            //     key: 'no',
            //     width: '12%',
            //     ...this.getColumnSearchProps('no'),
        }, {
            title: intl.get(DATE),
            dataIndex: 'projectDate',
            key: 'projectDate',
            width: '12%',
            ...this.getColumnSearchProps('projectDate'),
        }, {
            title: intl.get(CONTRACT_NUMBER),
            dataIndex: 'contract',
            key: 'contract',
            width: '12%',
            ...this.getColumnSearchProps('contract'),
        }, {
            //     title: intl.get(PROJECT_ADDRESS),
            //     dataIndex: 'address',
            //     key: 'address',
            //     width: '12%',
            //     ...this.getColumnSearchProps('address'),
            // }, {
            //     title: intl.get(MODIFY_TIME),
            //     dataIndex: 'updateTime',
            //     key: 'updateTime',
            //     width: '12%',
            //     ...this.getColumnSearchProps('updateTime'),
            // }, {
            //     title: intl.get(PRICE_BASELINE),
            //     dataIndex: 'priceBase',
            //     key: 'priceBase',
            //     width: '12%',
            //     ...this.getColumnSearchProps('priceBase'),
            // }, {
            title: intl.get(NON_STANDARD_QUOTATION_NUMBER),
            dataIndex: 'enquiryNo',
            key: 'enquiryNo',
            width: '12%',
            ...this.getColumnSearchProps('enquiryNo'),
        }, {
            title: intl.get(SALES),
            dataIndex: 'saler',
            key: 'saler',
            width: '12%',
            ...this.getColumnSearchProps('saler'),
        }, {
            title: intl.get(CUSTOMER_NAME),
            dataIndex: 'customerName',
            key: 'customerName',
            width: '12%',
            ...this.getColumnSearchProps('customerName'),
        }, {
            title: 'Action',
            key: 'action',
            render: (text, record) => {
                return <span>
                    {record.recordStatus == "archieved" ? '' :<a onClick={() => this.props.onUpdateProjectPre(record.pid)}>
                        {/* <i
                         data-id={record.pid}
                        className="iconfont icon-xiugaishijian" data-toggle="tooltip" data-placement="top"
                        title={intl.get(EDIT)}></i> */}
                        {intl.get(EDIT)}
                    </a>}

                    {record.recordStatus == "archieved" ? '' :<Divider type="vertical" />}
                    <a onClick={() => this.openExportProject(record.pid)}>
                        {/* <i className="iconfont icon-daochu"
                        onClick={() => this.openExportProject(record.pid)}
                        data-toggle="tooltip" data-placement="top"
                        title={intl.get(EXPORT)}></i> */}
                        {intl.get(EXPORT)}
                    </a>
                    <Divider type="vertical" />
                    {record.recordStatus == "archieved" ? '' : <a onClick={() => this.toArchieved(record.pid, this.props.archived)}>{intl.get(ARCHIVE)}</a>}
                    {record.recordStatus == "archieved" ? '' : <Divider type="vertical" />}

                    {/* <Link to={{ pathname: '/page2/' + record.pid, query: { name: 'app' } }} style={{
                        color: '#1890ff',
                        lineHeight: '40px'
                    }}>
                        {intl.get(ENTER_PROJECT)}
                    </Link> */}
                    {/* <Divider type="vertical" /> */}
                    <a onClick={() => this.openCreateReport(record.pid)}>{intl.get(GENERATE_REPORT)}</a>
                    <Divider type="vertical" />
                    {record.recordStatus == "archieved" ? '' :<a onClick={() => this.setDirection(record.pid)}>{intl.get(DIRECTION_SETTING)}</a>}
                    {record.recordStatus == "archieved" ? '' :<Divider type="vertical" />}
                    <a onClick={() => { this.props.onDelProject(record.pid, this.props.archived) }}>
                        {/* <i data-id={record.pid} className="iconfont icon-shanchu" data-toggle="tooltip"
                        data-placement="top" title={intl.get(DELETE)}></i> */}
                        {intl.get(DELETE)}
                    </a>


                </span>
            },
        }];
        let { data } = this.props
        data.forEach((value) => {
            if (value.projectDate == null || value.projectDate == '') {
                value.projectDate = ""
            } else {
                value.projectDate = DateUtil.getFormatDateByLong(value.projectDate, 'yyyy-MM-dd')
            }
            if (value.updateTime == null || value.updateTime == '') {
                value.updateTime = ""
            } else {
                value.updateTime = DateUtil.getFormatDateByLong(value.updateTime, 'yyyy-MM-dd hh:mm:ss')
            }
        })
        return <Table columns={columns} dataSource={data} onRow={(record) => {
            return {
            //   onClick: (event) => {console.log('11111', event)},       // 点击行
              onDoubleClick: (event) => {
                  this.router.push({ pathname: '/page2/' + record.pid, query: { name: 'app' } })
                },
            //   onContextMenu: (event) => {console.log('333333', event)},
            //   onMouseEnter: (event) => {console.log('444444', event)},  // 鼠标移入行
            //   onMouseLeave: (event) => {console.log('555555', event)}
            };
          }} />;
    }
}
ProjectList2.contextTypes = {
    router: React.PropTypes.object.isRequired
};



class Project extends React.Component {
    longToDate(longDate) {
        if (longDate == null || longDate == '') {
            return ""
        }
        return DateUtil.getFormatDateByLong(longDate, 'yyyy-MM-dd')
    }

    longToDateTime(longDate) {
        if (longDate == null || longDate == '') {
            return ""
        }
        return DateUtil.getFormatDateByLong(longDate, 'yyyy-MM-dd hh:mm:ss')
    }

    // openExportProject(projectId) {
    //     $('#BatchExport').data("projectId", projectId)
    //     $('#BatchExport').modal('show')
    // }

    // openAhuInit(projectId) {
    //     this.props.onFetchProkey(projectId)
    // }

    // openCreateReport(projectId) {
    //     $('#CreateReport').data("projectId", projectId)
    //     // $('#CreateReport').modal('show')
    //     $('#CreateReport').modal({ backdrop: 'static', keyboard: false })

    // }

    // setDirection(projectId) {
    //     this.props.onFetchAhuGroupList(projectId)
    //     $('#SetDirection').data("projectId", projectId)
    //     $('#SetDirection').modal('show')
    // }

    render() {
        const project = this.props
        return (
            <div className="panel panel-default" data-name="Project">
                <div className={classnames(style.panelHead, 'panel-heading')}>
                    <b>{intl.get(PROJECT_NO)}:{project.no}</b>
                    <div className="pull-right">
                        <ol className={classnames(style.rightMenu, style.textRightMenu, 'pull-left')}>
                            {/*<li><a onClick={() => this.openAhuInit(project.pid)} data-toggle="modal"//该按钮暂时禁用掉，zzf
                                   data-target="#AhuInit">{intl.get(SELECTION_BATCH_SETTING)}</a></li>*/}
                            <li><a onClick={() => this.openCreateReport(project.pid)}>{intl.get(GENERATE_REPORT)}</a>
                            </li>
                            <li><a onClick={() => this.setDirection(project.pid)}>{intl.get(DIRECTION_SETTING)}</a>
                            </li>
                        </ol>
                        <ol className={classnames(style.rightMenu, style.iconRightMenu, 'pull-left')}>
                            <li className="col1"><a href="#"><i className="iconfont icon-daochu"
                                onClick={() => this.openExportProject(project.pid)}
                                data-toggle="tooltip" data-placement="top"
                                title={intl.get(EXPORT)}></i></a>
                            </li>
                            {/*<li className="col2"><a href="#"><i className="iconfont icon-guidang" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title={intl.get(ARCHIVE)}></i></a>
                                </li>zzf*/}
                            <li className="col3"><a href="#"><i
                                onClick={() => this.props.onUpdateProjectPre(project.pid)} data-id={project.pid}
                                className="iconfont icon-xiugaishijian" data-toggle="tooltip" data-placement="top"
                                title={intl.get(EDIT)}></i></a></li>
                            <li className="col4" onClick={() => this.props.onDelProject(project.pid)}><a href="#"><i
                                data-id={project.pid} className="iconfont icon-shanchu" data-toggle="tooltip"
                                data-placement="top" title={intl.get(DELETE)}></i></a></li>
                        </ol>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-lg-10 col-md-10">
                            <div className={classnames(style.itemList1, 'row')}>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={classnames(style.itemIcon, 'pull-left')}><img src={pro_icon1Img} />
                                    </div>
                                    <div className="pull-left">
                                        <p className={style.itemTitle}>{intl.get(PROJECT_NAME)}</p>
                                        <b className={classnames(style.itemText, style.colorDark)}>{project.name}</b>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={classnames(style.itemIcon, 'pull-left')}><img src={pro_icon2Img} />
                                    </div>
                                    <div className="pull-left">
                                        <p className={style.itemTitle}>{intl.get(DATE)}</p>
                                        <b className={classnames(style.itemText, style.colorDark)}>{this.longToDate(project.projectDate)}</b>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={classnames(style.itemIcon, 'pull-left')}><img src={pro_icon3Img} />
                                    </div>
                                    <div className="pull-left">
                                        <p className={style.itemTitle}>{intl.get(CONTRACT_NUMBER)}</p>
                                        <b className={classnames(style.itemText, style.colorDark)}>{project.contract}</b>
                                    </div>
                                </div>
                            </div>
                            <div className={classnames(style.itemList2, 'row')}>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i
                                            className="iconfont icon-xiangmudizhi"></i></em>
                                        <span className={style.itemTitle}>{intl.get(PROJECT_ADDRESS)}</span>
                                        <b className={style.itemText}>{project.address}</b>
                                    </div>
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i
                                            className="iconfont icon-xiugaishijian"></i></em>
                                        <span className={style.itemTitle}>{intl.get(MODIFY_TIME)}</span>
                                        <b className={style.itemText}>{this.longToDateTime(project.updateTime)}</b>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i className="iconfont icon-hetonghao"></i></em>
                                        <span className={style.itemTitle}>{intl.get(PRICE_BASELINE)}</span>
                                        <b className={style.itemText}>{project.priceBase}</b>
                                    </div>
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i
                                            className="iconfont icon-feibiaoxunjiadanbianhao"></i></em>
                                        <span className={style.itemTitle}>{intl.get(NON_STANDARD_QUOTATION_NUMBER)}</span>
                                        <b className={style.itemText}>{project.enquiryNo}</b>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i
                                            className="iconfont icon-xiaoshouyuan"></i></em>
                                        <span className={style.itemTitle}>{intl.get(SALES)}</span>
                                        <b className={style.itemText}>{project.saler}</b>
                                    </div>
                                    <div className={style.itemDiv}>
                                        <em className={style.itemIcon}><i className="iconfont icon-hetonghao"></i></em>
                                        <span className={style.itemTitle}>{intl.get(CUSTOMER_NAME)}</span>
                                        <b className={style.itemText}>{project.customerName}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-2  col-md-2">
                            <div className={style.degOfComp}>
                                <h4>{intl.get(getStatus(project.recordStatus))}</h4>
                                <div className={style.projectAhuBtn}
                                >
                                    <Link to={{ pathname: '/page2/' + project.pid, query: { name: 'app' } }} style={{
                                        color: 'white',
                                        lineHeight: '40px'
                                    }}>
                                        {intl.get(ENTER_PROJECT)}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

