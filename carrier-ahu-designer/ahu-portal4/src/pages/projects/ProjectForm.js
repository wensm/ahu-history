import {
    EDIT,
    DATE,
    PROJECT_NO,
    PROJECT_NAME,
    SALES,
    PROJECT_ADDRESS,
    NON_STANDARD_QUOTATION_NO,
    CONTRACT_NO,
    PRICE_BASELINE,
    CUSTOMER_NAME,
    STATE,
    CANCEL,
	SAVE,
	CONTRACT_NO_REMIND,
	PLEASE_CHOOSE,
    NO_PRICE
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react';
import http from '../../misc/http';
import {Field,reduxForm} from 'redux-form'
import DateUtil from '../../misc/DateUtils';
import classnames from 'classnames'
import style from './ProjectForm.css'

import {getStatus} from '../../actions/projects'

class ProjectForm extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	      project:{}
	    }
	    this.saveProject = this.saveProject.bind(this);
	 }
	componentDidMount() {
		$('#projectDate').datepicker({
			format: 'yyyy-mm-dd'
		});
	}
	saveProject(){
		if(this.props.projectForm.hasOwnProperty('contract')){
			!this.contractValidate(this.props.projectForm.contract) && this.props.onSaveProject(this.props.projectForm, this.props.archived);
		}else{
			this.props.onSaveProject(this.props.projectForm, this.props.archived);
		}
	}
	contractValidate(value){
		if(value == '' || value == null){
			return false
		}
//		if((value.length == 9 || value.length == 10) && (value.substr(-1).charCodeAt(0)<=90 && value.substr(-1).charCodeAt(0)>=65)){
		if((value.length == 9 ) && (value.substr(-1).charCodeAt(0)<=90 && value.substr(-1).charCodeAt(0)>=65)){
			return false
		}
		
		return true
	}
	
	doCheck (projectForm){
		if(projectForm.hasOwnProperty('contract')){
			let bool = false;
			if(!!projectForm.contract){
				bool = this.contractValidate(projectForm.contract) 
			}
			return bool
		}

		return true //默认显示红字
	}
  render() {
	  let projectForm = this.props.projectForm;
	  let doCheck = this.doCheck(this.props.projectForm)
	  const { user,priceBase, type } = this.props;
      return (
	    <div className="collapse" id="newProject" data-name="ProjectForm">
	    	<form data-name="ProjectForm">
		    	<div className="panel panel-default" style={{margin:'20px 0px'}}>
				  	<div className="panel-heading" style={{height:'60px',lineHeight:'40px',backgroundColor:'#4c74ac'}}>
				    	<span style={{fontSize:'16px',fontWeight:'700',color:'#ffffff'}}>{intl.get(EDIT)}</span>
				  	</div>
					<div className="panel-body">
					    <div  className={classnames(style.newProjectBody, 'row')}>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(DATE)}</label>
					    		<Field name="projectDate" id="projectDate" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(PROJECT_NO)}</label>
						        <Field name="no" id="no" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(PROJECT_NAME)}</label>
						        <Field name="name" id="name" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(SALES)}</label>
						        <Field name="saler" id="saler" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(PROJECT_ADDRESS)}</label>
						        <Field name="address" id="address" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(NON_STANDARD_QUOTATION_NO)}</label>
						        <Field name="enquiryNo" id="enquiryNo" component="input" type="text" />
					    	</div>

					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(CONTRACT_NO)}</label>
						        <Field name="contract" id="contract" component="input" type="text" />
								{doCheck && <div style={{paddingLeft:'10%'}}><span style={{color:'#a94442'}}>{intl.get(CONTRACT_NO_REMIND)}</span></div>}
							</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(PRICE_BASELINE)}</label>
                                {
                                    user && user.userRole == 'Design'?
											<Field name="priceBase" id="priceBase" component="select" style={{ cursor: type != 'edit' ? 'pointer' : 'not-allowed' }} disabled={type == 'edit' ? true : false}>
                                            	<option value="0">--{intl.get(NO_PRICE)}--</option>
                                        	</Field>
											:<Field name="priceBase" id="priceBase" component="select" style={{ cursor: type != 'edit' ? 'pointer' : 'not-allowed' }} disabled={type == 'edit' ? true : false}>
                                        		<option value="-1">--{intl.get(PLEASE_CHOOSE)}--</option>
												{
													priceBase.map((baseline, index) => {
														return <option key={index} value={baseline.key}>{baseline.value}</option>
													})
												}
                                    		</Field>
                                }
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(CUSTOMER_NAME)}</label>
						        <Field name="customerName" id="customerName" component="input" type="text" />
					    	</div>
					    	<div className="form-group col-md-4 col-sm-6">
					    		<label>{intl.get(STATE)}</label>
                  <label htmlFor="">{intl.get(getStatus(projectForm.recordStatus))}</label>
					    	</div>
					    </div>
					    <div className={style.btnGroup}>
						  	<button type="button" className="btn btn-info"  role="button" data-toggle="collapse" href="#newProject" aria-expanded="false" aria-controls="newProject">{intl.get(CANCEL)}</button>
							<button type="button" className="btn btn-primary" onClick={this.saveProject}>{intl.get(SAVE)}</button>
					  	</div>
					</div>
				</div>
			</form>
		</div>
    )
  }
}

export default reduxForm({
  form: 'ProjectForm',
  enableReinitialize: true,
})(ProjectForm)
