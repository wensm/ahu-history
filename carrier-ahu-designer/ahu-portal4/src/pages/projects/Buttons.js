import {
    NEW_PROJECT,
    IMPORT_PROJECT,
    OTHERCONDITIONS,
    TIME,
    ARCHIVED,
    UNARCHIVED,
    ALL
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from './Buttons.css'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { projectPageFilterUpdate, START_NEW_PROJECT, changeArchived } from '../../actions/projects'
import DateUtil from '../../misc/DateUtils';
import { DatePicker, Switch, Select, Dropdown, Menu, Button, Icon} from 'antd'
const Option = Select.Option
class Buttons extends React.Component {
    constructor(props) {
        super()
        this.onFilterUpdate = this.onFilterUpdate.bind(this)
        this.createProject = this.createProject.bind(this)
        this.state = {
            SwitchState: 'others'
        }
    }

    onFilterUpdate(event) {

        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        clearTimeout(this._onFilterUpdateTimer)
        this._onFilterUpdateTimer = setTimeout(() => {
            this.props.onProjectsPageFilterUpdate(value)
        }, 500);

    }


    createProject() {
        this.props.onCreateProject(this.props.user.userName, this.props.price)
    }
    handleMenuClick(e){
        const {onFetchProjects, onChangeArchived} = this.props
        onChangeArchived(e.key)
        onFetchProjects(e.key)
    }

    render() {
        let { SwitchState } = this.state
        const { onProjectsPageFilterUpdate, archived} = this.props
        const menu = (
            <Menu onClick={(e)=>this.handleMenuClick(e)}>
                <Menu.Item key="archieved">{intl.get(ARCHIVED)}</Menu.Item>
                <Menu.Item key="unArchieved">{intl.get(UNARCHIVED)}</Menu.Item>
                <Menu.Item key="all">{intl.get(ALL)}</Menu.Item>
            </Menu>
        );
        return (
            <div className={classnames(style.project, 'main promain clear')} data-name="Buttons">
                <div className="pro_con pull-left">
                    <ul className={classnames(style.proCap, 'clear')}>
                        <li role="button" data-toggle="collapse" href="#newProject" aria-expanded="false"
                            aria-controls="newProject" onClick={this.createProject}>
                            <i className="iconfont icon-xinjianxiangmu"></i>
                            {intl.get(NEW_PROJECT)}
                        </li>
                        <li data-toggle="modal" data-target="#BatchImport">
                            <i className="iconfont icon-daoru"></i>
                            {intl.get(IMPORT_PROJECT)}
                        </li>
                        {/* <li>
                            <Select defaultValue="others" style={{ width: 120, backgroundColor: '#3070d2' }} onChange={() => { }}>
                                <Option value="time">{intl.get(ARCHIVED)}</Option>
                                <Option value="others">{intl.get(UNARCHIVED)}</Option>
                            </Select>
                        </li> */}
                        {/* <li> */}
                            <Dropdown overlay={menu}>
                                <Button type='primary' style={{ marginLeft: 8, backgroundColor: '#3070d2'}}>
                                    {archived === 'archieved' ? intl.get(ARCHIVED): (archived === 'unArchieved' ? intl.get(UNARCHIVED) : intl.get(ALL)) }
                                    <Icon type="down" />
                                </Button>
                            </Dropdown>
                        {/* </li> */}

                    </ul>
                </div>

                {/* {SwitchState == 'others' ? <div className="pull-right form-group form-inline">
                    <div className="input-group">
                        <span className="input-group-addon" id="basic-addon1"><i className="fa fa-filter"
                            aria-hidden="true"></i></span>
                        <input type="text" className="form-control" placeholder="Filter String"
                            aria-describedby="basic-addon1" onChange={this.onFilterUpdate} />
                    </div>
                </div> : <div className="pull-right form-group form-inline">
                        <DatePicker format="YYYY-MM-DD" placeholder="Filter Date" style={{ width: '210px' }} onChange={(date, dateString) => {

                            onProjectsPageFilterUpdate(dateString)
                        }} />
                    </div>
                } */}
                {/* <div className="pull-right form-group form-inline" style={{ marginRight: '10px' }}>
                    <Select defaultValue="others" style={{ width: 120 }} onChange={(value) => { onProjectsPageFilterUpdate(''); this.setState({ SwitchState: value }) }}>
                        <Option value="time">{intl.get(TIME)}</Option>
                        <Option value="others">{intl.get(OTHERCONDITIONS)}</Option>
                    </Select>
                </div> */}
                <div className={style.clear}></div>
            </div>
        )
    }
}


const mapStateToProps = (state, owProps) => {
    return {
        ahuFilterStr: state.projects.ahuFilterStr,
        user: state.general.user,
        price: state.general.priceBase,
        archived: state.projects.archived
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onProjectsPageFilterUpdate(filterStr) {
            dispatch(projectPageFilterUpdate(filterStr))
        },
        onChangeArchived(value){
            dispatch(changeArchived(value))
            
            
        },
        onCreateProject(name, price) {
            dispatch(
                {
                    name: name,
                    projectDate: DateUtil.format(new Date(), "yyyy-MM-dd"),
                    priceBase: price[0]['key'],
                    type: START_NEW_PROJECT,
                }
            )
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)








