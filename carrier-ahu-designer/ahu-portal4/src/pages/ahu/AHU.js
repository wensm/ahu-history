import React from 'react'
import classnames from 'classnames'
import style from './AHU.css'
import { isSingleLine, isTwinLine, STYLE_CHAMBER, STYLE_COMMON, STYLE_PLATE, STYLE_WHELE, 
    STYLE_VERTICAL_1, STYLE_VERTICAL_2, STYLE_DOUBLE_RETURN_1, STYLE_DOUBLE_RETURN_2,
    STYLE_SIDE_BY_SIDE_RETURN } from "../../actions/ahu"

import mix from '../../images/pro_mix_b.png'
const requireContext = require.context("../../images/mix", false, /^\.\/.*\.png$/);
const images = requireContext.keys().map(requireContext);
import fan1 from '../../images/pro_fan_l1.png'
import fan2 from '../../images/pro_fan_r2.png'
import fan3 from '../../images/pro_fan_t1.png'
import fan4 from '../../images/pro_fan_r1.png'
import fan5 from '../../images/pro_fan_t2.png'
import plugfan from '../../images/pro_plugfan.png'
import plugfanL from '../../images/pro_plugfan_l.png'

import comb from '../../images/put_icon8_1.png'
import comb2 from '../../images/put_icon2_1.png'
const MAP = {
    mix,
    fan1, fan2, fan3, fan4, fan5, comb, comb2, plugfan, plugfanL
}
images.map((v, k) => {
    MAP[`mix${k + 1}`] = v
})

export default class AHU extends React.Component {

    constructor(props) {
        super();
        this.state = {
            hasCombine: false
        }
        this.setupAhuLine = this.setupAhuLine.bind(this)
        this.setAhuFun = this.setAhuFun.bind(this)
        this.getSection = this.getSection.bind(this)
    }

    setAhuFun(nextProps, section, $section, hasCombine) {//进行r 跟s  修改图标

        let letmix
        let asection = $(`[data-section='${section.sectionKey}']`).attr("data-section")
        // console.log(asection)
        switch (asection) {
            case 'ahu.fan':
                nextProps.sections.map((k, index) => {
                    if (index == section.position - 1) {
                        let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
                        switch (ahuDirection['meta.section.fan.outletDirection']) {
                            case 'BHF':
                                letmix = MAP.fan2
                                break;
                            case 'UBF':
                                letmix = MAP.fan3
                                break;
                            case 'UBR':
                                letmix = MAP.fan4
                                break;
                            default:
                                if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
                                    letmix = MAP.fan5
                                } else {
                                    letmix = MAP.fan1
                                }
                                break;
                        }
                        if(ahuDirection['meta.section.fan.outlet'] == "wwk") {
                            if((nextProps.layout.style == STYLE_PLATE || nextProps.layout.style == STYLE_WHELE)
                                    && (ahuDirection['meta.section.airDirection'] == "R"))  {
                                letmix = MAP.plugfanL
                            } else {
                                letmix = MAP.plugfan
                            }
                        }
                        $($section[0]).css("background-image", 'url(' + letmix + ')')
                    }
                })
                break;
            case 'ahu.mix':
                nextProps.sections.map((k, index) => {
                    if (index == section.position - 1) {
                        let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
                        function imgSet(mix1, mix2, mix3, mix4, mix5, mix6, mix7, mix8, mix9, mix10, mix11, mix12, mix13, mix14, mix15) {
                            if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
                                letmix = MAP[mix1]
                            } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
                                letmix = MAP[mix2]
                            } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnRight']) == true) {
                                letmix = MAP[mix3]
                            } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnLeft']) == true) {
                                letmix = MAP[mix4]
                            } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
                                letmix = MAP[mix5]
                            } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
                                letmix = MAP[mix6]
                            } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
                                letmix = MAP[mix7]
                            } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
                                letmix = MAP[mix8]
                            } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnRight']) == true) {
                                letmix = MAP[mix9]
                            } else if (eval(ahuDirection['meta.section.mix.returnBack']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
                                letmix = MAP[mix10]
                            } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true) {
                                letmix = MAP[mix11]
                            } else if (eval(ahuDirection['meta.section.mix.returnBack']) == true) {
                                letmix = MAP[mix12]
                            } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true) {
                                letmix = MAP[mix13]
                            } else if (eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
                                letmix = MAP[mix14]
                            } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true) {
                                letmix = MAP[mix15]
                            } else {
                                letmix = MAP[mix10]
                            }
                        }
                        switch (ahuDirection['meta.section.airDirection']) {
                            case 'R':
                                if (hasCombine === true) {
                                    imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                                } else {
                                    imgSet('mix12', 'mix13', 'mix15', 'mix16', 'mix6', 'mix8', 'mix24', 'mix26', 'mix5', 'mix19', 'mix10', 'mix21', 'mix5', 'mix', 'mix23')
                                }
                                break;
                            default:
                                imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                                break;
                        }
                        


                        $($section[0]).css("background-image", 'url(' + letmix + ')')
                    }
                })
                break;
            case 'ahu.discharge':
                nextProps.sections.map((k, index) => {
                    if (index == section.position - 1) {
                        let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
                        switch (ahuDirection['meta.section.discharge.OutletDirection']) {
                            case 'T':
                                letmix = MAP.mix10
                                break;
                            case 'A':
                                if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
                                    letmix = MAP.mix2
                                } else {
                                    letmix = MAP.mix21
                                }
                                break;
                            case 'L':
                                letmix = MAP.mix22
                                break;
                            case 'R':
                                letmix = MAP.mix17
                                break;
                            default:
                                letmix = MAP.mix4
                                break;
                        }
                        $($section[0]).css("background-image", 'url(' + letmix + ')')
                    }
                })
                break;
            case 'ahu.filter':
                nextProps.sections.map((k, index) => {
                    if (index == section.position - 1) {
                        let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
                        if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
                            $($section[0]).css("background-image", 'url(' + MAP.comb + ')')
                        }
                    }
                })
                break;
            case 'ahu.combinedFilter':
                nextProps.sections.map((k, index) => {
                    if (index == section.position - 1) {
                        let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
                        if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
                            $($section[0]).css("background-image", 'url(' + MAP.comb2 + ')')
                        }
                    }
                })
                break;
            default:
                break;
        }
    }
    setupAhuLine(nextProps) {

        if (!nextProps.layout) {
            let ahus = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
            ahus.forEach(ahu => {
                ahu.find('[data-section]').remove()
            })
            return
        }
        let ahus = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
        ahus.forEach(ahu => {
            ahu.find('[data-section]').remove()
        })
        //let parent = $(`[data-section='${nextProps.selectedComponentId}']`)
        /* parent.each((index, record)=>{
            console.log(record.getAttribute('data-section'))
        if(record.getAttribute('data-section') === 'ahu.combinedMixingChamber'){
        
        }
        }) */
        if (isSingleLine(nextProps.layout.style)) {
            nextProps.layout.layoutData[0].forEach(index => {
                let section = nextProps.sections[index - 1]
                const $section = this.getSection(section)
                $section.attr('data-section-id', section.position)
                $section.attr('data-partid', section.partid)
                $section.attr('style', 'cursor:pointer')
                nextProps.layout.layoutData[0].length > 14 && $section.attr('style', 'width:35px;height:35px')
                $section.appendTo(ahus[0])
                let XinFen
                if ($section[0]) {
                    if ($section[0].getAttribute('data-section') === 'ahu.combinedMixingChamber') {
                        XinFen = true
                    }
                }
                this.setAhuFun(nextProps, section, $section, XinFen)//zzf
            })
        } else {
            let indexs = [1, 2, 3, 4]
            indexs.forEach(index => {
                let arr = nextProps.layout ? nextProps.layout.layoutData[index - 1] : []
                arr && arr.forEach(pos => {
                    let section = nextProps.sections[pos - 1]
                    const $section = this.getSection(section)
                    $section.attr('data-section-id', section.position)
                    $section.attr('data-partid', section.partid)
                    $section.attr('style', 'cursor:pointer')
                    // $section.attr('style', 'width:20px;height:20px')
                    $section.appendTo(ahus[index])
                    let XinFen
                    if ($section[0]) {
                        if ($section[0].getAttribute('data-section') === 'ahu.combinedMixingChamber') {
                            XinFen = true
                        } else {
                            XinFen = false
                        }
                    }
                    this.setAhuFun(nextProps, section, $section, XinFen)//zzf
                })
            })
        }

        $('[data-toggle="tooltip"]').tooltip()
        if (!this.state.drake) {
            this.setState({ "drake": this.props.dragInit() })
        }
    }

    getSection(section) {
        let $section = null
        // check if it's wwk fan then use wwkFan section
        let fanOutlet = JSON.parse(section.metaJson)["meta.section.fan.outlet"];
        if(fanOutlet && fanOutlet === "wwk") {
            $section = $('#sections').find(`[data-section='${section.sectionKey}']`).last().clone()
        } else {
            $section = $('#sections').find(`[data-section='${section.sectionKey}']`).first().clone()
        }
        return $section;
    }

    componentWillReceiveProps(nextProps) {
        this.setupAhuLine(nextProps)
    }

    componentDidMount() {
        this.setupAhuLine(this.props)

    }
    /*
     * 出现CLICK_COMPONENT事件
     */
    componentDidUpdate() {
        const { onClickSection } = this.props
        onClickSection()
    }
    render() {
        const { layout, onClickSection, a } = this.props
        let twin = false
        let lstyle = STYLE_COMMON
        if (layout) {
            lstyle = layout.style
            twin = isTwinLine(lstyle)
        }
        let airDirs = {}
        let ahuCls = '';
        let nextProps = this.props
        if (!twin) {
            airDirs = {
                dir1: <div className="col-md-3"><i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow</div>,
                dir2: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-left" aria-hidden="true"></i></div>,
            }
            if (lstyle == STYLE_PLATE) {
                airDirs.dir2 = <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-left" aria-hidden="true"></i></div>;
            }
            else if (lstyle == STYLE_WHELE || lstyle == STYLE_COMMON || lstyle == STYLE_CHAMBER) {
                airDirs.dir2 = <div className="col-md-2 col-md-offset-7"><i className="fa fa-arrow-right" aria-hidden="true"></i> &nbsp;Airflow</div>;
            }

        } else {
            if (lstyle == STYLE_WHELE || lstyle == STYLE_DOUBLE_RETURN_1 ||
                    lstyle == STYLE_DOUBLE_RETURN_2 || lstyle == STYLE_SIDE_BY_SIDE_RETURN) {
                ahuCls = style.AHUWhele;
                airDirs = {
                    dir1: <div className="col-md-3"><i className="fa fa-arrow-left" aria-hidden="true"></i>&nbsp; Airflow</div>,
                    dir2: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-left" aria-hidden="true"></i></div>,
                    dir3: <div className="col-md-3"><i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow</div>,
                    dir4: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-right" aria-hidden="true"></i></div>,
                }
                // $($('#Topahu').find(`[data-section-id='2']`)).css("background-image", 'url(' + MAP.fan5 + ')')

            } else if (lstyle == STYLE_PLATE) {
                ahuCls = style.AHUPlate;
                airDirs = {
                    dir1: <div className="col-md-3"><i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow</div>,
                    dir2: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-left" aria-hidden="true"></i></div>,
                    dir3: <div className="col-md-3"><i className="fa fa-arrow-left" aria-hidden="true"></i>&nbsp; Airflow</div>,
                    dir4: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-right" aria-hidden="true"></i></div>,
                }
                // $($('#Topahu').find(`[data-section-id='6']`)).css("background-image", 'url(' + MAP.fan5 + ')')

            } else if (lstyle == STYLE_VERTICAL_1 || lstyle == STYLE_VERTICAL_2) {
                ahuCls = style.AHUPlate;
                airDirs = {
                        dir1: <div className="col-md-3"><i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow</div>,
                        dir2: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-right" aria-hidden="true"></i></div>,
                        dir3: <div className="col-md-3"><i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow</div>,
                        dir4: <div className="col-md-2 col-md-offset-7">Airflow&nbsp;<i className="fa fa-arrow-right" aria-hidden="true"></i></div>,
                    }
            }

        }







        return (
            <div className={classnames(!twin && style.AHU, twin && style.AHUTwin)}>
                <div className="container-fluid">
                    <div className="row">
                        {
                            airDirs.dir1
                        }
                        {
                            airDirs.dir2
                        }
                    </div>
                </div>
                <div className={classnames(!twin && style.AHUContainer, twin && style.AHUContainerTwin, ahuCls)} id="Topahu">
                    <div className={classnames(!twin && style.Hidden)}>
                        <div className={style.AHULineContainer01}>
                            <div id="ahutl" className={style.AHULineLeftContainer + ' ahu-section-list'}></div>
                            <div id="ahutr" className={style.AHULineRightContainer + ' ahu-section-list'}></div>
                        </div>
                        <div className={style.AHULineContainer01}>
                            <div id="ahubl" className={style.AHULineLeftContainer + ' ahu-section-list'}></div>
                            <div id="ahubr" className={style.AHULineRightContainer + ' ahu-section-list'}></div>
                        </div>
                    </div>
                    <div id="ahu" className={classnames(style.AHULineContainer, twin && style.Hidden) + ' ahu-section-list'}></div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        {
                            airDirs.dir3
                        }
                        {
                            airDirs.dir4
                        }
                    </div>
                </div>
            </div>
        )
    }
}
