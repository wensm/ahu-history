import {
    PLEASE_OPEN_IN_CHROME,
    COPY,
    OPEN,
    OPEN_IN_BROWSER,
    SYSTEM,
    DEFAULT_PARAMETER,
    METRIC,
    IMPERIAL,
    TOOL,
    BATCH_SELECTION_RESULT,
    HELP,
    BROWSING,
    BROWSINGONLINE,
    DOWNLOAD,
    DOWNLOADWORD,
    TEMPLATE_DOWNLOAD,
    TEMPLATE_NAME,
    UPGRADE,
    UPGRADESYSTEM,
    VERSION,
    SAVED_AHU_AND_SECTIONS_INFO,
    SAVE_SUCCESS,
    UNIT,
    CANCEL,
    SUBMIT,
    OPTIMALCALCULATION,
    ALLCALCULATION,
    NUMBERCALCULATION,
    FULLQUANTITY,
    TEMPLATES,
    EXPORTMODE,
    GLOBALCP,
    SAVE,
    TEMPERATURETRANSFER,
    CAL_COILFAN_LISTPRICE,
    CLOSE, INVALID, CAL_COILFAN_LISTPRICE_DESC,
    VERSION_COLON,
    PATCH_COLON
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import sweetalert from 'sweetalert'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import style from './Header.css'
import logo from '../../images/LOGObg.png'
import personImg from '../../images/person.png'
import englishImg from '../../images/englishicon.png'
import chineseImg from '../../images/chineseIcon.png'
import LanguageSetting from '../../components/LanguageSettingContainer'
import UnitSetting from '../../components/UnitSettingContainer'
import $http from '../../misc/$http'
import { hashHistory } from 'react-router'
import { isEmpty } from 'lodash'
import { Menu, Dropdown, Icon, Tooltip, Button, Modal, Form, Input, Select } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const SubMenu = Menu.SubMenu;
import { updateLanguage, updateUserUnitCodeToServer, updateUserLocaleToServer, updateUserUnitCode, SupportedLocales, updateUserMetau ,fetchUserMeta} from '../../actions/general'
import { formatValueForUnit, normalizeValueForUnit } from '../../actions/ahu'
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            href: null,
            href1: null,
            visible: false,
            loading: false
        }
    }
    componentWillMount() {
        $http.get('help/document').then(data => {
            this.setState({
                href: '/' + data.data
            })
        }),
            $http.get('tool/templatedownload').then(data => {
                this.setState({
                    href1: '/' + data.data
                })
            })
    }
    href(href) {
        hashHistory.push({
            pathname: '/worder',
            query: {
                href: href
            }
        })
    }

    openLanguageSetting() {
        $('#LanguageSetting').data("currentLanguage", "enUS")
        $('#LanguageSetting').modal('show')
    }

    openUnitSetting() {
        $('#UnitSetting').data("currentLanguage", "enUS")
        $('#UnitSetting').modal('show')
    }

    switchLangage(locale) {
        this.props.onLocaleSwitched(locale)
    }

    checkLocale(theLocale) {
        return this.props.locale == theLocale
    }

    updateUnitPreferS(unitPreferCode, unitSetting) {
        let { metaUnit, metaLayout, componentValue, propUnit } = this.props
        if (propUnit != unitPreferCode) {
            // console.log('budengyu', propUnit, unitPreferCode)
            this.props.updateUnitPreferCode(unitPreferCode, this.props.unitSetting[unitPreferCode], metaUnit, metaLayout, componentValue)
        }
    }
    updateExplorer() {
        let explore = browserHistory.getCurrentLocation();
        // console.log('zzf1', browserHistory, explore)
        let userAgent = navigator.userAgent;
        if (typeof sendNSCommand !== "function") { // check if open in JWebBrowser
            window.open(explore.hash);
        } else {//${location.host+explore.hash}
            sweetalert({
                title: intl.get(PLEASE_OPEN_IN_CHROME),
                text: `<textarea style="resize:none;border:none;width:100%;text-align:center" id="hashCopy">${location.host + explore.hash}</textarea>`,
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonText: intl.get(OPEN),
                cancelButtonText: intl.get(COPY)
            }, function (open) {
                if (open) {
                    // send command to JWebBrowser
                    sendNSCommand('chrome', location.origin + explore.hash);
                } else {
                    let copy = $('#hashCopy').select()
                    document.execCommand("Copy")
                }
            });
        }
    }
    handleClick(e) {
        this.props.onFetchUserMeta()
        if (e.key == 'item_1') {
            this.setState({ visible: true })
        }

    }
    handleMenuClick(e) {
        // console.log('e', e)
    }
    handleMenuClickHelp() {

    }
    handleMenuClickLocale() {

    }
    handleOk() {
        let params = {}
        let { user, form, onUpdateUserMeta } = this.props
        let values = form.getFieldsValue()
        let copyValue = {}
        for(let key in values){
            copyValue[key] = values[key]['key']
        }
        // params.metaData = JSON.stringify(values)
        params.metaData = copyValue
        params.userId = user.userId
        params.factoryId = user.userFactory
        this.setState({ loading: true }, () => {
            onUpdateUserMeta(params, () => { this.setState({ visible: false, loading: false }) })
        })

    }
    handleCancel() {
        this.setState({ visible: false })
    }
    handleSubmit(e) {
        // console.log('e', e)
    }




    render() {
        const { user, unitSetting, version, patchVersion, form, fieldValues } = this.props
        const { getFieldDecorator } = form
        const menu = (
            <Menu onClick={(e) => this.handleClick(e)}>

                <SubMenu title={intl.get(UNIT)}>
                    <Menu.Item>
                        <div>
                            <span style={{ paddingRight: '30px' }} onClick={() => this.updateUnitPreferS("M")} tabIndex="-1">{intl.get(METRIC)}
                                {(user.unitPreferCode == 'M') && <span
                                    className="badge badge-success pull-right"><i className="fa fa-check"
                                        aria-hidden="true"></i></span>}
                            </span>
                        </div>
                    </Menu.Item>
                    <Menu.Item>
                        <div>
                            <span style={{ paddingRight: '30px' }} onClick={() => this.updateUnitPreferS("B")}
                                tabIndex="-1">{intl.get(IMPERIAL)}{(user.unitPreferCode == 'B') && <span
                                    className="badge badge-success pull-right"><i className="fa fa-check"
                                        aria-hidden="true"></i></span>}
                            </span>
                        </div>
                    </Menu.Item>
                </SubMenu>
                <Menu.Item>{intl.get(GLOBALCP)}</Menu.Item>

            </Menu>
        );
        const menuTool = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key="1">
                    <Link to="/tool">
                        {intl.get(BATCH_SELECTION_RESULT)}
                    </Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <a style={{}} href={this.state.href1} download={intl.get(TEMPLATE_NAME)}>{intl.get(TEMPLATE_DOWNLOAD)}</a>
                </Menu.Item>
            </Menu>
        );

        const menuHELP = (
            <Menu onClick={this.handleMenuClickHelp}>
                <Menu.Item key="1">
                    <Link style={{}} onClick={() => this.href(this.state.href)}>{intl.get(BROWSINGONLINE)}</Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <a style={{}} href={this.state.href} download="pdf">{intl.get(DOWNLOADWORD)}</a>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to="/upgrade">{intl.get(UPGRADESYSTEM)}</Link>
                </Menu.Item>
                <Menu.Item key="4">
                    <Link style={{}} onClick={() => {
                        let titleString = intl.get(VERSION_COLON) + version;
                        if(patchVersion) {
                            titleString += "\n" + intl.get(PATCH_COLON) + patchVersion;
                        }
                        sweetalert({
                            title: titleString,
                            // text: intl.get(SAVED_AHU_AND_SECTIONS_INFO),
                            // timer: 2000,
                            // type: 'success',
                            showConfirmButton: true
                        })
                    }} >{intl.get(VERSION)}</Link>
                </Menu.Item>
            </Menu>
        );

        const menuLocale = (
            <Menu onClick={this.handleMenuClickLocale}>
                <Menu.Item key="1">
                    <Link onClick={() => this.switchLangage("zh-CN")}>简体中文</Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link onClick={() => this.switchLangage("en-US")}>English</Link>
                </Menu.Item>
            </Menu>
        );
        const buttonStyle = {
            marginLeft: 8,
            float: 'left',
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            color: '#fff',
            borderColor: 'rgba(0, 0, 0, 0.1)'

        }
        const formItemLayout = {
            labelCol: { span: 10 },
            wrapperCol: { span: 12 },
        }
        return (
            <div className={style.Header} style={{ position: 'fixed', zIndex: '10', width: '99%' }}>
                <Link to="/">
                    <img className="pull-left" src={logo} />
                </Link>
                <div className="pull-right" style={{ marginTop: '8px' }}>
                    <div className="pull-left"><img src={personImg} /></div>
                    <div className="pull-left">
                        <p style={{ color: '#ffffff', margin: '0 10px' }}>{user.userName}</p>
                        <p style={{ color: '#ffffff', margin: '0 10px' }}>{user.userRole}</p>
                    </div>
                    <div className="pull-left" style={{ margin: '10px 10px 0 0', marginRight: "10px" }}>
                        <i className="iconfont icon-touxiangzhankaibtn" style={{ color: '#ffffff' }}></i>
                    </div>
                </div>
                <div className="pull-right" style={{ marginTop: '16px' }}>
                    {/* <a className={style.menuBtn} style={{ "textDecoration": "none", "color": "#ebf0f9" }} target="_Blank" onClick={() => this.updateExplorer()}>
                        <i className="iconfont"></i>{intl.get(OPEN_IN_BROWSER)}
                    </a> */}
                    {/* <Button style={buttonStyle} onClick={() => this.updateExplorer()}>
                        {intl.get(OPEN_IN_BROWSER)}
                    </Button> */}

                    {/* <Dropdown overlay={menu}>
                        <a className="ant-dropdown-link" href="#">
                            Cascading menu <Icon type="down" />
                        </a>
                    </Dropdown> */}
                    <Dropdown overlay={menu}>
                        <Button style={buttonStyle}>
                            {intl.get(SYSTEM)} <Icon type="down" />
                        </Button>
                    </Dropdown>
                    {/* <div className="dropdown pull-left">
                    
                        <button className={style.sysBtn} id="sysBtn" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i className="iconfont icon-svg11"></i>{intl.get(SYSTEM)}
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" role="menu" aria-labelledby="sysBtn">
                            {/* <li><Link to="/defaultParameter">{intl.get(DEFAULT_PARAMETER)}</Link></li>
                            <li role="separator" className="divider"></li> 
                            <li><Link onClick={() => this.updateUnitPreferS("M")} tabIndex="-1">{intl.get(METRIC)}
                                {(user.unitPreferCode == 'M') && <span
                                    className="badge badge-success pull-right"><i className="fa fa-check"
                                        aria-hidden="true"></i></span>}
                            </Link></li>
                            <li><Link onClick={() => this.updateUnitPreferS("B")}
                                tabIndex="-1">{intl.get(IMPERIAL)}{(user.unitPreferCode == 'B') && <span
                                    className="badge badge-success pull-right"><i className="fa fa-check"
                                        aria-hidden="true"></i></span>}</Link>
                            </li>
                        </ul>

                    </div> */}

                    {/* <Link className={style.menuBtn} to="/tool">
                        <i className="iconfont icon-gongju"></i>{intl.get(TOOL)}
                    </Link> */}
                    <Dropdown overlay={menuTool}>
                        <Button style={buttonStyle}>
                            {intl.get(TOOL)} <Icon type="down" />
                        </Button>
                    </Dropdown>

                    {/* <div className="dropdown pull-left">
                        <button className={style.sysBtn} id="tool" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i className="iconfont icon-gongju"></i>{intl.get(TOOL)}
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="tool">
                            <li>
                                <Link to="/tool">
                                    {intl.get(BATCH_SELECTION_RESULT)}
                                </Link>
                            </li>
                            <li>
                                <a style={{}} href={this.state.href1} download={intl.get(TEMPLATE_NAME)}>{intl.get(TEMPLATE_DOWNLOAD)}</a>
                            </li>
                        </ul>
                    </div> */}

                    <Dropdown overlay={menuHELP}>
                        <Button style={buttonStyle}>
                            {intl.get(HELP)} <Icon type="down" />
                        </Button>
                    </Dropdown>

                    {/* <div className="dropdown pull-left">
                        <button className={style.sysBtn} id="worder" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i className="iconfont icon-tishi1"></i>{intl.get(HELP)}
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="worder">
                            <li><Link style={{}} onClick={() => this.href(this.state.href)}>{intl.get(BROWSING)}</Link></li>
                            <li><a style={{}} href={this.state.href} download="pdf">{intl.get(DOWNLOAD)}</a></li>
                            <li><Link to="/upgrade">{intl.get(UPGRADE)}</Link></li>
                            <li ><Link style={{}} onClick={() => {
                                sweetalert({
                                    title: `版本： ${version}`,
                                    // text: intl.get(SAVED_AHU_AND_SECTIONS_INFO),
                                    // timer: 2000,
                                    // type: 'success',
                                    showConfirmButton: true
                                })
                            }} >{intl.get(VERSION)}</Link></li>
                        </ul>
                    </div> */}

                    <Dropdown overlay={menuLocale}>
                        <Button style={buttonStyle}>
                            {this.checkLocale("zh-CN") && "简体中文"}
                            {this.checkLocale("en-US") && "English"}
                            <Icon type="down" />
                        </Button>
                    </Dropdown>

                    {/* <div className="dropdown pull-left">
                        <button className={style.sysBtn} id="sysBtn" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            {this.checkLocale("zh-CN") && "简体中文"}
                            {this.checkLocale("en-US") && "English"}
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="sysBtn">
                            <li><Link onClick={() => this.switchLangage("zh-CN")}>简体中文</Link></li>
                            <li><Link onClick={() => this.switchLangage("en-US")}>English</Link></li>
                        </ul>
                    </div> */}
                    <a className={style.seach}>
                        <i className="iconfont icon-sousuoicon"></i>

                    </a>
                    <LanguageSetting flg="languageSetting"></LanguageSetting>
                    <UnitSetting flg="unitSetting"></UnitSetting>
                </div>
                <Modal
                    className="globalConfigurationParameters"
                    visible={this.state.visible}
                    // title="全局配置参数"
                    title={intl.get(GLOBALCP)}
                    onOk={() => this.handleOk()}
                    onCancel={() => this.handleCancel()}
                    footer={[
                        <Button key="back" onClick={() => this.handleCancel()}>{intl.get(CANCEL)}</Button>,
                        <Button key="submit" type="primary" loading={this.state.loading} onClick={() => this.handleOk()}>
                            {intl.get(SAVE)}
                        </Button>,
                    ]}
                >
                    <Form layout="horizontal" onSubmit={() => this.handleSubmit()}>
                        <FormItem {...formItemLayout} label={intl.get(NUMBERCALCULATION)}>
                            {getFieldDecorator('COLD_COOLING_SELECTION_SIZE', {
                                // defaultValue: fieldValues['COLD_COOLING_SELECTION_SIZE'] ? fieldValues['COLD_COOLING_SELECTION_SIZE']['value'] : '',
                                // rules: [{ required: true, message: 'Please input your username!' }],
                            })
                                (<Select labelInValue style={{ width: 200 }}>
                                    <Option value={'5'}>{intl.get(OPTIMALCALCULATION)}</Option>
                                    <Option value={'100'}>{intl.get(ALLCALCULATION)}</Option>
                                </Select>)
                            }
                        </FormItem>
                        <FormItem {...formItemLayout} label={intl.get(EXPORTMODE)}>
                            {getFieldDecorator('EXPORT_MODE', {
                                // defaultValue: fieldValues['COLD_COOLING_SELECTION_SIZE'] ? fieldValues['COLD_COOLING_SELECTION_SIZE']['value'] : '',
                                // rules: [{ required: true, message: 'Please input your username!' }],
                            })
                                (<Select labelInValue style={{ width: 200 }}>
                                    {/*<Option value={'0'}>{intl.get(FULLQUANTITY)}</Option>*/}
                                    <Option value={'1'}>{intl.get(TEMPLATES)}</Option>
                                </Select>)
                            }
                        </FormItem>
                        <FormItem {...formItemLayout} label={intl.get(TEMPERATURETRANSFER)}>
                            {getFieldDecorator('TEMPERATURE_TRANSMIT', {
                                // defaultValue: fieldValues['COLD_COOLING_SELECTION_SIZE'] ? fieldValues['COLD_COOLING_SELECTION_SIZE']['value'] : '',
                                // rules: [{ required: true, message: 'Please input your username!' }],
                            })
                                (<Select labelInValue style={{ width: 200 }}>
                                    <Option value={'true'}>{intl.get(OPEN)}</Option>
                                    <Option value={'false'}>{intl.get(CLOSE)}</Option>
                                </Select>)
                            }
                        </FormItem>
                        <FormItem {...formItemLayout} label={intl.get(CAL_COILFAN_LISTPRICE)}>
                            {getFieldDecorator('CAL_COILFAN_LISTPRICE', {
                                // defaultValue: fieldValues['CAL_COILFAN_LISTPRICE'] ? fieldValues['CAL_COILFAN_LISTPRICE']['value'] : '',
                                // rules: [{ required: true, message: 'Please input your username!' }],
                            })
                            (<Select labelInValue style={{ width: 200 }}>
                                <Option value={'true'}>{intl.get(OPEN)}</Option>
                                <Option value={'false'}>{intl.get(CLOSE)}</Option>
                            </Select>)
                            }
                            <Tooltip title={intl.get(CAL_COILFAN_LISTPRICE_DESC)} placement="right">
                                <Icon type="question-circle" />
                            </Tooltip>
                        </FormItem>

                    </Form>

                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let currentLocale = intl.determineLocale({
        urlLocaleKey: "lang",
        cookieLocaleKey: "lang"
    })
    if (!_.find(SupportedLocales, { value: currentLocale })) {
        currentLocale = state.general.user.preferredLocale
    }
    return {
        locale: currentLocale,
        user: state.general.user,
        version: state.metadata.version,
        patchVersion: state.metadata.patchVersion,
        propUnit: state.general.user && state.general.user.unitPreferCode,
        unitSetting: state.metadata.metaUnitSetting,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
        componentValue: state.ahu.componentValue,
        fieldValues: state.general.fieldValues

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLocaleSwitched: (locale) => {
            dispatch(updateUserLocaleToServer(locale))
        },
        updateUnitPreferCode: (preferCode, unitSetting, metaUnit, metaLayout, componentValue) => {
            dispatch(updateUserUnitCode(preferCode, unitSetting))
            dispatch(updateUserUnitCodeToServer(preferCode, unitSetting))
            setTimeout(() => {
                window.location.reload();
            }, 1000);

            // console.log('zzf4', preferCode, unitSetting, metaUnit, metaLayout, componentValue)
            if (preferCode == 'M') {//M 是英制切到公制
                for (let key in componentValue) {
                    for (let _key in componentValue[key]) {
                        if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            // console.log('zzflog', _key, componentValue[key][_key], unitSetting, metaUnit, metaLayout)
                            let a = normalizeValueForUnit(_key, componentValue[key][_key], unitSetting, metaUnit, metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            componentValue[key][_key] = a
                        }

                    }
                }
            } else {
                // formatValueForUnit(id.replace(/\./gi, '_'), value,  unitSetting, metaUnit, metaLayout)
                for (let key in componentValue) {
                    for (let _key in componentValue[key]) {
                        if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            let b = formatValueForUnit(_key, componentValue[key][_key], unitSetting, metaUnit, metaLayout)
                            // console.log('zzf6',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , b)

                            componentValue[key][_key] = b
                        }

                    }
                }
            }
        },
        onChangeFieldValues: (values) => {
            // console.log('values', values)
        },
        onUpdateUserMeta: (params, callback) => {
            dispatch(updateUserMetau(params, callback))
        },
        onFetchUserMeta:()=>{
            dispatch(fetchUserMeta())
        }

    }
}

const WrappedHeader = Form.create({
    onFieldsChange(props, changedFields) {
        // console.log('props, changedFields', props, changedFields)
        props.onChangeFieldValues({ ...props.fieldValues, ...changedFields });
    },
    mapPropsToFields(props) {
        // return props.fieldValues
        return {
            COLD_COOLING_SELECTION_SIZE: Form.createFormField({
                ...props.fieldValues.COLD_COOLING_SELECTION_SIZE,
                value: props.fieldValues.COLD_COOLING_SELECTION_SIZE ? {key:props.fieldValues.COLD_COOLING_SELECTION_SIZE.value} : '',
            }),
            EXPORT_MODE: Form.createFormField({
                ...props.fieldValues.EXPORT_MODE,
                value: props.fieldValues.EXPORT_MODE ? {key:props.fieldValues.EXPORT_MODE.value} : '',
            }),
            TEMPERATURE_TRANSMIT: Form.createFormField({
                ...props.fieldValues.TEMPERATURE_TRANSMIT,
                value: props.fieldValues.TEMPERATURE_TRANSMIT ? {key:props.fieldValues.TEMPERATURE_TRANSMIT.value} : '',
            }),
            CAL_COILFAN_LISTPRICE: Form.createFormField({
                ...props.fieldValues.CAL_COILFAN_LISTPRICE,
                value: props.fieldValues.CAL_COILFAN_LISTPRICE ? {key:props.fieldValues.CAL_COILFAN_LISTPRICE.value} : '',
            }),
        }
    }
})(Header);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedHeader)
