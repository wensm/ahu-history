import {
    PROMPT_DRAG_OUT_SECTION_TO_DELETE,
    PLEASE_SELECT_THE_UNIT_MODEL_FIRST,
    ALERT_COLON,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import classnames from 'classnames'
import DateUtils from '../../misc/DateUtils'

export default class AHUTips extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { validation, ahuSerial, defaultMessage, defaultType = true, panelConfirmTip } = this.props
        let type = 'alert-info'
        let msg = intl.get(PROMPT_DRAG_OUT_SECTION_TO_DELETE)
        if (defaultMessage) {
            msg = defaultMessage
        }

        if (!validation.pass) {
            type = 'alert-danger animated flash infinite'
            msg = intl.get(ALERT_COLON) + `${validation.msg}`
        } else if (validation.pass && validation.msg != '') {
            msg = msg + `${validation.msg}`

        }

        if (!validation.sectionPass || validation.sectionWarn) {
            type = 'alert-danger animated flash infinite'
            msg = intl.get(ALERT_COLON) + `${validation.sectionMsg}`
        }

        if (!ahuSerial) {
            type = 'alert-danger animated flash infinite'
            msg = intl.get(ALERT_COLON) + intl.get(PLEASE_SELECT_THE_UNIT_MODEL_FIRST)

        }

        // console.log("render ahu tips zzf", msg, defaultMessage);


        if (defaultType) {
        } else {
            type = 'alert-danger animated flash infinite'
        }

        return (
            <div data-name="AHUTips" style={{ marginBottom: '4px' }}>
                <div ref={(ele) => {
                    if ($(ele).hasClass("infinite")) {
                        setTimeout(() => {
                            $(ele).removeClass("infinite", 3000)
                        })
                    }
                }} className={classnames('alert', type)} role="alert"
                    style={{ padding: '1px 10px', marginBottom: '0px' }}>
                    <div>{msg}</div>
                    {panelConfirmTip && panelConfirmTip.map((e) => {
                        let str = `段${e.show} 已于${e.time} 保存`
                        return (
                            <div>
                                {str}
                            </div>
                        )

                    })}

                </div>
            </div>
        )
    }
}
