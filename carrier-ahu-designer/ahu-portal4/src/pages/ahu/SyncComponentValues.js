import {
  CURRENT_EDIT_PROPERTY_NAME,
  NEW_VALUE,
  OLD_VALUE,
  SECTION_BASIC_DATA,
  MIX_FORM_CHOOSE_TWO_AT_MOST,
  AIR_VOLUME,
} from '../intl/i18n'

import intl from 'react-intl-universal'
// name: {intl.get(CURRENT_EDIT_PROPERTY_NAME)}
// newValue：{intl.get(NEW_VALUE)}
// previousValue: {intl.get(OLD_VALUE)}
// selectedComponent:当前选中的AHU或SECTION的id和name
// componentValues:当前AHU和所有section的属性值
// ahus: 当前AHU下段的排列顺序
// baseSections：{intl.get(SECTION_BASIC_DATA)}

export default function synccomponentValues(name, newValue, previousValue, selectedComponent, componentValues, ahus, baseSections, state) {
  switch (true) {
    case name === 'meta_ahu_sairvolume'://送风
      syncFanAirVolumeByAhu(componentValues, name)
      break;
    case name === 'meta_ahu_eairvolume'://回风
      syncFanAirVolumeByAhu(componentValues, name, state)
      break;
    case selectedComponent.name === 'ahu.mix':
      mixType(componentValues, selectedComponent, name)
      break
    case name === 'ns_ahu_nsheight':
      syncValue(componentValues, name)
      break;
    case name === 'ns_ahu_nswidth':
      syncValue(componentValues, name)
      break;
    // case name === 'meta_section_fan_supplier':
    //   linkageValue(componentValues, newValue, name)
    //   break;
    case name === 'meta_ahu_sexternalstatic'://静压送
      syncFanAirVolumeByAhu(componentValues, name)
      break;

    default:

  }
  return componentValues
}

function linkageValue(componentValues, newValue, name) {
  let config = {
    'default': 1,
    'Dongyuan': 1,
    'ABB': 2,
    'Simens': 2
  }
  for (let variable in componentValues) {
    if (componentValues.hasOwnProperty(variable)) {
      for (let key in componentValues[variable]) {
        if (componentValues[variable].hasOwnProperty(key) && key == 'meta_section_fan_type') {
          componentValues[variable][key] = config[newValue]

        }
      }
    }
  }
}

function syncValue(componentValues, name) {
  for (var variable in componentValues) {
    if (componentValues.hasOwnProperty(variable)) {
      const componentValue = componentValues[variable]
      for (var variable in componentValue) {
        if (componentValue.hasOwnProperty(variable)) {
          if (name === 'ns_ahu_nsheight' && variable === 'ns_ahu_nsheight') {
            let param1 = componentValue['ns_ahu_nsheight'] ? componentValue['ns_ahu_nsheight'] : ''
            let param2 = componentValue['ns_ahu_nswidth'] ? componentValue['ns_ahu_nswidth'] : ''
            componentValue['ns_ahu_deformationSerial'] = `${param1}${param2}`
          }
          if (name === 'ns_ahu_nswidth' && variable === 'ns_ahu_nswidth') {
            let param1 = componentValue['ns_ahu_nsheight'] ? componentValue['ns_ahu_nsheight'] : ''
            let param2 = componentValue['ns_ahu_nswidth'] ? componentValue['ns_ahu_nswidth'] : ''
            componentValue['ns_ahu_deformationSerial'] = `${param1}${param2}`
          }
        }
      }
    }
  }
}

// {intl.get(MIX_FORM_CHOOSE_TWO_AT_MOST)}
function mixType(componentValues, selectedComponent, name) {
  const mixValues = componentValues[selectedComponent.id]
  const props = [
    'meta_section_mix_returnTop',
    'meta_section_mix_returnBack',
    'meta_section_mix_returnLeft',
    'meta_section_mix_returnRight',
    'meta_section_mix_returnButtom',
  ]
  let counter = 0
  props.forEach(prop => {
    if (mixValues[prop] == 'true' || mixValues[prop] === true) {
      counter++
    }
  })
  if (props.find(prop => prop === name) && counter > 2) {
    let mark = true
    props.forEach(prop => {
      if (mark && prop !== name && componentValues[selectedComponent.id][prop]) {
        componentValues[selectedComponent.id][prop] = false
        mark = false
      }
    })
  }
}

// 同步AHU 风量{intl.get(AIR_VOLUME)}和风机{intl.get(AIR_VOLUME)}
function syncFanAirVolumeByAhu(componentValues, name, state) {
  for (var variable in componentValues) {
    if (componentValues.hasOwnProperty(variable)) {
      const componentValue = componentValues[variable]
      for (var variable in componentValue) {
        if (componentValue.hasOwnProperty(variable)) {
          if (variable === 'meta_section_fan_airVolume' && name === 'meta_ahu_sairvolume') {
            componentValue[variable] = componentValues[0].meta_ahu_sairvolume
          }
          if (variable === 'meta_section_wheelHeatRecycle_NAVolume' && name === 'meta_ahu_sairvolume') {//转轮热回收--新风量(m3/h)
            componentValue[variable] = componentValues[0].meta_ahu_sairvolume
            componentValue['meta_section_wheelHeatRecycle_RAVolume'] = componentValues[0].meta_ahu_sairvolume
            
          }

          if (variable === 'meta_section_plateHeatRecycle_NAVolume' && name === 'meta_ahu_sairvolume') {//板式热回收--新风量(m3/h)
            componentValue[variable] = componentValues[0].meta_ahu_sairvolume
            componentValue['meta_section_plateHeatRecycle_RAVolume'] = componentValues[0].meta_ahu_sairvolume
            
          }
          // if (variable === 'meta_section_wheelHeatRecycle_RAVolume' && name === 'meta_ahu_eairvolume') {//转轮热回收--回风量(m3/h)
          //   componentValue[variable] = componentValues[0].meta_ahu_eairvolume
          // }
          if (name === 'meta_ahu_eairvolume' && componentValue['meta_section_airDirection'] == 'R') {//修改回风的时候 同时修改风机（回风风机R）的风量
            componentValue['meta_section_fan_airVolume'] = componentValues[0].meta_ahu_eairvolume
          }
          // if (variable === 'meta_ahu_evelocity' && name === 'meta_ahu_sairvolume') {//修改送风时同时修改 风速（回）
          //   componentValue[variable] = componentValues[0].meta_ahu_sairvolume
          // } 
          if (variable === 'meta_ahu_evelocity' && name === 'meta_ahu_eairvolume') {//修改回风时同时修改 风速（回）
            if (state.series.length > 0) {
              let serial = state.componentValue[0]['meta_ahu_serial']
              let serialCurrent = state.series.find((sery) => serial == sery.ahu)
              if (serialCurrent) {
                componentValue[variable] = (Number(componentValues[0].meta_ahu_eairvolume) / (serialCurrent.coilFaceArea * 3600)).toFixed(3)
              }
            }
          }
          if (name === 'meta_ahu_sairvolume' && variable === 'meta_ahu_eairvolume') {//修改送风的时候 同时修改回风
            componentValue[variable] = componentValues[0].meta_ahu_sairvolume
          }
          if (name === 'meta_ahu_sexternalstatic' && variable === 'meta_ahu_eexternalstatic') {//修改机外静压送的时候 同时机外静压回
            componentValue[variable] = componentValues[0].meta_ahu_sexternalstatic
          }

        }
      }
    }
  }
}
