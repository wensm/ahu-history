import React from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import SeriesModal from './SeriesModal'
import { selectAhuSeries } from '../../actions/ahu'

const mapStateToProps = state => {
  return {
    series: state.ahu.series,
    selectedSeries: state.ahu.componentValue[0] && state.ahu.componentValue[0].meta_ahu_serial,
    sairvolume: state.ahu.componentValue[0] && state.ahu.componentValue[0].meta_ahu_sairvolume,
    unitPreferCode: state.general.user.unitPreferCode,
    isExport: state.general.isExport,
    metaAhuDelivery: state.ahu.componentValue[0] && state.ahu.componentValue[0].meta_ahu_delivery,
    
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSelectAhuSeries: series => {
      dispatch(selectAhuSeries(series))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeriesModal)
