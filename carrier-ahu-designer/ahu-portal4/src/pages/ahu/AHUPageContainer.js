import {
    WILL_SAVE_ALL_AHU_INFO,
    SECTION_ORDER_CHANGED_AHU_MOVE_UNCLASSIFIED,
    OK_TO_SAVE_IT,
    OK,
    CANCEL
} from '../intl/i18n'

import { connect } from 'react-redux'
import intl from 'react-intl-universal'
import sweetalert from 'sweetalert'
import AHUPage from './AHUPage'
import { validateSection } from './ChainCalculator'
import {
    fetchAhuInfo,
    fetchAhuSections,
    clickComponent,
    addAhuComponent,
    removeAhuSection,
    fetchAhuSectionsValidation,
    dropSection,
    saveAhu,
    confirmSection,
    beforeConfirmSection,
    isSingleLine,
    isTwinLine,
    syncDirection,
    getSimpleSectionArr,
    getComplexSectionArr,
    calculateUnitValueToMetric2,
    cleanAhuInfo,
    calculateNsPrice
} from '../../actions/ahu'
import { fetchMetaValidation, fetchSerial, fetchLayout } from '../../actions/metadata'

const mapStateToProps = (state, ownProps) => {
    return {
        projectId: ownProps.params.projectId,
        ahuId: ownProps.params.ahuId,
        currentAHUSections: state.ahu.currentAHUSections,
        componentId: state.ahu.componentId,
        currentTemplates: state.ahu.currentAhuTemplates,
        componentValue: state.ahu.componentValue,
        selectedComponent: state.ahu.selectedComponent,
        templates: state.ahu.templates,
        formValues: state.form.component && state.form.component.values,
        sections: state.ahu.sections,
        layout: state.ahu.layout,
        canDropList: state.metadata.canDropList,
        psychometricModalState: state.ahu.PsychometricModalState,

        unitSetting: state.metadata.metaUnitSetting,
        propUnit: state.general.user && state.general.user.unitPreferCode,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
        doToolTip: state.ahu.doToolTip,
        preferredLocale: state.general.user && state.general.user.preferredLocale,
        isExport: state.general.isExport
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // onSyncDirection:()=>{
        // dispatch(syncDirection())
        // },
        onSyncComponentValues: (direction, propUnit) => {
            dispatch(dropSection(direction, propUnit))
        },
        onClickSection: (e, formValues) => {
            const id = e.target.getAttribute('data-section-id')
            const name = e.target.getAttribute('data-section')
            const displayName = e.target.getAttribute('data-original-title')
            if (id) {
                dispatch(clickComponent(id, name, displayName, formValues))
                $(`[data-section]`).removeAttr('data-section-selected')
                $(`[data-section-id='${id}']`).attr('data-section-selected', true)
                if ('ahu.wheelHeatRecycle' == name || "ahu.plateHeatRecycle" == name) {
                    $(`[data-section='${name}']`).attr('data-section-selected', true)
                }
            } else {
                dispatch(clickComponent(0, 'ahu.ahu', displayName, formValues))
                $(`[data-section]`).removeAttr('data-section-selected')
            }
        },
        onFetchAhuInfo: (ahuId, unitSetting, propUnit, metaUnit, metaLayout) => {

            dispatch(cleanAhuInfo())//清除之前段信息

            dispatch(fetchAhuInfo(ahuId, unitSetting, propUnit, metaUnit, metaLayout, () => {

                dispatch(fetchMetaValidation(ahuId))
                // dispatch(fetchSerial(ahuId))// zzf 型号和段联动
            }))

            // dispatch(fetchLayout(ahuId, () => {
            //     dispatch(fetchAhuInfo(ahuId, unitSetting, propUnit, metaUnit, metaLayout, ()=>{
            //         dispatch(fetchSerial(ahuId))// zzf 型号和段联动
            //     }))
            // }))//后端excel的详细信息


            // setTimeout(() => {
            //     dispatch(fetchMetaValidation(ahuId))//该方法不做延迟，多语言就获取不到
            //     dispatch(fetchAhuInfo(ahuId, unitSetting, propUnit, metaUnit, metaLayout))
            // }, 2000);

        },
        onFetchAhuSections: ahuId => {
            setTimeout(() => {
                dispatch(fetchAhuSections(ahuId))
            }, 500);
        },
        onDropSection: (id, name, displayName, formValues) => {
            dispatch(clickComponent(id, name, displayName, formValues))
            $(`[data-section]`).removeAttr('data-section-selected')
            $(`[data-section-id='${id}']`).attr('data-section-selected', true)
        },
        onAddNewSection: (sectionName, projectId, ahuId, propUnit) => {
            dispatch(addAhuComponent(sectionName, projectId, ahuId, propUnit))
        },
        onRemoveAhuSection: (sectionInfo) => {
            dispatch(clickComponent(0, 'ahu.ahu', null, null))
            dispatch(removeAhuSection(sectionInfo.sectionId, sectionInfo.lineIndex, sectionInfo.sectionName, sectionInfo.direction, sectionInfo.propUnit))
            $(`[data-section]`).removeAttr('data-section-selected')
        },
        onValidateAhuSections: (componentValue, selectedComponent) => {
            const sections = []
            $('.ahu-section-list').children().each((index, element) => {
                const $element = $(element)
                const section = {
                    fullName: $element.attr('data-section'),
                    cnName: $element.attr('data-original-title'),
                    position: index + 1,
                }
                sections.push(section)
            })
            dispatch(fetchAhuSectionsValidation(validateSection(undefined, componentValue, selectedComponent, true)))
        },
        onMarkAhu: (ahuId) => {

        },
        onCompleteSection: (theProps, unitPreferCode, unitSetting, metaUnit, metaLayout, isNs = false) => {
            //    console.log('zzf2', theProps.preferredLocale)
            let copythePropsValues = { ...theProps.componentValues }
            let validateSections = validateSection(undefined, theProps.componentValue, theProps.selectedComponent, true, theProps.layout)
            validateSections = JSON.stringify(validateSections)
            let id2 = theProps.selectedComponent.id
            let sections2 = getSimpleSectionArr();
            // console.log('zzf4', sections2)
            // getComplexSectionArr();
            let direction = 'S'
            sections2.forEach((re) => {
                if (re.id == id2) direction = re.direction
            })
            // let ahus2 = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
            // let index2 = 0
            // let type2 = 'common'
            // ahus2.forEach((ahu, index) => {
            //     console.log('zzf3', ahu.find(`[data-section-id='${id2}']`).length, index)
            //      if(ahu.find(`[data-section-id='${id2}']`).length == 1){
            //         index2 = index
            //      }
            // })
            //下面参数改成对应的公英制
            //theProps.componentValues
            if (theProps.propUnit == 'B') {//B 是 英制,需要切到公制传给后端
                for (let key in copythePropsValues) {
                    let currentSave = { ...copythePropsValues[key] }
                    for (let _key in currentSave) {
                        if (theProps.metaLayout[_key.replace(/\_/gi, '.')] && (theProps.metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || theProps.metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            let a = calculateUnitValueToMetric2(_key, currentSave[_key], theProps.unitSetting['B'], theProps.metaUnit, theProps.metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            currentSave[_key] = a
                        }
                    }
                    copythePropsValues[key] = currentSave
                }
            }
            const ahu = copythePropsValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)

            // const allSections = copythePropsValues
            // const newAhu = {}
            // for (var variable in ahu) {
            //     if (ahu.hasOwnProperty(variable)) {
            //         newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
            //     }
            // }
            // const ahuStr = JSON.stringify(newAhu)

            const sections = []
            let allSections = []
            //Get the section being edited.
            let selectedComponentId = theProps.selectedComponent.id;
            let sectionProps = copythePropsValues[selectedComponentId]
            // 转换属性中的_ 到 . , 作为参数，传递给后台
            const newSectionProps = {}
            for (var variable in sectionProps) {
                if (sectionProps.hasOwnProperty(variable)) {
                    newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                }
            }

            let secMetaJson = JSON.stringify(newSectionProps)

            for (let _key in copythePropsValues) {
                if (_key != 0) {
                    let sectionProps2 = copythePropsValues[_key]
                    // 转换属性中的_ 到 . , 作为参数，传递给后台
                    const newSectionProps2 = {}
                    for (let variable in sectionProps2) {
                        if (sectionProps2.hasOwnProperty(variable)) {
                            newSectionProps2[variable.replace(/_/gi, '.')] = sectionProps2[variable]
                        }
                    }

                    let asecMetaJson = JSON.stringify(newSectionProps2)
                    let element2 = $(`[data-section-id='${_key}']`)[0]
                    let allSection = {
                        pid: theProps.projectId,
                        unitid: theProps.ahuId,
                        key: $(element2).attr('data-section'),
                        position: _key,
                        metaJson: asecMetaJson,
                        partid: $(element2).attr('data-partid'),
                    }
                    allSections.push(allSection)
                }
            }
            let element = $(`[data-section-id='${selectedComponentId}']`)[0]
            let section = {
                pid: theProps.projectId,
                unitid: theProps.ahuId,
                key: $(element).attr('data-section'),
                position: selectedComponentId,
                metaJson: secMetaJson,
                partid: $(element).attr('data-partid'),
            }
            sections.push(section)
            const sectionStr = JSON.stringify(sections)
            const allSectionsStr = JSON.stringify(allSections)
            const layoutStr = JSON.stringify(theProps.layout);
            //3,4,7个参数
            dispatch(confirmSection(theProps.projectId, theProps.ahuId, ahuStr, sectionStr, selectedComponentId, direction, allSectionsStr, validateSections, theProps.selectedComponent.name, unitPreferCode, unitSetting, metaUnit, metaLayout, isNs, layoutStr))
            // dispatch(beforeConfirmSection(theProps.projectId, theProps.ahuId, ahuStr, sectionStr, selectedComponentId, direction, allSectionsStr, validateSections, theProps.selectedComponent.name, unitPreferCode, unitSetting, metaUnit, metaLayout, isNs, theProps.preferredLocale))
        },
        onCalculateNsPrice: (param1, param2, value, all) => {

            // console.log('param1, param2, value, all', param1, param2, value, all)
            let ahu = all.componentValues[0]
            let newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            newAhu = JSON.stringify(newAhu)
            setTimeout(() => {
                dispatch(calculateNsPrice(newAhu, all.ahuId))
            }, 100);

        },
        onSaveAhu: (projectId, ahuId, componentValue, sectionsNum, layout, bool = true, unitSetting, metaUnit, metaLayout, propUnit, callBack, standard) => {
            let copythePropsValues = { ...componentValue }
            // console.log('zzf222',  projectId, ahuId, componentValue, sectionsNum, layout, bool = true, unitSetting, metaUnit, metaLayout, propUnit)
            if (propUnit == 'B') {//B 是 英制,需要切到公制传给后端
                for (let key in copythePropsValues) {
                    let currentSave = { ...copythePropsValues[key] }
                    for (let _key in currentSave) {
                        if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            let a = calculateUnitValueToMetric2(_key, currentSave[_key], unitSetting['B'], metaUnit, metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            currentSave[_key] = a
                        }
                    }
                    copythePropsValues[key] = currentSave
                    // console.log('zzff', currentSave, componentValue)
                }
            }
            let nlayout = {}
            nlayout.style = layout.style ? layout.style : 10
            nlayout.layoutData = []
            let ahuDs = []
            // console.log('nlayout', layout)
            if (isSingleLine(nlayout.style)) {
                ahuDs.push($('#ahu'))
            } else if (isTwinLine(nlayout.style)) {
                ahuDs.push($('#ahutl'))
                ahuDs.push($('#ahutr'))
                ahuDs.push($('#ahubl'))
                ahuDs.push($('#ahubr'))
            }

            let ahus = []
            let sections = []
            let pos = 1
            let groupId = 0
            ahuDs.forEach((ahud, index) => {
                nlayout.layoutData.push([])
                ahud.children().each((index, element) => {
                    ahus.push($(element).attr('data-section-id'))
                    const sectionProps = copythePropsValues[$(element).attr('data-section-id')]
                    const newSectionProps = {}
                    for (var variable in sectionProps) {
                        if (sectionProps.hasOwnProperty(variable)) {
                            newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                        }
                    }
                    const section = {
                        pid: projectId,
                        unitid: ahuId,
                        key: $(element).attr('data-section'),
                        position: pos,
                        metaJson: JSON.stringify(newSectionProps),
                        partid: $(element).attr('data-partid'),
                    }
                    nlayout.layoutData[groupId].push(pos)
                    sections.push(section)
                    pos++

                })
                groupId++
            })

            // let text = intl.get(WILL_SAVE_ALL_AHU_INFO)
            // if (sectionsNum === ahus.length) {
            //     const ahuIdStr = ahus.join()
            //     if (!'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30'.includes(ahuIdStr)) {
            //         text = intl.get(SECTION_ORDER_CHANGED_AHU_MOVE_UNCLASSIFIED)
            //     }
            // } else {
            //     text = intl.get(SECTION_ORDER_CHANGED_AHU_MOVE_UNCLASSIFIED)
            // }

            const ahu = copythePropsValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)
            const sectionStr = JSON.stringify(sections)
            dispatch(saveAhu(projectId, ahuId, ahuStr, sectionStr, JSON.stringify(nlayout), bool, unitSetting, metaUnit, metaLayout, propUnit, callBack, standard))

            // edit by zzf 点击保持后取消提示框（确定保存吗？）直接走保持的接口
            // sweetalert({
            //     title: intl.get(OK_TO_SAVE_IT),
            //     text,
            //     type: 'warning',
            //     showCancelButton: true,
            //     confirmButtonText: intl.get(OK),
            //     cancelButtonText: intl.get(CANCEL),
            //     closeOnConfirm: true
            // }, isConfirm => {
            //     if (isConfirm) {
            //         const ahu = copythePropsValues[0]
            //         const newAhu = {}
            //         for (var variable in ahu) {
            //             if (ahu.hasOwnProperty(variable)) {
            //                 newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
            //             }
            //         }
            //         const ahuStr = JSON.stringify(newAhu)
            //         const sectionStr = JSON.stringify(sections)
            //         dispatch(saveAhu(projectId, ahuId, ahuStr, sectionStr, JSON.stringify(nlayout)))
            //     }
            // })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUPage)
