import {
    RESTORE,
    ENLARGE,
    CLOSE,
    PSYCHROMETRIC_CHART,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import loadingImg from '../../images/loding.gif'

class PsychometricModal extends React.Component {

  constructor(props){
    super(props)

    this.toggleExpand = this.toggleExpand.bind(this)

    this.state={
      expanded:false
    }
  }

  toggleExpand(){
    this.setState({
      expanded :!this.state.expanded
    })
  }

  render() {
    const { psychometricPath } = this.props
    return (
      <div className="modal fade" id="psychometricModal" tabIndex="-1" role="dialog" aria-labelledby="psychometricModal">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div className="modal-footer" style={{padding: "0px",border:"none",paddingRight: "24px",float: "right"}}>
              <button type="button" className="btn btn-default" onClick={this.toggleExpand}> {this.state.expanded?intl.get(RESTORE):intl.get(ENLARGE)}</button>
              <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
            </div>
              <h4 className="modal-title" id="psychometricModal">{intl.get(PSYCHROMETRIC_CHART)}</h4>
            </div>
            <div className="modal-body">
              <div style={{overflow:'auto',textAlign:'center'}}>
                {psychometricPath ? <img style={{"height": (this.state.expanded?window.innerHeight:window.innerHeight-220),"width": (this.state.expanded?window.innerHeight:window.innerHeight-220),display:"inline"}} src={psychometricPath} className="img-responsive" />
                :<img src={loadingImg} style={{position:'absolute',width:'30px',top:'0',left:'45%'}}/>}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
      psychometricPath: state.ahu.psychometricPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // onTodoClick: id => {
    //   dispatch(toggleTodo(id))
    // }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PsychometricModal)
