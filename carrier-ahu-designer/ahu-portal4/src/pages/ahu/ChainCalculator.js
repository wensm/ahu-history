import {
    LABEL_WWK_FAN_SECTION
} from '../intl/i18n'
import intl from 'react-intl-universal'
import {
    AHU_FLOW_ATTR_ARR, RESISTANCE_ATTR_ARR, getSimpleSectionArr, confirmSection,
    STYLE_DOUBLE_RETURN_1, STYLE_DOUBLE_RETURN_2, STYLE_SIDE_BY_SIDE_RETURN
} from '../../actions/ahu';

/**
 * 温度链的计算，当段排序变化，或者温度相关属性发生变化的时候，会影响后续的段
 *
 * @param sections
 * @param componentValue
 * @param selectedComponent 值有两种结构：case1: 删除节点时 case2: 添加节点时 case3: 参数变化时
 * @param functionID 操作类型，1为确认
 * @param operation 操作， drop为拖动，remove为删除
 * @param direction 当前段所在风向R或者S，COMBINE为新回排
 * @param isNS   true是非标， false是标准
 */
export default function calculate(sections, componentValue, selectedComponent, functionID, operation, direction, templateId, layout, isNS) {
    // console.log('zzf0.9', sections, componentValue, selectedComponent, functionID, operation, direction)

    // template_status_Calculate(sections, componentValue, selectedComponent, functionID);
    // 如果是删除段，delPos会有一个具体的值
    let delPos = selectedComponent.assis_section_position ? selectedComponent.assis_section_position - 1 : 999;
    // console.log('delPos', delPos)
    let selectedPosition = componentValue[selectedComponent.id] ? componentValue[selectedComponent.id].assis_section_position - 1 : 999;
    let {
        combinedMixingChamberIndexes,//新回排段分析
        combinedMixingChamberIndex,
        heatRecycleIndexes,//双层热回
        findWheelHeatRecycle,//是否有轮式
    } = getIsHasCombinedAndWheelHeatRecycle(sections, templateId)
    let processSectionList2 = validateSection(sections, componentValue, selectedComponent, false, layout);
    let line1EndIndex = -1, line2FirstIndex = -1;

    sections.forEach((section, i) => {
        const currentSection = componentValue[section.id];
        currentSection.assis_section_position = i + 1;
        if (section.line === 0) {
            line1EndIndex = i;
        }
    });
    if (line1EndIndex !== -1) {
        line2FirstIndex = line1EndIndex + 1;
    }
    let processSectionList = ''
    if (selectedComponent.name != "ahu.fan") {

        processSectionList = getProcessSectionList(componentValue, sections, heatRecycleIndexes, findWheelHeatRecycle, functionID, selectedPosition, line1EndIndex, line2FirstIndex, delPos, combinedMixingChamberIndex, selectedComponent, templateId);//添加方法
    }
    // console.log('processSectionList', processSectionList)


    if (operation == 'remove') {//删除段
        componentValue = cleanSelected(componentValue, processSectionList2, sections, 'remove', direction, selectedComponent, isNS)
        componentValue = calculateResistance(componentValue, processSectionList2, combinedMixingChamberIndexes, sections)//累加阻力


    } else if (operation == 'drop') {//拖动段，包括添加和移动
        if (selectedComponent.name == "ahu.fan") {//拖动或添加的是风机段
            componentValue = calculateResistance(componentValue, processSectionList2, combinedMixingChamberIndexes, sections)//累加阻力
        } else {
            componentValue = cleanSelected(componentValue, processSectionList2, sections, 'drop', direction, selectedComponent, isNS)

        }

    } else if (functionID == 1) {//确认操作，累加阻力到风机的系统静压
        componentValue = calculateResistance(componentValue, processSectionList2, combinedMixingChamberIndexes, sections)
        if (selectedComponent.name != "ahu.fan" && selectedComponent.name != "ahu.wheelHeatRecycle" && selectedComponent.name != "ahu.plateHeatRecycle") {
            componentValue = cleanSelected(componentValue, processSectionList2, sections, 'confirm', direction, selectedComponent, isNS)
        } else if (selectedComponent.displayName === "Unhoused Fan Section" || selectedComponent.displayName === "无蜗壳风机段") {
            componentValue = cleanSelected(componentValue, processSectionList2, sections, 'confirm', direction, selectedComponent, isNS)

        }

    }
    return componentValue

    // 拖拽，确认的时候，才需要计算阻力
    //resistanceCalculate(sections, componentValue, selectedComponent);
}



function getIsHasCombinedAndWheelHeatRecycle(sections, templateId) {

    //新回排段分析
    let combinedMixingChamberIndexes = [];
    let combinedMixingChamberIndex = -1;
    //双层热回
    let heatRecycleIndexes = [];
    //是否有轮式
    let findWheelHeatRecycle = false;

    sections && sections.forEach(function (section, index) {
        if (section.name === 'ahu.combinedMixingChamber') {
            combinedMixingChamberIndex = index;
            combinedMixingChamberIndexes.push(index);
        } else if (section.name === 'ahu.wheelHeatRecycle') {
            heatRecycleIndexes.push(index);
            findWheelHeatRecycle = true;
        } else if (section.name === 'ahu.plateHeatRecycle') {
            heatRecycleIndexes.push(index);
        }
    })
    // console.log('heatRecycleIndexes', heatRecycleIndexes, sections)
    let obj = {
        combinedMixingChamberIndexes,
        combinedMixingChamberIndex,
        heatRecycleIndexes,
        findWheelHeatRecycle,
        templateId
    }
    return obj
}



function getProcessSectionList(componentValue, sections, heatRecycleIndexes, findWheelHeatRecycle, functionID, selectedPosition, line1EndIndex, line2FirstIndex, delPos, combinedMixingChamberIndex, selectedComponent, templateId) {
    // console.log('templateId', templateId)
    let processSectionList = [];
    //new
    if (templateId == 'tp5' || templateId == 'tp6' || templateId == 'tp7' || templateId == 'tp8') {
        // console.log('双层')
    } else if (combinedMixingChamberIndex !== -1) {
        // console.log('心会拍')
    } else {
        // console.log('普通')

    }








    //old
    if (heatRecycleIndexes.length === 2) {
        //同步状态
        componentValue[sections[heatRecycleIndexes[1]].id] = componentValue[sections[heatRecycleIndexes[0]].id]


        //双层热回收
        if (!findWheelHeatRecycle) {
            //板式热回收
            //右下部分加左上部分为回风机组（风向从右下到左上） 计算是从左上到右下
            //左下部分加右上部分为送风机组（方向从右上到左下）计算是从左上到右下
            if (functionID == 1) {
                //确认
                if (heatRecycleIndexes.indexOf(selectedPosition) !== -1) {
                    //
                    processSectionList.push(getSectionIndexListByFromTo(heatRecycleIndexes[1], sections.length - 1));
                    processSectionList.push(getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex));
                } else if (selectedPosition < heatRecycleIndexes[0]) {
                    //左上
                    processSectionList.push(getSectionIndexListByFromTo(selectedPosition, heatRecycleIndexes[0]).concat(
                        getSectionIndexListByFromTo(heatRecycleIndexes[1], sections.length - 1)
                    ));
                } else if (selectedPosition <= line1EndIndex) {
                    //右上
                    processSectionList.push(getSectionIndexListByFromTo(selectedPosition, heatRecycleIndexes[0]).concat(
                        getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex)
                    ));
                } else if (selectedPosition < heatRecycleIndexes[1]) {
                    //左下
                    processSectionList.push(getSectionIndexListByFromTo(selectedPosition, line2FirstIndex));
                } else {
                    //右下
                    processSectionList.push(getSectionIndexListByFromTo(selectedPosition, sections.length - 1));
                }
            } else {
                if (delPos === 999) {
                    //新增
                    let addPos = componentValue[selectedComponent.id].assis_section_position - 1;
                    if (addPos === -1 || heatRecycleIndexes.indexOf(addPos) !== -1) {
                        //理论上不会出现
                    } else if (addPos < heatRecycleIndexes[0]) {
                        //左上
                        processSectionList.push(getSectionIndexListByFromTo(addPos, heatRecycleIndexes[0]).concat(
                            getSectionIndexListByFromTo(heatRecycleIndexes[1], sections.length - 1)
                        ));
                    } else if (addPos <= line1EndIndex) {
                        //右上
                        processSectionList.push(getSectionIndexListByFromTo(addPos, heatRecycleIndexes[0]).concat(
                            getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex)
                        ));
                    } else if (addPos < heatRecycleIndexes[1]) {
                        //左下
                        processSectionList.push(getSectionIndexListByFromTo(addPos, line2FirstIndex));
                    } else {
                        //右下
                        processSectionList.push(getSectionIndexListByFromTo(addPos, sections.length - 1));
                    }
                } else {
                    if (delPos <= heatRecycleIndexes[0]) {
                        //左上
                        processSectionList.push(getSectionIndexListByFromTo(Math.max(0, delPos), heatRecycleIndexes[0]).concat(
                            getSectionIndexListByFromTo(heatRecycleIndexes[1], sections.length - 1)
                        ));
                    } else if (delPos <= line1EndIndex) {
                        //右上
                        processSectionList.push(getSectionIndexListByFromTo(delPos, heatRecycleIndexes[0]).concat(
                            getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex)
                        ));
                    } else if (delPos === line2FirstIndex) {
                        //删的是第一排的
                        if (selectedComponent.line === 0) {
                            //右上
                            processSectionList.push(getSectionIndexListByFromTo(line1EndIndex, heatRecycleIndexes[0]).concat(
                                getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex)
                            ));
                        } else {
                            //左下
                            //processSectionList.push(getSectionIndexListByFromTo(delPos,line2FirstIndex));
                        }

                    } else if (delPos < heatRecycleIndexes[1]) {
                        //左下
                        processSectionList.push(getSectionIndexListByFromTo(delPos, line2FirstIndex));
                    } else {
                        //右下
                        processSectionList.push(getSectionIndexListByFromTo(Math.min(delPos, sections.length - 1), sections.length - 1));
                    }

                }
            }


        } else {
            //轮式热回收
            //上层为回风机组（风向从右到左）
            //下层为送风机组（方向从左到右）
            if (functionID == 1) {
                if (selectedPosition <= line1EndIndex) {
                    processSectionList.push(getSectionIndexListByFromTo(selectedPosition, 0));
                } else {
                    processSectionList.push(getSectionIndexListByFromTo(Math.max(line2FirstIndex, selectedPosition), sections.length - 1));
                }
            } else {
                if (delPos === 999) {
                    //新增
                    let addPos = componentValue[selectedComponent.id].assis_section_position - 1;
                    if (addPos <= line1EndIndex) {
                        processSectionList.push(getSectionIndexListByFromTo(addPos, 0));
                    }
                    else if (addPos === line2FirstIndex) {
                        //删的是第一排的
                        if (selectedComponent.line === 0) {
                            //右上最后一个
                            processSectionList.push(getSectionIndexListByFromTo(addPos, 0));
                        } else {
                            //左下第一个
                            //do nothing
                            processSectionList.push(getSectionIndexListByFromTo(Math.max(line2FirstIndex, addPos), sections.length - 1));
                        }
                    }
                    else {
                        processSectionList.push(getSectionIndexListByFromTo(Math.max(line2FirstIndex, addPos), sections.length - 1));
                    }
                } else {
                    if (delPos <= line1EndIndex) {
                        processSectionList.push(getSectionIndexListByFromTo(delPos, 0));
                    } else {
                        processSectionList.push(getSectionIndexListByFromTo(Math.max(line2FirstIndex, delPos - 1), sections.length - 1));
                    }

                }
            }

        }


    }
    else if (combinedMixingChamberIndex !== -1) {
        if (functionID == 1) {
            //编辑确定模式
            //新回排段（包含）之前的段为回风分机组，新回排段（包含）之后的为送风分机组
            if (selectedPosition < combinedMixingChamberIndex) {
                //回风机组发生变化
                processSectionList.push(getSectionIndexListByFromTo(selectedPosition, combinedMixingChamberIndex - 1));
            } else if (selectedPosition > combinedMixingChamberIndex) {
                //送风机组发生变化
                processSectionList.push(getSectionIndexListByFromTo(selectedPosition, sections.length - 1));
            } else if (selectedPosition === combinedMixingChamberIndex) {
                //新回排段发生变化
                processSectionList.push(getSectionIndexListByFromTo(combinedMixingChamberIndex, sections.length - 1));
            }
        } else {
            if (delPos === 999) {
                //新增
                let addPos = componentValue[selectedComponent.id].assis_section_position - 1;
                if (addPos < combinedMixingChamberIndex) {
                    //回风机组发生变化
                    processSectionList.push(getSectionIndexListByFromTo(addPos, combinedMixingChamberIndex - 1));
                } else if (addPos > combinedMixingChamberIndex) {
                    //送风机组发生变化
                    processSectionList.push(getSectionIndexListByFromTo(addPos - 1, sections.length - 1));
                } else if (addPos === combinedMixingChamberIndex) {
                    //新回排段发生变化
                    processSectionList.push(getSectionIndexListByFromTo(0, addPos));
                    processSectionList.push(getSectionIndexListByFromTo(addPos, sections.length - 1));
                }
            }
            else if (delPos <= combinedMixingChamberIndex) {
                //删除
                //回风机组发生变化
                processSectionList.push(getSectionIndexListByFromTo(Math.max(delPos - 1, 0), combinedMixingChamberIndex - 1));
            } else if (delPos > combinedMixingChamberIndex) {
                //删除
                //送风机组发生变化
                processSectionList.push(getSectionIndexListByFromTo(Math.max(delPos - 1, 0), sections.length - 1));
            }
        }

    } else {
        //单层
        if (functionID == 1) {
            //确认修改
            processSectionList.push(getSectionIndexListByFromTo(selectedPosition, sections.length - 1));

        } else {
            if (delPos === 999) {
                //新增
                let addPos = componentValue[selectedComponent.id].assis_section_position - 1;
                processSectionList.push(getSectionIndexListByFromTo(Math.max(0, addPos - 1), sections.length - 1));
            } else {
                // 删除
                processSectionList.push(getSectionIndexListByFromTo(Math.max(0, delPos - 1), sections.length - 1));
            }
        }
    }
    //    console.log('processSectionList', processSectionList)
    if (selectedComponent.name !== 'ahu.ahu') {
        processSectionList.forEach((subList) => {
            // 更新位置信息
            subList.forEach((sectionIndex, i) => {
                //从第二项开始,第一项是基准
                if (i > 0) {
                    let section = sections[sectionIndex],
                        lastSection = sections[subList[i - 1]];
                    //console.log('processSectionList modify section :'+section.id)
                    // componentValue[section.id].meta_section_completed = false; // zzf,清楚勾选，暂时注释
                    if (window.reduxStore.getState().general.fieldValues.TEMPERATURE_TRANSMIT.value === 'true') {
                        transferDataBetweenSection(lastSection, section, componentValue)

                    }
                }
            })
        });
    }
    return processSectionList
}

/**
 * 计算阻力
 * @param {*} componentValue 
 * @param {*} processSectionList2 
 * @param {*} combinedMixingChamberIndexes 
 * @param {*} sections 
 * 
 */
function calculateResistance(componentValue, processSectionList2, combinedMixingChamberIndexes, sections) {
    let componentValueCopy = { ...componentValue }
    processSectionList2.forEach((p) => {
        let dir = p[0];
        let list = p[1];
        let sv;
        let section;
        let sum = 0;
        let fan;
        let fanSection;
        let bool = false;
        let IResistance = '', RResistance = '';
        if (combinedMixingChamberIndexes.length == 1) {
            for (let key in componentValueCopy) {
                if (componentValueCopy[key].hasOwnProperty('meta_section_combinedMixingChamber_IResistance')) {
                    IResistance = componentValueCopy[key]['meta_section_combinedMixingChamber_IResistance']
                    RResistance = componentValueCopy[key]['meta_section_combinedMixingChamber_RResistance']
                }
            }
        }
        list.forEach((si) => {
            section = sections[si];
            if (!section) return;
            sv = componentValueCopy[section.id];
            //设置方向
            sv['meta_section_airDirection'] = dir;
            const currentPrefix = calculatePrefix(section.name);
            let theRes = ''
            if (combinedMixingChamberIndexes.length == 0) {
                theRes = sv[`${currentPrefix}_${RESISTANCE_ATTR_ARR[0]}`];
                if (section.name === "ahu.coolingCoil") {
                    theRes = Number(theRes) + Number(sv[`${currentPrefix}_driftEliminatorResistance`]);
                }

            } else if (combinedMixingChamberIndexes.length == 1) {
                if (section.name == 'ahu.combinedMixingChamber') {
                    theRes = 0
                } else if (section.name === "ahu.fan") {
                    if (sv['meta_section_airDirection'] == 'R') {
                        theRes = RResistance
                    } else {
                        theRes = IResistance
                    }
                } else {
                    theRes = sv[`${currentPrefix}_${RESISTANCE_ATTR_ARR[0]}`];
                }
            }
            if (section.name === "ahu.fan" && combinedMixingChamberIndexes.length == 0) {
                theRes = 0
            }
            if (theRes) {
                sum += parseFloat(theRes);
            }
            if (section.name === "ahu.fan") {
                fan = sv;
                return;
            }
        });
        if (fan) {
            // 更新风机阻力
            fan[RESISTANCE_ATTR_ARR[5]] = sum;
            // 更新系统压降
            fan[RESISTANCE_ATTR_ARR[1]] = sum;
            // 更新总静压
            let fanRESISTANCE_ATTR_ARR2 = Number.isFinite(Number(fan[RESISTANCE_ATTR_ARR[2]])) ? fan[RESISTANCE_ATTR_ARR[2]] : 0
            fan[RESISTANCE_ATTR_ARR[4]] = parseFloat(fan[RESISTANCE_ATTR_ARR[1]]) + parseFloat(fanRESISTANCE_ATTR_ARR2) + parseFloat(fan[RESISTANCE_ATTR_ARR[3]]);
        }

    });
    return componentValueCopy
}

/**
 *清除选中
 *  
 */

function cleanSelected(componentValue, processSectionList2, sections, operation, direction, selectedComponent, isNS) {
    // if (selectedComponent.name == 'ahu.combinedMixingChamber') {//拖动新回排时，全部取消勾选
    //     for (let key in componentValue) {
    //         componentValue[key]['meta_section_completed'] = false
    //     }
    //     return componentValue
    // }
    let currentId = selectedComponent.id
    let currentPosition = 0
    let currentPositionIndex = 0
    
    let prePosition = 0 //前一个段
    let prePositionIndex = 0 //前一个段index
    let nextPosition = 0 //后一个段
    let nextPositionIndex = 0 //后一个段index
    let prePositionName = '' //前一个段名
    let nextPositionName = '' //后一个段名
    let sectionlist1 = processSectionList2[0][1];
    let sectionlist2 = processSectionList2.length == 2 ? processSectionList2[1][1] : undefined
    sections && sections.forEach((record, index) => {
        if (record.id == currentId) {
            currentPositionIndex = index
            currentPosition = index
            
        }
    })
    if (direction == 'S') {
        currentPosition = sectionlist1.findIndex((r) => { return r == currentPosition })//链中的index
        if (operation != 'remove') {
            prePosition = Number(currentPosition)>0 ? Number(currentPosition) - 1 : 0
            prePosition = sectionlist1[prePosition]
            nextPosition = Number(currentPosition)>=0 ? Number(currentPosition) + 1 : 0
            nextPosition = sectionlist1[nextPosition]
            prePositionName = sections[prePosition] ? sections[prePosition] : {name : ''}
            nextPositionName = sections[nextPosition] ? sections[nextPosition] : { displayName: '' }
        }

    } else if (direction == 'R') {
        currentPosition = sectionlist2 && sectionlist2.findIndex((r) => { return r == currentPosition })
        if (operation != 'remove') {
            prePosition = Number(currentPosition)>=0 ? Number(currentPosition) - 1 : 0
            prePosition = sectionlist2[prePosition]
            nextPosition = Number(currentPosition)>=0 ? Number(currentPosition) + 1 : 0
            nextPosition = sectionlist2[nextPosition]
            prePositionName = sections[prePosition] ? sections[prePosition] : {name:'',displayName:'', id:''}
            nextPositionName = sections[nextPosition] ? sections[nextPosition] : { name:'', displayName: '', id:'' }
        }


    }
    // console.log('clean', componentValue, sectionlist1, sectionlist2, sections, selectedComponent, prePosition, prePositionName, currentPosition, nextPosition, nextPositionName)
    if (direction == 'S' && sectionlist1.length > 0) {
        sectionlist1.forEach((_record, _index) => {
            if (_index == (currentPosition-1) && (selectedComponent.displayName == '无蜗壳风机段' || selectedComponent.displayName == 'Unhoused Fan Section') && prePositionName.name == 'ahu.access') {
                
                componentValue[sections[prePosition].id]['meta_section_completed'] = false
            }
            if (selectedComponent.displayName == '无蜗壳风机段' || selectedComponent.displayName == 'Unhoused Fan Section' || (selectedComponent.name == 'ahu.access' && (nextPositionName.displayName == '无蜗壳风机段' || nextPositionName.displayName == 'Unhoused Fan Section'))) {
            } else {
                if (_index >= currentPosition) {
                    let id = sections[_record].id;
                    if (operation == 'remove') {//删除段的时候
                        sections.forEach((obj) => {
                            if (obj.name == 'ahu.fan') {//删除段的时候取消风机段的选中状态
                                componentValue[obj.id]['meta_section_completed'] = false
                                // componentValue[obj.id]['meta_section_fan_fanModel'] = ''
                            }
                        })
                    } else if (operation == 'drop') {
                        let isFan = false
                        sections.map((sec, index) => {
                            if (sec.id == id && sec.name == 'ahu.fan') {
                                isFan = true
                            }
                        })
                        if (window.reduxStore.getState().general.fieldValues.TEMPERATURE_TRANSMIT.value === 'true') {
                            componentValue[id]['meta_section_completed'] = false
                            // if(componentValue[id].hasOwnProperty('meta_section_fan_fanModel')){
                            //     componentValue[id]['meta_section_fan_fanModel'] = ''                            
                            // }
                        } else {
                            if (isFan) {
                                componentValue[id]['meta_section_completed'] = false
                                // componentValue[id]['meta_section_fan_fanModel'] = ''
                            }
                        }
                    } else {
                        let isFan = false
                        sections.map((sec, index) => {
                            if (sec.id == id && sec.name == 'ahu.fan') {
                                isFan = true
                            }
                        })
                        if (window.reduxStore.getState().general.fieldValues.TEMPERATURE_TRANSMIT.value === 'true' && !isNS) {
                            componentValue[id]['meta_section_completed'] = false
                            // if(componentValue[id].hasOwnProperty('meta_section_fan_fanModel')){
                            //     componentValue[id]['meta_section_fan_fanModel'] = ''                            
                            // }
                        } else {
                            if (isFan && !isNS) {
                                componentValue[id]['meta_section_completed'] = false
                                // componentValue[id]['meta_section_fan_fanModel'] = ''
                            }
                        }
                    }
                }
                if (componentValue[sections[_record].id].hasOwnProperty('meta_section_fan_Resistance') && !isNS) {
                    componentValue[sections[_record].id]['meta_section_completed'] = false
                }
                if (componentValue[sections[_record].id].hasOwnProperty('meta_section_fan_fanModel') && !isNS) {
                    componentValue[sections[_record].id]['meta_section_fan_fanModel'] = ''
                }
            }
        })
    }
    if (direction == 'R' && sectionlist2) {
        sectionlist2.forEach((__record, __index) => {
            if (__index == (currentPosition-1) && (selectedComponent.displayName == '无蜗壳风机段' || selectedComponent.displayName == 'Unhoused Fan Section') && prePositionName.name == 'ahu.access') {
                componentValue[sections[prePosition].id]['meta_section_completed'] = false
            }
            if (selectedComponent.displayName == '无蜗壳风机段' || selectedComponent.displayName == 'Unhoused Fan Section' || (selectedComponent.name == 'ahu.access' && (nextPositionName.displayName == '无蜗壳风机段' || nextPositionName.displayName == 'Unhoused Fan Section'))) {

            } else {
                if (__index >= currentPosition) {
                    let ids = sections[__record].id;
                    // componentValue[ids]['meta_section_completed'] = false
                    if (operation == 'remove') {
                        sections.forEach((obj) => {
                            if (obj.name == 'ahu.fan' && componentValue[obj.ids]) {
                                componentValue[obj.ids]['meta_section_completed'] = false
                            }
                        })
                    } else if (operation == 'drop') {
                        let isFan = false
                        sections.map((sec, index) => {
                            if (sec.id == ids && sec.name == 'ahu.fan') {
                                isFan = true
                            }
                        })
                        if (window.reduxStore.getState().general.fieldValues.TEMPERATURE_TRANSMIT.value === 'true') {
                            componentValue[ids]['meta_section_completed'] = false
                        } else {
                            if (isFan) {
                                componentValue[ids]['meta_section_completed'] = false
                            }
                        }


                    } else {
                        let isFan = false
                        sections.map((sec, index) => {
                            if (sec.id == ids && sec.name == 'ahu.fan') {
                                isFan = true
                            }
                        })
                        if (window.reduxStore.getState().general.fieldValues.TEMPERATURE_TRANSMIT.value === 'true' && !isNS) {
                            componentValue[ids]['meta_section_completed'] = false
                        } else {
                            if (isFan && !isNS) {
                                componentValue[ids]['meta_section_completed'] = false
                            }
                        }
                    }
                }
                if (componentValue[sections[__record].id].hasOwnProperty('meta_section_fan_Resistance') && !isNS) {
                    componentValue[sections[__record].id]['meta_section_completed'] = false
                }
                if (componentValue[sections[__record].id].hasOwnProperty('meta_section_fan_fanModel') && !isNS) {
                    componentValue[sections[__record].id]['meta_section_fan_fanModel'] = ''
                }
            }
        })
    }
    if (direction == 'COMBINE') {
        sections.forEach((section) => {
            if (section.name == 'ahu.fan') {
                componentValue[section.id]['meta_section_completed'] = false
            }
        })
    }


    return componentValue
}


export function validateSection(sections = getSimpleSectionArr(), componentValue, selectedComponent, format = true, layout) {//format为true是提供发给后端校验的格式，false为计算阻力等需要的格式
    //新回排段分析
    let combinedMixingChamberIndexes = [];
    let combinedMixingChamberIndex = -1;
    //双层热回
    let heatRecycleIndexes = [];
    //是否有轮式
    let findWheelHeatRecycle = false;
    //用于处理风机阻力/压力 和段方向
    let processSectionList2 = [];
    let findReturn = false;

    if (layout) {
        if (layout.style == STYLE_DOUBLE_RETURN_1 || layout.style == STYLE_DOUBLE_RETURN_2 ||
            layout.style == STYLE_SIDE_BY_SIDE_RETURN) {
            findReturn = true;
        }
    }

    sections.forEach(function (section, index) {
        if (section.name === 'ahu.combinedMixingChamber') {
            combinedMixingChamberIndex = index;
            combinedMixingChamberIndexes.push(index);
        } else if (section.name === 'ahu.wheelHeatRecycle') {
            heatRecycleIndexes.push(index);
            findWheelHeatRecycle = true;
        } else if (section.name === 'ahu.plateHeatRecycle') {
            heatRecycleIndexes.push(index);
        }
    })
    let line1EndIndex = -1, line2FirstIndex = -1;

    sections.forEach((section, i) => {
        const currentSection = componentValue[section.id];
        currentSection && (currentSection.assis_section_position = i + 1)

        if (section.line === 0) {
            line1EndIndex = i;
        }

    });
    if (line1EndIndex !== -1) {
        line2FirstIndex = line1EndIndex + 1;
    }
    if (findReturn) {
        //转向机组
        //上层为回风机组（风向从右到左）
        //下层为送风机组（方向从左到右）
        processSectionList2.push(["S", getSectionIndexListByFromTo(line2FirstIndex, sections.length - 1)])
        processSectionList2.push(["R", getSectionIndexListByFromTo(line1EndIndex, 0)])
    } else {
        if (heatRecycleIndexes.length === 2) {
            //双层热回收
            if (!findWheelHeatRecycle) {
                //板式热回收
                //右下部分加左上部分为回风机组（风向从右下到左上） 计算是从左上到右下
                //左下部分加右上部分为送风机组（方向从右上到左下）计算是从左上到右下

                processSectionList2.push(["S", getSectionIndexListByFromTo(0, heatRecycleIndexes[0]).concat(getSectionIndexListByFromTo(heatRecycleIndexes[1], sections.length - 1))])
                processSectionList2.push(["R", getSectionIndexListByFromTo(line1EndIndex, heatRecycleIndexes[0]).concat(getSectionIndexListByFromTo(heatRecycleIndexes[1], line2FirstIndex))])
            } else {
                //轮式热回收
                //上层为回风机组（风向从右到左）
                //下层为送风机组（方向从左到右）

                processSectionList2.push(["S", getSectionIndexListByFromTo(line2FirstIndex, sections.length - 1)])
                processSectionList2.push(["R", getSectionIndexListByFromTo(line1EndIndex, 0)])
            }
        } else if (combinedMixingChamberIndex !== -1) {
            if (combinedMixingChamberIndex != sections.length - 1) {
                processSectionList2.push(["S", getSectionIndexListByFromTo(combinedMixingChamberIndex + 1, sections.length - 1)]);
            }
            processSectionList2.push(["R", getSectionIndexListByFromTo(0, combinedMixingChamberIndex)]);
        } else {
            processSectionList2.push(["S", getSectionIndexListByFromTo(0, sections.length - 1)])
        }
    }

    let cloneProcessSectionList = []
    format && processSectionList2.forEach((record, index) => {
        let obj = {}
        obj.airDirection = record[0];
        record[1].forEach((_record, _index, _arr) => {
            _arr[_index] = sections[_record]
        })
        obj.value = record[1]
        cloneProcessSectionList.push(obj)

    })
    if (format) {
        cloneProcessSectionList.forEach(function (sectionList) {
            sectionList.value.forEach(function (section) {
                if (section.name === 'ahu.fan') {
                    let displayName = intl.get(LABEL_WWK_FAN_SECTION);
                    if (section.displayName === displayName) {
                        section.name = 'ahu.wwkFan'
                    }
                }
            })
        });
        return cloneProcessSectionList;
    } else {
        return processSectionList2
    }
}



//根据起始序号获取顺序段列表
export function getSectionIndexListByFromTo(fromIndex, toIndex) {
    if (fromIndex == -1) return [];//新回排的时候当新回排是左边第一个段时返回空
    if (fromIndex == 0 && toIndex == -1) return [];//当最后一个段被拖拽出去的时候返回空
    let list = [];
    if (fromIndex < toIndex) {
        for (let i = fromIndex; i <= toIndex; i++) {
            list.push(i);
        }

    } else if (fromIndex > toIndex) {
        for (let i = fromIndex; i >= toIndex; i--) {
            list.push(i);
        }
    } else {
        list.push(fromIndex);
    }
    if (list.length === 2 && list[0] === list[1]) {
        return [list[0]];
    }
    return list;

}

//数据传递
function transferDataBetweenSection(fromSection, toSection, componentValue) {
    const preSectionPrefix = calculatePrefix(fromSection.name);
    const currentPrefix = calculatePrefix(toSection.name);
    const fromSectionVal = componentValue[fromSection.id];
    const toSectionVal = componentValue[toSection.id];
    // console.log('AHU_FLOW_ATTR_ARR', fromSection, toSection)
    if (fromSection.name == 'ahu.heatingCoil') {
        let preValue1 = fromSectionVal[`${preSectionPrefix}_WOutDryBulbT`];
        let preValue2 = fromSectionVal[`${preSectionPrefix}_WOutWetBulbT`];
        let preValue3 = fromSectionVal[`${preSectionPrefix}_WOutRelativeT`];
        if (preValue1 != null) {
            toSectionVal[`${currentPrefix}_WOutDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_WInDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_SInDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_SOutDryBulbT`] = preValue1;
            // toSectionVal[`${currentPrefix}_SOutDryBulbT`] = preValue1;
        }
        if (preValue2 != null) {
            toSectionVal[`${currentPrefix}_WOutWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_WInWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_SInWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_SOutWetBulbT`] = preValue2;
            // toSectionVal[`${currentPrefix}_SOutWetBulbT`] = preValue2;
        }
        if (preValue3 != null) {
            toSectionVal[`${currentPrefix}_WOutRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_SOutRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_WInRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_SInRelativeT`] = preValue3;
        }

    } else if (toSection.name == 'ahu.heatingCoil') {
        let preValue1 = fromSectionVal[`${preSectionPrefix}_SOutDryBulbT`];
        let preValue2 = fromSectionVal[`${preSectionPrefix}_SOutWetBulbT`];
        let preValue3 = fromSectionVal[`${preSectionPrefix}_SOutRelativeT`];
        if (preValue1 != null) {
            toSectionVal[`${currentPrefix}_WOutDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_WInDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_SInDryBulbT`] = preValue1;
            toSectionVal[`${currentPrefix}_SOutDryBulbT`] = preValue1;
            // toSectionVal[`${currentPrefix}_SOutDryBulbT`] = preValue1;
        }
        if (preValue2 != null) {
            toSectionVal[`${currentPrefix}_WOutWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_WInWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_SInWetBulbT`] = preValue2;
            toSectionVal[`${currentPrefix}_SOutWetBulbT`] = preValue2;
            // toSectionVal[`${currentPrefix}_SOutWetBulbT`] = preValue2;
        }
        if (preValue2 != null) {
            toSectionVal[`${currentPrefix}_WOutRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_SOutRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_WInRelativeT`] = preValue3;
            toSectionVal[`${currentPrefix}_SInRelativeT`] = preValue3;
        }

    } else {
        AHU_FLOW_ATTR_ARR.forEach((attr) => {
            let preValue = fromSectionVal[`${preSectionPrefix}_${attr[0]}`];
            // console.log('attr', attr)
            //let currentValue=toSectionVal[`${currentPrefix}_${attr[1]}`];
            if (preValue != null) {
                toSectionVal[`${currentPrefix}_${attr[1]}`] = preValue;
                toSectionVal[`${currentPrefix}_${attr[0]}`] = preValue;
            }
        });
    }
}

function calculatePrefix(sectionKey) {
    return `meta_section_${sectionKey.substr(sectionKey.indexOf('.') + 1)}`;
}
/**
 * 依次计算各个段的阻力，根据风向，把阻力和赋值到相应的风机上
 * @param sections
 * @param componentValue
 * @param selectedComponent
 */


/**
 * id,当前选中的段id
 *
 */
export function changeFanPicture(id) {
    let ahus = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
    ahus.forEach(ahu => {
        const $section = ahu.find(`[data-section-id='${id}']`)
    })
}