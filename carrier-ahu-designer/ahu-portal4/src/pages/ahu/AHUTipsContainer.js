import { connect } from 'react-redux'
import AHUTips from './AHUTips'

function ahuSerial(ahu) {
  if (ahu && ahu.meta_ahu_serial) {
    return true
  }
  return false
}

const mapStateToProps = state => {
  return {
    validation: state.ahu.validation,
    ahuSerial: ahuSerial(state.ahu.componentValue[0]),
    // panelConfirmTip2:state.partition.panelConfirmTip
    
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // onTodoClick: id => {
    //   dispatch(toggleTodo(id))
    // }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUTips)
