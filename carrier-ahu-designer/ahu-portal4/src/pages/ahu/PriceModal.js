import {
    PRICE,
    WEIGHT,
    UNIT_NAME,
    TOTAL_PRICE_WEIGHT,
    CLOSE,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import loadingImg from '../../images/loding.gif'

class PriceModal extends React.Component {
  render() {
    const { total, weight, details } = this.props.price
    let bool = false
    if(details && details.length ==0){
      bool = true 
    }
    if(total == '0'){
      bool = false
    }
    return (
      <div className="modal fade" id="priceModal" tabIndex="-1" role="dialog" aria-labelledby="priceModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            {bool && <img src={loadingImg} style={{position:'absolute',width:'40px',top:'90%',left:'45%'}}/>}
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="priceModalLabel">{intl.get(PRICE)}</h4>
            </div>
            <div className="modal-body">
              <div style={{maxHeight:window.innerHeight-220,overflow:'auto'}}>
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th>{intl.get(UNIT_NAME)}</th>
                      <th>{intl.get(PRICE)}</th>
                      <th>{intl.get(WEIGHT)}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                        details?details.map((detail, index) => {
                        return (
                          <tr key={index}>
                            <td>{detail.name}</td>
                            <td>{Number(detail.price).toFixed(0)}</td>
                            <td>{Number(detail.weight).toFixed(0)}</td>
                          </tr>
                        )
                      }):''
                    }
                    <tr>
                      <td>{intl.get(TOTAL_PRICE_WEIGHT)}</td>
                      <td>{total?Number(total).toFixed(0):Number(0).toFixed(0)}</td>
                      <td>{weight?Number(weight).toFixed(0):Number(0).toFixed(0)}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    price: state.ahu.price
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // onTodoClick: id => {
    //   dispatch(toggleTodo(id))
    // }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PriceModal)
