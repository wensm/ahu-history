import {
    MIX_SECTION,
    FILTRATION_SECTION,
    COIL_SECTION,
    FAN_SECTION,
    CONTROL_SECTION,
    SILENCE_SECTION,
    HUMIDIFICATION_SECTION,
    HEAT_EXCHANGE,
} from '../intl/i18n'

import React from 'react'
import classnames from 'classnames'
import {connect} from 'react-redux'
import intl from 'react-intl-universal'
import style from './SectionList.css'
import Section from './Section'

const wwkFanSection = {
        cName: "wwk_fan_section",
        fullName: "wwkFan_1.0.0",
        metaId: "ahu.fan",
        name: "Fan",
        version: "1.0.0"
    };

export default class SectionList extends React.Component {


	constructor(props) {
        super(props)
        this.state = {
        	sections:false
        };
    }
	componentWillMount(){
		
	}
	sectclick(Kname,event){
		this.setState({
      	sections:!this.state.sections
  	});
   switch (Kname){/*
   	case "fan1":
	   	if(this.state.sections == false){
				$('.fan1').css("display","none")
			}else{
				$(".fan1").css("display","block")
			};
			break;
		case "fan2":
	   	if(this.state.sections == false){
				$('.fan2').css("display","none")
			}else{
				$(".fan2").css("display","block")
			};
			break;
		case "fan3":
	   	if(this.state.sections == false){
				$('.fan3').css("display","none")
			}else{
				$(".fan3").css("display","block")
			};
			break;
		case "fan4":
	   	if(this.state.sections == false){
				$('.fan4').css("display","none")
			}else{
				$(".fan4").css("display","block")
			};
			break;
		case "fan5":
	   	if(this.state.sections == false){
				$('.fan5').css("display","none")
			}else{
				$(".fan5").css("display","block")
			};
			break;
		case "fan6":
	   	if(this.state.sections == false){
				$('.fan6').css("display","none")
			}else{
				$(".fan6").css("display","block")
			};
   		break;
   	default:
   		break;
   */}
	}
  render() {
	const {sections, templates, canDropList	} = this.props
    let mix=[];
    mix.push([
    	sections[0],
    	sections[15],
    	sections[13],
    	sections[12]
    ])
    mix.push([
    	sections[1],
    	sections[2],
    	sections[16],
    	sections[18]
    ])
    mix.push([
    	sections[4],
    	sections[5],
    	sections[6],
    	sections[7],
    	sections[8]
    ])
    mix.push([
    	sections[3],
    	wwkFanSection,
    	sections[19],
    	sections[14]
    ])
    mix.push([
    	sections[9],
    	sections[10],
    	sections[11],
    	sections[17],
    ])
    mix.push([
    	sections[20],
    	sections[21]
    ])
    return (
      <div className={style.SectionList}>
        <div id="sections" className={style.SectionLi}>
        	<p><span className={style.SectionSpan}></span>{intl.get(MIX_SECTION)}</p>
	    		{sections.length!=0 && mix[0].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
		    <p><span className={style.SectionSpan}></span>{intl.get(FILTRATION_SECTION)}</p>
		    	{sections.length!=0 && mix[1].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
		    <p><span className={style.SectionSpan}></span>{intl.get(COIL_SECTION)}</p>
		    	{sections.length!=0 && mix[2].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
	    	<p><span className={style.SectionSpan}></span>{intl.get(FAN_SECTION)} &nbsp;{intl.get(CONTROL_SECTION)}&nbsp; {intl.get(SILENCE_SECTION)}</p>
	    		{sections.length!=0 && mix[3].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
	    	<p><span className={style.SectionSpan}></span>{intl.get(HUMIDIFICATION_SECTION)}</p>
		    	{sections.length!=0 && mix[4].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
		    <p><span className={style.SectionSpan}></span>{intl.get(HEAT_EXCHANGE)}</p>
        		{sections.length!=0 && mix[5].map((section, index) => <Section key={index} canDropList= {canDropList} {...section} />)}
        </div>
      </div>
    )
  }
}
