import {connect} from 'react-redux'
import PanelClass from './Panel'
import {PARTITION_PANEL_SELECTED, PARTITION_PANEL_DELETED, PARTITION_PANEL_DEVIDED, PARTITION_PANEL_LOCKING, PARTITION_PANEL_MERGES, merges} from '../../actions/partition'

const mapStateToProps = (state, ownProps) => {
    return {
        editingAHUPartitions: state.partition.editingAHUPartitions,
        editingAHUProps: state.partition.editingAHUProps,
        currentPanelIndex: state.partition.currentPanelIndex,
        currentPartitionIndex: state.partition.currentPartitionIndex,
        currentPanel: state.partition.currentEPanel,
        currentSubPanel: state.partition.currentESubPanel,
        currentParentPanel: state.partition.currentEParentPanel,
        fromMouseToLeft :state.partition.fromMouseToLeft,
        ahuSections: state.ahu.sections,
        isLockedPanel:state.partition.lockingPanel,
        // panelSeries: state.ahu.currentUnit ? state.ahu.currentUnit.panelSeries : ''

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDevidePanel: (currentPanel, subPanelId, direction, product, percent) => {
            // console.log("onDevidePanel  "+subPanelId+"  "+direction)
            dispatch(panelDevided(currentPanel, subPanelId, direction, product, percent))
        },
        onDeleteSubPanel: (currentPanel, subPanelId) => {
            dispatch(panelDeleted(currentPanel, subPanelId))
        },
        onToggleSelectPanel: (currentPanel, subPanelId) => {
            // dispatch(panelSelected(currentPanel, subPanelId))
        },
        onLocking: (currentPanel) => {
            dispatch(locking(currentPanel))
        },
        onMerge: (id1, id2, panels, product)=>{
            dispatch(merges(id1, id2, panels, product))
            
        }
    }
}
// function merges(thisPanel) {
//     return {
//         type: PARTITION_PANEL_MERGES,
//         thisPanel,
//     }
// }

function locking(currentPanel) {
    return {
        type: PARTITION_PANEL_LOCKING,
        currentPanel,
    }
}
function panelDeleted(currentPanel, subPanelId) {
    return {
        type: PARTITION_PANEL_DELETED,
        currentPanel: currentPanel,
        subPanelId: subPanelId,
    }
}
function panelSelected(currentPanel) {
    return {
        type: PARTITION_PANEL_SELECTED,
        currentPanel: currentPanel
    }
}

function panelDevided(currentPanel, subPanelId, direction, product, percent) {
    return {
        type: PARTITION_PANEL_DEVIDED,
        currentPanel: currentPanel,
        subPanelId: subPanelId,
        direction: direction,
        product,
        percent
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PanelClass)
