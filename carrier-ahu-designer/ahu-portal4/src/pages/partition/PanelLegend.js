import {
    ONE_CELL_EDIT_WINDOW,
    INTERNAL_PANEL_FRAME,
    OUTSIDE_PANEL_FRAME,
    COMBINED_FRAME,
    DOOR_PANEL,
    OUTLET_AIR_PANEL,
    HUMIDIFICATION_PANEL,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from './Panel.css'
import { PANEL_NAMES, PANEL_BGS, LINE_NAMES, LINE_BGS, PANEL_NAMES_G, PANEL_NAMES_XT, PANEL_BGS_G, PANEL_BGS_XT,LINE_NAMES_XT,LINE_BGS_XT } from './DivideConstants'

//{intl.get(ONE_CELL_EDIT_WINDOW)}
export default class PanelLegend extends React.Component {
    constructor(props) {
        super(props)

    }


    render() {
            //    console.log('LINE_BGS',LINE_BGS)
        const { product } = this.props
        let legends = [];
        let door = [
            // "PUTONG",
            "WU",
            "WAI",
            "MEN",
            "CHUFENG",
            "JIASHI",
            "JIAQIANG",
            "ZHENGYA",
            "CHAIXIE",
            "MEN_MING"];
        // let frames = [
        //     "JIAQIANG",
        //     "NEI",
        //     "ZUHE"]
        if (product == '39G') {
            door = [
                // "PUTONG",
                "WU",
                "WAI",
                "MEN",
                "CHUFENG",
                "JIASHI",
                "JIAQIANG2",
                "ZHENGYA",
                "CHAIXIE",
                "MEN_MING"];
            door.forEach((key) => {
                legends.push({
                    name: PANEL_NAMES_G[key],
                    bg: PANEL_BGS_G[key]
                })
            });
            [
                // "PUTONG",
                "JIAQIANG",
                "NEI",
                "ZUHE"].forEach((key) => {
                    legends.push({
                        name: LINE_NAMES[key],
                        bg: LINE_BGS[key][1]
                    })
                });
        } else if (product == '39XT') {
            door = [
                "WU",
                "MEN"];
            
            door.forEach((key) => {
                legends.push({
                    name: PANEL_NAMES_XT[key],
                    bg: PANEL_BGS_XT[key]
                })
            });
            [
                "NEIJIAOGANG",
                "NEIJIAOGANGZHUANGSHI"].forEach((key) => {
                    legends.push({
                        name: LINE_NAMES_XT[key],
                        bg: LINE_BGS_XT[key][1]
                    })
                });
        } else {

            door.forEach((key) => {
                legends.push({
                    name: PANEL_NAMES[key],
                    bg: PANEL_BGS[key]
                })
            });
            [
                // "PUTONG",
                "JIAQIANG",
                "NEI",
                "ZUHE"].forEach((key) => {
                    legends.push({
                        name: LINE_NAMES[key],
                        bg: LINE_BGS[key][1]
                    })
                });
        }
        return (
            <div className="panel panel-info">
                <div className="panel-heading">
                    <h5 className="panel-title">Legend</h5>
                </div>
                <div className="panel-body">
                    <div className="row">

                        {
                            legends.map((legend, index) => <div key={index} className="col-lg-6">
                                <span style={
                                    {
                                        display: "inline-block",
                                        width: 20,
                                        height: 20,
                                        background: "url(" + legend.bg + ") no-repeat center",
                                        backgroundSize: "100% auto",
                                        verticalAlign: "middle",
                                        marginRight: 8,
                                        border: legend.bg ? "" : "1px solid #999"
                                    }
                                }></span>
                                <span style={{ fontSize: '12px' }}>{legend.name}</span>
                            </div>)
                        }


                        {/*<div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(INTERNAL_PANEL_FRAME)}</span>
                         </div>
                         <div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(OUTSIDE_PANEL_FRAME)}</span>
                         </div>
                         <div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(COMBINED_FRAME)}</span>
                         </div>
                         <div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(DOOR_PANEL)}</span>
                         </div>
                         <div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(OUTLET_AIR_PANEL)}</span>
                         </div>
                         <div className="col-lg-12">
                         <span className="glyphicon glyphicon-resize-vertical"
                         aria-hidden="true">{intl.get(HUMIDIFICATION_PANEL)}</span>
                         </div>*/}
                    </div>
                </div>
            </div>
        )
    }
}
