import {connect} from 'react-redux'
import PartitionPage from './PartitionPage'
import {fetchAhuInfo, fetchAhuSections, fetchAhuPartitions, fetchPartitionMeta, panelInit, fetchThreeViews} from '../../actions/ahu'
import {
    PARTITION_PANEL_BACKWARD, PARTITION_PANEL_FORWARD, PARTITION_PANEL_SPLIT, reset,PARTITION_PANEL_CLEAN
} from '../../actions/partition'

const mapStateToProps = state => {
    var s = {
        editingAHUProps: state.partition.editingAHUProps||{},
        // currentPartitions: state.partition.editingAHUPartitions,
        // currentPanelIndex: state.partition.currentPanelIndex,
        // currentPartitionIndex: state.partition.currentPartitionIndex,
        layout: state.partition.layout,
        tips:state.partition.tip,
        locale:state.general.user.preferredLocale,
        panelConfirmTip: state.partition.panelConfirmTip,
        panelSeries: state.ahu.currentUnit ? state.ahu.currentUnit.panelSeries : ''
    }
    return s
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchAhuInfo: function (ahuId) {
            dispatch(fetchAhuInfo(ahuId))
        },
        onFetchAhuSections: function (ahuId) {
            dispatch(fetchAhuSections(ahuId))
        },
        onFetchAhuPartitions: function (ahuId) {
            dispatch(fetchAhuPartitions(ahuId))
        },
        onFetchPartitionMeta: function () {
            dispatch(fetchPartitionMeta())
        },
        onForward: (partitionIndex, layout) => {
            dispatch(forward(partitionIndex, layout))
        },
        onBackward: (partitionIndex, layout) => {
            dispatch(backward(partitionIndex, layout))

        },
        onSplit: (partitionIndex, layout) => {
            dispatch(split(partitionIndex, layout))
        },
        onResetTip: () => {
            dispatch(reset())
        },
        onPanelInit(ahuId, callBack, transformer=''){
            dispatch(panelInit(ahuId, callBack, transformer));
        },
        onClean(){
            dispatch(clean());
        },
        onFetchThreeViews(ahuId, fromWhere){
            dispatch(fetchThreeViews(ahuId, fromWhere));
        }
    }
}
function clean() {
    return {
        type: PARTITION_PANEL_CLEAN,
    }
}
function forward(partitionIndex, layout) {
    return {
        type: PARTITION_PANEL_FORWARD,
        partitionIndex: partitionIndex,
        layout: layout
    }
}

function backward(partitionIndex, layout) {
    return {
        type: PARTITION_PANEL_BACKWARD,
        partitionIndex: partitionIndex,
        layout: layout
    }
}

function split(partitionIndex, layout) {
    return {
        type: PARTITION_PANEL_SPLIT,
        partitionIndex: partitionIndex,
        layout: layout
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PartitionPage)
