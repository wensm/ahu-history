import {connect} from 'react-redux'
import AhuForm from './AhuForm'
import {
    saveAhu,
    checkUsefulNm,
    cancleAhu,
    cleanUsefulNm
} from '../../actions/groups'

const mapStateToProps = (state, owProps) => {
    return {
        initialValues: state.groups.updateAhu,
        // usefulNumber: state.groups.usefulNumber,
        checkUseFulNmRes: state.groups.checkUseFulNmRes,
        ahuForm: state.form.AhuForm == undefined ? {} : state.form.AhuForm.values,
        page:state.groups.page,
        isEditAhu:state.groups.isEditAhu
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        
        onCheckUsefulNm(value) {
            dispatch(checkUsefulNm(value, owProps.projectId))
        },
        onCleanUsefulNm() {
            dispatch(cleanUsefulNm( ))
        },
        
        onSaveAhu(ahu, page) {
            ahu['pid'] = owProps.projectId
            dispatch(saveAhu(ahu, page))
        },
        onCancelEditAhu() {
            dispatch(cancleAhu())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AhuForm)
