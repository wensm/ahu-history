import {
    PARAMETER_BATCH_SETTING,
    BATCH_SELECTION,
    EDIT_GROUP_INFO,
    IMPORT,
    EXPORT,
    DELETE_GROUP,
    NEW_AHU,
    CHOOSE,
    UNIT_NUMBER,
    DRAWING_NO,
    UNIT_NAME,
    UNIT_MODEL,
    PIPE_CONNECTION_DIRECTION,
    CONSTRUCTION_DOOR_DIRECTION,
    QUANTITY,
    WEIGHT,
    PRICE,
    MODIFY_TIME,
    STATE,
    OPERATION,
    UNIT_SERIES,
    CUSTOMERPO,
    CONNEXONLISTTYPE
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import DateUtil from '../../misc/DateUtils'
import http from '../../misc/http'
import style from './GroupDiv.css'
import AhuDiv from './AhuDiv'
import classnames from 'classnames'
import { fetchGroupInPage, cleanGroupList } from '../../actions/groups'
import { getStatus } from '../../actions/projects'
import { onSetStandard } from '../../actions/ahu'
import { onSetProduct , onSetSerial} from '../../actions/metadata'
import { push } from 'react-router-redux'


class GroupDiv extends React.Component {
    constructor(props) {
        super(props)
        this.openNewAhu = this.openNewAhu.bind(this)
        this.onBatchSelectSet = this.onBatchSelectSet.bind(this)
        this.doFilter = this.doFilter.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.BatchImport = this.BatchImport.bind(this)
        this.BatchExport = this.BatchExport.bind(this)
        this.toPage = this.toPage.bind(this)
    }

    componentDidMount() {
        $('[data-toggle="tooltip"]').tooltip()
    }

    openNewAhu(groupId, groupType) {
        this.props.onClick('ahu', 'new', groupId, groupType)
        // setTimeout(() => {

        //     $("#newAhu").data("groupid", groupId)
        //     $("#newAhu").data("grouptype", groupType)
        // }, 1000);
        // $(this.refs.groupDiv).before($("#newAhu"))
        // $("#newAhu").collapse('show');
    }

    handleClick(groupName) {
        $('#' + groupName).collapse('toggle')
    }

    onBatchSelectSet(groupId, ahuId) {
        $("#BatchSelectSet").data('groupid', groupId)
        this.props.onBatchSelectSet(groupId, ahuId)
    }

    onBatchCalculation(groupId, ahuId) {
        $("#BatchCalculation").data('groupid', groupId)
        $("#BatchCalculation").data('ahuId', 'all')
    }

    BatchImport() {
        $('#BatchImport').data("projectId", this.props.projectId)
        $('#BatchImport').data("groupId", this.props.groupId)
        $('#BatchImport').modal('show')
    }

    BatchExport() {
        $('#BatchExport').data("projectId", this.props.projectId)
        $('#BatchExport').data("groupId", this.props.groupId)
        $('#BatchExport').modal('show')
    }

    doFilter(ahu) {
        const { ahuFilterStr } = this.props
        let tempFilter = $.trim(ahuFilterStr).toLowerCase()
        if (tempFilter == "") {
            return true
        }
        if (!ahu.name || ahu.name.toLowerCase().includes(tempFilter)) {
            return true
        }
        if (!ahu.drawingNo || ahu.drawingNo.toLowerCase().includes(tempFilter)) {
            return true
        }
        if (!ahu.unitid || ahu.unitid.toLowerCase().includes(tempFilter)) {
            return true
        }

        if (!ahu.recordStatus || intl.get(getStatus(ahu.recordStatus)).toLowerCase().includes(tempFilter)) {
            return true
        }
        return false
    }

    toPage(page) {
        if (this.props.number + 1 == page) {
            return
        }
        push('./a')
        this.props.onToPage(page)
    }

    render() {
        let { ahuFilterStr, totalPages, number, first, last, SetStandard, page, onClick, personal,doCleanGroupList } = this.props
        // let arr = location.hash.split('page_')
        // if(arr[1] != undefined){
        //     page = arr[1]
        // }
        const ahuGroup = this.props
        let ahus = ahuGroup.ahus
        //        console.log('zzf7', ahus, ahuGroup)
        const ahuFirst = ahus[0]
        var flg = ahuGroup.flg
        if (flg == undefined) {
            flg = "group"
        }
        let icons = []
        for (var index = 0; index < totalPages; index++) {
            icons.push(index + 1)
        }

        ahus.sort((a, b) => {
            let va = Number(a.drawingNo);
            if (va === Number.NaN) {
                return 1;
            }
            let vb = Number(b.drawingNo);
            if (vb === Number.NaN) {
                return -1;
            }
            return va - vb;
        })
        return (

            <div data-name="GroupDiv" ref="groupDiv" className={classnames(style.groupDiv, 'pro_cont clear com_cont')}>
                <div data-name="Group" className="panel panel-default">
                    <div className="panel-heading clearfix" role="tab" id="headinggroup6"
                        style={{ backgroundColor: '#ffffff', height: '45px', lineHeight: '45px', padding: '0px 20px' }}>
                        <div className="panel-title pull-left">
                            <a role="button" data-toggle="collapse" href={"#" + this.props.groupName}
                                aria-expanded="true"
                                aria-controls={this.props.groupName}>{this.props.groupName}</a>
                        </div>
                        {flg == "group" &&
                            <div className="pull-right">
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.textBtn}
                                            style={{ cursor: ahuFirst ? 'pointer' : 'not-allowed' }}>
                                            <a
                                                onClick={() => { ahuFirst && this.onBatchSelectSet(ahuGroup.groupId, ahuFirst.unitid) }}
                                                data-toggle="modal"
                                                data-target="#BatchSelectSet"
                                                disabled={ahuFirst ? false : true}
                                                style={{ color: '#738092' }}>
                                                {intl.get(PARAMETER_BATCH_SETTING)}
                                            </a>
                                        </Link>
                                }
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.textBtn}
                                            style={{ cursor: ahuFirst ? 'pointer' : 'not-allowed' }}>
                                            <a
                                                onClick={() => this.onBatchCalculation(ahuGroup.groupId, ahuFirst.unitid)}
                                                data-toggle="modal"
                                                data-target="#BatchCalculation"
                                                disabled={ahuFirst ? false : true}
                                                style={{ color: '#738092' }}>
                                                {intl.get(BATCH_SELECTION)}
                                            </a>
                                        </Link>
                                }
                                {(ahuGroup.groupId == null) ? null :
                                    <span className="pull-left" style={{ color: '#d0d3dd' }}>|</span>
                                }
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.textBtn}
                                            onClick={() => {
                                                onClick('groups', 'edit')
                                                this.props.onUpdateGroupPre(ahuGroup.groupId)
                                            }
                                            }>{intl.get(EDIT_GROUP_INFO)}</Link>
                                }
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.iconBtn} onClick={this.BatchImport} data-toggle="tooltip"
                                            data-placement="top" title={intl.get(IMPORT)}>
                                            <i className="iconfont icon-daoru"></i>
                                        </Link>
                                }
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.iconBtn} onClick={this.BatchExport} data-toggle="tooltip"
                                            data-placement="top" title={intl.get(EXPORT)}>
                                            <i className="iconfont icon-daochu"></i>
                                        </Link>
                                }
                                {
                                    (ahuGroup.groupId == null) ? null :
                                        <Link className={style.iconBtn}
                                            onClick={() => this.props.onDelGroup(ahuGroup.groupId)} data-toggle="tooltip"
                                            data-placement="top" title={intl.get(DELETE_GROUP)}>
                                            <i className="iconfont icon-shanchu"></i>
                                        </Link>
                                }
                                <Link className={style.iconBtn}
                                    onClick={() => this.openNewAhu(ahuGroup.groupId, ahuGroup.groupType)}
                                    data-toggle="tooltip" data-placement="top"
                                    title={intl.get(NEW_AHU)}>
                                    <i className="iconfont icon-tianjia"></i>
                                </Link>
                                <Link onClick={() => this.handleClick(this.props.groupName)}>
                                    <i className="iconfont icon-liebiaozhankai"></i>
                                </Link>
                            </div>
                        }
                        {flg == "setDir" &&
                            <div className="pull-right">
                                <a onClick={() => this.handleClick(this.props.groupName)}>
                                    <i className="iconfont icon-liebiaozhankai"></i>
                                </a>
                            </div>
                        }
                    </div>
                    <div id={this.props.groupName} className="panel-collapse collapse in" role="tabpanel"
                        aria-labelledby="headinggroup6" aria-expanded="true">
                        <div className="panel-body" style={{ padding: '0px' }}>
                            <table className="table table-condensed">
                                <thead>
                                    <tr>
                                        {flg == "setDir" && <th>{intl.get(CHOOSE)}</th>}
                                        <th>{intl.get(UNIT_NUMBER)}</th>
                                        {/* <th>{intl.get(DRAWING_NO)}</th> */}
                                        <th>{intl.get(UNIT_NAME)}</th>
                                        <th>{intl.get(UNIT_MODEL)}</th>
                                        {flg == "setDir" && <th>{intl.get(PIPE_CONNECTION_DIRECTION)}</th>}
                                        {flg == "setDir" && <th>{intl.get(CONSTRUCTION_DOOR_DIRECTION)}</th>}
                                        
                                        {flg == "group" && personal && personal.userRole == 'Factory' ? <th>{intl.get(CUSTOMERPO)}</th> : <th>{intl.get(UNIT_SERIES)}</th>}
                                        {flg == "group" && <th>{intl.get(QUANTITY)}</th>}

                                        {flg == "group" && <th>{intl.get(WEIGHT)}</th>}
                                        {flg == "group" && personal && personal.userRole == 'Factory' ? <th>{intl.get(CONNEXONLISTTYPE)}</th> : <th>{intl.get(PRICE)}</th>}
                                        
                                        {/* {flg == "group" && <th>{intl.get(MODIFY_TIME)}</th>} */}
                                        {flg == "group" && <th>{intl.get(STATE)}</th>}
                                        {flg == "group" && <th>{intl.get(OPERATION)}</th>}
                                    </tr>
                                </thead>
                                <tbody>
                                    {ahus.map((ahu, index) => this.doFilter(ahu) && <AhuDiv key={index}
                                        flg={flg}
                                        data={ahu}
                                        ahuGroup={ahuGroup}
                                        onDelAhu={this.props.onDelAhu}
                                        onCopyAhu={this.props.onCopyAhu}
                                        onInsertAhu={this.props.onInsertAhu}
                                        onFetchCanMoveList={this.props.onFetchCanMoveList}
                                        onMoveAhuGroup={this.props.onMoveAhuGroup}
                                        onCheckDrawingNo={this.props.onCheckDrawingNo}
                                        onUpdateAhuPre={this.props.onUpdateAhuPre}
                                        standard={this.props.standard}
                                        SetStandard={SetStandard}
                                        onClick={onClick}
                                        personal={personal}
                                        ahukey={`${ahu.groupId}_${index}`}
                                        doCleanGroupList={doCleanGroupList}
                                    />)}
                                </tbody>
                            </table>
                            {totalPages > 1 &&
                                <nav aria-label="Page navigation" style={{ textAlign: 'center' }}>
                                    <ul className="pagination">
                                        <li key="-1" className={first && "disabled"}>
                                            <a onClick={() => this.toPage(1)} aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        {
                                            icons.map((index) => <li className={index == page && "active"}
                                                key={index}><a
                                                    onClick={() => this.toPage(index)}>{index}</a></li>)
                                        }
                                        <li key="999" className={last && "disabled"}>
                                            <a onClick={() => this.toPage(totalPages)} aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, owProps) => {
    return {
        ahuFilterStr: state.groups.ahuFilterStr,
        page: state.groups.page,
        personal: state.general.user

    }
}

const mapDispatchToProps = (dispatch, owProps) => {

    return {
        onToPage: (page) => {
            dispatch(fetchGroupInPage(owProps.projectId, owProps.groupId, page))
        },
        SetStandard: (bool, product, series) => {
            dispatch(onSetProduct(product))
            dispatch(onSetSerial(series, product))

            dispatch(onSetStandard(bool))
        },
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDiv)



