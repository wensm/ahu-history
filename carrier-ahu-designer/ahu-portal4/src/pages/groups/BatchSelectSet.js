import {
    PARAMETER_BATCH_CONFIG,
    CONFIGURING,
    UNIT_CONFIGURATION,
    MIX_SECTION,
    SINGLE_FILTRATION_SECTION,
    COMBINED_FILTRATION_SECTION,
    COOLING_COIL_SECTION,
    FAN_SECTION,
    DIRECT_EVAPORATION_COIL,
    CONTROL_SECTION,
    WHEEL_HEAT_RECYCLE,
    PLATE_HEAT_RECYCLE,
    CANCEL,
    SAVE,
    HOT_WATER_COIL_SECTION,
    STEAM_COIL_SECTION,
    ELECTRIC_HEATING_COIL_SECTION,
    WET_FILM_HUMIDIFICATION_SECTION,
    HIGH_PRESSURE_SPRAY_HUMIDIFICATION_SECTION,
    DRY_STEAM_HUMIDIFICATION_SECTION,
    FRESH_RETURN_AIR_SECTION,
    EMPTY_SECTION,
    SILENCE_SECTION,
    OUTLET_AIR_SECTION,
    HIGH_PERFORMANCE_FILTRATION_SECTION,
    ELECTRODE_HUMIDIFIER,
    ELECTROSTATIC_FILTER,
    CONFIGALL,
    CONFIGPART,
    PARAMETER_BATCH_SETTING,
    OK,
    CONFIRMCONFIGALL,
    UNIT_GROUP
} from '../intl/i18n'
import sweetalert from 'sweetalert'

import intl from 'react-intl-universal'
import React from 'react'
import { reduxForm, formValues } from 'redux-form'
import classnames from 'classnames'
import Field from '../ahu/Field'
import style from '../../components/AhuInit.css'
import DateUtil from '../../misc/DateUtils'
import modal_icon1Img from '../../images/modal_icon1.png'
import loadingImg from '../../images/loding.gif'
import { Collapse } from 'antd'
const Panel = Collapse.Panel;
let _this = null
class BatchSelectSet extends React.Component {
    constructor(props) {
        super(props)
        this.onSaveGroupOptionForm = this.onSaveGroupOptionForm.bind(this)
    }

    onSaveGroupOptionForm(type, layout, preferredLocale) {
        _this = this
        if (type != 'only') {
            sweetalert({
                title: intl.get(CONFIRMCONFIGALL),
                // text: "你将无法恢复该虚拟文件！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: false,
                // closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var groupId = $("#BatchSelectSet").data('groupid')
                        // console.log('groupOptionForm', this.props.groupOptionForm)
                        _this.props.onSaveGroupOptionForm(groupId, _this.props.batchComponentValue, _this.props.basicGroupSecList, type, layout, preferredLocale)
                        //   swal("删除！", "你的虚拟文件已经被删除。",
                        //   "success"); 
                    } else {
                        //   swal("取消！", "你的虚拟文件是安全的:)",
                        //   "error"); 
                    }
                })
        } else {
            var groupId = $("#BatchSelectSet").data('groupid')
            // console.log('groupOptionForm', this.props.groupOptionForm)
            _this.props.onSaveGroupOptionForm(groupId, _this.props.batchComponentValue, _this.props.basicGroupSecList, type, layout, preferredLocale)

        }


    }


    render() {
        let { batchComponentValue, basicGroupSecList, layout, preferredLocale } = this.props
        return (
            <div>
                <div className="modal fade" id="BatchSelectSet" data-name="BatchSelectSet" tabIndex="-1" role="dialog"
                    aria-labelledby="myModalLabel" data-groupid="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{ width: '730px' }}>
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">
                                    <b>{intl.get(PARAMETER_BATCH_SETTING)}</b></h4>
                            </div>
                            <div className="modal-body">
                                <BatchOption
                                    saveGroupOptionForm={(type) => this.onSaveGroupOptionForm(type, layout, preferredLocale)}
                                    basicGroupSecList={basicGroupSecList}
                                    groupOptionForm={batchComponentValue}
                                    ahuHasSecList={this.props.ahuHasSecList}
                                    initialValues={this.props.initialValues}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="isSetIng" data-name="isSetIng" tabIndex="-1" role="dialog"
                    aria-labelledby="myModalLabel" data-groupid="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body"
                                style={{ height: '185px', marginLeft: '100px', marginTop: '40px' }}>
                                <div style={{ marginTop: '50px', marginLeft: '100px' }}>
                                    <img style={{ width: '40px', height: '40px', float: 'left' }} src={loadingImg} /><span
                                        style={{
                                            fontSize: '18px',
                                            color: '#999999',
                                            float: 'left',
                                            marginTop: '7px',
                                            marginLeft: '26px'
                                        }}>{intl.get(CONFIGURING)}...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class BatchOption extends React.Component {
    tabItemtzkClick(index) {
    }

    resetSections() {

    }

    render() {
        const { project, ahuHasSecList, groupOptionForm, basicGroupSecList } = this.props


        this.resetSections();
        let obj = {}
        let arr = []
        let nameArr = []
        for (let i = 0; i < ahuHasSecList.length; i++) {
            let parse = JSON.parse(ahuHasSecList[i].metaJson)
            obj = { ...obj, ...parse }
            arr[i] = parse
            nameArr.push(ahuHasSecList[i]['sectionKey'].split('.')[1])

        }
        return (
            <div className="modal-content" style={{ width: '700px' }}>
                <div className="modal-body" style={{ height: '500px', overflow: 'auto' }}>
                    <form data-name="BatchSelectSet">
                        {arr && arr.length > 0 && <Collapse defaultActiveKey={['0']}  >
                            {
                                arr.map((element, index) => {
                                    return (<Panel header={intl.get(`label.${nameArr[index]}3`)} key={`${index}`} >
                                        <Param params={element} index={index} basicGroupSecList={basicGroupSecList} groupOptionForm={groupOptionForm} />
                                    </Panel>)

                                })
                            }
                        </Collapse>}

                    </form>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-default"
                        data-dismiss="modal">{intl.get(CANCEL)}</button>
                    <button type="button" className="btn btn-default"
                        onClick={() => this.props.saveGroupOptionForm()}>{intl.get(CONFIGALL)}</button>
                    <button type="button" className="btn btn-primary"
                        onClick={() => this.props.saveGroupOptionForm('only')}>{intl.get(CONFIGPART)}</button>
                </div>
            </div>
        )
    }
}
class Param extends React.Component {

    render() {
        let { params, index, groupOptionForm, basicGroupSecList } = this.props
        let arr = []
        for (let key in params) {
            let obj = {
                [key]: params[key]
            }
            arr.push(obj)
        }
        return (
            <div className="row">
                {arr.map((element, thisindex) => {
                    return <Ceil element={element} key={thisindex} basicGroupSecList={basicGroupSecList} groupOptionForm={groupOptionForm} />
                })}
            </div>

        )
    }
}
class Ceil extends React.Component {
    render() {
        let { element, groupOptionForm, basicGroupSecList } = this.props
        let thisKey = ''
        for (let key in element) {
            thisKey = key
        }
        // const this_keyName = thisKey.replace(/\./gi, '_')
        // let bool = false
        // if (basicGroupSecList && groupOptionForm && basicGroupSecList[this_keyName] != groupOptionForm[this_keyName]) {
        //     if (this_keyName == "meta_section_mix_damperMeterial" && basicGroupSecList) {
        //     console.log('1', basicGroupSecList, groupOptionForm)
        //     console.log('2', basicGroupSecList['meta_section_mix_damperMeterial'], groupOptionForm['meta_section_mix_damperMeterial'])

        //     }
        //     if (this_keyName == "meta_section_mix_damperOutlet" && basicGroupSecList) {
        //         // console.log('3', basicGroupSecList, groupOptionForm)
        //         // console.log('4', basicGroupSecList['meta_section_mix_damperOutlet'], groupOptionForm['meta_section_mix_damperOutlet'])

        //         }
        //     let compare1 = basicGroupSecList[this_keyName] == '' ? 'false' :basicGroupSecList[this_keyName]
        //     let compare2 = groupOptionForm[this_keyName] == '' ? 'false' :groupOptionForm[this_keyName]
        //     if(compare1 != compare2){
        //         bool = true
        //     }
        // }
        // if (this_keyName == "meta_section_mix_damperMeterial"){
        // console.log('render', basicGroupSecList, groupOptionForm)
        // }

        return (
            <div className="col-lg-6" >
                <Field from="batch" id={thisKey} />
            </div>
        )
    }
}


export default reduxForm({
    form: 'GroupOptionForm', // a unique identifier for this form
    enableReinitialize: true,
})(BatchSelectSet)






