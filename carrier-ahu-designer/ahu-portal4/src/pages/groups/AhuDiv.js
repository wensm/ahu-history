import {
    FUNCTION_DEVELOPING,
    PLEASE_FOLLOW_FOLLOW_UP_VERSION,
    OK,
    CANCEL,
    ASTERISK_NUMBER_1_999,
    TO_LEFT,
    TO_RIGHT,
    EXISTED,
    EDIT_AHU_INFO,
    NON_STANDARD_CONFIGURATION,
    COPY,
    MOVE_GROUP,
    INSERT,
    ARCHIVE,
    DELETE_AHU,
    OPEN_AHU,
    ONE_CLICK_SELECTION,
    MODIFY_SUCCESS,
    CONSTANDARD,
    CONFOREPART,
    CONPOSITIVE,
    CONVERTICAL
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { Link } from 'react-router'
import sweetalert from 'sweetalert'
import DateUtil from '../../misc/DateUtils'
import http from '../../misc/http'
import style from './AhuDiv.css'
import classnames from 'classnames'
import { getStatus } from '../../actions/projects'
import $http from '../../misc/$http'


export default class AhuDiv extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.longToDate = this.longToDate.bind(this)
        this.copyAhu = this.copyAhu.bind(this)
        this.insertAhu = this.insertAhu.bind(this)
        this.openMoveAhuGroup = this.openMoveAhuGroup.bind(this)
        this.changeDrawingNo = this.changeDrawingNo.bind(this)
        this.markAhu = this.markAhu.bind(this)
        this.router = context.router;
        this.state = {
            value: '',
            showStateCustomerName:false,
            showStatePanelType:false

        }
    }


    componentDidMount() {
        $('[data-toggle="tooltip"]').tooltip()
    }
    componentWillUnmount() {

        this.props.doCleanGroupList && this.props.doCleanGroupList()
        // this.setState({
        //     value: ''
        // })
    }

    longToDate(longDate) {
        if (longDate == null || longDate == '') {
            return ""
        }
        return DateUtil.getFormatDateByLong(longDate, 'yyyy-MM-dd hh:mm:ss')
    }

    copyAhu(ahuId) {
        this.props.onCopyAhu(ahuId)
    }

    insertAhu(ahuId) {
        this.props.onInsertAhu(ahuId)
    }

    markAhu(ahuId) {
        sweetalert({
            title: intl.get(FUNCTION_DEVELOPING),
            text: intl.get(PLEASE_FOLLOW_FOLLOW_UP_VERSION),
            type: 'info',
            showCancelButton: true,
            confirmButtonText: intl.get(OK),
            cancelButtonText: intl.get(CANCEL),
            closeOnConfirm: true
        })
    }

    openMoveAhuGroup(ahuId, ahuName) {
        this.props.onFetchCanMoveList(ahuId)
        $('#MoveGroup').data('ahuid', ahuId)
        $('#moveGroupAhuName').text(ahuName)
        $('#MoveGroup').modal('show')
    }

    changeDrawingNo(event) {
        var ahuId = event.target.getAttribute("data-id")
        var drawingNo = event.target.value
        var msgEl = event.target.nextElementSibling
        var tr = $(event.target).closest("tr")
        var addBox = $(tr).children().get(0)
        if (drawingNo == "") {
            msgEl.innerText = intl.get(ASTERISK_NUMBER_1_999)
            msgEl.style.color = '#fd4c4a'
            msgEl.style.display = 'block'
        } else {
            msgEl.style.display = 'none'
            this.props.onCheckDrawingNo(ahuId, drawingNo, msgEl, addBox)
        }
    }
    onBatchCalculation(groupId, ahuId) {
        $("#BatchCalculation").data('groupid', groupId)
        $("#BatchCalculation").data('ahuId', ahuId)
        $('#BatchCalculation').modal({ backdrop: 'static', keyboard: false })

    }
    getDirectionStr(dir) {
        if (dir == 'left') {
            return intl.get(TO_LEFT)
        } else if (dir == 'right') {
            return intl.get(TO_RIGHT)
        } else {
            return ""
        }
    }
    standard = (bool, product, series) => {
        const { SetStandard } = this.props
        SetStandard && SetStandard(bool, product, series)
    }
    onDoubleClick = (data, ahuGroup) => {
        this.standard(true, data.product ? data.product : '39CQ', data.series ? data.series : '')
        this.router.push({ pathname: `projects/${ahuGroup.projectId}/ahus/${data.unitid}`, query: { groupId: ahuGroup.groupId, product: data.product ? data.product : '39CQ' } })
    }


    render() {
        const { data, ahuGroup, onClick, personal, key, ahukey } = this.props
        let {showStateCustomerName, showStatePanelType} = this.state
        var _key = key
        const metaJson = eval('(' + data.metaJson + ')')
        const serial = ""
        if (metaJson != null) {
            this.serial = metaJson["meta.ahu.serial"]
        } else {
            this.serial = ""
        }
        let disabled = data.recordStatus != "selected" && data.recordStatus != "splitted" && data.recordStatus != "panelled";
        let ahukey_ahukey = `${ahukey}_${ahukey}`
        return (

            <tr
            // onDoubleClick = {()=>{
            //     this.standard(true, data.product?data.product:'39CQ')
            //     this.router.push({pathname :`projects/${ahuGroup.projectId}/ahus/${data.unitid}`, query: { groupId: ahuGroup.groupId, product:data.product?data.product:'39CQ'}})
            // }}
            >
                {this.props.flg == "setDir" && <td><input id={data.unitid} className="setDirAhuCheckBox" type="checkbox" /></td>}
                {this.props.flg == "report" &&
                    <td><input id={data.unitid} className={`selAhuCheckBox ${data.recordStatus}`} type="checkbox" disabled={disabled} /></td>
                }
                {/* <td className="add_box">{data.unitNo}</td> */}
                <td>
                    {this.props.flg == "setDir" &&
                        <b className="cir" style={{ position: 'relative' }}>{data.unitNo}</b>}
                    {this.props.flg == "report" &&
                        <b className="cir" style={{ position: 'relative' }}>{data.unitNo}</b>}
                    {this.props.flg == "group" && <b className="cir" style={{ position: 'relative' }}>
                        <input type="number" style={{
                            border: '#cccccc 1px solid',
                            borderRadius: '4px',
                            width: '50px',
                            height: '30px',
                            lineHeight: '30px',
                            textAlign: 'center'
                        }} data-id={data.unitid} key={data.unitNo} defaultValue={Number(data.unitNo)}
                            // value = {this.state.value || data.unitNo}
                            //     onChange={(e)=>{
                            //         let value = e.target.value
                            //         let split = e.target.value.split('')
                            //         console.log('value', value)
                            //         if(e.target.value>0 && e.target.value<10 && split[0]!='0' && split[1]!='0'){
                            //             value = `00${e.target.value}`
                            //         }else if(e.target.value>=10 && e.target.value<100 && split[0]!='0'){
                            //             value = `0${e.target.value}`
                            //         }
                            //         this.setState({
                            //             value:value
                            //         })
                            //     }}
                            onBlur={(event) => {
                                if ($(event.target).val() != this._lastDrawingNo) {
                                    this.changeDrawingNo(event)
                                }
                                this._lastDrawingNo = null;
                            }}
                            onFocus={(event) => {
                                this._lastDrawingNo = $(event.target).val();
                            }}
                        />
                        <i className="tex" style={{
                            color: '#fd4c4a',
                            fontSize: '12px',
                            fontStyle: 'normal',
                            fontWeight: '500',
                            display: 'none',
                        }}>* {intl.get(EXISTED)}</i>
                    </b>
                    }
                </td>
                <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{data.name}</td>
                <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{this.serial}</td>
                {this.props.flg == "group" && personal && personal.userRole == 'Factory' && <td className="customerName"
                //  onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}
                 >
                    <b style={{ position: 'relative' }} id={ahukey}>
                        <input type="type" style={{
                            border: '#cccccc 1px solid',
                            borderRadius: '4px',
                            width: '150px',
                            height: '30px',
                            lineHeight: '30px',
                            textAlign: 'center'
                        }}
                            // defaultValue={data.customerName}
                            value={showStateCustomerName ? this.state[ahukey]:(data.customerName||'')}
                            onChange = {(e)=>{
                                this.setState({
                                    showStateCustomerName :true,
                                    [ahukey]:e.target.value
                                })
                                // $(`#${ahukey}`).val(`${e.target.value}`)
                            }}
                            onFocus={() => {
                                $(`#${ahukey} i`).remove()
                            }}
                            onBlur={(e) => {
                                var that = this
                                let param = {
                                    ahuId: data.unitid,
                                    customerPO: e.target.value
                                }
                                $http.post('ahu/resetCustomerPO', param).then((data) => {
                                    if (data.data == true && data.code == '0' && data.msg == "Success") {
                                        $(`#${ahukey}`).append(`<i style='color: green; font-size: 12px; font-style: normal; font-weight: 500; display: block;' >${intl.get(MODIFY_SUCCESS)}</i>`);

                                    }
                                })
                            }}
                        />
                    </b>

                </td>}

                {this.props.flg == "group" && personal && personal.userRole != 'Factory' && <td className="product" onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{data.product}</td>}

                {this.props.flg == "group" && <td className="num" onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{data.mount}</td>}
                {this.props.flg == "setDir" && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{this.getDirectionStr(metaJson["meta.ahu.pipeorientation"])}</td>}
                {this.props.flg == "setDir" && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{this.getDirectionStr(metaJson["meta.ahu.doororientation"])}</td>}
                {this.props.flg == "group" && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>{data.weight ? data.weight.toFixed(0) : ""}kg</td>}
                {this.props.flg == "group" && personal && personal.userRole == 'Factory' && <td className="customerName" 
                // onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}
                >
                    <b style={{ position: 'relative' }} id={`${ahukey}_${ahukey}`}>
                        <select className="form-control" 
                        // defaultValue={data.paneltype} 

                        value={showStatePanelType ? this.state[ahukey_ahukey]:(data.paneltype||'Standard')}
                            onChange = {(e)=>{
                                this.setState({
                                    showStatePanelType :true,
                                    [ahukey_ahukey]:e.target.value
                                })
                                // $(`#${ahukey}`).val(`${e.target.value}`)
                            }}
                        onFocus={() => {
                            $(`#${ahukey_ahukey} i`).remove()
                        }}
                        onBlur={(e) => {
                            let param = {
                                ahuId: data.unitid,
                                paneltype: e.target.value
                            }
                            $http.post('ahu/setConnexonListType', param).then((data) => {
                                if (data.data == true && data.code == '0' && data.msg == "Success") {
                                    $(`#${ahukey}_${ahukey}`).append(`<i style='color: green; font-size: 12px; font-style: normal; font-weight: 500; display: block;' >${intl.get(MODIFY_SUCCESS)}</i>`);

                                }
                            })
                        }}>
                            <option value="Standard">{intl.get(CONSTANDARD)}</option>
                            <option value="Forepart">{intl.get(CONFOREPART)}</option>
                            <option value="Positive">{intl.get(CONPOSITIVE)}</option>
                            <option value="Vertical">{intl.get(CONVERTICAL)}</option>
                        </select>
                        {/* <input type="type" style={{
                            border: '#cccccc 1px solid',
                            borderRadius: '4px',
                            width: '150px',
                            height: '30px',
                            lineHeight: '30px',
                            textAlign: 'center'
                        }}
                            defaultValue={data.customerName}
                            onFocus={() => {
                                $(`#${ahukey} i`).remove()
                            }}
                            onBlur={(e) => {
                                var that = this
                                let param = {
                                    ahuId: data.unitid,
                                    customerPO: e.target.value
                                }
                                $http.post('ahu/resetCustomerPO', param).then((data) => {
                                    if (data.data == true && data.code == '0' && data.msg == "Success") {
                                        $(`#${ahukey}`).append(`<i style='color: green; font-size: 12px; font-style: normal; font-weight: 500; display: block;' >${intl.get(MODIFY_SUCCESS)}</i>`);

                                    }
                                })
                            }}
                        /> */}
                    </b>

                </td>}
                {this.props.flg == "group" && personal && personal.userRole != 'Factory' && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>￥{data.price ? data.price.toFixed(0) : ""}</td>}
                {/* {this.props.flg == "group" && <td className="time">
                        <p>{this.longToDate(data.updateTime)}</p>
                    </td>} */}
                {this.props.flg == "group" && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}><span className="zt"><em className="rid  graycol"></em>
                    {intl.get(getStatus(data.recordStatus))}
                </span></td>}
                {this.props.flg == "report" && <td onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}><span className="zt"><em className="rid  graycol"></em>
                    {intl.get(getStatus(data.recordStatus))}
                </span></td>}
                {this.props.flg == "group" && <td style={{ whiteSpace: 'nowrap' }} onDoubleClick={() => this.onDoubleClick(data, ahuGroup)}>
                    <Link>
                        <span className={style.gardenIconBtn} onClick={() => {
                            onClick('ahu', 'edit', undefined, undefined, data.unitid)
                            this.props.onUpdateAhuPre(data.unitid)
                        }}
                            // data-toggle="tooltip"
                            //  data-placement="top" 
                            title={intl.get(EDIT_AHU_INFO)}>
                            <i className="iconfont icon-category-copy"></i>
                        </span>
                    </Link>
                    {/*非标*/}
                    {/* <Link href={'/#/' + 'projects/' + ahuGroup.projectId + '/ahus/' + data.unitid} onClick={() => this.standard(false, data.product ? data.product : '39CQ',  data.series ? data.series : '')}>
                        <span className={style.gardenIconBtn} data-toggle="tooltip" data-placement="top"
                            title={intl.get(NON_STANDARD_CONFIGURATION)}>
                            <i className="iconfont icon-biaopei"></i>
                        </span>
                    </Link> */}
                    <span className={classnames(style.tbmorSpanHover)}>
                        <span className={style.gardenIconBtn}><i className="iconfont icon-fuzhi"></i>
                            <span className={classnames(style.tbmorSpan, 'more_icon tbmor_icon')}>
                                <em className="moricon-tex">
                                    <a className="moretex" onClick={() => this.copyAhu(data.unitid)}><span
                                        className="iconfont icon-fuzhi"></span>{intl.get(COPY)}</a>
                                    <a className="moretex"
                                        onClick={() => this.openMoveAhuGroup(data.unitid, data.name)}><span
                                            className="iconfont icon-yizu"></span>{intl.get(MOVE_GROUP)}</a>
                                    <a className="moretex" onClick={() => this.insertAhu(data.unitid)}><span
                                        className="iconfont icon-xinzengAHU"></span>{intl.get(INSERT)}</a>
                                    {/*<a onClick={() => this.markAhu(data.unitid)} className="moretex"><span
                                    className="iconfont icon-guidang"></span>{intl.get(ARCHIVE)}</a>zzf*/}
                                </em>
                                <i></i>
                            </span>
                        </span>
                    </span>

                    <Link
                        onClick={() => this.onBatchCalculation(ahuGroup.groupId, data.unitid)}
                        data-toggle="modal"
                    //  data-target="#BatchCalculation"
                    >

                        <span className={style.gardenIconBtn} data-toggle="tooltip" data-placement="top"
                            title={intl.get(ONE_CLICK_SELECTION)}>
                            <i className="glyphicon glyphicon-sort-by-attributes" ></i>
                        </span>

                    </Link>


                    <span className={style.gardenIconBtn}
                        onClick={() => this.props.onDelAhu(data.unitid)} data-id={data.unitid}>
                        <i className="iconfont icon-shanchu" data-toggle="tooltip" data-placement="top"
                            title={intl.get(DELETE_AHU)}></i>
                    </span>


                    <Link to={{ pathname: 'projects/' + ahuGroup.projectId + '/ahus/' + data.unitid, query: { groupId: ahuGroup.groupId, product: data.product ? data.product : '39CQ' } }} onClick={() => this.standard(true, data.product ? data.product : '39CQ', data.series ? data.series : '')}>
                        <span className={style.gardenIconBtn}><i className="iconfont icon-chakan" data-toggle="tooltip"
                            data-placement="top"
                            title={intl.get(OPEN_AHU)}></i></span>
                    </Link>
                </td>}
            </tr>

        )

    }
}
AhuDiv.contextTypes = {
    router: React.PropTypes.object.isRequired
};
