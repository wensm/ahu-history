import {connect} from 'react-redux'

import Groups from './Groups'
import {
    fetchAhuGroupList,
    fetchGroupTypeList,
    saveBatchImport,
    saveBatchExport,
    saveCreateReport,
    receiveReport,
    clearReports,
    getUsefulNm
} from '../../actions/groups'
import {fetchLayout} from '../../actions/metadata'

const mapStateToProps = (state, owProps) => {
    return {
        projectId: owProps.params.projectId,
        page:state.groups.page,

    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onFetchAhuGroupList(page){
            dispatch(fetchAhuGroupList(owProps.params.projectId, page));
                        // dispatch(fetchLayout())//后端excel的详细信息

        },
        onFetchGroupTypeList(){
            dispatch(fetchGroupTypeList())
        },
        saveBatchImport(formData, groupId,needCover, callBack){
            dispatch(saveBatchImport(formData, groupId, owProps.params.projectId,needCover, callBack));
        },
        saveBatchExport(projectIdNull, fileName, groupid){
            dispatch(saveBatchExport(owProps.params.projectId, fileName, groupid));
        },
        onSaveCreateReport(param, startGeneratingReport){
            dispatch(saveCreateReport(param, startGeneratingReport));
        },
        onReportGenerated(reports){
            dispatch(receiveReport(reports));
        },
        onClearReports(){
            dispatch(clearReports());
        },
        onGetUsefulNm(onGetUsefulNm = '') {
            dispatch(getUsefulNm(onGetUsefulNm))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Groups)
