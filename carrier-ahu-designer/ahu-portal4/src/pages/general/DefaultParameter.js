import {
    HOME,
    DEFAULT_PARAMETER,
    AIR_PARAMETER_SETTING,
    DEFAULT_OPTION_SETTING,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    FRESH_AIR_TEMP_SUMMER,
    FRESH_AIR_TEMP_WINTER,
    PERIPHERAL_PANEL,
    CONFIRM,
    BACK,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {Link,browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {reduxForm} from 'redux-form'
import Group from '../ahu/Group'
import Field from '../ahu/Field'
import {required} from '../ahu/Validate'

class DefaultParameter extends React.Component {
	constructor(props) {
        super();
        this.state = {}
        this.goback = this.goback.bind(this)
    }
	goback(){
      browserHistory.goBack();
    }
    render() {
        const {
            onSave
        } = this.props
        
        return (
            <div>
                <ol className="breadcrumb" style={{marginTop: '20px'}}>
                    <li><Link to="/">{intl.get(HOME)}</Link></li>
                    <li><Link>{intl.get(DEFAULT_PARAMETER)}</Link></li>
                </ol>
                <form data-name="CoolingCoil">
                    <div>
                        <ul className="nav nav-tabs" role="tablist" style={{marginBottom: '10px'}}>
                            <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                                                          role="tab"
                                                                          data-toggle="tab">{intl.get(AIR_PARAMETER_SETTING)}</a>
                            </li>
                            <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                                       data-toggle="tab">{intl.get(DEFAULT_OPTION_SETTING)}</a></li>
                        </ul>
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="summer">
                                <div className="col-lg-6">
                                    <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group4">
                                        <div className="col-lg-12">
                                            <Field id="meta.defaultPara.SInDryBulbT" validate={[required]}/>
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.SInWetBulbT"  validate={[required]}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.SInRelativeT"  validate={[required]}
                                            />
                                        </div>
                                    </Group>
                                </div>
                                <div className="col-lg-6">
                                    <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group5">
                                        <div className="col-lg-12">
                                            <Field id="meta.defaultPara.SNewDryBulbT" validate={[required]}/>
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.SNewWetBulbT"  validate={[required]}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.SNewRelativeT"  validate={[required]}
                                            />
                                        </div>
                                    </Group>
                                </div>
                                <div className="col-lg-6">
                                    <Group title={intl.get(FRESH_AIR_TEMP_SUMMER)} id="group7">
                                        <div className="col-lg-12">
                                            <Field id="meta.defaultPara.WInDryBulbT" validate={[required]}/>
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.WInWetBulbT"  validate={[required]}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.WInRelativeT"  validate={[required]}
                                            />
                                        </div>
                                    </Group>
                                </div>
                                <div className="col-lg-6">
                                    <Group title={intl.get(FRESH_AIR_TEMP_WINTER)} id="group8">
                                        <div className="col-lg-12">
                                            <Field id="meta.defaultPara.WNewDryBulbT" validate={[required]}/>
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.WNewWetBulbT"  validate={[required]}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.defaultPara.WNewRelativeT"  validate={[required]}
                                            />
                                        </div>
                                    </Group>
                                </div>
                            </div>
                            <div role="tabpanel" className="tab-pane" id="winter">
                                <Group title={intl.get(PERIPHERAL_PANEL)} id="group7">
                                    <div className="col-lg-3">
                                        <Field id="meta.defaultPara.MaxWPD" validate={[required]}/>
                                    </div>
                                    <div className="col-lg-3">
                                        <Field
                                            id="meta.defaultPara.AirVolumeUnit"
                                        />
                                    </div>
                                    <div className="col-lg-3">
                                        <Field
                                            id="meta.defaultPara.HeaderMaterial"
                                        />
                                    </div>
                                    <div className="col-lg-3">
                                        <Field id="meta.defaultPara.InternalSkinThickness" validate={[required]}/>
                                    </div>
                                    <div className="col-lg-3">
                                        <Field
                                            id="meta.defaultPara.OutsideSkinThickness"
                                        />
                                    </div>
                                    <div className="col-lg-3">
                                        <Field
                                            id="meta.defaultPara.Power"
                                        />
                                    </div>
                                    <div className="col-lg-3">
                                        <Field id="meta.defaultPara.MotorSupplier"/>
                                    </div>
                                </Group>
                            </div>
                        </div>
                        <div className="col-lg-12" style={{paddingBottom: '10px'}}>
                            <button type="button" className="btn btn-primary" disabled={!this.props.canConfirm ||this.props.invalid} style={{marginLeft: '10px'}}
                                    onClick={() => onSave(this.props.formValues)}>
                                {intl.get(CONFIRM)}
                            </button>
                            <button type="button" className="btn btn-primary" style={{marginLeft: '10px'}}
                                    onClick={this.goback}>
                                {intl.get(BACK)}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default reduxForm({
    form: 'DefaultParameter', // a unique identifier for this form
    enableReinitialize: true,
})(DefaultParameter)
