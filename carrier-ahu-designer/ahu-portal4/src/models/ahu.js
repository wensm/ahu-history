export const SECTION_COOLINGCOIL = "ahu.coolingCoil";
export const SECTION_DIRECTEXPENSIONCOIL = "ahu.directExpensionCoil";
export const SECTION_HEATINGCOIL = "ahu.heatingCoil";
export const SECTION_STEAMCOIL = "ahu.steamCoil";
export const SECTION_ELECTRICHEATINGCOIL = "ahu.electricHeatingCoil";

export function hasRowOfCoilIn357(componentValue) {
    initNSDeformationMeta(componentValue);
    let has = false;
    $('.ahu-section-list').children().each((index, element) => {
        let sectionId = $(element).attr('data-section-id')
        let sectionName = $(element).attr('data-section')
        let sectionProps = componentValue[sectionId]
        if(sectionName && sectionProps) {
            if(isRowOfCoilIn357(sectionName, sectionProps)) {
              has = true;
            }
        }
    })
    return has;
}

function initNSDeformationMeta(componentValue) {
    if(!componentValue[0]['ns_ahu_deformation']) {
        componentValue[0]['ns_ahu_deformation'] = false;
    }
}

function isRowOfCoilIn357(sectionName, meta) {
    let rowMetaKey = getRowMetaKey(sectionName);
    return [3, 5, 7].includes(parseInt(meta[rowMetaKey]));
}

function getRowMetaKey(sectionName) {
    if(sectionName === SECTION_COOLINGCOIL ||
        sectionName === SECTION_DIRECTEXPENSIONCOIL ||
        sectionName === SECTION_HEATINGCOIL) {
        return getMetaSectionKey(sectionName, "rows");
    } else if(sectionName === SECTION_STEAMCOIL ||
        sectionName === SECTION_ELECTRICHEATINGCOIL) {
        return getMetaSectionKey(sectionName, "RowNum");
    }
}

function getMetaSectionKey(sectionName, key) {
    return sectionName.replace('ahu.', 'meta_section_') + "_" + key;
}

// check if has top damper section in AHU
export function hasTopDamper(componentValue) {
    let has = false;
    $('.ahu-section-list').children().each((index, element) => {
        let sectionId = $(element).attr('data-section-id')
        let sectionName = $(element).attr('data-section')
        let sectionProps = componentValue[sectionId]
        if(sectionProps) {
            if(sectionName == 'ahu.fan') {
                let outletDirection = sectionProps['meta_section_fan_outletDirection'];
                if(outletDirection && (outletDirection == 'UBF' || outletDirection == 'UBR')) {
                    has = true;
                }
            } else if(sectionName == 'ahu.mix') {
                let returnTop = sectionProps['meta_section_mix_returnTop'];
                if(returnTop) {
                    has = true;
                }
            } else if(sectionName == 'ahu.discharge') {
                let outletDirection = sectionProps['meta_section_discharge_OutletDirection'];
                if(outletDirection && outletDirection == 'T') {
                    has = true;
                }
            }
        }
    })
    return has;
}
