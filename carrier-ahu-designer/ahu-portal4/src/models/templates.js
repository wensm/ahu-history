import tp1 from '../images/tp1.png'
import tp1_2 from '../images/tp1_2.png'
import tp2 from '../images/tp2.png'
import tp3 from '../images/tp3.png'
import tp4 from '../images/tp4.png'
import tp5 from '../images/tp5.png'
import tp6 from '../images/tp6.png'
import tp7 from '../images/tp7.png'
import tp8 from '../images/tp8.png'
import tp9 from '../images/tp9.png'
import tp9_2 from '../images/tp9_2.png'
import tp10 from '../images/tp10.png'

const groupTypeBasic = "tp1";
const groupTypeBasic2 = "tp1_2";
const groupTypeDoublePipe = "tp2";
const groupTypeFourPipe = "tp3";
const groupTypeFullYear = "tp4";
const groupTypeRunner = "tp5";
const groupTypePlate = "tp6";
const groupTypeVertical1 = "tp7";
const groupTypeVertical2 = "tp8";
const groupTypeDoubleReturn1 = "tp9";
const groupTypeDoubleReturn2 = "tp9_2";
const groupTypeSideBySideReturn = "tp10";

export const GROUP_TYPE_IMAGES = {
    tp1_2, tp1, tp2, tp3, tp4, tp5, tp6, tp7, tp8, tp9, tp9_2, tp10
}

const GROUP_AHU_FILTERS = {};
GROUP_AHU_FILTERS[groupTypeRunner] = 2333; // 热回收机组使用范围0608~2333
GROUP_AHU_FILTERS[groupTypePlate] = 1621;
GROUP_AHU_FILTERS[groupTypeVertical1] = 1621; // 立式机组使用范围0608~1621
GROUP_AHU_FILTERS[groupTypeVertical2] = 1621;

export const GROUP_TYPE_BASIC = groupTypeBasic;
export const GROUP_TYPE_BASIC_2 = groupTypeBasic2;
export const GROUP_TYPE_DOUBLE_PIPE = groupTypeDoublePipe;
export const GROUP_TYPE_FOUR_PIPE = groupTypeFourPipe;
export const GROUP_TYPE_FULL_YEAR = groupTypeFullYear;
export const GROUP_TYPE_RUNNER = groupTypeRunner;
export const GROUP_TYPE_PLATE = groupTypePlate;
export const GROUP_TYPE_VERTICAL_1 = groupTypeVertical1;
export const GROUP_TYPE_VERTICAL_2 = groupTypeVertical2;
export const GROUP_TYPE_DOUBLE_RETURN_1 = groupTypeDoubleReturn1;
export const GROUP_TYPE_DOUBLE_RETURN_2 = groupTypeDoubleReturn2;
export const GROUP_TYPE_SIDE_BY_SIDE_RETURN = groupTypeSideBySideReturn;

export function groupAHUFilter(templateId, ahuSeries) {
    if (templateId && GROUP_AHU_FILTERS[templateId]) {
        return ahuSeries.filter((unitSeries) => {
            let unitNo = unitSeries.ahu.substring(unitSeries.ahu.length - 4) * 1;
            return unitNo <= GROUP_AHU_FILTERS[templateId]
        })
    }
    return ahuSeries;
}