import syncComponentValues from '../pages/ahu/SyncComponentValues.js'
import chainCalculate, { changeFanPicture } from '../pages/ahu/ChainCalculator.js'
import syncnewComponentValuesDrop from '../pages/ahu/syncComponentValuesDrop.js'

import {
    CLICK_COMPONENT,
    COMPLETE_SECTION,
    ADD_AHU_COMPONENT,
    SELECT_AHU_SERIES,
    REMOVE_AHU_SECTION,
    SYNC_AHU_COMPONENT_VALUES,
    UPDATE_COOLING_COIL_BY_INDEX,
    RECEIVE_AHU_INFO,
    RECEIVE_AHU_PRICE,
    RECEIVE_AHU_SERIES,
    RECEIVE_AHU_TEMPLATE,
    RECEIVE_AHU_SECTIONS,
    RECEIVE_AHU_TEMPLATES,
    RECEIVE_AHU_THREE_VIEW,
    RECEIVE_AHU_PSYCHOMETRIC,
    RECEIVE_AHU_BASE_SECTIONS,
    RECEIVE_COOLING_COIL_CALCS,
    RECEIVE_COOLING_COIL_Dele,
    RECEIVE_FAN_CALCS,
    UPDATE_FAN_VALUE,
    RECEIVE_COMPONENT_DEFAULT_VALUE,
    RECEIVE_AHU_SECTIONS_VALIDATION,
    RECEIVE_AHU_SECTION_VALIDATION,
    UPDATE_COMPONENT_VALUE,
    UPDATE_RAVOLUME_VALUE,
    UPDATE_NAVOLUME_VALUE,
    RECEIVE_OUTLET_PARAMS,
    DROP_SECTION,
    AHU_TOGGLE_STANDARD,
    UPDATE_COMPONENT_VALUES,
    RECEIVE_CHOOSE_MODEL,
    RECEIVE_STEAMHUMDIDFIER_CHOOSE_MODEL,
    THREE_VIEW_MODAL_CLOSED,
    initAhuLayout,
    updateLayout,
    getSimpleSectionArr,
    UPDATE_STEAM_HUMIDIFIER_BY_INDEX,
    AHU_SET_STANDARD,
    SYNC_OTHER_PARAM,
    SYNC_UVMODEL,
    cleanCalcute,
    UPDATE_SELECTED,
    DISMISS,
    PMSTATE,
    RECEIVE_AHU_SECTION_VALIDATIONP,
    RESETPERFORMANCEJ,
    calculateUnitValueToBritish,
    RECEIVE_EHC,
    HANDLEOK,
    formatValueForUnit,
    CLEAN_AHU_INFO,
    CLEAN_PRICE,
    SAVE_AHU_PRICE
} from '../actions/ahu'
import isEmpty from 'lodash/isEmpty'

const ahu = (state = {
    sections: [],
    selectedComponent: {
        id: 0,
        name: 'ahu.ahu'
    },
    componentId: 10000,
    componentValue: {},
    templates: [],
    defaultValue: {},
    baseSections: [],
    price: {
        details: []
    },
    threeView: '',
    series: [],
    validation: {
        pass: true,
        sectionPass: true,
        sectionWarn: false
    },
    coolingCoilCalcs: 'clean',
    humidifierCalcs: {},
    steamHumidifierCalcs: {},
    fanCalcs: [],
    standard: true,
    templateId: "tp1",//行業類型
    psychometricModalState: false,
    validationP: '',
    sectionPass: false,
    ElectricHeatingCoil: '',
    currentOptionName: '',
    recordStatus: 'selecting',
    doToolTip: false,
    // sectionMsg:''
}, action) => {

    switch (action.type) {
        case AHU_TOGGLE_STANDARD:
            // console.log('state.standard', state.standard)
            return Object.assign({}, state, {
                standard: !state.standard
            })
        case AHU_SET_STANDARD:
            return Object.assign({}, state, {
                standard: action.standard
            })
        case UPDATE_FAN_VALUE:
            var fanCalcs = state.fanCalcs[action.index]
            var newFanCalcs = {}
            for (var variable in fanCalcs) {
                if (fanCalcs.hasOwnProperty(variable)) {
                    newFanCalcs[variable.replace(/\./gi, '_')] = fanCalcs[variable]
                }
            }
            var componentValues = JSON.parse(JSON.stringify(state.componentValue))
            Object.assign(componentValues[state.selectedComponent.id], newFanCalcs)
            return Object.assign({}, state, {
                componentValue: componentValues,
                fanCalcs: []
            })
        case CLEAN_AHU_INFO:
            return Object.assign({}, state, {
                sections: {},
                componentValue: {},
                layout: '',
                componentId: 10000,
                currentUnit: '',
                templateId: 'tp1'
            })

        case RECEIVE_AHU_INFO:
            state.validation.msg = ''
            state.validation.pass = true
            let { unitSetting, metaUnit, metaLayout, propUnit } = action
            var ahuProps = JSON.parse(action.ahu.unit.metaJson)
            let copythePropsValues = { ...ahuProps }
            //判断若系统为英制，则将数据从公制改成英制
            if (propUnit == 'B') {//B 是 英制,需要切到英制显示到界面
                for (let _key in copythePropsValues) {
                    if (metaLayout[_key] && (metaLayout[_key]['valueType'] == 'DoubleV' || metaLayout[_key]['valueType'] == 'IntV')) {
                        let a = calculateUnitValueToBritish(_key, copythePropsValues[_key], unitSetting['B'], metaUnit, metaLayout)
                        copythePropsValues[_key] = a
                    }
                }
            }

            // . => _
            var newAhuProps = {}
            for (var variable in copythePropsValues) {
                if (copythePropsValues.hasOwnProperty(variable)) {
                    newAhuProps[variable.replace(/\./gi, '_')] = copythePropsValues[variable]
                }
            }
            // overwrite ahu default values
            var finalAhuProps = Object.assign({}, state.componentValue[0], newAhuProps)
            // var componentValue = Object.assign({}, state.componentValue, { 0: finalAhuProps })
            var componentValue = Object.assign({}, { 0: finalAhuProps })
            // Resolve sections
            var sections = action.ahu.parts.sort((a, b) => Number(a.position) - Number(b.position))
            let firstCycleJson = null;
            sections.forEach((section, index) => {
                var props = {}
                const sectionProps = JSON.parse(section.metaJson)
                let copythePropsValues2 = { ...sectionProps }

                if (propUnit == 'B') {//B 是 英制,需要切到英制显示到界面
                    for (let _key in copythePropsValues2) {
                        if (metaLayout[_key] && (metaLayout[_key]['valueType'] == 'DoubleV' || metaLayout[_key]['valueType'] == 'IntV')) {
                            let a = calculateUnitValueToBritish(_key, copythePropsValues2[_key], unitSetting['B'], metaUnit, metaLayout)
                            // console.log('zzfa', _key, a, copythePropsValues2[_key])
                            copythePropsValues2[_key] = a
                        }
                    }
                }
                // . => _
                for (var variable in copythePropsValues2) {
                    if (copythePropsValues2.hasOwnProperty(variable)) {
                        props[variable.replace(/\./gi, '_')] = copythePropsValues2[variable]
                    }
                }
                //位置重新赋值
                props['assis_section_position'] = section.position;
                if (Number(section.position) === 0) {
                    console.error('section position must greater than 0')
                }

                // //热回属性 使用同一个对象引用
                if (["ahu.wheelHeatRecycle", "ahu.plateHeatRecycle"].includes(section.sectionKey)) {
                    if (!firstCycleJson) {
                        firstCycleJson = props;
                        // console.log("找到热回1："+section.position)
                        componentValue[section.position] = props
                    } else {
                        // console.log("找到热回2："+section.position)
                        componentValue[section.position] = firstCycleJson
                    }
                } else {
                    componentValue[section.position] = props
                }
            })

            // Resolve layout info
            let layoutStr = action.ahu.unit.layoutJson
            let layout = {}
            if (layoutStr && (layoutStr.trim() != '')) {
                layout = JSON.parse(layoutStr)
            } else {
                layout = initAhuLayout(sections)

                //layoutData = [[], [], [], []]
            }

            let mSections = [];
            // console.log('layout', layout, sections)
            layout.layoutData.forEach((ls, i) => {
                ls.forEach((idx) => {
                    idx = Number(idx);
                    mSections.push({
                        id: idx,
                        name: sections ? sections[idx - 1].sectionKey : '',
                        line: i <= 1 ? 0 : 1
                    })
                })
            });
            $(`[data-section]`).removeAttr('data-section-selected')//确保保存的时候，所有段都下来，没有选中的状态
            // chainCalculate(mSections, componentValue, state.selectedComponent, 2);
            return Object.assign({}, state, {
                componentValue: componentValue,
                layout: layout,
                sections: sections,
                componentId: sections.length + 1,
                currentUnit: action.ahu.unit,
                recordStatus: action.ahu.unit.recordStatus,
                doToolTip: false,
                templateId: action.ahu.templateId || state.templateId
            })
        case RECEIVE_AHU_SECTIONS:
            var componentValue = {}
            componentValue[0] = Object.assign({}, state.componentValue[0])
            var sections = action.ahuSections.sort((a, b) => Number(a.position) - Number(b.position))
            sections.forEach((section, index) => {
                var props = {}
                const sectionProps = JSON.parse(section.metaJson)
                // . => _
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        props[variable.replace(/\./gi, '_')] = sectionProps[variable]
                    }
                }
                if (Number(section.position) === 0) {
                    console.error('section position must greater than 0')
                }
                componentValue[section.position] = props
            })

            return Object.assign({}, state, {
                componentValue,
                sections: action.ahuSections,
            })
        case ADD_AHU_COMPONENT:
            var section = `_${action.sectionName.split('.')[1]}_`
            // get section default values
            // console.log('state.defaultValue', state, state.defaultValue, action.propUnit)
            var sectionValue = { meta_section_completed: false }
            for (var variable in state.defaultValue[action.propUnit]) {
                // console.log('state.defaultValue[action.propUnit]', state.defaultValue[action.propUnit])
                if (state.defaultValue[action.propUnit].hasOwnProperty(variable)) {
                    if (variable.includes(section)) {
                        sectionValue[variable] = state.defaultValue[action.propUnit][variable]
                    }
                }
            }
            var value = {}
            if (section === '_ahu_') {
                value[0] = sectionValue
            } else {
                value[state.componentId] = sectionValue
            }
            var componentValue = Object.assign({}, state.componentValue, value)
            return Object.assign({}, state, {
                componentValue,
                componentId: state.componentId + 1
            })
        case REMOVE_AHU_SECTION:
            var componentValue = Object.assign({}, state.componentValue)
            let delIndex = componentValue[action.sectionId]['assis_section_position'];
            delete componentValue[action.sectionId]
            let sections = getSimpleSectionArr()
            let nlayoutInfo = updateLayout(state.layout.style, state.componentValue, sections)
            var componentValue = syncnewComponentValuesDrop(componentValue, {
                assis_section_position: delIndex,
                sectionName: action.sectionName,
                line: action.lineIndex,
                sectionIndex: action.sectionIndex
            }, sections, state.defaultValue[action.propUnit], nlayoutInfo.airDirection, 'remove', action.direction, state.templateId)
            return Object.assign({}, state, {
                componentValue,
                recordStatus: 'selecting',
                doToolTip: true,
            })
        case DROP_SECTION:
            sections = getSimpleSectionArr()
            let nlayoutInfo2 = updateLayout(state.layout, state.componentValue, sections)
            // console.log('action.direction.propUnit', action.propUnit)
            componentValue = syncnewComponentValuesDrop(state.componentValue, state.selectedComponent, sections, state.defaultValue[action.propUnit], nlayoutInfo2.airDirection, 'drop', action.direction, state.templateId)
            // Section__Section--3_Xj- Section__discharge--SYZsU hvr-float-shadow
            // Section__Section--3_Xj- Section__discharge--SYZsU hvr-float-shadow
            // Section__Section--3_Xj- Section__fan--1E0Cn hvr-float-shadow"
            // Section__Section--3_Xj- Section__fan--1E0Cn hvr-float-shadow
            // background-imageSection__Section--3_Xj- Section__fan--1E0Cn hvr-float-shadow
            return Object.assign({}, state, {
                componentValue,
                forDirection: nlayoutInfo2,
                recordStatus: 'selecting',
                doToolTip: true,
                // layout:nlayoutInfo2.nlayout,
            })
        case SYNC_AHU_COMPONENT_VALUES:
            // changeFanPicture(state.selectedComponent.id)
            var componentValues = JSON.parse(JSON.stringify(state.componentValue))
            componentValues[state.selectedComponent.id][action.name] = action.newValue
            //            console.log('componentValues[state.selectedComponent.id][action.name]', componentValues[state.selectedComponent.id][action.name])
            if (state.standard) {
                componentValues[state.selectedComponent.id].meta_section_completed = false
            }

            //双层结构
            if (state.layout && state.layout.style == 21 || state.layout && state.layout.style == 22) {
                let newSelemctedComponent = JSON.parse(JSON.stringify(state.selectedComponent))
                let arr1 = state.layout.layoutData[0]
                let arr2 = state.layout.layoutData[2]
                if (arr1.length > 0 && arr1[arr1.length - 1] == state.selectedComponent.id) {
                    newSelemctedComponent.id = arr2[arr2.length - 1]
                } else if (arr2.length > 0 && arr2[arr2.length - 1] == state.selectedComponent.id) {
                    newSelemctedComponent.id = arr1[arr1.length - 1]
                }

                if (newSelemctedComponent.id != state.selectedComponent.id) {
                    componentValues[newSelemctedComponent.id][action.name] = action.newValue
                    componentValues[newSelemctedComponent.id].meta_section_completed = false
                }
            }
            sections = getSimpleSectionArr()
            componentValues = syncComponentValues(action.name, action.newValue, action.previousValue, state.selectedComponent, componentValues, sections, state.baseSections, state)
            componentValues = cleanCalcute(action.name, componentValues, state.selectedComponent, action.metaLayout)
            return Object.assign({}, state, {
                componentValue: componentValues,
                currentOptionName: action.name,
                doToolTip: true,
                recordStatus: 'selecting',
            })
        case CLICK_COMPONENT:
            state.validation.sectionPass = true
            state.validation.sectionWarn = false

            return Object.assign({}, state, {
                selectedComponent: {
                    id: action.id,
                    name: action.name,
                    displayName: action.displayName
                },
                validation: Object.assign({}, state.validation, {
                    sectionPass: true,
                    sectionWarn: false,
                    sectionMsg: ""
                })
            })
        case RECEIVE_COMPONENT_DEFAULT_VALUE:
            let valueM = {}
            let valueB = {}
            // console.log('zzf value ', action)
            for (let variable in action.data['M']) {
                if (action.data['M'].hasOwnProperty(variable)) {
                    Object.assign(valueM, action.data['M'][variable])
                }
            }
            for (let variable in action.data['B']) {
                if (action.data['B'].hasOwnProperty(variable)) {
                    Object.assign(valueB, action.data['B'][variable])
                }
            }
            let M = {}
            let B = {}
            for (let variableM in valueM) {
                if (valueM.hasOwnProperty(variableM)) {
                    M[variableM.replace(/\./gi, '_')] = valueM[variableM]
                }
            }
            for (let variableB in valueB) {
                if (valueB.hasOwnProperty(variableB)) {
                    B[variableB.replace(/\./gi, '_')] = valueB[variableB]
                }
            }
            let defaultValue = {
                M,
                B
            }
            //    console.log('zzffaultValue', defaultValue)
            return Object.assign({}, state, {
                defaultValue
            })
        case RECEIVE_AHU_BASE_SECTIONS:
            //Slice the result into sections data and default parameter data zzf 默认参数
            let length = action.data.length
            return Object.assign({}, state, {
                baseSections: action.data.slice(0, length - 1),
                defaultPara: action.data[action.data.length - 1]
            })
        case RECEIVE_AHU_TEMPLATE:
            return Object.assign({}, state, {
                template: action.data
            })
        case RECEIVE_AHU_TEMPLATES:
            return Object.assign({}, state, {
                templates: action.templates
            })
        case CLEAN_PRICE:
            return Object.assign({}, state, {
                price: {
                    details: []
                }
            })
        case RECEIVE_AHU_PRICE:
            return Object.assign({}, state, {
                price: action.data
            })
        case RECEIVE_AHU_SERIES:

            return Object.assign({}, state, {
                series: action.series,
            })
        case RECEIVE_AHU_SECTIONS_VALIDATION:
            return Object.assign({}, state, {
                validation: Object.assign({}, state.validation, action.data)
            })
        case RECEIVE_AHU_SECTION_VALIDATION:
            return Object.assign({}, state, {
                validation: Object.assign({}, state.validation, action.data)
            })
        case RECEIVE_AHU_SECTION_VALIDATIONP:
            return Object.assign({}, state, {
                validationP: Object.assign({}, state.validation, action.data),
                humidifierCalcs: {},
                sectionPass: false
            })
        case RESETPERFORMANCEJ:
            return Object.assign({}, state, {
                validationP: '',
                humidifierCalcs: {},
                sectionPass: false
            })
        case SELECT_AHU_SERIES:
            var series = state.series.find(serie => serie.ahu === action.series)
            var ahuValue = Object.assign({}, state.componentValue[0])
            var velocity = Number(ahuValue.meta_ahu_sairvolume) / (series.coilFaceArea * 3600)
            var eVelocity = Number(ahuValue.meta_ahu_eairvolume) / (series.coilFaceArea * 3600)
            ahuValue = Object.assign({}, ahuValue, {
                meta_ahu_serial: series.ahu,
                meta_ahu_width: series.width,
                meta_ahu_height: series.height,
                meta_ahu_svelocity: velocity.toFixed(3),
                meta_ahu_evelocity: eVelocity.toFixed(3),
                assis_section_ahu_height: Number(action.series.substr(action.series.length - 4, 2)),
            })
            var componentValue = Object.assign({}, state.componentValue)
            componentValue[0] = ahuValue;
            // let copyComValues = {...state.componentValues}
            for (let key in componentValue) {
                if (componentValue[key].hasOwnProperty('meta_section_completed')) {
                    componentValue[key]['meta_section_completed'] = false
                }
            }

            return Object.assign({}, state, {
                componentValue
            })
        case RECEIVE_COOLING_COIL_CALCS:
            let coolingCoilCalcs = {}
            //            console.log('action', action)
            if (typeof action.data == 'string') {
                coolingCoilCalcs = action.data
            } else {
                if (!isEmpty(action.data)) {
                    coolingCoilCalcs.resultData = action.data.resultData
                    coolingCoilCalcs.season = action.data.season
                    coolingCoilCalcs.sectionKey = action.data.sectionKey
                } else {
                    coolingCoilCalcs = 'clean'
                }
            }
            return Object.assign({}, state, {
                coolingCoilCalcs: coolingCoilCalcs
            })
        case RECEIVE_COOLING_COIL_Dele:
            // 清空表格数据
            return Object.assign({}, state, {
                coolingCoilCalcs: 'clean'
            })
        case RECEIVE_FAN_CALCS:
            let fanCalcs = action.data;
            return Object.assign({}, state, {
                fanCalcs: fanCalcs
            })
        case UPDATE_COOLING_COIL_BY_INDEX:
            var coolingCoilCalc = action.name == 'SprayHumidifier' ? state.humidifierCalcs : state.coolingCoilCalcs

            let nCoolingCoilCalcs = 'clean'
            var data = coolingCoilCalc.resultData[action.index]
            var newCoolingCoilCalc = {}
            // let {unitSetting, metaUnit, metaLayout} = action
            for (var variable in data) {
                if (data.hasOwnProperty(variable)) {
                    let key = (coolingCoilCalc.sectionKey + "." + variable).replace(/\./gi, '_')
                    if (!state.componentValue[state.selectedComponent.id][key]) {
                        key = (coolingCoilCalc.sectionKey + "." + coolingCoilCalc.season + variable).replace(/\./gi, '_')
                        if (action.name == 'SprayHumidifier') {

                            key = (coolingCoilCalc.sectionKey + "." + variable).replace(/\./gi, '_')
                        }
                    }
                    let b = ''
                    if (action.unitPreferCode == 'B' && action.metaLayout[key.replace(/\_/gi, '.')] && (action.metaLayout[key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || action.metaLayout[key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                        b = formatValueForUnit(key, data[variable], action.unitSetting['B'], action.metaUnit, action.metaLayout)
                        // componentValue[key][_key] = b

                    }
                    newCoolingCoilCalc[key] = b ? b : data[variable]
                }
            }
            // formatValueForUnit(name, left, unitPrefer, metaUnit, state.layout)//公制切成英制
            // for (let key in componentValue) {
            //     for (let _key in componentValue[key]) {
            //         if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
            //             let b = formatValueForUnit(_key, componentValue[key][_key], unitSetting, metaUnit, metaLayout)
            //             // console.log('zzf6',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , b)

            //             componentValue[key][_key] = b
            //         }

            //     }
            // }
            //            console.log('newCoolingCoilCalc', newCoolingCoilCalc)
            var coolingCoil = Object.assign({}, state.componentValue[state.selectedComponent.id], newCoolingCoilCalc)
            var componentValue = Object.assign({}, state.componentValue)
            componentValue[state.selectedComponent.id] = coolingCoil

            return Object.assign({}, state, {
                componentValue: componentValue,
                coolingCoilCalcs: nCoolingCoilCalcs
            })
        case RECEIVE_AHU_THREE_VIEW:
            let threeView = action.data
            return Object.assign({}, state, {
                threeView: threeView
            })
        case RECEIVE_AHU_PSYCHOMETRIC:
            return Object.assign({}, state, {
                psychometricPath: action.data,
                // psychometricModalState:true,

            })
        case COMPLETE_SECTION:
            let componentValues1 = state.componentValue;//JSON.parse(JSON.stringify(state.componentValue))
            var result = {}
            sections = getSimpleSectionArr()
            for (var variable in action.data) {
                if (action.data.hasOwnProperty(variable)) {
                    let b = ''
                    if (action.unitPreferCode == 'B' && action.metaLayout[variable] && (action.metaLayout[variable]['valueType'] == 'DoubleV' || action.metaLayout[variable]['valueType'] == 'IntV')) {
                        b = formatValueForUnit(variable.replace(/\./gi, '_'), action.data[variable], action.unitSetting['B'], action.metaUnit, action.metaLayout)
                    }

                    result[variable.replace(/\./gi, '_')] = b ? b : action.data[variable]
                }
            }

            // Object.assign(componentValues1[state.selectedComponent.id], result)
            let copy = { ...state.componentValue }
            copy[state.selectedComponent.id] = { ...componentValues1[state.selectedComponent.id], ...result }

            if (state.selectedComponent.name == 'ahu.wheelHeatRecycle' || state.selectedComponent.name == 'ahu.plateHeatRecycle') {//这俩段是两个一样的，所以需要都修改参数
                let arr = []
                sections.forEach((record) => {
                    if (record.name == 'ahu.wheelHeatRecycle' || record.name == 'ahu.plateHeatRecycle') {
                        arr.push(record.id)
                    }
                })
                arr.forEach((_record) => {
                    copy[_record] = { ...componentValues1[_record], ...result }
                })
            }


            componentValues1 = chainCalculate(sections, copy, state.selectedComponent, 1, 'confirm', action.direction, null, state.layout, action.isNS)
            if (!action.isNS) {
                componentValues1[state.selectedComponent.id]['meta_section_completed'] = true
            }
            return Object.assign({}, state, {
                componentValue: componentValues1,
                doToolTip: true,

            })
        case UPDATE_COMPONENT_VALUE:
            let componentValues2 = JSON.parse(JSON.stringify(state.componentValue))
            let actionName1 = action.propUnit == 'B' ? formatValueForUnit(action.name, action.value, action.unitSetting['B'], action.metaUnit, action.metaLayout) : action.value
            componentValues2[state.selectedComponent.id][action.name] = actionName1.toFixed(3)
            let actionself1 = action.propUnit == 'B' ? formatValueForUnit(action.self, action.selfValue, action.unitSetting['B'], action.metaUnit, action.metaLayout) : action.selfValue
            componentValues2[state.selectedComponent.id][action.self] = actionself1

            return Object.assign({}, state, {
                componentValue: componentValues2
            })
        case UPDATE_RAVOLUME_VALUE:
            var componentValue = JSON.parse(JSON.stringify(state.componentValue))
            componentValue[state.selectedComponent.id][action.name[0]] = action.value
            componentValue[state.selectedComponent.id][action.name[1]] = action.value
            componentValue[state.selectedComponent.id][action.self[0]] = action.selfValue
            componentValue[state.selectedComponent.id][action.self[1]] = action.selfValue
            return Object.assign({}, state, {
                componentValue: componentValue
            })
        case UPDATE_NAVOLUME_VALUE:
            var componentValue = JSON.parse(JSON.stringify(state.componentValue))
            componentValue[state.selectedComponent.id][action.name[0]] = action.value
            componentValue[state.selectedComponent.id][action.name[1]] = action.value
            componentValue[state.selectedComponent.id][action.self[0]] = action.selfValue
            componentValue[state.selectedComponent.id][action.self[1]] = action.selfValue
            return Object.assign({}, state, {
                componentValue: componentValue
            })
        case UPDATE_COMPONENT_VALUES:

            let componentValues = JSON.parse(JSON.stringify(state.componentValue))
            let componentValues11 = { ...componentValues }
            let data1 = action.data.data
            let frontName = action.names[0].split('_')
            frontName = frontName.slice(0, frontName.length - 1).join('_')
            let values1 = {}

            action.names.forEach((key, index) => {
                let b = ''
                if (action.propUnit == 'B' && action.metaLayout[key.replace(/\_/gi, '.')] && (action.metaLayout[key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || action.metaLayout[key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                    b = formatValueForUnit(key, action.data.data[action.resultKeys[index]], action.unitSetting['B'], action.metaUnit, action.metaLayout)
                    // componentValue[key][_key] = b

                }
                values1[key] = b ? b : action.data.data[action.resultKeys[index]]

            })
            for (let key1 in values1) {
                componentValues11[state.selectedComponent['id']][key1] = values1[key1]
            }

            return Object.assign({}, state, {
                componentValue: componentValues11
            })
        case RECEIVE_OUTLET_PARAMS:
            var componentValues = JSON.parse(JSON.stringify(state.componentValue))
            if (action.season === 'summer') {
                componentValues[state.selectedComponent.id].meta_section_mix_SOutDryBulbT = action.outletParams.parmT.toFixed(3)
                componentValues[state.selectedComponent.id].meta_section_mix_SOutWetBulbT = action.outletParams.parmTb.toFixed(3)
                componentValues[state.selectedComponent.id].meta_section_mix_SOutRelativeT = action.outletParams.parmF.toFixed(3)
            } else if (action.season === 'winter') {
                componentValues[state.selectedComponent.id].meta_section_mix_WOutDryBulbT = action.outletParams.parmT.toFixed(3)
                componentValues[state.selectedComponent.id].meta_section_mix_WOutWetBulbT = action.outletParams.parmTb.toFixed(3)
                componentValues[state.selectedComponent.id].meta_section_mix_WOutRelativeT = action.outletParams.parmF.toFixed(3)
            }
            return Object.assign({}, state, {
                componentValue: componentValues
            })
        case RECEIVE_CHOOSE_MODEL:
            let humidifierCalcs = {}
            humidifierCalcs.resultData = action.data.data.resultData
            humidifierCalcs.season = action.data.data.season
            humidifierCalcs.sectionKey = action.data.data.sectionKey

            return Object.assign({}, state, {
                humidifierCalcs: humidifierCalcs,
                sectionPass: true
            })
        case RECEIVE_STEAMHUMDIDFIER_CHOOSE_MODEL:
            let steamHumidifierCalcs = {}
            let resultDataCopy = []
            // action.data.data.resultData.forEach((element, index) => {//适配后台数据
            //     let elementCopy = { ...element }
            //     elementCopy.hTypes = element.htypes
            //     delete elementCopy.htypes
            //     resultDataCopy.push(elementCopy)
            // })
            steamHumidifierCalcs.resultData = action.data.data.resultData
            steamHumidifierCalcs.season = action.data.data.season
            steamHumidifierCalcs.sectionKey = action.data.data.sectionKey
            return Object.assign({}, state, {
                steamHumidifierCalcs: steamHumidifierCalcs
            })
        case THREE_VIEW_MODAL_CLOSED:
            return Object.assign({}, state, { threeView: '' });
        case UPDATE_STEAM_HUMIDIFIER_BY_INDEX:
            let componentValueCopy = { ...state.componentValue }

            for (let key in componentValueCopy) {
                for (let _key in componentValueCopy[key]) {
                    if (_key == 'meta_section_steamHumidifier_HTypes') {
                        componentValueCopy[key][_key] = action.resultData.HTypes
                    }
                    if (_key == 'meta_section_steamHumidifier_VaporPressure') {
                        componentValueCopy[key][_key] = action.resultData.VaporPressure
                    }
                }
            }
            componentValueCopy[state.selectedComponent['id']]['meta_section_steamHumidifier_humidificationQ'] = action.resultData.humidificationQ

            return Object.assign({}, state, {
                componentValue: componentValueCopy
            })
        case SYNC_OTHER_PARAM:
            let copyComValue = { ...state.componentValue }
            let copyDefaultValue = { ...state.defaultValue }
            // console.log('copyDefaultValue', copyDefaultValue, action)
            // let copyDefaultValueUnit = { ...copyDefaultValue[action.unitPreferCode] }
            for (let key in copyComValue) {
                // console.log('defaultValue[action.unitPreferCode]', action.unitPreferCode, key)
                if (copyComValue[key]['meta_section_airDirection'] == action.direction && copyComValue[key].hasOwnProperty(action.name)) {
                    copyComValue[key][action.name] = action.newValue;
                    copyDefaultValue[action.unitPreferCode][action.name] = action.newValue
                }
            }
            return Object.assign({}, state, {
                componentValue: copyComValue,
                defaultValue: copyDefaultValue
            })
        case SYNC_UVMODEL:
            let syncuvmodel = { ...state.componentValue }
            for (let key in syncuvmodel) {
                if (syncuvmodel[key].hasOwnProperty('meta_section_access_uvModel')) {
                    syncuvmodel[key]['meta_section_access_uvModel'] = action.value
                }
            }

            return Object.assign({}, state, {
                componentValue: syncuvmodel,
            })
        case UPDATE_SELECTED:
            return Object.assign({}, state, {
                selectedComponent: action.selected,
            })
        case DISMISS:
            let psychometricModalState = action ? action.bool : state.psychometricModalState
            return Object.assign({}, state, {
                psychometricModalState: psychometricModalState,
            })
        case PMSTATE:
            let psychometricModalState2 = action.bool
            return Object.assign({}, state, {
                psychometricModalState: psychometricModalState2,
            })
        case RECEIVE_EHC:
            let ElectricHeatingCoil = {}
            if (typeof action.data == 'string') {
                ElectricHeatingCoil = action.data
            } else {
                if (!isEmpty(action.data)) {
                    ElectricHeatingCoil.resultData = action.data.resultData
                    ElectricHeatingCoil.sectionKey = action.data.sectionKey
                }
            }
            return Object.assign({}, state, {
                ElectricHeatingCoil: ElectricHeatingCoil
            })
        case HANDLEOK:
            let newComponentValues = { ...state.componentValue }
            for (let key in action.data) {
                newComponentValues[state.selectedComponent.id][key.replace(/\./gi, '_')] = action.data[key]
            }
            return Object.assign({}, state, {
                componentValue: newComponentValues
            })
        case SAVE_AHU_PRICE:
            // let copyCom = { ...state.componentValue }
            let copyCom = JSON.parse(JSON.stringify(state.componentValue))
            
            if (action.data.hasOwnProperty('ns.ahu.nsChannelSteelBasePrice')) {
                copyCom[0].ns_ahu_nsChannelSteelBasePrice = action.data['ns.ahu.nsChannelSteelBasePrice']
            } else if (action.data.hasOwnProperty('ns_ahu_nsChannelSteelBasePrice')) {
                copyCom[0].ns_ahu_nsChannelSteelBasePrice = action.data['ns_ahu_nsChannelSteelBasePrice']
            }
            if (action.data.hasOwnProperty('ns.ahu.deformationPrice')) {
                copyCom[0].ns_ahu_deformationPrice = action.data['ns.ahu.deformationPrice']
            } else if (action.data.hasOwnProperty('ns_ahu_deformationPrice')) {
                copyCom[0].ns_ahu_deformationPrice = action.data['ns_ahu_deformationPrice']
            }
            return Object.assign({}, state, {
                componentValue: copyCom
            })
        default:
            return state
    }
}

export default ahu
