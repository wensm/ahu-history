import {
	SAVE_SUCCESS,
	PROJECT_CREATION_SUCCESS,
	SAVE_FAILED,
	PROJECT_CREATION_FAILED,
	UPLOAD_SUCCESS,
	UPLOAD_FAILED,
	SELECTION_CONFIGURING,
	SECTION_UNSPLIT,
	IMPORT_COMPLETE,
	SELECTION_COMPLETE,
	SPLIT_SECTION_COMPLETE,
	PANEL_ARRANGEMENT_COMPLETE,
	ARCHIVED,
	UNKNOWN_STATE,
	GENERATE_FAILED
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import http from '../misc/http'
import $http from '../misc/$http'
import NProgress from 'nprogress'
import sweetalert from 'sweetalert'

export const RECEIVE_PROJECTS = 'RECEIVE_PROJECTS'
export const PROJECT_PROKEY = 'PROJECT_PROKEY'
export const PROJECT_ID = 'PROJECT_ID'
export const PROJECT_REPORTS = 'PROJECT_REPORTS'
export const CLEAR_REPORTS = 'CLEAR_REPORTS'
export const RECEIVE_UPDATE_PROJECT = 'RECEIVE_UPDATE_PROJECT'
export const RECEIVE_PAGE_PROJECT_FILTERUPDATE = 'RECEIVE_PAGE_PROJECT_FILTERUPDATE'
export const START_NEW_PROJECT = 'START_NEW_PROJECT'
export const CHANGE_ARCHIVED = 'CHANGE_ARCHIVED'

export function fetchProjects(param = 'all') {
	let obj = {
		recordStatus: param
	}
	return dispatch => $http.get('project/list', obj).then(data => dispatch(receiveProjects(data.data)))
}

function receiveProjects(projects) {
	return {
		type: RECEIVE_PROJECTS,
		projects
	}
}

export function saveProject(peoject, archived) {
	return dispatch => $http.post('project/save', peoject).then(data => {
		if (data.code === '0' && data.msg === 'Success') {
			sweetalert({
				title: intl.get(SAVE_SUCCESS),
				text: intl.get(PROJECT_CREATION_SUCCESS),
				timer: 1000,
				type: 'success',
				showConfirmButton: false
			})

			// dispatch(changeArchived('all'));
			dispatch(fetchProjects(archived));
		} else {
			sweetalert({
				title: intl.get(SAVE_FAILED),
				text: intl.get(PROJECT_CREATION_FAILED),
				timer: 1000,
				type: 'error',
				showConfirmButton: false
			})
		}
		$("#newProject").collapse('hide');
		dispatch(receiveUpdateProject({}));
	})
}

export function delProject(peojectId, archived) {
	return dispatch => $http.post('project/delete/' + peojectId).then(data => {
		// dispatch(changeArchived('all'));
		dispatch(fetchProjects(archived));
	});
}

export function saveBatchImport(formData, projectId, callBack, archived) {
	return dispatch => $.ajax({
		url: `${SERVICE_URL}import/proj` + (projectId ? (`?projectId=` + projectId) : ``),
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function (data) {
			if (data.code == "0") {
				sweetalert({
					title: intl.get(UPLOAD_SUCCESS), type: 'success',
					// timer: 1000,
					showConfirmButton: true
				});
				// dispatch(changeArchived('all'));

				dispatch(fetchProjects(archived));
			} else {
				if (data.code == "226") {
					let msg = data.msg.replace("name", data.data.name);
					callBack && callBack(msg);
					return;
				} else {
					sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false });
				}
			}
			callBack && callBack()
		},
		error: function () {
			sweetalert({ title: intl.get(UPLOAD_FAILED), type: 'error', timer: 1000, showConfirmButton: true });
			callBack && callBack()
			$('#BatchImport').modal('hide')
		}
	});
}

export function saveBatchExport(id, fileName) {
	return dispatch => {
		$('#BatchExport').modal('hide');
		// window.location = `${SERVICE_URL}` + 'export/proj/' + id + '?filename=' + fileName;
		let a = `${SERVICE_URL}` + 'export/proj/' + id + '?filename=' + fileName
		window.open(a)
	}
}

export function fetchprokey(id) {
	return dispatch => $http.get('mc/getpc/' + id).then(data => {
		dispatch(receiveProjectProkey(data.data.prokey));
		dispatch(receiveProjectId(data.data.projid));
	});
}

function receiveProjectProkey(prokey) {
	return {
		type: PROJECT_PROKEY,
		prokey
	}
}

function receiveProjectId(projectId) {
	return {
		type: PROJECT_ID,
		projectId
	}
}

export function saveAhuInit(id, AhuInitValues) {
	return dispatch => $http.post('mc/savepc', {
		projid: id,
		prokey: JSON.stringify(AhuInitValues)
	}).then(data => {
		if (data.code == 0) {
			sweetalert({ title: intl.get(SAVE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false });
		} else {
			sweetalert({ title: intl.get(SAVE_FAILED), type: 'error', timer: 1000, showConfirmButton: false });
		}
		$('#AhuInit').modal('hide')
	})
}

export function saveCreateReport(param, startGeneratingReport) {
	$("#generatingReport").modal('show')
	return dispatch => $http.post('report', {
		"reports": JSON.stringify(param)
	}).then(data => {
		if (data.code == 0) {
			startGeneratingReport();
		} else {
			sweetalert({ title: intl.get(GENERATE_FAILED), type: 'error', timer: 1000, showConfirmButton: false });
		}
	})
}

export function receiveReport(reports) {
	return {
		type: PROJECT_REPORTS,
		reports
	}
}

export function clearReports() {
	return {
		type: CLEAR_REPORTS
	}
}

export function updateProjectPre(projectId) {
	return dispatch => $http.get('project/' + projectId).then(data => {
		dispatch(receiveUpdateProject(data.data));
		$("#newProject").collapse('show');
	});
}

function receiveUpdateProject(updatePeoject) {
	return {
		type: RECEIVE_UPDATE_PROJECT,
		updatePeoject
	}
}


export function projectPageFilterUpdate(filterStr) {
	return {
		type: RECEIVE_PAGE_PROJECT_FILTERUPDATE,
		filterStr: filterStr
	}
}
export function changeArchived(value) {
	return {
		value: value,
		type: CHANGE_ARCHIVED,
	}
}

export function getStatus(status) {
	if (status == 'selecting') {
		return SELECTION_CONFIGURING
	} else if (status == 'unsplit') {
		return SECTION_UNSPLIT
	} else if (status == 'imported') {
		return IMPORT_COMPLETE
	} else if (status == 'selected') {
		return SELECTION_COMPLETE
	} else if (status == 'splitted') {
		return SPLIT_SECTION_COMPLETE
	} else if (status == 'panelled') {
		return PANEL_ARRANGEMENT_COMPLETE
	} else if (status == 'archieved') {
		return ARCHIVED
	} else {
		return UNKNOWN_STATE
	}


}

export function toArchieved(projectId, archived) {
	return dispatch => $http.post(`project/archieved/${projectId}`).then(data => {
		console.log('data', data)
		true
		if(data.data == true ){
			dispatch(fetchProjects(archived));
			
		}
		// dispatch(receiveUpdateProject(data.data));
		// $("#newProject").collapse('show');
	});
}
