import {
    SAVE_SUCCESS,
    DELETE_SUCCESS,
    COPY_SUCCESS,
    INSERT_SUCCESS,
    MOVE_GROUP_SUCCESS,
    UPLOAD_SUCCESS,
    UPLOAD_FAILED,
    SAVE_FAILED,
    BATCH_SELECTION_IN_QUOTATION_PHASE_ONLY,
    OK_SELECT_IT,
    OK,
    CANCEL,
    DRAWING_NO_IS,
    UNIT_CONFIGURATION_SUCCESS,
    UNIT_CONFIGURATION_FAILED,
    CONFIGURATION_FAILED,
    MODIFY_SUCCESS,
    GENERATE_FAILED,
    IMPORT_RESULT
} from '../pages/intl/i18n'
import { updateSelected } from './ahu'
import intl from 'react-intl-universal'
import http from '../misc/http'
import $http from '../misc/$http'
import NProgress from 'nprogress'
import sweetalert from 'sweetalert'
// import {RECEIVE_AHU_INFO} from "./ahu"
import { isEmpty } from 'lodash'
import { Modal } from 'antd';
import React from 'react'


export const RECEIVE_GROUP = 'RECEIVE_GROUP'
export const RECEIVE_AHU = 'RECEIVE_AHU'
export const RECEIVE_GROUP_TYPE_LIST = 'RECEIVE_GROUP_TYPE_LIST'
export const USEFUL_NUMBER = 'USEFUL_NUMBER'
export const CHECK_USEFUL_NM_RES = 'CHECK_USEFUL_NM_RES'
export const CHECK_GROUP_NAME_RES = 'CHECK_GROUP_NAME_RES'
export const CAN_MOVE_LIST = 'CAN_MOVE_LIST'
export const RECEIVE_AHU_HAS_SEC = 'RECEIVE_AHU_HAS_SEC'
export const RECEIVE_GROUP_SEC_LIST = 'RECEIVE_GROUP_SEC_LIST'
export const RECEIVE_GROUP_CALC_DATA = 'RECEIVE_GROUP_CALC_DATA'
export const GROUPS_REPORTS = 'GROUPS_REPORTS'
export const CLEAR_REPORTS = 'CLEAR_REPORTS'
export const RECEIVE_UPDATE_GROUP = 'RECEIVE_UPDATE_GROUP'
export const RECEIVE_UPDATE_AHU = 'RECEIVE_UPDATE_AHU'
export const CANCEL_EDIT_AHU = 'CANCEL_EDIT_AHU'
export const RECEIVE_PAGE_GROUP_FILTERUPDATE = 'RECEIVE_PAGE_GROUP_FILTERUPDATE'
export const RECEIVE_AHU_IN_GROUP_PAGE = 'RECEIVE_AHU_IN_GROUP_PAGE'
export const TabId = "TabId"
export const SYNC_BATCH_VALUES = 'SYNC_BATCH_VALUES'
export const RESET = 'RESET'
export const CLEANGROUPLIST = 'CLEANGROUPLIST'
export const CLEAN_USEFUL_NM = 'CLEAN_USEFUL_NM'
export const RECEIVE_AHULIST_FORREPORT = 'RECEIVE_AHULIST_FORREPORT'


export function setAhuListDirection(projectId, ahuList, doorOrientation, pipeOrientation) {
    return dispatch => $http.post('project/setdirection', {
        projectId: projectId,
        ahuIds: ahuList.join(','),
        doororientation: doorOrientation,
        pipeorientation: pipeOrientation
    }).then(data => {
        if (data.code == 0) {
            sweetalert({ title: intl.get(SAVE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchAhuGroupList(projectId));
        } else {
            sweetalert({ title: data.msg, type: 'error' })
        }

    });
}

export function fetchAhuGroupList(projectId, page) {
    // console.log('zzf10012')

    return dispatch => $http.get('group/lists/' + projectId).then(data => {
        dispatch(receiveGroups(data.data))
        dispatch(fetchAhus(projectId, page))
        dispatch(updateSelected({ id: 0, name: 'ahu.ahu' }))
    })
}

export function fetchGroups(projectId) {
    return dispatch => $http.get('group/lists/' + projectId).then(data => dispatch(receiveGroups(data.data)))
}
export function setTab(id) {
    return tabid(id)
}
function tabid(id) {
    return {
        type: TabId,
        id
    }
}

function receiveGroups(groupsList) {
    return {
        type: RECEIVE_GROUP,
        groupsList
    }
}

export function fetchAhus(projectId, page = 1) {
    return dispatch => $http.get('ahu/listinpage', {
        projectId: projectId,
        groupId: "",
        page: page,
        count: 20
    }).then(data => dispatch(receiveAhus(data.data, page)))
}


export function fetchGroupInPage(projectId, groupId, page) {
    return dispatch => $http.get('ahu/listinpage', {
        projectId: projectId,
        groupId: groupId,
        page: page,
        count: 20
    }).then(data => dispatch(receiveAhuInGroupPages(data.data, groupId, page)))
}

function receiveAhuInGroupPages(ahuLists, groupId, page) {
    return {
        type: RECEIVE_AHU_IN_GROUP_PAGE,
        ahuLists,
        groupId,
        page
    }
}

function receiveAhus(ahuLists, page) {
    return {
        type: RECEIVE_AHU,
        ahuLists,
        page
    }
}

export function fetchGroupTypeList() {
    return dispatch => $http.get('grouptype/list').then(data => dispatch(receiveGroupTypeList(data.data)))
}

function receiveGroupTypeList(groupTypeList) {
    return {
        type: RECEIVE_GROUP_TYPE_LIST,
        groupTypeList
    }
}

export function checkGroupName(name, projectId) {
    return dispatch => $http.get('group/checkname/' + name + '/' + projectId).then(data => dispatch(receiveCheckGroupName(data.data)))
}

function receiveCheckGroupName(checkGroupNameRes) {
    return {
        type: CHECK_GROUP_NAME_RES,
        checkGroupNameRes
    }
}

export function saveGroup(group, callBack) {
    var projectId = group['projectId']
    if (group.groupType == -1) delete group.groupType
    return dispatch => (group.groupId ? $http.put('group/update', group) : $http.post('group/add', group)).then(data => {
        if (data.code == "0") {
            callBack()
            sweetalert({ title: intl.get(SAVE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId))
            })

        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
        $('#newGroup').collapse('hide')
        dispatch(receiveUpdateGroup({}))
    })
}

export function delGroup(groupId, projectId) {
    return dispatch => $http.delete('group/delete/' + groupId).then(data => {
        if (data.code == "0") {
            sweetalert({ title: intl.get(DELETE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {

                dispatch(fetchAhus(projectId))
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}

export function getUsefulNm(projectid) {
    return dispatch => $http.get('ahu/getusefulnum' + "/" + projectid).then(data => dispatch(receiveUsefulNm(data.data)))
}

function receiveUsefulNm(usefulNumber) {
    return {
        type: USEFUL_NUMBER,
        usefulNumber
    }
}

export function checkUsefulNm(value, projectid) {
    return dispatch => $http.get('ahu/checkno/' + value + '/' + projectid).then(data => dispatch(receiveCheckUsefulNm(data.data)))
}

function receiveCheckUsefulNm(checkUseFulNmRes) {
    return {
        type: CHECK_USEFUL_NM_RES,
        checkUseFulNmRes
    }
}


export function cleanUsefulNm() {
    return {
        type: CLEAN_USEFUL_NM,
    }
}

export function saveAhu(ahu, page) {
    var projectId = ahu['pid']
    return dispatch => $http.post('ahu/save', ahu).then(data => {
        if (data.code == "0") {
            sweetalert({ title: intl.get(SAVE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId, page))
                dispatch({
                    type: CANCEL_EDIT_AHU
                })
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
        $("#newAhu").data("groupid", "")
        $("#newAhu").data("grouptype", "")
        // $('#newAhu').collapse('hide')
    })
}

export function cancleAhu() {
    // $('#newAhu').data("update", false)
    return dispatch => dispatch({
        type: CANCEL_EDIT_AHU
    })
}

export function delAhu(ahuId, projectId) {
    return dispatch => $http.post('ahu/delete/' + ahuId).then(data => {
        if (data.code == "0") {
            sweetalert({ title: intl.get(DELETE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                /*let timeout = setTimeout(() => {
                    history.go(0)
                    clearTimeout(timeout)
                    }, 1500)*/
                dispatch(fetchAhus(projectId))
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}


export function copyAhu(ahuId, projectId) {
    return dispatch => $http.get('ahu/copy/' + ahuId).then(data => {
        if (data.code == "0") {
            sweetalert({ title: intl.get(COPY_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId))
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}

export function insertAhu(ahuId, projectId) {
    return dispatch => $http.get('ahu/insert/' + ahuId).then(data => {
        if (data.code == "0") {
            sweetalert({ title: intl.get(INSERT_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId))
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}

export function fetchCanMoveList(ahuId, projectId) {
    return dispatch => $http.get('group/lists/' + projectId + '/' + ahuId).then(data => dispatch(receiveCanMoveList(data.data)))
}

function receiveCanMoveList(canMoveList) {
    return {
        type: CAN_MOVE_LIST,
        canMoveList
    }
}

export function saveMoveGroup(ahuId, groupId, projectId) {
    return dispatch => $http.get('ahu/resetgroup/' + ahuId + '/' + groupId).then(data => {
        if (data.code == "0") {
            $('#MoveGroup').data('ahuid', "")
            $('#moveGroupAhuName').text("")
            $('#MoveGroup').modal('hide')
            sweetalert({ title: intl.get(MOVE_GROUP_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId))
            })
        } else if (data.code == "1") {
            sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}

export function saveBatchImport(formData, groupId, projectId, needCover, callBack) {
    return dispatch => {
        var ajaxUrl = ""
        // console.log('zzf8', formData)
        if (groupId == "" || groupId == undefined || groupId == null) {
            ajaxUrl = `${SERVICE_URL}import/ungroup/`
            ajaxUrl += projectId
        } else {
            ajaxUrl = `${SERVICE_URL}import/group/`
            ajaxUrl += projectId
            ajaxUrl += "/" + groupId
            ajaxUrl += "?model=" + (needCover ? 1 : 0)
        }
        $.ajax({
            url: ajaxUrl,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.code == "0") {
                    let arr = []
                    for (let key in data.data) {
                        let sp = data.data[key].split(':')
                        let p1 = <p>{`${sp[0]}：`}</p>
                        let p2 = <p style={{ color: 'red' }}>{sp[1]}</p>
                        if (sp.length >1 && sp[1] != '') {
                            arr.push(p1)
                            arr.push(p2)
                        }
                    }
                    Modal.info({
                        title: intl.get(IMPORT_RESULT),
                        width: '1000px',
                        content: (
                            <div>
                                {arr.map((ele) => {
                                    return ele
                                })}
                            </div>
                        ),
                        onOk() { },
                    });


                    dispatch(fetchGroups(projectId)).then(() => {
                        dispatch(fetchAhus(projectId))
                    })
                } else {
                    sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false })
                }
                $('#BatchImport').data("groupid", "")
                $('#BatchImport').modal('hide')
                callBack && callBack()
            },
            error: function () {
                sweetalert({ title: intl.get(UPLOAD_FAILED), type: 'error', timer: 1000, showConfirmButton: false })
                $('#BatchImport').data("groupid", "")
                $('#BatchImport').modal('hide')
                callBack && callBack()
                
            }
        })
    }
}

export function saveBatchExport(projectId, fileName, groupId) {
    return dispatch => {
        $('#BatchExport').data("groupid", "")
        $('#BatchExport').modal('hide')
        window.location = `${SERVICE_URL}` + 'export/group/' + projectId + '/' + groupId + '?filename=' + fileName
    }
}

export function fetchGroupSection(projectId, groupId, ahuId) {
    return dispatch => $http.get('ahu/metadata/material/' + ahuId).then(data => {
        if (data.code == "0") {
            let arr = [];
            let obj = { ...data.data }
            obj.sectionKey = "ahu.ahu"
            delete obj.parts
            arr.push(obj)
            data.data.parts.forEach((element) => {
                let has = false
                arr.forEach((ele) => {
                    if (ele.sectionKey == element.sectionKey) {
                        has = true
                    }
                })
                if (!has) {
                    arr.push(element)
                }
            })
            dispatch(receiveAhuHasSec(arr))
            dispatch(fetchGroupSecList(projectId, groupId))
        }
    })
}

function receiveAhuHasSec(ahuHasSecList) {
    return {
        type: RECEIVE_AHU_HAS_SEC,
        ahuHasSecList
    }
}

export function fetchGroupSecList(projectId, groupId) {
    // return dispatch => $http.get('mc/getgc/' + projectId + '/' + groupId).then(data => {
    return dispatch => $http.get('mc/getgc/' + projectId + '/' + groupId).then(data => {
        if (data.code == "0") {
            dispatch(receiveGroupSecList(data.data.prokey))
        }
    })
}

function receiveGroupSecList(groupSecList) {
    //    console.log('fetchGroupSecList', groupSecList)
    return {
        type: RECEIVE_GROUP_SEC_LIST,
        groupSecList
    }
}
function receiveGroupCalcData(calcData) {
    return {
        type: RECEIVE_GROUP_CALC_DATA,
        calcData
    }
}

export function saveGroupOptionForm(projectId, groupId, groupOptionForm) {
    let tempData = {}
    for (var key in groupOptionForm) {
        tempData[key.replace(/_/gi, '.')] = groupOptionForm[key]
    }
    //    console.log("groupOptionForm>",groupOptionForm);
    return dispatch => $http.post('mc/savegc', {
        projid: projectId,
        groupid: groupId,
        prokey: JSON.stringify(tempData)
    }).then(data => {
        if (data.code == 0) {
            sweetalert({ title: intl.get(SAVE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false })
            dispatch(fetchGroups(projectId)).then(() => {
                dispatch(fetchAhus(projectId))
            })
        } else {
            sweetalert({ title: intl.get(SAVE_FAILED), type: 'error', timer: 1000, showConfirmButton: false })
        }
        $("#BatchSelectSet").modal('hide')
    })
}
export function saveQutSet(projectId, groupId, ahuId, selType, isButton, obj, onSuccess, onFailed) {
    let objc = JSON.stringify(obj)
    let ooo = {
        userConfig: obj == '' ? '' : objc
    }
    // console.log('ooo', obj, ooo)
    groupId = groupId == '' ? null : groupId
    return dispatch => {
        if (selType != undefined) {
            $http.post('mcset/' + projectId + '/' + groupId + '/' + selType + '/' + ahuId, ooo).then(
                onSuccess, onFailed)

            return;
            var text = intl.get(BATCH_SELECTION_IN_QUOTATION_PHASE_ONLY)
            $("#BatchSelectSet").modal('hide')
            sweetalert({
                title: intl.get(OK_SELECT_IT),
                text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    $("#isCalcIng").modal('show')
                    // setTimeout(()=>{
                    $http.post('mcset/' + projectId + '/' + groupId + '/' + selType).then(data => {
                        /*var successList = intl.get(DRAWING_NO_IS)
                        var FailureList = intl.get(DRAWING_NO_IS)
                        var returnType = "error"
                        if (data.data.Success.length == 0) {
                          successList = ""
                        } else {
                          for (var i = 0; i < data.data.Success.length; i++) {
                            successList += data.data.Success[i] + ","
                          }
                          successList = successList.substring(0, successList.length - 1)
                          successList += intl.get(UNIT_CONFIGURATION_SUCCESS)
                          returnType = 'success'
                        }
                        if (data.data.Failure.length == 0) {
                          FailureList = ""
                        } else {
                          for (var i = 0; i < data.data.Failure.length; i++) {
                            FailureList += data.data.Failure[i] + ","
                          }
                          FailureList = FailureList.substring(0, FailureList.length - 1)
                          FailureList += intl.get(UNIT_CONFIGURATION_FAILED)
                        }
                        setTimeout(() => {
                          $("#isCalcIng").modal('hide')
                          if (data.code == 1) {
                            sweetalert({
                              title: intl.get(CONFIGURATION_FAILED),
                              text: '',
                              type: 'error',
                             ,timer: 1000, showConfirmButton: false
                            })
                          } else {
                            dispatch(fetchGroups(projectId)).then(() => {
                              dispatch(fetchAhus(projectId))
                            })
                            setTimeout(function () {
                              sweetalert({
                                title: successList + '\n' + FailureList,
                                type: returnType,
                               ,timer: 1000, showConfirmButton: false
                              })
                            }, 1000)
                          }
                        }, 1000)*/
                    }, () => {
                        $("#isCalcIng").modal('hide')
                        sweetalert({
                            title: intl.get(CONFIGURATION_FAILED),
                            text: '',
                            type: 'error'
                        })

                    })
                    // },1000)

                }
            })
        }
    }
}

export function updateProject(projectId) {
    return dispatch => dispatch(fetchGroups(projectId)).then(() => {
        dispatch(fetchAhus(projectId))
    })
}
export function updateProjectCalcData(projectId) {
    return dispatch => $http.get('ws/getalljson/' + projectId).then(data => {
        if (data.code == "0") {
            dispatch(receiveGroupCalcData(data.data))
        }
    })
}


export function saveCreateReport(param, startGeneratingReport) {

    $("#generatingReport").modal('show')
    return dispatch => $http.post('report', {
        "reports": JSON.stringify(param)
    }).then(data => {
        if (data.code == 0) {
            startGeneratingReport();
        } else {
            sweetalert({ title: intl.get(GENERATE_FAILED), type: 'error', timer: 1000, showConfirmButton: false })
        }
    })
}

export function receiveReport(reports) {
    return {
        type: GROUPS_REPORTS,
        reports
    }
}

export function clearReports() {
    return {
        type: CLEAR_REPORTS
    }
}

export function checkDrawingNo(ahuId, drawingNo, msgEl, addBox) {
    return dispatch => $http.get('ahu/resetno/' + ahuId + '/' + drawingNo).then(data => {
        if (data.data.type == 1) {
            msgEl.innerText = '* ' + data.data.errMsg
            msgEl.style.color = '#fd4c4a'
        } else {
            msgEl.innerText = '* ' + intl.get(MODIFY_SUCCESS)
            msgEl.style.color = 'green'
            // $(addBox).text(data.data.unitNo)
        }
        msgEl.style.display = 'block'

        // msgEl.style.display = 'inline'
    })
}

export function updateGroupPre(groupId) {
    return dispatch => $http.get('group/' + groupId).then(data => {
        dispatch(receiveUpdateGroup(data.data))
        // $('#newGroup').collapse('show')
    })
}

function receiveUpdateGroup(updateGroup) {
    return {
        type: RECEIVE_UPDATE_GROUP,
        updateGroup
    }
}

export function groupPageFilterUpdate(filterStr) {
    return {
        type: RECEIVE_PAGE_GROUP_FILTERUPDATE,
        filterStr: filterStr
    }
}


export function receiveAhuListForReport (data) {
    return {
        type: RECEIVE_AHULIST_FORREPORT,
        data
    }
}
export function clickReport(projectId, callBack) {
    let obj ={
        all:true,
        projectId
    }
    return dispatch => $http.get('ahu/list/', obj).then(data => {
        dispatch(receiveAhuListForReport(data.data))
        callBack()
    })
}


export function updateAhuPre(ahuId) {
    // $('#newAhu').data("update", true)
    return dispatch => $http.get('ahu/' + ahuId).then(data => {
        dispatch(receiveUpdateAhu(data.data))
        // $('#newAhu').collapse('show')
    })
}

function receiveUpdateAhu(updateAhu) {
    return {
        type: RECEIVE_UPDATE_AHU,
        updateAhu
    }
}

export function syncBatchValues(name, value) {
    return {
        type: SYNC_BATCH_VALUES,
        name,
        value
    }
}

export function solveLinkage(name, value, currentValue) {
    if (name == 'meta_section_mix_returnTop' ||
        name == 'meta_section_mix_returnBack' ||
        name == 'meta_section_mix_returnLeft' ||
        name == 'meta_section_mix_returnRight' ||
        name == 'meta_section_mix_returnButtom') {
        let value = mixType(name, currentValue)
        return value
        // let copy = {
        //     0:value
        // }
        // return copy
    }

    return currentValue
}

function mixType(name, currentValue) {
    const mixValues = currentValue
    const props = [
        'meta_section_mix_returnTop',
        'meta_section_mix_returnBack',
        'meta_section_mix_returnLeft',
        'meta_section_mix_returnRight',
        'meta_section_mix_returnButtom',
    ]
    let counter = 0
    props.forEach(prop => {
        if (mixValues[prop] == 'true' || mixValues[prop] == true) {
            counter++
        }
    })
    if (props.find(prop => prop === name) && counter > 2) {
        let mark = true
        props.forEach(prop => {
            if (mark && prop !== name && currentValue[prop]) {
                currentValue[prop] = false
                mark = false
            }
        })
    }
    return mixValues
}


export function reset() {
    return {
        type: RESET,
    }
}

export function cleanGroupList() {
    return {
        type: CLEANGROUPLIST,
    }
}
