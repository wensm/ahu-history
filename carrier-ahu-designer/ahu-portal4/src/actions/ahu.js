import {
    SAVE_SUCCESS,
    SAVED_AHU_AND_SECTIONS_INFO,
    UPLOAD_SUCCESS,
    OK,
    SEND,
    UPLOAD_FAILED,
    UNIT,
    IMPERIAL,
    METRIC,
    CONSTRUCTION_DOOR_DIRECTION,
    LABEL_TOOLTIP,
    WHEELHEATRECYCLE_TOOLTIP,
    DIRECTEXPENSIONCOIL_TOOLTIP,
    REPORT_AHU_ERROR,
    SEND_ERROR_FILE_TO,
    CONFIRMCONFIGBOTTOM,
    CANCEL
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import NProgress from 'nprogress'
import sweetalert from 'sweetalert'
import $http from '../misc/$http'
import * as metadataAction from './metadata'
import { Modal } from 'antd'
const confirm = Modal.confirm;
import React from 'react'

import { validateSection } from '../pages/ahu/ChainCalculator'
import { GROUP_TYPE_RUNNER, GROUP_TYPE_PLATE, groupAHUFilter } from '../models/templates.js'

export const RECEIVE_LAYOUT = 'RECEIVE_LAYOUT'
export const RECEIVE_GROUP = 'RECEIVE_GROUP'
export const RECEIVE_AHU_INFO = 'RECEIVE_AHU_INFO'
export const RECEIVE_AHU_SECTIONS = 'RECEIVE_AHU_SECTIONS'
export const RECEIVE_AHU_TEMPLATES = 'RECEIVE_AHU_TEMPLATES'
export const RECEIVE_AHU_TEMPLATE = 'RECEIVE_AHU_TEMPLATE'
export const RECEIVE_COMPONENT_DEFAULT_VALUE = 'RECEIVE_COMPONENT_DEFAULT_VALUE'
export const UPDATE_COMPONENT_VALUE = 'UPDATE_COMPONENT_VALUE'
export const RECEIVE_AHU_BASE_SECTIONS = 'RECEIVE_AHU_BASE_SECTIONS'
export const RECEIVE_AHU_PRICE = 'RECEIVE_AHU_PRICE'
export const RECEIVE_AHU_THREE_VIEW = 'RECEIVE_AHU_THREE_VIEW'
export const RECEIVE_AHU_PSYCHOMETRIC = 'RECEIVE_AHU_PSYCHOMETRIC'
export const RECEIVE_AHU_SERIES = 'RECEIVE_AHU_SERIES'
export const RECEIVE_COOLING_COIL_CALCS = 'RECEIVE_COOLING_COIL_CALCS'
export const RECEIVE_COOLING_COIL_Dele = "RECEIVE_COOLING_COIL_Dele"
export const RECEIVE_FAN_CALCS = 'RECEIVE_FAN_CALCS'
export const RECEIVE_AHU_SECTIONS_VALIDATION = 'RECEIVE_AHU_SECTIONS_VALIDATION'
export const RECEIVE_AHU_SECTION_VALIDATION = 'RECEIVE_AHU_SECTION_VALIDATION'
export const CLICK_COMPONENT = 'CLICK_COMPONENT'
export const INCREASE_COMPONENT_ID = 'INCREASE_COMPONENT_ID'
export const ADD_AHU_COMPONENT = 'ADD_AHU_COMPONENT'
export const REMOVE_AHU_SECTION = 'REMOVE_AHU_SECTION'
export const SYNC_AHU_COMPONENT_VALUES = 'SYNC_AHU_COMPONENT_VALUES'
export const SELECT_AHU_SERIES = 'SELECT_AHU_SERIES'
export const UPDATE_COOLING_COIL_BY_INDEX = 'UPDATE_COOLING_COIL_BY_INDEX'
export const COMPLETE_SECTION = 'COMPLETE_SECTION'
export const RECEIVE_OUTLET_PARAMS = 'RECEIVE_OUTLET_PARAMS'
export const DROP_SECTION = 'DROP_SECTION'
export const UPDATE_FAN_VALUE = 'UPDATE_FAN_VALUE'
export const AHU_TOGGLE_STANDARD = 'AHU_TOGGLE_STANDARD'
export const RECEIVE_AHU_Partitions = 'RECEIVE_AHU_Partitions'
export const RECEIVE_PARTITION_META = 'RECEIVE_PARTITION_META'
export const UPDATE_COMPONENT_VALUES = 'UPDATE_COMPONENT_VALUES'
export const RECEIVE_CHOOSE_MODEL = 'RECEIVE_CHOOSE_MODEL'
export const RECEIVE_STEAMHUMDIDFIER_CHOOSE_MODEL = 'RECEIVE_STEAMHUMDIDFIER_CHOOSE_MODEL'
export const THREE_VIEW_MODAL_CLOSED = 'THREE_VIEW_MODAL_CLOSED'
export const UPDATE_RAVOLUME_VALUE = 'UPDATE_RAVOLUME_VALUE'
export const UPDATE_NAVOLUME_VALUE = 'UPDATE_NAVOLUME_VALUE'
export const UPDATE_STEAM_HUMIDIFIER_BY_INDEX = 'UPDATE_STEAM_HUMIDIFIER_BY_INDEX'
export const AHU_SET_STANDARD = 'AHU_SET_STANDARD'
export const SYNC_OTHER_PARAM = 'SYNC_OTHER_PARAM'
export const SYNC_UVMODEL = 'SYNC_UVMODEL'
export const CLEAN_Partitions = 'CLEAN_Partitions'
export const UPDATE_SELECTED = 'UPDATE_SELECTED'
export const DISMISS = 'DISMISS'
export const PMSTATE = 'PMSTATE'
export const RESETPERFORMANCEJ = 'RESETPERFORMANCEJ'
export const RECEIVE_AHU_SECTION_VALIDATIONP = 'RECEIVE_AHU_SECTION_VALIDATIONP'
export const RECEIVE_EHC = 'RECEIVE_EHC'
export const HANDLEOK = 'HANDLEOK'
export const CLEAN_AHU_INFO = 'CLEAN_AHU_INFO'
export const CLEAN_PRICE = 'CLEAN_PRICE'
export const SAVE_AHU_PRICE = 'SAVE_AHU_PRICE'

export const UNIT_METRIX = "M"
export const UNIT_BRITISH = "B"
export const UNIT_ITEM_TEMPRETURE = "Tempreture"

export const STYLE_COMMON = 10
export const STYLE_CHAMBER = 11
export const STYLE_WHELE = 21
export const STYLE_PLATE = 22
export const STYLE_VERTICAL_1 = 31
export const STYLE_VERTICAL_2 = 32
export const STYLE_DOUBLE_RETURN_1 = 41
export const STYLE_DOUBLE_RETURN_2 = 42
export const STYLE_SIDE_BY_SIDE_RETURN = 43

export const AHU_FLOW_ATTR_ARR = [
    ['WOutDryBulbT', 'WInDryBulbT'],
    ['WOutWetBulbT', 'WInWetBulbT'],
    ['SOutDryBulbT', 'SInDryBulbT'],
    ['SOutWetBulbT', 'SInWetBulbT'],

    ['WOutRelativeT', 'WInRelativeT'],
    ['SOutRelativeT', 'SInRelativeT'],
]

export const RESISTANCE_ATTR_ARR = ['Resistance',
    'meta_section_fan_sysStatic',
    'meta_section_fan_extraStatic',
    'meta_section_fan_externalStatic',
    'meta_section_fan_totalStatic',
    'meta_section_fan_Resistance',
    'IResistance',
    'RResistance'

]
export const TOOLTIP_INPUT = [
    'SInWetBulbT',
    'SInRelativeT',
    'SNewWetBulbT',
    'SNewRelativeT',
    'WInWetBulbT',
    'WInRelativeT',
    'WNewWetBulbT',
    'WNewRelativeT',
]

export function onToggleStandard() {
    return {
        type: AHU_TOGGLE_STANDARD,
    }
}
export function onSetStandard(bool) {
    return {
        type: AHU_SET_STANDARD,
        standard: bool,
    }
}

export function updateFanValue(index) {
    return {
        type: UPDATE_FAN_VALUE,
        index
    }
}

export function dropSection(direction, propUnit) {
    return {
        type: DROP_SECTION,
        direction,
        propUnit
    }
}

function receiveOutletParams(season, outletParams) {
    return {
        type: RECEIVE_OUTLET_PARAMS,
        season,
        outletParams,
    }
}

export function fetchThreeViewDwg(ahuId) {
    window.location = `${SERVICE_URL}` + 'ahu/cad/threeview/export/' + ahuId + '/dwg';
}

export function fetchOutletParams(airFlow, freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN, names, resultKeys) {
    return dispatch => $http.get(`calc/mixAir/${airFlow}/${freshAirRate}/${freshAirTIN}/${freshAirTbIN}/${returnAirTIN}/${returnTbIN}`).then(data => {
        dispatch(receiveOutletParams(season, data.data))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchRelativeHumidity(dryBulbTemperature, wetBulbTemperature, name, self, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/FAirParm1/${dryBulbTemperature}/${wetBulbTemperature}`).then(data => {
        dispatch(updateComponentValue(name, data.data.parmF, self, wetBulbTemperature, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchRAVolume(aVolume, naVolume, name, self) {
    return dispatch => $http.get(`calc/NAVolume/${aVolume}/${naVolume}`).then(data => {
        dispatch(updateRAVolumeValue(name, data.data, self, naVolume))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchNAVolume(aVolume, raVolume, name, self) {
    return dispatch => $http.get(`calc/RAVolume/${aVolume}/${raVolume}`).then(data => {
        dispatch(updateNAVolumeValue(name, data.data, self, raVolume))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchWetBulbTemperature(dryBulbTemperature, relativeHumidity, name, self, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/FAirParm3/${dryBulbTemperature}/${relativeHumidity}`).then(data => {
        dispatch(updateComponentValue(name, data.data.parmTb, self, relativeHumidity, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

//求等焓加濕量
export function fetchIsoenthalpyJSBHumidificationVolume(DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/IsoenthalpyHumidificationRH/${DryBulbTemperature}/${WetBulbTemperature}/${RelativeHumidity}/${eairvolume}`).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}
//求等温加濕量
export function fetchIsothermalJSBHumidificationVolume(DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/IsothermalHumidificationRH/${DryBulbTemperature}/${WetBulbTemperature}/${RelativeHumidity}/${eairvolume}`).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}
//Mix1
export function fetchCalcMixAir1(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {

    return dispatch => $http.post(`calc/mixAir1`, obj, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit));
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

//Mix2
export function fetchCalcMixAir2(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {

    return dispatch => $http.post(`calc/mixAir2`, obj, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit));
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

//mix4
export function fetchCalcMixAir4(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {

    return dispatch => $http.post(`calc/mixAir4`, obj, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit));
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchCalcHeatingQ(sectionKey, season, airVolume, InDryBulbTemperature, InWetBulbTemperature, InRelativeHumidity, HumDryBulbTemperature, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.post(`calc/heatCoilT`, {
        season: season,
        InDryBulbT: InDryBulbTemperature,
        InWetBulbT: InWetBulbTemperature,
        InRelativeT: InRelativeHumidity,
        airVolume: airVolume,
        sectionKey: sectionKey,
        HumDryBulbT: HumDryBulbTemperature
    }, true
    ).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit));
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchCalcHumDryBulbT(sectionKey, season, airVolume, InDryBulbTemperature, InWetBulbTemperature, InRelativeHumidity, HeatQ, names, resultKeys, serial, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.post(`calc/heatCoilQ`, {
        season: season,
        InDryBulbT: InDryBulbTemperature,
        InWetBulbT: InWetBulbTemperature,
        InRelativeT: InRelativeHumidity,
        airVolume: airVolume,
        sectionKey: sectionKey,
        HeatQ: HeatQ,
        serial: serial
    }, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate())
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

function setAhuSectionValidateError(msg) {
    return (receiveAhuSectionValidation({
        sectionPass: false,
        sectionMsg: msg
    }))
}
function setAhuSectionValidateErrorp(msg) {
    return (receiveAhuSectionValidationp({
        sectionPass: false,
        sectionMsg: msg
    }))
}

function setAhuSectionValidateWarn(msg) {
    return (receiveAhuSectionValidation({
        sectionWarn: true,
        sectionMsg: msg
    }))
}

function resetAhuSectionValidate() {
    return (receiveAhuSectionValidation({
        sectionPass: true,
        sectionWarn: false,
        sectionMsg: ""
    }))
}


//求等焓相對濕度
export function fetchIsoenthalpyJSBRelativeHumidity(DryBulbTemperature, WetBulbTemperature, humidification, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/IsoenthalpyHumidification/${DryBulbTemperature}/${WetBulbTemperature}/${humidification}/${eairvolume}`, undefined, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}
//求等温相對濕度
export function fetchIsothermalJSBRelativeHumidity(DryBulbTemperature, WetBulbTemperature, humidification, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {
    return dispatch => $http.get(`calc/IsothermalHumidification/${DryBulbTemperature}/${WetBulbTemperature}/${humidification}/${eairvolume}`, undefined, true).then(data => {
        dispatch(updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

export function fetchChooseModel(season, fEfficiencyS, fEfficiencyW, supplier) {
    return dispatch => {
        dispatch(resetperformanceJ())
        $http.post(`calc/performanceJ`, {
            supplier: supplier,
            fEfficiencyW: fEfficiencyW,
            fEfficiencyS: fEfficiencyS,
            season: season,
        }, true).then(data => {
            dispatch(receiveChooseModel(data))
            //   dispatch(resetAhuSectionValidate());
        }, (err) => {
            if (err && err.code === -99) {
                dispatch(setAhuSectionValidateErrorp(err.msg))
            }
        })
    }
}
function resetperformanceJ() {
    return {
        type: RESETPERFORMANCEJ,
        date: ''
    }
}

export function fetchSteamHumidifierChooseModel(season, inputVaporPressure, supplier) {//干蒸加湿段
    return dispatch => $http.post(`calc/performanceH`, {
        supplier: supplier,
        inputVaporPressure,
        season: season,
    }, true).then(data => {
        dispatch(receiveSteamHumidifierChooseModel(data))
        dispatch(resetAhuSectionValidate());
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateError(err.msg))
        }
    })
}

function updateRAVolumeValue(name, value, self, selfValue) {
    return {
        type: UPDATE_RAVOLUME_VALUE,
        name,
        value,
        self,
        selfValue
    }
}

function updateNAVolumeValue(name, value, self, selfValue) {
    return {
        type: UPDATE_NAVOLUME_VALUE,
        name,
        value,
        self,
        selfValue
    }
}

function updateComponentValue(name, value, self, selfValue, unitSetting, metaUnit, metaLayout, propUnit) {
    return {
        type: UPDATE_COMPONENT_VALUE,
        name,
        value,
        self,
        selfValue, unitSetting, metaUnit, metaLayout, propUnit
    }
}

function updateComponentValueHV(data, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) {


    return {
        type: UPDATE_COMPONENT_VALUES,
        names,
        resultKeys,
        data, unitSetting, metaUnit, metaLayout, propUnit
    }
}


export function beforeConfirmSection(projectId, ahuId, ahuStr, sectionStr, position, direction, allSectionsStr, validateSections, selectedName, unitPreferCode, unitSetting, metaUnit, metaLayout, isNS, preferredLocale) {
    let content = []
    // let differentName = (<div key={key} style={{ marginBottom: '4px' }}><span><span style={{fontWeight:'900'}}>{keyName}({sectionName}): &nbsp;&nbsp;</span>  From<span style={{ padding: '0 5px' }}>{`${oldValue}`}</span>To<span style={{ paddingLeft: '5px', color: 'red' }}>{`${newValue}`}</span></span></div>)
    //         content.push(differentName)
    let sectionStrCopy = JSON.parse(sectionStr)
    let allSectionValue = JSON.parse(sectionStrCopy[0]['metaJson'])
    let currentSelected = sectionStrCopy[0].key.split('.')[1]
    let language = preferredLocale == "zh-CN" ? 'memo' : 'name'

    for (let key in allSectionValue) {
        if (metaLayout[key] && key.split('.')[2] == currentSelected && key.split('.')[0] != 'ns' && key.split('.')[3] != 'EnableWinter' && key.split('.')[3] != 'EnableSummer' && key.split('.')[3] != 'price' && key.split('.')[3] != 'k') {
            let type = metaLayout[key]['valueType'] == "BooleanV" ? 'checkbox' : (metaLayout[key]['option'] == null || metaLayout[key]['option']['options'].length == 0) ? 'input' : 'selected'
            let value = ''
            if (type == 'checkbox' || type == 'input') {
                value = allSectionValue[key]
            } else {
                let indexold = ''
                metaLayout[key].option.optionPairs.forEach((ele, index) => {
                    if (ele.option == allSectionValue[key]) {
                        indexold = index
                    }

                })
                let opairs = metaLayout[key].option.optionPairs
                value = preferredLocale == "zh-CN" ? (opairs[indexold] && opairs[indexold]['clabel']) : (opairs[indexold] && opairs[indexold]['label'])
            }
            if (allSectionValue[key] != '' && allSectionValue[key] != undefined) {
                //    console.log('sectionStr', metaLayout, allSectionValue, metaLayout[key][language])
                content.push(<div className="col-lg-3" key={key} style={{ marginBottom: '4px' }}><span><span style={{ fontWeight: '900' }}>{metaLayout[key][language]}&nbsp;: &nbsp;&nbsp;</span> <span style={{ padding: '0 5px' }}>{`${value}`}</span></span></div>)
            }
        }
    }
    return dispatch => {
        //modal
        confirm({
            title: intl.get(CONFIRMCONFIGBOTTOM),
            content: content,
            okText: intl.get(OK),
            okType: 'primary',
            cancelText: intl.get(CANCEL),
            zIndex: 2000,
            width: 900,
            onOk() {
                dispatch(confirmSection(projectId, ahuId, ahuStr, sectionStr, position, direction, allSectionsStr, validateSections, selectedName, unitPreferCode, unitSetting, metaUnit, metaLayout, isNS))

            },
            onCancel() {
            },
        });
    }
}
export function confirmSection(projectId, ahuId, ahuStr, sectionStr, position, direction, allSectionsStr, validateSections, selectedName, unitPreferCode, unitSetting, metaUnit, metaLayout, isNS, layoutStr) {

    return dispatch => $http.post('sec/confirm', {
        pid: projectId,
        unitid: ahuId,
        ahuStr,
        sectionStr,
        position,
        allSectionsStr,
        validateSections,
        isNS,
        layoutStr
    }, true).then(data => {
        dispatch(completeSection(data.data, direction, unitPreferCode, unitSetting, metaUnit, metaLayout, isNS))
        dispatch(resetAhuSectionValidate());
        if (selectedName.split('.')[1] == 'wheelHeatRecycle') {
            sweetalert({
                title: intl.get(LABEL_TOOLTIP),
                text: intl.get(WHEELHEATRECYCLE_TOOLTIP),
                // timer: 2000,
                type: 'warning',
                showConfirmButton: true
            })
        }
        if (selectedName.split('.')[1] == 'directExpensionCoil') {
            sweetalert({
                title: intl.get(LABEL_TOOLTIP),
                text: intl.get(DIRECTEXPENSIONCOIL_TOOLTIP),
                // timer: 2000,
                type: 'warning',
                showConfirmButton: true
            })
        }
    }, (err) => {
        if (err && err.code === -99) {
            dispatch(setAhuSectionValidateWarn(err.msg))
        }
    })
}


export function calculateNsPrice(param, ahuId) {
    return dispatch => $http.post(`ahu/nsprice`, {
        ahuStr: param,
        unitid: ahuId,
        pid:'',
        sectionStr:'[]',
        position:'',
        allSectionsStr:'[]',
        isNS:true,
        layoutStr:''
    }, true).then(data => {
        dispatch(saveAhuPrice(data.data))


    })
}

export function saveAhuPrice(data) {
    return {
        type: SAVE_AHU_PRICE,
        data,
    }
}

export function completeSection(data, direction, unitPreferCode, unitSetting, metaUnit, metaLayout, isNS) {
    return {
        type: COMPLETE_SECTION,
        data,
        direction,
        unitPreferCode,
        unitSetting,
        metaUnit,
        metaLayout,
        isNS
    }
}

export function updateCoolingCoilByIndex(index, unitPreferCode, unitSetting, metaUnit, metaLayout, name = '') {
    return {
        type: UPDATE_COOLING_COIL_BY_INDEX,
        index,
        unitPreferCode,
        unitSetting,
        metaUnit,
        metaLayout,
        name
    }
}
export function updateSteamHumidifierByIndex(resultData) {
    return {
        type: UPDATE_STEAM_HUMIDIFIER_BY_INDEX,
        resultData
    }
}

export function calcComponentValue(projectId, ahuId, ahuStr, sectionStr, position, sectionName, season, propUnit) {

    return dispatch => {
        switch (sectionName) {
            case 'ahu.coolingCoil':
                dispatch(receiveCoolingCoilCalc('clean'))
                break
            case 'ahu.fan':
                dispatch(receiveFanCalc('clean'))
                break
            default:

        }
        $http.post('cal' + (season ? "?season=" + season : ""), {
            pid: projectId,
            unitid: ahuId,
            ahuStr, sectionStr, position
        }, true).then(data => {
            // $('#coolingCoilModal').modal('show')
            if (data.code === '0' && data.data !== null) {
                dispatch(resetAhuSectionValidate());
                switch (sectionName) {
                    case 'ahu.coolingCoil':
                        dispatch(receiveCoolingCoilCalc(data.data))
                        break
                    case 'ahu.fan':
                        dispatch(receiveFanCalc(data.data))
                        break
                    default:
                }
            } else {
                dispatch(resetAhuSectionValidate());
                switch (sectionName) {
                    case 'ahu.coolingCoil':
                        dispatch(receiveCoolingCoilCalc(data.msg))
                        break
                    case 'ahu.fan':
                        dispatch(receiveFanCalc(data.msg))
                        break
                    default:
                }
            }
        }, (err) => {
            // dispatch(receiveAhuSectionValidation({
            //   pass:false,
            //   msg:err.msg
            // }))
            switch (sectionName) {
                case 'ahu.coolingCoil':
                    dispatch(receiveCoolingCoilCalc(err.msg))
                    break
                case 'ahu.fan':
                    dispatch(receiveFanCalc(err.msg))
                    break
                default:

            }
        })
    }
}
export function calculateEHC(projectId, ahuId, ahuStr, sectionStr, position, sectionName, season, propUnit, callback) {

    return dispatch => {

        $http.post('cal', {
            pid: projectId,
            unitid: ahuId,
            ahuStr, sectionStr, position
        }, true).then(data => {
            if (data.code === '0' && data.data !== null) {
                dispatch(receiveEHC(data.data))//接口处理
                callback()
            }
        }, (err) => {
            dispatch(receiveEHC(err.msg))
            callback()

            // switch (sectionName) {
            //     case 'ahu.coolingCoil':
            //         break
            //     case 'ahu.fan':
            //         dispatch(receiveFanCalc(err.msg))
            //         break
            //     default:

            // }
        })
    }
}

function receiveEHC(data) {
    return {
        type: RECEIVE_EHC,
        data
    }
}
export function deleComponentValue() {
    return {
        type: RECEIVE_COOLING_COIL_Dele
    }
}

function receiveSteamHumidifierChooseModel(data) {
    return {
        type: RECEIVE_STEAMHUMDIDFIER_CHOOSE_MODEL,
        data
    }
}

function receiveChooseModel(data) {
    return {
        type: RECEIVE_CHOOSE_MODEL,
        data
    }
}

function receiveFanCalc(data) {
    return {
        type: RECEIVE_FAN_CALCS,
        data
    }
}

function receiveCoolingCoilCalc(data) {
    return {
        type: RECEIVE_COOLING_COIL_CALCS,
        data
    }
}

export function fetchAhuSectionsValidation(sections) {
    return (dispatch, getState) => {
        $http.get('sectionrelation', { sections: JSON.stringify(sections) }).then(data => {
            dispatch(receiveAhuSectionsValidation(data))
            dispatch(resetAhuSectionValidate());
        })
    }

}

function receiveAhuSectionsValidation(data) {
    return {
        type: RECEIVE_AHU_SECTIONS_VALIDATION,
        data
    }
}
function receiveAhuSectionValidation(data) {
    return {
        type: RECEIVE_AHU_SECTION_VALIDATION,
        data
    }
}
function receiveAhuSectionValidationp(data) {
    return {
        type: RECEIVE_AHU_SECTION_VALIDATIONP,
        data
    }
}

export function selectAhuSeries(series) {
    return dispatch => {
        dispatch(selectAhuSeries2(series));
        dispatch(metadataAction.changeSeriesCanDropList(series));

    }

}
function selectAhuSeries2(series) {
    return {
        type: SELECT_AHU_SERIES,
        series
    }
}

export function fetchAhuSeries(templateId, unitSeries, airVolume) {
    return dispatch => $http.get(`ahuseries?unitSeries=${unitSeries}&airVolume=${airVolume}`).then(data => {
        let dataFilter = [...data]
        dataFilter = groupAHUFilter(templateId, dataFilter);
        dispatch(receiveAhuSeries(dataFilter));
    })
}

function receiveAhuSeries(series) {
    return {
        type: RECEIVE_AHU_SERIES,
        series
    }
}

export function fetchAhuTemplate() {
    return dispatch => $http.get('ahutemplate/list').then(data => dispatch(receiveAhuTemplate(data.data)))
}

function receiveAhuTemplate(data) {
    return {
        type: RECEIVE_AHU_TEMPLATE,
        data
    }
}

export function syncAhuComponentValues(name, newValue, previousValue, propUnit = '', metaUnit = '', metaLayout = '') {
    return (dispatch, getState) => {
        // let ahuValues = getState().ahu.componentValue
        // let selectedComponent = getState().ahu.selectedComponent
        // dispatch(metadataAction.fetchMetaVariableValidation(ahuValues, selectedComponent, name))//zzf2
        dispatch(syncAhuComponentValues2(newValue, previousValue, name, metaLayout))
    }
}
function syncAhuComponentValues2(newValue, previousValue, name, metaLayout) {
    return {
        type: SYNC_AHU_COMPONENT_VALUES,
        newValue: newValue,
        previousValue: previousValue,
        name: name,
        metaLayout
    }
}
export function syncAhuComponentValidate(name, newValue, previousValue) {
    return (dispatch, getState) => {
        let ahuValues = getState().ahu.componentValue
        // debugger
        // ahuValues[3]['meta_section_coolingCoil_SInDryBulbT'] = newValue
        // console.log('zzfnew', name, newValue, ahuValues[3]['meta_section_coolingCoil_SInDryBulbT'])
        let selectedComponent = getState().ahu.selectedComponent
        let obj = {};
        getState().ahu.baseSections.forEach((record, index) => {
            obj = { ...obj, ...record.parameters }
        })
        let locale = getState().general.user.preferredLocale
        dispatch(metadataAction.fetchMetaVariableValidation(ahuValues, selectedComponent, name, obj, locale))//zzf2

    }
}


export function normalizeValueForUnit(name, newValue, propUnit, metaUnit, metaLayout) {
    let newValue1 = calculateUnitValueToMetric(name, newValue, propUnit, metaUnit, metaLayout)
    // console.log('newValue', newValue, newValue1)
    return newValue1
}

export function formatValueForUnit(name, newValue, propUnit, metaUnit, metaLayout) {
    let newValue1 = calculateUnitValueToBritish(name, newValue, propUnit, metaUnit, metaLayout)
    return newValue1
}

export function saveAhu(pid, unitid, ahuStr, sectionStr, layoutStr, bool, unitSetting, metaUnit, metaLayout, propUnit, callBack, standard) {
    NProgress.start()
    return dispatch => $http.post('section/bacthsave', {
        pid, unitid, ahuStr, sectionStr, layoutStr, isNS:!standard
    }).then(data => {
        if (data.code === '0' && data.msg === 'Success' && bool && !callBack) {
            sweetalert({
                title: intl.get(SAVE_SUCCESS),
                text: intl.get(SAVED_AHU_AND_SECTIONS_INFO),
                timer: 1000,
                type: 'success',
                showConfirmButton: false
            })
            dispatch(clickComponent(0, 'ahu.ahu'))
            dispatch(fetchAhuInfo(unitid, unitSetting, metaUnit, metaLayout, propUnit))
        } else {
            callBack && callBack()
            dispatch(clickComponent(0, 'ahu.ahu'))
            dispatch(fetchAhuInfo(unitid, unitSetting, metaUnit, metaLayout, propUnit))
        }
    })
}

export function fetchThreeViews(ahuId, fromWhere = '') {
    let params = {
        fromPanel: fromWhere == 'panel' ? false : false
    }
    return dispatch => $http.get(`ahu/cad/threeview/${ahuId}`, params).then(data => dispatch(receiveThreeViews(data.data)))
}

export function fetchPsychometric(ahuId) {
    return dispatch => {
        dispatch(receivePsychometric(''))
        $http.get(`ahu/psychometric/${ahuId}`).then(data => dispatch(receivePsychometric(data.data)))
    }
}
function psychometricModalState(bool) {
    return {
        type: PMSTATE,
        bool
    }
}
export function saveImportAhu(formData, projectId, ahuId) {
    return dispatch => $.ajax({
        url: `${SERVICE_URL}import/unit` + (ahuId ? (`?ahuId=` + ahuId) : ``),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.code == "0") {
                sweetalert({
                    title: intl.get(UPLOAD_SUCCESS), type: 'success',
                    showCancelButton: false,
                    confirmButtonText: intl.get(OK),
                    closeOnConfirm: true
                }, isConfirm => {
                    window.location.reload();
                })
            } else {
                sweetalert({ title: data.msg, type: 'error', timer: 1000, showConfirmButton: false });
            }
            $('#ImportAhu').modal('hide')
        },
        error: function () {
            sweetalert({ title: intl.get(UPLOAD_FAILED), type: 'error', timer: 1000, showConfirmButton: false });
            $('#ImportAhu').modal('hide')
        }
    });
}

export function reportError(ahuId) {
    return dispatch => {
        return $http.get(`log/error?ahuId=${ahuId}`).then(data => {
            window.open(`${SERVICE_URL}` + data.data.errorfile);
            sweetalert({
                title: intl.get(SEND_ERROR_FILE_TO),
                type: 'success',
                text: data.data.email,
                showCancelButton: true,
                confirmButtonText: intl.get(SEND),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    let mailTo = "mailto:" + data.data.email;
                    mailTo += "?subject=" + intl.get(REPORT_AHU_ERROR);
                    window.location.href = mailTo;
                }
            })
        })
    }
}

export function panelInit(ahuId, callback, transformer='') {
    let param = {
        panelSeries:transformer
    }
    return dispatch => {
        return $http.get(`panel/init/${ahuId}`, transformer ? param : '').then(data => {
            if (data && data.msg == "Success") {//接口返回后回调查询接口
                callback()
            }
        })
    }
}
function receiveThreeViews(data) {
    return {
        type: RECEIVE_AHU_THREE_VIEW,
        data
    }
}

function receivePsychometric(data) {
    return {
        type: RECEIVE_AHU_PSYCHOMETRIC,
        data
    }
}

export function fetchPrice(ahuId) {
    return (dispatch) => {
        dispatch(cleanPrice());
        return $http.get(`price/${ahuId}`).then(data => {
            if (data.code === '0') {
                dispatch(receivePrice(data.data));
                sweetalert({
                    title: data.msg,
                    timer: 1000,
                    type: 'success',
                    showConfirmButton: false
                });
            }
        })
    }
}

function cleanPrice() {
    return {
        type: CLEAN_PRICE,
    }
}

function receivePrice(data) {
    return {
        type: RECEIVE_AHU_PRICE,
        data
    }
}

export function fetchComponentDefaultValue() {
    return dispatch => $http.get('template').then(data => dispatch(receiveComponentDefaultValue(data)))
}

function receiveComponentDefaultValue(data) {
    return {
        type: RECEIVE_COMPONENT_DEFAULT_VALUE,
        data
    }
}

export function fetchAhuBaseSections() {
    return dispatch => $http.get('meta/sections').then(data => dispatch(receiveAhuBaseSections(data)))
}

function receiveAhuBaseSections(data) {
    return {
        type: RECEIVE_AHU_BASE_SECTIONS,
        data
    }
}

export function removeAhuSection(sectionId, a, b, direction, propUnit) {
    return {
        type: REMOVE_AHU_SECTION,
        sectionId,
        direction,
        propUnit
    }
}

export function addAhuComponent(sectionName, projectId, ahuId, propUnit) {
    return {
        type: ADD_AHU_COMPONENT,
        sectionName,
        projectId,
        ahuId,
        propUnit
    }
}

export function clickComponent(id, name, displayName) {
    return {
        type: CLICK_COMPONENT,
        id,
        name,
        displayName,
    }
}

export function fetchAhuTemplates() {
    return dispatch => {
        return $http.get('templates').then(data => dispatch(receiveAhuTemplates(data)))
    }
}

function receiveAhuTemplates(templates) {
    return {
        type: RECEIVE_AHU_TEMPLATES,
        templates
    }
}

export function fetchAhuInfo(ahuId, unitSetting, metaUnit, metaLayout, propUnit, callBack) {
    return (dispatch, getState) => {
        return $http.get(`ahu/${ahuId}`).then(data => {
            dispatch(receiveAhuInfo(data.data, unitSetting, metaUnit, getState().metadata.layout, propUnit))
            setTimeout(() => {
                dispatch(metadataAction.onSetProduct(data.data.unit.product))
            }, 1000);
            callBack && callBack()
        })
    }
}
export function cleanAhuInfo() {
    return {
        type: CLEAN_AHU_INFO
    }
}

export function receiveAhuInfo(ahu, unitSetting, metaUnit, metaLayout, propUnit) {//action.ahu.unit.metaJson 机组的初始化值
    return {
        type: RECEIVE_AHU_INFO,
        ahu,
        unitSetting,
        metaUnit,
        metaLayout,
        propUnit
    }
}

export function fetchAhuSections(ahuId) {
    return dispatch => {
        return $http.get(`section/list?ahuId=${ahuId}`).then(data => {
            dispatch(receiveAhuSections(data.data))
        })
    }
}

export function receiveAhuSections(ahuSections) {
    return {
        type: RECEIVE_AHU_SECTIONS,
        ahuSections
    }
}

export function fetchAhuPartitions(ahuId) {
    return dispatch => {
        dispatch(cleanPartitions())
        return $http.get(`ahu/partition/${ahuId}`).then(data => {
            dispatch(receiveAhuPartitions(data.data))
        })
    }
}
function cleanPartitions() {
    return {
        type: CLEAN_Partitions,
    }
}
function receiveAhuPartitions(ahuPartitions) {
    return {
        type: RECEIVE_AHU_Partitions,
        ahuPartitions
    }
}

export function fetchPartitionMeta() {
    return dispatch => {
        return $http.get(`meta/partition`).then(data => {
            dispatch(receivePartitionMeta(data.data))
        })
    }
}

function receivePartitionMeta(data) {
    return {
        type: RECEIVE_PARTITION_META,
        data
    }
}

/**
 * Do unit transfer based prop values and metadata
 */
export function calculateUnitValues(initValues, propUnit, unitMeta, metaLayout) {
    if (!propUnit || !unitMeta || !metaLayout) {
        return initValues
    }
    let initValues1 = {}
    if (initValues) {
        for (var key in initValues) {
            let value = initValues[key]
            let nKey = key.replace(/\_/gi, '.')
            let propMeta = metaLayout[nKey]
            if (propMeta && propMeta['valueSource']
                && unitMeta[propMeta['valueSource']]
                && propUnit[unitMeta[propMeta['valueSource']]['key']]
                && propUnit[unitMeta[propMeta['valueSource']]['key']] == UNIT_BRITISH
            ) {
                let newValue = value

                if (unitMeta[propMeta['valueSource']]['key'] == UNIT_ITEM_TEMPRETURE) {
                    //计算温度，逻辑单独在方法中定义
                    newValue = transferTempretureToBritsh(value)
                } else {
                    //使用默认的乘除来计算
                    newValue = value * unitMeta[propMeta['valueSource']]['tob']
                    newValue = Number(newValue.toFixed(4))
                }
                //Transfer to british value
                // initValues1[key] = value * unitMeta[propMeta['valueSource']]
                initValues1[key] = newValue
                // console.log("Convert value from " + value + " to " + newValue)
            } else {
                initValues1[key] = value
            }


        }
    }
    return initValues1
}

/**
 * 判断一个属性，用户选择的是英制还是公制，如果是英制，在存储是，需要把英制的value转换为公制存储，这样默认在存储的值都会是公制，
 * 所有的英制的value，都是在渲染过程中计算出来
 * @param key 属性key
 * @param value 属性值
 * @param propUnit 属性的公制／英制定义
 * @param unitMeta 属性类型的元数据
 * @param metaLayout 属性的元数据
 **/
export function calculateUnitValueToMetric(key, value, propUnit, unitMeta, metaLayout) {

    let nKey = key.replace(/\_/gi, '.')
    let propMeta = metaLayout[nKey]
    let newValue = value
    // if(key == 'meta_ahu_sairvolume'){
    // }   
    if (propMeta && propMeta['valueSource']
        && unitMeta[propMeta['valueSource']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']] == 'M'
    ) {

        if (unitMeta[propMeta['valueSource']]['key'] == UNIT_ITEM_TEMPRETURE) {
            newValue = transferTempretureToMetrix(value)
        } else {
            newValue = value * unitMeta[propMeta['valueSource']]['tom']

        }
        if (propMeta['valueType'] != "IntV") {
            newValue = Number(newValue.toFixed(1))
        } else {
            newValue = Number(newValue.toFixed(0))
        }
    }
    return newValue
}

//在给后端传递数据的时候需要走这个方法把英制改成公制
export function calculateUnitValueToMetric2(key, value, propUnit, unitMeta, metaLayout) {

    let nKey = key.replace(/\_/gi, '.')
    let propMeta = metaLayout[nKey]
    let newValue = value
    // if(key == 'meta_ahu_sairvolume'){
    // }   
    if (propMeta && propMeta['valueSource']
        && unitMeta[propMeta['valueSource']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']] == 'B'
    ) {

        if (unitMeta[propMeta['valueSource']]['key'] == UNIT_ITEM_TEMPRETURE) {
            newValue = transferTempretureToMetrix(value)
        } else {
            newValue = value * unitMeta[propMeta['valueSource']]['tom']

        }
        if (propMeta['valueType'] != "IntV") {
            newValue = Number(newValue.toFixed(1))
        } else {
            newValue = Number(newValue.toFixed(0))
        }
    }
    return newValue
}

/**
 * 判断一个属性，用户选择的是英制还是公制，如果是英制，在展现时，需要把公制的value转换为英制存储
 * ，过程在渲染过程中执行
 * @param key 属性key
 * @param value 属性值
 * @param propUnit 属性的公制／英制定义
 * @param unitMeta 属性类型的元数据
 * @param metaLayout 属性的元数据
 **/
export function calculateUnitValueToBritish(key, value, propUnit, unitMeta, metaLayout) {

    let nKey = key.replace(/\_/gi, '.')
    let propMeta = metaLayout[nKey]
    let newValue = value
    if (propMeta && propMeta['valueSource']
        && unitMeta[propMeta['valueSource']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']]
        && propUnit[unitMeta[propMeta['valueSource']]['key']] == UNIT_BRITISH
    ) {

        if (unitMeta[propMeta['valueSource']]['key'] == UNIT_ITEM_TEMPRETURE) {
            newValue = transferTempretureToBritsh(value)
        } else {
            newValue = value * unitMeta[propMeta['valueSource']]['tob']

        }
        if (propMeta['valueType'] != "IntV") {
            newValue = Number(newValue.toFixed(1))
        } else {
            newValue = Number(newValue.toFixed(0))
        }
    }
    return newValue
}

/**
 * {intl.get(UNIT)}转换， to {intl.get(IMPERIAL)}
 * @param value
 */
function transferTempretureToBritsh(value) {
    if (!value) {
        value = 0
    }
    let newValue = 32 + value * 1.8
    newValue = Number(newValue.toFixed(2))
    return newValue
}

/**
 * {intl.get(UNIT)}转换，to{intl.get(METRIC)}
 * @param value
 */
function transferTempretureToMetrix(value) {
    let newValue = (value - 32) / 1.8
    newValue = Number(newValue.toFixed(2))
    return newValue
}

let initLayoutData = function (layout, sections) {
    let layoutData = [[], [], [], []]
    layout.layoutData = layoutData
    if (isSingleLine(layout.style)) {
        sections.forEach((section, index) => {
            layoutData[0].push(section.position)
        })
    } else if (layout.style == STYLE_WHELE) {
        let recycleStart = false
        sections.forEach((section, index) => {
            if (!recycleStart) {
                layoutData[0].push(section.position)
            } else {
                layoutData[2].push(section.position)
            }
            if (section.sectionKey === "ahu.wheelHeatRecycle") {
                recycleStart = true
            }
        })
    } else if (layout.style == STYLE_PLATE) {
        let recycleStart = false
        sections.forEach((section, index) => {
            if (!recycleStart) {
                layoutData[0].push(section.position)
            } else {
                layoutData[3].push(section.position)
            }

            if (section.sectionKey === "ahu.plateHeatRecycle") {
                recycleStart = true
            }
        })
    }
}

/**
 * 初始化一个机组的布局信息
 * @param sections
 * @returns {{}}
 */
export function initAhuLayout(sections) {
    let layout = {}
    layout.style = STYLE_COMMON
    for (var index in sections) {
        let section = sections[index]
        if (section.sectionKey == "ahu.wheelHeatRecycle") {
            layout.style = STYLE_WHELE
            break
        } else if (section.sectionKey == "ahu.plateHeatRecycle") {
            layout.style = STYLE_PLATE
            break
        } else if (section.sectionKey == "ahu.combinedMixingChamber") {
            layout.style = STYLE_CHAMBER
            break
        } else {
            layout.style = STYLE_COMMON
        }
    }
    initLayoutData(layout, sections)
    return layout

}
/**
 * 本方法用于获取style，单排10，单排且有新回排11，STYLE_WHELE转轮热回收21，STYLE_PLATE板式热回收22
 */
export function getStyle(sections) {
    let style = 10
    sections.forEach((section) => {
        if (section.name === 'ahu.wheelHeatRecycle') {
            style = 21
        } else if (section.name === 'ahu.plateHeatRecycle') {
            style = 22
        } else if (section.name === 'ahu.combinedMixingChamber') {
            style = 11
        }
    })
    return style
}

/**
 * 本方法基于扫描当前的机组的结构，重新构造段的布局信息，并且依据布局信息，更新每个段的风向
 */
export function updateLayout(style, componentValue, sections) {
    let nlayout = {}
    let airDirection = {}
    // style.style = getStyle(sections)
    nlayout.style = getStyle(sections)


    nlayout.layoutData = []
    let ahuDs = []
    if (isSingleLine(nlayout.style)) {
        ahuDs.push($('#ahu'))
    } else if (isTwinLine(nlayout.style)) {
        ahuDs.push($('#ahutl'))
        ahuDs.push($('#ahutr'))
        ahuDs.push($('#ahubl'))
        ahuDs.push($('#ahubr'))
    }

    let combinedMixingChamberPos = 100
    let pos = 0
    let groupId = 0
    ahuDs.forEach((ahud, index) => {
        nlayout.layoutData.push([])
        ahud.children().each((index, element) => {
            let eid = $(element).attr('data-section-id')
            let ekey = $(element).attr('data-section')
            if (ekey == 'ahu.combinedMixingChamber') {
                combinedMixingChamberPos = pos
            }
            if (eid) {//eid没取到的时候不应该添加该属性
                airDirection[eid] = calculateAirDirection(nlayout.style, groupId, pos, combinedMixingChamberPos)
            }
            nlayout.layoutData[groupId].push(eid)
            pos++
        })
        groupId++
    })
    return { nlayout: nlayout, airDirection: airDirection }
}

/**
 *
 * @param layoutStyle
 * @param groupId 在哪一个组
 * @param pos 当前段的位置
 * @param combinedMixingChamberPos 新回排风段的位置，默认100
 * @returns {*}
 */
function calculateAirDirection(layoutStyle, groupId, pos, combinedMixingChamberPos) {
    if (layoutStyle == STYLE_WHELE) {
        if (groupId == 0 || groupId == 1) {
            return "R"
        } else {
            return "S"
        }

    } else if (layoutStyle == STYLE_PLATE) {
        if (groupId == 0 || groupId == 3) {
            return "S"
        } else {
            return "R"
        }
    } else if (layoutStyle == STYLE_CHAMBER) {
        if (pos < combinedMixingChamberPos) {
            return "R"
        } else {
            return "S"
        }
    } else {
        return "S"
    }
}

export function isSingleLine(style) {
    return style == STYLE_COMMON || style == STYLE_CHAMBER
}

export function isSectionPositionFixed(layout, sectionPosition) {
    // 立式双层机组，风机与底部空段/干蒸汽加湿段固定
    if (layout.style == STYLE_VERTICAL_1 || layout.style == STYLE_VERTICAL_2 ||
            layout.style == STYLE_DOUBLE_RETURN_1 || layout.style == STYLE_DOUBLE_RETURN_2
            || layout.style == STYLE_SIDE_BY_SIDE_RETURN) {
        let topLeftLayout = layout.layoutData[0];
        let bottomLeftLayout = layout.layoutData[2];
        if (topLeftLayout[topLeftLayout.length - 1] == sectionPosition) {
            return true;
        } else if (bottomLeftLayout[bottomLeftLayout.length - 1] == sectionPosition) {
            return true;
        }
    }
    return false;
}

export function isVerticalLine(style) {
    return style == STYLE_VERTICAL_1 || style == STYLE_VERTICAL_2
}

export function isReturnLine(style) {
    return style == STYLE_DOUBLE_RETURN_1 || style == STYLE_DOUBLE_RETURN_2 || style == STYLE_SIDE_BY_SIDE_RETURN
}

export function isTwinLine(style) {
    return style == STYLE_PLATE || style == STYLE_WHELE || isVerticalLine(style) || isReturnLine(style)
}

/**
 * 计算当前的段的顺序，并返回数组
 * @returns {Array}
 */
export function getSimpleSectionArr() {
    var sections = []
    let ahuDivs = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
    let hasImpact = false, hasWheelHeat = false, hasPlateHeat = false, impactIndex = 1
    ahuDivs.forEach((ahuDiv, ahuIndex) => {
        let isLine1 = ['ahutl', 'ahutr'].indexOf(ahuDiv.attr('id')) !== -1;
        let isLine2 = ['ahubl', 'ahubr'].indexOf(ahuDiv.attr('id')) !== -1;
        // console.log('zzf4', ahuDiv.children('[data-section]'))
        ahuDiv.children('[data-section]').each((index, element) => {
            if ($(element).attr('data-section') == 'ahu.plateHeatRecycle') {
                hasPlateHeat = true
            } else if ($(element).attr('data-section') == 'ahu.wheelHeatRecycle') {
                hasWheelHeat = true
            } else if ($(element).attr('data-section') == 'ahu.combinedMixingChamber') {
                hasImpact = true
                impactIndex = index
            }
        })
        ahuDiv.children('[data-section]').each((index, element) => {
            var section = {
                id: $(element).attr('data-section-id'),
                name: $(element).attr('data-section'),
                displayName: $(element).attr('data-original-title'),
            }
            if (ahuIndex == 0) {
                if (hasImpact) {
                    if (index <= impactIndex) {
                        section.direction = 'R'
                    } else {
                        section.direction = 'S'
                    }
                } else {
                    section.direction = 'S'
                }
            }
            if (ahuIndex == 1) {
                if (hasPlateHeat) {
                    section.direction = 'S'
                } else {
                    section.direction = 'R'
                }
            }
            if (ahuIndex == 2) {
                if (hasPlateHeat) {
                    section.direction = 'R'
                } else {
                    section.direction = 'R'
                }
            }
            if (ahuIndex == 3) {
                if (hasPlateHeat) {
                    section.direction = 'R'
                } else {
                    section.direction = 'S'
                }
            }
            if (ahuIndex == 4) {
                if (hasPlateHeat) {
                    section.direction = 'S'
                } else {
                    section.direction = 'S'
                }
            }

            if (isLine1) {
                section.line = 0;
            } else if (isLine2) {
                section.line = 1;
            }
            sections.push(section)
        })
    })

    return sections
}

/**
 * 获取当前段，同时包含前后段信息
 */
export function getPreAndNextSection(componentValue, selectedComponent) {
    let section = getSimpleSectionArr();
    let currentIndex = null;//当前选中的段在sections中的index
    let pre = null, next = null;
    section.forEach((record, index) => {
        if (record.id == selectedComponent.id) {
            currentIndex = index
        }
    })
    let directionSection = validateSection(section, componentValue, selectedComponent, false)
    directionSection.forEach((_record) => {
        let dsIndex = null//当前段在排好RS链中的index
        _record[1].forEach((__record, __index) => {
            if (__record == currentIndex) {
                dsIndex = __index
            }
        })
        if (dsIndex != null) {
            if (dsIndex == 0) {
                if (_record[1].length > 1) {
                    next = _record[1][dsIndex + 1]
                }
            } else if (dsIndex == (_record[1].length - 1)) {
                pre = _record[1][dsIndex - 1]
            } else {
                pre = _record[1][dsIndex - 1]
                next = _record[1][dsIndex + 1]
            }
        }
    })
    if (pre != null) {
        pre = section[pre].name.split('.')[1]
    }
    if (next != null) {
        next = section[next].name.split('.')[1]
    }
    let result = {
        pre: pre,
        next: next
    }
    return result
}
/**
 * 同步段图片的方向
 */
export function syncDirection() {
    // console.log('zzf7', getSimpleSectionArr())
}

/**
 * 部分参数失去焦点时，将该参数同步到其他属性上
 */
export function syncOtherParams(event, newValue, previousValue, ownProps, unitPreferCode) {
    return dispatch => {

        if (event.target.name == 'meta_ahu_sexternalstatic') {//机外静压(送)(Pa),同步所有风机（送）的余压
            dispatch(syncOtherParam('S', 'meta_section_fan_externalStatic', newValue, unitPreferCode))
        }
        if (event.target.name == 'meta_ahu_eexternalstatic') {//机外静压(回)(Pa),同步所有风机（回）的余压
            dispatch(syncOtherParam('R', 'meta_section_fan_externalStatic', newValue, unitPreferCode))
        }
        if (event.target.name == 'meta_ahu_sairvolume') {// 
            dispatch(syncOtherParam('S', 'meta_section_fan_airVolume', newValue, unitPreferCode))
        }
        if (event.target.name == 'meta_ahu_eairvolume') {// 
            dispatch(syncOtherParam('R', 'meta_section_fan_airVolume', newValue, unitPreferCode))
        }
        if (event.target.name == 'meta_section_fan_extraStatic') {//其他静压,同步总静压
            let value = Number(ownProps.componentValue.meta_section_fan_sysStatic) + Number(ownProps.componentValue.meta_section_fan_externalStatic) + Number(newValue)
            dispatch(syncOtherParam(ownProps.componentValue.meta_section_airDirection, 'meta_section_fan_totalStatic', value, unitPreferCode))
        }


    }
}

export function syncOtherParam(direction, name, newValue, unitPreferCode) {
    return {
        type: SYNC_OTHER_PARAM,
        direction,
        name,
        newValue,
        unitPreferCode
    }
}

export function changeUvLamp(value) {
    return (dispatch, getState) => {
        let serial = getState().ahu.componentValue[0]['meta_ahu_serial']
        if (value === 'true') {
            return $http.get(`calc/uv/${serial}`).then(data => {
                dispatch(syncUvModel(data.data))
            })
        } else {
            dispatch(syncUvModel(''))
        }
    }
}

function syncUvModel(value) {
    return {
        type: SYNC_UVMODEL,
        value,
    }
}

export function cleanCalcute(name, componentValues, selectedComponent, metaLayout) {
    let copyValues = { ...componentValues }
    let str = 'meta', arr = [];
    let seasonSplit = name.split('_')
    let season = seasonSplit[seasonSplit.length - 1].split('')[0]//夏季为S，冬季为W，其他的为通用属性
    let isPerformance = metaLayout[name.replace(/\_/g, '.')].rPerformance
    if (!isPerformance) {
        return componentValues
    }
    if (selectedComponent.id != 0) {
        str = str + '.section.' + selectedComponent.name.split('.')[1]
        for (let key in metaLayout) {
            if (key.includes(str) && metaLayout[key].global) {
                let keySplit = key.split('.')
                let keys = keySplit[keySplit.length - 1].split('')[0]
                if (season == keys || (season != 'W' && season != 'S')) {
                    arr.push(key.replace(/\./g, '_'))
                }
            }
        }
        arr.forEach((record) => {
            if (record != name) {
                copyValues[selectedComponent.id][record] = ''
            }
        })
    } else if (name == 'meta_ahu_sairvolume') {
        let strArr = [
            'meta.section.coolingCoil',
            'meta.section.heatingCoil',
            'meta.section.fan',
        ]
        strArr.forEach((record) => {
            for (let key in metaLayout) {
                if (key.includes(record) && metaLayout[key].global) {
                    arr.push(key.replace(/\./g, '_'))
                }
            }
        })
        arr.forEach((record) => {
            for (let key in copyValues) {
                if (copyValues[key].hasOwnProperty(record)) {
                    copyValues[key][record] = ''
                }
            }
        })
        for (let key in componentValues) {
            if (componentValues[key].hasOwnProperty('meta_section_completed')) {
                componentValues[key]['meta_section_completed'] = false
            }
        }
    }
    return componentValues

}

export function updateSelected(selected) {
    return {
        type: UPDATE_SELECTED,
        selected,
    }
}


export function dismiss(bool) {
    return {
        type: DISMISS,
        bool,
    }
}

export function handleOk(data) {
    return {
        type: HANDLEOK,
        data,
    }
}

export function onThreeViewModalClosed() {
    return {
        type: THREE_VIEW_MODAL_CLOSED
    }
}

export function ahuSerial(ahu) {
    if (ahu && ahu.meta_ahu_serial) {
        return true
    }
    return false
}