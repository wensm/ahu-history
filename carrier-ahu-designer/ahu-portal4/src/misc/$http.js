import {
    ERROR,
} from '../pages/intl/i18n'

import NProgress from 'nprogress'
import sweetalert from 'sweetalert'
import intl from 'react-intl-universal'
import { hashHistory } from 'react-router'
const $http = {

  get(url, data,hideErrAlert) {
    return this.request(url, data, 'get',hideErrAlert);
  },

  post(url, data,hideErrAlert) {
    return this.request(url, data, 'post',hideErrAlert)
  },

  put(url, data,hideErrAlert) {
    return this.request(url, data, 'put',hideErrAlert)
  },

  delete(url, data,hideErrAlert) {
    return this.request(url, data, 'delete',hideErrAlert)
  },

  request(url, data, type,hideErrAlert) {
    NProgress.start()
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${SERVICE_URL}${url}`,
        type: type,
        data: data ? data : {},
        cache: false,
      }).done(resp => {
        const data = typeof(resp) === 'string' ? JSON.parse(resp) : resp
        if (data.code && data.code !== '0') {
          if(!hideErrAlert){
            sweetalert({
              title: intl.get(ERROR),
              type: 'error',
              text: data.msg,
              showConfirmButton: true
            })
          }
          reject({
            code:-99,
            msg:data.msg
          })
        }else {
          resolve(data)
        }
      }).fail(response => {
        sweetalert({
          title: intl.get(ERROR) + ((response.responseJSON && response.responseJSON.message) ? '\n' + response.responseJSON.message : ''),
          type: 'error',
          showConfirmButton: true
        })
        reject(typeof(response) === 'string' ? JSON.parse(response) : response)
      }).always(() => {
        NProgress.done()
      })
    });
  },
}

export default $http
