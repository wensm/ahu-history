package com.carrier.ahu.service.cal;

import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;

/**
 * ahu 、section计算
 *
 * @author
 */
public interface ICalEngine {
    String ID_WEIGHT = "cal.weight";
    String ID_PRICE = "cal.price";
    String ID_RESISTANCE = "cal.resistance";
    String ID_FILTERARRANGEMENT = "cal.filterarrangement";
    String ID_LENGTH = "cal.length";
    String ID_TEMPERATURE = "cal.temperature";
    String ID_HEATQ = "cal.heatq";
    String ID_HUMQ = "cal.humq";
    String ID_COIL = "cal.coil";
    String ID_FAN = "cal.fan";
    String ID_HUM = "cal.humidification";
    String ID_RECYCLE = "cal.recycle";
    String ID_ELECTRICHEATINGCOIL = "cal.electricheatingcoil";
    String ID_NS_PRICE = "cal.nsprice";
    String ID_VALIDATE = "cal.validate";

    ICalContext cal(AhuParam ahuParam, PartParam partParam, ICalContext context) throws Exception;

    String[] getEnabledSectionIds();

    /**
     * Return unit series array which not apply for this cal engine.
     * 
     * @return
     */
    String[] getDisabledUnitSeries();

    int getOrder();

    String getKey();
    
    int getCalLevel();
    
}
