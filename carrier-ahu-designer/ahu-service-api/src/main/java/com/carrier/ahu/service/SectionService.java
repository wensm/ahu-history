package com.carrier.ahu.service;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.report.PartPO;

import java.util.List;

/**
 * 段业务接口 Created by Wen zhengtao on 2017/3/17.
 */
public interface SectionService {
	/**
	 * 新增段
	 *
	 * @param part
	 * @param userName
	 * @return
	 */
	String addSection(Part part, String userName);

	/**
	 * 批量新增段
	 *
	 * @param parts
	 * @return
	 */
	List<String> addSections(List<Part> parts);

	/**
	 * 批量修改段
	 *
	 * @param parts
	 * @return
	 */
	List<String> updateSections(List<Part> parts);

	/**
	 * 修改段
	 * 
	 * @param part
	 * @param userName
	 * @return
	 */
	String updateSection(Part part, String userName);

	/**
	 * 删除段
	 * 
	 * @param sectionId
	 */
	void deleteSection(String sectionId);

	/**
	 * 删除机组中所有的段
	 * 
	 * @param unitid
	 */
	void deleteAHUSections(String unitid);

	/**
	 * 查询段详情
	 * 
	 * @param sectionId
	 * @return
	 */
	Part findSectionById(String sectionId);

	/**
	 * 查询段列表
	 * 
	 * @param ahuId
	 * @return
	 */
	List<Part> findSectionList(String ahuId);

	/**
	 * 有序列表
	 * 
	 * @param ahuId
	 * @return
	 */
	public List<PartPO> findLinkedSectionsList(String ahuId);

	/**
	 * 批量获取项目下所有段的ID
	 *
	 * @param pid
	 * @return
	 */
	List<String> getAllIds(String pid);
}
