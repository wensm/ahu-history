package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.Partition;

/**
 * 分段业务接口
 */
public interface PartitionService {
    
    Partition savePartition(Partition partition, String userName);
    
	/**
	 * 新增分段
	 *
	 * @param partition
	 * @return
	 */
	Partition addPartition(Partition partition);

	/**
	 * 修改段
	 *
	 * @param partition
	 * @return
	 */
	Partition updatePartition(Partition partition);

	/**
	 * 删除分段
	 *
	 * @param partitionId
	 */
	void deletePartition(String partitionId);

	/**
	 * 根据分段ID查询分段详情
	 *
	 * @param partitionId
	 * @return
	 */
	Partition findPartitionById(String partitionId);

	/**
	 * 根据查询分段
	 *
	 * @param ahuId
	 * @return
	 */
	Partition findPartitionByAHUId(String ahuId);

	/**
	 * 更新Partition
	 * 
	 * @param partitionList
	 * @param userName
	 * @return
	 */
	List<String> addOrUpdateAhus(List<Partition> partitionList);

	/**
	 * 批量获取项目下所有分段的ID
	 *
	 * @param pid
	 * @return
	 */
	List<String> getAllIds(String pid);
}
