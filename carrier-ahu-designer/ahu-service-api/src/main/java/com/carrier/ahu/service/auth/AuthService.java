package com.carrier.ahu.service.auth;

import com.carrier.ahu.common.entity.User;

/**
 * 认证类业务接口
 * @author JL
 *
 */
public interface AuthService {
	/**
	 * 用户登录
	 * @param name
	 * @param password
	 * @return
	 */
	User validate(String name, String password);
}
