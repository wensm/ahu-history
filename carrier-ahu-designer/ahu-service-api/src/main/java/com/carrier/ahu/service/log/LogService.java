package com.carrier.ahu.service.log;

import java.io.File;
import java.util.List;

import com.carrier.ahu.common.entity.LoginoutLog;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
public interface LogService {
    /**
     * 保存认证日志
     * @param log
     * @return
     */
    String saveLog(LoginoutLog log);

    List<File> getSystemLogFiles();

}
