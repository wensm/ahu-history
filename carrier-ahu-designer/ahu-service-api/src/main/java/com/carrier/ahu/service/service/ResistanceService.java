package com.carrier.ahu.service.service;

import org.springframework.stereotype.Service;

import com.carrier.ahu.common.exception.calculation.ResistanceCalcException;
import com.carrier.ahu.resistance.param.ResistanceBCVYBean;
import com.carrier.ahu.resistance.param.ResistanceParam;

/**
 * Created by liangd4 on 2017/9/28.
 * 计算阻力
 */
@Service
public interface ResistanceService {

    double getResistance(ResistanceParam resistanceParam) throws ResistanceCalcException;

    ResistanceBCVYBean getResistanceBean(ResistanceParam resistanceParam) throws ResistanceCalcException;

}
