package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.GroupInfo;

/**
 * 组相关接口
 */
public interface GroupService {
	/**
	 * 新增组
	 * 
	 * @param group
	 * @param userName
	 * @return
	 */
	String addGroup(GroupInfo group, String userName);

	/**
	 * 批量新增组
	 * 
	 * @param groupList
	 * @return
	 */
	List<String> addGroups(List<GroupInfo> groupList);

	/**
	 * 修改组
	 * 
	 * @param group
	 * @param userName
	 * @return
	 */
	String updateGroup(GroupInfo group, String userName);

	/**
	 * 删除组
	 * 
	 * @param groupId
	 * @return
	 */
	void deleteGroup(String groupId);

	/**
	 * 查询组详情
	 * 
	 * @param groupId
	 * @return
	 */
	GroupInfo findGroupById(String groupId);

	/**
	 * 查询组列表
	 * 
	 * @param projectId
	 * @return
	 */
	List<GroupInfo> findGroupList(String projectId);

	/**
	 * 查询组列表
	 * 
	 * @return
	 */
	Iterable<GroupInfo> findAll();

	/**
	 * 检查分组名称是否重复
	 * 
	 * @param groupName
	 * @return
	 */
	boolean checkName(String groupName);

	/**
	 * 检查分组名称是否重复
	 * 
	 * @param projectId
	 * @param groupName
	 * @return
	 */
	boolean checkName(String projectId, String groupName);

	/**
	 * 根据类型查询组列表
	 * 
	 * @param projectId
	 * @param groupType
	 * @return
	 */
	List<GroupInfo> findGroupListByType(String projectId, String groupType);

	/**
	 * 根据组编码查询组列表
	 * 
	 * @param projectId
	 * @param groupCode
	 * @return
	 */
	List<GroupInfo> findGroupListByCode(String projectId, String groupCode);

	/**
	 * 批量获取项目下所有组的ID
	 *
	 * @param pid
	 * @return
	 */
	List<String> getAllIds(String pid);
}
