package com.carrier.ahu.service;

import java.io.File;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.po.UnitType;

/**
 * ahu空气处理单元业务接口 Created by Wen zhengtao on 2017/3/17.
 */
public interface AhuService {
    /**
     * 默认每页现实的条数
     */
    int PAGE_SIZE = 20;

    /**
     * 新增ahu
     * 
     * @param unit
     * @param userName
     * @return
     */
    String addAhu(Unit unit, String userName);

    /**
     * 更新ahu
     * 
     * @param unit
     * @param userName
     * @return
     */
    String updateAhu(Unit unit, String userName);

    /**
     * 更新ahu
     * 
     * @param units
     * @param userName
     * @return
     */
    List<String> addOrUpdateAhus(List<Unit> units);

    /**
     * 删除ahu
     * 
     * @param ahuId
     * @return
     */
    void deleteAhu(String ahuId);

    /**
     * 查询ahu详情
     * 
     * @param ahuId
     * @return
     */
    Unit findAhuById(String ahuId);

    /**
     * 查询ahu列表
     * 
     * @param projectId
     * @param groupId 可以为空，为空时查询所有
     * @return
     */
    List<Unit> findAhuList(String projectId, String groupId);

    List<UnitType> getUnitTypes();

    /**
     * 查询当前最大AHU编号
     * 
     * @return
     */
    String getMaxAhuId();

    /**
     * 查询当前最大图纸编号
     * 
     * @return
     */
    String getMaxdrawingNo();

    /**
     * 根据图纸编号查询AHU，没有则为空
     * 
     * @param drawingNO
     * @return
     */
    Unit findAhuByDrawingNO(String projectid, String drawingNO);
    
    /**
     * 根据图纸编号和组编号查询AHU，没有则为空
     * @param projectid
     * @param drawingNO
     * @param groupId
     * @return
     */
    Unit findAhuByDrawingNOAndGroupId(String projectid, String drawingNO, String groupId);

    /**
     * 获取所有在用的机组编号
     */
    List<String> findAllUnitNOInuse(String projectId) throws Exception;

    /**
     * 修正项目下所有机组的编码排序<br>
     * exp:1 2 3 6 8 9 修正后 1 2 3 4 5
     */
    List<String> fixUnitNo(String projectId);

    /**
     * 分页查询ahu的接口，默认groupID不为空
     * 
     * @param projectId
     * @param groupId 可以为空，为空只查询未分组
     * @param page
     * @param pageSize
     * @return
     */
    Page<Unit> findAhuList(String projectId, String groupId, Pageable pageable);

    /**
     * 批量获取项目下所有机组的ID
     *
     * @param pid
     * @return
     */
    List<String> getAllIds(String pid);

    /**
     * Export AHU file.
     * 
     * @param unit
     * @return
     */
    File exportUnit(Unit unit,String version) throws Exception;

}
