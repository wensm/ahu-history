package com.carrier.ahu.service;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.util.AirConditionBean;

/**
 * @ClassName: ACCalculatorService 
 * @Description: (空调配置参数计算相关) 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年1月29日 下午7:34:13 
 *
 */
public interface ACCalculatorService {
	/**
	 * T,TB----->φ,I,D,F 通过干球温度、湿球温度 计算：相对湿度、焓、 含湿量、相对湿度
	 * @param ParmT
	 * @param ParmTB
	 * @param languageEnum 
	 * @return
	 * @throws TempCalErrorException 
	 */
	AirConditionBean FAirParmCalculate1(double ParmT, double ParmTB, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 求(等焓【湿膜加湿段(Wetfilm Humidifier)/高压喷雾加湿段(Spray Humidifier)】)加湿量
	 * 根据干球温度、湿球温度、相对湿度 ,求出风：干球温度、湿球温度、相对湿度、加湿量
	 * @param E_Db
	 * @param E_Wb
	 * @param RH
	 * @param airV
	 * @return
	 * @throws TempCalErrorException 
	 */
	HumidificationBean IsoenthalpyRHConvert(double E_Db, double E_Wb, double RH, int airV, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 求(等温【电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)】)加湿量
	 * 根据干球温度、湿球温度、相对湿度 ,求出风：干球温度、湿球温度、相对湿度、加湿量
	 * @param E_Db
	 * @param E_Wb
	 * @param RH
	 * @param airV
	 * @return
	 * @throws TempCalErrorException 
	 */
	HumidificationBean IsothermalRHConvert(double E_Db, double E_Wb, double RH, int airV, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 求(等焓【湿膜加湿段(Wetfilm Humidifier)/高压喷雾加湿段(Spray Humidifier)】)相对湿度
     * 根据干球温度、湿球温度 加湿量 求出风：干球温度、湿球温度、相对湿度、加湿量
	 * @param E_Db
	 * @param E_Wb
	 * @param humidification
	 * @param airV
	 * @return
	 * @throws TempCalErrorException 
	 */
	HumidificationBean IsoenthalpyHumidificationConvert(double E_Db, double E_Wb,
			double humidification, int airV, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 求(等温【电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)】)相对湿度
     * 根据干球温度、湿球温度 加湿量 求出风：干球温度、湿球温度、相对湿度、加湿量
	 * @param E_Db
	 * @param E_Wb
	 * @param humidification
	 * @param airV
	 * @return
	 * @throws TempCalErrorException 
	 */
	HumidificationBean IsothermalHumidificationConvert(double E_Db, double E_Wb,
			double humidification, int airV, LanguageEnum languageEnum) throws TempCalErrorException;
}
