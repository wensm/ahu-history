package com.carrier.ahu.engine.fan;

import org.junit.Test;

/**
 * Created by liaoyw on 2017/3/30.
 */
public class FanLibTest {
    static {
        System.setProperty("jna.debug_load", "true");
        System.setProperty("jna.library.path", "d:/workspace/java/CarrierAHU/carrier-ahu-designer/ahu-rest/asserts/dll");
    }

    @Test
    // 能出东西即可，具体含义还待整合
    public void testFanSelection() {
        int count = FanLib.INSTANCE.FanSelection(18000, 67.6, 0);
        System.out.println(count);

    }

    public static void main(String[] args) {
        System.setProperty("jna.debug_load", "true");
        System.setProperty("jna.library.path", "d:\\workspace\\java\\CarrierAHU\\carrier-ahu-designer\\ahu-rest\\asserts\\dll");
        int count = FanLib.INSTANCE.FanSelection(18000, 67.6, 0);
        System.out.println(count);
    }
}
