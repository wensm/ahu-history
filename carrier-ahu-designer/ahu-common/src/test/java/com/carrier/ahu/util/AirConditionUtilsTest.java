package com.carrier.ahu.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;

import com.carrier.ahu.unit.BaseDataUtil;

public class AirConditionUtilsTest {

    @Test
    public void testFAirParmCalculate1() throws Exception { // CalcOutAirParm
        double SInDryBulbT = 34;
        double SIndoorDryBulbT = 27;
        double SInWetBulbT = 28.2;
        double SIndoorWetBulbT = 19.5;
        double WheelEfficiency = 61.2;

        double SOutDryBulbT = BaseDataUtil
                .decimalConvert(SInDryBulbT - WheelEfficiency * (SInDryBulbT - SIndoorDryBulbT) / 100, 1);
        AirConditionBean SInWetAcb = AirConditionUtils.FAirParmCalculate1(SInDryBulbT, SInWetBulbT);
        AirConditionBean SIndoorWetAcb = AirConditionUtils.FAirParmCalculate1(SIndoorDryBulbT, SIndoorWetBulbT);

        double lOutD = SInWetAcb.paramD - WheelEfficiency * (SInWetAcb.paramD - SIndoorWetAcb.paramD) / 100;
        AirConditionBean SOutAcb = AirConditionUtils.FAirParmCalculate2(SOutDryBulbT, lOutD);

        double SOutWetBulbT = BaseDataUtil.decimalConvert(SOutAcb.parmTb, 0);
        double SOutRelativeT = BaseDataUtil.decimalConvert(SOutAcb.parmF, 1);
        System.out.println(SOutDryBulbT);
        System.out.println(SOutWetBulbT);
        System.out.println(SOutRelativeT);

        double WInDryBulbT = 10;
        double WIndoorDryBulbT = 27;
        double WInWetBulbT = 5;
        double WIndoorWetBulbT = 19.5;

        double WOutDryBulbT = BaseDataUtil
                .decimalConvert(WInDryBulbT - WheelEfficiency * (WInDryBulbT - WIndoorDryBulbT) / 100, 1);
        AirConditionBean WInWetAcb = AirConditionUtils.FAirParmCalculate1(WInDryBulbT, WInWetBulbT);
        AirConditionBean WIndoorWetAcb = AirConditionUtils.FAirParmCalculate1(WIndoorDryBulbT, WIndoorWetBulbT);

        lOutD = WInWetAcb.paramD - WheelEfficiency * (WInWetAcb.paramD - WIndoorWetAcb.paramD) / 100;
        AirConditionBean WOutAcb = AirConditionUtils.FAirParmCalculate2(WOutDryBulbT, lOutD);

        double WOutWetBulbT = BaseDataUtil.decimalConvert(WOutAcb.parmTb, 0);
        double WOutRelativeT = BaseDataUtil.decimalConvert(WOutAcb.parmF, 1);
        System.out.println(WOutDryBulbT);
        System.out.println(WOutWetBulbT);
        System.out.println(WOutRelativeT);
        
        System.out.println(new BigDecimal(WOutAcb.parmF).setScale(1, RoundingMode.HALF_EVEN).doubleValue());
    }

    @Test
    public void testFAirParmCalculate2() throws Exception {
        double SInDryBulbT = 34;
        double SIndoorDryBulbT = 27;
        double SInWetBulbT = 28.2;
        double SIndoorWetBulbT = 19.5;
        double WheelEfficiency = 61.2;

        double SExhaustDryBulbT = BaseDataUtil
                .decimalConvert(SIndoorDryBulbT - WheelEfficiency * (SIndoorDryBulbT - SInDryBulbT) / 100, 1);

        AirConditionBean SInWetAcb = AirConditionUtils.FAirParmCalculate1(SInDryBulbT, SInWetBulbT);
        AirConditionBean SIndoorWetAcb = AirConditionUtils.FAirParmCalculate1(SIndoorDryBulbT, SIndoorWetBulbT);

        double lExhaustD = SInWetAcb.paramD - WheelEfficiency * (SInWetAcb.paramD - SIndoorWetAcb.paramD) / 100;

        AirConditionBean SOutAcb = AirConditionUtils.FAirParmCalculate2(SExhaustDryBulbT, lExhaustD);
        double SExhaustWetBulbT = BaseDataUtil.decimalConvert(SOutAcb.parmTb, 0);
        double SExhaustRelativeT = BaseDataUtil.decimalConvert(SOutAcb.parmF, 1);

        System.out.println(SExhaustDryBulbT);
        System.out.println(SExhaustWetBulbT);
        System.out.println(SExhaustRelativeT);

        double WInDryBulbT = 10;
        double WIndoorDryBulbT = 27;
        double WInWetBulbT = 5;
        double WIndoorWetBulbT = 19.5;

        double WExhaustDryBulbT = BaseDataUtil
                .decimalConvert(WIndoorDryBulbT - WheelEfficiency * (WIndoorDryBulbT - WInDryBulbT) / 100, 1);

        AirConditionBean WInWetAcb = AirConditionUtils.FAirParmCalculate1(WInDryBulbT, WInWetBulbT);
        AirConditionBean WIndoorWetAcb = AirConditionUtils.FAirParmCalculate1(WIndoorDryBulbT, WIndoorWetBulbT);

        lExhaustD = WIndoorWetAcb.paramD - WheelEfficiency * (WIndoorWetAcb.paramD - WInWetAcb.paramD) / 100;

        AirConditionBean WOutAcb = AirConditionUtils.FAirParmCalculate2(WExhaustDryBulbT, lExhaustD);
        double WExhaustWetBulbT = BaseDataUtil.decimalConvert(WOutAcb.parmTb, 0);
        double WExhaustRelativeT = BaseDataUtil.decimalConvert(WOutAcb.parmF, 1);

        System.out.println(WExhaustDryBulbT);
        System.out.println(WExhaustWetBulbT);
        System.out.println(WExhaustRelativeT);
    }

    @Test
    public void test() {
        double dBV11 = 18000;
        double dDB11 = 27;
        double dDB21 = 34;
        double a = dBV11 / 3600 * 1.27 * 273 / (273 + dDB11) * (dDB11 - dDB21);
        System.out.println(a);
    }

    @SuppressWarnings("unused")
    @Test
    public void testCalcHeatParm() throws Exception {
        double NAVolume = 5;
        // double SInDryBulbT = 34;
        double SIndoorDryBulbT = 27;
        double SInWetBulbT = 28.2;
        double SIndoorWetBulbT = 19.5;
        double WheelEfficiency = 72.25;
        // double SInRelativeT = 65;

        double SInDryBulbT = 34;
        double SInRelativeT = 65;
        double SOutDryBulbT = 28.9;
        double SOutRelativeT = 56.4;

        AirConditionBean SIndoorAcb = AirConditionUtils.FAirParmCalculate3(SInDryBulbT, SInRelativeT);
        AirConditionBean SOutdoorAcb = AirConditionUtils.FAirParmCalculate3(SOutDryBulbT, SOutRelativeT);
        double SQt = Math.abs(BaseDataUtil.decimalConvert(
                NAVolume * 1.27 * 273 / (273 + SInDryBulbT) * (SIndoorAcb.paramI - SOutdoorAcb.paramI), 2));

        // 2、SQs
        double SQs = Math.abs(BaseDataUtil
                .decimalConvert(NAVolume * 1.27 * 273 / (273 + SInDryBulbT) * (SInDryBulbT - SOutDryBulbT), 2));

        // 3、SQl
        double SQl = SQt - SQs;

        System.out.println(SQt);
        System.out.println(SQs);

        double WInDryBulbT = 10;
        double WInRelativeT = 44;
        double WOutDryBulbT = 22.3;
        double WOutRelativeT = 53.5;
        AirConditionBean WIndoorAcb = AirConditionUtils.FAirParmCalculate3(WInDryBulbT, WInRelativeT);
        AirConditionBean WOutdoorAcb = AirConditionUtils.FAirParmCalculate3(WOutDryBulbT, WOutRelativeT);
        double WQt = Math.abs(BaseDataUtil.decimalConvert(
                NAVolume * 1.27 * 273 / (273 + WInDryBulbT) * (WIndoorAcb.paramI - WOutdoorAcb.paramI), 2));

        double WQs = Math.abs(BaseDataUtil
                .decimalConvert(NAVolume * 1.27 * 273 / (273 + WInDryBulbT) * (WInDryBulbT - WOutDryBulbT), 2));

        double WQl = WQt - WQs;
        System.out.println(WQt);
        System.out.println(WQs);
    }

}
