package com.carrier.ahu.license;

/**
 * Created by liangd4 on 2018/3/1.
 */
public class LicenseAdminTest {
    private static final String licensePath = "D:\\Work\\ahu_20180514\\carrier-ahu-designer\\ahu-rest\\asserts\\license\\";
    private static final String date = "2019-03-29";//到期日期
    private static final String factory = "THC";//工厂
    private static final String role = "engineer";//engineer、sale、factory

    public static void main(String[] args) throws Exception {
        String target = LicenseGeneratorTester.publicKeyDecode(licensePath + "\\license.dat");
        String[] tarArr = target.split(";");
        LicenseInfo licenseInfo = new LicenseInfo();
        for (int i = 0; i < tarArr.length; i++) {
            String value = tarArr[i];
            String[] valueArr = value.split("=");
            if (valueArr.length == 2) {
                String valueStr = valueArr[1];//获取对应信息的值
                if (i == 0) {
                    licenseInfo.setMac(valueStr);//获取文件mac
                } else if (i == 1) {
                    licenseInfo.setUserName(valueStr);//获取文件userName
                } else if (i == 2) {
                    licenseInfo.setDomain(valueStr);//获取文件domain
                }
            }
        }
        licenseInfo.setDate(date);
        licenseInfo.setFactory(factory);
        licenseInfo.setRole(role);
        LicenseGenerator.privateKeyEncodeTest(licenseInfo, licensePath);
    }
}
