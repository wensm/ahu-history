package com.carrier.ahu.common.upgrade;

import lombok.Data;

/**
 * Created by Braden Zhou on 2018/07/16.
 */
@Data
public class UpgradeMessage {

    private String type;
    private String content;

}
