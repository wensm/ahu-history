package com.carrier.ahu.common.exception.engine;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class EngineException extends AhuException {

    public EngineException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public EngineException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public EngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public EngineException(String message) {
        super(message);
    }

}
