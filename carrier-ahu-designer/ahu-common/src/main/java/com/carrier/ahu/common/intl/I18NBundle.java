package com.carrier.ahu.common.intl;

import static com.carrier.ahu.vo.FileNamesLoadInSystem.INTL_JSON_FILE_PATH;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * Resource bundle class provides i18n message interface.
 * 
 * Please use {@link com.carrier.ahu.common.configuration.AHUContext} instead of using it directly.
 * 
 * Created by Braden Zhou on 2018/04/12.
 */
@Slf4j
public class I18NBundle {

    private static I18NBundle instance = new I18NBundle();

    private Map<String, Map<String, String>> i18nBundles;

    public static I18NBundle getInstance() {
        if (null == instance) {
            instance = new I18NBundle();
        }
        return instance;
    }

    public static String getString(String resourceId, LanguageEnum locale) {
        return getInstance().getIntlString(resourceId, locale);
    }

    public static String getString(String resourceId, LanguageEnum locale, Object... objects) {
        return MessageFormat.format(getString(resourceId, locale), objects);
    }

    private I18NBundle() {
        i18nBundles = new HashMap<>();
    }

    private String getIntlString(String resourceId, LanguageEnum locale) {
        if (locale == null) {
            locale = LanguageEnum.Chinese;
        }
        if (i18nBundles.containsKey(locale.getId())) {
            Map<String, String> i18nBundle = i18nBundles.get(locale.getId());
            String tempId = resourceId + "_1"; // merge from back-end, remove after finished refactor
            if (i18nBundle.containsKey(tempId)) {
                return i18nBundle.get(tempId);
            } else if (i18nBundle.containsKey(resourceId)) {
                return i18nBundle.get(resourceId);
            }
        } else {
            loadBundle(locale);
            return this.getIntlString(resourceId, locale);
        }
        return resourceId;
    }

    private void loadBundle(LanguageEnum locale) {
        String jsonFile = String.format(INTL_JSON_FILE_PATH, locale.getId());
        InputStream is = I18NBundle.class.getClassLoader().getResourceAsStream(jsonFile);
        if (EmptyUtil.isEmpty(is)) {
            log.info(String.format("Failed to load resouce json file for language: %s", locale.getId()));
            i18nBundles.put(locale.getId(), new HashMap<>());
        } else {
            Gson gson = new Gson();
            Map<String, Map<String, String>> bundle = gson.fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),
                    new TypeToken<Map<String, Map<String, String>>>() {
                    }.getType());
            Map<String, String> i18nBundle = new HashMap<>();
            for (String key : bundle.keySet()) {
                Map<String, String> sectionBundle = bundle.get(key);
                i18nBundle.putAll(sectionBundle);
            }
            i18nBundles.put(locale.getId(), i18nBundle);
            log.info(String.format("Load resouce json file for language: %s successfully", locale.getId()));
        }
    }

}
