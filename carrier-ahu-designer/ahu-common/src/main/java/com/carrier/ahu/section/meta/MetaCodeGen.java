package com.carrier.ahu.section.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.po.meta.Meta;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.ValueType;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

public class MetaCodeGen {

	protected static Logger logger = LoggerFactory.getLogger(MetaCodeGen.class);

	/**
	 * 根据Section的Meta，计算section对应的属性的前缀。 用于框架自动生成Meta属性，以及属性的计算
	 *
	 * @param meta
	 * @return
	 */
	public static String calculateAttributePrefix(Meta meta) {
		return calculateAttributePrefix(meta.getMetaId());
	}

	public static String calculateAttributePrefix(String metaId) {
		if (EmptyUtil.isEmpty(metaId)) {
			logger.warn("metaId is null when calculateAttributePrefix()");
			return null;
		}
		if (CommonConstant.METACOMMON_DEFAULTPARA.equals(metaId)) {
			return metaId;
		}
		String simpleType = metaId.replace(CommonConstant.METAHU_AHU_NAME, CommonConstant.METASEXON_SECTION);
		String sectionName = CommonConstant.SYS_BLANK;
		if (StringUtils.isNotBlank(simpleType)) {
			sectionName = CommonConstant.SYS_PUNCTUATION_DOT + simpleType;
		}
		return CommonConstant.METACOMMON_META + sectionName;
	}

	/**
	 * 机组配置中的属性值
	 * 
	 * @param metaId
	 * @return
	 */
	public static String genAHUParamName(String metaId) {
		if (EmptyUtil.isEmpty(metaId)) {
			logger.warn("metaId is null when calculateAttributePrefix()");
			return null;
		}

		if (metaId.contains(CommonConstant.METAHU_AHU_NAME)) {
			return metaId;
		}

		return CommonConstant.METAHU_PREFIX + metaId;
	}

	/**
	 * 
	 * @param metaId
	 *            <br>
	 *            属性名，Exp：meta.section.filter.Resistance
	 * @return
	 */
	public static String calculateSectionTypeIdByMetaKey(String metaKey) {
		if (EmptyUtil.isEmpty(metaKey)) {
			logger.warn("metaKey is null when calculateSectionTypeIdByMetaKey()");
			return null;
		}
		String rpKey = metaKey.replace(CommonConstant.METASEXON_PREFIX, CommonConstant.METAHU_AHU_NAME);
		int index = rpKey.indexOf(CommonConstant.SYS_PUNCTUATION_DOT);
		String cutKey = rpKey.substring(index + 1);
		int index1 = cutKey.indexOf(CommonConstant.SYS_PUNCTUATION_DOT);
		if (index1 < 0) {
			return null;
		}
		String simpleKey = cutKey.substring(0, index1);
		if ("ahu".equals(simpleKey)) {
			return simpleKey;
		} else {
			return CommonConstant.METAHU_AHU_NAME + simpleKey;
		}
	}

	/**
	 *
	 * @param meta
	 * @param metaId
	 */
	@Deprecated
	public static void appendDefaultMeta(Meta meta, String metaId) {
		appendDefaultMeta(meta);
	}

	/**
	 * Add default parameter into every Meta object
	 *
	 * @param meta
	 */
	@Deprecated
	public static void appendDefaultMeta(Meta meta) {
		String prefix = calculateAttributePrefix(meta);

		// Default Parameter
//		meta.appendParameter(new MetaParameter(prefix + ".SInDryBulbT", "InDryBulbT", "DB Temp.(R.A.)", false, true,
//				true, true, false, false, "Tempreture", ValueType.DoubleV, "干球温度(回)", "DB Temp.(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".SInRelativeT", "InRelativeT", "RH(R.A.)", false, true, false,
//				true, false, false, "Persent", ValueType.DoubleV, "相对湿度(回)", "RH(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".SInWetBulbT", "InWetBulbT", "WB Temp.(R.A.)", false, true,
//				true, true, false, false, "Tempreture", ValueType.DoubleV, "湿球温度(回)", "WB Temp.(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".SOutDryBulbT", "OutDryBulbT", "DB Temp.(L.A.)", false, true,
//				true, false, false, false, "Tempreture", ValueType.DoubleV, "干球温度(出)", "DB Temp.(L.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".SOutRelativeT", "OutRelativeT", "RH(L.A.)", false, true,
//				false, false, false, false, "Persent", ValueType.DoubleV, "相对湿度(出)", "RH(L.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".SOutWetBulbT", "OutWetBulbT", "WB Temp.(L.A.)", false, true,
//				true, false, false, false, "Tempreture", ValueType.DoubleV, "湿球温度(出)", "WB Temp.(L.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WInDryBulbT", "InDryBulbT", "DB Temp.(R.A.)", false, true,
//				true, true, false, false, "Tempreture", ValueType.DoubleV, "干球温度(回)", "DB Temp.(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WInRelativeT", "InRelativeT", "RH(R.A.)", false, true, false,
//				true, false, false, "Persent", ValueType.DoubleV, "相对湿度(回)", "RH(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WInWetBulbT", "InWetBulbT", "WB Temp.(R.A.)", false, true,
//				true, true, false, false, "Tempreture", ValueType.DoubleV, "湿球温度(回)", "WB Temp.(R.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WOutDryBulbT", "OutDryBulbT", "DB Temp.(L.A.)", false, true,
//				true, false, false, false, "Tempreture", ValueType.DoubleV, "干球温度(出)", "DB Temp.(L.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WOutRelativeT", "OutRelativeT", "RH(L.A.)", false, true,
//				false, false, false, false, "Persent", ValueType.DoubleV, "相对湿度(出)", "RH(L.A.)"));
//		meta.appendParameter(new MetaParameter(prefix + ".WOutWetBulbT", "OutWetBulbT", "WB Temp.(L.A.)", false, true,
//				true, false, false, false, "Tempreture", ValueType.DoubleV, "湿球温度(出)", "WB Temp.(L.A.)"));
		// Default Parameter2
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_SECTIONL, CommonConstant.METACOMMON_SECTIONL, CommonConstant.SYS_BLANK, false, true, true, true, false,
				false, prefix + CommonConstant.METACOMMON_POSTFIX_SECTIONL, ValueType.IntV, "段长", CommonConstant.METACOMMON_LENGTH_NAME));
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_RESISTANCE, CommonConstant.SYS_STRING_RESISTANCE_3, CommonConstant.SYS_BLANK, false, true, true, false,
				false, false, CommonConstant.METACOMMON_RESISTANCE, ValueType.DoubleV, "阻力", CommonConstant.SYS_STRING_RESISTANCE_3));
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_WEIGHT, CommonConstant.METACOMMON_WEIGHT_NAME, CommonConstant.SYS_BLANK, false, true, true, false, false, false,
				CommonConstant.METACOMMON_WEIGHT, ValueType.DoubleV, "重量", CommonConstant.METACOMMON_WEIGHT_NAME));
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_NOSTANDARD, CommonConstant.METACOMMON_NOSTANDARD_NAME, CommonConstant.SYS_BLANK, false, true, true, false,
				false, false, CommonConstant.METACOMMON_NOSTANDARD, ValueType.BooleanV, "非标", CommonConstant.METACOMMON_UNREGULAR));// N2
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_PRICE, CommonConstant.METACOMMON_PRICE_NAME, CommonConstant.SYS_BLANK, false, true, true, false, false, false,
				CommonConstant.METACOMMON_PRICE, ValueType.DoubleV, "价格", CommonConstant.METACOMMON_PRICE_NAME));// N2
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_MEMO, CommonConstant.METACOMMON_MEMO_NAME, CommonConstant.SYS_BLANK, false, true, true, false, false, false,
				CommonConstant.METACOMMON_MEMO, ValueType.StringV, CommonConstant.METACOMMON_MEMO_NAME, CommonConstant.METACOMMON_MEMO_NAME));// N2
		meta.appendParameter(new MetaParameter(prefix + CommonConstant.METACOMMON_POSTFIX_SEASON, CommonConstant.METACOMMON_SEASON_NAME, CommonConstant.SYS_BLANK, false, true, true, false, false, false,
				CommonConstant.METACOMMON_SEASON, ValueType.StringV, "季节", CommonConstant.METACOMMON_SEASON_NAME));// N2
		meta.appendParameter(new MetaParameter(SystemCalculateConstants.META_SECTION_COMPLETED, CommonConstant.METACOMMON_COMPLETE_NAME, CommonConstant.SYS_BLANK, false,
				true, false, false, false, false, SystemCalculateConstants.META_SECTION_COMPLETED, ValueType.BooleanV,
				"完成", CommonConstant.SYS_MAP_COMPLETED_1));// N2
		meta.appendParameter(new MetaParameter(CommonConstant.METAHU_ASSIS_POSITION, CommonConstant.SYS_STRING_POSITION, CommonConstant.METAHU_ASSIS, false, false, false, false,
				false, false, CommonConstant.SYS_BLANK, ValueType.IntV, "位置", CommonConstant.SYS_STRING_POSITION));// N2
		meta.appendParameter(new MetaParameter(CommonConstant.METAHU_ASSIS_HEIGHT, CommonConstant.METAHU_AHUHEIGHT, CommonConstant.METAHU_ASSIS, false, false, false,
				false, false, false, CommonConstant.SYS_BLANK, ValueType.IntV, "机组高度", CommonConstant.METAHU_HEIGHT_NAME));// N2
		meta.appendParameter(new MetaParameter(CommonConstant.METAHU_ASSIS_FACESPEED, CommonConstant.METAHU_FACESPEED_NAME, CommonConstant.METAHU_ASSIS, false, false, false,
				false, false, false, CommonConstant.SYS_BLANK, ValueType.DoubleV, "面风速", CommonConstant.METAHU_FACE_SPEED));// N2
		// 非标的默认开关，打开后，非标的值会生效
		String nsPrefix = prefix.replace(CommonConstant.METACOMMON_META, CommonConstant.METACOMMON_NS);
		meta.appendParameter(new MetaParameter(nsPrefix + CommonConstant.METACOMMON_POSTFIX_ENABLE, CommonConstant.METANS_ENABLE_UNREGULAR, CommonConstant.SYS_BLANK, false, false, false, true,
				false, false, CommonConstant.SYS_BLANK, ValueType.BooleanV, "允许非标", CommonConstant.METANS_ENABLE_UNREGULAR));
		meta.appendParameter(new MetaParameter(nsPrefix + CommonConstant.METACOMMON_POSTFIX_MEMO, CommonConstant.METANS_NS_MEMO, CommonConstant.SYS_BLANK, false, false, false, true, false,
				false, CommonConstant.METANS_MEMO, ValueType.StringV, CommonConstant.METACOMMON_MEMO_NAME, CommonConstant.METACOMMON_MEMO_NAME));// N2
		meta.appendParameter(new MetaParameter(nsPrefix + CommonConstant.METACOMMON_POSTFIX_PRICE, "L-P", CommonConstant.SYS_BLANK, false, false, false, true, false, false,
				CommonConstant.METACOMMON_PRICE, ValueType.DoubleV, "价格", CommonConstant.METACOMMON_PRICE_NAME));// N2
		// Air Direction
		meta.appendParameter(new MetaParameter(CommonConstant.METASEXON_AIRDIRECTION, CommonConstant.METACOMMON_AIR_DIRECTION, CommonConstant.SYS_BLANK, false, false, false,
				false, false, false, CommonConstant.METASEXON_AIRDIRECTION, ValueType.StringV, "风向", CommonConstant.METACOMMON_AIRDIRECTION_NAME));// N2

		// Append default value
		// comment it since it's been deprecated
		// TemplateUtil.appendDefaultValue(meta.getMetaId(), (CommonConstant.METASEXON_AIRDIRECTION),
		// 		TemplateUtil.getDefaultAirDirection());
	}

	/*
	 * 这些属性不是业务属性的一部分，主要用于参与页面展现规则的计算
	 *
	 */
	public static List<String> getAssistantParameter() {
		List<String> result = new ArrayList<>();
		result.add(CommonConstant.METAHU_ASSIS_POSITION);
		result.add(CommonConstant.METAHU_ASSIS_HEIGHT);
		result.add(CommonConstant.METAHU_ASSIS_FACESPEED);
		return result;
	}

	// Start Code For Section KEY, available for CAD output.

	public static final Map<String, Short> KEY_CODE_MAP = new HashMap<>();
	public static final String KEY_UNKNOWN = CommonConstant.SYS_MAP_UNKNOWN;

	static {
		// Phase 1
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_MIX.getId(), (short) 1);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_SINGLE.getId(), (short) 2);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_COMPOSITE.getId(), (short) 3);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_COLD.getId(), (short) 4);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_FAN.getId(), (short) 11);
		// Phase 2
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_HEATINGCOIL.getId(), (short) 5);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_STEAMCOIL.getId(), (short) 6);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId(), (short) 7);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId(), (short) 9);
		// TODO TYPE_SPRAYHUMIDIFIER and TYPE_STEAMHUMIDIFIER need to confirm
		// KEY_CODE_MAP.put(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId(),
		// (short) 18);
		// KEY_CODE_MAP.put(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId(),
		// (short) 8);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId(), (short) 10);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId(), (short) 8);

		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_ACCESS.getId(), (short) 15);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId(), (short) 12);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_ATTENUATOR.getId(), (short) 13);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_DISCHARGE.getId(), (short) 14);
		// Phase3
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_HEPAFILTER.getId(), (short) 22);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId(), (short) 25);
		// TEMP PHASE
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId(), (short) 23);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId(), (short) 43);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_CTR.getId(), (short) 26);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId(), (short) 24);
		KEY_CODE_MAP.put(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId(), (short) 18);

		KEY_CODE_MAP.put(KEY_UNKNOWN, (short) 999);

	}

	public static List<String> translateGroupCodeToIds(String groupCode) {
		List<String> result = new ArrayList<String>();
		if (EmptyUtil.isEmpty(groupCode)) {
			logger.warn("groupCode is null when translateGroupCodeToIds()");
			return result;
		}
		if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
			return result;
		}
		int k = groupCode.length() / 3;
		for (int i = 0; i < k; i++) {
			String key = groupCode.substring(i * 3, (i + 1) * 3);
			Short value = Short.parseShort(key);
			for (Entry<String, Short> it : MetaCodeGen.KEY_CODE_MAP.entrySet()) {
				if (it.getValue().shortValue() == value.shortValue()) {
					result.add(it.getKey());
					break;
				}
			}
		}
		return result;
	}

	public static Short getSectionType(String key) {
		if (KEY_CODE_MAP.containsKey(key)) {
			return KEY_CODE_MAP.get(key);
		} else {
			return KEY_CODE_MAP.get(KEY_UNKNOWN);
		}
	}

	/**
	 * Based on the section type, generate a 3-digit code
	 *
	 * @param key
	 * @return
	 */
	public static String getSectionTypeCode(String key) {
		Short c = getSectionType(key);
		String s = "000" + String.valueOf(c);
		return s.substring(s.length() - 3);
	}

	// Complete Code For Section KEY, available for CAD output.
	// Start Code For AHU Group KEY, available for group editing.
	/**
	 * This code is designed for mark a group of ahu, which contains the same
	 * sections in an identical sequence.
	 *
	 * @param sectionList
	 * @return
	 */
	public static final Map<String, String> AHU_SEQUENCE_KEY_GROUP_MAP = new HashMap<>();
	public static final String AHU_SEQUENCE_UNKNOWN = CommonConstant.SYS_MAP_OTHERS;

	static {
		AHU_SEQUENCE_KEY_GROUP_MAP.put("001002004011", "二进制舒适型");
		AHU_SEQUENCE_KEY_GROUP_MAP.put(AHU_SEQUENCE_UNKNOWN, CommonConstant.SYS_MAP_OTHERS);
	}

	public static String getAhuGroupCode(LinkedList<String> sectionKeyList) {
		if (sectionKeyList != null && sectionKeyList.size() > 0) {
			StringBuilder build = new StringBuilder();
			Iterator<String> it = sectionKeyList.iterator();
			while (it.hasNext()) {
				String key = it.next();
				build.append(getSectionTypeCode(key));
			}
			return build.toString();
		} else {
			return CommonConstant.SYS_BLANK;
		}
	}

	/**
	 * 内部使用，用于统一段属性值的代码生成
	 * 
	 * @param optionKey
	 * @param optionValue
	 * @return
	 */
	public static String getFieldDefStr(String optionKey, String optionValue) {
		String template = "String SECTION_UVALUE = \"VALUE\";";
		if (optionKey.startsWith(CommonConstant.METASEXON_PREFIX)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_PREFIX.length());
		} else if (optionKey.startsWith(CommonConstant.METAHU_PREFIX)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_META_PREFIX.length());
		} else if (optionKey.startsWith(CommonConstant.METACOMMON_PREFIX_DEFAULTPARA)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_META_PREFIX.length());
		} else {
			System.out.println();
		}
		optionKey = optionKey.toUpperCase().replace(CommonConstant.SYS_PUNCTUATION_DOT, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN);
		String noptionValue = tranformFieldValue(optionValue);
		String outputStr = template.replace("SECTION", optionKey).replace("UVALUE", noptionValue.toUpperCase())
				.replace("VALUE", optionValue);
		return outputStr;
	}

	/**
	 * 内部使用，用于生成属性值的结构映射所需要的fieldName
	 * 
	 * @param optionKey
	 * @param optionValue
	 * @return
	 */
	public static String getFieldName(String optionKey, String optionValue) {
		String template = "SECTION_UVALUE";
		if (optionKey.startsWith(CommonConstant.METASEXON_PREFIX)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_PREFIX.length());
		} else if (optionKey.startsWith(CommonConstant.METAHU_PREFIX)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_META_PREFIX.length());
		} else if (optionKey.startsWith(CommonConstant.METACOMMON_PREFIX_DEFAULTPARA)) {
			optionKey = optionKey.substring(CommonConstant.METASEXON_META_PREFIX.length());
		}
		optionKey = optionKey.toUpperCase().replace(CommonConstant.SYS_PUNCTUATION_DOT, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN);
		optionValue = tranformFieldValue(optionValue);
		String outputStr = template.replace("SECTION", optionKey).replace("UVALUE", optionValue.toUpperCase());
		return outputStr;
	}

	/**
	 * 内部使用，转换value为合法的属性名
	 * 
	 * @param value
	 * @return
	 */
	public static String tranformFieldValue(String value) {

		if (value.equals("-1")) {
			value = "auto";
		}
		value = value.replace(CommonConstant.SYS_PUNCTUATION_DOT, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN).replace(CommonConstant.SYS_PUNCTUATION_SLASH, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN).replace(" ", CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN).replace("″", CommonConstant.SYS_BLANK).replace(CommonConstant.SYS_PUNCTUATION_HYPHEN, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN)
				.replace(CommonConstant.SYS_PUNCTUATION_PLUS, CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN);
		return value;
	}

}