package com.carrier.ahu.common.enums;

/**
 * Domestic and Export version.
 * 
 * Created by Braden Zhou on 2018/08/14.
 */
public enum VersionTypeEnum {

    DOMESTIC, EXPORT;

}
