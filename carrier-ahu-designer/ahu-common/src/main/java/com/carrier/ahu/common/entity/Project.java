package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.po.AbstractPo;
import com.carrier.ahu.util.EmptyUtil;

import java.io.Serializable;

/**
 * 项目实体
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class Project extends AbstractPo implements Serializable {
    private static final long serialVersionUID = -4225080318716384441L;
    @Id
    String pid;// 主键ID
    String no;// 项目编号
    String name;// 项目名称
    String address;// 项目地址
    String contract;// 合同号
    String saler;// 销售员
    String enquiryNo;// 非标询价单编号
    String projectDate;
    String priceBase;// 价格基准：
    String customerName;// 客户名称
    String factoryId;// 工厂编号

    public String getPriceBase() {
        if (EmptyUtil.isEmpty(priceBase)) { // default price base
            return CommonConstant.SYS_PRICE_VERSION_20190101;
        }
        return this.priceBase;
    }

}