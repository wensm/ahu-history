package com.carrier.ahu.calculator.price;

import lombok.Data;

@Data
public class PriceCodeXSLXPO {
	private String sectionKey;
	private String no;
	private String name;
	private String description;
	private String valueType;
	private String value;
	private String valueDesp;
	private String code;
	private String group;
	private String metaKey;
	private String metaValue;

	public String getPoKey() {
		return metaKey + "_" + metaValue;
	}

	public String getNoKey() {
		return sectionKey + "." + no;
	}

	@Override
	public String toString() {
		return "PriceCodeXSLXPO [sectionKey=" + sectionKey + ", no=" + no + ", name=" + name + ", description="
				+ description + ", valueType=" + valueType + ", value=" + value + ", valueDesp=" + valueDesp + ", code="
				+ code + ", group=" + group + ", metaKey=" + metaKey + ", metaValue=" + metaValue + "]";
	}

}
