package com.carrier.ahu.common.util;

import java.net.Socket;

import com.carrier.ahu.constant.CommonConstant;

/**
 * Created by Braden Zhou on 2018/11/21.
 */
public class NetworkUtils {

    public static boolean isPortInUse(int port) {
        return isPortInUse(CommonConstant.SYS_DEFAULT_HOST, port);
    }

    public static boolean isPortInUse(String host, int port) {
        Socket socket;
        try {
            socket = new Socket(host, port);
            socket.close();
            return true;
        } catch (Exception e) {
            System.out.println("Exception occured" + e);
        }
        return false;
    }

}
