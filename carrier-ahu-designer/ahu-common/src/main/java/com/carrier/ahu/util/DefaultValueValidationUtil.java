package com.carrier.ahu.util;

import com.carrier.ahu.common.exception.calculation.HeatQCalcException;
import com.carrier.ahu.common.exception.calculation.HumQCalcException;

import static com.carrier.ahu.common.intl.I18NConstants.HEATING_CAPACITY_GREATER_THAN_ZERO;
import static com.carrier.ahu.common.intl.I18NConstants.HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO;

public class DefaultValueValidationUtil {
    public static void zeroHumQ(double value) {
        if (value <= 0) {
            throw new HumQCalcException(HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO);
        }
    }

    public static void zeroHeatQ(double value) {
        if (value <= 0) {
            throw new HeatQCalcException(HEATING_CAPACITY_GREATER_THAN_ZERO);
        }
    }
}
