package com.carrier.ahu.length.service;

/**
 * Created by LIANGD4 on 2017/9/18.
 */
public interface LengthService {

    //TODO 参数定义 段类型不同计算参数设计
    //sectionType 段
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    double getLength(String sectionType, String version);

}
