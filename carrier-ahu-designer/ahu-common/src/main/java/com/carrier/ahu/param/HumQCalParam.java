package com.carrier.ahu.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/14.
 */
@Data
public class HumQCalParam {
    /*机组参数*/
    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向
    /*进风温度*/
    private double InDryBulbT;//干球温度
    private double InWetBulbT;//湿球温度
    /*条件*/
    private double OutDryBulbT;//目标干球温度
    private double HumidificationQ;//目标加湿量
    private String supplier;//供应商
    
    private boolean EnableWinter;
    private boolean EnableSummer;
}
