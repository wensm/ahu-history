package com.carrier.ahu.engine.cad;

import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.util.EmptyUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by liaoyw on 2017/4/16.
 */
class IniUtils {
	// parts
	static Map<Integer, Map<String, Object>> partsConfig = new HashMap<>(); // parts
																			// configure
	static {
		partsConfig.put(1, configMapPartType1());
		partsConfig.put(2, configMapPartType2());
		partsConfig.put(3, configMapPartType3());
		partsConfig.put(4, configMapPartType4());
		partsConfig.put(5, configMapPartType5());
		partsConfig.put(6, configMapPartType6());
		partsConfig.put(7, configMapPartType7());
		partsConfig.put(8, configMapPartType8());
		partsConfig.put(9, configMapPartType9());
		partsConfig.put(10, configMapPartType10());
		partsConfig.put(11, configMapPartType11());
		partsConfig.put(12, configMapPartType12());
		partsConfig.put(13, configMapPartType13());
		partsConfig.put(14, configMapPartType14());
		partsConfig.put(15, configMapPartType15());
		partsConfig.put(16, configMapPartType16());
		partsConfig.put(22, configMapPartType22());
		partsConfig.put(23, configMapPartType23());
		partsConfig.put(24, configMapPartType24());
		partsConfig.put(25, configMapPartType25());
		partsConfig.put(26, configMapPartType26());
		
	}

	static void create(AhuParam params, String destPath) throws IOException {
		try (PrintWriter writer = new PrintWriter(new File(destPath), "gbk")) {
			writeUnitInfo(params, writer);
			writeFrames(params, writer);
			writeParts(params, writer);
			writeMxb(params, writer);
		}
	}

	private static void writeMxb(AhuParam params, PrintWriter writer) {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("COLUMNNAME", "名称+参数");
		configMap.put("ROW2", "电压:+380V-3Ph-50Hz");
		configMap.put("ROW3", "盘管段");
		configMap.put("ROW4", "盘管型号:+4R/10/FL");
		configMap.put("ROW5", "冷量:+113.2kW");
		configMap.put("ROW6", "水流量:+5.39L/s");
		configMap.put("ROW7", "水阻力:+15kPa");
		configMap.put("ROW8", "风机段");
		configMap.put("ROW9", "风量:+20000m3/h");
		configMap.put("ROW10", "全静压:+1807Pa");
		configMap.put("ROW11", "电机功率:+15kW");
		configMap.put("ROW12", "风机型号:+BC560K");
		configMap.put("ROW13", "盘管段");
		configMap.put("ROW14", "盘管型号:+4R/10/FL/4R/10/FL");
		configMap.put("ROW15", "冷量:+113.2/103.4kW");
		configMap.put("ROW16", "水流量:+5.39/4.94L/s");
		configMap.put("ROW17", "水阻力:+15/13kPa");
		configMap.put("RULE", "1");
		configMap.put("LENGTH", "60");
		configMap.put("ALLROW",
				"ROW2+ROW3+ROW4+ROW5+ROW6+ROW7+ROW8+ROW9+ROW10+ROW11+ROW12+ROW13+ROW14+ROW15+ROW16+ROW17");
		writeSection("MXB", configMap, writer);
	}

	private static final Map<String, Object> configMapPartType16() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "16");
		configMap.put("BIGTYPE", "0");
		configMap.put("DUALHEADER", "0");
		configMap.put("DIMEN", "1");
		configMap.put("BREAKWATERTYPE", "D");
		configMap.put("SECTIONL", "6");
		configMap.put("DSQPATH", "dsq");
		configMap.put("DSQTHICKNESS", "50");
		configMap.put("BWBHEIGHT", "10");
		configMap.put("DRAINPANHEIGHT", "50");
		configMap.put("DRAINPANWIDTH", "630");
		configMap.put("HEADERLENGTH", "100");
		configMap.put("DRAINPANPATH", "shuipan");
		configMap.put("FB", "158");
		configMap.put("FC", "1142");
		configMap.put("FD", "84");
		configMap.put("FE", "180.5");
		configMap.put("FG", "0");
		configMap.put("COLITHICKNESS", "200");
		configMap.put("COILHEIGHT", "1247");
		configMap.put("COILWIDTH", "2030");
		configMap.put("SDIA", "89");
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("WATERBREAKHEIGHT", "50");
		configMap.put("COILDIRECTION", "R");
		configMap.put("FILEPATH_TOP", "DSQ.DXF");
		configMap.put("FILEPATH_FRONT", "DSQ.DXF");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "CWCOIL");
		configMap.put("DIMENSION_SM_THICKNESS", "100");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType8() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "8");
		configMap.put("SECTIONL", "6");
		configMap.put("DIMENSIONH", "412");
		configMap.put("DIMENSIONL", "140");
		configMap.put("DIMENSIONW", "307");
		configMap.put("DGHEIGHT", "50");
		configMap.put("DIRECTION", "R");
		configMap.put("FILEPATH_FRONT", "GZJS.DXF");
		configMap.put("FILEPATH_TOP", "GZJS_TOP.DXF");
		configMap.put("PARTDETAIL", "STHUMID");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType9() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("DIMENSION_SM_THICKNESS", "100");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("BREAKWATERTYPE", "D");
		configMap.put("DSQPATH", "dsq");
		configMap.put("BWBHEIGHT", "10");
		configMap.put("DRAINPANHEIGHT", "50");
		configMap.put("DRAINPANWIDTH", "630");
		configMap.put("DRAINPANPATH", "shuipan");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType10() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("DIMENSION_GYPW_L", "420");
		configMap.put("DIMENSION_GYPW_H", "612.5");
		configMap.put("DSQ_STARTPIONT", "963.6");
		return configMap;
	}

	@SuppressWarnings("unused")
	private static final Map<String, Object> configMapPartTypeX() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		return configMap;
	}

	private static final Map<String, Object> configMapPartType14() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "14");
		configMap.put("AirFlowO", "2");
		configMap.put("MIXTYPE", "A");
		configMap.put("DAMPERTYPE", "A");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("SECTIONL", "8");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOOR", "1");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "628*1428");
		configMap.put("DIMENSION_A", "638");
		configMap.put("DIMENSION_B", "125");
		configMap.put("DIMENSION_C", "1756");
		configMap.put("DIMENSION_D", "67");
		configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
		configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		configMap.put("DIMENSION_E", "130");
		configMap.put("FLAN_OUT", "20");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("PARTDETAIL", "OUTLET");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType22() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "22");
		configMap.put("FILTERTYPE", "1");
		configMap.put("SECTIONL", "9");
		configMap.put("PRESSUREGAGE", "0");
		configMap.put("FILTERTHICKNESSX", "550");
		configMap.put("FLTFRONT_DRAWING", "VFLT_F+VFLT_F");
		configMap.put("FLTFRONT_DIMENSION", "610+610");
		configMap.put("FLTTOP_DRAWING", "VFLT_6D_T+VFLT_6D_T+VFLT_4D_T");
		configMap.put("FLTTOP_DIMENSION", "610+610+406");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("STARTPOINT", "300");
		configMap.put("PARTDETAIL", "HEFILTER");
		configMap.put("DOOR", "0");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "428*2428");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType13() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "13");
		configMap.put("SECTIONL", "6");
		configMap.put("XYQWIDTH", "1830");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "ATTEN");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType4() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "4");
		configMap.put("BIGTYPE", "0");
		configMap.put("DUALHEADER", "0");
		configMap.put("DIMEN", "1");
		configMap.put("BREAKWATERTYPE", "D");
		configMap.put("SECTIONL", "5");
		configMap.put("DSQPATH", "dsq");
		configMap.put("DSQTHICKNESS", "50");
		configMap.put("BWBHEIGHT", "10");
		configMap.put("DRAINPANHEIGHT", "50");
		configMap.put("DRAINPANWIDTH", "530");
		configMap.put("HEADERLENGTH", "100");
		configMap.put("DRAINPANPATH", "shuipan");
		configMap.put("TUOJIA1_L", "471");
		configMap.put("TUOJIA1_H", "0");
		configMap.put("TUOJIA2_L", "0");
		configMap.put("TUOJIA2_H", "0");
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("TUOJIA1DRAWING", "tuojia1");
		configMap.put("TUOJIA2DRAWING", "tuojia2");
		configMap.put("FB", "158");
		configMap.put("FC", "1142");
		configMap.put("FD", "84");
		configMap.put("FE", "180.5");
		configMap.put("FG", "0");
		configMap.put("COLITHICKNESS", "200");
		configMap.put("COILHEIGHT", "1247");
		configMap.put("COILWIDTH", "2030");
		configMap.put("SDIA", "89");
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("WATERBREAKHEIGHT", "50");
		configMap.put("COILDIRECTION", "R");
		configMap.put("FILEPATH_TOP", "DSQ.DXF");
		configMap.put("FILEPATH_FRONT", "DSQ.DXF");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "CWCOIL");
		//configMap.put("DIMENSION_GYPW_L", "420");
		//configMap.put("DIMENSION_GYPW_H", "612.5");
		//configMap.put("DSQ_STARTPIONT", "963.6");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType11() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "11");
		configMap.put("SECTIONL", "13");
		configMap.put("DOORDIRECTION", "2");
		configMap.put("MOTORDIRECTION", "2");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOORDIMENSION", "628*1428");
		configMap.put("DOOR", "1");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("OUTLET", "THF_R");
		configMap.put("TUYUANPATHFORV", "THF&BHF_Motor_Right_R");
		configMap.put("DIMENSION_FANA", "863.5");
		configMap.put("DIMENSION_FAND", "715");
		configMap.put("DIMENSION_FANE", "549");
		configMap.put("DIMENSION_FALANGETHICK", "50");
		configMap.put("DIMENSION_FALANGEEDGE", "50");
		configMap.put("ABSORBERHEIGHT", "40");
		configMap.put("ABSORBERNUM", "2");
		configMap.put("DUALDOOR", "0");
		configMap.put("DUALMOTOR", "0");
		configMap.put("OUTLETDISTANCE", "30");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("AIRFLOW", "0");
		configMap.put("AIRDIRECTION", "1");
		configMap.put("PARTDETAIL", "FAN");
		configMap.put("ABSORBDRAWING", "HD");
		configMap.put("FANBASEDRWING", "FANBASE");
		configMap.put("FANBASEDIMENSION", "80");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType5() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "5");
		configMap.put("BIGTYPE", "0");
		configMap.put("DUALHEADER", "0");
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("SECTIONL", "3");
		configMap.put("DIMEN", "1");
		configMap.put("TUOJIA1_L", "245");
		configMap.put("TUOJIA1_H", "1595");
		configMap.put("TUOJIA1DRAWING", "HCTJ1");
		configMap.put("DGORRRAINPAN", "0");
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");
		configMap.put("FB", "137");
		configMap.put("FC", "1183");
		configMap.put("FD", "55");
		configMap.put("FE", "138");
		configMap.put("FG", "0");
		configMap.put("COLITHICKNESS", "129");
		configMap.put("COILWIDTH", "1830");
		configMap.put("COILHEIGHT", "1247");
		configMap.put("SDIA", "48");
		configMap.put("COILDIRECTION", "L");
		configMap.put("HEADERLENGTH", "100");
		configMap.put("AIRFLOW", "0");
		configMap.put("PARTDETAIL", "HWCOIL");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType15() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "15");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("SECTIONL", "6");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOOR", "1");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "568*1428");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("PARTDETAIL", "ACCESS");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType7() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "7");
		configMap.put("SECTIONL", "3");
		configMap.put("ROWNUM", "1");
		configMap.put("COILHEIGHT", "1190");
		configMap.put("COLITHICKNESS", "196");
		configMap.put("COILWIDTH", "1500");
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");
		configMap.put("FILEPATH_TOP", "HEATER_TOP");
		configMap.put("FILEPATH_FRONT", "HEATER_FRONT");
		configMap.put("COILDIRECTION", "L");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "ELCOIL");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType6() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "6");
		configMap.put("SECTIONL", "3");
		configMap.put("BIGTYPE", "0");
		configMap.put("DUALHEADER", "0");
		configMap.put("DIMEN", "1");
		configMap.put("BREAKWATERTYPE", "A");
		configMap.put("TUOJIA1_L", "290");
		configMap.put("TUOJIA1_H", "1682");
		configMap.put("TUOJIA1DRAWING", "ZQTJ");
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");
		configMap.put("FB", "73");
		configMap.put("FD", "80");
		configMap.put("FE", "150");
		configMap.put("FC", "631");
		configMap.put("FG", "0");
		configMap.put("SDIA", "60");
		configMap.put("COILDIRECTION", "L");
		configMap.put("COILHEIGHT", "721");
		configMap.put("COLITHICKNESS", "280");
		configMap.put("COILWIDTH", "1430");
		configMap.put("HEADERLENGTH", "100");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "STCOIL");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType3() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "3");
		configMap.put("SECTIONL", "8");
		configMap.put("FILTERTHICKNESSX_P", "50");
		configMap.put("FILTERTHICKNESSX", "750");
		configMap.put("FLTFRONT_DIMENSION", "610+610");
		configMap.put("FLTFRONT_DRAWING", "BFLT_F+BFLT_F");
		configMap.put("FLTTOP_DRAWING", "BFLT_6D_T+BFLT_6D_T+BFLT_6D_T");
		configMap.put("FLTTOP_DIMENSION", "610+610+610");
		configMap.put("FLTFRONT_DIMENSION_P", "610+610");
		configMap.put("FLTFRONT_DRAWING_P", "PFLT_F+PFLT_F");
		configMap.put("FLTTOP_DRAWING_P", "PFLT_F+PFLT_F+PFLT_F");
		configMap.put("FLTTOP_DIMENSION_P", "610+610+610");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "PBFILTER");
		configMap.put("DOOR", "1");
		configMap.put("DOORDIRECTION", "2");
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "528*1418");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39G_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39G_Door_front");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType1() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "1");
		configMap.put("MIXTYPE", "11000");
		configMap.put("DAMPERTYPE", "A");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("SECTIONL", "10");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOOR", "1");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "628*1028");
		configMap.put("DIMENSION_A", "795");
		configMap.put("DIMENSION_B", "146.5");
		configMap.put("DIMENSION_C", "1456");
		configMap.put("DIMENSION_D", "67");
		configMap.put("DIMENSION_A_2", "48");
		configMap.put("DIMENSION_B_2", "104");
		configMap.put("DIMENSION_C_2", "1456");
		configMap.put("DIMENSION_D_2", "67");
		configMap.put("DIMENSION_E", "160");
		configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
		configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("AIRHOOD", "0");
		configMap.put("AIRHOODDRAWING_TOP", "AIRHOOD_TOP.DWG");
		configMap.put("AIRHOODDRAWING_FRONT", "AIRHOOD_FRONT.DWG");
		configMap.put("AIRHOOD_H", "371");
		configMap.put("PARTDETAIL", "MIX");
		return configMap;
	}

	private static final Map<String, Object> configMapPartType2() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "2");
		configMap.put("DRAWOPTION", "3");
		configMap.put("FLTERTYPE", "3");
		configMap.put("SECTIONL", "0");
		configMap.put("PRESSUREGAGE", "0");
		configMap.put("FILTERTHICKNESSX", "98");
		configMap.put("FLTFRONT_DIMENSION", "114+600+600+114");
		configMap.put("FLTFRONT_DRAWING", "WCGTJ_T+WCG11+WCG11+WCGTJ_B");
		configMap.put("FLTTOP_DRAWING", "WCGTJ_T+WCG11+WCG11+WCG11+WCGTJ_B");
		configMap.put("FLTTOP_DIMENSION", "114+500+500+600+114");
		configMap.put("DIMESION_FT", "98");
		configMap.put("STARTPOINT", "1");
		configMap.put("FILEPATH_TOP", "D:\\WCG11.DXF");
		configMap.put("FILEPATH_FRONT", "D:\\WCG11.DXF");
		configMap.put("FILEPATH_SIDE", "D:\\WCG12.DXF");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("PARTDETAIL", "");
		configMap.put("DOOR", "0");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		configMap.put("DOORDIMENSION", "428*1418");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39G_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39G_Door_front");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType12() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "12");
		configMap.put("DAMPERTYPE", "A");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("SECTIONL", "12");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOORDIMENSION", "628*928");
		configMap.put("OSDOOR", "1");
		configMap.put("OSLAMP", "0");
		configMap.put("OSVIEWPORT", "0");
		configMap.put("ISDOOR", "1");
		configMap.put("ISLAMP", "0");
		configMap.put("ISVIEWPORT", "0");
		configMap.put("DIMENSION_A", "322.5");
		configMap.put("DIMENSION_B", "162.75");
		configMap.put("DIMENSION_C", "1384");
		configMap.put("DIMENSION_D", "67");
		configMap.put("DIMENSION_B1", "275.5");
		configMap.put("DIMENSION_A_IN", "468");
		configMap.put("DIMENSION_B_IN", "87");
		configMap.put("DIMENSION_C_IN", "1078");
		configMap.put("DIMENSION_D_IN", "187");
		configMap.put("DIMENSION_E", "130");
		configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
		configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		configMap.put("AIRFLOW", "0");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("PARTDETAIL", "RET & MIX");
		return configMap;
	}
    
		
	private static final Map<String, Object> configMapPartType25() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "25");
		configMap.put("SECTIONL", "6");
		configMap.put("FILTERTHICKNESSX", "550");
		configMap.put("FLTFRONT_DRAWING", "JDFLT_F+JDFLT_F");
		configMap.put("FLTFRONT_DIMENSION", "610+610");
		configMap.put("FLTTOP_DRAWING", "JDFLT_T+JDFLT_T+JDFLT_T");
		configMap.put("FLTTOP_DIMENSION", "610+610+406");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("STARTPOINT", "0");
		configMap.put("PARTDETAIL", "Electrostatic Filter");
		configMap.put("DOOR", "0");
		configMap.put("DOORDIMENSION", "428*1328");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType24() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "24");
		configMap.put("SECTIONL", "6");
		configMap.put("PARTDETAIL", "ELEC HUMID");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType26() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "26");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("SECTIONL", "8");
		configMap.put("DOORTYPE", "1");
		configMap.put("DOOR", "1");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "1");
		configMap.put("DOORDIMENSION", "728*1428");
		configMap.put("AIRDIRECTION", "0");
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		configMap.put("PARTDETAIL", "Control");
		return configMap;
	}
	
	private static final Map<String, Object> configMapPartType23() {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("TYPE", "23");
		configMap.put("SECTIONL", "6");
		configMap.put("HEATHEIGHT", "2920");
		configMap.put("HEATTHICKNESS", "200");
		configMap.put("FRAMEOPTION", "0");
		configMap.put("HEATWIDTH", "1830");
		configMap.put("WHEELTYPE", "2");
		configMap.put("DRAWING_TOP", "HeatWheel_Top");
		configMap.put("DRAWING_FRONT", "HeatWheel_Front");
		configMap.put("AIRDIRECTION", "0");
		return configMap;
	}
	
	private static void writeParts(AhuParam params, PrintWriter writer) {
		int i = 1;
		for (PartParam pp : params.getPartParams()) {
			Map<String, Object> configMap = partsConfig.get(Integer.valueOf(pp.getTypes()));
			if (EmptyUtil.isEmpty(configMap)) {
				configMap = new LinkedHashMap<>(1);
				configMap.put("TYPE", pp.getTypes());
			}
			writeSection("PART" + i, configMap, writer);
			i++;
		}
	}

	private static void writeFrames(AhuParam params, PrintWriter writer) {
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put("FRAMENAME", "");
		writeSection("BOTTOMFRAME", configMap, writer);

		configMap.clear();
		configMap.put("FRAMENAME", IntStream.range(1, params.getPartParams().size() + 1).mapToObj(i -> "FRAME" + i)
				.collect(Collectors.joining("+")));
		writeSection("TOPFRAME", configMap, writer);

		for (int i = 1; i <= params.getPartParams().size(); i++) {
			configMap.clear();
			configMap.put("PARTSNAME", "PART" + i);
			writeSection("FRAME" + i, configMap, writer);
		}
	}

	private static void writeUnitInfo(AhuParam params, PrintWriter writer) {
		Map<String, Object> configMap = new LinkedHashMap<String, Object>();
		configMap.put("PRODUCT", params.getProduct());
		configMap.put("DIMSCALE", 1);
		configMap.put("ENGLISHTEMPLATE", 0);
		configMap.put("BOTTOMFRAMALIGNMODE", 0);
		configMap.put("UNITHEIGHT", 1400);
		configMap.put("UNITHEIGHT_TOP", "1400");
		configMap.put("UNITWIDTH", "2000");
		configMap.put("UNITCONFIG", "H");
		configMap.put("TOPFRAMEALIGNMODE", "R");
		configMap.put("PIPEORIENTATION", "R");
		configMap.put("DOORDIRECTION", "2");
		configMap.put("RAINTOP", "1");
		configMap.put("GROUPS", params.getPartParams().size());
		configMap.put("SECTIONCOUNT", params.getPartParams().size());
		configMap.put("BASEHEIGHT", "100");
		configMap.put("MODULE", "100");
		configMap.put("FRMTHICK", "30");
		configMap.put("IDMDH4", "130");
		configMap.put("INTERVAL", "90");
		configMap.put("PANELTHICK", "50");
		configMap.put("VOLTAGE", "380V-3Ph-50Hz");
		configMap.put("ID", "001");
		configMap.put("AHUMODEL", params.getProduct());
		configMap.put("AHUMARKING", params.getUnitNo());
		configMap.put("PROJECTNAME", "20170227");
		writeSection("UNITINFO", configMap, writer);
	}

	private static void writeSection(String sectionName, Map<String, Object> configMap, PrintWriter writer) {
		writer.printf("[%s]\n", sectionName);
		for (Map.Entry<String, Object> entry : configMap.entrySet()) {
			writer.printf("%s=%s\n", entry.getKey(), entry.getValue() == null ? "" : entry.getValue());
		}
	}

	public static void main(String[] args) {
		System.out.println("1".compareTo("2"));
	}
}
