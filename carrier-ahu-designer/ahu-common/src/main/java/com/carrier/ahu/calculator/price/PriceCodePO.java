package com.carrier.ahu.calculator.price;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.common.entity.Part;

import lombok.Data;

@Data
public class PriceCodePO {

    private String sectionCode;
    private Short sectionNo; // 相同功能段编号
    private Short partitionNo;
    private String priceCode;
    private Short sectionList; // 功能段序号
    private Part part;
    private List<PriceCodeXSLXPO> codeRules = new ArrayList<>();

    public PriceCodePO() {
    }

    public PriceCodePO(String sectionCode, Short sectionNo, String priceCode, Short sectionList) {
        this.sectionCode = sectionCode;
        this.sectionNo = sectionNo;
        this.priceCode = priceCode;
        this.sectionList = sectionList;
    }

    public String getFuncName() {
        return String.format("AHU39_FUNC_%s_%d", sectionCode, sectionNo);
    }

    public String getFuncNameWithoutNo() {
        return String.format("AHU39_FUNC_%s", sectionCode);
    }

    public void addCodeRule(PriceCodeXSLXPO codeRule) {
        codeRules.add(codeRule);
    }

}
