package com.carrier.ahu.section.meta.devhelp;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

/**
 * Helper class to do codegen
 * @author liujianfeng
 *
 */
@Data
public class DevHelperInCommon {

	private static DevHelperInCommon instance;
	private Map<String, String> ckMap = new HashMap<String, String>();
	private Map<String, String> kcMap = new HashMap<String, String>();
	private Map<String, String> keMap = new HashMap<String, String>();
	
	private String prefix = "moon_intl_str_";
	
	private final static int startIndex = 1200;
	private int cindex = 0;
	
	public void recordIntlString(String cnStr) {
		if (ckMap.containsKey(cnStr)) {
			return;
		}
		if (cindex == 0) {
			cindex = startIndex;
		}
		String key = prefix + cindex;
		ckMap.put(cnStr, key);
		kcMap.put(key, cnStr);
		keMap.put(key, cnStr);
		
		cindex++;
		
		
	}
	
	public static DevHelperInCommon getInstance() {
		if(instance == null) {
			instance = new DevHelperInCommon();
		}
		return instance;
	}
	
}
