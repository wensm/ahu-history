package com.carrier.ahu.report;

import com.carrier.ahu.common.entity.Part;

import lombok.ToString;

@ToString
public class ExportPart extends Part {
	private static final long serialVersionUID = -3386215885060435303L;
}