package com.carrier.ahu.length.param;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ElectricHeatingParam {

    private double WHeatQ;//冬季热量
    private double SHeatQ;//夏季热量
    private String serial;//机组型号
    private String GroupC;//组数
    private boolean EnableWinter;//冬季生效

}
