package com.carrier.ahu.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by liangd4 on 2017/9/28.
 */
public class CoilLen {

    public static Logger logger = LoggerFactory.getLogger(CoilLen.class.getName());

    /**
     * 冷水盘管段段长计算
     *
     * @param lengthParam
     * @param sectionId
     * @return
     */
    public Integer getLength(LengthParam lengthParam, String sectionId) {
        String unitModel = lengthParam.getSerial();//序列号
        String eliminator = lengthParam.getEliminator();//挡水器
        try {
            if (sectionId.equals(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId())) {
                if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {//直接蒸发式盘管选择无挡水器时，段长6M
                    return 6;
                }
                //后面有湿膜或高压喷雾，段长为12M
                if (null != lengthParam.getNextPartKey() &&
                        (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER) ||
                                SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER))) {
                    return 12;
                }
                return 8;//直接蒸发式盘管8
            }
            String cutSerial = unitModel.substring(unitModel.length() - 4, unitModel.length());
            Integer ser = Integer.parseInt(cutSerial);
            String rows = lengthParam.getRows();
            //盘管不为直膨，同时机组高度<20,后面是热水盘管的组合，段长为9M（旧软件中，冷水盘管后有热水盘管，总长为9M，其中冷水为6M，热水为3M）
            if (sectionId.equals(SectionTypeEnum.TYPE_COLD.getId()) && ser < 2500
                    && null != lengthParam.getNextPartKey()
                    && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)) {
                return 9 - 3;
            }
            //盘管为10R/12R,且后面没有湿膜加湿或者热水盘管组合，同时挡水器为无挡水器或者铝网挡水器时，段长为6M
            if ((rows.equals("10") || rows.equals("12"))
                    && null != lengthParam.getNextPartKey()
                    && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
                    && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) || SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                    return 6;
                }
            }
            //盘管为10R/12R,且后面有湿膜加湿器组合时，段长为8M
            if ((rows.equals("10") || rows.equals("12"))
                    && null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                if (!SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) || SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                    return 8;
                }
                return 8;
            }
            //盘管为10R/12R,且后面没有湿膜加湿或者热水盘管组合，同时挡水器为其他挡水器时，段长为8M
            if ((rows.equals("10") || rows.equals("12"))
                    && null != lengthParam.getNextPartKey()
                    && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
                    && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                if (!SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) || SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                    return 8;
                }
            }
            if (ser >= 2532) { //如果机型大于2532 12M
                return 12;
            } else {
                //后面有湿膜加湿器组合时，段长为6M
                if (null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                    return 6;
                }
                //后面有高压喷雾加湿器组合时，段长为5M
                if (null != lengthParam.getNextPartKey() && (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER) || (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)))) {
                    return 5;
                }
                if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                    return 5;
                } else {
                    return 6;
                }
            }
        } catch (Exception e) {
            logger.error("Failed to calculate sectionL for Cooling Coil: " + unitModel + " & " + eliminator, e);
            return 0;
        }
    }

    /**
     * 热水盘管段段长计算
     *
     * @param lengthParam
     * @return
     */
    public Integer getHLength(LengthParam lengthParam) {
        String unitModel = lengthParam.getSerial();
        String eliminator = lengthParam.getEliminator();
        String rows = lengthParam.getRows();
        int sectionL = 3;//排数3R-8R,且没有组合的湿膜加湿时，段长5M
        try {
            String cutSerial = unitModel.substring(unitModel.length() - 4, unitModel.length());
            Integer ser = Integer.parseInt(cutSerial);

            //热水+湿膜：热水盘管，段长特殊处理
            if (null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                //有组合的湿膜加湿时，段长6M
                return 6;
            }//冷水+热水：热水盘管，段长特殊处理：0+3
            if (ser < 2500 && null != lengthParam.getPrePartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getPrePartKey()).equals(SectionTypeEnum.TYPE_COLD)) {
                //有组合的湿膜加湿时，段长6M
                return 0 + 3;
            }
            if (rows.equals("2") || rows.equals("1")) {//排数1R/2R,且没有组合的湿膜加湿时，段长3M
                return 3;
            } else {
                return 5;
            }
        } catch (Exception e) {
            logger.error("Failed to calculate sectionL for Cooling Coil: " + unitModel + " & " + eliminator, e);
            return 0;
        }
    }
}
