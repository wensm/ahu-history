package com.carrier.ahu.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

/**
 * Created by liaoyw on 2017/4/16.
 */
public class ExecUtils {
    public static CommandResult executeCmd(String cmd) {
        int retCode = 1;
        String out = null;
        String err;
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            retCode = p.waitFor();
            out = readInputStream(p.getInputStream());
            err = readInputStream(p.getErrorStream());
        } catch (Exception e) {
            err = String.format("Exception-%s: %s", e.getClass(), e.getMessage());
        }
        return new CommandResult(retCode == 0, out, err, cmd);
    }

    private static String readInputStream(InputStream in) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(in,Charset.forName("UTF-8")))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }

    public static class CommandResult {
        public final boolean ok;
        public final String  out;
        public final String  err;
        public final String  cmd;

        public CommandResult(boolean ok, String out, String err, String cmd) {
            this.ok = ok;
            this.out = out;
            this.err = err;
            this.cmd = cmd;
        }

        public String toString() {
            return String.format("execute [%s] => %s\nout==> %s\nerr==> %s", cmd, ok, out, err);
        }
    }

}
