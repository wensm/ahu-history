package com.carrier.ahu.common.entity;

import com.carrier.ahu.po.AbstractPo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * // 单机应用，暂时不读取数据库，读取内存配置 后期进行扩展
 * 
 * @author JL
 *
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class User extends AbstractPo implements Serializable {
	private static final long serialVersionUID = -2273913931710382807L;
	public final static User UnknownUser = new User();
	@Id
	String userId;// 主键ID
	String userName;// 用户名
	String password;// 密码
	String userMobile;// 手机号码
	String userEmail;// 电子邮箱
	String userAddress;// 用户地址
	String userGroup;// 用户组织名
	String preferredLocale;// 用户选择的语言
	@Transient
	Map<String, String> unitPrefer;// 用户的单位选择
	String unitPreferCode;// 用户的单位选择代码
	@Transient
	Map<String, String> defaultParaPrefer;// 用户的单位选择
	String userFactory;//工厂
	String userRole;//角色
	String expireDate;//权限到期日期
}
