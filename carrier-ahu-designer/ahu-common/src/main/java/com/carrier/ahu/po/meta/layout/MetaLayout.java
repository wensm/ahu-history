package com.carrier.ahu.po.meta.layout;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Data
public class MetaLayout implements Serializable {
	private String key;
	private String version;
	
	private List<Group> groups = new ArrayList<>();
	
	public MetaLayout(String key) {
		this.key = key;
	}

}
