package com.carrier.ahu.report;

import com.carrier.ahu.common.entity.Unit;

import lombok.Data;

@Data
public class ReportUnitContent {
	private Unit unit;
	private String[][] content;
}
