package com.carrier.ahu.engine.heatrecycle;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MS270Bean extends DRIBean {
}
