package com.carrier.ahu.common.exception.engine;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class CoilEngineException extends EngineException {

    public CoilEngineException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public CoilEngineException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public CoilEngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public CoilEngineException(String message) {
        super(message);
    }

}
