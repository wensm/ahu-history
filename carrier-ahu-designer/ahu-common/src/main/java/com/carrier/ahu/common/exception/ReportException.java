package com.carrier.ahu.common.exception;

/**
 * Report Exception Class.
 * 
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class ReportException extends AhuException {

    public ReportException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public ReportException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public ReportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReportException(String message) {
        super(message);
    }

}
