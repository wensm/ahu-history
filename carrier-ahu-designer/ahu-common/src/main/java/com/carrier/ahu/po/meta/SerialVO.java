package com.carrier.ahu.po.meta;

import lombok.Data;

/**
 * Created by liangd4 on 2018/2/2.
 */
@Data
public class SerialVO {
    private String serial;//型号范围
}
