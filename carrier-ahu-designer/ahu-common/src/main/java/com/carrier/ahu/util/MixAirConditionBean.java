package com.carrier.ahu.util;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/14.
 */
@Data
public class MixAirConditionBean {

    /*混合段*/
    private double InWetBulbT;//回风湿球温度
    private double InRelativeT;//回风相对湿度
    private double NewWetBulbT;//新风湿球温度
    private double NewRelativeT;//新风相对湿度
    private double OutDryBulbT;//出风干球温度
    private double OutWetBulbT;//出风湿球温度
    private double OutRelativeT;//出风相对湿度
    /*公式计算*/
    private double NARatio;//新风比
    private double NAVolume;//新风量

}
