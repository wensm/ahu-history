package com.carrier.ahu.engine.dxcoil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.sun.jna.ptr.DoubleByReference;

/**
 * Created by liaoyw on 2017/3/28.
 */
class DXUtils {
    static DoubleByReference dRef(double d) {
        return new DoubleByReference(d);
    }

    static DoubleByReference dRef() {
        return new DoubleByReference();
    }

    static String readCpc(String cpcPath) {
        try {
            List<String> lines = Files.readAllLines(new File(cpcPath).toPath());
            return lines.stream().map(s -> s.trim()).collect(Collectors.joining("#"));
        } catch (IOException e) {
            throw new CoilEngineException("Failed to read cpc", e);
        }
    }
}
