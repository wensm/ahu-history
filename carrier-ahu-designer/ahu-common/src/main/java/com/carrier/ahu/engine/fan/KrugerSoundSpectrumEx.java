package com.carrier.ahu.engine.fan;

import lombok.Data;

/**
 * Kruger SoundSpectrumEx Rest API response object.
 * 
 * Created by Braden Zhou on 2019/02/20.
 */
@Data
public class KrugerSoundSpectrumEx {

    private String Inlet_LPA_Overall;
    private float[] Inlet_LpA_Spectrum;
    private float Inlet_LWA_Overall;
    private float[] Inlet_LwA_Spectrum;
    private float Inlet_LwLin_Overall;
    private float[] Inlet_LwLin_Spectrum;
    private String Outlet_LPA_Overall;
    private String[] Outlet_LpA_Spectrum;
    private float Outlet_LWA_Overall;
    private float[] Outlet_LwA_Spectrum;
    private float Outlet_LwLin_Overall;
    private float[] Outlet_LwLin_Spectrum;

}
