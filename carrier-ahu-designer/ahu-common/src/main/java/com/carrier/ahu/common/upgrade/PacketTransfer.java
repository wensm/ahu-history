package com.carrier.ahu.common.upgrade;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.carrier.ahu.constant.CommonConstant;

/**
 * 
 * Created by Braden Zhou on 2018/07/19.
 */
public class PacketTransfer {

    public static UpgradePacket sendAndReceivePacket(Socket socket, UpgradePacket packet) throws IOException {
        sendPacket(socket, packet);
        return receivePacket(socket);
    }

    public static void sendPacket(Socket socket, UpgradePacket packet) throws IOException {
        OutputStream output = socket.getOutputStream();
        output.write(packet.getPacketBytes());
        output.flush();
    }

    public static UpgradePacket receivePacket(Socket socket) throws IOException {
        UpgradePacket packet = null;
        InputStream input = socket.getInputStream();
        byte[] header = streamToBytes(input, CommonConstant.SYS_UPGRADE_HEADER_LENGTH);
        if (UpgradePacket.validateHeader(header)) {
            byte[] bodyLength = streamToBytes(input, CommonConstant.SYS_UPGRADE_BODY_LENGTH_LENGTH);
            byte[] body = streamToBytes(input, Integer.valueOf(new String(bodyLength)));
            byte[] md5 = streamToBytes(input, CommonConstant.SYS_UPGRADE_MD5_LENGTH);

            packet = new UpgradePacket();
            packet.setHeader(new String(header));
            packet.setBodyLength(new String(bodyLength));
            packet.setBody(new String(body));
            packet.setMd5(new String(md5));
        }

        return packet;
    }

    public static byte[] streamToBytes(InputStream inputStream, int len) throws IOException {
        byte[] bytes = new byte[len];
        inputStream.read(bytes, 0, len);
        return bytes;
    }

}
