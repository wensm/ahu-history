package com.carrier.ahu.engine.price;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Native;

/**
 * 
 * Created by Braden Zhou on 2018/11/12.
 */
@AhuPrice(CommonConstant.SYS_PRICE_VERSION_20190101)
public interface AHUPriceCal20190101 extends AHUPriceCal {

    AHUPriceCal PRICE_DLL_INSTANCE_20190101 = (AHUPriceCal) Native.loadLibrary(
            String.format(CommonConstant.SYS_PATH_PRICE_DLL_VERSION, CommonConstant.SYS_PRICE_VERSION_20190101),
            AHUPriceCal.class);

}
