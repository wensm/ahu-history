package com.carrier.ahu.common.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * User Meta table composite keys.
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
@Data
@Embeddable
public class UserMetaId implements Serializable {

    private static final long serialVersionUID = -7640306674892374083L;

    @NotNull
    private String userId; // use machine name for standalone version

    @NotNull
    private String metaKey;

    public UserMetaId() {
    }

    public UserMetaId(String userId, String metaKey) {
        this.userId = userId;
        this.metaKey = metaKey;
    }

}
