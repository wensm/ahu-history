package com.carrier.ahu.common.exception;

/**
 * Used for exception when API is called. </br>
 * It'll return a meaningful message to client.
 * 
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class ApiException extends AhuException {

    public ApiException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public ApiException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiException(String message) {
        super(message);
    }

}
