package com.carrier.ahu.po;

public class UnitType {
	
	private String typeName;
	
	private String velocity;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getVelocity() {
		return velocity;
	}

	public void setVelocity(String velocity) {
		this.velocity = velocity;
	}
	
	
	
}
