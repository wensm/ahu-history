package com.carrier.ahu.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class PublicParam {

    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection;//风向

}
