package com.carrier.ahu.po.meta;

import com.carrier.ahu.constant.CommonConstant;
import com.google.gson.annotations.Expose;
import lombok.Data;

import java.util.Set;

/**
 * Created by liangd4 on 2018/2/2.
 */
@Data
public class RulesVO {

    @Expose(serialize = true, deserialize = true)
    private String rule;//规则名称
    @Expose(serialize = true, deserialize = true)
    private Set<String> ruleValueSet;//规则对应值
    @Expose(serialize = true, deserialize = true)
    private String ruleMassage;//页面显示消息code
    @Expose(serialize = true, deserialize = true)
    private String level = CommonConstant.SYS_ASSERT_ERROR;//等级 error warning
    @Expose(serialize = true, deserialize = true)
    private String alarm = CommonConstant.SYS_ASSERT_FALSE;//true false
}
