package com.carrier.ahu.po.meta;

import java.io.Serializable;

/**
 * Describe a meta data parameter
 * 
 * @author JL
 *
 */
public class MetaParameter implements Serializable {

	// 唯一的属性的名字
	private String key;
	// 英文名字，临时用于多语言时，界面的英文展现
	private String name;
	// 属性的虚拟分组，暂时没有意义，与界面上的属性面板的名字对应，可以先写name
	private String group;
	// 描述该属性是否与材料相关
	private boolean rMaterial;
	// 描述该属性是否与ahu的方向相关
	private boolean rDirection;
	// 描述该属性是否与性能相关
	private boolean rPerformance;
	// 描述该属性是否可编辑，不可编辑，意味着时通过计算获取
	private boolean editable;
	// 是否为全局属性，默认为false，暂时不使用
	private boolean global;
	// 一个属性有很多选项值，例如材料，这些值需要来自另一个元数据
	// SOURCE 定义在：MetaValueSource
	private String valueSource;
	// 属性的值的类型
	private ValueType valueType = ValueType.StringV;
	// 其他额外信息，现在存放中文名称
	private String memo;
	// 默认存放英文的信息
	private String ememo;
	// 改属性是否允许非标配置，如果允许，会从该属性衍生出一个非标属性，元数据与原始属性相同
	private boolean urEnable = false;
	
	public MetaParameter() {

	}

//	public MetaParameter(String key, String name, String group, boolean rMaterial, boolean rDirection,
//			boolean rPerformance, boolean editable, boolean global, String valueSource, ValueType valueType,
//			String memo) {
//		this(key, name, group, rMaterial, rDirection, rPerformance, editable, false, global, valueSource, valueType,
//				memo, "default");
//
//	}

	public MetaParameter(String key, String name, String group, boolean rMaterial, boolean rDirection,
			boolean rPerformance, boolean editable, boolean urEnable, boolean global, String valueSource,
			ValueType valueType, String memo, String ememo) {
		super();
		this.key = key;
		this.name = name;
		this.group = group;
		this.rMaterial = rMaterial;
		this.rDirection = rDirection;
		this.rPerformance = rPerformance;
		this.editable = editable;
		this.urEnable = urEnable;
		this.global = global;
		this.valueSource = valueSource;
		this.valueType = valueType;
		this.memo = memo;
		this.ememo = ememo;
	}

//	public MetaParameter(String key, String name, String group, boolean rMaterial, boolean rDirection,
//			boolean rPerformance, boolean editable, boolean urEnable, boolean global, String valueSource,
//			ValueType valueType, String memo) {
//		this(key, name, group, rMaterial, rDirection, rPerformance, editable, urEnable, global, valueSource, valueType,
//				memo, "default");
//	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean isrMaterial() {
		return rMaterial;
	}

	public void setrMaterial(boolean rMaterial) {
		this.rMaterial = rMaterial;
	}

	public boolean isrDirection() {
		return rDirection;
	}

	public void setrDirection(boolean rDirection) {
		this.rDirection = rDirection;
	}

	public boolean isrPerformance() {
		return rPerformance;
	}

	public void setrPerformance(boolean rPerformance) {
		this.rPerformance = rPerformance;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isGlobal() {
		return global;
	}

	public void setGlobal(boolean global) {
		this.global = global;
	}

	public String getValueSource() {
		return valueSource;
	}

	public void setValueSource(String valueSource) {
		this.valueSource = valueSource;
	}

	public ValueType getValueType() {
		return valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public boolean isUrEnable() {
		return urEnable;
	}

	public void setUrEnable(boolean urEnable) {
		this.urEnable = urEnable;
	}

	public String getEmemo() {
		return ememo;
	}

	public void setEmemo(String ememo) {
		this.ememo = ememo;
	}

}
