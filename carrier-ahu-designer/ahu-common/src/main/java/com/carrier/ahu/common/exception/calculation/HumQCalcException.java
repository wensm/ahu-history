package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/06/07.
 */
@SuppressWarnings("serial")
public class HumQCalcException extends CalculationException {

    public HumQCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public HumQCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public HumQCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public HumQCalcException(String message) {
        super(message);
    }

}
