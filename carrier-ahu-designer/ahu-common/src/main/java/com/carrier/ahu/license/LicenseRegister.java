package com.carrier.ahu.license;

import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.common.util.RSAUtils;

/**
 * 生成license
 * 
 * @author Simon 2018.01.28
 */
public class LicenseRegister {
	/** localValue format */
	private static final String MSG = "mac=%s;name=%s;domain=%s;";
	/** RSA算法 公钥和私钥是一对，此处只用私钥加密 */
	public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiQq0T5YwvRYsdx/HGg1zAISyI5MZ788qis0XG\r\n"
			+ "faf4QsrSsaactxN1HH3NvfZ7k6+Gia/7coniTEabXtxG96CY1ccIRax47YxJEX4QJceAcTEr4AmA\r\n"
			+ "z4DR6MS3Bq4DiskM1szyMl0i4B7Dy7G0VypgOb6JFhdmaB0KkGnRpMhyOwIDAQAB";

	/**
	 * 公钥解密
	 * 
	 * @param licensePath
	 * @throws Exception
	 */
	public static String publicKeyDecode(String licensePath) throws Exception {
		byte[] encodedData = FileUtil.fileToByte(licensePath);
		byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
		String target = new String(decodedData);
		System.out.println("解密后: \r\n" + target);
		return target;
	}

    public static String publicKeyDecode(byte[] encodedData) throws Exception {
        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        return new String(decodedData);
    }

	public static boolean verify(String target, String localValue) {
		if (localValue.equals(target)) {
			return true;
		}
		return false;
	}

	public static String getLocalValue(String mac, String userName, String domain) {
		return String.format(MSG, mac, userName, domain);
	}

	public static void main(String[] args) throws Exception {
		publicKeyDecode(args[0]);
	}
}