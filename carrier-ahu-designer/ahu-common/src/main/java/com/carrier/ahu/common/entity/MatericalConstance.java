/**
 * Title: MatericalConstance <br>
 * @Description:[] <br>
 * @date 2017-06-22 <br>
 * Copyright (c) 2016 ZXZL YCT.  <br>
 * 
 * @author zhiqiang1.zhang@dhc.com.cn
 * @version V1.0
 */
package com.carrier.ahu.common.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.carrier.ahu.po.AbstractPo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description: 本类为自动生成，对应数据库表名： matericalconstance <br>
 * @date 2017-06-22 16:37 <br>
 * 
 * @author taosy@adinnet.cn
 * @version V1.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class MatericalConstance extends AbstractPo implements Serializable {
	private static final long serialVersionUID = -4225080318716353441L;
	@Id
	@GeneratedValue
	String pid;
	String projid;
	String groupid;
	String calctype;//性能最优 or 最经济 or none
	@Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name="PROKEY", columnDefinition="CLOB", nullable=true)
	String prokey;

}