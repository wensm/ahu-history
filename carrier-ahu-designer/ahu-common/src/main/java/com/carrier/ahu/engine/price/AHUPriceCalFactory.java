package com.carrier.ahu.engine.price;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2018/11/12.
 */
@Slf4j
public class AHUPriceCalFactory {

    public static AHUPriceCal getAHUPriceCal(String version) {
        if (CommonConstant.SYS_PRICE_VERSION_20190101.equals(version)) {
            return AHUPriceCal20190101.PRICE_DLL_INSTANCE_20190101;
        }
        return AHUPriceCal20190101.PRICE_DLL_INSTANCE_20190101;
    }

    public static List<String> getAHUPriceCalVersion() {
        List<String> versions = new ArrayList<>();
        try {
            ImmutableSet<ClassInfo> classInfos = ClassPath.from(AHUPriceCalFactory.class.getClassLoader())
                    .getTopLevelClasses(CommonConstant.SYS_PATH_PRICE_PACKAGE);
            for (ClassInfo classInfo : classInfos) {
                Class<?> clazz = classInfo.load();
                AhuPrice ahuPrice = clazz.getAnnotation(AhuPrice.class);
                if (ahuPrice != null && EmptyUtil.isNotEmpty(ahuPrice.value())) {
                    versions.add(ahuPrice.value());
                }
            }
        } catch (Exception exp) {
            log.error("System configuration error occurred!", exp);
        }
        Collections.sort(versions);
        return versions;
    }

}
