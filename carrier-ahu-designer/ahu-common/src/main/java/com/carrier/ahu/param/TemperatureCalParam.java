package com.carrier.ahu.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/14.
 */
@Data
public class TemperatureCalParam {

    /*机组参数*/
    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向
    /*新风温度*/
    private double InDryBulbT;//干球温度
    private double InWetBulbT;//湿球温度
    /*回风温度*/
    private double NewDryBulbT;//干球温度
    private double NewWetBulbT;//湿球温度
    /*条件*/
    private double NARatio;//新风比
    /*新回排风段*/
    private double NAVolume;//新风量
    private double RAVolume;//回风量

    private boolean EnableWinter;//新回排
    private boolean EnableSummer; //混合段
}
