package com.carrier.ahu.common.enums;

/**
 * 分段中包含的端面类型
 * 
 */
public enum FaceTypeEnum {

    LEFT_ONLY(1), RIGHT_ONLY(2), LEFT_RIGHT(3), NONE(4);

    private int code;

    private FaceTypeEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    /**
     * 一个分段在机组层（上/下）包含左端面
     * @return
     */
    public boolean isHasLeft() {
        return this.code == LEFT_ONLY.code || this.code == LEFT_RIGHT.code;
    }

    /**
     * 一个分段在机组层（上/下）包含右端面
     * @return
     */
    public boolean isHasRight() {
        return this.code == RIGHT_ONLY.code || this.code == LEFT_RIGHT.code;
    }



}
