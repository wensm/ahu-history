package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class CalculationException extends AhuException {

    public CalculationException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public CalculationException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public CalculationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculationException(String message) {
        super(message);
    }

}
