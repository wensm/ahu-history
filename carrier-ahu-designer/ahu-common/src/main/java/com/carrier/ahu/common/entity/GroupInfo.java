package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.po.AbstractPo;

/**
 * Created by Wen zhengtao on 2017/3/25.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class GroupInfo extends AbstractPo {
	private static final long serialVersionUID = -1972700562234954895L;
	@Id
	String groupId;// 主键ID
	String projectId;// 项目ID
	String groupType;// 组类型,comes from AHU Type Code
	String groupName;// 组名称
	String groupCode;// 组编号
	String groupMemo;// 组备注
	
	/**
	 * 获取机组段的总数
	 * @return
	 */
	public int getSectionsNum() {
		if(StringUtils.isBlank(groupCode)) {
			return 0;
		}
		return groupCode.length()/3;
	}
}
