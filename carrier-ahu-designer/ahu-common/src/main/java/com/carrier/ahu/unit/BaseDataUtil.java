package com.carrier.ahu.unit;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.EmptyUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * Created by liangd4 on 2017/7/25.
 */
public class BaseDataUtil {

    public static Double stringConversionDouble(String string) {
        if (EmptyUtil.isEmpty(string))
            return 0.0;
        else {
            if (CommonConstant.SYS_ASSERT_NULL.equals(string) || CommonConstant.SYS_ASSERT_NULL_1.equals(string)) {//防止null的字符串转换报错。
                return 0.0;
            } else {
                return Double.valueOf(string);
            }
        }
    }

    public static Integer stringConversionInteger(String string) {
        string = string.replace(CommonConstant.SYS_PUNCTUATION_SLASH, CommonConstant.SYS_BLANK);
        return strEmpty(string) ? Integer.valueOf(string.split("\\.")[0]) : null;
    }

    public static Integer doubleConversionInteger(Double d) {
        return new Double(d).intValue();
    }

    public static Boolean stringConversionBoolean(String string) {
        return Boolean.valueOf(string);
    }

    public static Long integerConversionLong(Integer integer) {
        return Long.valueOf(String.valueOf(integer));
    }

    public static Integer longConversionInteger(Long l) {
        return Integer.valueOf(String.valueOf(l));
    }

    public static Long doubleConversionLong(Double d) {
        return Math.round(d);
    }

    public static Double integerConversionDouble(Integer integer) {
        return Double.valueOf(integer);
    }

    public static float intConversionFloat(int f) {
        return BaseDataUtil.doubleConversionFloat(Double.valueOf(f));
    }

    public static String integerConversionString(Integer integer) {
        return String.valueOf(integer);
    }

    public static String doubleConversionString(Double d) {
        return String.valueOf(d);
    }

    public static String doubleConversionIntString(Double d) {
        String[] strings = String.valueOf(d).split("\\.");
        if (strings.length > 0) {
            return strings[0];
        }
        return String.valueOf(d);
    }

    public static Boolean strEmpty(String string) {
        if (StringUtils.isEmpty(string)) {
            return false;
        }
        return true;
    }

    public static Short integerConversionShort(Integer integer) {
        return Short.valueOf(String.valueOf(integer));
    }

    public static Short doubleConversionShort(Double d) {
        return integerConversionShort(doubleConversionInteger(d));
    }

    // double转String 保留i位小数
    public static String doubleToString(double d, int i) {
        String s = "%." + i + "f";
        return String.format(s, d);
    }

    /* 四舍五入保留 i 位小数 */
    public static Double decimalConvert(double d, int i) {
        BigDecimal bg = new BigDecimal(d);
        return bg.setScale(i, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
    /* 四舍五入保留 i 位小数 */
    public static Double decimalConvert(float d, int i) {
        BigDecimal bg = new BigDecimal(d);
        return bg.setScale(i, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /*数据类型强转String*/
    public static String constraintString(Object o) {
        if (EmptyUtil.isEmpty(o)) {
            return CommonConstant.SYS_BLANK;
        }
        return o.toString();
    }

    /*绝对值*/
    public static Double doubleConvertABS(double d) {
        return Math.abs(d);
    }

    public static double objectToDouble(Object o) {
        return Double.valueOf(o.toString());
    }

    public static String objectToString(Object o) {
        return String.valueOf(o);
    }

    public static void main(String[] args) {
//		System.out.println(decimalConvert(1.8989898989, 3));
//		System.out.println(decimalConvert(1.8989898989, 0));
        System.out.println(constraintString(true));
    }

    public static int trans2int(String str) {
        int retInt = 0;
        if (null == str || CommonConstant.SYS_BLANK.equals(str.trim()) || CommonConstant.SYS_ASSERT_NULL.equals(str.toLowerCase().trim())) {
        } else {
            try {
                retInt = Integer.parseInt(str);
            } catch (NumberFormatException e) {
            }
        }
        return retInt;
    }

    public static double trans2Double(String str) {
        double retInt = 0;
        if (null == str || CommonConstant.SYS_BLANK.equals(str.trim()) || CommonConstant.SYS_ASSERT_NULL.equals(str.toLowerCase().trim())) {
        } else {
            try {
                retInt = Double.parseDouble(str);
            } catch (NumberFormatException e) {
            }
        }
        return retInt;
    }

    /*double转float*/
    public static float doubleConversionFloat(Double d) {
        if (EmptyUtil.isEmpty(d)) {
            return 0;
        } else {
            return Float.valueOf(String.valueOf(d));
        }
    }

}


