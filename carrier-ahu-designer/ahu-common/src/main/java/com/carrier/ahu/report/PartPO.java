package com.carrier.ahu.report;

import com.carrier.ahu.common.entity.Part;

import lombok.Data;

@Data
public class PartPO {
	
	Part currentPart;
	Part prePart;
	Part nextPart;

}
