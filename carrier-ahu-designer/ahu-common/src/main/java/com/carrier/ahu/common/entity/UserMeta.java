package com.carrier.ahu.common.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

/**
 * User Meta table to store settings of user behavior.
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
@Data
@ToString
@Entity
public class UserMeta implements Serializable {

    private static final long serialVersionUID = -9202509453766699241L;

    @EmbeddedId
    private UserMetaId userMetaId; // use machine name for standalone version
    @NotNull
    private String factoryId;
//    private String metaKey;
    private String metaValue;

    public UserMeta() {
    }

    public UserMeta(UserMetaId userMetaId) {
        this.userMetaId = userMetaId;
    }

}