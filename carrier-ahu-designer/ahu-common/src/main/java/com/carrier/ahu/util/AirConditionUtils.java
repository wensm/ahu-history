package com.carrier.ahu.util;

import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.unit.BaseDataUtil;

/**
 *
 * t (DB) 干球温度℃ tb (WB) 湿球温度 φ(%) 相对湿度 i (kj/kg) 焓 d (g/kg) 含湿量 Pg (Pa) 水蒸汽分压力  Td露点温度
 *
 *
 */
public class AirConditionUtils {

	public static String error_msg="无效的温度计算！";
	/*
	 * Formula 1)interpolation (y3-y2)/(x3-x2)=(y2-y1)/(x2-x1)
	 */
	@SuppressWarnings("unused")
	public static double fInterpolation(double x1, double x2, double x3, double y1, double y3) {

		double y2;
		if (Math.abs(x3 - x1) < AirConditionData.PRECSIONTMP) {
			return AirConditionData.CALCULATION_FAIL;
		} else if (Math.abs(x3 - x2) < AirConditionData.PRECSIONTMP) {
			return y3;
		} else if (Math.abs(x1 - x2) < AirConditionData.PRECSIONTMP) {
			return y1;
		}

		return (y1 * (x3 - x2) + y3 * (x2 - x1)) / (x3 - x1);

	}

	// Formula 2)TB---->Db
	// search the value form the saturable table
	//

	@SuppressWarnings("unused")
	public static double fSearchTBDb(double parmTB) {

		double LVarTB, LVarD, LVarD1, LVarD2, LVarTmpTB1, LVarTmpTB2, LVarTquery;

		LVarTB = AirConditionDvalue.processValue(parmTB);

		LVarTmpTB1 = AirConditionPvapor.getBackwardValue(LVarTB);
		LVarD1 = AirConditionDvalue.getDbValue(LVarTmpTB1);

		if (Math.abs(LVarTB - LVarTmpTB1) < AirConditionData.PRECSIONTMP) {
			return LVarD1;
		}

		LVarTmpTB2 = AirConditionPvapor.getForwardValue(LVarTB);
		LVarD2 = AirConditionDvalue.getDbValue(LVarTmpTB2);

		if (Math.abs(LVarTB - LVarTmpTB2) < AirConditionData.PRECSIONTMP) {
			return LVarD2;

		}

		LVarD = fInterpolation(LVarTmpTB1, LVarTB, LVarTmpTB2, LVarD1, LVarD2);
		// Interpolation
		if (LVarD != AirConditionData.CALCULATION_FAIL) {
			return LVarD;
		}

		return AirConditionData.CALCULATION_FAIL;
	}

	// Formula 3)T------>Pqb (search the value form the saturable table)

	@SuppressWarnings("unused")
	public static double FSearchTPqb(double parmT) {

		double LVarTB, LVarD, LVarD1, LVarD2, LVarTmpTB1, LVarTmpTB2, LVarTquery;
		LVarTB = AirConditionPvapor.processValue(parmT);

		LVarTmpTB1 = AirConditionPvapor.getBackwardValue(LVarTB);
		LVarD1 = AirConditionPvapor.getDbValue(LVarTmpTB1);

		if (Math.abs(LVarTB - LVarTmpTB1) < AirConditionData.PRECSIONTMP) {
			return LVarD1;
		}

		LVarTmpTB2 = AirConditionPvapor.getForwardValue(LVarTB);
		LVarD2 = AirConditionPvapor.getDbValue(LVarTmpTB2);

		if (Math.abs(LVarTB - LVarTmpTB2) < AirConditionData.PRECSIONTMP) {
			return LVarD2;

		}

		LVarD = fInterpolation(LVarTmpTB1, LVarTB, LVarTmpTB2, LVarD1, LVarD2);
		// Interpolation
		if (LVarD != AirConditionData.CALCULATION_FAIL) {
			return LVarD;
		}

		return AirConditionData.CALCULATION_FAIL;
	}

	// Formula 4)Ib=(1.01+1.84*Db/1000)*TB+2500*Db/1000
	// or I=(1.01+1.84*D/1000)*T+2500*D/1000 (kj/kg)
	// return: FFormaulaIb

	public static double FFormulaIb(double ParmDb, double ParmTB) {
		return (1.01 + 1.84 * ParmDb / 1000) * ParmTB + 2500 * ParmDb / 1000;
	}

	// Formula 5)D=[(4.19*TB*Db/1000-Ib+1.01*T)/(4.19*TB-1.84*T-2500)]*1000
	// (g/kg)
	// return: FFormulaD

	public static double FFormulaD(double ParmTB, double ParmDb, double ParmIb, double ParmT) {
		return ((4.19 * ParmTB * ParmDb / 1000 - ParmIb + 1.01 * ParmT) / (4.19 * ParmTB - 1.84 * ParmT - 2500)) * 1000;
	}

	// Formula 6)Pq=[101325*(D/1000)]/[0.622+(D/1000)] (Pa)
	// return:FFormulaPq

	public static double FFormulaPq(double ParmD) {
		return (101325 * (ParmD / 1000)) / (0.622 + ParmD / 1000);
	}

	// Formula 7)d=[(0.622* Pq)/(101325- Pq)]*1000 (g/kg)
	// retrurn:FFormulaDPq

	public static double FFormulaDPq(double ParmPq) {
		return ((0.622 * ParmPq) / (101325 - ParmPq)) * 1000;
	}

	// Formula 8)ρ=(0.003484*101325)/[273.15+(t_in+t_out)/2]

	public static double FFormulaDensity(double ParmTin, double ParmTout) {
		return (0.003484 * 101325) / (273.15 + (ParmTin + ParmTout) / 2);
	}
	
	public static double FFormulaTD(double ParmTin, double parmRH) {
		return 243.04*(Math.log(parmRH/100)+((17.625*ParmTin)/(243.04+ParmTin)))/(17.625-Math.log(parmRH/100)-((17.625*ParmTin)/(243.04+ParmTin))) ;
	}
	
	public static void main(String args[]) {
		System.out.println(FFormulaTD(25.0,50.0));
	}

	// Iteration
	// 1)TB=T TB------>Db
	// 2)Ib=(1.01+1.84*Db/1000)*TB+2500*Db/1000
	// 3)D=[(4.19*TB*Db/1000-Ib+1.01*T)/(4.19*TB-1.84*T-2500)]*1000
	// 4)|D-Dx|<0.01 ----------->TB (TB<T)
	// -20<=TB<100 TB(Increase) --------> D(Increase)
	@SuppressWarnings("unused")
	public static double FIterationTBDb(double ParmT, double ParmD) {
		int i;
		double LVarDx, LVarIb, LVarTB, LVarDb, LVarStepSize, RetParmTB;
		LVarTB = ParmT;
		if (LVarTB == 0) {
			LVarStepSize = AirConditionData.MAXSTEPSIZE;
		} else {
			LVarStepSize = LVarTB;
		}

		for (i = 1; i <= AirConditionData.IETERATIONNUM; i++) {
			LVarDb = fSearchTBDb(LVarTB);
			if (LVarDb != AirConditionData.CALCULATION_FAIL) {
				// 1)TB=T TB------>Db the saturable table
				LVarIb = FFormulaIb(LVarDb, LVarTB); // 2)Ib=(1.01+1.84*Db/1000)*TB+2500*Db/1000
				LVarDx = FFormulaD(LVarTB, LVarDb, LVarIb, ParmT); // 3)D=[(4.19*TB*Db/1000-Ib+1.01*T)/(4.19*TB-1.84*T-2500)]*1000
				if (Math.abs(LVarDx - ParmD) >= AirConditionData.PRECISIOND) {
					// StepSize
					// -20<=TB<100 TB(Increase) --------> D(Increase)
					if ((LVarDx - ParmD) > 0) {

						LVarStepSize = Math.abs(LVarStepSize) / 2;
						if (Math.abs(LVarStepSize) < AirConditionData.MINSTEPSIZE) {
							LVarStepSize = AirConditionData.MINSTEPSIZE;
						}
					} else {
						LVarStepSize = -Math.abs(LVarStepSize) / 2;
						if (Math.abs(LVarStepSize) < AirConditionData.MINSTEPSIZE) {
							LVarStepSize = -AirConditionData.MINSTEPSIZE;
						}
					}
					LVarTB = LVarTB - LVarStepSize;
				} else {
					// Found the value 4)|D-Dx|<0.01 ----------->TB
					break;
				}
			}

			else {
				return AirConditionData.CALCULATION_FAIL;
			}
		}
		return LVarTB;
	}

	// Iteration
	// 1)if ParmFlag=true then Tbx(ParmTx)-->Dbx (the saturable table)
	// else Tx(ParmTx)-->Dx (1)T-->Pqb(the saturable table)
	// (2) Pq = φ* Pqb
	// (3) D=[(0.622* Pq)/(101325- Pq)]*1000 (g/kg)
	// 2)Ix=(1.01+1.84*Dx/1000)* Tx+2500*Dx/1000
	// 3)|Ix-I|<0.01 ------------>T
	// -20<=T<100 T(Increase) --------> I(Increase)

	@SuppressWarnings("unused")
	public static double FIterationTI(double ParmTx, double ParmI, double ParmF, boolean ParmFlag) {
		int i;
		double LVarDx, LVarIx, LVarPq, LVarPqb, LVarTx, LVarStepSize, RetParmT;
		LVarTx = ParmTx;
		if (LVarTx == 0) {
			LVarStepSize = AirConditionData.MAXSTEPSIZE;
		}else {
			LVarStepSize = LVarTx;
		}
		for (i = 1; i < AirConditionData.IETERATIONNUM; i++) { // Iteration
			if (ParmFlag) {
				// 1) Tbx(ParmTx)-->Dbx (the saturable table)
				LVarDx = fSearchTBDb(LVarTx);
				if (LVarDx == AirConditionData.CALCULATION_FAIL) {
					return AirConditionData.CALCULATION_FAIL;
				}
			} else {
				// //Tx(ParmTx)-->Dx
				LVarPqb = FSearchTPqb(LVarTx);
				if (LVarPqb == AirConditionData.CALCULATION_FAIL) { // 1)(1)T-->Pqb(the saturable
					return AirConditionData.CALCULATION_FAIL;
				} else {
					LVarPq = ParmF * LVarPqb; // 1)(2) Pq = φ* Pqb
					LVarDx = FFormulaDPq(LVarPq); // 1)(3) D=[(0.622*
													// Pq)/(101325- Pq)]*1000
													// (g/kg)
				}
			}

			LVarIx = FFormulaIb(LVarDx, LVarTx); // 2)Ix=(1.01+1.84*Dx/1000)*
													// Tx+2500*Dx/1000
			if (Math.abs(LVarIx - ParmI) >= AirConditionData.PRECISIONI) {
				// StepSize
				// -20<=T<100 T(Increase) --------> I(Increase)
				if ((LVarIx - ParmI) > 0) {
					LVarStepSize = Math.abs(LVarStepSize) / 2;
					if (Math.abs(LVarStepSize) < AirConditionData.MINSTEPSIZE) {
						LVarStepSize = AirConditionData.MINSTEPSIZE;
					}
				} else {
					LVarStepSize = -Math.abs(LVarStepSize) / 2;
					if (Math.abs(LVarStepSize) < AirConditionData.MINSTEPSIZE) {
						LVarStepSize = -AirConditionData.MINSTEPSIZE;
					}
				}
				LVarTx = LVarTx - LVarStepSize;
			} else {
				// Found the value 4)|Ix-I|<0.1 ----------->T
				break;
			}
		}
		return LVarTx;

	}

	// Air parameter calculate 1 T,TB----->φ,I,D
	// return: FAirParmCalculate1 succeed true
	// fail false

	@SuppressWarnings("unused")
	public static AirConditionBean FAirParmCalculate1(double ParmT, double ParmTB) {
		double LVarDb, LVarIb, LVarPq, LVarPqb, LVarF, RetParmF, RetParmI, RetParmD,RetParmTD;
		LVarDb = fSearchTBDb(ParmTB);
		AirConditionBean retAir = new AirConditionBean();
		if (LVarDb != AirConditionData.CALCULATION_FAIL) {
			LVarIb = FFormulaIb(LVarDb, ParmTB);
			RetParmD = FFormulaD(ParmTB, LVarDb, LVarIb, ParmT);
			LVarPq = FFormulaPq(RetParmD);
			LVarPqb = FSearchTPqb(ParmT);
			if (LVarPqb != AirConditionData.CALCULATION_FAIL) // the saturable table
			{
				RetParmF = LVarPq / LVarPqb;
				RetParmI = FFormulaIb(RetParmD, ParmT);
				RetParmTD=FFormulaTD(ParmT,RetParmF);
			} else {
				retAir.setFlg(false);
				return retAir;
			}
		} else {
			retAir.setFlg(false);
			return retAir;
		}

		retAir.setParamD(BaseDataUtil.decimalConvert(RetParmD,1));
		retAir.setParamI(BaseDataUtil.decimalConvert(RetParmI,1));
		retAir.setParmF(BaseDataUtil.decimalConvert(RetParmF,0));
		retAir.setParmT(ParmT);
		retAir.setParmTb(ParmTB);
		retAir.setParamTd(BaseDataUtil.decimalConvert(RetParmTD,1));
		return retAir;
	}

	// Air parameter calculate 2 T,D ----->φ,I,TB
	// return: FAirParmCalculate2 succeed true
	// fail false

	public static AirConditionBean FAirParmCalculate2(double ParmT, double ParmD) throws TempCalErrorException {
		double LVarPqb, LVarPq, RetParmF, RetParmI, RetParmTB,RetParmTD;

		LVarPqb = FSearchTPqb(ParmT);
		if (LVarPqb != AirConditionData.CALCULATION_FAIL) {
			// the saturable table
			LVarPq = FFormulaPq(ParmD);
			RetParmF = LVarPq / LVarPqb;
			RetParmI = FFormulaIb(ParmD, ParmT);
			// Iteration
			RetParmTB = FIterationTBDb(ParmT, ParmD);
			RetParmTD=FFormulaTD(ParmT,RetParmF);
			if (RetParmTB == AirConditionData.CALCULATION_FAIL) {
				//TODO update to error message id
				throw new TempCalErrorException(error_msg);
			}
		} else {
			//TODO update to error message id
			throw new TempCalErrorException(error_msg);
		}
		AirConditionBean retAir = new AirConditionBean();
		retAir.setParamD(ParmD);
		retAir.setParamI(RetParmI);
		retAir.setParmF(RetParmF);
		retAir.setParmT(ParmT);
		retAir.setParmTb(RetParmTB);
		retAir.setParamTd(BaseDataUtil.decimalConvert(RetParmTD,1));
		return retAir;
	}

	// Air parameter calculate 3 T,φ----->TB,I,D
	// return: FAirParmCalculate3 succeed true
	// fail false

	public static AirConditionBean FAirParmCalculate3(double ParmT, double ParmF) throws TempCalErrorException {
		double LVarPqb, LVarPq, RetParmTB, RetParmI, RetParmD,RetParmTD;
		LVarPqb = FSearchTPqb(ParmT);
		if (LVarPqb != AirConditionData.CALCULATION_FAIL) // the saturable table
		{
			LVarPq = ParmF * LVarPqb;
			RetParmD = FFormulaDPq(LVarPq);
			RetParmI = FFormulaIb(RetParmD, ParmT);
			// Iteration
			RetParmTB = FIterationTBDb(ParmT, RetParmD);
			RetParmTD=FFormulaTD(ParmT,ParmF);
			if (RetParmTB == AirConditionData.CALCULATION_FAIL) {
				throw new TempCalErrorException(error_msg);
			}
		} else {
			throw new TempCalErrorException(error_msg);
		}
		AirConditionBean retAir = new AirConditionBean();
		retAir.setParamD(RetParmD);
		retAir.setParamI(RetParmI);
		retAir.setParmF(ParmF);
		retAir.setParmT(ParmT);
		retAir.setParmTb(RetParmTB);
		retAir.setParamTd(BaseDataUtil.decimalConvert(RetParmTD,1));
		return retAir;
	}

	// Air parameter calculate 4 I,D ----->φ,T,TB
	// return: FAirParmCalculate4 succeed true
	// fail false

	public static AirConditionBean FAirParmCalculate4(double ParmI, double ParmD) throws TempCalErrorException {
		double LVarPq, LVarPqb, RetParmF, RetParmT, RetParmTB,RetParmTD;
		RetParmT = (ParmI - 2500 * ParmD / 1000) / (1.01 + 1.84 * ParmD / 1000);
		RetParmTB = FIterationTBDb(RetParmT, ParmD);
		if (RetParmTB == AirConditionData.CALCULATION_FAIL) {
			throw new TempCalErrorException(error_msg);
		}
		LVarPq = FFormulaPq(ParmD);
		LVarPqb = FSearchTPqb(RetParmT);
		if (LVarPqb != AirConditionData.CALCULATION_FAIL) { // the saturable table
			RetParmF = LVarPq / LVarPqb;
		} else {
			throw new TempCalErrorException(error_msg);
		}
		RetParmTD=FFormulaTD(RetParmT,ParmD);
		AirConditionBean retAir = new AirConditionBean();
		retAir.setParamD(BaseDataUtil.decimalConvert(ParmD,2));
		retAir.setParamI(BaseDataUtil.decimalConvert(ParmI,2));
		retAir.setParmF(BaseDataUtil.decimalConvert(RetParmF,2));
		retAir.setParmT(BaseDataUtil.decimalConvert(RetParmT,2));
		retAir.setParmTb(BaseDataUtil.decimalConvert(RetParmTB,1));//TODO : 根据焓量计算出的湿球温度，会有误差暂时保持误差;后面系统计算确定没问题，直接修改为湿球温度
		retAir.setParamTd(BaseDataUtil.decimalConvert(RetParmTD,1));
		return retAir;

		/*function FAirParmCalculate4(ParmI, ParmD: Double; var RetParmF, RetParmT, RetParmTB: Double): boolean;
		var LVarPq, LVarPqb: Double;
		begin
		RetParmT := (ParmI - 2500 * ParmD / 1000) / (1.01 + 1.84 * ParmD / 1000);
		//Iteration
		if FIterationTBDb(RetParmT, ParmD, RetParmTB) = false then
				begin
		FAirParmCalculate4 := false;
		exit;
		end;
		LVarPq := FFormulaPq(ParmD);
		if FSearchTPqb(RetParmT, 100, LVarPqb) = True then //the saturable table
		RetParmF := LVarPq / LVarPqb
  else
		begin
		FAirParmCalculate4 := false;
		exit;
		end;
		FAirParmCalculate4 := true;
		end;*/
	}

	// Air parameter calculate 5 φ,I,T'(in),TB'(in)----->T(out),TB(out),D(out)
	// return: FAirParmCalculate5 succeed true
	// fail false

	@SuppressWarnings("unused")
	public static AirConditionBean FAirParmCalculate5(double ParmF, double ParmI, double ParmTIn, double ParmTBIn) throws TempCalErrorException {
		double LVarTb, LVarDb, LVarTx2, RetParmTOut = 0, RetParmTBOut, RetParmDOut;
		LVarTb = FIterationTI(RetParmTOut, ParmI, ParmF, Boolean.TRUE);
		if (LVarTb != AirConditionData.CALCULATION_FAIL) {
			LVarDb = fSearchTBDb(LVarTb);
			if (LVarDb != AirConditionData.CALCULATION_FAIL) {
				LVarTx2 = FIterationTI(RetParmTOut, ParmI, ParmF, false);
				if (LVarTx2 != AirConditionData.CALCULATION_FAIL) {
					RetParmTOut = LVarTx2;

				} else {
					throw new TempCalErrorException(error_msg) ;
				}
			} else {
				throw new TempCalErrorException(error_msg) ;
			}
		} else {
			throw new TempCalErrorException(error_msg) ;
		}
		throw new TempCalErrorException(error_msg) ;

	}

	/**
	 *
	 * @param airFlow
	 *            总风量
	 * @param freshAirRate
	 *            新风比
	 * @param freshAirTIN
	 *            新风干球温度
	 * @param freshAirTbIN
	 *            新风湿球温度
	 * @param returnAirTIN
	 *            回风干球温度
	 * @param returnTbIN
	 *            回风湿球温度
	 * @return
	 */
	public static AirConditionBean mixAirCalculate(double airFlow, double freshAirRate, double freshAirTIN,
			double freshAirTbIN, double returnAirTIN, double returnTbIN) throws TempCalErrorException {
		System.out.println(airFlow+"<>"+freshAirRate+"<>"+freshAirTIN+"<>"+freshAirTbIN+"<>"+returnAirTIN+"<>"+returnTbIN);
		AirConditionBean freshAir = new AirConditionBean();

		freshAir = FAirParmCalculate1(freshAirTIN, freshAirTbIN);

		if (freshAirRate == 100) {
			return freshAir;
		}

		AirConditionBean returnAir = new AirConditionBean();
		returnAir = FAirParmCalculate1(returnAirTIN, returnTbIN);
		if (freshAirRate == 0) {
			return returnAir;
		}

		double freshAirflow = airFlow * freshAirRate / 100;
		double returnAirflow = airFlow * ((100 - freshAirRate) / 100);
		double outParmI = (returnAirflow * returnAir.getParamI() + freshAirflow * freshAir.getParamI()) / airFlow;

		double outParmD = (returnAirflow * returnAir.getParamD() + freshAirflow * freshAir.getParamD()) / airFlow;

		return FAirParmCalculate4(outParmI, outParmD);

	}

	//增加计算公式
	public static Double FTemperatureConvert(Double ParmCF, Boolean ParmFlag) {
		if (ParmFlag) {
			return ParmCF * 9 / 5 + 32;
		} else {
			return (ParmCF - 32) * 5 / 9;
		}
	}

	/*
	 * //4-F G Heating Spiral Section T,TB,G,Tout---------->Q
	 * //1))Tin,TBin--->Iin,Din //2)Dout=Din //3)Tout,Dout--->Iout
	 * //4)ρ=(0.003484*101325)/[273.15+(Tin+Tout)/2] //5)Q=G*1.01*( Tin-Tout)* ρ
	 * 
	 * function FHeatSpiralSectTOut(ParmT, ParmTB, ParmG, ParmTout: Double; var
	 * RetParmQ: Double): Boolean; var LVarI, LVarF, LVarD, LVarIout, LVarDout,
	 * LVarDensity: Double; begin if FAirParmCalculate1(ParmT, ParmTB, LVarF,
	 * LVarI, LVarD) then //1) begin LVarDout := LVarD; //2) LVarIout :=
	 * FFormulaIb(LVarDout, ParmTout); //3) LVarDensity :=
	 * FFormulaDensity(ParmT, ParmTout); //4) RetParmQ := ParmG * 1.01 *
	 * (ParmTout - ParmT) * LVarDensity; //5) FHeatSpiralSectTOut := true; end
	 * else FHeatSpiralSectTOut := false; end;
	 * 
	 * //4-F G Heating Spiral Section T,TB,G,Q---------->Tout
	 * //1))Tin,TBin--->Iin,Din //2)ρ=(0.003484*101325)/[273.15+(Tin+Tout)/2]
	 * //3)Q=G*1.01*( Tin-Tout)* ρ // G*1.01*(Tin-Tout)*
	 * (0.003484*101325)/[273.15+(Tin+Tout)/2] // = Q // Tout=
	 * (273.15*Q+Q*Tin/2+0.003484*101325*1.01*G*Tin)/ //
	 * (0.003484*101325*1.01*G-Q/2)
	 * 
	 * function FHeatSpiralSectQ(ParmT, ParmTB, ParmG, ParmQ: Double; var
	 * RetParmTout: Double): Boolean; var LVarI, LVarF, LVarD: Double; begin if
	 * FAirParmCalculate1(ParmT, ParmTB, LVarF, LVarI, LVarD) then //1) begin
	 * //3) RetParmTout := (273.15 * ParmQ + ParmQ * ParmT / 2 + 0.003484 *
	 * 101325 * 1.01 * ParmG * ParmT) / (0.003484 * 101325 * 1.01 * ParmG -
	 * ParmQ / 2); FHeatSpiralSectQ := true; end else FHeatSpiralSectQ := false;
	 * end;
	 * 
	 * //4-F Steam Coil section pressure
	 * 
	 * function FSteamCoilSectPres(ParmG, ParmTin, ParmTout: Double; var
	 * RetParmPres: Double): Boolean; begin FSteamCoilSectPres := true; end;
	 * 
	 * //4-F Steam Coil Section resistance
	 * 
	 * function FSteamCoilSectResi(ParmG: Double; var RetParmResi): Boolean;
	 * begin FSteamCoilSectResi := true; end;
	 * 
	 * //Centigrade <------------->fahrenheit //ParmFlag:true Centigrade
	 * ------------->fahrenheit // false fahrenheit ------------->Centigrade
	 * 
	 * function FTemperatureConvert(ParmCF: Double; ParmFlag: Boolean): Double;
	 * begin if ParmFlag then FTemperatureConvert := ParmCF * 9 / 5 + 32 else
	 * FTemperatureConvert := (ParmCF - 32) * 5 / 9; end;
	 */

}
