package com.carrier.ahu.common.enums;

/**
 * 系统支持的语言枚举
 * 
 * @author Simon
 *
 */
public enum LanguageEnum {
	Chinese("zh-CN", "中文", "Chinese"),

	English("en-US", "英文", "English"),

	Malysia("Malysia", "马来西亚", "Malysia");

	private String id;// 中文名称
	private String cname;// 中文名称
	private String ename;// 英文名称

	LanguageEnum(String id, String cname, String ename) {
		this.id = id;
		this.cname = cname;
		this.ename = ename;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public static LanguageEnum byId(String id) {
		for (LanguageEnum enu : values()) {
			if (enu.getId().equals(id)) {
				return enu;
			}
		}
		return null;
	}

	public static LanguageEnum byName(String ename) {
		for (LanguageEnum enu : values()) {
			if (enu.getEname().equals(ename)) {
				return enu;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.name();
	}
}
