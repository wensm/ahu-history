package com.carrier.ahu.common.entity;

import java.util.Map;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.UserConfigEnum;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

import lombok.Data;

@Data
public class UserConfigParam {

    private Map<String, Object> mapConfig;
    private String calNumber;
    private Double maximumCoilFaceVelocity;

    public String getCalNumber() {
        UserMeta userMeta = AHUContext.getUserMeta(UserMetaEnum.COLD_COOLING_SELECTION_SIZE);
        if (!EmptyUtil.isEmpty(userMeta)) {
            return userMeta.getMetaValue();
        }
        return calNumber;
    }
    
    public String getExportMode() {
    	UserMeta userMeta = AHUContext.getUserMeta(UserMetaEnum.EXPORT_MODE);
        if (!EmptyUtil.isEmpty(userMeta)) {
            return userMeta.getMetaValue();
        }
        return calNumber;
    }

    public Double getMaximumCoilFaceVelocity() {
        if (!EmptyUtil.isEmpty(this.mapConfig)) {
            String maximumCoilFaceVelocityKey = UserConfigEnum.MAXIMUM_COIL_FACE_VELOCITY.getCode();
            if (mapConfig.containsKey(maximumCoilFaceVelocityKey)) {//最大迎面风速
                String value = mapConfig.get(maximumCoilFaceVelocityKey).toString();
                if (!EmptyUtil.isEmpty(value)) {
                    return BaseDataUtil.stringConversionDouble(value);
                }
            }
        }
        return null;
    }
}
