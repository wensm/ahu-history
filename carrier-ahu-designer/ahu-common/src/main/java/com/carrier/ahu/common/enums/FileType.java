package com.carrier.ahu.common.enums;

/**
 * File types used in system.
 * 
 * Created by Braden Zhou on 2018/05/30.
 */
public enum FileType {

    XLSX(".xlsx"), XLS(".xls");

    private String extension;

    private FileType(String extension) {
        this.extension = extension;
    }

    public String extension() {
        return this.extension;
    }

}
