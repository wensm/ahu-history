package com.carrier.ahu.common.entity;

import com.carrier.ahu.po.AbstractPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Role extends AbstractPo implements Serializable{
    private static final long serialVersionUID = 3330138902489258965L;
    @Id
    @GeneratedValue
    String roleId;//角色ID
    String roleName;//角色名称
    String roleDesc;//角色描述
}
