package com.carrier.ahu.report;

import java.util.List;

import com.carrier.ahu.common.entity.GroupInfo;

import lombok.ToString;

@ToString
public class ExportGroup extends GroupInfo {
	private static final long serialVersionUID = 3851609044956690872L;
	private List<ExportUnit> units;

	public List<ExportUnit> getUnits() {
		return units;
	}

	public void setUnits(List<ExportUnit> units) {
		this.units = units;
	}

}