package com.carrier.ahu.common.util;

import static com.carrier.ahu.constant.CommonConstant.SYS_DOT_SEPARATOR;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_NUMBER_0;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by Braden Zhou on 2019/01/03.
 */
public class VersionUtils {

    /**
     * Compare two versions, as format 1.0.0.
     * 
     * @param newVersion
     * @param oldVersion
     * @return
     */
    public static int compare(String newVersion, String oldVersion) {
        if (!EmptyUtil.isEmpty(newVersion) && !EmptyUtil.isEmpty(oldVersion)) {
            String[] newVersionChunks = newVersion.split(SYS_DOT_SEPARATOR);
            String[] oldVersionChunks = oldVersion.split(SYS_DOT_SEPARATOR);
            int size = newVersionChunks.length > oldVersionChunks.length ? newVersionChunks.length
                    : oldVersionChunks.length;

            for (int i = 0; i < size; i++) {
                String newVersionChunk = i < newVersionChunks.length ? newVersionChunks[i] : SYS_STRING_NUMBER_0;
                String oldVersionChunk = i < oldVersionChunks.length ? oldVersionChunks[i] : SYS_STRING_NUMBER_0;
                if (!newVersionChunk.equals(oldVersionChunk)) {
                    if (NumberUtils.toInt(newVersionChunk) > NumberUtils.toInt(oldVersionChunk)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            }
            return 0;
        }
        return -1;
    }

}
