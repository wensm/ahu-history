package com.carrier.ahu.resistance;

/**
 * Created by liangd4 on 2017/9/11.
 * 新回排风段
 */
public class RepeatingWindRes {

    public double getResistance() {
        //默认固定值5，不可以修改
        return 5.00;
    }

}
