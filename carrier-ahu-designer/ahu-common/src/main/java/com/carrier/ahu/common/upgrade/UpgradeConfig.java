package com.carrier.ahu.common.upgrade;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.constant.CommonConstant;

/**
 * Created by Braden Zhou on 2018/07/18.
 */
public class UpgradeConfig {

    public static String getUpdateFileName(String release, String factory, String lang, String updateVersion) {
        if (CommonConstant.SYS_UPGRADE_RELEASE_MINOR.equals(release)) {
            return String.format("%scarrier-ahu-update-%s-%s.zip", CommonConstant.SYS_UPGRADE_AHU_UPGRADE_FOLDER, factory, updateVersion);
        } else if (CommonConstant.SYS_UPGRADE_RELEASE_MAJOR.equals(release)) {
            return String.format("%sCarrierAHU-%s-%s-%s.exe", CommonConstant.SYS_UPGRADE_AHU_UPGRADE_FOLDER, lang, factory, updateVersion);
        }
        return null;
    }

    public static String getLanguageCode(String lang) {
        LanguageEnum languageEnum = LanguageEnum.byId(lang);
        if (languageEnum != null && !LanguageEnum.Chinese.equals(languageEnum)) {
            return CommonConstant.SYS_LANGUAGE_EN;
        }
        return CommonConstant.SYS_LANGUAGE_CN;
    }

    public static boolean isMajorRelease(String ahuVersion, String updateVersion) {
        int ahuMajorVersion = NumberUtils.toInt(ahuVersion.substring(0, ahuVersion.indexOf(".")));
        int updateMajorVersion = NumberUtils.toInt(updateVersion.substring(0, updateVersion.indexOf(".")));
        return (ahuMajorVersion < updateMajorVersion);
    }

}
