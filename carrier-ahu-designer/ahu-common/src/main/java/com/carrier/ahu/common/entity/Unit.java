package com.carrier.ahu.common.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.po.AbstractPo;
import com.carrier.ahu.vo.SysConstants;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * ahu实体
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class Unit extends AbstractPo implements Serializable {

    private static final long serialVersionUID = -4242243289899265181L;
    @Id
    String unitid;// 主键ID
    String pid;// 项目ID
    String groupId;// 组ID
    String unitNo;// 机组编号
    String drawingNo;// 图纸编号
    String series;// 机组型号
    String panelSeries;// 面板变形机组型号
    String needReloadThreeView;// 面板三视图是否需要重新加载
    String product;
    Short mount;// 数量
    String name;// AHU名称
    Double weight;// 重量
    Double price;// 价格
    String modifiedno;
    String paneltype;// （目前用于传递 “段连接清单类型”参数）
    Double nsprice;
    Double cost;
    Double lb;
    byte[] drawing;
    String customerName;// 客户名称
    String groupCode;
    String layoutJson; // The layout of AHU, it would be different for different style ahu
    @Transient
    List<Part> parts;
    
    public void setUnitNo(String unitNo) {
        this.unitNo = StringUtils.leftPad(unitNo, SysConstants.UNIT_NO_LENGTH, "0");
        if (StringUtils.isNotBlank(unitNo)) {
            try {
                setDrawingNo("" + Integer.parseInt(unitNo));
            } catch (Exception e) {
            }
        }
    }

}