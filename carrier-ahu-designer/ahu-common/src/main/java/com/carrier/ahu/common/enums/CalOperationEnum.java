package com.carrier.ahu.common.enums;

public enum CalOperationEnum {
	
	COMFIRM(0),
	BATCH(1);
	
	//set batch run as default
	private int id;
	
	
	CalOperationEnum(int id){
		this.setId(id);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	

}
