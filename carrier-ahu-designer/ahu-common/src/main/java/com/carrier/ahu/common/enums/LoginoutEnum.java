package com.carrier.ahu.common.enums;

/**
 *
 * Created by Wen zhengtao on 2017/6/20.
 */
public enum LoginoutEnum {
    LOGIN("登入/登录"),
    LOGOUT("登出/退出/注销");

    private String cnName;//中文名称

    LoginoutEnum(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return this.cnName;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
