package com.carrier.ahu.util;

import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.unit.BaseDataUtil;

public class CoilHeatCalUtil {

    /**
     * Get heat quality by template-input
     *
     * @param airFlow  input air flow
     * @param parmTin  input TB
     * @param parmTBin input DB
     * @param parmTout output TB
     * @return
     */

    public static CoilHeatAirConditionBean calHeatByOutT(double airFlow, double parmTin, double parmTBin, double parmTout) throws TempCalErrorException {
        CoilHeatAirConditionBean result = new CoilHeatAirConditionBean();
        result.setOutputT(BaseDataUtil.decimalConvert(parmTout, 0));
        AirConditionBean input = AirConditionUtils.FAirParmCalculate1(parmTin, parmTBin);
        double ldVarDout = input.getParamD();
        double ldVarIout = BaseDataUtil.decimalConvert(AirConditionUtils.FFormulaIb(ldVarDout, parmTout), 1);
        double ldVardensity = AirConditionUtils.FFormulaDensity(input.getParmT(), parmTout);
        result.setHeatQ(Math.round(airFlow / 3600  * (ldVarIout - BaseDataUtil.decimalConvert(input.getParamI(), 1)) * ldVardensity));
        AirConditionBean output = AirConditionUtils.FAirParmCalculate2(parmTout, input.getParamD());
        result.setParmT(BaseDataUtil.decimalConvert(output.getParmT(), 2));
        result.setParmTb(BaseDataUtil.decimalConvert(output.getParmTb(), 2));
        result.setParmF(BaseDataUtil.decimalConvert(output.getParmF(), 0));
        return result;

    }

    /**
     * Get heat quality by HeatQuality-input
     *
     * @param airFlow  input air flow
     * @param parmTin  input TB
     * @param parmTBin input DB
     * @param heatQ    output heatQ
     * @return
     */
    public static CoilHeatAirConditionBean calHeatByHeatQ(double airFlow, double parmTin, double parmTBin, double heatQ) throws TempCalErrorException {
        CoilHeatAirConditionBean result = new CoilHeatAirConditionBean();
        result.setHeatQ(BaseDataUtil.decimalConvert(heatQ, 0));
        AirConditionBean input = AirConditionUtils.FAirParmCalculate1(parmTin, parmTBin);
        double retTout = (input.getParamI() + 273.15 * heatQ / (0.003484 * 101325 * airFlow / 3600) + input.getParmT() * heatQ / (0.003484 * 101325 * 2 * airFlow / 3600)
                - 2500 * input.getParamD() / 1000) / ((1.01 + 1.84 * input.getParamD() / 1000) - heatQ / (airFlow / 3600 * 0.003484 * 101325 * 2));
        result.setOutputF(BaseDataUtil.decimalConvert(input.getParmF(), 0));//批量计算期间使用到
        result.setOutputT(BaseDataUtil.decimalConvert(retTout,2));
        AirConditionBean output = AirConditionUtils.FAirParmCalculate2(retTout, input.getParamD());
        result.setParmT(BaseDataUtil.decimalConvert(output.getParmT(), 2));
        result.setParmTb(BaseDataUtil.decimalConvert(output.getParmTb(), 2));
        result.setParmF(BaseDataUtil.decimalConvert(output.getParmF(), 0));
        return result;
    }

    public static void main(String[] args) {
        try {
            calHeatByHeatQ(2000.0, 27.0, 19.5, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
