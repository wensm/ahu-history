package com.carrier.ahu.report;

import com.carrier.ahu.common.entity.Partition;

import lombok.ToString;

@ToString
public class ExportPartition extends Partition {
	private static final long serialVersionUID = -3386215885060435303L;
}