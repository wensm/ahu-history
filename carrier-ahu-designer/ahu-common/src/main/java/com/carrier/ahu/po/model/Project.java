package com.carrier.ahu.po.model;

import com.carrier.ahu.common.enums.AhuStatusEnum;

/**
 * 
 * Describe a project info
 * 
 * @author JL
 *
 */
public class Project extends AbstractModel {

	private static final long serialVersionUID = 1L;
	private Group[] groups;
	public void setGroups(Group[] groups) {
		this.groups = groups;
	}

	public void setAhus(Ahu[] ahus) {
		this.ahus = ahus;
	}

	private Ahu[] ahus;
	private AhuStatusEnum state;

	public Group[] getGroups() {
		return groups;

	}

	public Ahu[] getAhus() {
		return ahus;
	}

	public AhuStatusEnum getState() {
		return state;
	}

	public void setState(AhuStatusEnum state) {
		this.state = state;
	}

}
