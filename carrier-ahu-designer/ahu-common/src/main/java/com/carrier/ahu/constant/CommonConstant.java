package com.carrier.ahu.constant;

import java.io.File;

import com.carrier.ahu.vo.SysConstants;
/**
 * 
 * 一、Json配置常量规则：
 *  1.段名_属性名_值 
 *  JSON
 * 	例如：meta.section.coolingCoil.eliminator可以用常量JSON_COOLINGCOIL_ELIMINATOR替代。
 *
 * 二、parameter1.3常量规则：
 *  1.段名/AHU_属性名 
 *  METASEXON = "meta.section" 
 *  METAHU = "meta.ahu" 
 *  例如：meta.section.fan.outlet可以用常量METASEXON_FAN_OUTLET替代
 *  例如：meta.ahu.product可以用常量METAHU_PRODUCT替代 
 * 三、其他常量规则： 
 * 1.标点符号
 * 2.日志规则： LOG_EXPLANATION
 * 
 *
 */
public class CommonConstant{
	/*
	 * ####################JSON配置常量区####################
	 */
	//sys
	public static final String ROLE_Design = "Design";

	//unit
	public static final String JSON_UNIT_NAME_AHU = "AHU";
	public static final String JSON_UNIT_PANELTYPE_STANDARD = "Standard";//标准
	public static final String JSON_UNIT_PANELTYPE_FOREPART = "Forepart";//盘管前段连接
	public static final String JSON_UNIT_PANELTYPE_POSITIVE = "Positive";//正压
	public static final String JSON_UNIT_PANELTYPE_VERTICAL = "Vertical";//立式
	
	//AHU
	public static final String JSON_AHU_EXSKINM_GI = "GI";
	public static final String JSON_AHU_EXSKINM_PREPAINTEDSTEEL = "prePaintedSteal";
	public static final String JSON_AHU_EXSKINM_STAINLESS = "stainless";
	public static final String JSON_AHU_EXSKINM_HDPPREPAINTEDSTEEL = "HDPPrePaintedSteel";
	public static final String JSON_AHU_SKINTHICKNESS_0_5MM = "0.5mm";
	public static final String JSON_AHU_SKINTHICKNESS_0_8MM = "0.8mm";
	public static final String JSON_AHU_SKINTHICKNESS_1_0MM = "1.0mm";
	public static final String JSON_AHU_SKINTHICKNESS_1_2MM = "1.2mm";
	public static final String JSON_AHU_MATERIAL_GISTEEL = "GS";
	public static final String JSON_AHU_MATERIAL_STAINLESSSTEEL = "SS";
	public static final String JSON_AHU_MATERIAL_PAINTEDCOLDROLLEDSTEEL = "PS";
	public static final String JSON_AHU_MATERIAL_ALMESH = "AM";
	public static final String JSON_AHU_MATERIAL_PLASTIC = "PC";
	public static final String JSON_AHU_MATERIAL_ALUMINUM = "AI";
	public static final String JSON_AHU_MATERIAL_NONE = "NA";
	public static final String JSON_AHU_SEASON_S = "S";
	public static final String JSON_AHU_SEASON_W = "W";
	public static final String JSON_AHU_SEASON_SUMMER = "Summer";
	public static final String JSON_AHU_SEASON_WINTER = "Winter";
	public static final String JSON_AHU_SERIAL_0608 = "0608";
	public static final String JSON_AHU_SERIAL_0711 = "0711";
	
	//Filter
	public static final String JSON_FILTER_FILTEREFFICIENCY_Z2 = "Z2";
	public static final String JSON_FILTER_FILTEREFFICIENCY_C1 = "C1";
	public static final String JSON_FILTER_FILTEREFFICIENCY_C2 = "C2";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F5 = "F5";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F6 = "F6";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F7 = "F7";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F8 = "F8";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F9 = "F9";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F7G = "F7G";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F8G = "F8G";
	public static final String JSON_FILTER_FILTEREFFICIENCY_F9G = "F9G";
	public static final String JSON_FILTER_MEDIALOADING_FRONTLOADING = "frontLoading";
	public static final String JSON_FILTER_FILTERF_B = "B";
	public static final String JSON_FILTER_FILTERF_P = "P";
	public static final String JSON_FILTER_FILTEROPTIONS_NOFILTER = "No Filter";
	public static final String JSON_FILTER_FILTEROPTIONS_NO = "No";
	public static final String JSON_FILTER_PRESSUREGUAGE_PDS = "pressureDifferenceSwitch";//压差开关
	public static final String JSON_FILTER_PRESSUREGUAGE_PDPG = "Point Differential Pressure Guage";//指针式压差计
	public static final String JSON_FILTER_PRESSUREGUAGE_WO = "w/o";//无

	//CombinedFilter
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDS = "pressureDifferenceSwitch";//压差开关
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDPG = "Point Differential Pressure Guage";//指针式压差计
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO = "w/o";//无
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDS = "pressureDifferenceSwitch";//压差开关
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDPG = "Point Differential Pressure Guage";//指针式压差计
	public static final String JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO = "w/o";//无

	//Hepafilter
	public static final String JSON_HEPAFILTER_PRESSUREGUAGE_PDS = "pressureDifferenceSwitch";//压差开关
	public static final String JSON_HEPAFILTER_PRESSUREGUAGE_PDPG = "Point Differential Pressure Guage";//指针式压差计
	public static final String JSON_HEPAFILTER_PRESSUREGUAGE_WO = "w/o";//无

	//mix
	public static final String JSON_MIX_OUTTYPE_T = "T";//T:顶部出风
	public static final String JSON_MIX_OUTTYPE_A = "A";//A:后出风
	public static final String JSON_MIX_OUTTYPE_L = "L";//L:左侧出风
	public static final String JSON_MIX_OUTTYPE_R = "R";//R:右侧出风
	public static final String JSON_MIX_OUTTYPE_F = "F";//F:底部出风
	public static final String JSON_MIX_DOORTYPE_DN = "DN";//DN:开门无观察窗 
	public static final String JSON_MIX_DOORTYPE_DY = "DY";//DY:开门有观察窗
	public static final String JSON_MIX_DOORTYPE_ND = "ND";//ND:不开门
	
	//fan
	public static final String JSON_FAN_FANMODEL_FC = "FC";
	public static final String JSON_FAN_FANMODEL_FDA = "FDA";
	public static final String JSON_FAN_FANMODEL_BDB = "BDB";
	public static final String JSON_FAN_FANMODEL_ADA = "ADA";
	public static final String JSON_FAN_FANMODEL_KTQ = "KTQ";
	public static final String JSON_FAN_FANMODEL_BC = "BC";
	public static final String JSON_FAN_FANMODEL_KTH = "KTH";
	public static final String JSON_FAN_FANMODEL_YFH_G = "YFH(G)";
	public static final String JSON_FAN_FANMODEL_K = "K";
	public static final String JSON_FAN_FANSUPPLIER_A = "A";
	public static final String JSON_FAN_FANSUPPLIER_Y = "Y";
	public static final String JSON_FAN_FANSUPPLIER_KRUGER = "kruger";
	public static final String JSON_FAN_SUPPLIER_DEFAULT = "default";
	public static final String JSON_FAN_SUPPLIER_WOLONG_KEY = "wolong";
	public static final String JSON_FAN_SUPPLIER_DONGYUAN = "Dongyuan";
	public static final String JSON_FAN_SUPPLIER_ABB = "ABB";
	public static final String JSON_FAN_SUPPLIER_SIMENS = "Simens";
	public static final String JSON_FAN_OUTLET_WWK = "wwk";
	public static final String JSON_FAN_OUTLET_FOB = "fob";
	public static final String JSON_FAN_OUTLET_F = "f";
	public static final String JSON_FAN_OUTLET_B = "b";
	public static final String JSON_FAN_TYPE_A = "A";//三级能效机
	public static final String JSON_FAN_TYPE_B = "B";//防爆电机
	public static final String JSON_FAN_TYPE_C = "C";//双速电机
	public static final String JSON_FAN_TYPE_D = "D";//变频电机
	public static final String JSON_FAN_TYPE_E = "E";//二级能效机
	public static final String JSON_FAN_TYPE_NUM_1 = "1";//二级能效机
	public static final String JSON_FAN_TYPE_NUM_2 = "2";//三级能效机
	public static final String JSON_FAN_TYPE_NUM_3 = "3";//防爆电机
	public static final String JSON_FAN_TYPE_NUM_4 = "4";//双速电机
	public static final String JSON_FAN_TYPE_NUM_5 = "5";//变频电机（IC416）
	public static final String JSON_FAN_TYPE_NUM_6 = "6";//变频电机直联（IC416）
	public static final String JSON_FAN_SERIES = "series";
	public static final String JSON_FAN_MODEL = "model";
	public static final String JSON_FAN_EFFICIENCY = "efficiency";
	public static final String JSON_MIX_DAMPEROUTLET_ED = "ED";//ED:电动风阀
	public static final String JSON_MIX_DAMPEROUTLET_MD = "MD";//MD:手动风阀
	public static final String JSON_MIX_DAMPEROUTLET_FD = "FD";//FD:法兰
	public static final String JSON_MIX_DAMPEROUTLET_FD_CN = "法兰";//FD:法兰
	public static final String JSON_FAN_STARTSTYLE_NO = "no";//"无启动柜",
	public static final String JSON_FAN_STARTSTYLE_VFD = "vfd";//"VFD变频启动柜用户安装",
	public static final String JSON_FAN_STARTSTYLE_STAR = "star";//"星-三角启动-用户安装",
	public static final String JSON_FAN_STARTSTYLE_DIRECT = "direct";//"直接启动-用户安装",
	public static final String JSON_FAN_STARTSTYLE_ENBEDDED = "enbedded";//"嵌入式VFD变频启动柜"
	
	//coil
	public static final String JSON_COIL_ZUA_0711 = "ZUA_0711";
	public static final String JSON_COIL_ZUA_C_R = "ZUA_C_R";
	public static final String JSON_COIL_ZUA_C_F = "ZUA_C_F";
	public static final String JSON_COIL_ZUA_C_O = "ZUA_C_O";
	public static final String JSON_COIL_TH = "th";
	public static final String JSON_COIL_PRESSURE = "pressure";
	
	//coolinigCoil
	public static final String JSON_COOLINGCOIL_FINTYPE_COPPER = "copper";
	public static final String JSON_COOLINGCOIL_FINTYPE_PROCOATEDAL = "procoatedAl";
	public static final String JSON_COOLINGCOIL_FINTYPE_AL = "al";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_ALMESH = "alMesh";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_NONE = "none";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_PLASTIC = "plastic";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_STAINLESS = "stainless";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_GISTEEL = "giSteel";
	public static final String JSON_COOLINGCOIL_ELIMINATOR_ALUMINUM = "aluminum";
	public static final String JSON_COOLINGCOIL_COILMODEL_12_7 = "12.7";
	public static final String JSON_COOLINGCOIL_COILMODEL_9_52 = "9.52";
	
	//HeatRecycle
	public static final String JSON_HEATRECYCLE_POWER_EN = "POWER_EN";
	//wheelheatrecycle
	public static final String JSON_WHEELHEATRECYCLE_MODEL_1 = "1";//分子筛
	public static final String JSON_WHEELHEATRECYCLE_MODEL_2 = "2";//硅胶
	public static final String JSON_WHEELHEATRECYCLE_MODEL_3 = "3";//纯铝
	public static final String JSON_WHEELHEATRECYCLE_BRAND_A = "A";//标准供应商热转轮
	public static final String JSON_WHEELHEATRECYCLE_BRAND_B = "B";//百瑞热转轮
	public static final String JSON_WHEELHEATRECYCLE_BRAND_C = "C";//毅科热转轮
	public static final String JSON_PLATEHEATRECYCLE_WHEELDEPTH_1 = "1";//200
	public static final String JSON_PLATEHEATRECYCLE_WHEELDEPTH_1_VAl = "200";//200
	public static final String JSON_PLATEHEATRECYCLE_WHEELDEPTH_2 = "2";//270
	public static final String JSON_PLATEHEATRECYCLE_WHEELDEPTH_2_VAl = "270";//270


	//attenuator
	public static final String JSON_ATTENUATOR_DEADENING_A = "A";
	
	/*
	 * ####################parameter1.3常量区####################
	 */
	//meta.section
	public static final String METASEXON_SECTION = "section.";
	public static final String METASEXON_META_PREFIX = "meta.";
	public static final String METASEXON_PREFIX = "meta.section.";
	public static final String METASEXON_AIRDIRECTION = "meta.section.airDirection";//风向
	public static final String METASEXON_COMPLETED = "meta.section.completed";//确认状态
	
	//AHU
	public static final String METAHU_AHU = "ahu";
	public static final String METAHU_AHU_NAME = "ahu.";
	public static final String METAHU_PREFIX = "meta.ahu.";
	public static final String METAHU_PRODUCT = "meta.ahu.product";
	public static final String METAHU_SAIRVOLUME = "meta.ahu.sairvolume";
	public static final String METAHU_SEXTERNALSTATIC = "meta.ahu.sexternalstatic";//机外静压(送)
	public static final String METAHU_EAIRVOLUME = "meta.ahu.eairvolume";
	public static final String METAHU_EEXTERNALSTATIC = "meta.ahu.eexternalstatic";//机外静压(回)
	public static final String METAHU_ISPRERAIN = "meta.ahu.ISPRERAIN";
	public static final String METAHU_SERIAL = "meta.ahu.serial";
	public static final String METAHU_DOORORIENTATION = "meta.ahu.doororientation";
	public static final String METAHU_PIPEORIENTATION = "meta.ahu.pipeorientation";
	public static final String METAHU_HEIGHT = "meta.ahu.height";
	public static final String METAHU_HEIGHT_NAME = "Height";
	public static final String METAHU_WIDTH = "meta.ahu.width";
	public static final String METAHU_SVELOCITY = "meta.ahu.svelocity";
	public static final String METAHU_EVELOCITY = "meta.ahu.evelocity";
	public static final String METAHU_ASSIS = "assis";
	public static final String METAHU_ASSIS_FACESPEED = "assis.section.ahu.facespeed";
	public static final String METAHU_ASSIS_HEIGHT = "assis.section.ahu.height";
	public static final String METAHU_ASSIS_POSITION = "assis.section.position";
	public static final String METAHU_EXSKINM = "meta.ahu.exskinm";
	public static final String METAHU_EXSKINW = "meta.ahu.exskinw";
	public static final String METAHU_INSKINM = "meta.ahu.inskinm";
	public static final String METAHU_INSKINW = "meta.ahu.inskinw";
	public static final String METAHU_UNITCONFIG = "meta.ahu.unitConfig";
	public static final String METAHU_VOLTAGE = "meta.ahu.voltage";
	public static final String METAHU_WEIGHT1 = "meta.ahu.Weight1";
	public static final String METAHU_AHUSIZE = "meta.ahu.ahusize";
	public static final String METAHU_AIRVOLUME = "meta.ahu.airVolume";
	public static final String METAHU_VELOCITY = "meta.ahu.velocity";
	public static final String METAHU_AHUHEIGHT = "ahuHeight";
	public static final String METAHU_FACESPEED_NAME = "faceSpeed";
	public static final String METAHU_FACE_SPEED = "Face Speed";

	//default
	public static final String META_DEFAULT_SINDRYBULBT = "meta.meta.defaultPara.SInDryBulbT";
	public static final String META_DEFAULT_SINWETBULBT = "meta.meta.defaultPara.SInWetBulbT";
	public static final String META_DEFAULT_SINRELATIVET = "meta.meta.defaultPara.SInRelativeT";
	public static final String META_DEFAULT_SNEWDRYBULBT = "meta.meta.defaultPara.SNewDryBulbT";
	public static final String META_DEFAULT_SNEWWETBULBT = "meta.meta.defaultPara.SNewWetBulbT";
	public static final String META_DEFAULT_SNEWRELATIVET = "meta.meta.defaultPara.SNewRelativeT";
	public static final String META_DEFAULT_WINDRYBULBT = "meta.meta.defaultPara.WInDryBulbT";
	public static final String META_DEFAULT_WINWETBULBT = "meta.meta.defaultPara.WInWetBulbT";
	public static final String META_DEFAULT_WINRELATIVET = "meta.meta.defaultPara.WInRelativeT";
	public static final String META_DEFAULT_WNEWDRYBULBT = "meta.meta.defaultPara.WNewDryBulbT";		
	public static final String META_DEFAULT_WNEWWETBULBT = "meta.meta.defaultPara.WNewWetBulbT";
	public static final String META_DEFAULT_WNEWRELATIVET = "meta.meta.defaultPara.WNewRelativeT";
	public static final String META_DEFAULT_MAXWPD = "meta.meta.defaultPara.MaxWPD";
	public static final String META_DEFAULT_AIRVOLUMEUNIT = "meta.meta.defaultPara.AirVolumeUnit";
	public static final String META_DEFAULT_HEADERMATERIAL = "meta.meta.defaultPara.HeaderMaterial";
	public static final String META_DEFAULT_INTERNALSKINTHICKNESS = "meta.meta.defaultPara.InternalSkinThickness";
	public static final String META_DEFAULT_OUTSIDESKINTHICKNESS = "meta.meta.defaultPara.OutsideSkinThickness";
	public static final String META_DEFAULT_POWER = "meta.meta.defaultPara.Power";
	public static final String META_DEFAULT_MOTORSUPPLIER = "meta.meta.defaultPara.MotorSupplier";
	
	//common
	public static final String METACOMMON_RESISTANCE = "meta.section.resistance";
	public static final String METACOMMON_WEIGHT = "meta.section.weight";
	public static final String METACOMMON_NOSTANDARD = "meta.section.NoStandard";
	public static final String METACOMMON_NOSTANDARD_NAME = "NoStandard";
	public static final String METACOMMON_POSTFIX_NOSTANDARD = ".NoStandard";
	public static final String METACOMMON_UNREGULAR = "Unregular";
	public static final String METACOMMON_COMMON = "meta.common";
	public static final String METACOMMON_NONSTANDARD = "meta.nonStandard";
	public static final String METACOMMON_DEFAULTPARA = "meta.defaultPara";
	public static final String METACOMMON_PREFIX_DEFAULTPARA = "meta.defaultPara.";
	public static final String METACOMMON_PRICE = "meta.section.Price";
	public static final String METACOMMON_POSTFIX_PRICE = ".Price";
	public static final String METACOMMON_PRICE_NAME = "Price";
	public static final String METACOMMON_MEMO = "meta.section.Memo";
	public static final String METACOMMON_POSTFIX_MEMO = ".MEMO";
	public static final String METACOMMON_MEMO_NAME = "MEMO";
	public static final String METACOMMON_META = "meta";
	public static final String METACOMMON_SEASON = "meta.section.Season";
	public static final String METACOMMON_POSTFIX_SEASON = ".Season";
	public static final String METACOMMON_SEASON_NAME = "Season";
	public static final String METACOMMON_NS = "ns";
	public static final String METACOMMON_SECTIONL = "sectionL";
	public static final String METACOMMON_POSTFIX_SECTIONL = ".sectionL";
	public static final String METACOMMON_LENGTH_NAME = "Section Length";
	public static final String METACOMMON_POSTFIX_CALSECTIONL = ".calSectionL";
	public static final String METACOMMON_POSTFIX_DEFAULTSECTIONL = ".defaultSectionL";
	public static final String METACOMMON_POSTFIX_RESISTANCE = ".Resistance";
	public static final String METACOMMON_COMPLETE_NAME = "Complete";
	public static final String METACOMMON_POSTFIX_WINRELATIVET = ".WInRelativeT";
	public static final String METACOMMON_POSTFIX_SINRELATIVET = ".SInRelativeT";
	public static final String METACOMMON_POSTFIX_ROWNUM = ".RowNum";
	public static final String METACOMMON_POSTFIX_GROUPC = ".GroupC";
	public static final String METACOMMON_POSTFIX_FILTERARRANGE = ".FilterArrange";
	public static final String METACOMMON_POSTFIX_VAPORPRESSURE = ".VaporPressure";
	public static final String METACOMMON_POSTFIX_WATERQUANTITY = ".WaterQuantity";
	public static final String METACOMMON_POSTFIX_WATERFLOW = ".WaterFlow";
	public static final String METACOMMON_POSTFIX_HEATRECOVERYEFFICIENCY = ".HeatRecoveryEfficiency";
	public static final String METACOMMON_POSTFIX_HEATRECYCLEDETAIL = ".HeatRecycleDetail";
	public static final String METACOMMON_POSTFIX_SECTIONSIDEL = ".SectionSideL";//截面边长
	public static final String METACOMMON_POSTFIX_HEATEXCHANGERL = ".HeatExchangerL";//热交换器长度
	public static final String METACOMMON_POSTFIX_DINITIALPD = ".dinitialPD";
	public static final String METACOMMON_POSTFIX_DEVERAGEPD = ".deveragePD";
	public static final String METACOMMON_POSTFIX_DFINALPD = ".dfinalPD";
	public static final String METACOMMON_POSTFIX_INITIALPDP = ".initialPDP";
	public static final String METACOMMON_POSTFIX_EVERAGEPDP = ".everagePDP";
	public static final String METACOMMON_POSTFIX_FINALPDP = ".finalPDP";
	public static final String METACOMMON_POSTFIX_INITIALPDB = ".initialPDB";
	public static final String METACOMMON_POSTFIX_EVERAGEPDB = ".everagePDB";
	public static final String METACOMMON_POSTFIX_FINALPDB = ".finalPDB";
	public static final String METACOMMON_POSTFIX_DRIFTELIMINATORRESISTANCE = ".driftEliminatorResistance";
	public static final String METACOMMON_POSTFIX_THICKNESS = ".thickness";
	public static final String METACOMMON_POSTFIX_OUTLETPRESSURE = ".outletPressure";
	public static final String METACOMMON_POSTFIX_AINTERFACER = ".AInterfaceR";
	public static final String METACOMMON_POSTFIX_WEIGHT = ".Weight";
	public static final String METACOMMON_WEIGHT_NAME = "Weight";
	public static final String METACOMMON_POSTFIX_SECTION = ".section";
	public static final String METACOMMON_STRING_SOUTDRYBULBT = "SOutDryBulbT";
    public static final String METACOMMON_STRING_SOUTRELATIVET = "SOutRelativeT";
    public static final String METACOMMON_STRING_SOUTWETBULBT = "SOutWetBulbT";
    public static final String METACOMMON_STRING_WOUTDRYBULBT = "WOutDryBulbT";
    public static final String METACOMMON_STRING_WOUTRELATIVET = "WOutRelativeT";
    public static final String METACOMMON_STRING_WOUTWETBULBT = "WOutWetBulbT";
    public static final String METACOMMON_STRING_SINDRYBULBT = "SInDryBulbT";
    public static final String METACOMMON_STRING_SINRELATIVET = "SInRelativeT";
    public static final String METACOMMON_STRING_SINWETBULBT = "SInWetBulbT";
    public static final String METACOMMON_STRING_WINDRYBULBT = "WInDryBulbT";
    public static final String METACOMMON_STRING_WINRELATIVET = "WInRelativeT";
    public static final String METACOMMON_STRING_WINWETBULBT = "WInWetBulbT";
    public static final String METACOMMON_POSTFIX_ENABLE = ".enable";
    public static final String METACOMMON_ENABLE_SUMMER = "EnableSummer";
    public static final String METACOMMON_ENABLE_WINTER = "EnableWinter";
    public static final String METACOMMON_TYPE_SECTION = "section";
	public static final String METACOMMON_TYPE_PARTITION = "partition";
	public static final String METACOMMON_CAL_AHU_PACKAGE = "ahu.package";
	public static final String METACOMMON_CONSTANCE = "meta.constance";
	public static final String METACOMMON_UNITINFO = "UNITINFO";
	public static final String METACOMMON_ID = "id";
	public static final String METACOMMON_AIR_DIRECTION = "Air Direction";
	public static final String METACOMMON_AIRDIRECTION_NAME = "AirDirection";
	
	//mix
	public static final String METASEXON_MIX = "mix";
	public static final String METASEXON_MIX_NAME = "mix.";
	public static final String METASEXON_MIX_PREFIX = "meta.section.mix.";
	public static final String METASEXON_MIX_SECTIONL = "meta.section.mix.sectionL";
	public static final String METASEXON_MIX_SINDRYBULBT = "meta.section.mix.SInDryBulbT";
	public static final String METASEXON_MIX_SINWETBULBT = "meta.section.mix.SInWetBulbT";
	public static final String METASEXON_MIX_WINDRYBULBT = "meta.section.mix.WInDryBulbT";
	public static final String METASEXON_MIX_WINWETBULBT = "meta.section.mix.WInWetBulbT";
	public static final String METASEXON_MIX_NARATIO = "meta.section.mix.NARatio";
	public static final String METASEXON_MIX_FRESHAIRHOOD = "meta.section.mix.freshAirHood";
	public static final String METASEXON_MIX_DAMPERMETERIAL = "meta.section.mix.damperMeterial";//风阀材料
	public static final String METASEXON_MIX_RETURNBUTTOM = "meta.section.mix.returnButtom";
	public static final String METASEXON_MIX_RETURNLEFT = "meta.section.mix.returnLeft";
	public static final String METASEXON_MIX_RETURNRIGHT = "meta.section.mix.returnRight";
	public static final String METASEXON_MIX_RETURNTOP = "meta.section.mix.returnTop";
	public static final String METASEXON_MIX_RETURNBACK = "meta.section.mix.returnBack";
	public static final String METASEXON_MIX_DAMPEROUTLET = "meta.section.mix.damperOutlet";//风口接件
	public static final String METASEXON_MIX_DOORDIRECTION = "meta.section.mix.DoorDirection";
	public static final String METASEXON_MIX_ENABLESUMMER = "meta.section.mix.EnableSummer";
	public static final String METASEXON_MIX_ENABLEWINTER = "meta.section.mix.EnableWinter";
	public static final String METASEXON_MIX_RESISTANCE = "meta.section.mix.Resistance";
	public static final String METASEXON_MIX_UVLAMP = "meta.section.mix.uvLamp";
	public static final String METASEXON_MIX_UVLAMPSERIAL = "meta.section.mix.uvLampSerial";
	public static final String METASEXON_MIX_DAMPEREXECUTOR = "meta.section.mix.damperExecutor";
	public static final String METASEXON_MIX_DOORO = "meta.section.mix.DoorO";
	public static final String METASEXON_MIX_FIXREPAIRLAMP = "meta.section.mix.fixRepairLamp";
	public static final String METASEXON_MIX_POSITIVEPRESSUREDOOR = "meta.section.mix.PositivePressureDoor";
	public static final String METASEXON_MIX_STYLE = "meta.section.mix.style";
	
	//filter
	public static final String METASEXON_FILTER = "filter";
	public static final String METASEXON_FILTER_NAME = "filter.";
	public static final String METASEXON_FILTER_PREFIX = "meta.section.filter.";
	public static final String METASEXON_FILTER_SECTIONL = "meta.section.filter.sectionL";
	public static final String METASEXON_FILTER_FITETF = "meta.section.filter.fitetF";
	public static final String METASEXON_FILTER_FILTEROPTIONS = "meta.section.filter.filterOptions";
	public static final String METASEXON_FILTER_FILTEREFFICIENCY = "meta.section.filter.filterEfficiency";
	public static final String METASEXON_FILTER_MEDIALOADING = "meta.section.filter.mediaLoading";
	public static final String METASEXON_FILTER_RIMTHICKNESS = "meta.section.filter.rimThickness";
	public static final String METASEXON_FILTER_PRESSUREGUAGE = "meta.section.filter.pressureGuage";
	public static final String METASEXON_FILTER_DEVERAGEPD = "meta.section.filter.deveragePD";
	public static final String METASEXON_FILTER_SUPPLIER = "meta.section.filter.Supplier";// 供应商
	public static final String METASEXON_FILTER_FILTERARRANGE = "meta.section.filter.FilterArrange";// 过滤器布置
	
	//combinedfilter
	public static final String METASEXON_COMBINEDFILTER = "combinedFilter";
	public static final String METASEXON_COMBINEDFILTER_NAME = "combinedFilter.";
	public static final String METASEXON_COMBINEDFILTER_PREFIX = "meta.section.combinedFilter.";
	public static final String METASEXON_COMBINEDFILTER_SECTIONL = "meta.section.combinedFilter.sectionL";
	public static final String METASEXON_COMBINEDFILTER_FILTERF = "meta.section.combinedFilter.filterF";
	public static final String METASEXON_COMBINEDFILTER_LMATERIAL = "meta.section.combinedFilter.LMaterial";
	public static final String METASEXON_COMBINEDFILTER_LMATERIALE = "meta.section.combinedFilter.LMaterialE";
	public static final String METASEXON_COMBINEDFILTER_RMATERIAL = "meta.section.combinedFilter.RMaterial";
	public static final String METASEXON_COMBINEDFILTER_RMATERIALE = "meta.section.combinedFilter.RMaterialE";
	public static final String METASEXON_COMBINEDFILTER_RIMTHICKNESS = "meta.section.combinedFilter.rimThickness";// 边框厚度
	public static final String METASEXON_COMBINEDFILTER_PRESSUREGUAGEB = "meta.section.combinedFilter.pressureGuageB";
	public static final String METASEXON_COMBINEDFILTER_PRESSUREGUAGEP = "meta.section.combinedFilter.pressureGuageP";
	public static final String METASEXON_COMBINEDFILTER_METIALOADING = "meta.section.combinedFilter.mediaLoading";
	public static final String METASEXON_COMBINEDFILTER_RIMTHICK = "meta.section.combinedFilter.rimThick";
	public static final String METASEXON_COMBINEDFILTER_AINTERFACE = "meta.section.combinedMixingChamber.AInterface";
	public static final String METASEXON_COMBINEDFILTER_EVERAGEPDP = "meta.section.combinedFilter.everagePDP";
	public static final String METASEXON_COMBINEDFILTER_EVERAGEPDB = "meta.section.combinedFilter.everagePDB";
	public static final String METASEXON_COMBINEDFILTER_DAMPERMETERIAL = "meta.section.combinedMixingChamber.DamperMeterial";
	public static final String METASEXON_COMBINEDFILTER_FITETF = "meta.section.combinedFilter.fitetF";// 过滤形式
	public static final String METASEXON_COMBINEDFILTER_RIMTHICKNESSP = "meta.section.combinedFilter.rimThicknessP";// 边框厚度
	public static final String METASEXON_COMBINEDFILTER_RIMTHICKNESSB = "meta.section.combinedFilter.rimThicknessB";// 边框厚度
	public static final String METASEXON_COMBINEDFILTER_FILTERARRANGE = "meta.section.combinedFilter.FilterArrange";// 供应商
	public static final String METASEXON_COMBINEDFILTER_SUPPLIER = "meta.section.combinedFilter.Supplier";// 过滤器布置
	public static final String METASEXON_COMBINEDFILTER_PARAP1 = "meta.section.combinedFilter.paraP1";
	public static final String METASEXON_COMBINEDFILTER_PARAP2 = "meta.section.combinedFilter.paraP2";
	public static final String METASEXON_COMBINEDFILTER_PARAP3 = "meta.section.combinedFilter.paraP3";
	public static final String METASEXON_COMBINEDFILTER_PARAP4 = "meta.section.combinedFilter.paraP4";
	public static final String METASEXON_COMBINEDFILTER_PARAPN1 = "meta.section.combinedFilter.paraPN1";
	public static final String METASEXON_COMBINEDFILTER_PARAPN2 = "meta.section.combinedFilter.paraPN2";
	public static final String METASEXON_COMBINEDFILTER_PARAPN3 = "meta.section.combinedFilter.paraPN3";
	public static final String METASEXON_COMBINEDFILTER_PARAPN4 = "meta.section.combinedFilter.paraPN4";
	public static final String METASEXON_COMBINEDFILTER_PARAB1 = "meta.section.combinedFilter.paraB1";
	public static final String METASEXON_COMBINEDFILTER_PARAB2 = "meta.section.combinedFilter.paraB2";
	public static final String METASEXON_COMBINEDFILTER_PARAB3 = "meta.section.combinedFilter.paraB3";
	public static final String METASEXON_COMBINEDFILTER_PARAB4 = "meta.section.combinedFilter.paraB4";
	public static final String METASEXON_COMBINEDFILTER_PARABN1 = "meta.section.combinedFilter.paraBN1";
	public static final String METASEXON_COMBINEDFILTER_PARABN2 = "meta.section.combinedFilter.paraBN2";
	public static final String METASEXON_COMBINEDFILTER_PARABN3 = "meta.section.combinedFilter.paraBN3";
	public static final String METASEXON_COMBINEDFILTER_PARABN4 = "meta.section.combinedFilter.paraBN4";
	public static final String METASEXON_COMBINEDFILTER_INITIALPD = "meta.section.combinedFilter.initialPD";// 初阻力
	public static final String METASEXON_COMBINEDFILTER_FINALPD = "meta.section.combinedFilter.finalPD";// 终阻力
	public static final String METASEXON_COMBINEDFILTER_APPENDIX = "meta.section.combinedFilter.appendix";
	
	//HEPAFilter
	public static final String METASEXON_HEPAFILTER = "HEPAFilter";
	public static final String METASEXON_HEPAFILTER_NAME = "HEPAFilter.";
	public static final String METASEXON_HEPAFILTER_PREFIX = "meta.section.HEPAFilter.";
	public static final String METASEXON_HEPAFILTER_SECTIONL = "meta.section.HEPAFilter.sectionL";
	public static final String METASEXON_HEPAFILTER_FITETF = "meta.section.HEPAFilter.fitetF";
	public static final String METASEXON_HEPAFILTER_FILTEREFFICENCY = "meta.section.HEPAFilter.filterEfficiency";
	public static final String METASEXON_HEPAFILTER_SUPPLIER = "meta.section.HEPAFilter.Supplier";// 供应商
	public static final String METASEXON_HEPAFILTER_FILTERARRANGE ="meta.section.HEPAFilter.FilterArrange";//过滤器布置
	public static final String METASEXON_HEPAFILTER_PARA1 = "meta.section.HEPAFilter.para1";
	public static final String METASEXON_HEPAFILTER_PARAN1 = "meta.section.HEPAFilter.paraN1";
	public static final String METASEXON_HEPAFILTER_PARA2 = "meta.section.HEPAFilter.para2";
	public static final String METASEXON_HEPAFILTER_PARAN2 = "meta.section.HEPAFilter.paraN2";
	public static final String METASEXON_HEPAFILTER_PRESSUREGUAGE = "meta.section.HEPAFilter.pressureGuage";
	
	//electrostaticFilter
	public static final String METASEXON_ELECTROSTATICFILTER = "electrostaticFilter";
	public static final String METASEXON_ELECTROSTATICFILTER_NAME = "electrostaticFilter.";
	public static final String METASEXON_ELECTROSTATICFILTER_PREFIX = "meta.section.electrostaticFilter.";
	public static final String METASEXON_ELECTROSTATICFILTER_SECTIONL = "meta.section.electrostaticFilter.sectionL";
	public static final String METASEXON_ELECTROSTATICFILTER_FILTERTYPE = "meta.section.electrostaticFilter.filterType";
	public static final String METASEXON_ELECTROSTATICFILTER_FILTEREFFICIENCY = "meta.section.electrostaticFilter.filterEfficiency";
	public static final String METASEXON_ELECTROSTATICFILTER_SUPPLIER = "meta.section.electrostaticFilter.Supplier";
	public static final String METASEXON_ELECTROSTATICFILTER_WAY = "meta.section.electrostaticFilter.way";
	
	//coolingcoil
	public static final String METASEXON_COOLINGCOIL = "coolingCoil";
	public static final String METASEXON_COOLINGCOIL_NAME = "coolingCoil.";
	public static final String METASEXON_COOLINGCOIL_PREFIX = "meta.section.coolingCoil.";
	public static final String METASEXON_COOLINGCOIL_SECTIONL = "meta.section.coolingCoil.sectionL";
	public static final String METASEXON_COOLINGCOIL_COILTYPE = "meta.section.coolingCoil.coilType";
	public static final String METASEXON_COOLINGCOIL_FINTYPE = "meta.section.coolingCoil.finType";//翅片材质
	public static final String METASEXON_COOLINGCOIL_AIRVOLUME = "meta.section.coolingCoil.airVolume";
	public static final String METASEXON_COOLINGCOIL_ENTERINGAIRDB = "meta.section.coolingCoil.enteringAirDB";
	public static final String METASEXON_COOLINGCOIL_ENTERINGAIRWB = "meta.section.coolingCoil.enteringAirWB";
	public static final String METASEXON_COOLINGCOIL_TOTALCAPACITY = "meta.section.coolingCoil.totalCapacity";
	public static final String METASEXON_COOLINGCOIL_ENTERINGFLUIDTEMPERATURE = "meta.section.coolingCoil.enteringFluidTemperature";
	public static final String METASEXON_COOLINGCOIL_MAXWPD = "meta.section.coolingCoil.maxWPD";
	public static final String METASEXON_COOLINGCOIL_TEMPRETURECHANGE = "meta.section.coolingCoil.tempretureChange";
	public static final String METASEXON_COOLINGCOIL_ROWS = "meta.section.coolingCoil.rows";//排数
	public static final String METASEXON_COOLINGCOIL_CIRCUIT = "meta.section.coolingCoil.circuit";//回路
	public static final String METASEXON_COOLINGCOIL_FINDENSITY = "meta.section.coolingCoil.finDensity";//片距
	public static final String METASEXON_COOLINGCOIL_SRETURNCOLDQ = "meta.section.coolingCoil.SreturnColdQ";//夏季冷量计算结果
	public static final String METASEXON_COOLINGCOIL_WRETURNHEATQ = "meta.section.coolingCoil.WreturnHeatQ";//冬季热量计算结果
	public static final String METASEXON_COOLINGCOIL_ENABLEWINTER = "meta.section.coolingCoil.EnableWinter";//冬季生效
	public static final String METASEXON_COOLINGCOIL_COOLINGDETAIL_ROWS = "meta.section.coolingCoil.coolingDetail.rows";
	public static final String METASEXON_COOLINGCOIL_COOLINGDETAIL_FINDENSITY = "meta.section.coolingCoil.coolingDetail.finDensity";
	public static final String METASEXON_COOLINGCOIL_COOLINGDETAIL_CIRCUIT = "meta.section.coolingCoil.coolingDetail.circuit";
	public static final String METASEXON_COOLINGCOIL_VELOCITY = "meta.section.coolingCoil.velocity";
	public static final String METASEXON_COOLINGCOIL_AREA = "meta.section.coolingCoil.area";
	public static final String METASEXON_COOLINGCOIL_ENTERINGRH = "meta.section.coolingCoil.enteringrh";
	public static final String METASEXON_COOLINGCOIL_RETURNAIRDB = "meta.section.coolingCoil.returnAirDB";
	public static final String METASEXON_COOLINGCOIL_RETURNAIRWB = "meta.section.coolingCoil.returnAirWB";
	public static final String METASEXON_COOLINGCOIL_RETURNRH = "meta.section.coolingCoil.returnrh";
	public static final String METASEXON_COOLINGCOIL_AIRRESISTANCE = "meta.section.coolingCoil.airResistance";
	public static final String METASEXON_COOLINGCOIL_VOLUMETRICFLOWRATE = "meta.section.coolingCoil.volumetricFlowrate";
	public static final String METASEXON_COOLINGCOIL_FLUIDVELOCITY = "meta.section.coolingCoil.fluidvelocity";
	public static final String METASEXON_COOLINGCOIL_SENSIBLECAPACITY = "meta.section.coolingCoil.sensibleCapacity";
	public static final String METASEXON_COOLINGCOIL_LEAVINGAIRDB = "meta.section.coolingCoil.leavingAirDB";
	public static final String METASEXON_COOLINGCOIL_ELIMINATOR = "meta.section.coolingCoil.eliminator";
	public static final String METASEXON_COOLINGCOIL_BAFFLEMATERIAL = "meta.section.coolingCoil.baffleMaterial";// 挡风板材质
	public static final String METASEXON_COOLINGCOIL_SCOLDQ = "meta.section.coolingCoil.ScoldQ";
	public static final String METASEXON_COOLINGCOIL_SWATERFLOW = "meta.section.coolingCoil.SwaterFlow";
	public static final String METASEXON_COOLINGCOIL_SWATERRESISTANCE = "meta.section.coolingCoil.SwaterResistance";
	public static final String METASEXON_COOLINGCOIL_DRIFTELIMINATORRESISTANCE = "meta.section.coolingCoil.driftEliminatorResistance";
	public static final String METASEXON_COOLINGCOIL_SAIRRESISTANCE = "meta.section.coolingCoil.SAirResistance";
	public static final String METASEXON_COOLINGCOIL_WRETURNWATERFLOW = "meta.section.coolingCoil.WreturnWaterFlow";
	public static final String METASEXON_COOLINGCOIL_SRETURNWATERFLOW = "meta.section.coolingCoil.SreturnWaterFlow";
	public static final String METASEXON_COOLINGCOIL_PIPE = "meta.section.coolingCoil.pipe";
	public static final String METASEXON_COOLINGCOIL_WATERCOLLECTING = "meta.section.coolingCoil.waterCollecting";
	public static final String METASEXON_COOLINGCOIL_MODENOTE = "meta.section.coolingCoil.modeNote";// 模型描述
	public static final String METASEXON_COOLINGCOIL_COILNOTE = "meta.section.coolingCoil.coilNote";// 盘管线描述
	public static final String METASEXON_COOLINGCOIL_TUBEDIAMETER = "meta.section.coolingCoil.tubeDiameter";// 管径
	public static final String METASEXON_COOLINGCOIL_INTERVAL = "meta.section.coolingCoil.interval";// 管间距
	public static final String METASEXON_COOLINGCOIL_FINTEM = "meta.section.coolingCoil.finTem";// 最小翅片表面温度
	public static final String METASEXON_COOLINGCOIL_CPTEM = "meta.section.coolingCoil.cpTem";// 最小铜管壁表面温度
	public static final String METASEXON_COOLINGCOIL_COILFRAMEMATERIALNOTE = "meta.section.coolingCoil.coilFrameMaterialNote";// 盘管支架材质
	public static final String METASEXON_COOLINGCOIL_COILFRAMEMATERIAL = "meta.section.coolingCoil.coilFrameMaterial";// 盘管框架材质
	public static final String METASEXON_COOLINGCOIL_CONNECTIONS = "meta.section.coolingCoil.connections";// 连接方式
	public static final String METASEXON_COOLINGCOIL_HEIGHTLENGTH = "meta.section.coolingCoil.heightLength";// 盘管高度/长度
	public static final String METASEXON_COOLINGCOIL_HEADERDIA = "meta.section.coolingCoil.headerDia";// 进出水管径
	public static final String METASEXON_COOLINGCOIL_COILMODEL = "meta.section.coolingCoil.coilModel";// 盘管型号描述
	public static final String METASEXON_COOLINGCOIL_SCONCENTRATION = "meta.section.coolingCoil.Sconcentration";//浓度
	public static final String METASEXON_COOLINGCOIL_SCOOLANT = "meta.section.coolingCoil.Scoolant";//介质
	public static final String METASEXON_COOLINGCOIL_SRETURNENTERINGFLUIDTEMPERATURE = "meta.section.coolingCoil.SreturnEnteringFluidTemperature";//进水温度（夏季计算结果）
	public static final String METASEXON_COOLINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE = "meta.section.coolingCoil.WreturnEnteringFluidTemperature";//冬季进水温度
	public static final String METASEXON_COOLINGCOIL_SRETURNWTASCEND = "meta.section.coolingCoil.SreturnWTAScend";//水温升（夏季计算结果）
	public static final String METASEXON_COOLINGCOIL_WWTASCEND = "meta.section.coolingCoil.WWTAScend";//水温降
	
	public static final String METASEXON_COOLINGCOIL_SRETURNOUTFLUIDTEMPERATURE = "meta.section.coolingCoil.SreturnOutFluidTemperature";//出水温度
	public static final String METASEXON_COOLINGCOIL_WRETURNOUTFLUIDTEMPERATURE = "meta.section.coolingCoil.WreturnOutFluidTemperature";//冬季出水温度
	public static final String METASEXON_COOLINGCOIL_SINDRYBULBT = "meta.section.coolingCoil.SInDryBulbT";//干球温度(夏入)
	public static final String METASEXON_COOLINGCOIL_SINWETBULBT = "meta.section.coolingCoil.SInWetBulbT";//湿球温度(夏入)
	public static final String METASEXON_COOLINGCOIL_SVELOCITY = "meta.section.coolingCoil.Svelocity";//迎面风速
	
	//heatingcoil
	public static final String METASEXON_HEATINGCOIL = "heatingCoil";
	public static final String METASEXON_HEATINGCOIL_NAME = "heatingCoil.";
	public static final String METASEXON_HEATINGCOIL_PREFIX = "meta.section.heatingCoil.";
	public static final String METASEXON_HEATINGCOIL_SECTIONL = "meta.section.heatingCoil.sectionL";
	public static final String METASEXON_HEATINGCOIL_ROWS = "meta.section.heatingCoil.rows";// 排数
	public static final String METASEXON_HEATINGCOIL_CIRCUIT = "meta.section.heatingCoil.circuit";//回路
	public static final String METASEXON_HEATINGCOIL_FINDENSITY = "meta.section.heatingCoil.finDensity";//片距
	public static final String METASEXON_HEATINGCOIL_WRETURNHEATQ = "meta.section.heatingCoil.WreturnHeatQ";//冬季热量计算结果
	public static final String METASEXON_HEATINGCOIL_SRETURNCOLDQ = "meta.section.heatingCoil.SreturnColdQ";//夏季冷量计算结果
	public static final String METASEXON_HEATINGCOIL_ENABLESUMMER = "meta.section.heatingCoil.EnableSummer";//夏季生效
	public static final String METASEXON_HEATINGCOIL_BAFFLEMATERIAL = "meta.section.heatingCoil.baffleMaterial";// 挡风板材质
	public static final String METASEXON_HEATINGCOIL_WHEATQ = "meta.section.heatingCoil.WheatQ";
	public static final String METASEXON_HEATINGCOIL_WWATERFLOW = "meta.section.heatingCoil.WwaterFlow";
	public static final String METASEXON_HEATINGCOIL_WWATERRESISTANCE = "meta.section.heatingCoil.WwaterResistance";
	public static final String METASEXON_HEATINGCOIL_WENTERINGFLUIDTEMPERATURE = "meta.section.heatingCoil.WenteringFluidTemperature";
	public static final String METASEXON_HEATINGCOIL_WWTASCEND = "meta.section.heatingCoil.WWTAScend";
	public static final String METASEXON_HEATINGCOIL_FINTYPE = "meta.section.heatingCoil.finType";
	public static final String METASEXON_HEATINGCOIL_PIPE = "meta.section.heatingCoil.pipe";//进水管
	public static final String METASEXON_HEATINGCOIL_WATERCOLLECTION = "meta.section.heatingCoil.waterCollection";//集水管材质
	public static final String METASEXON_HEATINGCOIL_MODENOTE = "meta.section.heatingCoil.modeNote";// 模型描述
	public static final String METASEXON_HEATINGCOIL_COILNOTE = "meta.section.heatingCoil.coilNote";// 盘管线描述
	public static final String METASEXON_HEATINGCOIL_TUBEDIAMETER = "meta.section.heatingCoil.tubeDiameter";// 管径
	public static final String METASEXON_HEATINGCOIL_INTERVAL = "meta.section.heatingCoil.interval";// 管间距
	public static final String METASEXON_HEATINGCOIL_COILFRAMEMATERIALNOTE = "meta.section.heatingCoil.coilFrameMaterialNote";// 盘管支架材质
	public static final String METASEXON_HEATINGCOIL_COILFRAMEMATERIAL = "meta.section.heatingCoil.coilFrameMaterial";// 盘管框架材质
	public static final String METASEXON_HEATINGCOIL_CONNECTIONS = "meta.section.heatingCoil.connections";// 连接方式
	public static final String METASEXON_HEATINGCOIL_HEADERDIA = "meta.section.heatingCoil.headerDia";// 进出水管径
	public static final String METASEXON_HEATINGCOIL_COILMODEL = "meta.section.heatingCoil.coilModel";// 盘管型号描述
	public static final String METASEXON_HEATINGCOIL_HEIGHTLENGTH = "meta.section.heatingCoil.heightLength";// 高度/长度
	public static final String METASEXON_HEATINGCOIL_SCONCENTRATION = "meta.section.heatingCoil.Sconcentration";//浓度
	public static final String METASEXON_HEATINGCOIL_WRETURNWATERFLOW = "meta.section.heatingCoil.WreturnWaterFlow";//水流量冬
	public static final String METASEXON_HEATINGCOIL_SCOOLANT = "meta.section.heatingCoil.Scoolant";//介质
	public static final String METASEXON_HEATINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE = "meta.section.heatingCoil.WreturnEnteringFluidTemperature";//进水温度（冬季计算结果）
	public static final String METASEXON_HEATINGCOIL_WRETURNWTASCEND = "meta.section.heatingCoil.WreturnWTAScend";//水温升（夏季计算结果）
	public static final String METASEXON_HEATINGCOIL_WRETURNOUTFLUIDTEMPERATURE = "meta.section.heatingCoil.WreturnOutFluidTemperature";//出水温度
	public static final String METASEXON_HEATINGCOIL_WINDRYBULBT = "meta.section.heatingCoil.WInDryBulbT";//冬季进风干球温度
	public static final String METASEXON_HEATINGCOIL_WINWETBULBT = "meta.section.heatingCoil.WInWetBulbT";//冬季进风湿球温度
	public static final String METASEXON_HEATINGCOIL_FROSTPROTECTIONOPTION = "meta.section.heatingCoil.frostProtectionOption";//防冻开关
	public static final String METASEXON_HEATINGCOIL_WVELOCITY = "meta.section.heatingCoil.Wvelocity";
	
	//directExpensionCoil
	public static final String METASEXON_DXCOIL = "directExpensionCoil";
	public static final String METASEXON_DXCOIL_NAME = "directExpensionCoil.";
	public static final String METASEXON_DXCOIL_PREFIX = "meta.section.directExpensionCoil.";
	public static final String METASEXON_DXCOIL_ROWS = "meta.section.directExpensionCoil.rows";// 排数
	public static final String METASEXON_DXCOIL_CIRCUIT = "meta.section.directExpensionCoil.circuit";//回路
	public static final String METASEXON_DXCOIL_FINDENSITY = "meta.section.directExpensionCoil.finDensity";//片距
	public static final String METASEXON_DXCOIL_SRETURNCOLDQ = "meta.section.directExpensionCoil.SreturnColdQ";//夏季冷量计算结果
	public static final String METASEXON_DXCOIL_ELIMINATOR = "meta.section.directExpensionCoil.eliminator";
	public static final String METASEXON_DXCOIL_BAFFLEMATERIAL = "meta.section.directExpensionCoil.baffleMaterial";// 挡风板材质
	public static final String METASEXON_DXCOIL_ENABLEWINTER = "meta.section.directExpensionCoil.enableWinter";
	public static final String METASEXON_DXCOIL_MODENOTE = "meta.section.directExpensionCoil.modeNote";// 模型描述
	public static final String METASEXON_DXCOIL_COILNOTE = "meta.section.directExpensionCoil.coilNote";// 盘管线描述
	public static final String METASEXON_DXCOIL_TUBEDIAMETER = "meta.section.directExpensionCoil.tubeDiameter";
	public static final String METASEXON_DXCOIL_INTERVAL = "meta.section.directExpensionCoil.interval";// 管间距
	public static final String METASEXON_DXCOIL_FINTEM = "meta.section.directExpensionCoil.finTem";// 最小翅片表面温度
	public static final String METASEXON_DXCOIL_CPTEM = "meta.section.directExpensionCoil.cpTem";// 最小铜管壁表面温度
	public static final String METASEXON_DXCOIL_COILFRAMEMATERIALNOTE = "meta.section.directExpensionCoil.coilFrameMaterialNote";// 盘管支架材质
	public static final String METASEXON_DXCOIL_COILFRAMEMATERIAL = "meta.section.directExpensionCoil.coilFrameMaterial";// 盘管框架材质
	public static final String METASEXON_DXCOIL_CONNECTIONS = "meta.section.directExpensionCoil.connections";// 连接方式
	public static final String METASEXON_DXCOIL_HEIGHTLENGTH = "meta.section.directExpensionCoil.heightLength";// 高度/长度
	public static final String METASEXON_DXCOIL_SVELOCITY = "meta.section.directExpensionCoil.Svelocity";//迎面风速
	
	//electricHeatingCoil
	public static final String METASEXON_ELECTRICHEATINGCOIL = "electricHeatingCoil";
	public static final String METASEXON_ELECTRICHEATINGCOIL_NAME = "electricHeatingCoil.";
	public static final String METASEXON_ELECTRICHEATINGCOIL_PREFIX = "meta.section.electricHeatingCoil.";
	public static final String METASEXON_ELECTRICHEATINGCOIL_SECTIONL = "meta.section.electricHeatingCoil.sectionL";
	public static final String METASEXON_ELECTRICHEATINGCOIL_POWER = "meta.section.electricHeatingCoil.power";
	public static final String METASEXON_ELECTRICHEATINGCOIL_ROWNUM = "meta.section.electricHeatingCoil.RowNum";
	public static final String METASEXON_ELECTRICHEATINGCOIL_ENABLESUMMER = "meta.section.electricHeatingCoil.EnableSummer";
	public static final String METASEXON_ELECTRICHEATINGCOIL_ENABLEWINTER = "meta.section.electricHeatingCoil.EnableWinter";
	public static final String METASEXON_ELECTRICHEATINGCOIL_HEATMEDIUM = "meta.section.electricHeatingCoil.heatMedium";// 热媒
	public static final String METASEXON_ELECTRICHEATINGCOIL_WVELOCITY = "meta.section.electricHeatingCoil.Wvelocity";//迎面风速
	
	//steamCoil
	public static final String METASEXON_STEAMCOIL = "steamCoil";
	public static final String METASEXON_STEAMCOIL_NAME = "steamCoil.";
	public static final String METASEXON_STEAMCOIL_PREFIX = "meta.section.steamCoil.";
	public static final String METASEXON_STEAMCOIL_SECTIONL = "meta.section.steamCoil.sectionL";
	public static final String METASEXON_STEAMCOIL_ENABLESUMMER = "meta.section.steamCoil.EnableSummer";
	public static final String METASEXON_STEAMCOIL_ENABLEWINTER = "meta.section.steamCoil.EnableWinter";
	public static final String METASEXON_STEAMCOIL_CONNECTIONTYPE = "meta.section.steamCoil.ConnectionType";
	public static final String METASEXON_STEAMCOIL_HEATMEDIUM = "meta.section.steamCoil.heatMedium";// 热媒
	public static final String METASEXON_STEAMCOIL_WVELOCITY = "meta.section.steamCoil.Wvelocity";//迎面风速
	
	//fan
	public static final String METASEXON_FAN = "fan";
	public static final String METASEXON_FAN_NAME = "fan.";
	public static final String METASEXON_FAN_MOTOREFFPOLE="meta.section.fan.motorEffPole";
	public static final String METASEXON_FAN_FREQUENCYRANGE="meta.section.fan.frequencyRange";
	public static final String METASEXON_FAN_PREFIX = "meta.section.fan.";
	public static final String METASEXON_FAN_SECTIONL = "meta.section.fan.sectionL";
	public static final String METASEXON_FAN_OUTLET = "meta.section.fan.outlet";//风机形式
	public static final String METASEXON_FAN_SUPPLIER = "meta.section.fan.supplier";
	public static final String METASEXON_FAN_FANSUPPLIER = "meta.section.fan.fanSupplier";
	public static final String METASEXON_FAN_TYPE = "meta.section.fan.type";// 电机类型
	public static final String METASEXON_FAN_EXTRASTATIC = "meta.section.fan.extraStatic";
	public static final String METASEXON_FAN_RETURNPOSITION = "meta.section.fan.returnPosition";//送风出口位置
	public static final String METASEXON_FAN_SENDPOSITION = "meta.section.fan.sendPosition";
	public static final String METASEXON_FAN_AIRVOLUME = "meta.section.fan.airVolume";
	public static final String METASEXON_FAN_FANMODEL = "meta.section.fan.fanModel";//风机标识
	public static final String METASEXON_FAN_STARTSTYLE = "meta.section.fan.startStyle";
	public static final String METASEXON_FAN_ELIMINATOR = "meta.section.fan.eliminator";
	public static final String METASEXON_FAN_PROTECTION = "meta.section.fan.Protection";// 防护等级
	public static final String METASEXON_FAN_TOTALSTATIC = "meta.section.fan.totalStatic";
	public static final String METASEXON_FAN_MOTORPOWER = "meta.section.fan.motorPower";
	public static final String METASEXON_FAN_POWER = "meta.section.fan.power";//电源
	public static final String METASEXON_FAN_DOORONBOTHSIDE = "meta.section.fan.doorOnBothSide";//两侧开门
	public static final String METASEXON_FAN_FIXREPAIRLAMP = "meta.section.fan.fixRepairLamp";//安装检修灯
	public static final String METASEXON_FAN_POSITIVEPRESSUREDOOR = "meta.section.fan.PositivePressureDoor";//正压门
	public static final String METASEXON_FAN_PARA13 = "meta.section.fan.para13";//耐盐雾型减震器
	public static final String METASEXON_FAN_PTC = "meta.section.fan.ptc";//ptc
	public static final String METASEXON_FAN_ACCESSDOOR = "meta.section.fan.accessDoor";
	public static final String METASEXON_FAN_OUTLETDIRECTION = "meta.section.fan.outletDirection";//出风方向
	public static final String METASEXON_FAN_MOTORPOSITION = "meta.section.fan.motorPosition";
	public static final String METASEXON_FAN_STANDBYMOTOR = "meta.section.fan.standbyMotor";
	public static final String METASEXON_FAN_MOTORBASENO = "meta.section.fan.motorBaseNo";// 机座号
	public static final String METASEXON_FAN_AINTERFACEPOSITION = "meta.section.fan.aInterfacePosition";
	public static final String METASEXON_FAN_SUPPLYDAMPER = "meta.section.fan.supplyDamper";//送风口型式
	public static final String METASEXON_FAN_SYSSTATIC = "meta.section.fan.sysStatic";
	public static final String METASEXON_FAN_CURVE = "meta.section.fan.curve";
	public static final String METASEXON_FAN_SFAN = "meta.section.sfan";
	public static final String METASEXON_FAN_RFAN = "meta.section.rfan";
	public static final String METASEXON_FAN_TOTALPRESSURE = "meta.section.fan.totalPressure";
	public static final String METASEXON_FAN_EFFICIENCY = "meta.section.fan.efficiency";
	public static final String METASEXON_FAN_BELTGUARD = "meta.section.fan.beltGuard";// 皮带保护罩
	public static final String METASEXON_FAN_SEISMICPRINGISOLATOR = "meta.section.fan.seismicPringIsolator";// 防剪切减震器
	public static final String METASEXON_FAN_INSULATION = "meta.section.fan.insulation";// 绝缘等级
	public static final String METASEXON_FAN_MOTORBRAND = "meta.section.fan.motorBrand";// 备用电机
	public static final String METASEXON_FAN_LW1 = "meta.section.fan.LW1";
	public static final String METASEXON_FAN_LW2 = "meta.section.fan.LW2";
	public static final String METASEXON_FAN_LW3 = "meta.section.fan.LW3";
	public static final String METASEXON_FAN_LW4 = "meta.section.fan.LW4";
	public static final String METASEXON_FAN_LW5 = "meta.section.fan.LW5";
	public static final String METASEXON_FAN_LW6 = "meta.section.fan.LW6";
	public static final String METASEXON_FAN_LW7 = "meta.section.fan.LW7";
	public static final String METASEXON_FAN_LW8 = "meta.section.fan.LW8";
	public static final String METASEXON_FAN_LW = "meta.section.fan.LW";// A声功率级
	public static final String METASEXON_FAN_MOTOREFF = "meta.section.fan.motorEff";
	public static final String METASEXON_FAN_RPM = "meta.section.fan.RPM";
	public static final String METASEXON_FAN_OUTLETVELOCITY = "meta.section.fan.outletVelocity";
	public static final String METASEXON_FAN_ENGINEDATA1 = "meta.section.fan.engineData1";
	public static final String METASEXON_FAN_ENGINEDATA2 = "meta.section.fan.engineData2";
	public static final String METASEXON_FAN_ENGINEDATA3 = "meta.section.fan.engineData3";
	public static final String METASEXON_FAN_ENGINEDATA4 = "meta.section.fan.engineData4";
	public static final String METASEXON_FAN_ENGINEDATA5 = "meta.section.fan.engineData5";
	public static final String METASEXON_FAN_ENGINEDATA6 = "meta.section.fan.engineData6";
	public static final String METASEXON_FAN_ENGINEDATA7 = "meta.section.fan.engineData7";
	public static final String METASEXON_FAN_ENGINEDATA8 = "meta.section.fan.engineData8";
	public static final String METASEXON_FAN_LP1 = "meta.section.fan.LP1";
	public static final String METASEXON_FAN_LP2 = "meta.section.fan.LP2";
	public static final String METASEXON_FAN_LP3 = "meta.section.fan.LP3";
	public static final String METASEXON_FAN_LP4 = "meta.section.fan.LP4";
	public static final String METASEXON_FAN_LP5 = "meta.section.fan.LP5";
	public static final String METASEXON_FAN_LP6 = "meta.section.fan.LP6";
	public static final String METASEXON_FAN_LP7 = "meta.section.fan.LP7";
	public static final String METASEXON_FAN_LP8 = "meta.section.fan.LP8";
	public static final String METASEXON_FAN_LP = "meta.section.fan.LP";// A声声压级
	public static final String METASEXON_FAN_POSITIONA = "meta.section.fan.positionA";// 减震器
	public static final String METASEXON_FAN_POSITIONB = "meta.section.fan.positionB";
	public static final String METASEXON_FAN_POSITIONC = "meta.section.fan.positionC";
	public static final String METASEXON_FAN_POSITIOND = "meta.section.fan.positionD";
	public static final String METASEXON_FAN_POSITIONE = "meta.section.fan.positionE";
	public static final String METASEXON_FAN_POSITIONF = "meta.section.fan.positionF";
	public static final String METASEXON_FAN_POSITIONG = "meta.section.fan.positionG";
	public static final String METASEXON_FAN_POSITIONH = "meta.section.fan.positionH";
	public static final String METASEXON_FAN_POSITIONI = "meta.section.fan.positionI";
	public static final String METASEXON_FAN_POSITIONJ = "meta.section.fan.positionJ";
	public static final String METASEXON_FAN_HZ = "meta.section.fan.hz";
	public static final String META_SECTION_FAN_ENGINETYPE = "meta.section.fan.engineType";
	public static final String META_SECTION_FAN_POLE = "meta.section.fan.pole";
	public static final String NS_SECTION_FAN_ENABLE = "ns.section.fan.enable";
	public static final String NS_SECTION_FAN_NSCHANGEFAN = "ns.section.fan.nsChangeFan";
	public static final String NS_SECTION_FAN_NSFANMODEL = "ns.section.fan.nsFanModel";

	//access
	public static final String METASEXON_ACCESS = "access";
	public static final String METASEXON_ACCESS_NAME = "access.";
	public static final String METASEXON_ACCESS_PREFIX = "meta.section.access.";
	public static final String METASEXON_ACCESS_SECTIONL = "meta.section.access.sectionL";
	public static final String METASEXON_ACCESS_ODOOR = "meta.section.access.ODoor";
	public static final String METASEXON_ACCESS_FUNCTION = "meta.section.access.Function";
	public static final String METASEXON_ACCESS_DOORONBOTHSIDE = "meta.section.access.DoorOnBothSide";
	public static final String METASEXON_ACCESS_FIXREPAIRLAMP = "meta.section.access.FixRepairLamp";
	public static final String METASEXON_ACCESS_POSITIVEPRESSUREDOOR = "meta.section.access.PositivePressureDoor";
	public static final String METASEXON_ACCESS_FUNCTIONS = "meta.section.access.functionS";//功能
	public static final String METASEXON_ACCESS_UVLAMP = "meta.section.access.uvLamp";//安装杀菌灯
	
	//discharge
	public static final String METASEXON_DISCHARGE = "discharge";
	public static final String METASEXON_DISCHARGE_NAME = "discharge.";
	public static final String METASEXON_DISCHARGE_PREFIX = "meta.section.discharge.";
	public static final String METASEXON_DISCHARGE_SECTIONL = "meta.section.discharge.sectionL";
	public static final String METASEXON_DISCHARGE_OUTLETDIRECTION = "meta.section.discharge.OutletDirection";
	public static final String METASEXON_DISCHARGE_AINTERFACE = "meta.section.discharge.AInterface";
	public static final String METASEXON_DISCHARGE_DAMPEREXECUTOR = "meta.section.discharge.DamperExecutor";//执行器
	public static final String METASEXON_DISCHARGE_DAMPERMETERIAL = "meta.section.discharge.DamperMeterial";//风阀材料
	public static final String METASEXON_DISCHARGE_DOORDIRECTION = "meta.section.discharge.DoorDirection";
	public static final String METASEXON_DISCHARGE_RESISTANCE = "meta.section.discharge.Resistance";
	public static final String METASEXON_DISCHARGE_FIXREPAIRLAMP = "meta.section.discharge.FixRepairLamp";
	public static final String METASEXON_DISCHARGE_POSITIVEPRESSUREDOOR = "meta.section.discharge.PositivePressureDoor";//正压门
	public static final String METASEXON_DISCHARGE_DISCHARGEODOOR = "meta.section.discharge.ODoor";//开门

	
	//wetfilmHumidifier
	public static final String METASEXON_WETFILMHUMIDIFIER = "wetfilmHumidifier";
	public static final String METASEXON_WETFILMHUMIDIFIER_NAME = "wetfilmHumidifier.";
	public static final String METASEXON_WETFILMHUMIDIFIER_PREFIX = "meta.section.wetfilmHumidifier.";
	public static final String METASEXON_WETFILMHUMIDIFIER_SECTIONL = "meta.section.wetfilmHumidifier.sectionL";
	public static final String METASEXON_WETFILMHUMIDIFIER_THICKNESS = "meta.section.wetfilmHumidifier.thickness";
	public static final String METASEXON_WETFILMHUMIDIFIER_ENABLESUMMER = "meta.section.wetfilmHumidifier.EnableSummer";
	public static final String METASEXON_WETFILMHUMIDIFIER_ENABLEWINTER = "meta.section.wetfilmHumidifier.EnableWinter";
	public static final String METASEXON_WETFILMHUMIDIFIER_SHUMIDIFICATIONQ = "meta.section.wetfilmHumidifier.SHumidificationQ";
	public static final String METASEXON_WETFILMHUMIDIFIER_WHUMIDIFICATIONQ = "meta.section.wetfilmHumidifier.WHumidificationQ";
	public static final String METASEXON_WETFILMHUMIDIFIER_STYLE = "meta.section.wetfilmHumidifier.style";
	public static final String METASEXON_WETFILMHUMIDIFIER_WATERQUALITY = "meta.section.wetfilmHumidifier.waterQuality";
	public static final String METASEXON_WETFILMHUMIDIFIER_INSTALL = "meta.section.wetfilmHumidifier.install";// 安装段
	public static final String METASEXON_WETFILMHUMIDIFIER_PREVIOUS = "ahu.wetfilmHumidifier.previous";// 当前段标识
	public static final String METASEXON_WETFILMHUMIDIFIER_SINDRYBULBT = "meta.section.wetfilmHumidifier.SInDryBulbT";//夏季进风干球
	public static final String METASEXON_WETFILMHUMIDIFIER_SINWETBULBT = "meta.section.wetfilmHumidifier.SInWetBulbT";//夏季进风湿球
	public static final String METASEXON_WETFILMHUMIDIFIER_WATERQUANTITY = "meta.section.wetfilmHumidifier.WaterQuantity";//水量

	//steamHumidifier
	public static final String METASEXON_STEAMHUMIDIFIER = "steamHumidifier";
	public static final String METASEXON_STEAMHUMIDIFIER_NAME = "steamHumidifier.";
	public static final String METASEXON_STEAMHUMIDIFIER_PREFIX = "meta.section.steamHumidifier.";
	public static final String METASEXON_STEAMHUMIDIFIER_SECTIONL = "meta.section.steamHumidifier.sectionL";
	public static final String METASEXON_STEAMHUMIDIFIER_HTYPES = "meta.section.steamHumidifier.HTypes";//类型
	public static final String METASEXON_STEAMHUMIDIFIER_VAPORPRESSURE = "meta.section.steamHumidifier.VaporPressure";
	public static final String METASEXON_STEAMHUMIDIFIER_ENABLESUMMER = "meta.section.steamHumidifier.EnableSummer";
	public static final String METASEXON_STEAMHUMIDIFIER_ENABLEWINTER = "meta.section.steamHumidifier.EnableWinter";
	public static final String METASEXON_STEAMHUMIDIFIER_CONTROLM = "meta.section.steamHumidifier.ControlM";//控制方式
	public static final String METASEXON_STEAMHUMIDIFIER_WHUMIDIFICATIONQ = "meta.section.steamHumidifier.WHumidificationQ";
	public static final String METASEXON_STEAMHUMIDIFIER_SHUMIDIFICATIONQ = "meta.section.steamHumidifier.SHumidificationQ";
	public static final String METASEXON_STEAMHUMIDIFIER_HUMIDIFICATIONQ = "meta.section.steamHumidifier.humidificationQ";
	public static final String METASEXON_STEAMHUMIDIFIER_WAY = "meta.section.steamHumidifier.way";//加湿方式
	public static final String METASEXON_STEAMHUMIDIFIER_SUPPLIER = "meta.section.steamHumidifier.Supplier";// 供应商
	public static final String METASEXON_STEAMHUMIDIFIER_TUBEDIAMETER = "meta.section.steamHumidifier.tubeDiameter";// 进气管径
	
	//sprayHumidifier
	public static final String METASEXON_SPRAYHUMIDIFIER = "sprayHumidifier";
	public static final String METASEXON_SPRAYHUMIDIFIER_NAME = "sprayHumidifier.";
	public static final String METASEXON_SPRAYHUMIDIFIER_PREFIX = "meta.section.sprayHumidifier.";
	public static final String METASEXON_SPRAYHUMIDIFIER_SECTIONL = "meta.section.sprayHumidifier.sectionL";
	public static final String METASEXON_SPRAYHUMIDIFIER_HQ = "meta.section.sprayHumidifier.hq";
	public static final String METASEXON_SPRAYHUMIDIFIER_NOZZLEN = "meta.section.sprayHumidifier.nozzleN";
	public static final String METASEXON_SPRAYHUMIDIFIER_APERTURE = "meta.section.sprayHumidifier.aperture";
	public static final String METASEXON_SPRAYHUMIDIFIER_WHUMIDIFICATIONQ = "meta.section.sprayHumidifier.WHumidificationQ";
	public static final String METASEXON_SPRAYHUMIDIFIER_SHUMIDIFICATIONQ = "meta.section.sprayHumidifier.SHumidificationQ";
	public static final String METASEXON_SPRAYHUMIDIFIER_ELIMINATOR = "meta.section.sprayHumidifier.Eliminator";
	public static final String METASEXON_SPRAYHUMIDIFIER_ENABLESUMMER = "meta.section.sprayHumidifier.EnableSummer";
	public static final String METASEXON_SPRAYHUMIDIFIER_ENABLEWINTER = "meta.section.sprayHumidifier.EnableWinter";
	public static final String METASEXON_SPRAYHUMIDIFIER_WAY = "meta.section.sprayHumidifier.way";// 加湿方式
	public static final String METASEXON_SPRAYHUMIDIFIER_WATERQUALITY = "meta.section.sprayHumidifier.waterQuality";// 供水水质
	public static final String METASEXON_SPRAYHUMIDIFIER_QUANTITY = "meta.section.sprayHumidifier.quantity";// 供水量;
	public static final String METASEXON_SPRAYHUMIDIFIER_PRESSURE = "meta.section.sprayHumidifier.pressure";// 进水压力
	public static final String METASEXON_SPRAYHUMIDIFIER_POWER = "meta.section.sprayHumidifier.power";// 电源与额定功率
	public static final String METASEXON_SPRAYHUMIDIFIER_SUPPLIER = "meta.section.sprayHumidifier.Supplier";// 供应商
	public static final String METASEXON_SPRAYHUMIDIFIER_INSTALL = "meta.section.sprayHumidifier.install";// 安装段
	public static final String METASEXON_SPRAYHUMIDIFIER_PREVIOUS = "ahu.sprayHumidifier.previous";// 当前段标识;
	public static final String METASEXON_SPRAYHUMIDIFIER_SINDRYBULBT = "meta.section.sprayHumidifier.SInDryBulbT";//夏季进风干球
	public static final String METASEXON_SPRAYHUMIDIFIER_SINWETBULBT = "meta.section.sprayHumidifier.SInWetBulbT";//夏季进风湿球
	
	//electrodeHumidifier
	public static final String METASEXON_ELECTRODEHUMIDIFIER = "electrodeHumidifier";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_NAME = "electrodeHumidifier.";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_PREFIX = "meta.section.electrodeHumidifier.";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_SECTIONL = "meta.section.electrodeHumidifier.sectionL";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ = "meta.section.electrodeHumidifier.WHumidificationQ";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ = "meta.section.electrodeHumidifier.SHumidificationQ";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_ENABLEWINTER = "meta.section.electrodeHumidifier.EnableWinter";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_ENABLESUMMER = "meta.section.electrodeHumidifier.EnableSummer";
	public static final String METASEXON_ELECTRODEHUMIDIFIER_WAY = "meta.section.electrodeHumidifier.way";// 加湿方式
	public static final String METASEXON_ELECTRODEHUMIDIFIER_SUPPLIER = "meta.section.electrodeHumidifier.Supplier";// 供应商
	public static final String METASEXON_ELECTRODEHUMIDIFIER_WATERQUALIITY = "meta.section.electrodeHumidifier.waterQuality";// 供水水质
	public static final String METASEXON_ELECTRODEHUMIDIFIER_PRESSURE = "meta.section.electrodeHumidifier.pressure";// 进水压力
	public static final String METASEXON_ELECTRODEHUMIDIFIER_TUBEDIAMETER = "meta.section.electrodeHumidifier.tubeDiameter";// 进气管径
	public static final String METASEXON_ELECTRODEHUMIDIFIER_WASTETUBEDIAMETER = "meta.section.electrodeHumidifier.wasteTubeDiameter";// 排污管径
	public static final String METASEXON_ELECTRODEHUMIDIFIER_POWER = "meta.section.electrodeHumidifier.power";// 额定功率 查询表
	public static final String METASEXON_ELECTRODEHUMIDIFIER_CONTROLLER = "meta.section.electrodeHumidifier.controller";// 控制方式
	public static final String METASEXON_ELECTRODEHUMIDIFIER_SINDRYBULBT = "meta.section.electrodeHumidifier.SInDryBulbT";//夏季进风干球
	public static final String METASEXON_ELECTRODEHUMIDIFIER_SINWETBULBT = "meta.section.electrodeHumidifier.SInWetBulbT";//夏季进风湿球
	
	//wheelHeatRecycle
	public static final String METASEXON_WHEELHEATRECYCLE_BRAND = "meta.section.wheelHeatRecycle.Brand";
	public static final String METASEXON_WHEELHEATRECYCLE = "wheelHeatRecycle";
	public static final String METASEXON_WHEELHEATRECYCLE_NAME = "wheelHeatRecycle.";
	public static final String METASEXON_WHEELHEATRECYCLE_PREFIX = "meta.section.wheelHeatRecycle.";
	public static final String METASEXON_WHEELHEATRECYCLE_SECTIONL = "meta.section.wheelHeatRecycle.sectionL";
	public static final String METASEXON_WHEELHEATRECYCLE_HEATWHEEL = "heatWheel";
	public static final String METASEXON_WHEELHEATRECYCLE_WHEELDEPTH = "meta.section.wheelHeatRecycle.WheelDepth";
	public static final String METASEXON_WHEELHEATRECYCLE_WINRELATIVET = "meta.section.wheelHeatRecycle.WInRelativeT";
	public static final String METASEXON_WHEELHEATRECYCLE_WINWETBULBT = "meta.section.wheelHeatRecycle.WInWetBulbT";
	public static final String METASEXON_WHEELHEATRECYCLE_SINRELATIVET = "meta.section.wheelHeatRecycle.SInRelativeT";
	public static final String METASEXON_WHEELHEATRECYCLE_SINWETBULBT = "meta.section.wheelHeatRecycle.SInWetBulbT";
	public static final String METASEXON_WHEELHEATRECYCLE_WINDRYBULBT = "meta.section.wheelHeatRecycle.WInDryBulbT";
	public static final String METASEXON_WHEELHEATRECYCLE_SINDRYBULBT = "meta.section.wheelHeatRecycle.SInDryBulbT";
	public static final String METASEXON_WHEELHEATRECYCLE_HEATRECYCLEDETAIL = "meta.section.wheelHeatRecycle.HeatRecycleDetail";
	public static final String METASEXON_WHEELHEATRECYCLE_MODEL = "meta.section.wheelHeatRecycle.Model";
	
	//plateHeatRecycle
	public static final String METASEXON_PLATEHEATRECYCLE = "plateHeatRecycle";
	public static final String METASEXON_PLATEHEATRECYCLE_NAME = "plateHeatRecycle.";
	public static final String METASEXON_PLATEHEATRECYCLE_PREFIX = "meta.section.plateHeatRecycle.";
	public static final String METASEXON_PLATEHEATRECYCLE_SECTIONL = "meta.section.plateHeatRecycle.sectionL";
	public static final String METASEXON_PLATEHEATRECYCLE_HEATPLATE = "heatPlate";
	public static final String METASEXON_PLATEHEATRECYCLE_WHEELDEPTH = "meta.section.plateHeatRecycle.WheelDepth";
	public static final String METASEXON_PLATEHEATRECYCLE_WNEWRELATIVET = "meta.section.plateHeatRecycle.WNewRelativeT";
	public static final String METASEXON_PLATEHEATRECYCLE_WNEWWETBULBT = "meta.section.plateHeatRecycle.WNewWetBulbT";
	public static final String METASEXON_PLATEHEATRECYCLE_SNEWRELATIVET = "meta.section.plateHeatRecycle.SNewRelativeT";
	public static final String METASEXON_PLATEHEATRECYCLE_SNEWWETBULBT = "meta.section.plateHeatRecycle.SNewWetBulbT";
	public static final String METASEXON_PLATEHEATRECYCLE_WNEWDRYBULBT = "meta.section.plateHeatRecycle.WNewDryBulbT";
	public static final String METASEXON_PLATEHEATRECYCLE_SNEWDRYBULBT = "meta.section.plateHeatRecycle.SNewDryBulbT";
	public static final String METASEXON_PLATEHEATRECYCLE_HEATRECYCLEDETAIL = "meta.section.plateHeatRecycle.HeatRecycleDetail";
	
	//combinedMixingChamber
	public static final String METASEXON_COMBINEDMIXINGCHAMBER = "combinedMixingChamber";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_NAME = "combinedMixingChamber.";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_PREFIX = "meta.section.combinedMixingChamber";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_EXECUTOR1 = "meta.section.combinedMixingChamber.Executor1";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WAY = "meta.section.combinedMixingChamber.way";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_NARATIO = "meta.section.combinedMixingChamber.NARatio";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_SNAVOLUME = "meta.section.combinedMixingChamber.SNAVolume";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP1 = "meta.section.combinedMixingChamber.windValveP1";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP2 = "meta.section.combinedMixingChamber.windValveP2";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH1 = "meta.section.combinedMixingChamber.windValveH1";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH2 = "meta.section.combinedMixingChamber.windValveH2";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN1 = "meta.section.combinedMixingChamber.windValveN1";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN2 = "meta.section.combinedMixingChamber.windValveN2";
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR = "meta.section.combinedMixingChamber.ISDoor";//新风侧开门
	public static final String METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR = "meta.section.combinedMixingChamber.OSDoor";//回风侧开门
	public static final String META_SECTION_COMBINEDMIXINGCHAMBER_ENABLEWINTER = "meta.section.combinedMixingChamber.EnableWinter";

	//attenuator
	public static final String METASEXON_ATTENUATOR = "attenuator";
	public static final String METASEXON_ATTENUATOR_NAME = "attenuator.";
	public static final String METASEXON_ATTENUATOR_PREFIX = "meta.section.attenuator.";
	public static final String METASEXON_ATTENUATOR_SECTIONL = "meta.section.attenuator.sectionL";
	public static final String METASEXON_ATTENUATOR_RESISTANCE = "meta.section.attenuator.Resistance";
	public static final String METASEXON_ATTENUATOR_DEADENING = "meta.section.attenuator.Deadening";//消音级数
	public static final String METASEXON_ATTENUATOR_TYPE = "meta.section.attenuator.Type";//消音器类型
	
	//ctr
	public static final String METASEXON_CTR = "ctr";
	public static final String METASEXON_CTR_NAME = "ctr.";
	public static final String METASEXON_CTR_PREFIX = "meta.section.ctr.";
	public static final String METASEXON_CTR_SECTIONL = "meta.section.ctr.sectionL";
	public static final String METASEXON_CTR_DOORO = "meta.section.ctr.DoorO";
	public static final String METASEXON_CTR_CONTROLCABINET = "meta.section.ctr.ControlCabinet";//控制柜
	public static final String METASEXON_CTR_ITAHSENSOR = "meta.section.ctr.ITaHSensor";//室内温湿度传感器
	public static final String METASEXON_CTR_ICCSENSOR = "meta.section.ctr.ICCSensor";//室内二氧化碳浓度传感器
	public static final String METASEXON_CTR_APHSENSOR = "meta.section.ctr.APHSensor";//风管静压传感器
	public static final String METASEXON_CTR_WATERVALVEACTUATOR = "meta.section.ctr.WaterValveActuator";//水阀执行器
	
	//other
	public static final String METASEXON_OTHERS_SYSSTATIC = "meta.others.sysStatic";
	public static final String METASEXON_OTHERS_RESISTANCE = "meta.others.Resistance";
	public static final String METASEXON_OTHERS_COILRESI = "meta.others.coilresi";
	public static final String METASEXON_OTHERS_FILTERRESI = "meta.others.filterresi";
	public static final String METASEXON_HUMIDIFIER = "Humidifier";
	public static final String METASEXON_HUMIDIFIER_METHOD = "meta.section.humidifier.method";
	public static final String METASEXON_HUMIDIFIER_HUMIDIFICATIONQ = "meta.section.humidifier.HumidificationQ";
	
	//ns
	public static final String METANS_NS_PREFIX = "ns.";
	public static final String METANS_ID_MEMO = "ns.section.id.MEMO";
	public static final String METANS_ID_PRICE = "ns.section.id.Price";
	public static final String METANS_ID_ENABLE = "ns.section.id.enable";
	public static final String METANS_NS_SECTION = "ns.section.";
	public static final String METANS_NS_AHU_ENABLE = "ns.ahu.enable";
	public static final String METANS_ENABLE_UNREGULAR = "Enable Unregular";
	public static final String METANS_MEMO = "ns.section.Memo";
	public static final String METANS_NS_MEMO = "NS MEMO";
	
	
	
	/*
	 * ####################其他常量区####################
	 */
	//命令command/CMD
	public static final String CMD_CHROME = "chrome";//WebBrowserPanel
    public static final String CMD_CHROME_LINE = "cmd /c start chrome ";//WebBrowserPanel
    
	//系统变量
    public static final String SYS_BLANK = "";
    public static final String SYS_BLANK_SPACE = " ";
    public static final String SYS_DOT_SEPARATOR = "\\.";

    //alphabet
    public static final String SYS_ALPHABET_A = "a";
    public static final String SYS_ALPHABET_A_UP = "A";
    public static final String SYS_ALPHABET_B = "b";
    public static final String SYS_ALPHABET_B_UP = "B";
    public static final String SYS_ALPHABET_C = "c";
    public static final String SYS_ALPHABET_C_UP = "C";
    public static final String SYS_ALPHABET_D = "d";
    public static final String SYS_ALPHABET_D_UP = "D";
    public static final String SYS_ALPHABET_E = "e";
    public static final String SYS_ALPHABET_E_UP = "E";
    public static final String SYS_ALPHABET_F = "f";
    public static final String SYS_ALPHABET_F_UP = "F";
    public static final String SYS_ALPHABET_G = "g";
    public static final String SYS_ALPHABET_G_UP = "G";
    public static final String SYS_ALPHABET_H = "h";
    public static final String SYS_ALPHABET_H_UP = "H";
    public static final String SYS_ALPHABET_I = "i";
    public static final String SYS_ALPHABET_I_UP = "I";
    public static final String SYS_ALPHABET_J = "j";
    public static final String SYS_ALPHABET_J_UP = "J";
    public static final String SYS_ALPHABET_K = "k";
    public static final String SYS_ALPHABET_K_UP = "K";
    public static final String SYS_ALPHABET_L = "l";
    public static final String SYS_ALPHABET_L_UP = "L";
    public static final String SYS_ALPHABET_M = "m";
    public static final String SYS_ALPHABET_M_UP = "M";
    public static final String SYS_ALPHABET_N = "n";
    public static final String SYS_ALPHABET_N_UP = "N";
    public static final String SYS_ALPHABET_O = "o";
    public static final String SYS_ALPHABET_O_UP = "O";
    public static final String SYS_ALPHABET_P = "p";
    public static final String SYS_ALPHABET_P_UP = "P";
    public static final String SYS_ALPHABET_Q = "q";
    public static final String SYS_ALPHABET_Q_UP = "Q";
    public static final String SYS_ALPHABET_R = "r";
    public static final String SYS_ALPHABET_R_UP = "R";
    public static final String SYS_ALPHABET_S = "s";
    public static final String SYS_ALPHABET_S_UP = "S";
    public static final String SYS_ALPHABET_T = "t";
    public static final String SYS_ALPHABET_T_UP = "T";
    public static final String SYS_ALPHABET_U = "u";
    public static final String SYS_ALPHABET_U_UP = "U";
    public static final String SYS_ALPHABET_V = "v";
    public static final String SYS_ALPHABET_V_UP = "V";
    public static final String SYS_ALPHABET_W = "w";
    public static final String SYS_ALPHABET_W_UP = "W";
    public static final String SYS_ALPHABET_X = "x";
    public static final String SYS_ALPHABET_X_UP = "X";
    public static final String SYS_ALPHABET_Y = "y";
    public static final String SYS_ALPHABET_Y_UP = "Y";
    public static final String SYS_ALPHABET_Z = "z";
    public static final String SYS_ALPHABET_Z_UP = "Z";
    
    //assert
    public static final String SYS_ASSERT_TRUE = "true";
    public static final String SYS_ASSERT_NONE = "none";
    public static final String SYS_ASSERT_FALSE = "false";
    public static final String SYS_ASSERT_TOP = "top";
    public static final String SYS_ASSERT_ALL = "all";
    public static final String SYS_ASSERT_YES = "Yes";
    public static final String SYS_ASSERT_NULL = "null";
    public static final String SYS_ASSERT_NULL_1 = "NULL";
    public static final String SYS_ASSERT_BOTTOM = "bottom";
    public static final String SYS_ASSERT_ERROR = "error";
    
    //string
    public static final String SYS_STRING_ACCESSSECTION = "Accesssection";
    public static final String SYS_STRING_DIFFUSER = "Diffuser";
    public static final String SYS_STRING_S500 = "S500";

    public static final String SYS_STRING_GET = "get";
    public static final String SYS_STRING_BASE = "Base";
    public static final String SYS_STRING_DATA = "data";
    public static final String SYS_STRING_PNAME = "pname";
    public static final String SYS_STRING_CODE = "code";
    public static final String SYS_STRING_PROJID = "projid";
    public static final String SYS_STRING_PROJECTID = "projectId";
    public static final String SYS_STRING_GROUPID1 = "groupid";
    public static final String SYS_STRING_GROUPID = "groupId";
    public static final String SYS_STRING_GROUPNAME = "groupName";
    public static final String SYS_STRING_GROUPCODE = "groupCode";
    public static final String SYS_STRING_GROUPTYPE = "groupType";
    public static final String SYS_STRING_RECORDSTATUS = "recordStatus";
    public static final String SYS_STRING_RECORDSTATUS_ARCHIEVED = "archieved";
    public static final String SYS_STRING_RECORDSTATUS_UNARCHIEVED = "unArchieved";
    public static final String SYS_STRING_FACTORYNAME = "factoryName";
    public static final String SYS_STRING_USERMETAID = "userMetaId";
    public static final String SYS_STRING_USERID = "userId";
    public static final String SYS_STRING_FACTORYID = "factoryId";
    public static final String SYS_STRING_DEFAULT = "default";
    public static final String SYS_STRING_DEFAULT_1 = "Default";
    public static final String SYS_STRING_GROUPTYPE_TP_0 = "tp0";
    public static final String SYS_STRING_NUMBER_0 = "0";
    public static final String SYS_STRING_NUMBER_1 = "1";
    public static final String SYS_STRING_NUMBER_2 = "2";
    public static final String SYS_STRING_NUMBER_3 = "3";
    public static final String SYS_STRING_NUMBER_4 = "4";
    public static final String SYS_STRING_NUMBER_5 = "5";
    public static final String SYS_STRING_NUMBER_6 = "6";
    public static final String SYS_STRING_NUMBER_7 = "7";
    public static final String SYS_STRING_NUMBER_8 = "8";
    public static final String SYS_STRING_NUMBER_9 = "9";
    public static final String SYS_STRING_MEDIALOADING_FRONTLOADING = "frontLoading";
    public static final String SYS_STRING_MEDIALOADING_FRONTLOADING_CN = "正抽";
    public static final String SYS_STRING_MEDIALOADING_SIDELOADING = "sideLoading";
    public static final String SYS_STRING_MEDIALOADING_SIDELOADING_CN = "侧抽";
    public static final String SYS_STRING_MEDIALOADING_EXTERNALLOADING = "externalLoading";
    public static final String SYS_STRING_MEDIALOADING_EXTERNALLOADING_CN = "外抽";
    public static final String SYS_STRING_WEBBROWSER_COMPONENT = "Native Web Browser component";
    public static final String SYS_STRING_WEBBROWSER_NAVIGATE = "http://www.google.com";
    public static final String SYS_STRING_WEBBROWSER_MENU_BAR = "Menu Bar";
    public static final String SYS_STRING_WEBBROWSER_SWING_TEST = "DJ Native Swing Test";
    public static final String SYS_STRING_COOLINGCOIL_ACCESSES = "coolingcoil.accesses";
    public static final String SYS_STRING_AIRVOLUME = "airVolume";
    public static final String SYS_STRING_EXTRASTATIC = "extraStatic";//其他压降
    public static final String SYS_STRING_SYSSTATIC = "sysStatic";//系统压降
    public static final String SYS_STRING_EXTERNALSTATIC = "externalStatic";//余压
    public static final String SYS_STRING_TOTALSTATIC = "totalStatic";//总静压
    public static final String SYS_STRING_POSITION = "position";
    public static final String SYS_STRING_SPRING_JD = "JD";
    public static final String SYS_STRING_SPRING_HD = "HD";
    public static final String SYS_STRING_NSSUPPLIERPRICE = "nsSupplierPrice";//非标供应商价格
    public static final String SYS_STRING_NS_FAN_PRICE = "nsFanPrice";//变更风机价格
    public static final String SYS_STRING_NS_PRESSURE_GUAGE_PRICE = "nsPressureGuagePrice";
    public static final String SYS_STRING_NS_CHANNEL_STEEL_BASE_PRICE = "nsChannelSteelBasePrice";
    public static final String SYS_STRING_NS_MOTOR_PRICE = "nsMotorPrice";
    public static final String SYS_STRING_NS_SPRING_TRANSFORM_PRICE = "nsSpringtransformPrice";
    public static final String SYS_STRING_DEFORMATION_PRICE = "deformationPrice";

    public static final String SYS_STRING_UNKNOW = "UNKNOW";
    public static final String SYS_STRING_LEFT = "left";
    public static final String SYS_STRING_RIGHT = "right";
    public static final String SYS_STRING_BILATERAL = "bilateral";
    public static final String SYS_STRING_LEFT_L = "L";
    public static final String SYS_STRING_RIGHT_R = "R";
    public static final String SYS_STRING_BOTH = "both";
    public static final String SYS_STRING_FRONT = "front";
    public static final String SYS_STRING_HAVE = "have";
    public static final String SYS_STRING_W_O = "w/o";
    public static final String SYS_STRING_RESISTANCE = "resistance";
    public static final String SYS_STRING_RESISTANCE_2 = "resistance2";
    public static final String SYS_STRING_RESISTANCE_3 = "Resistance";
    public static final String SYS_STRING_RRESISTANCE = "RResistance";//回风侧阻力
    public static final String SYS_STRING_IRESISTANCE = "IResistance";//进风侧阻力
    public static final String SYS_STRING_MIN = "min";
    public static final String SYS_STRING_MAX = "max";
    public static final String SYS_STRING_VELOCITY = "velocity";
    public static final String SYS_STRING_TEMPRETURE = "Tempreture";
    public static final String SYS_STRING_BOOLEANV = "BooleanV";
    public static final String SYS_STRING_STRINGV = "StringV";
    public static final String SYS_STRING_LONGSTRINGV = "LongStringV";
    public static final String SYS_STRING_INTV = "IntV";
    public static final String SYS_STRING_DOUBLEV = "DoubleV";
    public static final String SYS_STRING_DOUBLE2V = "Double2V";
    public static final String SYS_STRING_STEAMCOIL = "SteamCoil";
    public static final String SYS_STRING_STEAMCOIL_1 = "steamCoil";
    public static final String SYS_STRING_ROTORDIA = "rotorDia";
    public static final String SYS_STRING_TOTAL = "Total:";
    public static final String SYS_STRING_SET = "set";
    public static final String SYS_STRING_JAVA_LANG_STRING = "java.lang.String";
    public static final String SYS_STRING_JAVA_LANG_DOUBLE = "java.lang.Double";
    public static final String SYS_STRING_JAVA_DOUBLE = "double";
    public static final String SYS_STRING_JAVA_LANG_INTEGER = "java.lang.Integer";
    public static final String SYS_STRING_JAVA_INT = "int";
    public static final String SYS_STRING_JAVA_LANG_BOOLEAN = "java.lang.Boolean";
    public static final String SYS_STRING_JAVA_BOOLEAN = "boolean";
    public static final String SYS_STRING_JAVA_LANG_FLOAT = "java.lang.Float";
    public static final String SYS_STRING_JAVA_FLOAT = "float";
    public static final String SYS_STRING_RESULT = "result";
    public static final String SYS_STRING_PRB = "PRB";
    public static final String SYS_STRING_ACTION = "action";
    public static final String SYS_STRING_ACTION_ADD = "ADD";
    public static final String SYS_STRING_ACTION_COVER = "COVER";
    public static final String SYS_STRING_ACTION_SKIP = "SKIP";
    
    //int
    public static final int SYS_INT_UNIT_NO_SIZE = 4; // 39CQ1418 - 1418
    public static final int SYS_INT_DEFAULT_SECTION_LENGTH = 3; // 3M
    public static final int SYS_INT_MOLD_UNIT_SIZE = 100; // 100mm/M(100毫米/模)
    public static final int SYS_INT_HW_SIZE = 2; // 1418 - 14, 18
    public static final int SYS_INT_DEFAULT_HEIGHT = -1;
    public static final int SYS_INT_DEFAULT_WIDTH = -1;
    public static final int SYS_INT_DEFAULT_WEIGHT = -1;
    public static final int SYS_INT_LARGE_UNIT_THRESHOLD = 35;
    
    //wrappedmap
    public static final String SYS_MAP_USERNAME = "USERNAME";
    public static final String SYS_MAP_COMPUTERNAME = "COMPUTERNAME";
    public static final String SYS_MAP_USERDOMAIN = "USERDOMAIN";
    public static final String SYS_MAP_METAID = "metaId";
	public static final String SYS_MAP_POS = "pos";
	public static final String SYS_MAP_KEY = "key";
	public static final String SYS_MAP_VALUE = "value";
	public static final String SYS_MAP_NAME = "name";
	public static final String SYS_MAP_PROPS = "props";
	public static final String SYS_MAP_SECTIONS = "sections";
	public static final String SYS_MAP_AHU_KEY = "ahu.key";
	public static final String SYS_MAP_NO = "no";
	public static final String SYS_MAP_ITEM = "Item";
	public static final String SYS_MAP_COMPLETED = "completed";
	public static final String SYS_MAP_COMPLETED_1 = "Completed";
	public static final String SYS_MAP_FAN_REMARK = "fan_remark";
	public static final String SYS_MAP_FAN_REMARK_ONE = "fan_remark_one";
	public static final String SYS_MAP_FAN_REMARK_TWO = "fan_remark_two";
	public static final String SYS_MAP_FAN_TYPE_REMARK = "fan_type_remark";
	public static final String SYS_MAP_FAN_FANMODEL_SYW = "SYW";
	public static final String SYS_MAP_FAN_FANMODEL_SYQ = "SYQ";
	public static final String SYS_MAP_FAN_FANMODEL_SYD = "SYD";
	public static final String SYS_MAP_FAN_FANMODEL_RH = "RH";
	public static final String SYS_MAP_FAN_FANMODEL_FC = "FC";
	public static final String SYS_MAP_FAN_FANMODEL_BC = "BC";
	public static final String SYS_MAP_FAN_FANMODEL_ZC = "ZC";
	public static final String SYS_MAP_FAN_FANMODEL_K = "K";
	public static final String SYS_MAP_HEATINGCOIL_MODENOTE_1_2 = "28CU/Heating/Water/12.7mm/31.75x27.5mm/Smooth/Corrugated with Rippled/Steel, Cu";
	public static final String SYS_MAP_HEATINGCOIL_MODENOTE_3_8 = "28CU/Heating/Water/9.52mm/25.4x22mm/Smooth/Corrugated with Rippled/Steel, Cu";
	public static final String SYS_MAP_COOLINGCOIL_MODENOTE_1_2 = "28CW/Heating and Cooling/Water/12.7mm/31.75x27.5mm/Smooth/Corrugated with Rippled/Steel, Cu";
	public static final String SYS_MAP_COOLINGCOIL_MODENOTE_3_8 = "28CW/Heating and Cooling/Water/9.52mm/25.4x22mm/Smooth/Corrugated with Rippled/Steel, Cu";
	public static final String SYS_MAP_ROWS = "rows";
	public static final String SYS_MAP_RETURNHEATQ = "returnHeatQ";
	public static final String SYS_MAP_RETURNCOLDQ = "returnColdQ";
	public static final String SYS_MAP_FINDENSITY = "finDensity";
	public static final String SYS_MAP_CIRCUIT = "circuit";
	public static final String SYS_MAP_RESULT = "Result:";
	public static final String SYS_MAP_UNKNOWN = "UNKNOWN";
	public static final String SYS_MAP_OTHERS = "OTHERS";
	
    public static final int SYS_DEFAULT_PORTAL = 20000;//AhuServerProperties
	public static final int SYS_MAX_PORT_NUMBER = 65536;//AhuServerProperties
    
    public static final String SYS_USER_NAME = "userName";//AhuLoggerInterceptor
	public static final String SYS_USER_NAME_UNKNOWN = "unknown";//AhuLoggerInterceptor
	
	public static final String SYS_LOCAL_SERVER_PORT = "local.server.port";//AhuFrontendListener
	public static final String SYS_BEAN_WEIGHT_CALCULATOR = "weightCalculator";//AhuFrontendListener
	
	public static final int SYS_UNIT_NO_LENGTH = 3;
	
	public static final String SYS_PART_PREVIOUS = "previous";
	public static final String SYS_PART_NEXT = "next";
	
	public static final String SYS_SHUTDOWN_BAT = "shutdown.bat";
	public static final String SYS_SHUTDOWN_PID = "shutdown.pid";
	
	public static final String SYS_STOMP_ENDPOINT_ADD = "/mcendpoint";
	public static final String SYS_STOMP_SIMPLEBROKER_ENABLE = "/topic";
	
	//unit
	public static final String SYS_UNIT = "unit";
	public static final String SYS_UNIT_NAME_COPY = "Copy";
	public static final String SYS_UNIT_NAME_BREAKIN = "Breakin";
	public static final String SYS_UNIT_SPEC_SECTION_AHU = "ahu";
	public static final String SYS_UNIT_SERIES_39C = "39C";
	public static final String SYS_UNIT_SERIES_39G = "39G";
	public static final String SYS_UNIT_SERIES_39CQ = "39CQ";
	public static final String SYS_UNIT_SERIES_39XT = "39XT";
	public static final String SYS_UNIT_SERIES_39XTEC = "39XTEC";
	public static final String SYS_UNIT_SERIES_39CBFI = "39CBFI";
	public static final String SYS_UNIT_SERIES_39CBF = "39CBF";
	public static final String SYS_UNIT_SERIES_39CQEC = "39CQEC";
	public static final String SYS_UNIT_SERIES_CQ = "CQ";
	public static final String SYS_UNIT_SERIES_G = "G";
	public static final String SYS_UNIT_SERIES_XT = "XT";
	public static final String SYS_UNIT_AHU_ID = "ahuId";
	
	public static final String SYS_IMG_DATA_START ="data:image//png;base64";
	
	//CAD
	public static final String SYS_CAD_FRAME = "FRAME";
	public static final String SYS_CAD_FRAMENAME = "FRAMENAME";
	public static final String SYS_CAD_TOPFRAME = "TOPFRAME";
	public static final String SYS_CAD_BOTTOMFRAME = "BOTTOMFRAME";
	public static final String SYS_CAD_PARTSNAME = "PARTSNAME";
	public static final String SYS_CAD_PART = "PART";
	public static final String SYS_CAD_COLUMNNAME = "COLUMNNAME";
	public static final String SYS_CAD_ROW = "ROW";
	public static final String SYS_CAD_RULE = "RULE";
	public static final String SYS_CAD_LENGTH = "LENGTH";
	public static final String SYS_CAD_ALLROW = "ALLROW";
	public static final String SYS_CAD_MXB = "MXB";
	public static final String SYS_CAD_PRODUCT = "PRODUCT";
	public static final String SYS_CAD_DIMSCALE = "DIMSCALE";
	public static final String SYS_CAD_ENGLISHTEMPLATE = "ENGLISHTEMPLATE";
	public static final String SYS_CAD_BOTTOMFRAMALIGNMODE = "BOTTOMFRAMALIGNMODE";
	public static final String SYS_CAD_UNITHEIGHT = "UNITHEIGHT";
	public static final String SYS_CAD_FRMTHICK = "FRMTHICK";
	public static final String SYS_CAD_INTERVAL = "INTERVAL";
	public static final String SYS_CAD_TYPE = "TYPE";
	public static final String SYS_CAD_SECTIONL = "SECTIONL";
	public static final String SYS_CAD_TOP = "TOP";
	public static final String SYS_CAD_UNITWIDTH = "UNITWIDTH";
	public static final String SYS_CAD_BASEHEIGHT = "BASEHEIGHT";
	public static final String SYS_CAD_AIRDIRECTION = "AIRDIRECTION";
	
	//configuration
	public static final String SYS_CONFIG_ADD_VIEW_NAME_WS = "/ws";
	public static final String SYS_CONFIG_THREAD_NAME_PREFIX = "hello-";
	
	//font
	public static final String SYS_FONT_ARIAL = "Arial";
	public static final String SYS_FONT_SONGTI = "宋体";
	public static final String SYS_FONT_VERDANA = "Verdana";
	
	//format
	public static final String SYS_FORMAT_h_mm_ss = "h:mm:ss";
	public static final String SYS_FORMAT_yyMMddHHmmss = "yyMMddHHmmss";
	public static final String SYS_FORMAT_yyyy_MM_dd = "yyyy-MM-dd";
	public static final String SYS_FORMAT_HH_mm = "HH:mm";
	public static final String SYS_FORMAT_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
	public static final String SYS_FORMAT_HH_mm_ss = "HH:mm:ss";
	public static final String SYS_FORMAT_yyyyMMdd_HHmmss = "yyyyMMdd HHmmss";
	public static final String SYS_FORMAT_yyyyMMdd = "yyyyMMdd";
	public static final String SYS_FORMAT_0F = "%.0f";
	public static final String SYS_FORMAT_1F = "%.1f";
	public static final String SYS_FORMAT_2F = "%.2f";
	public static final String SYS_FORMAT_3F = "%.3f";
	public static final String SYS_FORMAT_4F = "%.4f";
	
	//language
	public static final String SYS_LANGUAGE_EN_US = "en-US";
	public static final String SYS_LANGUAGE_CN = "CN";
	public static final String SYS_LANGUAGE_EN = "EN";
	
	
	//Panel
	/** 顶面 */
	public static final String SYS_PANEL_TOP = "0";
	/** 底面 */
	public static final String SYS_PANEL_BOTTOM = "1";
	/** 右面 */
	public static final String SYS_PANEL_RIGHT = "2";
	/** 左面 */
	public static final String SYS_PANEL_LEFT = "3";
	/** 端面 */
	public static final String SYS_PANEL_FRONT = "4";
	/** 端面 */
	public static final String SYS_PANEL_BACK = "5";
	/** 普通面板 */
	public static final int SYS_PANEL_TYPE_PUTONG = 0;
	public static final String SYS_PANEL_TYPE_PUTONG_CATEGORY = "Panel";
	/** 外面板 */
	public static final int SYS_PANEL_TYPE_WAI = 1;
	public static final String SYS_PANEL_TYPE_WAI_CATEGORY = "Panel";
	/** 门面板 */
	public static final int SYS_PANEL_TYPE_MEN = 2;
	public static final String SYS_PANEL_TYPE_MEN_CATEGORY = "Door";
	/** 出风面板 */
	public static final int SYS_PANEL_TYPE_CHUFENG = 3;
	public static final String SYS_PANEL_TYPE_CHUFENG_CATEGORY = "Panel";
	/** 无面板 */
	public static final int SYS_PANEL_TYPE_WU = 4;
	public static final String SYS_PANEL_TYPE_WU_CATEGORY = "NullPanel";
	/** 加湿面板 */
	public static final int SYS_PANEL_TYPE_JIASHI = 5;
	public static final String SYS_PANEL_TYPE_JIASHI_CATEGORY = "HumiPanel";
	/** 加强面板 底面板*/
	public static final int SYS_PANEL_TYPE_JIAQIANG = 6;
	public static final String SYS_PANEL_TYPE_JIAQIANG_CATEGORY = "B Panel";
	/** 正压面板 */
	public static final int SYS_PANEL_TYPE_ZENGYA = 7;
	public static final String SYS_PANEL_TYPE_ZENGYA_CATEGORY = "Panel";
	/** 拆卸式门 */
	public static final int SYS_PANEL_TYPE_CHAIXIE = 8;
	public static final String SYS_PANEL_TYPE_CHAIXIE_CATEGORY = "Panel";
	/** 门面板(铭牌) */
	public static final int SYS_PANEL_TYPE_MEN_MING = 9;
	public static final String SYS_PANEL_TYPE_MEN_MING_CATEGORY = "Panel";
	/** 切割方向 - 无切割 */
	public static final String SYS_PANEL_CUT_DIRECTION_NONE = "none";
	/** 切割方向 - 横切 */
	public static final String SYS_PANEL_CUT_DIRECTION_HORIZONTAL = "horizontal";
	/** 切割方向 - 竖切 */
	public static final String SYS_PANEL_CUT_DIRECTION_VERTICAL = "vertical";
	public static final int SYS_PANEL_DIRECTION_NONE = 0;//没有切割
	public static final int SYS_PANEL_DIRECTION_HORIZONTAL = 1;//横向切割
	public static final int SYS_PANEL_DIRECTION_VERTICAL = 2;//纵向切割

	public static final String SYS_PANEL_JSONSPLIT_ManualID = "ManualID";//新回排中间面json切割panelid 替换token
	public static final String SYS_PANEL_DESC_HASVIEWPORT = "Has";//门面板有观察窗

	public static final String SYS_PANEL_XT_SPLIT_BY_M = "9";//XT 面板切割单位9M
	public static final int SYS_PANEL_XT_SPLIT_BY_MM = 960;//XT 面板切割单位9.6M 100mm/M(100毫米/模)
	public static final String SYS_PANEL_XT_X = "X";//X
	public static final double SYS_PANEL_18 = 0.96;//双层转轮机组大于等于18**系列箱体价格系数

	/** 类别符号 **/
	public static final String SYS_PANEL_PANELCONNECTOR_F = "F";//加强底面板，列表符号
	public static final String SYS_PANEL_PANELCONNECTOR_0 = "0";
	public static final String SYS_PANEL_PANELCONNECTOR_A = "A";
	public static final String SYS_PANEL_PANELCONNECTOR_00 = "00";
	public static final String SYS_PANEL_PANELCONNECTOR_0A = "0A";
	public static final String SYS_PANEL_PANELCONNECTOR_A0 = "A0";

	//UPGRADE
	public static final int SYS_UPGRADE_UPGRADE_DISCOVERY_PORT = 20008;
    public static final int SYS_UPGRADE_UPGRADE_PORT = 20009;
    public static final String SYS_UPGRADE_LOCAL_ADDRESS = "0.0.0.0";
    public static final String SYS_UPGRADE_BROADCAST_ADDRESS = "255.255.255.255";
    public static final int SYS_UPGRADE_RECEIVE_TIMEOUT = 30000; // 30 seconds
    public static final String SYS_UPGRADE_RELEASE_MINOR = "MINOR";
    public static final String SYS_UPGRADE_RELEASE_MAJOR = "MAJOR";
    public static final String SYS_UPGRADE_AHU_LIB_FOLDER = "lib/"; // all jar libraries
    public static final String SYS_UPGRADE_AHU_UPGRADE_FOLDER = "upgrade/"; // new release installation file
    public static final String SYS_UPGRADE_FILE_PREFIX = "carrier-ahu-update-";
    public static final String SYS_UPGRADE_UPGRADE = "UPGRADE";
    public static final String SYS_UPGRADE_LATEST_RELEASE_VERSION = "%s_LATEST_RELEASE_VERSION";
    public static final String SYS_UPGRADE_RELEASE_PROPERTY_FILE = "release.properties";
    public static final int SYS_UPGRADE_MD5_LENGTH = 32;
    public static final int SYS_UPGRADE_HEADER_LENGTH = 3;
    public static final int SYS_UPGRADE_BODY_LENGTH_LENGTH = 8;
    
	//weight
	public static final String SYS_WGT_PARTITION_PARAMETER = "partition.parameter";
	public static final String SYS_WGT_PARTITION_PARAMETER_COFF = "COFF";
	public static final String SYS_WGT_PARTITION_FRAME = "partition.frame";
	public static final String SYS_WGT_PARTITION_FACEPANEL = "partition.facePanel";
	public static final String SYS_WGT_PARTITION_SEC = "partition.sec";
	public static final String SYS_WGT_PARTITION_FACEPANEL_X = "X";
	public static final String SYS_WGT_PARTITION_PANELTBFE = "partition.panelTBFE";
	public static final String SYS_WGT_PARTITION_BASE = "partition.base";
	public static final String SYS_WGT_PARTITION_TOP = "partition.top";
	public static final String SYS_WGT_PARTITION_HYPHEN = "Partition-";
	public static final String SYS_WGT_PARTITION_WEIGHT = "分段重量-";
	public static final String SYS_WGT_PARTITION_VERSION = "1.0.0";
	public static final String SYS_WGT_ACCESSORIES_MLAMP = "accessories.mlamp";
	public static final String SYS_WGT_ACCESSORIES_UVC = "accessories.uvc";
	public static final String SYS_WGT_ACCESSORIES_DOOR = "accessories.door";
	public static final String SYS_WGT_ACCESSORIES_DOOR2 = "accessories.door2";
	public static final String SYS_WGT_ACCESSORIES_WINDOW = "accessories.window";
	public static final String SYS_WGT_ACCESSORIES_WINDOW2 = "accessories.window2";
	public static final String SYS_WGT_ACCESSORIES_PSWITCH = "accessories.pswitch";
	public static final String SYS_WGT_ACCESSORIES_PSWITCH2 = "accessories.pswitch2";
	public static final String SYS_WGT_ACCESSORIES_PMETER = "accessories.pmeter";
	public static final String SYS_WGT_ACCESSORIES_PMETER2 = "accessories.pmeter2";
	public static final String SYS_WGT_ACCESSORIES_FAC = "accessories.fac";
	
	//File
	public static final String SYS_EXCEL = "excel";
	public static final String SYS_PDF = "pdf";
	public static final String SYS_WORD = "word";
	public static final String SYS_DWG = "dwg";
	public static final String SYS_PNG = "png";
	public static final String SYS_PDF_EXTENSION = ".pdf";
	public static final String SYS_EXCEL2007_EXTENSION = ".xlsx";
	public static final String SYS_EXCEL2003_EXTENSION = ".xls";
	public static final String SYS_WORD2007_EXTENSION = ".docx";
	public static final String SYS_WORD2003_EXTENSION = ".doc";
	public static final String SYS_JAR_EXTENSION = ".jar";
	public static final String SYS_ZIP_EXTENSION = ".zip";
	public static final String SYS_LOG_EXTENSION = ".log";
	public static final String SYS_ERROR_FILE_PREFIX = "error_";
	public static final String SYS_BMP_EXTENSION = ".bmp";
	public static final String SYS_DWG_EXTENSION = ".dwg";
	public static final String SYS_PNG_EXTENSION = ".png";
	public static final String SYS_CSV_EXTENSION = ".csv";
	public static final String SYS_TXT_EXTENSION = ".txt";
	public static final String SYS_JSON_EXTENSION = ".json";
	public static final String SYS_EXPORT_JSON_EXTENSION = ".export.json";
	public static final String SYS_VERSION_EXPORT = " Export";
	
	public static final String SYS_FILE_NAME_LICENSE_DAT = "license.dat";
	
	/** excelkey列索引   **/
	public static final int KEY_COLUMN_NUM = 0;

	/** excel 老系统计算结果列索引  **/
	public static final String OLD_VALUE_FLAG = "_oldValue";
	/** excel 老系统计算结果列索引  **/
	public static final int OLD_CALC_COLUMN_NUM = 15;

	/** excel 目标计算结果列索引  **/
	public static final int CALC_COLUMN_NUM = 16;

	/** excel 新老系统结果比较列索引  **/
	public static final int CALC_OLD_NEW_COLUMN_NUM = 18;

	/** 测试案例  case excel folder */
	public static final  String TEST_CASE_FOLDER = SysConstants.ASSERT_DIR +SysConstants.TEST_CASE;
	
	//Punctuation
	public static final String SYS_PUNCTUATION_COLON = ":";//COLON	英文冒号:
	public static final String SYS_PUNCTUATION_DOT = ".";//DOT	英文句号.
	public static final String SYS_PUNCTUATION_BRACKET_OPEN = "(";//BRACKET_OPEN	英文左括号(
	public static final String SYS_PUNCTUATION_BRACKET_CLOSE = ")";//BRACKET_OPEN	英文右括号)
	public static final String SYS_PUNCTUATION_SEMICOLON = ";";//SEMICOLON 	英文分号;
	public static final String SYS_PUNCTUATION_POUND = "#";//POUND	井号#
	public static final String SYS_PUNCTUATION_STAR = "*";//STAR	星号*
	public static final String SYS_PUNCTUATION_SLASH = "/";//SLASH	斜线/
	public static final String SYS_PUNCTUATION_BACKSLASH = "\\";//BACKSLASH	反斜线\
	public static final String SYS_PUNCTUATION_DB_QUOTATION = "\"";//DB_QUOTATION 双引号"
	public static final String SYS_PUNCTUATION_COMMA = ",";//COMMA 逗号,
	public static final String SYS_PUNCTUATION_SLIGHT_PAUSE = "、";//SLIGHT_PAUSE 顿号,
	public static final String SYS_PUNCTUATION_LOW_HYPHEN = "_";//LOW_HYPHEN 下划线_
	public static final String SYS_PUNCTUATION_HYPHEN = "-";//HYPHEN 连字符-
	public static final String SYS_PUNCTUATION_AND = "&";//AND and符&
	public static final String SYS_PUNCTUATION_PLUS = "+";//PLUS 加号+
	public static final String SYS_PUNCTUATION_OR = "|";//OR 逻辑或符号|
	public static final String SYS_PUNCTUATION_PERCENTAGE = "%";//PERCENTAGE 百分号%
	public static final String SYS_PUNCTUATION_DQUOTATION = "\"";//D-QUOTATION 双引号"
	public static final String SYS_PUNCTUATION_SQUOTATION = "'";//S-QUOTATION 单引号'
	public static final String SYS_PUNCTUATION_BIGGER = ">";//BIGGER 大于号>
	public static final String SYS_PUNCTUATION_BIGGER_EQUAL = ">=";//BIGGER_EQUAL 大于等于号>=
	public static final String SYS_PUNCTUATION_LESS = "<";//LESS 小于号<
	public static final String SYS_PUNCTUATION_LESS_EQUAL = "<=";//LESS_EQUAL 小于等于号<=
	public static final String SYS_PUNCTUATION_EQUAL = "=";//EQUAL 等于号=
	public static final String SYS_PUNCTUATION_NEWLINE =  "\n";//RETURN 换行符\n
	
	public static final String SYS_PUNCTUATION_CN_DOT = "。";//DOT	中文句号。
	public static final String SYS_PUNCTUATION_CN_COLON = "：";//COLON	中文冒号：
	public static final String SYS_PUNCTUATION_CN_BRACKET_OPEN = "（";//BRACKET_OPEN	中文左括号（
	public static final String SYS_PUNCTUATION_CN_BRACKET_CLOSE = "）";//BRACKET_OPEN	中文右括号）
	public static final String SYS_PUNCTUATION_CN_SEMICOLON = "；";//SEMICOLON 	中文分号；
	public static final String SYS_PUNCTUATION_CN_COMMA = "，";//COMMA 中文逗号,
	public static final String SYS_PUNCTUATION_CN_DQUOTATION_OPEN = "“";//D-QUOTATION 中文双引号“
	public static final String SYS_PUNCTUATION_CN_DQUOTATION_CLOSE = "”";//D-QUOTATION 中文双引号”
	public static final String SYS_PUNCTUATION_CN_SQUOTATION_OPEN = "‘";//S-QUOTATION 中文单引号‘
	public static final String SYS_PUNCTUATION_CN_SQUOTATION_CLOSE = "’";//S-QUOTATION 中文单引号’
	
	//Encoding
	public static final String SYS_ENCODING_UTF8 = "utf-8";
	public static final String SYS_ENCODING_UTF8_UP = "UTF-8";
	/** 7位ASCII字符，也叫作ISO646-US、Unicode字符集的基本拉丁块 */
	public static final String SYS_ENCODING_US_ASCII = "US-ASCII";
	/** ISO 拉丁字母表 No.1，也叫作 ISO-LATIN-1 */
	public static final String SYS_ENCODING_ISO_8859_1 = "ISO-8859-1";
	/** 8 位 UCS 转换格式 */
	public static final String SYS_ENCODING_UTF_8 = "UTF-8";
	/** 16 位 UCS 转换格式，Big Endian（最低地址存放高位字节）字节顺序 */
	public static final String SYS_ENCODING_UTF_16BE = "UTF-16BE";
	/** 16 位 UCS 转换格式，Little-endian（最高地址存放低位字节）字节顺序 */
	public static final String SYS_ENCODING_UTF_16LE = "UTF-16LE";
	/** 16 位 UCS 转换格式，字节顺序由可选的字节顺序标记来标识 */
	public static final String SYS_ENCODING_UTF_16 = "UTF-16";
	/** 中文超大字符集 */
	public static final String SYS_ENCODING_GBK = "GBK";
	
    //Property
    public static final String SYS_PROPERTY_OS_NAME = "os.name";
    public static final String SYS_PROPERTY_WINDOWS = "windows";
    public static final String SYS_PROPERTY_USER_DIR = "user.dir";
    public static final String SYS_PROPERTY_LINE_SEPARATOR = "line.separator";
    public static final String SYS_PROPERTY_JNA_DEBUG_LOAD = "jna.debug_load";
    public static final String SYS_PROPERTY_JNA_LIBRARY_PATH = "jna.library.path";
    public static final String SYS_PROPERTY_ASSERTS_DLL = "asserts/dll";
    public static final String SYS_PROPERTY_JAVA_AWT_HEADLESS = "java.awt.headless";
    
    //Resource
    public static final String SYS_RESOURCE_CARRIER_LOGO = "carrier-logo-92x56.gif";
    
    //Section Alias
    public static final String SYS_SEXON_ALIAS_MIX = "A";//混合段
    public static final String SYS_SEXON_ALIAS_FILTER = "B";//过滤段
    public static final String SYS_SEXON_ALIAS_COMBINEDFILTER = "C";//综合过滤段
    public static final String SYS_SEXON_ALIAS_COOLINGCOIL = "D";//冷水盘管段
    public static final String SYS_SEXON_ALIAS_HEATINGCOIL = "E";//热水盘管段
    public static final String SYS_SEXON_ALIAS_STEAMCOIL = "F";//蒸汽盘管段
    public static final String SYS_SEXON_ALIAS_ELECTRICHEATINGCOIL = "G";//电加热盘管段
    public static final String SYS_SEXON_ALIAS_STEAMHUMIDIFIER = "H";//干蒸汽加湿段
    public static final String SYS_SEXON_ALIAS_WETFILMHUMIDIFIER = "I";//湿膜加湿段
    public static final String SYS_SEXON_ALIAS_SPRAYHUMIDIFIER = "J";//高压喷雾加湿段
    public static final String SYS_SEXON_ALIAS_FAN = "K";//风机段
    public static final String SYS_SEXON_ALIAS_COMBINEDMIXINGCHAMBER = "L";//新回排风段
    public static final String SYS_SEXON_ALIAS_ATTENUATOR = "M";//消音段
    public static final String SYS_SEXON_ALIAS_DISCHARGE = "N";//出风段
    public static final String SYS_SEXON_ALIAS_ACCESS = "O";//空段
    public static final String SYS_SEXON_ALIAS_HEPAFILTER = "V";//高效过滤段
    public static final String SYS_SEXON_ALIAS_HEATRECYCLE = "W";//热回收段
    public static final String SYS_SEXON_ALIAS_ELECTRODEHUMIDIFIER = "X";//电极加湿段
    public static final String SYS_SEXON_ALIAS_ELECTROSTATICFILTER = "Y";//静电过滤器
    public static final String SYS_SEXON_ALIAS_CTR = "Z";//控制段
    
    //Message
    public static final String SYS_MSG_NO_DATA = "No Data";
    public static final String SYS_MSG_RESPONSE_RESULT_PASS = "pass";
    public static final String SYS_MSG_RESPONSE_RESULT_MSG = "msg";
    public static final String SYS_MSG_RESPONSE_RESULT_TYPE = "type";
    public static final String SYS_MSG_RESPONSE_RESULT_ERRMSG = "errMsg";
    public static final String SYS_MSG_RESPONSE_RESULT_UNITNO = "unitNo";
    public static final String SYS_MSG_RESPONSE_RESULT_DRAWINGNO = "drawingNo";
    public static final String SYS_MSG_RESPONSE_SUCCESS = "Success";
    public static final String SYS_MSG_RESPONSE_SUCCESS_1 = "success";
    public static final String SYS_MSG_RESPONSE_FAILURE = "Failure";
    public static final String SYS_MSG_RESPONSE_FILEPATH = "filePath";
    public static final String SYS_MSG_RESPONSE_TYPE = "type";
    public static final String SYS_MSG_RESPONSE_MESSAGE = "message";
    public static final String SYS_MSG_RESPONSE_URL = "url";
    public static final String SYS_MSG_RESPONSE_INDEX_HTML = "index.html";
    public static final String SYS_MSG_RESPONSE_OK = "ok";
    public static final String SYS_MSG_RESPONSE_TEXT = "text";
    public static final String SYS_MSG_RESPONSE_CHILDREN = "children";
    public static final String SYS_MSG_RESPONSE_ERROR_FILE = "errorfile";
    public static final String SYS_MSG_RESPONSE_EMAIL = "email";
    public static final String SYS_MSG_RESPONSE_NO_DATA = "No Data";
    
    public static final String SYS_MSG_REQUEST_UNITID = "unitid";
    public static final String SYS_MSG_REQUEST_PARTITIONID = "partitionid";
    public static final String SYS_MSG_REQUEST_BASE64 = "base64";
    public static final String SYS_MSG_REQUEST_PARTITIONNUMBER = "partitionNumber";
    
    //Path
    /** 导出报告目录 **/
	public static final String SYS_REPORT_DIR = "output/report/";
	/** 报告图片资源目录 **/
	public static final String SYS_REPORT_IMG_DIR = "static/reportimg/";
	/** 报告图片资源目录 **/
	public static final String SYS_TEST_CASE = "testcase/";
	/***/
	public static final CharSequence SYS_FILES_DIR = "files/";
	public static final String SYS_LOG_DIR = "logs/";
	/** 资源目录位置 */
	public static final String SYS_ASSERT_DIR = "asserts/";
	/** 焓湿图存放目录 */
	public static final String SYS_PSYCHOMETRIC_DIR = "output/psychometric";
	/** CAD文件的存放目录 */
	public static final String SYS_CAD_DIR = "output/cad";
	/** 给类模版文件的存放目录 */
	public static final String SYS_TEMPLATE_DIR = "input/template";
	/***/
	public static final CharSequence SYS_SYSTEMVERSION = "Version 6.4.0 (20170731)";
	/** ahu导出文件名,集成ahuID */
	public static final String SYS_NAME_EXPORT_AHU = "ExportUnit.{0}.ahu";
	/** 项目导出文件名,集成项目ID */
	public static final String SYS_NAME_EXPORT_PROJECT = "ExportProject.{0}.prj";
	/** 项目导出文件名,集成分组ID */
	public static final String SYS_NAME_EXPORT_GROUP = "ExportGroup.{0}.xls";
	public static final String SYS_NAME_GUKE_BMP = "Guke.bmp";
	public static final String SYS_NAME_YLD_BMP = "XXX.BMP";
	public static final String SYS_NAME_BMP = ".BMP";
	public static final String SYS_NAME_KRUGER_CHART = "chart";
	public static final String SYS_NAME_CAD_FLAG = "Cad";
	/** 项目导出文件名,集成分组ID */
	public static final String SYS_DIR_EXPORT = "asserts/output/export/pro1/";
	/** 三视图导出文件路径,集成机组ID */
	public static final String SYS_DIR_EXPORT_THREE = "asserts/output/export/{0}/";
	/** 机组价格编码存放路径 */
	public static final String SYS_PRICE_CODE_OUTPUT = "asserts/dll/price/PartList.ini"; // fix file name PartList.ini
	public static final String SYS_TEST_PRICE_CODE_OUTPUT = "asserts/dll/price/PartList_Test.ini"; // fix file name PartList.ini
	public static final String SYS_FILE_AHU_PRICE_TXT = "asserts/dll/price/temp.txt"; // the result file of price calculation
	public static final String SYS_FILE_AHU_PRICE_PATH = "asserts/dll/price/";
	public static final int SYS_PRICE_LANGUAGE_DEFAULT = 0;
	public static final int SYS_PRICE_LANGUAGE_EXPORT = 1;
	public static final String SYS_INDEX_URL = "http://localhost:PORT/";// 首页地址
	public static final String SYS_DEFAULT_HOST = "localhost";
    public static final String SYS_KEY_PORT = "PORT";
    public static final String SYS_STARTING_URL = "Welcome.html";// 加载页地址
    public static final String SYS_VERSION_URL = "Version.html";
    public static final String SYS_INDEX_TITLE = "Carrier AHU Selection Tool";// 首页标题
    public static final String SYS_STATIC_FOLDER = "{staticFolder}";// 首页资源绝对路径token
    public static final String SYS_PATH_WELCOMPAGE_IMG = "/asserts/static/welcompageimg/";
    public static final String SYS_SESSION_PATH_PID = "pid";
    public static final String SYS_PATH_ATTACHMENT = "attachment; filename=";
    public static final String SYS_PATH_FILES = "/files/";
    public static final String SYS_PATH_TEMPLATE_EN = "files/input/tool/en/Template Import In Groups-srdc(English Vsersion).xls";//英文模板路径
    public static final String SYS_PATH_TEMPLATE_CN = "files/input/tool/cn/Template Import In Groups-srdc(Chinese Vsersion).xls";//中文模板路径
    public static final String SYS_PATH_ASSERTS_TEMPLATE_EN = "asserts/input/tool/en/Template Import In Groups-srdc(English Vsersion).xls";//英文模板路径
    public static final String SYS_PATH_ASSERTS_TEMPLATE_CN = "asserts/input/tool/cn/Template Import In Groups-srdc(Chinese Vsersion).xls";//中文模板路径
    public static final String SYS_PATH_TEMPLATE_NAME_EN = "Template Import In Groups-srdc(English Vsersion)";//英文模板文件名
    public static final String SYS_PATH_TEMPLATE_NAME_CN = "Template Import In Groups-srdc(Chinese Vsersion)";//中文模板文件名
    public static final String SYS_PATH_MAIN_RESOURCES = "/src/main/resources/";
    public static final String SYS_PATH_MANUAL_EN = "files/static/help/en/EN AHU Selection Software User Manual.pdf";//英文说明
    public static final String SYS_PATH_MANUAL_CN = "files/static/help/cn/CN AHU Selection Software User Manual.pdf";//中文说明
    public static final String SYS_PATH_RESOURCES_SECTIONS_V1 = "/src/main/resources/sections/v1";
    public static final String SYS_PATH_REDIRECT_SWAGGER_UI_HTML = "redirect:swagger-ui.html";
    public static final String SYS_PATH_REGISTRY_RESOURCE_HANDLER = "/files/**";
    public static final String SYS_PATH_REGISTRY_RESOURCE_LOCATION = "file:asserts/";
    public static final String SYS_PATH_CAD_EXPORTER_PATH = SysConstants.ASSERT_DIR + "input/bin" + File.separator + "cad_exporter.exe";
    public static final String SYS_PATH_CAD_DEST_DIR = SysConstants.ASSERT_DIR;
    public static final String SYS_PATH_INI_NAME = "UnitConfigCad.ini";
	/** 模版位置 [参数列表] */
	public static final String SYS_PATH_TEMPLATE_PATH_PARA = "asserts/input/template/template.excel.paramlist.xlsx";
	/** 模版位置 [参数列表] 英文版*/
	public static final String SYS_PATH_TEMPLATE_PATH_PARA_EN = "asserts/input/template/template.excel.paramlist.en.xlsx";
	/** 模版位置 [项目清单] */
	public static final String SYS_PATH_TEMPLATE_PATH_PROJ = "asserts/input/template/template.excel.prolist.xlsx";
	/** 模版位置 [项目清单] 英文版 */
	public static final String SYS_PATH_TEMPLATE_PATH_PROJ_EN = "asserts/input/template/template.excel.prolist.en.xlsx";
	/** 模版位置 [长期供货] */
	public static final String SYS_PATH_TEMPLATE_PATH_DELI = "asserts/input/template/template.excel.delivery.xlsx";
	/** 模版位置 [长期供货] 英文版*/
	public static final String SYS_PATH_TEMPLATE_PATH_DELI_EN = "asserts/input/template/template.excel.delivery.en.xlsx";
	/** 模版位置 [CRM清单] */
	public static final String SYS_PATH_TEMPLATE_PATH_CRMLIST = "asserts/input/template/template.excel.crmlist.xlsx";
	/** 模版位置 [CRM清单] 英文版*/
	public static final String SYS_PATH_TEMPLATE_PATH_CRMLIST_EN = "asserts/input/template/template.excel.crmlist.en.xlsx";
	/** SAP Price Code */
    public static final String SYS_PATH_TEMPLATE_PATH_SAP_PRICE_CODE = "asserts/input/template/template.sap.price.code.xls";
	/** speciallist word 模板 */
	public static final String template_word = "asserts/input/template/template.word.speciallist.docx";

	//非标
	public static final String TEMPLATE_WORD = "asserts/input/template/template.word.speciallist.docx";
	public static final String TEMPLATE_WORD_EN = "asserts/input/template/template.word.speciallist.en.docx";
	//技术说明（工程版）
	public static final String TECHPROJ_TEMPLATE_WORD = "asserts/input/template/template.word.techproj.docx";
	public static final String TECHPROJ_TEMPLATE_WORD_EN = "asserts/input/template/template.word.techproj.en.docx";

	/** 汉字字体 宋体 */
	public static final String SYS_PATH_CHINESESONGTIFONTPATH = "asserts/static/font/simsun.ttf";
	/** 汉字字体 微软雅黑 */
	public static final String SYS_PATH_CHINESEMSYHFONTPATH = "asserts/static/font/msyh.ttf";

	public static final String SYS_PATH_YUFENGDLL = "asserts/dll/yufeng/YuFengDll";
	public static final String SYS_PATH_CPC = "asserts/dll/coilwater/cpc/Dia";
	public static final String SYS_PATH_BMP_GK = "asserts/output/bmp/gk/";
	public static final String SYS_PATH_BMP_YLD = "asserts/output/bmp/yld/";
	public static final String SYS_PATH_BMP_KRUGER = "asserts/output/bmp/kruger/";
	public static final String SYS_PATH_BMP_CAD = "asserts/output/cad/";
	public static final String SYS_PATH_DX_WATER_COIL_DLL = "EvapAir1Dll.dll";//直接蒸发式
	public static final String SYS_PATH_FAN_DLL = "asserts/dll/fanyld/VSQS";
	public static final String SYS_PATH_GUKE_FAN_DLL = "asserts/dll/fangk/GukeFan.dll";//谷科风机
	public static final String SYS_PATH_KRUGER_FAN_DLL = "asserts/dll/kruger/";
	public static final String SYS_PATH_CALDENSITY_DLL = "asserts/dll/heatex/Fitting";
	public static final String SYS_PATH_HEATERX_DLL = "asserts/dll/heatex/Heatex32";
	public static final String SYS_PATH_PRICE_DLL = "asserts/dll/price/AHU_PRICE_CAL";
	public static final String SYS_PATH_PRICE_DLL_VERSION = "asserts/dll/price/%s/AHU_PRICE_CAL";
	public static final String SYS_PATH_WATER_COIL_DLL = "asserts/dll/coilwater/Sim_WCoil_THC";//冷水盘管
	public static final String SYS_PATH_PRICE_PACKAGE = "com.carrier.ahu.engine.price";
	public static final String SYS_PRICE_VERSION_20190101 = "20190101";
	public static final String SYS_PRICE_VERSION_NULL = "0";

	public static final String SYS_REST_KRUGER_URL = "http://localhost:60064/api/";
	public static final String SYS_REST_KRUGER_SELECT = "Kruger/Select";
	public static final String SYS_REST_KRUGER_CURVE_POINTS = "Kruger/CurvePoints";
	public static final String SYS_REST_KRUGER_SOUND_SPECTRUM_EX = "Kruger/SoundSpectrumEx";
	public static final String SYS_REST_KRUGER_GENERATE_CHART = "Kruger/GenerateChart";

	/* 文件读取缓冲区大小*/
	public static final String SYS_PATH_LICENSE_FILE = "license.dat";
    /* license文件放置路径 */
	public static final String SYS_PATH_LICENSE_PATH = "asserts/static/license/";// 文件加密路径
	public static final String SYS_PATH_LICENSE_DAT = "D:\\Work\\ahu_20180203\\CarrierAHU\\carrier-ahu-designer\\ahu-common\\build\\classes\\main\\license.dat";
	public static final String SYS_PATH_W_AHU_PACKAGE = "weight/ahu.package.xlsx";
	public static final String SYS_PATH_W_SECTION_ELECTRODEHUMIDIFIER = "weight/section.electrodeHumidifier.xls";
	public static final String SYS_PATH_PRICE_SPEC_PATH = "price.csv";
	public static final String SYS_PATH_PRICE_DATA_PATH = "price/data.xlsx";
	
	
	
    //TABLE数据库表
    public static final String SYS_TABLE_S_JUNLIUQIRESI = "s_junliuqiresi";//"s_junliuqiresi"
    public static final String SYS_TABLE_S_ELECTRICITYRESI = "s_electricityresi";//"s_electricityresi"
    public static final String SYS_TABLE_S_CLEARSOUNDRESI = "s_clearsoundresi";//"s_clearsoundresi"
    public static final String SYS_TABLE_S_BREAKWATERRESI_BIG = "s_breakwaterresi_big";//"s_breakwaterresi_big"
    public static final String SYS_TABLE_S_BREAKWATERRESI = "s_breakwaterresi";//"s_breakwaterresi"
    
    //Exception
    public static final String SYS_EXCEPTION_SECURITYEXCEPTION = "SecurityException";
    public static final String SYS_EXCEPTION_NOSUCHMETHODEXCEPTION = "NoSuchMethodException";
    public static final String SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION = "InvocationTargetException";
    public static final String SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION = "IllegalArgumentException";
    public static final String SYS_EXCEPTION_ILLEGALACCESSEXCEPTION = "IllegalAccessException";
    public static final String SYS_EXCEPTION_NUMBERFORMATEXCEPTION = "NumberFormatException";
    
	//日志log
	public static final String LOG_BLANK = "";
	public static final String LOG_AHU_CLOSING_INIT = "AhuClosingListener init";
	public static final String LOG_AHU_FRONTED_STARTING_LISTENER_INIT = "AhuFrontendStartingListener init";
	public static final String LOG_AHU_CONTEXT_LISTENER_INIT = "AhuContextListener init";
	public static final String LOG_FAILED_TO_VALIDATE_LICENSE_INFO = "Failed to validate license info.";
	public static final String LOG_AHU_FRONTEND_LISTENER_INIT = "AhuFrontendListener init";
	public static final String LOG_QUIT_WITHOUT_CLIENT = "Quit, since there is no client opened";
	public static final String LOG_CLIENT_BROWSER_URL ="Client brower URL ";
	public static final String LOG_SKIP_OPENNING_CLIENT_SINCE_SYSTEM_IS = "Skip openning client since the system is ";

	//Excel导入验证

	public static final String META_COOLINGCOIL_SQUALIFIEDCONDITIONS = "meta.section.coolingCoil.SqualifiedConditions";
	public static final String META_HEATINGCOIL_WQUALIFIEDCONDITIONS = "meta.section.heatingCoil.WqualifiedConditions";
	
	// info keys defined in code
	public static final String METANS_INFO_PREFIX = "info.";
    public static final String INFO_AHU_SECTION_COUNT = "info.ahu.section.count";
    public static final String INFO_AHU_SERIES = "info.ahu.series";
    public static final String INFO_AHU_WIDTH = "info.ahu.width";
    public static final String INFO_AHU_HEIGHT = "info.ahu.height";
    public static final String INFO_AHU_BOX_COUNT = "info.ahu.box.count";
    public static final String INFO_AHU_BOX_ZYDNUMBER = "info.ahu.box.ZYDNumber";
    public static final String INFO_HEATRECYCLE_TOPLAYER_SECTIONL = "info.heatRecycle.topLayer.sectionL";

}