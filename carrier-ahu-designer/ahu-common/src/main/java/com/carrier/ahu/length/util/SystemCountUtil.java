package com.carrier.ahu.length.util;

import java.math.BigDecimal;

import com.carrier.ahu.constant.CommonConstant;

/**
 * Created by liangd4 on 2017/9/27.
 */
public class SystemCountUtil {

    //机组高度
    public static int getUnitHeight(String unitModel) {
        return Integer.valueOf(unitModel.trim().substring(unitModel.trim().length() - 4, unitModel.trim().length() - 2));//机组高度;

    }

    //机组宽度
    public static int getUnitWidth(String unitModel) {
        return Integer.valueOf(unitModel.trim().substring(unitModel.trim().length() - 2, unitModel.trim().length()));//机组宽度;
    }

    //机组型号
    public static String getUnit(String unitModel) {
        return unitModel.trim().substring(unitModel.trim().length() - 4, unitModel.trim().length());//机组型号;
    }

    //机组中的G替换成CQ
    public static String getGReplaceCQ(String unitModel) {
       return unitModel.replace(CommonConstant.SYS_UNIT_SERIES_G,CommonConstant.SYS_UNIT_SERIES_CQ);
    }


    //a/b c：保留几位小数
    public static double getDivide(double a, double b, int c) {
        return new BigDecimal((float) a / b).setScale(c, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double velfacecal(double airVolume, double rotorDia, String volumeUnit) {
        switch (volumeUnit) {
            case CommonConstant.SYS_STRING_NUMBER_1:
                return airVolume / 3600 / ((Math.PI * (rotorDia / 2 / 1000) * (rotorDia / 2 / 1000)) / 2.1);
            case CommonConstant.SYS_STRING_NUMBER_2:
                return airVolume / ((Math.PI * (rotorDia / 2 / 1000) * (rotorDia / 2 / 1000)) / 2.1);
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(getUnitHeight("39CQ0608"));
        System.out.println(getUnitWidth("39CQ0608"));
    }

}
