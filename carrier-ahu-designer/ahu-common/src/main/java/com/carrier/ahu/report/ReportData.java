package com.carrier.ahu.report;

import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;

import lombok.Data;

@Data
public class ReportData {
	private Project project;
	private List<Unit> unitList;
	private Map<String, PartPO> partMap;
	private Map<String, Partition> partitionMap;
}
