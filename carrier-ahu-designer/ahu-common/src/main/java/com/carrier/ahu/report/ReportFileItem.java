package com.carrier.ahu.report;

public class ReportFileItem {
	private String name;
	private String type;
	private Boolean output;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getOutput() {
		return output;
	}

	public void setOutput(Boolean output) {
		this.output = output;
	}

	@Override
	public String toString() {
		return "ReportFileItem [name=" + name + ", type=" + type + ", output=" + output + "]";
	}

}
