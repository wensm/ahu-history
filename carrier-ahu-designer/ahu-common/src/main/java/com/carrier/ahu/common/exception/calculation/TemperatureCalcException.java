package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class TemperatureCalcException extends CalculationException {

    public TemperatureCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public TemperatureCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public TemperatureCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public TemperatureCalcException(String message) {
        super(message);
    }

}
