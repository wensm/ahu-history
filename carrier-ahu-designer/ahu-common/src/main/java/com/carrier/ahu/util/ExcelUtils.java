package com.carrier.ahu.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.extern.slf4j.Slf4j;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

@Slf4j
public class ExcelUtils {
	/** 总行数 */
	private static int totalRows = 0;

	/** 总列数 */
	private static int totalCells = 0;

	/** 错误信息 */
	private static String errorInfo;

	public int getTotalRows() {
		return totalRows;
	}

	public int getTotalCells() {
		return totalCells;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	/**
	 * 根据文件名读取excel文件
	 * 
	 * @param filePath
	 *            文件路径
	 * @param sheetNo
	 *            要读取的sheet页码，从0开始
	 * @param startRow
	 *            sheet页中有效数据起始行，从0开始
	 * @return
	 */
	public static List<List<String>> read(String filePath, Integer sheetNo, Integer startRow) {
		List<List<String>> dataLst = new ArrayList<List<String>>();
		InputStream is = null;
		try {
			/* 验证文件是否合法 */
			if (!validateExcel(filePath)) {
				System.out.println(errorInfo);
				return null;

			}
			/* 判断文件的类型，是2003还是2007 */
			boolean isExcel2003 = true;
			if (isExcel2007(filePath)) {
				isExcel2003 = false;
			}

			/* 调用本类提供的根据流读取的方法 */
			File file = new File(filePath);
			is = new FileInputStream(file);
			dataLst = read(is, isExcel2003, sheetNo, startRow);
			is.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is!=null) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		/* 返回最后读取的结果 */
		return dataLst;

	}

	/**
	 * 根据流读取Excel文件
	 * 
	 * @param inputStream
	 * @param isExcel2003
	 *            区分2003和2007版本
	 * @param sheetNo
	 *            要读取的sheet页码，从0开始
	 * @param startRow
	 *            sheet页中有效数据起始行，从0开始
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	public static List<List<String>> read(InputStream inputStream, boolean isExcel2003, Integer sheetNo,
			Integer startRow) throws IOException, EncryptedDocumentException, InvalidFormatException {
		/* 根据版本选择创建Workbook的方式 */
		Workbook wb = null;
		try {
			if (isExcel2003) {
				wb = openExcelByPOIFSFileSystem(inputStream);
			} else {
				wb = openExcelByFactory(inputStream);
			}
		} finally {
			if (!EmptyUtil.isEmpty(inputStream)) {
				inputStream.close();
			}
		}
		List<List<String>> dataList = read(wb, sheetNo, startRow);
		return dataList;
	}


	/**
	 * 根据流读取Excel文件
	 * 
	 * @param inputStream
	 * @param isExcel2003
	 *            区分2003和2007版本
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	public static Workbook read(InputStream inputStream, boolean isExcel2003)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		/* 根据版本选择创建Workbook的方式 */
		Workbook wb = null;
		if (isExcel2003) {
			wb = openExcelByPOIFSFileSystem(inputStream);
		} else {
			wb = openExcelByFactory(inputStream);
		}
		return wb;

	}

	/**
	 * 根据流读取Excel文件
	 * 
	 * @param inputStream
	 * @param isExcel2003
	 *            区分2003和2007版本
	 * @param sheetName
	 *            要读取的sheet名称
	 * @param startRow
	 *            sheet页中有效数据起始行，从0开始
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	public static List<List<String>> read(InputStream inputStream, boolean isExcel2003, String sheetName,
			Integer startRow) throws IOException, EncryptedDocumentException, InvalidFormatException {
		/* 根据版本选择创建Workbook的方式 */
		Workbook wb = null;
		try {
			if (isExcel2003) {
				wb = openExcelByPOIFSFileSystem(inputStream);
			} else {
				wb = openExcelByFactory(inputStream);
			}
		} finally {
			if (!EmptyUtil.isEmpty(inputStream)) {
				inputStream.close();
			}
		}
		List<List<String>> dataList = readSheet(wb, sheetName, startRow);
		return dataList;
	}
	
	public static List<List<String>> readSheet(Workbook wb, String sheetName, Integer startRow) {
		int sheetNo = wb.getSheetIndex(sheetName);
		if (sheetNo < 0) {
			log.error("Failed to find sheet with name : " + sheetName);
			return Collections.emptyList();
		}

		List<List<String>> dataList = read(wb, wb.getSheetIndex(sheetName), startRow);
		return dataList;
	}

	/**
	 * 读取数据
	 * 
	 * @param wb
	 * @param sheetNo
	 *            要读取的sheet页码，从0开始
	 * @param startRow
	 *            sheet页中有效数据起始行，从0开始
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private static List<List<String>> read(Workbook wb, Integer sheetNo, Integer startRow) {
		List<List<String>> dataList = new ArrayList<List<String>>();

		/** 得到shell */
		Sheet sheet = wb.getSheetAt(sheetNo);

		/** 得到Excel的行数 */
		totalRows = sheet.getPhysicalNumberOfRows();

		/** 得到Excel的列数 */
		if (totalRows >= 1 && EmptyUtil.isNotEmpty(sheet.getRow(0))) {
			totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
		}

		/** 循环Excel的行 */
		for (int r = startRow; r < totalRows; r++) {
			Row row = sheet.getRow(r);
			if (EmptyUtil.isEmpty(row)) {
				continue;
			}

			List<String> rowLst = new ArrayList<String>();
			/** 循环Excel的列 */

			for (int c = 0; c < totalCells; c++) {
				Cell cell = row.getCell(c);
				String cellValue = "";
				if (EmptyUtil.isNotEmpty(cell)) {
					// 以下是判断数据的类型
					switch (cell.getCellType()) {
					case HSSFCell.CELL_TYPE_NUMERIC: // 数字
						cellValue = cell.getNumericCellValue() + "";
						break;

					case HSSFCell.CELL_TYPE_STRING: // 字符串
						cellValue = cell.getStringCellValue();
						break;

					case HSSFCell.CELL_TYPE_BOOLEAN: // Boolean
						cellValue = cell.getBooleanCellValue() + "";
						break;

					case HSSFCell.CELL_TYPE_FORMULA: // 公式
						cellValue = cell.getCellFormula() + "";
						break;

					case HSSFCell.CELL_TYPE_BLANK: // 空值
						cellValue = "";
						break;

					case HSSFCell.CELL_TYPE_ERROR: // 故障
						cellValue = "非法字符";
						break;

					default:
						cellValue = "未知类型";
						break;
					}
				}
				rowLst.add(cellValue);
			}
			/** 保存第r行的第c列 */
			dataList.add(rowLst);
		}
		return dataList;
	}

	/**
	 * 验证excel文件
	 * 
	 * @param filePath
	 *            文件完整路径
	 * @return boolean
	 */
	public static boolean isExcelFileName(String filePath) {
		/* 检查文件名是否为空或者是否是Excel格式的文件 */
		if (EmptyUtil.isEmpty(filePath)) {
			errorInfo = "filename is empty";
			return false;
		}
		if (!(isExcel2003(filePath) || isExcel2007(filePath))) {
			errorInfo = "filename is not Excel type";
			return false;
		}
		return true;
	}

	/**
	 * 验证excel文件
	 * 
	 * @param filePath
	 *            文件完整路径
	 * @return boolean
	 */
	public static boolean validateExcel(String filePath) {
		isExcelFileName(filePath);
		/* 检查文件是否存在 */
		File file = new File(filePath);
		if (EmptyUtil.isEmpty(file)) {
			errorInfo = "File is not exist";
			return false;
		}
		if (!file.exists()) {
			errorInfo = "File is not exist";
			return false;
		}
		return true;
	}

	/**
	 * 使用WorkbookFactory 打开Excel文件
	 * 
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public static Workbook openExcelByFactory(InputStream inputStream)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		return WorkbookFactory.create(inputStream);
	}

	/**
	 * 使用POIFSFileSystem 打开Excel文件
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static Workbook openExcelByPOIFSFileSystem(InputStream inputStream) throws IOException {
		return new HSSFWorkbook(inputStream);
	}

	/**
	 * 打开加密的excel文档
	 * 
	 * @param inputStream
	 * @param passqword
	 * @return
	 * @throws IOException
	 */
	public static Workbook openEncryptedWorkBook(BufferedInputStream inputStream, String passqword) throws IOException {
		BufferedInputStream binput = new BufferedInputStream(inputStream);
		POIFSFileSystem poifs = new POIFSFileSystem(binput);
		Biff8EncryptionKey.setCurrentUserPassword(passqword);
		return new HSSFWorkbook(poifs);
	}

	/**
	 * 是否是2003的excel，返回true是2003
	 * 
	 * @param filePath
	 *            完整路径
	 * @return
	 */
	public static boolean isExcel2003(String filePath) {
		return filePath.matches("^.+\\.(?i)(xls)$");

	}

	/**
	 * 是否是2007的excel，返回true是2007
	 * 
	 * @param filePath
	 *            完整路径
	 * @return
	 */
	public static boolean isExcel2007(String filePath) {
		return filePath.matches("^.+\\.(?i)(xlsx)$");
	}

	/**
	 * 读指定Excel文件中指定表单中指定行列的单元格数据<br>
	 * 
	 * @param fileName
	 *            Excel文件名
	 * @param sheetName
	 *            sheet表单名
	 * @param rowNum
	 *            行号
	 * @param sheetName
	 *            列号
	 * @return 单元格的值
	 * @author Administrator
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public String readExcel(String fileName, String sheetName, Integer rowNum, Integer columnNum) throws IOException {
		String unitValue = null; // 初始化单元格值为空
		InputStream input = new FileInputStream(new File(fileName)); // 建立输入流
		Workbook wb = null;
		// 根据文件格式(2003或者2010)来初始化
		wb = new XSSFWorkbook(input);
		Sheet sheet = wb.getSheet(sheetName); // 获得表单
		// logger.info("开始读取Excel文件:"+fileName+"的sheet表:"+sheetName);
		// Sheet sheet = wb.getSheetAt(0); //获得第一个表单
		int rowCount = sheet.getPhysicalNumberOfRows(); // 获取总行数
		// 遍历每一行
		for (int r = 0; r < rowCount; r++) {
			Row row = sheet.getRow(r);
			int cellCount = row.getPhysicalNumberOfCells(); // 获取总列数
			// 遍历每一列
			for (int c = 0; c < cellCount; c++) {
				Cell cell = row.getCell(c);
				if (r == rowNum && c == columnNum) {
					cell.setCellType(1);
					unitValue = cell.getStringCellValue();
					// logger.info("读取Excel文件:"+fileName+"的sheet表:"+sheetName+"中第"+rowNum+"行"+"第"+columnNum+"列,获取到的值为:"+unitValue);
				} else {
					continue;// 继续遍历查找
				}
			}
		}
		if (unitValue.equals("空")) {
			// logger.info("读取Excel文件:"+fileName+"的sheet表"+sheetName+"中第"+rowNum+"行"+"第"+columnNum+"列,获取到的值为空,将返回\"\"");
			unitValue = "";
		}
		wb.close();
		return unitValue;
	}

	/**
	 * 将字符串写入指定文件的指定行列中 <br>
	 * 
	 * @param fileName
	 *            待写入的Excel路径文件名 <br>
	 * @param sheetName
	 *            sheet表单名 <br>
	 * @param unitValue
	 *            待写入的字符串 <br>
	 * @param rowNum
	 *            待写入的列号 <br>
	 * @param column
	 *            待写入的行号 <br>
	 * @return
	 * @author Administrator
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void writeExcel(String fileName, String sheetName, String unitValue, int rowNum, int column)
			throws IOException {
		InputStream input = new FileInputStream(new File(fileName)); // 建立输入流
		Workbook wb = null;
		// 根据文件格式(2003或者2010)来初始化
		wb = new XSSFWorkbook(input);
		Sheet sheet = wb.getSheet(sheetName); // 获得表单
		// Sheet sheet = wb.getSheetAt(0); //获得第一个表单
		Row row = sheet.getRow(rowNum);
		Cell cell = row.getCell(column);
		cell.setCellType(1);
		cell.setCellValue(unitValue);
		// logger.info("向Excel文件:" + fileName + "的sheet表:" + sheetName +
		// "中第" + rowNum + "行第" + column + "列写入数据:"
		// + unitValue + "成功!");

		// 写入后保存到Excel
		OutputStream os = null;
		os = new FileOutputStream(new File(fileName));
		wb.write(os);
		// logger.info("向Excel文件:" + fileName + "的sheet表:" + sheetName +
		// "写入数据完成!");
		os.close();
		wb.close();
	}

	/**
	 * 将字符串写入指定文件的指定列中 <br>
	 * 
	 * @param fileName
	 *            待写入的Excel路径文件名 <br>
	 * @param sheetName
	 *            sheet表单名 <br>
	 * @param unitValue
	 *            待写入的字符串链表<br>
	 * @param column
	 *            待写入的列号 <br>
	 * @param rowNum
	 *            从哪一行开始 <br>
	 * @return
	 * @author Administrator
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void writeExcelCol(String fileName, String sheetName, List<String> unitValue, int column, int rowNum)
			throws IOException {
		InputStream input = new FileInputStream(new File(fileName)); // 建立输入流
		Workbook wb = null;
		// 根据文件格式(2003或者2010)来初始化
		wb = new XSSFWorkbook(input);
		Sheet sheet = wb.getSheet(sheetName); // 获得表单
		// Sheet sheet = wb.getSheetAt(0); //获得第一个表单
		// 根据读取的rowNum来决定写入哪一行
		for (int r = 0; r < unitValue.size(); r++) {
			Row row = sheet.createRow(r + rowNum);
			Cell cell = row.createCell(column);
			cell.setCellType(1);
			cell.setCellValue(unitValue.get(r));
			// logger.info("向Excel文件:"+fileName+"的sheet表:"+sheetName+"中第"+(r+rowNum)+"行第"+column+"列写入数据:"+unitValue.get(r)+"成功!");
		}

		// 写入后保存到Excel
		OutputStream os = null;
		os = new FileOutputStream(new File(fileName));
		wb.write(os);
		// logger.info("向Excel文件:"+fileName+"的sheet表:"+sheetName+"写入数据完成!");
		os.close();
		wb.close();
	}

	/**
	 * 将字符串写入指定文件的指定行中 <br>
	 * 
	 * @param fileName
	 *            待写入的Excel路径文件名 <br>
	 * @param sheetName
	 *            sheet表单名 <br>
	 * @param unitValue
	 *            待写入的字符串链表 <br>
	 * @param rowNum
	 *            待写入的行号 <br>
	 * @param colNum
	 *            从哪一列开始写入 <br>
	 * @return
	 * @author Administrator
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void writeExcelRow(String fileName, String sheetName, List<String> unitValue, int rowNum, int colNum)
			throws IOException {
		InputStream input = new FileInputStream(new File(fileName)); // 建立输入流
		Workbook wb = null;
		// 根据文件格式(2003或者2010)来初始化
		wb = new XSSFWorkbook(input);
		Sheet sheet = wb.getSheet(sheetName); // 获得表单
		// Sheet sheet = wb.getSheetAt(0); //获得第一个表单
		// 根据读取的rowNum来决定写入哪一行
		Row row = sheet.createRow(rowNum);
		for (int c = 0; c < unitValue.size(); c++) {
			Cell cell = row.createCell(c + colNum);
			cell.setCellType(1);
			cell.setCellValue(unitValue.get(c));
			// logger.info("向Excel文件:" + fileName + "的sheet表:" + sheetName +
			// "中第" + rowNum + "行第" + (c + colNum)
			// + "列写入数据:" + unitValue.get(c) + "成功!");
		}

		// 写入后保存到Excel
		OutputStream os = null;
		os = new FileOutputStream(new File(fileName));
		wb.write(os);
		// logger.info("向Excel文件:" + fileName + "的sheet表:" + sheetName +
		// "写入数据完成!");
		os.close();
		wb.close();
	}

	/**
	 * 生成Workbook
	 * 
	 * @throws IOException
	 */
	public static void createExcel() throws IOException {
		String testPath = "c:\\text.xls";
		// 生成Workbook
		XSSFWorkbook wb = new XSSFWorkbook();
		// 生成Workbook OOXML形式(.xlsx)
		// XSSFWorkbook wb = new XSSFWorkbook();
		// 添加Worksheet（不添加sheet时生成的xls文件打开时会报错）
		@SuppressWarnings("unused")
		Sheet sheet1 = wb.createSheet();
		@SuppressWarnings("unused")
		Sheet sheet2 = wb.createSheet();
		@SuppressWarnings("unused")
		Sheet sheet3 = wb.createSheet("new sheet");
		@SuppressWarnings("unused")
		Sheet sheet4 = wb.createSheet("rensanning");
		// 保存为Excel文件
		FileOutputStream out = new FileOutputStream(testPath);
		wb.write(out);
		out.close();
		wb.close();
	}

    public static String getStringCellValue(Cell cell) {
        String cellValue = "";
        if (EmptyUtil.isNotEmpty(cell)) {
            if (cell.getCellTypeEnum() == CellType.STRING) {
                cellValue = cell.getStringCellValue();
            } else {
                cellValue = cell.getNumericCellValue() + "";
            }
        }
        return cellValue;
    }

}
