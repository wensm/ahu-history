package com.carrier.ahu.common.enums;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
public enum FanEnginesEnum {

    YLD("yld", "亿利达", "Yilida"),

    KRUGER("kruger", "kruger", "kruger"),

    GK("gk", "谷科", "Guke");

    private String code;//code
    private String cnName;// 中文名称
    private String enName;// 接收类名

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    FanEnginesEnum(String code, String cnName, String className) {
        this.code = code;
        this.cnName = cnName;
        this.enName = className;
    }
}
