package com.carrier.ahu.engine.fan;

import lombok.Data;

/**
 * Kruger CurvePoints Rest API response object.
 * 
 * Created by Braden Zhou on 2019/02/20.
 */
@Data
public class KrugerCurvePoints {

    private float[] VolumePoints;
    private float[] PressurePoints;
    private float[] PowerPoints;

}
