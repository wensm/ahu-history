package com.carrier.ahu.license;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Braden Zhou on 2018/04/25.
 */
public class LicenseParser {

    public static final String MAC = "mac";
    public static final String NAME = "name";
    public static final String COMPUTER_NAME = "computerName";
    public static final String DOMAIN = "domain";
    public static final String DATE = "date";
    public static final String FACTORY = "factory";
    public static final String ROLE = "role";
    public static final String VERSION = "version";
    public static final String PATCH_VERSION = "patchVersion";
    public static final String LICENSE_ATTR_SEPARATOR = ";";

    public static LicenseInfo parseUserLicenseFile(File file) {
        return parseUserLicenseFile(file.getAbsolutePath());
    }

    public static LicenseInfo parseUserLicenseFile(String licensePath) {
        try {
            String licenseString = LicenseRegister.publicKeyDecode(licensePath);
            return parseLicenseInfo(licenseString);
        } catch (Exception e) {
        }
        return null;
    }

    public static LicenseInfo parseUserLicenseFile(byte[] encodedData) {
        try {
            String licenseString = LicenseRegister.publicKeyDecode(encodedData);
            return parseLicenseInfo(licenseString);
        } catch (Exception e) {
        }
        return null;
    }

    public static LicenseInfo parseLicenseInfo(String licenseString) {
        LicenseInfo licenseInfo = new LicenseInfo();
        licenseInfo.setMac(getLicenseAttribute(MAC, licenseString));
        licenseInfo.setUserName(getLicenseAttribute(NAME, licenseString));
        licenseInfo.setComputerName(getLicenseAttribute(COMPUTER_NAME, licenseString));
        licenseInfo.setDomain(getLicenseAttribute(DOMAIN, licenseString));
        licenseInfo.setDate(getLicenseAttribute(DATE, licenseString));
        licenseInfo.setFactory(getLicenseAttribute(FACTORY, licenseString));
        licenseInfo.setRole(getLicenseAttribute(ROLE, licenseString));
        licenseInfo.setVersion(getLicenseAttribute(VERSION, licenseString));
        licenseInfo.setPatchVersion(getLicenseAttribute(PATCH_VERSION, licenseString));
        return licenseInfo;
    }

    private static String getLicenseAttribute(String attribute, String licenseString) {
        int index = licenseString.indexOf(attribute);
        if (index != -1) {
            licenseString = licenseString.substring(index);
            licenseString = licenseString.substring(attribute.length() + 1,
                    licenseString.indexOf(LICENSE_ATTR_SEPARATOR));
        } else {
            return StringUtils.EMPTY;
        }
        return "null".equals(licenseString) ? StringUtils.EMPTY : licenseString;
    }

}
