package com.carrier.ahu.common.enums;

/**
 * 国际单位制枚举
 * 
 * @author Simon
 *
 */
public enum UnitSystemEnum {
	B("B", "英制"), M("M", "公制");

	private String id;
	private String name;

	UnitSystemEnum(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static UnitSystemEnum byId(String id) {
		for (UnitSystemEnum enu : values()) {
			if (enu.getId().equals(id)) {
				return enu;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.name();
	}
}
