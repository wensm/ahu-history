package com.carrier.ahu.util;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.intl.I18NConstants;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

public class TechHeader extends PdfPageEventHelper {

    String header;
    PdfTemplate total;
    Font font;

    public TechHeader(String header, Font font) {
        this.header = header;
        this.font = font;
    }

    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(42, 16);
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void onCloseDocument(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                new Phrase(String.format(AHUContext.getIntlString(I18NConstants.TOTAL_PAGE_NUMBER_FORMAT),
                        writer.getPageNumber() - 1), font),
                2, 4, 0);
    }

    public void onStartPage(PdfWriter writer, Document document) {
        float lineHeight = 56;
        float rectHeight = 65;
        float boder = 36;
        PdfContentByte cb = writer.getDirectContentUnder();
        cb.setLineWidth(1f);
        cb.moveTo(boder, PageSize.A4.getHeight() - lineHeight);
        cb.lineTo(PageSize.A4.getWidth() - boder, PageSize.A4.getHeight() - lineHeight);
        cb.stroke();
        cb.moveTo(boder, PageSize.A4.getHeight() - lineHeight - rectHeight);
        cb.lineTo(PageSize.A4.getWidth() - boder, PageSize.A4.getHeight() - lineHeight - rectHeight);
        cb.stroke();
        cb.moveTo(boder, PageSize.A4.getHeight() - lineHeight);
        cb.lineTo(boder, PageSize.A4.getHeight() - lineHeight - rectHeight);
        cb.stroke();
        cb.moveTo(PageSize.A4.getWidth() - boder, PageSize.A4.getHeight() - lineHeight);
        cb.lineTo(PageSize.A4.getWidth() - boder, PageSize.A4.getHeight() - lineHeight - rectHeight);
        cb.stroke();

        PdfPTable table = new PdfPTable(3);
        try {
            table.setWidths(new int[] { 76, 16, 8 });
            table.setTotalWidth(527);
            table.setLockedWidth(true);
            table.getDefaultCell().setFixedHeight(20);
            table.getDefaultCell().setBorder(Rectangle.TOP);

            Phrase phrase = new Phrase(header, font);
            PdfPCell title = new PdfPCell(phrase);
            title.setBorder(Rectangle.TOP);
            title.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(title);

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(new Phrase(
                    String.format(AHUContext.getIntlString(I18NConstants.PAGE_NUMBER_FORMAT), writer.getPageNumber()),
                    font));

            PdfPCell cell = new PdfPCell(Image.getInstance(total));
            cell.setBorder(Rectangle.TOP);

            table.addCell(cell);
            table.writeSelectedRows(0, -1, 34, 20, writer.getDirectContent());
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
}
