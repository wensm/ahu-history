package com.carrier.ahu.common.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
public class AhuTemplate implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5578205257513079716L;

    @Id
    @GeneratedValue
    private String id;
    private String templateName;
    private String cnName;
    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "AHU_CONTENT", columnDefinition = "CLOB", nullable = true)
    private String ahuContent;

    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "SECTION_CONTENT", columnDefinition = "CLOB", nullable = true)
    private String sectionContent;

    @Transient
    private List<Part> parts;

}
