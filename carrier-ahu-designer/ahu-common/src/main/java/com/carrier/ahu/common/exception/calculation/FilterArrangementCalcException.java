package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/06/07.
 */
@SuppressWarnings("serial")
public class FilterArrangementCalcException extends CalculationException {

    public FilterArrangementCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public FilterArrangementCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public FilterArrangementCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public FilterArrangementCalcException(String message) {
        super(message);
    }

}
