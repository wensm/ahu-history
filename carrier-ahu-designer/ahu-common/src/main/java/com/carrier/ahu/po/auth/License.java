package com.carrier.ahu.po.auth;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by liangd4 on 2018/3/1.
 */
@SuppressWarnings("serial")
@Data
public class License implements Serializable {

    private String licenseDat;
    private String copyPath;
    private String adminEmail;
    private boolean success;

}
