package com.carrier.ahu.length.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;
import lombok.ToString;

/**
 * Created by liangd4 on 2017/10/11.
 */
@Data
@ToString
public class LengthParam {

    /*所有段必传参数*/
    private String sectionId;
    private String sectionType;//sectionType A:混合段 B:过滤段 C:综合过滤段 D:冷水盘管段 E:热水盘管段 F:蒸汽盘管段 G:电加热盘管段 H:干蒸汽加湿段 I:湿膜加湿段 J:高压喷雾加湿段 K:风机段 L:新回排风段 M:消音段 N:出风段 O:空段 V:高效过滤段 W:热回收段 X:电极加湿 Y:静电过滤器 Z:控制段
    private String version;//版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    private String serial;//机组型号
    private int sairvolume;//送风风量
    private int eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向
    private double sectionL;//段长
    private double calSectionL;//最小段长
    private double defaultSectionL;//默认段长
    private String prePartKey;//上一个段的key
    private String nextPartKey;//下一个段的key

    /*混合段*/ /*备注：mixType页面无需传入*/
    private String mixType;//混合形式 1："顶部出风" 2："后出风" 3："左侧出风" 3："右侧出风" 1："底部出风"
    private boolean uv;//是否安装检修灯
    private boolean returnTop;//顶部回风
    private boolean returnButtom;//底部回风
    private boolean returnBack;//后回风
    private boolean returnLeft;//左侧回风
    private boolean returnRight;//右侧回风


    /*单层过滤器&综合过滤段*/  /*高效过滤器 使用所有段必传参数*/ /*静电过滤器 使用所有段必传参数*/
    private String fitetF;//过滤形式 out:外抽、panel:板式、bag:袋式
    private String mediaLoading;//frontLoading:正面抽、sideLoading:侧面抽 other:其它
    private String filterEfficiency;//袋式时PM2.5过滤器 "F5", "F6", "F7", "F8", "F9", "F7(高尘容量)", "F8(高尘容量)", "F9(高尘容量)"

    /*盘管段*/
    private String eliminator;//挡水器
    private String drainpanType;//水盘型式
    private String rows;//盘管排数

    /*风机*/
    private String outlet;//风机形式 wwk:无蜗壳
    private String fanModel;//风机型号 SYW400R
    private String motorPosition;//电机位置
    private String outletDirection;//出风方向 'A': 'THF' 'B': BHF' 'C': 'UBF' 'D': 'UBR'
    private boolean standbyMotor;//备用电机
    private String motorBaseNo;//电机型號
    //private String id;//1.为标准供应商 属性来源-页面选择 2.占时没用到 页面中缺少此字段 暂时取消此字段


    /*出风段*/
    private String outType;//出风形式 T:顶部出风 A:后出风 L:左侧出风 R:右侧出风 F:底部出风
    private String OutletDirection;
    private String doorType;//开门方式 DN:开门无观察窗 DY:开门有观察窗 ND:不开门

    /*热回收*/
    private String heatRecovery;// W1:板式热回收 W2:轮式热回收

    /*空段*/
    private boolean uvLamp;//是否安装杀菌灯
    private String ODoor;//开门
    private String Function;//功能选择

    /*消音段*/
    private String Deadening;//消⾳级数
    private boolean verticalUnit;

}
