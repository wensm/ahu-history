package com.carrier.ahu.engine.watercoil;


import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.sun.jna.ptr.LongByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.carrier.ahu.engine.watercoil.Utils.dRef;
import static com.carrier.ahu.engine.watercoil.Utils.readCpc;

/**
 * Created by Administrator on 2017/3/27.
 * 盘管引擎
 */
public class WaterCoilEngine {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaterCoilEngine.class.getName());

    public static WaterCoilResult calculate(String cpcPath, double vface, double dryBulbTemperatureIn,
                                            double wetBulbTemperatureIn, double airPressureIn, double foulingFactor,
                                            double surfEff, double radMult, double dpdMult, double rawMult, double dpwMult,
                                            double lbts, double odexp, double tw, double idBend, double idHairpin, double pt,
                                            double pr, double tcond, int curveNum, double fThick, double fCond,
                                            double fpi, double waterTemperatureIn, double waterTemperatureDelta,
                                            double flowRate, int fluidId, double fluidConcentration, double height) {
        LOGGER.info("important param cpcPath:" + cpcPath + " vface:" + vface + " dryBulbTemperatureIn:" + dryBulbTemperatureIn + "wetBulbTemperatureIn:" + wetBulbTemperatureIn + "airPressureIn:" + airPressureIn);
        LOGGER.info("important param foulingFactor:" + foulingFactor + " surfEff:" + surfEff + " radMult:" + radMult + "dpdMult:" + dpdMult + "rawMult:" + rawMult);
        LOGGER.info("important param dpwMult:" + dpwMult + " lbts:" + lbts + " odexp:" + odexp + "tw:" + tw + "idBend:" + idBend);
        LOGGER.info("important param idHairpin:" + idHairpin + " pt:" + pt + " pr:" + pr + "tcond:" + tcond + "curveNum:" + curveNum);
        LOGGER.info("important param fThick:" + fThick + " fCond:" + fCond + " fpi:" + fpi + "waterTemperatureIn:" + waterTemperatureIn + "waterTemperatureDelta:" + waterTemperatureDelta);
        LOGGER.info("important param flowRate:" + flowRate + " fluidId:" + fluidId + " fluidConcentration:" + fluidConcentration + "height:" + height);
        WaterCoilResult result = null;

//        surfEff = surfEff * 0.97;//23以上 确定
//        surfEff = surfEff * 0.98;//18-23
//        surfEff = surfEff * 0.975;//18已下
//        surfEff = surfEff * 0.975;//13-18 确定
//        surfEff = surfEff * 0.97;//11 确定
//        surfEff = surfEff * 0.967;//00 确定
//        surfEff = surfEff * 0.99;//热
        try {
            byte[] errMsg = new byte[255];
            LongByReference errno = new LongByReference();
            fluidConcentration = 0.0;
            height = 0.0;
//        Native.setProtected(false);
            Thread.sleep(200);
            result = new WaterCoilResult(waterTemperatureDelta, flowRate);
            WaterCoilLib.INSTANCE.RunWaterCoil_SI(readCpc(cpcPath), dRef(vface), dRef(dryBulbTemperatureIn),
                    dRef(wetBulbTemperatureIn), dRef(airPressureIn), dRef(foulingFactor), dRef(surfEff),
                    dRef(radMult), dRef(dpdMult), dRef(rawMult), dRef(dpwMult), dRef(lbts), dRef(odexp), dRef(tw),
                    dRef(idBend), dRef(idHairpin), dRef(pt), dRef(pr), dRef(tcond), new LongByReference(curveNum), dRef(fThick),
                    dRef(fCond), dRef(fpi), dRef(waterTemperatureIn), result.waterTemperatureDelta, result.waterFlowRate,
                    dRef(fluidId), dRef(fluidConcentration), result.totalCap, result.sensibleCap, result.airDrop,
                    result.waterDrop, result.dryBulbTemperatureOut, result.wetBulbTemperatureOut, result.waterTemperatureOut,
                    dRef(height), result.bpf, result.vwbrAve, errno, errMsg);

            if (errno.getValue() != 0) {
                throw new CoilEngineException(new String(errMsg));
            }
        } catch (InterruptedException e) {
            LOGGER.error("WaterCoilEngine Thread sleep exception" + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            LOGGER.error("WaterCoilEngine Dll Cal Exception" + e.getMessage());
        }
        return result;
    }

    /**
     * 不使用矫正系数
     *
     * @return
     */
    public static WaterCoilResult calculateWithoutMult(String cpcPath, double vface, double dryBulbTemperatureIn,
                                                       double wetBulbTemperatureIn, double airPressureIn, double foulingFactor,
                                                       double surfEff, double lbts, double odexp, double tw, double idBend,
                                                       double idHairpin, double pt, double pr, double tcond, int curveNum, double fThick, double fCond,
                                                       double fpi, double waterTemperatureIn, double waterTemperatureDelta,
                                                       double flowRate, int fluidId, double fluidConcentration, double height) {
        return calculate(cpcPath, vface, dryBulbTemperatureIn, wetBulbTemperatureIn, airPressureIn, foulingFactor, surfEff,
                1, 1, 1, 1, lbts, odexp, tw, idBend, idHairpin, pt, pr, tcond, curveNum, fThick, fCond, fpi,
                waterTemperatureIn, waterTemperatureDelta, flowRate, fluidId, fluidConcentration, height);
    }


}
