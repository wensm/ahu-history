package com.carrier.ahu.psychometric;

import lombok.Data;

/**
 * Created by liujianfeng on 2017/8/30.
 */
@Data
public class PsyCalBean {
    private String sectionType;
    private double dryBulbTempA;
    private double dryBulbTempB;
    private double humidityA;
    private double humidityB;
}
