package com.carrier.ahu.report;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.intl.CnKeyTemp;

@Deprecated
public class ReportI18N {

    public static ReportI18N getInstance() {
        return instance;
    }

    public static void reload() {
        instance = new ReportI18N();
    }

    private ReportI18N() {
    }

    public String getIntlString(String cnStr, LanguageEnum locale) {
        return CnKeyTemp.getString(cnStr, locale);
    }

    /** 单例模式的机组、段属性配置类 */
    private static ReportI18N instance = new ReportI18N();

}
