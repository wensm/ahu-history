package com.carrier.ahu.common.enums;

/**
 * 批量配置类型</br>
 */
public enum CalcMethodEnum {
	Price("price", "Calculate Price", "价钱计算"),

	Length("length", "Calculate Length", "段长计算"),

	Weight("weight", "Calculate Weight", "重量计算"),

	Resistance("resistance", "Calculate Resistance", "阻力计算"),

	Temperature("temperature", "Calculate Temperature", "段长计算"),

	Humidity("humidity", "Calculate Humidity", "湿度计算");

	private String id;// id
	private String name;// 英文名称
	private String cnName;// 中文名称

	CalcMethodEnum(String id, String cnName, String name) {
		this.id = id;
		this.cnName = cnName;
		this.name = name;
	}

	public static CalcMethodEnum getById(String id) {
		for (CalcMethodEnum code : values()) {
			if (code.getId().equals(id)) {
				return code;
			}
		}
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

}
