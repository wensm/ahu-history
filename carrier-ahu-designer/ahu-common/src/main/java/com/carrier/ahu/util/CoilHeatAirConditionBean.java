package com.carrier.ahu.util;

public class CoilHeatAirConditionBean extends AirConditionBean{
	public double outputF;
	public double outputT;
	public double heatQ;
	
	public double getOutputF() {
		return outputF;
	}
	public void setOutputF(double outputF) {
		this.outputF = outputF;
	}
	public double getOutputT() {
		return outputT;
	}
	public void setOutputT(double outputT) {
		this.outputT = outputT;
	}
	public double getHeatQ() {
		return heatQ;
	}
	public void setHeatQ(double heatQ) {
		this.heatQ = heatQ;
	}

}
