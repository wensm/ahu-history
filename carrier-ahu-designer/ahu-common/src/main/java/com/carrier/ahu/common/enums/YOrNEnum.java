package com.carrier.ahu.common.enums;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
public enum YOrNEnum {
    Y("是/成功"),
    N("否/失败");

    private String cnName;//中文名称

    YOrNEnum(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return this.cnName;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
