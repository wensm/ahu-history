package com.carrier.ahu.resistance.param;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/12/15.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ResistanceBCVYBean {

    /*单层过滤段*/
    private double dinitialPD;//初始阻力
    private double deveragePD;//平均阻力
    private double dfinalPD;//终阻力

    /*综合过滤段*/
    private double dinitialPDB;//初始阻力
    private double deveragePDB;//平均阻力
    private double dfinalPDB;//终阻力
}
