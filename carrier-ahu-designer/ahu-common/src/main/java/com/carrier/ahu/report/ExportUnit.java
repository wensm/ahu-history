package com.carrier.ahu.report;

import java.util.List;

import com.carrier.ahu.common.entity.Unit;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ExportUnit extends Unit {
	private static final long serialVersionUID = 5969641593652467824L;

	private ExportPartition partition;

	private List<ExportPart> exportParts;

	private String version;//版本

}