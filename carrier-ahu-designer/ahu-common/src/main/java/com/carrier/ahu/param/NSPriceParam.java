package com.carrier.ahu.param;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NSPriceParam {

    /* 所有段必传参数 */
    private String unitid;
    private String sectionId;
    private String sectionType;// sectionType A:混合段 B:过滤段 C:综合过滤段 D:冷水盘管段 E:热水盘管段 F:蒸汽盘管段 G:电加热盘管段 H:干蒸汽加湿段
                               // I:湿膜加湿段 J:高压喷雾加湿段 K:风机段 L:新回排风段 M:消音段 N:出风段 O:空段 V:高效过滤段 W:热回收段 X:电极加湿
                               // Y:静电过滤器 Z:控制段
    private String serial;// 机组型号
    private String enable;
    private double nsheight;
    private double nswidth;
    private String ns_ahu_deformation;
    private double boxPrice;

    /* 风机 */
    private String fanModel;// 风机型号 SYW400R
    private String motorBaseNo;
    private String type;
    private String supplier;
    private String ns_section_fan_nsChangeFan;// 是否变更风机供应商
    private String ns_section_fan_nsFanSupplier;// 非标风机供应商英文名
    private String ns_section_fan_nsFanModel;// 非标风机型号

    /* 电机 */
    private String ns_section_fan_nsChangeMotor;// 是否变更电机功率
    private String ns_section_fan_nsMotorPower;// 电机功率
    private String ns_section_fan_nsMmotorPole;// 电机极数
    private String insulation;// 防护、绝缘
    
    /* 弹簧  */
    private String ns_section_fan_nsSpringtransform;

    /* 湿膜加湿段 */
    private String thickness;
    private String ns_section_wetfilmHumidifier_nsChangeSupplier;// 是否变更供应商

    /* 高压喷雾加湿段 */
    private String ns_section_sprayHumidifier_nsChangeSupplier;// 是否变更供应商
    private int hq;// 最大喷雾量

    /* 过滤段 */
    private String ns_section_filter_nsPressureGuage;
    private String ns_section_filter_nsPressureGuageModel;
    private String ns_section_combinedFilter_nsPressureGuage;
    private String ns_section_combinedFilter_nsPressureGuageModel;
    private String ns_section_HEPAFilter_nsPressureGuage;
    private String ns_section_HEPAFilter_nsPressureGuageModel;
    
    /* 钢槽底座 */
    private String ns_ahu_nsChannelSteelBase;
    private String ns_ahu_nsChannelSteelBaseModel;

}
