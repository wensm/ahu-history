package com.carrier.ahu.common.util;

import java.util.Map;

import com.carrier.ahu.util.EmptyUtil;

/**
 * Extract from ValueFormatUtil class.
 * 
 * Created by Braden Zhou on 2018/06/21.
 */
public class MapValueUtils {

    /**
     * get String Value From Map<String, Object> By Key
     * 
     * @param key
     * @param map
     * @return String
     */
    public static String getStringValue(String key, Map<String, Object> map) {
        if (EmptyUtil.isEmpty(key)) {
            return null;
        }
        if (EmptyUtil.isEmpty(map)) {
            return null;
        }
        if (!map.containsKey(key)) {
            return null;
        }
        return map.get(key).toString();
    }

    /**
     * get Integer Value From Map<String, Object> By Key
     * 
     * @param key
     * @param map
     * @return Integer
     */
    public static Integer getIntegerValue(String key, Map<String, Object> map) {
        if (EmptyUtil.isEmpty(key)) {
            return null;
        }
        if (EmptyUtil.isEmpty(map)) {
            return null;
        }

        if (!map.containsKey(key)) {
            return null;
        }
        Double d = Double.parseDouble(map.get(key).toString());
        return d.intValue();
    }

    /**
     * get Boolean Value From Map<String, Object> By Key
     * 
     * @param key
     * @param map
     * @return Boolean
     */
    public static Boolean getBooleanValue(String key, Map<String, Object> map) {
        if (EmptyUtil.isEmpty(key)) {
            return Boolean.FALSE;
        }
        if (EmptyUtil.isEmpty(map)) {
            return Boolean.FALSE;
        }

        if (!map.containsKey(key)) {
            return Boolean.FALSE;
        }
        return Boolean.parseBoolean(map.get(key).toString());
    }

    /**
     * get Double Value From Map<String, Object> By Key
     * 
     * @param key
     * @param map
     * @return Double
     */
    public static Double getDoubleValue(String key, Map<String, Object> map) {
        if (EmptyUtil.isEmpty(key)) {
            return null;
        }
        if (EmptyUtil.isEmpty(map)) {
            return null;
        }

        if (!map.containsKey(key)) {
            return null;
        }
        return Double.parseDouble(map.get(key).toString());
    }

}
