package com.carrier.ahu.po;

public class ProjectMemo {
    private String unitid;

    private String pid;

    private String value1;

    private String memo1;

    private String value2;

    private String memo2;

    private String value3;

    private String memo3;

    private String value4;

    private String memo4;

    private String value5;

    private String memo5;

    private String value6;

    private String memo6;

    private String value7;

    private String memo7;

    private String value8;

    private String memo8;

    private String value9;

    private String memo9;

    private String value10;

    private String memo10;

    private String value11;

    private String memo11;

    private String value12;

    private String memo12;

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1 == null ? null : value1.trim();
    }

    public String getMemo1() {
        return memo1;
    }

    public void setMemo1(String memo1) {
        this.memo1 = memo1 == null ? null : memo1.trim();
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2 == null ? null : value2.trim();
    }

    public String getMemo2() {
        return memo2;
    }

    public void setMemo2(String memo2) {
        this.memo2 = memo2 == null ? null : memo2.trim();
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3 == null ? null : value3.trim();
    }

    public String getMemo3() {
        return memo3;
    }

    public void setMemo3(String memo3) {
        this.memo3 = memo3 == null ? null : memo3.trim();
    }

    public String getValue4() {
        return value4;
    }

    public void setValue4(String value4) {
        this.value4 = value4 == null ? null : value4.trim();
    }

    public String getMemo4() {
        return memo4;
    }

    public void setMemo4(String memo4) {
        this.memo4 = memo4 == null ? null : memo4.trim();
    }

    public String getValue5() {
        return value5;
    }

    public void setValue5(String value5) {
        this.value5 = value5 == null ? null : value5.trim();
    }

    public String getMemo5() {
        return memo5;
    }

    public void setMemo5(String memo5) {
        this.memo5 = memo5 == null ? null : memo5.trim();
    }

    public String getValue6() {
        return value6;
    }

    public void setValue6(String value6) {
        this.value6 = value6 == null ? null : value6.trim();
    }

    public String getMemo6() {
        return memo6;
    }

    public void setMemo6(String memo6) {
        this.memo6 = memo6 == null ? null : memo6.trim();
    }

    public String getValue7() {
        return value7;
    }

    public void setValue7(String value7) {
        this.value7 = value7 == null ? null : value7.trim();
    }

    public String getMemo7() {
        return memo7;
    }

    public void setMemo7(String memo7) {
        this.memo7 = memo7 == null ? null : memo7.trim();
    }

    public String getValue8() {
        return value8;
    }

    public void setValue8(String value8) {
        this.value8 = value8 == null ? null : value8.trim();
    }

    public String getMemo8() {
        return memo8;
    }

    public void setMemo8(String memo8) {
        this.memo8 = memo8 == null ? null : memo8.trim();
    }

    public String getValue9() {
        return value9;
    }

    public void setValue9(String value9) {
        this.value9 = value9 == null ? null : value9.trim();
    }

    public String getMemo9() {
        return memo9;
    }

    public void setMemo9(String memo9) {
        this.memo9 = memo9 == null ? null : memo9.trim();
    }

    public String getValue10() {
        return value10;
    }

    public void setValue10(String value10) {
        this.value10 = value10 == null ? null : value10.trim();
    }

    public String getMemo10() {
        return memo10;
    }

    public void setMemo10(String memo10) {
        this.memo10 = memo10 == null ? null : memo10.trim();
    }

    public String getValue11() {
        return value11;
    }

    public void setValue11(String value11) {
        this.value11 = value11 == null ? null : value11.trim();
    }

    public String getMemo11() {
        return memo11;
    }

    public void setMemo11(String memo11) {
        this.memo11 = memo11 == null ? null : memo11.trim();
    }

    public String getValue12() {
        return value12;
    }

    public void setValue12(String value12) {
        this.value12 = value12 == null ? null : value12.trim();
    }

    public String getMemo12() {
        return memo12;
    }

    public void setMemo12(String memo12) {
        this.memo12 = memo12 == null ? null : memo12.trim();
    }
}