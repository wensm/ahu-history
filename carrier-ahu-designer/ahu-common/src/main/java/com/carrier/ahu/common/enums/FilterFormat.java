package com.carrier.ahu.common.enums;

/**
 * 过滤器抽取方式: 2 - 正抽, 3 - 外抽, 5 - 侧抽
 * 
 * Created by Braden Zhou on 2018/05/15.
 */
public enum FilterFormat {

    FRONT_LOADING("2"), OUT_LOADING("3"), SIDE_LOADING("5");

    private String format;

    private FilterFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

}
