package com.carrier.ahu.report;

import lombok.Data;

@Data
public class Report {
	private String type;
	private String name;
	private String path;
	private String classify;
	private boolean output;
	private ReportItem[] items;
}
