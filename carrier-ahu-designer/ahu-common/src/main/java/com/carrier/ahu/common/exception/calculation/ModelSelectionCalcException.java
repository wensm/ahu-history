package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/06/07.
 */
@SuppressWarnings("serial")
public class ModelSelectionCalcException extends CalculationException {

    public ModelSelectionCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public ModelSelectionCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public ModelSelectionCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModelSelectionCalcException(String message) {
        super(message);
    }

}
