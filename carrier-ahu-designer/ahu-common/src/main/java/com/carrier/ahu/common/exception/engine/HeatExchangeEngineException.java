package com.carrier.ahu.common.exception.engine;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class HeatExchangeEngineException extends EngineException {

    public HeatExchangeEngineException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public HeatExchangeEngineException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public HeatExchangeEngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public HeatExchangeEngineException(String message) {
        super(message);
    }

}
