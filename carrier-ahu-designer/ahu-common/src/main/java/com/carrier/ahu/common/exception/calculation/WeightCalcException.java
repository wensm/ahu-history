package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class WeightCalcException extends CalculationException {

    public WeightCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public WeightCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public WeightCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeightCalcException(String message) {
        super(message);
    }

}
