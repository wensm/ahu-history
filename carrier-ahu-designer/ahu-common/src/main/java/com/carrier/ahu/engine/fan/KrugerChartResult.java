package com.carrier.ahu.engine.fan;

import lombok.Data;

/**
 * Kruger generate chart result.
 * 
 * Created by Braden Zhou on 2019/02/25.
 */
@Data
public class KrugerChartResult {

    private String chartFilePath;
    private boolean success;

}
