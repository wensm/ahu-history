package com.carrier.ahu.calculator.price;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.engine.price.AHUPriceCalFactory;
import com.carrier.ahu.vo.SysConstants;

public class PriceCalRunner extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(PriceCalRunner.class.getName());

    // input
    private final static BlockingQueue<String> bq = new ArrayBlockingQueue<String>(1);
    // output
    private final static BlockingQueue<Map<String, String>> temQueue = new ArrayBlockingQueue<Map<String, String>>(100);

    private boolean flg = true;
    private boolean runningFlg = false;
    private String priceBase;
    private int language;

    private PriceCalRunner() {
    }

    private final static PriceCalRunner runner = new PriceCalRunner();

    public static PriceCalRunner getInstance() {
        return runner;
    }

    public void begin(String priceBase, int language) {
        if (!PriceCalRunner.getInstance().runningFlg) {
            PriceCalRunner.getInstance().runningFlg = true;
            PriceCalRunner.getInstance().priceBase = priceBase;
            PriceCalRunner.getInstance().language = language;
            PriceCalRunner.getInstance().setDaemon(true);
            PriceCalRunner.getInstance().start();
        }
    }

    @SuppressWarnings("static-access")
    public void run() {
        Map<String, String> map = null;
        File priceResultFile = new File(String.format(SysConstants.FILE_AHU_PRICE_TXT, priceBase));
        while (PriceCalRunner.getInstance().flg) {
            try {
                map = new HashMap<String, String>();
                PriceCalRunner.getInstance().runningFlg = true;
                String key = bq.take();
                if (priceResultFile.exists()) {
                    priceResultFile.delete();
                }
                try {
                    AHUPriceCalFactory.getAHUPriceCal(priceBase).PriceCAL(language);
                    Thread.sleep(500);
                    String destFileName = SysConstants.FILE_AHU_PRICE_PATH.concat(key);
                    if (priceResultFile.exists()) {
                        priceResultFile.renameTo(new File(destFileName));
                        map.put(key, destFileName);
                    }
                } catch (Error e) {
                    logger.error(e.getMessage(), e);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                PriceCalRunner.getInstance().temQueue.clear();
                PriceCalRunner.getInstance().temQueue.put(map);
            } catch (InterruptedException e) {
                logger.error("Error in Price RUNNER", e);
            }
        }
    }

    public boolean end() {
        PriceCalRunner.getInstance().flg = true;
        PriceCalRunner.getInstance().runningFlg = false;
        return true;
    }

    public void setInput(String timestamp) {
        try {
            bq.put(timestamp);
        } catch (InterruptedException e) {
            logger.error("Error in Price RUNNER to set input", e);
        }
    }

    public String getOutput(String key) {
        try {

            // 1: 消费所有消息，如果有满足的key返回结果。
            for (Map<String, String> stringStringMap : temQueue) {
                String object = stringStringMap.get(key);
                if (null == object) {
                    temQueue.poll();// 消费多余key
                } else {
                    return object;
                }
            }

            // 2: 如果所有key 消费结束还没有结果,等待30秒
            Map<String, String> result = temQueue.poll(30, TimeUnit.SECONDS);
            if (result != null) {
                String object = result.get(key);
                return object;
            }

        } catch (InterruptedException e) {
            logger.error("Error in price calculation to get output", e);
        }
        return null;
    }
}
