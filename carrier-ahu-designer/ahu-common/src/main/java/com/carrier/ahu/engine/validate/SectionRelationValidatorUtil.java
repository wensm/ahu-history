package com.carrier.ahu.engine.validate;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.calculation.ValidateCalcException;
import com.carrier.ahu.param.ValidateParam;
import com.carrier.ahu.vo.SystemCalculateConstants;

import static com.carrier.ahu.common.intl.I18NConstants.FAN_WWK_CALCULATION_FAILED;

/**
 * 段关系校验
 */
public final class SectionRelationValidatorUtil {

    /**
     * 确认按钮校验最新段前后顺序是否符合规则
     * @throws ApiException
     * @param validateParam
     */
    public static void confirmValidate(ValidateParam validateParam) throws ApiException {

        //1： 无蜗壳风机前必须保留进风段(空段、混合段)
        if (SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(validateParam.getOutlet())
                && (null != validateParam.getPrePartKey() && !SectionTypeEnum.getSectionTypeFromId(validateParam.getPrePartKey()).equals(SectionTypeEnum.TYPE_MIX))
                && (null != validateParam.getPrePartKey() && !SectionTypeEnum.getSectionTypeFromId(validateParam.getPrePartKey()).equals(SectionTypeEnum.TYPE_ACCESS))
                ) {
            throw new ValidateCalcException(FAN_WWK_CALCULATION_FAILED);
        }


    }
}
