package com.carrier.ahu.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * 判断对象是否为空
 * 
 * @author Rex
 * 
 */
public class EmptyUtil {
    /**
     * 判断对象为空
     * 
     * @param obj 对象名
     * @return 是否为空
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Object obj) {
        if (null == obj) {
            return true;
        }
        if ((obj instanceof List)) {
            return ((List) obj).size() == 0;
        }
        if ((obj instanceof Map)) {
            return ((Map) obj).size() == 0;
        }
        if ((obj instanceof String)) {
            return StringUtils.isBlank((String) obj);
        }
        return false;
    }

    /**
     * 判断数组为空
     * 
     * @param obj 数组名
     * @return 是否为空
     */
    public static boolean isEmpty(Object[] obj) {
        if (null == obj) {
            return true;
        }
        if (obj.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * 判断对象不为空
     * 
     * @param obj 对象名
     * @return 是否不为空
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 判断数组不为空
     * 
     * @param obj 数组名
     * @return 是否不为空
     */
    public static boolean isNotEmpty(Object[] obj) {
        return !isEmpty(obj);
    }

    public static String toString(Object obj, String def) {
        if (isEmpty(obj)) {
            return def;
        }
        return String.valueOf(obj);
    }

}
