package com.carrier.ahu.common.model.partition;

import lombok.Data;

@Data
public class FrameLine {

	/** 普通中框 */
	public static final String TYPE_PUTONG = "0";
	public static final String TYPE_PUTONG_CATEGORY = "InnerFrame";
	/** 加强中框 */
	public static final String TYPE_JIAQIANG = "1";
	public static final String TYPE_JIAQIANG_CATEGORY = "StrFrame";
	/** 内面板框 */
	public static final String TYPE_NEI = "2";
	public static final String TYPE_NEI_CATEGORY = "InnerFrame";
	/** 组合中框 */
	public static final String TYPE_ZUHE = "3";
	public static final String TYPE_ZUHE_CATEGORY = "InnerFrame";


	/** 底座 **/
	public static final String TYPE_BASE_CATEGORY = "Base";
	/** 框条 **/
	public static final String TYPE_FRAME = "Frame";
	/** Y 接角 **/
	public static final String TYPE_YCROSS_CATEGORY = "YCross";
	/** 底框条 **/
	public static final String TYPE_BOTTOMFRAME_CATEGORY = "BaseFrame";
	/** 顶框条 **/
	public static final String TYPE_TOPFRAME_CATEGORY = "BaseFrame";
	/** 高框条 **/
	public static final String TYPE_HEIGHTFRAME_CATEGORY = "BaseFrame";

	/** 线连接器 0,A,B (竖切继承面板panelConnector第一位；横切继承面板panelConnector 第二位) */
	private String lineConnector = "";


	// private String id;
	private String type;
	private int lineLength;

	public FrameLine() {
	}

	public FrameLine(String type, int length) {
		this.type = type;
		this.lineLength = length;
	}
}
