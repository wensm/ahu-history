package com.carrier.ahu.report;

import java.util.List;

public class ReportRequest {
	private String ahuId;
	private String projectId;
	private List<ReportFile> reports;

	public String getAhuId() {
		return ahuId;
	}

	public void setAhuId(String ahuId) {
		this.ahuId = ahuId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<ReportFile> getReports() {
		return reports;
	}

	public void setReports(List<ReportFile> reports) {
		this.reports = reports;
	}

	@Override
	public String toString() {
		return "ReportRequest [ahuId=" + ahuId + ", projectId=" + projectId + ", reports=" + reports + "]";
	}

}
