package com.carrier.ahu.calculator.price;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.vo.SysConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalPriceTool {

	/**
	 * 将AHU的价格编码输出到文件
	 * 
	 * @param fileName
	 * @param codeJson
	 *            [{"sectionKey":"CommonConstant.SYS_PUNCTUATION_COMMAsort":"CommonConstant.SYS_PUNCTUATION_COMMApriceCode":""}]
	 * @throws IOException
	 */
    public static void output2File(String priceBase, String codeJson) throws IOException {
        File file = new File(String.format(SysConstants.PRICE_CODE_OUTPUT, priceBase));
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (file.exists()) {
            file.delete();
        }

        file.createNewFile();
        FileOutputStream o = new FileOutputStream(file);
        o.write(codeJson.getBytes(Charset.forName(CommonConstant.SYS_ENCODING_UTF8_UP)));
        o.close();
    }

	/**
	 * 将集合转化成字符串输出
	 * 
	 * @param codeList
	 * @return
	 */
	public static String transList2String(LinkedList<String> codeList) {
		StringBuffer sb = new StringBuffer();
		for (String codeChar : codeList) {
			sb.append(codeChar);
		}
		return sb.toString();
	}

	/**
	 * Ascii对应表<br>
	 * 0-9 48-57 <br>
	 * A-Z 65-90 <br>
	 * a-z 97-122 <br>
	 */
	public static String sectionL2String(int sectionL) {
		if (sectionL > -1 && sectionL < 10) {
			return String.valueOf(byteAsciiToChar(sectionL + 48));
		} else if (sectionL > 10 && sectionL < 36) {
			return String.valueOf(byteAsciiToChar(sectionL + 65 - 11));
		} else {
			return String.valueOf(sectionL);
		}
	}

	/**
	 * ascii转换为char 直接int强制转换为char
	 * 
	 * @param ascii
	 * @return
	 */
	private static char byteAsciiToChar(int ascii) {
		char ch = (char) ascii;
		return ch;
	}

    /**
     * Call AHU price cal dll to get price for each section and box.
     * 
     * @param priceBase
     * @param language
     * @return
     */
    public static Map<String, Double> calPrice(String priceBase, LanguageEnum language) {
        String filePath;
        Map<String, Double> priceMap = new HashMap<String, Double>();
        try {
            // call price calculation
        	String key = KeyGenerator.getTimeStampSequence();
            PriceCalRunner.getInstance().begin(priceBase, getPriceLanguage());
            PriceCalRunner.getInstance().setInput(key);// seems no difference if pass other language 
            filePath=PriceCalRunner.getInstance().getOutput(key);
        } catch (Exception e) {
            log.error("Failed to call price dll", e);
            throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        }

        if(StringUtils.isBlank(filePath)) {
//        	throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        	return priceMap;
        }
        File priceResultFile = new File(filePath);
        if (priceResultFile.exists()) { // new price file exists
            try (BufferedReader reader = new BufferedReader(new FileReader(priceResultFile))) {
                String line = reader.readLine();
                while (line != null) {
                    if (!line.contains(CommonConstant.SYS_MAP_RESULT)) { // skip first line
                        String[] priceStrs = line.split(CommonConstant.SYS_PUNCTUATION_COMMA);
                        priceMap.put(priceStrs[0], Double.valueOf(priceStrs[1]));
                    }else {
                    		String[] priceStrs = line.split(CommonConstant.SYS_PUNCTUATION_COLON);
                    		priceMap.put(CommonConstant.SYS_MAP_RESULT, Double.valueOf(priceStrs[1]));
                    }
                    line = reader.readLine();
                }
            } catch (Exception e) {
                log.error("Failed to parse price data.");
            }finally {
            	if(priceResultFile!=null&&priceResultFile.exists()) {
            		priceResultFile.delete();
            	}
            }
        }
        return priceMap;
    }

    private static int getPriceLanguage() {
        if (AHUContext.isExportVersion()) {
            return CommonConstant.SYS_PRICE_LANGUAGE_EXPORT;
        }
        return CommonConstant.SYS_PRICE_LANGUAGE_DEFAULT;
    }

}
