package com.carrier.ahu.common.upgrade;

/**
 * Action types when transfer release file.
 * 
 * Created by Braden Zhou on 2018/07/18.
 */
public enum MessageType {

    LOGIN, DONE, CHECK_UPDATE, RELEASE, DOWNOADING, RELEASE_MINOR, RELEASE_MAJOR, READY, ERROR, CONFIRM, PROGRESS

}
