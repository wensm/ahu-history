package com.carrier.ahu.engine.heatrecycle;

import lombok.Data;

@Data
public class DRIBean {
    private double DRIoutSaEfficiencySummerTotal;
    private double DRIoutSaEfficiencyWinterTotal;
    private double DRIoutSaPDropWinter;
    private String DRIOutECOFRESHModel;
}
