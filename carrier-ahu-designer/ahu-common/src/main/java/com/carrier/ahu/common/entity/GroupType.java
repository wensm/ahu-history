package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.carrier.ahu.po.AbstractPo;

/**
 * Created by Simon on 2018-01-27.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class GroupType extends AbstractPo {
	private static final long serialVersionUID = -172700562234954895L;
	@Id
	String id;// 主键ID
	String name;//分组类型名称
	String code;// 组编号
	String projectId;// 项目ID
}
