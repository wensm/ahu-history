package com.carrier.ahu.po.model;

import java.util.List;

/**
 * Describe a group info, which is designed to organize the AHUs.
 * 
 * @author JL
 *
 */
public class Group extends AbstractModel {
	private static final long serialVersionUID = -3303689439011378676L;
	private String name;
	private List<String> ahus;

	public List<String> getAhus() {
		return ahus;
	}

	public void setAhus(List<String> ahus) {
		this.ahus = ahus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
