package com.carrier.ahu.engine.heatrecycle;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

/**
 * Caldensity engine
 */
public interface CaldensityLib extends StdCallLibrary {

    CaldensityLib CALDENSITY_INSTANCE = (CaldensityLib) Native.loadLibrary(CommonConstant.SYS_PATH_CALDENSITY_DLL, CaldensityLib.class);

    double CaldensityBritish(double attitude, double warmTemp);


}
