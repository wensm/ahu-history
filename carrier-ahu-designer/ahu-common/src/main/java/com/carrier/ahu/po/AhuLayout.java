package com.carrier.ahu.po;

import lombok.Data;

/**
 * 描述一个AHU的布局信息
 * 
 * @author liujianfeng
 *
 */
@Data
public class AhuLayout {

    public static final int TOP_LEFT = 0;
    public static final int TOP_RIGHT = 1;
    public static final int BOTTOM_LEFT = 2;
    public static final int BOTTOM_RIGHT = 3;

    /**
     * 10: 通用类型 11: 新回排风 21: 轮式 22: 板式
     */
    private int style;
    private int[][] layoutData;

}
