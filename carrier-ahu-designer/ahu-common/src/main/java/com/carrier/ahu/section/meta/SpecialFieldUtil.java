package com.carrier.ahu.section.meta;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.unit.BaseDataUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_A;

/**
 * Created by liangd4 on 2018/3/8.
 */
public class SpecialFieldUtil {

    private static final String airDirection = CommonConstant.METASEXON_AIRDIRECTION;//风向
    private static final String outletDirection = CommonConstant.METASEXON_DISCHARGE_OUTLETDIRECTION;//出风形式
    private static final String returnBack = CommonConstant.METASEXON_MIX_RETURNBACK;//后回风
    private static final String returnButtom = CommonConstant.METASEXON_MIX_RETURNBUTTOM;//底部回风
    private static final String returnLeft = CommonConstant.METASEXON_MIX_RETURNLEFT;//左侧回风
    private static final String returnRight = CommonConstant.METASEXON_MIX_RETURNRIGHT;//右侧回风
    private static final String returnTop = CommonConstant.METASEXON_MIX_RETURNTOP;//顶部回风

    /*全年节能特殊字段封装*/
    public static String packageFullYearField(String sectionContent) {
        List<Part> partList = JSONArray.parseArray(sectionContent, Part.class);
        List<Part> newPartList = new ArrayList<Part>();
        for (Part part : partList) {//封装功能段的特殊字段
            String metaJson = part.getMetaJson();
            Map<String, Object> newMap = JSONArray.parseObject(metaJson, Map.class);
            String position = BaseDataUtil.constraintString(part.getPosition());
            /*上层回风、下层送风*/
            if (position.equals("1")) {//根据所在位置封装字段
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("2")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("3")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("4")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("5")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("6")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("7")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            part.setMetaJson(JSONArray.toJSONString(newMap));
            newPartList.add(part);//重新封装新的partList
        }
        return JSONArray.toJSONString(newPartList);
    }

    /*转轮特殊字段封装*/
    public static String packageWheelField(String sectionContent) {
        List<Part> partList = JSONArray.parseArray(sectionContent, Part.class);
        List<Part> newPartList = new ArrayList<Part>();
        for (Part part : partList) {//封装功能段的特殊字段
            String metaJson = part.getMetaJson();
            Map<String, Object> newMap = JSONArray.parseObject(metaJson, Map.class);
            String position = BaseDataUtil.constraintString(part.getPosition());
            /*上层回风、下层送风*/
            if (position.equals("1")) {//根据所在位置封装字段
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
                newMap.put(outletDirection, DISCHARGE_OUTLETDIRECTION_A);
            } else if (position.equals("2")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("3")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("4")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
                newMap.put(returnBack, false);
                newMap.put(returnButtom, true);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, false);
            } else if (position.equals("5")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("6")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
                newMap.put(returnBack, true);
                newMap.put(returnButtom, false);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, false);
            } else if (position.equals("7")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
                newMap.put(returnBack, true);
                newMap.put(returnButtom, false);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, false);
            } else if (position.equals("8")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("9")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("10")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("11")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
                newMap.put(returnBack, false);
                newMap.put(returnButtom, false);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, true);
            } else if (position.equals("12")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("13")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("14")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("15")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            part.setMetaJson(JSONArray.toJSONString(newMap));
            newPartList.add(part);//重新封装新的partList
        }
        return JSONArray.toJSONString(newPartList);
    }

    /*板式特殊字段封装*/
    public static String packagePlateField(String sectionContent) {
        List<Part> partList = JSONArray.parseArray(sectionContent, Part.class);
        List<Part> newPartList = new ArrayList<Part>();
        for (Part part : partList) {//封装功能段的特殊字段
            String metaJson = part.getMetaJson();
            Map<String, Object> newMap = JSONArray.parseObject(metaJson, Map.class);
            String position = BaseDataUtil.constraintString(part.getPosition());
            /*右上左下回风、左上右下送风*/
            if (position.equals("1")) {//根据所在位置封装字段
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
                newMap.put(returnBack, true);
                newMap.put(returnButtom, false);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, false);
            } else if (position.equals("2")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("3")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("4")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("5")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
                newMap.put(returnBack, true);
                newMap.put(returnButtom, false);
                newMap.put(returnLeft, false);
                newMap.put(returnRight, false);
                newMap.put(returnTop, false);
            } else if (position.equals("6")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("7")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("8")) {
                newMap.put(airDirection, AirDirectionEnum.RETURNAIR.getCode());
            } else if (position.equals("9")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("10")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("11")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            } else if (position.equals("12")) {
                newMap.put(airDirection, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            part.setMetaJson(JSONArray.toJSONString(newMap));
            newPartList.add(part);//重新封装新的partList
        }
        return JSONArray.toJSONString(newPartList);
    }
}
