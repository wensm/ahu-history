package com.carrier.ahu.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/14.
 */
@Data
public class HeatQCalParam {
    /*机组参数*/
    private String serial;//机组型号
    private double sairvolume;//送风风量
    private double eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向
    /*进风温度*/
    private double InDryBulbT;//干球温度
    private double InWetBulbT;//湿球温度
    private double InRelativeT;
    /*条件*/
    private double OutDryBulbT;//目标干球温度
    private double HeatQ;//目标热量
    
    private boolean EnableWinter;
    
    private double HumDryBulbT;
}
