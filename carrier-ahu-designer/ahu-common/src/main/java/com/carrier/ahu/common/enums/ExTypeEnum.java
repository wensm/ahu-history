package com.carrier.ahu.common.enums;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
public enum ExTypeEnum {

    EX2SYDR("SYDR", "亿利达SYDR"),// SYDR
    EX2SYDK("SYDK", "亿利达SYDK"); //SYDK

    private String code;//值
    private String name;//名称

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    ExTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
