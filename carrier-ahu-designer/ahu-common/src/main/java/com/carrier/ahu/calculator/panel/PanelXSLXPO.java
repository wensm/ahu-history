package com.carrier.ahu.calculator.panel;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PanelXSLXPO {
	private String serial;
	private String sectionTypeNo;
	private String h;
	private String w;
	private String hr;//横切
	private String hp;
	private String wr;//竖切
	private String wp;
	private String partlen;
	private String hw;

	//额外计算参数
    private String sectionMetaId;//section metaid
    private String sectionDoorOpendAndViewport;//段是否开门/观察窗格式：（true;true）

}
