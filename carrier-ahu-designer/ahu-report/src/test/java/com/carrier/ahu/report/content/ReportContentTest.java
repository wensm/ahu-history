package com.carrier.ahu.report.content;

import static org.junit.Assert.*;

import org.junit.Test;

import com.carrier.ahu.report.common.ReportConstants;

/**
 * Created by Braden Zhou on 2018/10/30.
 */
public class ReportContentTest {

    @Test
    public void testGetReportContent() {
        String[][] content = ReportContent.getReportContent(ReportConstants.DEFAULT_ALIGNMENT);
        assertNotNull(content);
    }

}
