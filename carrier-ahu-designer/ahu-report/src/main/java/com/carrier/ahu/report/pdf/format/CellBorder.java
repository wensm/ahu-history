package com.carrier.ahu.report.pdf.format;

import java.util.List;

import lombok.Data;

/**
 * 边框参数 以上下左右四个边框对应四个'0'、'1'代码，形成16进制字符 </br>
 * 例 0->0000->无边框；1->0001->只有右边框；非正常格式则为无边框 </br>
 * 
 * Created by Braden Zhou on 2018/10/24.
 */
@Data
public class CellBorder {

    private String border;
    private List<String> positions;

}
