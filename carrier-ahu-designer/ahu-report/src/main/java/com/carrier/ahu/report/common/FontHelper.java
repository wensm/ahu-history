package com.carrier.ahu.report.common;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import lombok.extern.slf4j.Slf4j;

import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_CHINESEMSYHFONTPATH;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_CHINESESONGTIFONTPATH;

/**
 * Created by Braden Zhou on 2018/10/24.
 */
@Slf4j
public class FontHelper {

    private static BaseFont BASE_FONT_SIMSUN = null;
    private static BaseFont BASE_FONT_MSYH = null;

    static {
        try {
            BASE_FONT_SIMSUN = BaseFont.createFont(SYS_PATH_CHINESESONGTIFONTPATH, BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED);
            BASE_FONT_MSYH = BaseFont.createFont(SYS_PATH_CHINESEMSYHFONTPATH, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.error("Failed to create font for PDF report.", e);
        }
    }

    public static Font getFont(FontEnum fontType, int fontSize) {
        switch (fontType) {
        case SIMSUN:
            return new Font(BASE_FONT_SIMSUN, fontSize, Font.NORMAL);
        case SIMSUN_BOLD:
            return new Font(BASE_FONT_SIMSUN, fontSize, Font.BOLD);
        case MSYH:
            return new Font(BASE_FONT_MSYH, fontSize, Font.NORMAL);
        case MSYH_BOLD:
            return new Font(BASE_FONT_MSYH, fontSize, Font.BOLD);
        }
        return null;
    }

}
