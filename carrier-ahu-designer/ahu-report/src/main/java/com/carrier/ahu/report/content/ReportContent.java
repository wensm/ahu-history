package com.carrier.ahu.report.content;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.vo.SysConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2018/10/30.
 */
@Slf4j
public class ReportContent {

    private static Map<String, String[][]> reportContents = new HashMap<>();

    public static String[][] getReportContent(String reportName) {
        String[][] content = null;
        if (reportContents.containsKey(reportName)) {
            content = reportContents.get(reportName);
        } else {
            content = loadReportContent(reportName);
            reportContents.put(reportName, content);
        }
        return content;
    }

    /**
     * Report content are two dimensional string array.
     * 
     * @param reportName
     * @return
     */
    private static String[][] loadReportContent(String reportName) {
        InputStream inputStream = ReportContent.class.getClassLoader()
                .getResourceAsStream(String.format(ReportConstants.CONTENT_PATH, reportName));
        if (inputStream == null) {
            log.error("Failed to load report content for: " + reportName);
            return new String[0][0];
        }
        InputStreamReader fileReader = new InputStreamReader(inputStream, Charset.forName(SysConstants.CHARSET_UTF8));
        JsonReader reader = new JsonReader(fileReader);

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(reader, String[][].class);
    }

}
