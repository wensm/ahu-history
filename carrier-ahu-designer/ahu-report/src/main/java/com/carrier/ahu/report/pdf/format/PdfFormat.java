package com.carrier.ahu.report.pdf.format;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.report.common.FontEnum;
import com.carrier.ahu.report.common.ReportConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created by Braden Zhou on 2018/10/24.
 */
import lombok.Data;

@Data
public class PdfFormat {

    private String name;
    private String font;
    private Integer fontSize;
    private String tableHorizontalAlignment;
    private Float paragraphLeading;
    private String paragraphAlignment;
    private Float widthPercentage;
    private Float[] widths;
    private Float cellPaddingTop;
    private Float cellPaddingBottom;
    private String cellHorizontalAlignment;
    private String cellVerticalAlignment;
    private String border;
    private Float borderWidth;
    private List<CellAlignment> cellAlignments;
    private List<CellBorder> cellBorders;
    private List<CellSpan> cellSpans;
    private List<CellImage> cellImages;
    private List<PdfFormat> details;

    public static PdfFormat loadPdfFormat(String name) {
        InputStream jsonStream = PdfFormat.class.getClassLoader()
                .getResourceAsStream(String.format(ReportConstants.FORMAT_PATH, name));
        if (jsonStream != null) {
            return new Gson().fromJson(new InputStreamReader(jsonStream, Charset.forName("UTF-8")),
                    new TypeToken<PdfFormat>() {
                    }.getType());
        }
        return null;
    }

    public PdfFormat getDetailFormat(String name) {
        PdfFormat detailFormat = null;
        if (details != null) {
            for (PdfFormat pdfFormat : details) {
                if (pdfFormat.name != null && pdfFormat.name.equals(name)) {
                    detailFormat = pdfFormat;
                    break;
                }
            }
        }
        // if not found detail report
        if (detailFormat == null) {
            detailFormat = new PdfFormat();
        }

        // override parent report format
        if (detailFormat.font == null) {
            detailFormat.font = this.font == null ? FontEnum.SIMSUN.toString() : this.font;
        }
        if (detailFormat.fontSize == null) {
            detailFormat.fontSize = this.fontSize == null ? ReportConstants.DEFAULT_FONT_SIZE : this.fontSize;
        }
        if (detailFormat.tableHorizontalAlignment == null) {
            detailFormat.tableHorizontalAlignment = this.tableHorizontalAlignment == null
                    ? ReportConstants.DEFAULT_ALIGNMENT
                    : this.tableHorizontalAlignment;
        }
        if (detailFormat.paragraphLeading == null) {
            detailFormat.paragraphLeading = this.paragraphLeading == null ? ReportConstants.DEFAULT_PARAGRAPH_LEADING
                    : this.paragraphLeading;
        }
        if (detailFormat.paragraphAlignment == null) {
            detailFormat.paragraphAlignment = this.paragraphAlignment == null ? ReportConstants.DEFAULT_ALIGNMENT
                    : this.paragraphAlignment;
        }
        if (detailFormat.widthPercentage == null) {
            detailFormat.widthPercentage = this.widthPercentage == null ? ReportConstants.DEFAULT_WIDTH_PERCENTAGE
                    : this.widthPercentage;
        }
        if (detailFormat.widths == null) {
            detailFormat.widths = this.widths == null ? new Float[0] : this.widths;
        }
        if (detailFormat.cellPaddingTop == null) {
            detailFormat.cellPaddingTop = this.cellPaddingTop == null ? ReportConstants.DEFAULT_PADDING
                    : this.cellPaddingTop;
        }
        if (detailFormat.cellPaddingBottom == null) {
            detailFormat.cellPaddingBottom = this.cellPaddingBottom == null ? ReportConstants.DEFAULT_PADDING
                    : this.cellPaddingBottom;
        }
        if (detailFormat.cellHorizontalAlignment == null) {
            detailFormat.cellHorizontalAlignment = this.cellHorizontalAlignment == null
                    ? ReportConstants.DEFAULT_ALIGNMENT
                    : this.cellHorizontalAlignment;
        }
        if (detailFormat.cellVerticalAlignment == null) {
            detailFormat.cellVerticalAlignment = this.cellVerticalAlignment == null ? ReportConstants.DEFAULT_ALIGNMENT
                    : this.cellVerticalAlignment;
        }
        if (detailFormat.border == null) {
            detailFormat.border = this.border == null ? ReportConstants.DEFAULT_BORDER : this.border;
        }
        if (detailFormat.borderWidth == null) {
            detailFormat.borderWidth = this.borderWidth == null ? ReportConstants.DEFAULT_BORDER_WIDTH
                    : this.borderWidth;
        }
        if (detailFormat.cellAlignments == null) {
            detailFormat.cellAlignments = this.cellAlignments == null ? new ArrayList<CellAlignment>()
                    : this.cellAlignments;
        }
        if (detailFormat.cellBorders == null) {
            detailFormat.cellBorders = this.cellBorders == null ? new ArrayList<CellBorder>() : this.cellBorders;
        }
        if (detailFormat.cellSpans == null) {
            detailFormat.cellSpans = this.cellSpans == null ? new ArrayList<CellSpan>() : this.cellSpans;
        }
        if (detailFormat.cellImages == null) {
            detailFormat.cellImages = this.cellImages == null ? new ArrayList<CellImage>() : this.cellImages;
        }
        return detailFormat;
    }

}
