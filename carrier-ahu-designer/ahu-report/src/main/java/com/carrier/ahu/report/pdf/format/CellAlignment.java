package com.carrier.ahu.report.pdf.format;

import java.util.List;

import lombok.Data;

/**
 * Set special cell alignment.
 * 
 * Created by Braden Zhou on 2018/10/24.
 */
@Data
public class CellAlignment {

    private String alignment;
    private List<String> positions;

}
