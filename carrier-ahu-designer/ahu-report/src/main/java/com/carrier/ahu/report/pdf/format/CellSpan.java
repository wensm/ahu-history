package com.carrier.ahu.report.pdf.format;

import java.util.List;

import lombok.Data;

/**
 * Define cell span for pdf cells.
 * 
 * Created by Braden Zhou on 2018/10/24.
 */
@Data
public class CellSpan {

    private int rowSpan;
    private int colSpan;
    private String border;
    private Integer fontSize;
    private Float cellPaddingTop;
    private Float cellPaddingBottom;
    private String paragraphAlignment;
    private String cellHorizontalAlignment;
    private List<String> positions;

}
