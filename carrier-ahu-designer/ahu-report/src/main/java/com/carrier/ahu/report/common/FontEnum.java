package com.carrier.ahu.report.common;

/**
 * Created by Braden Zhou on 2018/10/24.
 */
public enum FontEnum {

    SIMSUN, MSYH, SIMSUN_BOLD, MSYH_BOLD

}
