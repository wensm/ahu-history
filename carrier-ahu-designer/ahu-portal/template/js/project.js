$(function(){
    $(".contabs li a").click(function () {
        $(this).addClass("active").parent('li').siblings().children("a").removeClass("active")
    });
    $(".concho_click li ").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
        $(this).find("i").addClass("active").parents('li').siblings().find("i").removeClass("active");
    });
    //进入项目
    $(".hover_icon").hover(function(){
        $(this).find(".tb_icon").show();
        $(this).find(".tbmor_icon").show();
    },function(){
        $(this).find(".tb_icon").hide();
        $(this).find(".tbmor_icon").hide();
    });

    //展开收起
    $(" .down_btn .iconzk").click(function(){
        if (!$(this).hasClass("downicon")){
            $(this).addClass("downicon");
            $(this).children("i").addClass("rotasty9");
            $(this).closest(".com_cont").find(".com_bottom").hide()
        }
        else{
            $(this).removeClass("downicon");
            $(this).children("i").removeClass("rotasty9");
            $(this).closest(".com_cont").find(".com_bottom").show()
        }
    });

    //下拉框
//多选按钮
    $(".news-list .alrlist").on("click",function(){
        $(this).toggleClass( "on_check" );
    });

    //全选
    $('.choseAllHook').click(function(event) {

        var wrap = $(this).closest('.panel-default').find(".bom_tablist");
        if( $(this).is(':checked') ){
            wrap.find(':checkbox').each(function() {
                $(this).prop('checked', true);
            });
        }else{
            wrap.find(':checkbox').each(function() {
                $(this).prop('checked', false);
            });
        }
    });

    //check_box 全选
    $(".choseAllHook_").click(function(event){
        var wra = $(this).closest('.modal-body').find(".botm_li");
        if( $(this).is(':checked') ){
            wra.find(':checkbox').each(function() {
                $(this).prop('checked', true);
            });
        }else{
            wra.find(':checkbox').each(function() {
                $(this).prop('checked', false);
            });
        }
    });
    //modal_ select
    if($(".select_leng").length>0){
        $(".scroll_botm").perfectScrollbar();
        $('.select-ture').click(function(){
            var s=$(this).nextAll('.select-list');
            s.removeClass('hide');
            $(this).next('.icon_down').removeClass("hide");
            $(this).parents("li").siblings().find('.select-list').addClass('hide');
            $(this).parents("li").siblings().find('.icon_down').addClass('hide');
            //首页选择批量设置
            $(this).parents(".label_select").siblings().find('.select-list').addClass('hide');
            $(this).parents(".label_select").siblings().find('.icon_down').addClass('hide');
        });

        $('.select-list li').click(function(){
            $('.select-list').addClass('hide');
            $(this).addClass('active').siblings().removeClass('active');
            var txtSelect =  $(this).html();
            $(this).parents('ul').siblings('input').val(txtSelect);
            $(this).parents(".label_select").find(".icon_down").addClass("hide");
        });
    }

    //下拉三角左右旋转 down_box down_icon down_none
    //tp:top值
    //首页
   $(".icon_a").click(function(){
       if($(this).attr("aria-expanded")=="false"){
           $(this).children("i").removeClass("rotasty9");
           $(this).children("i").removeClass("tp");
           $(this).parents("li").addClass("bgcolr");
           $(this).parents("li").siblings().removeClass("bgcolr");
           $(this).parents("li").siblings().find(".botm_zk").addClass("rotasty9");
           $(this).parents("li").siblings().find(".botm_zk").addClass("tp");
       }
       if($(this).attr("aria-expanded")=="true"){
           $(this).children("i").addClass("rotasty9");
           $(this).children("i").addClass("tp");
           $(this).parents("li").removeClass("bgcolr");
           $(this).parents("li").siblings().addClass("bgcolr");

           $(this).parents("li").siblings().find(".botm_zk").removeClass("rotasty9");
           $(this).parents("li").siblings().find(".botm_zk").removeClass("tp");
           if($(this).children("i").hasClass("rotasty9")){
               $(this).parents("li").siblings().find(".botm_zk").addClass("rotasty9");
               $(this).parents("li").siblings().find(".botm_zk").addClass("tp");
               $(this).parents("li").removeClass("bgcolr");
               $(this).parents("li").siblings().removeClass("bgcolr");
           }
       }
   });
    //
    //首页modal 弹框  全部展开
    $(".zktab_all").click(function(){
        //选项批量设置
        var set=$(this).closest(".com_zktex").next(".deploy_ul");
       if($(this).hasClass("active")) {
           $(this).removeClass("active");
           $(this).children("em").text("全部展开");
           $(this).children("i").removeClass("rotasty9");
            $(this).closest(".modal-header").nextAll("").find(".deploy_li .down_icon").removeClass("rotasty9");
            $(this).closest(".modal-header").nextAll("").find(".deploy_li .iconshow_cont").removeClass("hide");

           //选项批量设置find(".chall_cont")
           set.find(".chall_cont").removeClass("hide");
           set.find(".down_icon").removeClass("rotasty9");
        }
       else{
           $(this).addClass("active");
           $(this).children("em").text("全部收起");
           $(this).children("i").addClass("rotasty9");
           $(this).closest(".modal-header").nextAll("").find(".deploy_li .down_icon").addClass("rotasty9");
           $(this).closest(".modal-header").nextAll("").find(".deploy_li .iconshow_cont").addClass("hide");

           //选项批量设置
           set.find(".chall_cont").addClass("hide");
           set.find(".down_icon").addClass("rotasty9");
       }

    });
    //首页modal 弹框
    $(".deploy_ul").find(".icon_show").click(function(){
        if(!$(this).children(".down_icon").hasClass("rotasty9")){
            $(this).children(".down_icon").addClass("rotasty9");
            $(this).closest(".deploy_li").find(".iconshow_cont").addClass("hide");
        }else{
            $(this).children(".down_icon").removeClass("rotasty9");
            $(this).closest(".deploy_li").find(".iconshow_cont").removeClass("hide");
        }

    });

    //新建group
    $(".newgrop_btn").click(function(){
        if( !$(".newgrop_cont").hasClass("hide")){
            $(".newgrop_cont").addClass("hide")
        }
        else{
            $(".newgrop_cont").removeClass("hide")
        }
        $(".newgrop_close").click(function(){
            $(".newgrop_cont").addClass("hide")
        })
    });
    //新建AHU
    $(".newgrop_btn1").click(function(){
        if( !$(".newgrop_cont1").hasClass("hide")){
            $(".newgrop_cont1").addClass("hide")
        }
        else{
            $(".newgrop_cont1").removeClass("hide")
        }
        $(".newgrop_close1").click(function(){
            $(".newgrop_cont1").addClass("hide")
        })
    });

    //日历
    if ($('#picktime').length > 0) {
        $.fn.datetimepicker.dates['zh-CN'] = {
            days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
            daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
            daysMin: ["日", "一", "二", "三", "四", "五", "六", "日"],
            months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            today: "今天",
            suffix: [],
            meridiem: ["上午", "下午"]
        };
        $('.picktime').datetimepicker({
            language: 'zh-CN',
            format: 'yyyy-mm-dd',
            startView: 'month',
            minView: 'month',
            todayBtn: true,
            autoclose: true
        });
    };

    //鼠标点击加号事件
    $(".add_box").find(".addicon").click(function(){
       var s=$(this).next(".inser_box");
        if (!s.hasClass("hide")){
            s.addClass("hide");
        }
        else{
            s.removeClass("hide");
            $(this).closest("tr").siblings().find(".inser_box").addClass("hide")
        }
         //确认取消按钮
        $(".insbo_ok").click(function(){
            $(this).closest(".inser_box").addClass("hide")
        });
        $(".insbo_clo").click(function(){
            $(this).closest(".inser_box").addClass("hide")
        })
    });

    //tab切换
    function table(arr,arrone){
        var newarr=$(arr);
        var arr=$(arrone);
        $.each(newarr, function(i,val) {
            $(newarr[i]).on('click',function(){
                $(this).addClass('cur');
                $(this).siblings().removeClass('cur');
                $.each(arr, function(index,value){
                    if(i==index){
                        $(arr[index]).removeClass('none');
                    }else{
                        $(arr[index]).addClass('none');
                    }
                });
            });
        })
    }
    table(".defa_tab li",".defacon_cont .comcon_tex");
    $(".next_btn").click(function(){
        $(this).parents(".deprigcont").find(".nex_cont").removeClass("none");
        $(this).parents(".decont").addClass("none");
        $(this).closest(".dep_rig").find(".nex_tab").addClass("cur");
        $(this).closest(".dep_rig").find(".nex_tab").addClass("cur").prev("").removeClass("cur");
        table('.detop_ul .detop_li','.deprigcont .decont');
        table(".come_tab li",".com_cont .comcon_tex");
    });


    //登录验证
    $(".lobtn").on('click', function () {
        var username=$("#loginname").val();
        var password = $("#password").val();
        //登录-用户名
        if(username ==""){
            $(".ertext-con1").addClass("active");
        }else{
            $(".ertext-con1").removeClass("active")
        }
        //密码
        if (password == "") {
            $(".ertext-con2").addClass("active");
        } else {
            $(".ertext-con2").removeClass("active");
        }
    });

    //首页框中的编辑按钮
    $(".newgrop_cont").click(function(){
        var aft=$(this).closest(".con_after");
        if(!aft.hasClass("hide")){
            aft.addClass("hide");
            $(this).closest(".pro_cont").find(".con_prove").removeClass("hide")
        }
        else{
            aft.removeClass("hide");
            $(this).closest(".pro_cont").find(".con_prove").addClass("hide")
        }
      //取消
        $(".newgrop_close").click(function(){
            $(this).closest(".pro_cont").find(".con_after").removeClass("hide");
            $(this).closest(".con_prove").addClass("hide");
        });
        //确认
        $(".btn_add").click(function(){
            $(this).closest(".pro_cont").find(".con_after").removeClass("hide");
            $(this).closest(".con_prove").addClass("hide");
        });
    })

    //首页系统下拉
    $(".selec_ul").hover(function(){
        $(this).children(".icon_down").removeClass("hide");
        $(this).children(".selec_li").removeClass("hide");
        //var list=$(this).find(".select-list").children("li")
        //list.hover()
    },function(){
        $(this).children(".icon_down").addClass("hide");
        $(this).children(".selec_li").addClass("hide")
    })


    //进入项目中--浏览文件的路径
    if ($(".file-leng").length>0){
        //参数导入input value
        var attachment=document.getElementById("attachment");
        var trueattachment=document.getElementById("trueattachment");
        attachment.onchange=function(){
            trueattachment.value=getFullPath(this);
            console.log(trueattachment.value)
        };
        //生成报告路径
        var attachment2=document.getElementById("attachment2");
        var trueattachment2=document.getElementById("trueattachment2");
        attachment2.onchange=function(){
            trueattachment2.value=getFullPath(this);
            console.log(trueattachment2.value)
        };

        function getFullPath(obj){
            if(obj)
            {
                //ie
                if (window.navigator.userAgent.indexOf("MSIE")>=1)
                {
                    obj.select();
                    return document.selection.createRange().text;
                }
                //firefox
                else if(window.navigator.userAgent.indexOf("Firefox")>=1)
                {
                    if(obj.files)
                    {
                        return obj.files.item(0).getAsDataURL();
                    }
                    return obj.value;
                }
                return obj.value;
            }
        }
        //end 浏览文件的路径
    }


    //生成报告 全选
    //check_box 全选 6.9
    $(".prev_choseAll").click(function(event){
        var wra = $(this).closest('.revfr-con').find(".label-checked");
        if( $(this).is(':checked') ){
            wra.find(':checkbox').each(function() {
                $(this).prop('checked', true);
            });
        }else{
            wra.find(':checkbox').each(function() {
                $(this).prop('checked', false);
            });
        }
    });


    //打开数据
    $(".open_inser").click(function(){
        $(".inser_befer").addClass("hide");
        $(".inser_after").removeClass("hide");
    });

    //6.30
//    显示自定义按钮
    $(".select-list").click(function(){
        var btntex=$(this).children(".active");
        var par=$(this).closest(".label_select");
        if(btntex.text()==="自定义"){
            par.nextAll(".zdy_btn").show()
        }
        else{
            par.nextAll(".zdy_btn").hide()
        }
    })
});


//批量处理
$(".check_ul").find("input").click(function(){
    if($(this).is(':checked')) {
        //单选
        $(this).closest("li").siblings().find("input").prop('checked', false);
    }
});

//8.11
//全选 子元素选中父元素必选中
$(".comAll_chek").click(function(){
    var check_=$(this).closest(".check_box").next(".comNewcheck");
    if( $(this).is(':checked') ){
        check_.find(':checkbox').each(function() {
            $(this).prop('checked', true);
            $(this).attr('disabled',false);
        });
    }
    else if(!$(this).is(':checked')){
        check_.find(':checkbox').each(function() {
            $(this).attr('disabled',true);
            $(this).prop('checked', false);

        });
    }
});

//打开非标
$(".open_btn").click(function(){
    if($(this).html()== "打开非标" ){
        $(".clse_one").hide();
        $(".clse_two").show();
        $(this).html("关闭非标")
        $(".Money").removeClass("debtm_btn1").addClass("debtm_btn2")
        $(".Money").attr("disabled",false)


    }else{
        $(".clse_one").show();
        $(".clse_two").hide();
        $(this).html("打开非标")
        $(".Money").addClass("debtm_btn1").removeClass("debtm_btn2")
        $(".Money").attr("disabled",true)

    }



});

$(".NOb_btn").click(function(){
    $(".addNOimg").css("display","block")
});