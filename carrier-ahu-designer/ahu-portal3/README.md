# Setting up the Project

First you'll need [Node.js](https://nodejs.org) and the package manager that comes with npm.

Once you've got that working, head to the command line where we'll set up our project.

## Clone the Project

```
git clone https://github.com/tranfu/react-starter
cd react-starter
npm install -g cnpm --registry=https://registry.npm.taobao.org
cnpm install --global yarn
yarn config set registry https://registry.npm.taobao.org
yarn install
yarn build:dev
```

Now open up [http://localhost:8080](http://localhost:8080)

You should see a "Hello React" message in the browser.

## Make Some Changes

Open up `HelloMessage.js` and change the text to something like "Hi". The browser automatically reloads with your new code.
