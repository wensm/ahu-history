/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function () {
  return {
    entry: {
      index: './src/index.js',
      vendor: ['react', 'react-dom', 'react-router', 'react-redux', 'redux', 'redux-thunk', 'redux-logger', 'axios', 'classnames', 'dragula']
    },
    output: {
      filename: 'js/[name].[chunkhash].js',
      path: path.resolve(__dirname, '../dist')
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          // use: ['babel-loader', 'eslint-loader'],
          use: ['babel-loader'],
          exclude: /node_modules/
        },
        {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'assets/[hash].[ext]'
          }
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin('css/[name].[contenthash].css'),
      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'manifest'] // Specify the common bundle's name.
      }),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        chunksSortMode: 'dependency'
      })
    ]
  };
}
