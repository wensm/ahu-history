import React from 'react';

import style from './app.css';

export default class App extends React.Component {
  render() {
    const childrenWithProps = React.Children.map(this.props.children, child => React.cloneElement(child, { data: 'react-redux' }));
    return (
      <div className={style.app}>
        <div className={style.header}>
          header
        </div>
        <div className={style.main}>
          {childrenWithProps}
        </div>
        <div className={style.footer}>
          footer
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.element.isRequired,
};
