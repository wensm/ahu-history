import React from 'react'
import classnames from 'classnames'
import dragula from 'dragula'

import style from './Ahu.css'

export default class Ahu extends React.Component {

  componentDidMount() {
    const sections = document.getElementById('sections')
    const ahu =document.getElementById('ahu')
    dragula([sections, ahu], {
      copy(el, source) { return source === sections },
      accepts(el, target) { return target === ahu },
      direction: 'horizontal',
      removeOnSpill: true,
    })
  }

  render() {
    return (
      <div className={classnames(style.Ahu, 'clearfix')}>
        <div className={classnames(style.west, 'clearfix')}>
          <div className={classnames(style.sections, 'clearfix')} id="sections">
            <div className={style.section}>1</div>
            <div className={style.section}>2</div>
            <div className={style.section}>3</div>
            <div className={style.section}>4</div>
            <div className={style.section}>5</div>
            <div className={style.section}>6</div>
            <div className={style.section}>7</div>
            <div className={style.section}>8</div>
            <div className={style.section}>9</div>
          </div>
        </div>
        <div className={style.center}>
          <div className={style.ahu} id="ahu"></div>
        </div>
        <div className={style.east}>
          east
        </div>
      </div>
    )
  }
}
