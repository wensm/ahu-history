import React from 'react';

import style from './a.css';

export default class A extends React.Component {
  render() {
    return (
      <div className={style.a}>
        A
      </div>
    );
  }
}
