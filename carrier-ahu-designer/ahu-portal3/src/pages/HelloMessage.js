import React from 'react';
import classnames from 'classnames';

import style from './helloMessage.css';

import girl from '../images/girl.png';

export default class HelloMessage extends React.Component {
  render() {
    return (
      <div className={style.helloMessage}>
        <i className="fa fa-imdb" style={{ fontSize: '160px' }} />
        <span className={classnames(style.bg, style.red)}>Hello {this.props.data}</span>
        <img src={girl} alt="" />
      </div>
    );
  }
}

HelloMessage.propTypes = {
  data: React.PropTypes.string,
};

HelloMessage.defaultProps = {
  data: 'Jane',
};
