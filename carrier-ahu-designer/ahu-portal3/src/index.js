/* eslint-disable */
import React, { PropTypes } from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { Router, Route, browserHistory, hashHistory, IndexRoute } from 'react-router'

import style from './css/style'
import rootReducer from './reducers'
import App from './components/App'
import Todo from './pages/Todo'
import HelloMessage from './pages/HelloMessage'

import Ahu from './pages/Ahu'

const loggerMiddleware = createLogger()

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    loggerMiddleware // neat middleware that logs actions
  )
)

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App} >
        <IndexRoute component={HelloMessage} />
        <Route path="hello" component={HelloMessage} />
        <Route path="todo(/:filter)" component={Todo} />
        <Route path="projects/:projectId/ahus/:ahuid" component={Ahu} />
      </Route>
    </Router>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired,
}

render(<Root store={store} />, document.getElementById('root'))

console.log(PRODUCTION) // eslint-disable-line
console.log(SERVICE_URL) // eslint-disable-line
