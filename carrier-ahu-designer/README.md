﻿#项目简介
carrier ahu designer

#项目结构说明
ahu-db:数据库脚本
ahu-doc:设计文档

ahu-common:通用公共包
ahu-dao:数据库访问层包装
ahu-service-api:业务接口规范定义包
ahu-service:业务接口实现包
ahu-rest:供前端使用的rest api包
ahu-portal:前端视图包
ahu-migration:老系统数据迁移项目

#项目地址
https://www.teambition.com/project/58b3f6cd8c431c69028022f3
http://git.adinnet.cn/dengyeting/CarrierAHU/wikis/home
http://git.adinnet.cn/dengyeting/CarrierAHU/tree/master/design

#项目构建和启动
cd ahu-rest
gradle -x clean build
gradle bootRun

java -Djava.awt.headless=falsh -Dspring.profiles.active=default -jar ahu-rest-1.0.0.jar server application.yml

#git区分大小写
git config core.ignorecase false

#META-INF
加入到classpath下，项目界面才可以启动

#h2数据库访问
http://localhost:20000/h2
jdbc:h2:D:\ework\CarrierAHU\carrier-ahu-designer\ahu-rest\h2database\ahu
