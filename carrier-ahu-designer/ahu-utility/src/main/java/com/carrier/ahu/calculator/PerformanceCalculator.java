package com.carrier.ahu.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.FilterFormat;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.param.FilterPanelParam;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.filter.FilterComposing;
import com.carrier.ahu.metadata.entity.filter.HEPAFilterComposing;
import com.carrier.ahu.metadata.entity.filter.S4yFilterFormat;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

import static com.carrier.ahu.common.intl.I18NConstants.NOT_FOUND_SPRAY_HUMIDIFIER_MODAL;
import static com.carrier.ahu.constant.CommonConstant.SYS_ALPHABET_X_UP;

/**
 * Created by LIANGD4 on 2017/10/24.
 */
public class PerformanceCalculator implements SystemCalculateConstants {

	// 高压喷雾加湿段页面选择型号
	// fEfficiencyS:夏季加湿量、fEfficiencyW:冬季加湿量
	public static List<SJSupplier> getPerformanceJ(String season, double fEfficiencyS, double fEfficiencyW,
			String supplier) throws TempCalErrorException {
		supplier = "B";// TODO 旧系统供应商不确定，加湿段供应商excel中为 B S,风机段A,B,C 暂时使用B
		double fv1 = 0.00;
		if (season.equals(UtilityConstant.JSON_AHU_SEASON_W)) {
			fv1 = fEfficiencyW;
		} else {
			if (season.equals(UtilityConstant.JSON_AHU_SEASON_S)) {
				fv1 = fEfficiencyS;
			} else {
				if (fEfficiencyS > fEfficiencyW) {
					fv1 = fEfficiencyS;
				} else {
					fv1 = fEfficiencyW;
				}
			}
		}
		fv1 = fv1 / 0.35;
		double fratio = 0.00;
		if (fv1 > 100) {
			fratio = 0.2;
		} else {
			fratio = 0.5;
		}
		double minHQ = fv1 * (1 - fratio);
		double maxHQ = fv1 * (1 + fratio);
		return getSJSupplierListPoBySH(supplier, minHQ, maxHQ);
	}

    private static List<SJSupplier> getSJSupplierListPoBySH(String supplier, double minHQ, double maxHQ) throws TempCalErrorException {
        List<SJSupplier> sjSuppliers = AhuMetadata.findList(SJSupplier.class, supplier);
        List<SJSupplier> sJSupplierList = new ArrayList<SJSupplier>();
        for (SJSupplier sjSupplier : sjSuppliers) {
            if (maxHQ >= sjSupplier.getHq() && minHQ <= sjSupplier.getHq()) {
                sJSupplierList.add(sjSupplier);
            }
        }
        Collections.sort(sJSupplierList, Comparator.comparing(SJSupplier::getHq));
        if(sJSupplierList.size()==0){
			throw new TempCalErrorException(AHUContext.getIntlString(NOT_FOUND_SPRAY_HUMIDIFIER_MODAL));
		}
        return sJSupplierList;
    }

	// 干蒸汽加湿段
	public static List<S4HHumiQ> getPerformanceH(String season, double humidificationS, double humidificationW,
			String supplier) {
		supplier = "B";// TODO 旧系统供应商不确定，加湿段供应商excel中为 B S,风机段A,B,C
		double minDVarHumiQ = 0.00;
		if (season.equals(UtilityConstant.JSON_AHU_SEASON_S)) {
			minDVarHumiQ = humidificationS;
		} else if (season.equals(UtilityConstant.JSON_AHU_SEASON_W)) {
			minDVarHumiQ = humidificationW;
		} else {
			if (humidificationS >= humidificationW) {
				minDVarHumiQ = humidificationS;
			} else {
				minDVarHumiQ = humidificationW;
			}
		}
		double maxDvaHumiQ = 0.00;
		if (minDVarHumiQ <= 10) {
			maxDvaHumiQ = minDVarHumiQ * 2.5;
		} else {
			maxDvaHumiQ = minDVarHumiQ * 1.5;
		}
		return getS4HHumiQRangeFrom(supplier, minDVarHumiQ, maxDvaHumiQ);
	}

    private static List<S4HHumiQ> getS4HHumiQRangeFrom(String supplier, double minDVarHumiQ, double maxDvaHumiQ) {
        List<S4HHumiQ> s4hHumiQList = AhuMetadata.findList(S4HHumiQ.class, supplier);

        List<S4HHumiQ> s4HHumiQPoList = new ArrayList<S4HHumiQ>();
        for (S4HHumiQ sJSupplierPo : s4hHumiQList) {
            if (maxDvaHumiQ >= sJSupplierPo.getHumidificationQ() && minDVarHumiQ <= sJSupplierPo.getHumidificationQ()) {
                s4HHumiQPoList.add(sJSupplierPo);
            }
        }
        Collections.sort(s4HHumiQPoList, new Comparator<S4HHumiQ>() {
            public int compare(S4HHumiQ o1, S4HHumiQ o2) {
                return Integer.valueOf(o1.getHTypes()).compareTo(Integer.valueOf(o2.getHTypes()));
            }
        });
        return s4HHumiQPoList;
    }

    /**
	 * E_Db：干球温度 E_Wb：湿球温度 RH：相对湿度 airV：风量
	 */
	// 根据干球温度、湿球温度、相对湿度（加湿比）、风量 计算出风加湿量、出干球温度、出湿球温度、出相对湿度
	public static HumidificationBean rHConvert(double E_Db, double E_Wb, double RH, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean db_wb = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);
		double fauxi = AirConditionUtils.FIterationTI(db_wb.getParmT(), db_wb.getParamI(), RH, false);
		double paramD = db_wb.getParamD(); // 含湿量
		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate3(fauxi, RH);
		double FD = airConditionBean.getParamD();
		double humidificationQ = (FD - paramD) * 3.6 * 1.2 * airV / 3600;
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmTb(), 1));
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(BaseDataUtil.decimalConvert(humidificationQ, 0));
		return humidificationBean;
	}

	/**
	 * 求(等焓【湿膜加湿段(Wetfilm Humidifier)/高压喷雾加湿段(Spray Humidifier)】)加湿量
	 * E_Db：干球温度 E_Wb：湿球温度 RH：相对湿度 airV：风量
	 * 根据干球温度、湿球温度、相对湿度（加湿比）、风量 计算出风加湿量、出干球温度、出湿球温度、出相对湿度
	 */
	public static HumidificationBean IsoenthalpyRHConvert(double E_Db, double E_Wb, double RH, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean db_wb = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);
		double fauxi = AirConditionUtils.FIterationTI(db_wb.getParmT(), db_wb.getParamI(), RH, false);
		double paramD = db_wb.getParamD(); // 含湿量

		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate3(fauxi, RH);//等焓

		double FD = airConditionBean.getParamD();

		double humidificationQ = (FD - paramD) * 3.6 * 1.2 * airV / 3600;//等焓
		humidificationBean.setInRelativeT(BaseDataUtil.decimalConvert(db_wb.getParmF(), 0));
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmTb(), 1));
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(BaseDataUtil.decimalConvert(humidificationQ, 0));
		return humidificationBean;
	}
	/**
	 * 求(等温【电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)】)加湿量
	 * E_Db：干球温度 E_Wb：湿球温度 RH：相对湿度 airV：风量
	 * 根据干球温度、湿球温度、相对湿度（加湿比）、风量 计算出风加湿量、出干球温度、出湿球温度、出相对湿度
	 */
	public static HumidificationBean IsothermalRHConvert(double E_Db, double E_Wb, double RH, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean db_wb = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);
		double paramD = db_wb.getParamD(); // 含湿量

		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate3(E_Db, RH);//等温
		double FD = airConditionBean.getParamD();
		double humidificationQ = (FD - paramD) * 3.6 * 1.2 * airV / 3600;//等焓

		//等温是否要用这个公式，从结果看是对的，以后再看是否需要调整？retHumi:=(ldvarDout-ldvarDin)*dparmAirFlow*ldvarDensity/1000*3600;
		humidificationBean.setInRelativeT(BaseDataUtil.decimalConvert(db_wb.getParmF(), 0));
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmTb(), 1));
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(BaseDataUtil.decimalConvert(humidificationQ, 0));
		return humidificationBean;
	}
	/**
	 * airV ：风量 E_DB 干球温度 E_WB 湿球温度 Humidification :加湿量 airV ：风量 根据干球温度、湿球温度、加湿量、风量
	 * 计算出风干球温度、出风湿球温度、出风相对湿度
	 */
	public static HumidificationBean humidificationConvert(double E_Db, double E_Wb, double humidification, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);
		double fD = humidification * 1000 / 1.2 / airV + airConditionBean.getParamD();
		double fI = airConditionBean.getParamI();
		AirConditionBean conditionBean = AirConditionUtils.FAirParmCalculate4(fI, fD);// 根据焓
																						// 含湿量
																						// 计算出风相对湿度、干球温度、湿球温度
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmTb(), 1));
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(conditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(humidification);
		return humidificationBean;
	}

	/**
	 * 求(等焓加湿段)相对湿度
	 * airV ：风量 E_DB 干球温度 E_WB 湿球温度 Humidification :加湿量 airV ：风量 根据干球温度、湿球温度、加湿量、风量
	 * 计算出风干球温度、出风湿球温度、出风相对湿度
	 */
	public static HumidificationBean IsoenthalpyHumidificationConvert(double E_Db, double E_Wb, double humidification, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);

		double fD = humidification * 1000 / 1.2 / airV + airConditionBean.getParamD();//根据加湿量计算含湿量

		double fI = airConditionBean.getParamI();
		AirConditionBean conditionBean = AirConditionUtils.FAirParmCalculate4(fI, fD);// 根据焓
																						// 含湿量
																						// 计算出风相对湿度、干球温度、湿球温度
		humidificationBean.setInRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 1));
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmTb(), 1));//TODO : 根据焓量计算出的湿球温度，会有误差暂时保持误差;后面系统计算确定没问题，直接修改为湿球温度
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(conditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(humidification);
		return humidificationBean;
	}
	/**
	 * 求(等温加湿段)相对湿度
	 * airV ：风量 E_DB 干球温度 E_WB 湿球温度 Humidification :加湿量 airV ：风量 根据干球温度、湿球温度、加湿量、风量
	 * 计算出风干球温度、出风湿球温度、出风相对湿度
	 */
	public static HumidificationBean IsothermalHumidificationConvert(double E_Db, double E_Wb, double humidification, int airV)
			throws TempCalErrorException {
		HumidificationBean humidificationBean = new HumidificationBean();
		AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);

		double fD = humidification * 1000 / 1.2 / airV + airConditionBean.getParamD();//根据加湿量计算含湿量

		AirConditionBean conditionBean = AirConditionUtils.FAirParmCalculate2(E_Db, fD);// 根据：干球温度，含湿量
																						// 计算出风 相对湿度、干球温度、湿球温度
		humidificationBean.setInRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 1));
		humidificationBean.setOutDryBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmT(), 1));
		humidificationBean.setOutWetBulbT(BaseDataUtil.decimalConvert(conditionBean.getParmTb(), 1));
		humidificationBean.setOutRelativeT(BaseDataUtil.decimalConvert(conditionBean.getParmF(), 0));
		humidificationBean.setHumidificationQ(humidification);
		return humidificationBean;
	}
	/**
	 * 过滤段计算过滤器布置 1.初效过滤 段类别：单层过滤器B、综合过滤器C、静电过滤器Y 抽取方式：正抽、外抽、侧抽 2.高效过滤 段类别：高效过滤器V
	 *
	 * @param serial
	 *            机组型号
	 * @param sectionType
	 *            段类别 BCVY
	 * @param mediaLoading
	 *            抽取方式
	 * @return
	 */
	public static List<FilterPanelBean> filterPanelBeanList(String serial, String sectionType, String mediaLoading) {
		// TODO 过滤器抽取方式 外抽待确认
		if (sectionTypeV.equals(sectionType)) {// 高效过滤器 高效滤网2
			List<HEPAFilterComposing> filterComposingList = AhuMetadata.findList(HEPAFilterComposing.class, serial, FilterFormat.FRONT_LOADING.getFormat());
			return packageHFilterPanelBeanList(filterComposingList);
		} else {
            List<FilterComposing> filterComposingList = new ArrayList<FilterComposing>();
            if (sectionTypeY.equals(sectionType)) {// 静电过滤器
                filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.FRONT_LOADING.getFormat());
            } else {
                // 正抽或侧抽
                if (FILTER_MEDIALOADING_FRONTLOADING.equals(mediaLoading)) {
                    filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.FRONT_LOADING.getFormat());
                } else if (FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading)) {
                    filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.SIDE_LOADING.getFormat());
                }
            }
			return packageFFilterPanelBeanList(filterComposingList, sectionType, true, true);
		}
	}

	/**
	 * 过滤段计算过滤器布置 1.初效过滤 段类别：单层过滤器B、综合过滤器C、静电过滤器Y 抽取方式：正抽、外抽、侧抽 2.高效过滤 段类别：高效过滤器V
	 *
	 * @return
	 */
	public static List<FilterPanelBean> filterPanelBeanList(FilterPanelParam filterPanelParam) {
		if (UtilityConstant.JSON_FILTER_FILTEROPTIONS_NOFILTER.equals(filterPanelParam.getRMaterial()) && UtilityConstant.JSON_FILTER_FILTEROPTIONS_NO.equals(filterPanelParam.getLMaterial())) {
			return null;
		}
		String serial = filterPanelParam.getSerial();// 机组型号
		String sectionType = filterPanelParam.getSectionType();// 段类别 BCVY
		String mediaLoading = filterPanelParam.getMediaLoading();// 抽取方式
		// TODO 过滤器抽取方式 外抽待确认
		if (sectionTypeV.equals(sectionType)) {// 高效过滤器 高效滤网2
			if(SystemCalculateConstants.HEPAFILTER_SUPPLIER_NONE.equals(filterPanelParam.getSupplier())) {
				return null;//如果高效过滤器供应商为无过滤器 过滤器数量为空
			}else{
				List<HEPAFilterComposing> filterComposingList = AhuMetadata.findList(HEPAFilterComposing.class, serial, FilterFormat.FRONT_LOADING.getFormat());
				return packageHFilterPanelBeanList(filterComposingList);
			}
		} else {
			List<FilterComposing> filterComposingList = new ArrayList<FilterComposing>();
			if (sectionTypeY.equals(sectionType)) {// 静电过滤器
				List<S4yFilterFormat> s4yFilterFormatList = AhuMetadata.findList(S4yFilterFormat.class, serial, "A");
				return packageS4yFilterFormatList(s4yFilterFormatList, sectionType,!(UtilityConstant.JSON_FILTER_FILTEROPTIONS_NO.equals(filterPanelParam.getLMaterial())),
						!(UtilityConstant.JSON_FILTER_FILTEROPTIONS_NOFILTER.equals(filterPanelParam.getRMaterial())));
			} else {
				if(SYS_ALPHABET_X_UP.equals(filterPanelParam.getFitetF())){
					filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.OUT_LOADING.getFormat());
				}else{
					// 正抽或侧抽
					if (FILTER_MEDIALOADING_FRONTLOADING.equals(mediaLoading)) {
						filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.FRONT_LOADING.getFormat());
					} else if (FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading)) {
						filterComposingList = AhuMetadata.findList(FilterComposing.class, serial, FilterFormat.SIDE_LOADING.getFormat());
					}
				}
			}
			return packageFFilterPanelBeanList(filterComposingList, sectionType,!(UtilityConstant.JSON_FILTER_FILTEROPTIONS_NO.equals(filterPanelParam.getLMaterial())),
					!(UtilityConstant.JSON_FILTER_FILTEROPTIONS_NOFILTER.equals(filterPanelParam.getRMaterial())));
		}
	}

	private static List<FilterPanelBean> packageHFilterPanelBeanList(List<HEPAFilterComposing> filterComposingList) {
		List<FilterPanelBean> filterPanelBeanList = new ArrayList<FilterPanelBean>();
		for (HEPAFilterComposing filterComposing : filterComposingList) {
			FilterPanelBean filterPanelBean = new FilterPanelBean();
			filterPanelBean.setOption(filterComposing.getFilterSize());
			filterPanelBean.setCount(BaseDataUtil.integerConversionString(filterComposing.getFilterNum()));
			filterPanelBeanList.add(filterPanelBean);
		}
		return filterPanelBeanList;
	}

	private static List<FilterPanelBean> packageFFilterPanelBeanList(List<FilterComposing> filterComposingList,
			String sectionType, boolean rFlg, boolean lFlg) {
		List<FilterPanelBean> filterPanelBeanList = new ArrayList<FilterPanelBean>();
		for (FilterComposing filterComposing : filterComposingList) {
			FilterPanelBean filterPanelBean = new FilterPanelBean();
			if (sectionTypeC.equals(sectionType)) {
				if (rFlg) {
					filterPanelBean.setPCount(BaseDataUtil.integerConversionString(filterComposing.getFilterNum()));
					filterPanelBean.setPOption(filterComposing.getFilterSize());
				}
				if (lFlg) {
					filterPanelBean.setLCount(BaseDataUtil.integerConversionString(filterComposing.getFilterNum()));
					filterPanelBean.setLOption(filterComposing.getFilterSize());
				}
			} else {
				filterPanelBean.setOption(filterComposing.getFilterSize());
				filterPanelBean.setCount(BaseDataUtil.integerConversionString(filterComposing.getFilterNum()));
			}
			filterPanelBeanList.add(filterPanelBean);
		}
		return filterPanelBeanList;
	}

	private static List<FilterPanelBean> packageS4yFilterFormatList(List<S4yFilterFormat> s4yFilterFormatList,String sectionType, boolean rFlg, boolean lFlg) {
		List<FilterPanelBean> filterPanelBeanList = new ArrayList<FilterPanelBean>();
		for (S4yFilterFormat s4yFilterFormat : s4yFilterFormatList) {
			FilterPanelBean filterPanelBean = new FilterPanelBean();
			if (sectionTypeC.equals(sectionType)) {
				if (rFlg) {
					filterPanelBean.setPCount(BaseDataUtil.integerConversionString(s4yFilterFormat.getFilterNum()));
					filterPanelBean.setPOption(s4yFilterFormat.getFilterSize());
				}
				if (lFlg) {
					filterPanelBean.setLCount(BaseDataUtil.integerConversionString(s4yFilterFormat.getFilterNum()));
					filterPanelBean.setLOption(s4yFilterFormat.getFilterSize());
				}
			} else {
				filterPanelBean.setOption(s4yFilterFormat.getFilterSize());
				filterPanelBean.setCount(BaseDataUtil.integerConversionString(s4yFilterFormat.getFilterNum()));
			}
			filterPanelBeanList.add(filterPanelBean);
		}
		return filterPanelBeanList;
	}

}
