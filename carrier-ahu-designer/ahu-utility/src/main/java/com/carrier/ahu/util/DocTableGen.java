package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.report.common.ReportConstants.*;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.report.pdf.PDFTableGenerator;
import com.carrier.ahu.unit.BaseDataUtil;
import com.lowagie.text.DocumentException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DocTableGen {

	@SuppressWarnings("unused")
    public static void addSection(XWPFDocument doc, String key, int order, Map<String, String> noChangeMap,Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws DocumentException, IOException {
		switch (SectionTypeEnum.getSectionTypeFromId(key)) {
		case TYPE_MIX: {
			allMap = SectionDataConvertUtils.getMix(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_MIX, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getMix(contents, noChangeMap,allMap, language);
			if (doc != null) {
				WordTableGen.genTechMix(contents,doc);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_MIX_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechMix2(contents2,doc);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_MIX_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechMix2(contents3,doc);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_MIX);
			}
			return;
		}
		case TYPE_SINGLE: {
			allMap = SectionDataConvertUtils.getFilter(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_SINGLE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FILTER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getSingle(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechFilter(contents,doc);
			}

			// 添加非标选项
//			if (doc != null) {
//				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_SINGLE);
//			}
			//添加单层过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_FILTER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_SINGLE.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getSingleNS(nsAHUcontents, unit, language, allMap);
				if (doc != null) {
					WordTableGen.genTechSectionNS(nsAHUcontents, doc);
				}
			}
			return;
		}
		case TYPE_COMPOSITE: {
			allMap = SectionDataConvertUtils.getCombinedFilter(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_COMPOSITE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDFILTER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getComposite(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechCombined(contents,doc);
			}			
			
			// 袋式压差计
			Object pressureGuageB = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB);
			String valuePGB = BaseDataUtil.constraintString(pressureGuageB);
			// 板式压差计
			Object pressureGuageP = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP);
			String valuePGP = BaseDataUtil.constraintString(pressureGuageP);
			if (!UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO.equals(valuePGB)
					&& !UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO.equals(valuePGP)) {// 压差计展示
				String[][] contentAppendix = ValueFormatUtil.transReport(
						ReportContent.getReportContent(REPORT_TECH_COMBINEDFILTER_APPENDIX), project, unit, allMap,
						language, unitType);
				if (doc != null) {
					WordTableGen.genTechSectionNS(contentAppendix, doc);
				}
				// document.add(PDFTableGenerator.generate(contentAppendix,
				// ReportConstants.REPORT_TECH_COMBINEDFILTER_APPENDIX));
			}
			
			// 添加非标选项
//			if (doc != null) {
//				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_COMPOSITE);
//			}
			//添加综合过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_COMPOSITE), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_COMPOSITE.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getCompositeNS(nsAHUcontents, unit, language, allMap);
				if (doc != null) {
					WordTableGen.genTechCombined(nsAHUcontents, doc);
				}
			}
			
			return;
		}
		case TYPE_COLD: {
			allMap = SectionDataConvertUtils.getCoolingCoil(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_COLD, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COOLINGCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getCoolingCoil(contents, allMap, language, unitType);// 定制转换
			if (doc != null) {
				WordTableGen.genTechCoolingCoil(contents,doc);
			}
			
//			//添加AHRI标记
//			String[][] contentsAhri = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_AHRI), project, unit, allMap,
//					language, unitType);
//
//			if (doc != null) {
//				WordTableGen.genTechFan6(contentsAhri, doc, false);
//			}
			
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COOLINGCOIL_WINTER), project, unit,
						allMap, language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechCoolingCoil1(contents1,doc);
				}
				
//				//添加AHRI标记
//				String[][] contentsAhri1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_AHRI), project, unit, allMap,
//						language, unitType);
//
//				if (doc != null) {
//					WordTableGen.genTechFan6(contentsAhri1, doc, false);
//				}
			}
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_COLD);
			}
			return;
		}
		case TYPE_DIRECTEXPENSIONCOIL: {
			allMap = SectionDataConvertUtils.getDXCoil(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getDirectexpensioncoil(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechCoolingCoil(contents,doc);
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_DXCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechCoolingCoil1(contents1,doc);
				}
			}
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL);
			}
			return;
		}
		case TYPE_HEATINGCOIL: {
			allMap = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_HEATINGCOIL, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATINGCOIL), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getHeatingCoil(contents, allMap, language, unitType);// 定制转换
			if (doc != null) {
				WordTableGen.genTechHea(contents,doc);
			}
			
//			//添加AHRI标记
//			String[][] contentsAhri = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_AHRI), project, unit, allMap,
//					language, unitType);
//
//			if (doc != null) {
//				WordTableGen.genTechFan6(contentsAhri, doc, false);
//			}
			
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_HEATINGCOIL);
			}
			return;
		}
		case TYPE_STEAMCOIL: {
			allMap = SectionDataConvertUtils.getSteamCoil(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_STEAMCOIL, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getTechSte(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechSte(contents,doc,true);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLESUMMER);
			if (true) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSte(contentsS,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSte(contentsW,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_STEAMCOIL);
			}
			return;
		}
		case TYPE_ELECTRICHEATINGCOIL: {
			allMap = SectionDataConvertUtils.getElectricHeatingCoil(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getElectricheatingcoil(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechElec2(contents,doc,true);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLESUMMER);
			if (true) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechElec2(contentsS,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechElec2(contentsW,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL);
			}
			return;
		}
		case TYPE_STEAMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSteamHumidifier(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_STEAMHUMIDIFIER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getSteamhumidifier(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechSte1(contents,doc,true);
			}
			
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSte1(contents2,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSte1(contents3,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_STEAMHUMIDIFIER);
			}
			return;
		}
		case TYPE_WETFILMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getWetfilmHumidifier(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getWetFilmHumidifier(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechWet(contents,doc,true);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechWet(contents2,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechWet(contents3,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER);
			}
			return;
		}
		case TYPE_SPRAYHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSprayHumidifier(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getSprayhumidifier(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechSpr(contents,doc,true);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSpr(contents2,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechSpr(contents3,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER);
			}
			return;
		}
		case TYPE_FAN: {
			allMap = SectionDataConvertUtils.getFan(noChangeMap,allMap, language);
			/* 当类型为风机：根据风向 送风、回风重新封装报告名称 */
			String fanName = genName(order, SectionTypeEnum.TYPE_FAN, language);
			fanName = SectionDataConvertUtils.getFanName(noChangeMap,allMap, fanName, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, fanName);
			}
			
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN), project, unit, allMap, language,
					unitType);
			// 封装风机段特殊字段
			contents = SectionContentConvertUtils.getFan(contents, noChangeMap, allMap, language);
			if (doc != null) {
				WordTableGen.genTechFan1(contents,doc,true);
			}
			String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_SOUND), project, unit, allMap,
					language, unitType);
			if (doc != null) {
				WordTableGen.genTechFan2(contents2,doc,false);
			}
			String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_ABSORBER), project, unit, allMap,
					language, unitType);
			
			if (doc != null) {
				WordTableGen.genTechFan3(contents3,doc,false);
			}

			String[][] contents5 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_REMARK), project, unit, allMap,
					language, unitType);

			if (doc != null) {
				WordTableGen.genTechFan5(contents5,doc,false);
			}

			// 添加非标选项
//			if (doc != null) {
//				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_FAN);
//			}
			//添加风机过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_FAN), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_HEPAFILTER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getFanNS(nsAHUcontents, unit, language, allMap);
				if (doc != null) {
					WordTableGen.genTechSectionNS(nsAHUcontents, doc);
				}
			}
			
			return;
		}
		case TYPE_COMBINEDMIXINGCHAMBER: {
			allMap = SectionDataConvertUtils.getCombinedMixingChamber(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER), project, unit, allMap, language,
					unitType);
			if (doc != null) {
				WordTableGen.genTechComb(contents,doc,true);
			}
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER), project, unit, allMap,
					language, unitType);
			
			if (doc != null) {
				WordTableGen.genTechComb1(contents1,doc,false);
			}
			
			Object enableWinter = allMap.get(UtilityConstant.META_SECTION_COMBINEDMIXINGCHAMBER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(
						ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER), project, unit, allMap,
						language, unitType);

				if (doc != null) {
					WordTableGen.genTechComb2(contents2, doc, false);
				}

			}
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER);
			}

			return;
		}
		case TYPE_ATTENUATOR: {
			allMap = SectionDataConvertUtils.getAttenuator(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_ATTENUATOR, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ATTENUATOR), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getAttenuator(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechAtte(contents,doc,true);
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ATTENUATOR);
			}
			return;
		}
		case TYPE_DISCHARGE: {
			allMap = SectionDataConvertUtils.getDischarge(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_DISCHARGE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DISCHARGE), project, unit, allMap, language,
					unitType);
			// 封装出风段特殊字段
			contents = SectionContentConvertUtils.getDischarge(contents, allMap, language);
			
			if (doc != null) {
				WordTableGen.genTechDis(contents,doc,true);
			}
			
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_DISCHARGE);
			}
			
			return;
		}
		case TYPE_ACCESS: {
			allMap = SectionDataConvertUtils.getAccess(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_ACCESS, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ACCESS), project, unit, allMap, language,
					unitType);
			// 封装空段特殊字段
			contents = SectionContentConvertUtils.getAccess(contents, allMap, language);
			
			if (doc != null) {
				WordTableGen.genTechAcce(contents,doc,true);
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ACCESS);
			}
			
			return;
		}
		case TYPE_HEPAFILTER: {
			allMap = SectionDataConvertUtils.getHEPAFilter(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_HEPAFILTER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEPAFILTER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getHepafilter(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechHepa(contents,doc,true);
			}

			// 添加非标选项
//			if (doc != null) {
//				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_HEPAFILTER);
//			}
			//添加高效过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_HEPAFILTER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_HEPAFILTER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getHepaNS(nsAHUcontents, unit, language, allMap);
				if (doc != null) {
					WordTableGen.genTechSectionNS(nsAHUcontents, doc);
				}
			}
			
			return;
		}
		case TYPE_ELECTRODEHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getElectrodeHumidifier(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getTechElec1(contents, allMap, language);
			
			if (doc != null) {
				WordTableGen.genTechElec1(contents,doc,true);
			}
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechElec1(contentsS,doc,false);
				}
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				
				if (doc != null) {
				    WordTableGen.addBr(doc);
					WordTableGen.genTechElec1(contentsW,doc,false);
				}
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER);
			}
			return;
		}
		case TYPE_ELECTROSTATICFILTER: {
			allMap = SectionDataConvertUtils.getElectrostaticFilter(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_ELECTROSTATICFILTER, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTROSTATICFILTER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getElectrostaticfilter(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechElec(contents,doc,true);
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ELECTROSTATICFILTER);
			}
			return;
		}
		case TYPE_CTR: {
			allMap = SectionDataConvertUtils.getCtr(noChangeMap,allMap, language);
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_CTR, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_CTR), project, unit, allMap, language,
					unitType);
			
			if (doc != null) {
				WordTableGen.genTechCtr(contents,doc,true);
			}

			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_CTR);
			}
			return;
		}
		case TYPE_HEATRECYCLE: {
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_HEATRECYCLE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEPAFILTER), project, unit, allMap, language,
					unitType);
			if (doc != null) {
				WordTableGen.genTechHepa(contents,doc,true);
			}
			
			// 添加非标选项
			if (doc != null) {
				addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_HEATRECYCLE);
			}
			return;
		}
		case TYPE_WHEELHEATRECYCLE: {
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_WHEELHEATRECYCLE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATRECYCLE), project, unit, allMap, language,
					unitType);
			
			if (doc != null) {
				WordTableGen.genTechWhee(contents,doc,true);
			}
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATRECYCLE_DELIVERY), project, unit, allMap,
					language, unitType);
			
			if (doc != null) {
				WordTableGen.genTechWhee1(contents1,doc,true);
			}

			// 添加非标选项
			addCommonF(doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_WHEELHEATRECYCLE);
			return;
		}
		case TYPE_PLATEHEATRECYCLE: {
			if (doc != null) {
				Word4ReportUtils.addPartName(doc, genName(order, SectionTypeEnum.TYPE_PLATEHEATRECYCLE, language));
			}
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_PLATEHEATRECYCLE), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getPlateHeatRecycle(contents, allMap, language);
			if (doc != null) {
				WordTableGen.genTechWhee(contents,doc,true);
			}
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_PLATEHEATRECYCLE_DELIVERY), project, unit, allMap,
					language, unitType);
			
			if (doc != null) {
				WordTableGen.genTechWhee1(contents1,doc,true);
			}

			// 添加非标选项
			addCommonF( doc, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_PLATEHEATRECYCLE);
			
			return;
		}
		
		default:
			log.warn(MessageFormat.format("技术说明报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}", unit.getUnitNo(),
					key));
		}
	}

	private static String genName(int order, SectionTypeEnum type, LanguageEnum language) {
		StringBuffer s = new StringBuffer("" + order);
		s.append(")");
		s.append(getIntlString(type.getCnName()));
		return s.toString();
	}
	
	private static void addCommonF(XWPFDocument doc, Map<String, String> allMap,
			LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
			throws IOException, DocumentException {
		String[][] techCommonFcontents = ReportContent.getReportContent(REPORT_TECH_NONSTANDARD);
		techCommonFcontents = SectionContentConvertUtils.getCommonF(techCommonFcontents, allMap, sectionType);
		if (null != techCommonFcontents) {
			techCommonFcontents = ValueFormatUtil.transReport(techCommonFcontents, project, unit, allMap, language,
					unitType);
			if (doc != null) {
				WordTableGen.genTechCommonF(techCommonFcontents, doc);
			}
		}
	}
	
}