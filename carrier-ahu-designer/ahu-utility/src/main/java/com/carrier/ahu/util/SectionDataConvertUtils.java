package com.carrier.ahu.util;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.datahouse.common.FacaVelocityUtil;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionL;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo1;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.report.*;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SINGLE;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_S500;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2018/1/3. 报告使用
 */
public class SectionDataConvertUtils {

    public static final String PRESSURE_RANGE = "0.1～0.6Mpa";
    public static final String POWER_STANDARD = "AC22V、50HZ,400w";

    // 封装控制段
    public static Map<String, String> getCtr(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String controlCabinet = UtilityConstant.METASEXON_CTR_CONTROLCABINET;// 控制柜
        String iTaHSensor = UtilityConstant.METASEXON_CTR_ITAHSENSOR;// 室内温湿度传感器
        String iCCSensor = UtilityConstant.METASEXON_CTR_ICCSENSOR;// 室内二氧化碳浓度传感器
        String aPHSensor = UtilityConstant.METASEXON_CTR_APHSENSOR;// 风管静压传感器
        String waterValveActuator = UtilityConstant.METASEXON_CTR_WATERVALVEACTUATOR;// 水阀执行器
        // 控制柜 TODO
        map.put(controlCabinet, getIntlString(ELEVEN_KW_VFC_CABINET));
        if (map.containsKey(iTaHSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(iTaHSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(iTaHSensor, getIntlString(INDOOR_TEMPERATURE_HUMIDITY_SENSOR));
            } else {
                map.put(iTaHSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(iCCSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(iCCSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(iCCSensor, getIntlString(INDOOR_CO2_CONCENTRATION_SENSOR));
            } else {
                map.put(iCCSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(aPHSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(aPHSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(aPHSensor, getIntlString(AIR_DUCT_STATIC_PRESSURE_SENSOR));
            } else {
                map.put(aPHSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(waterValveActuator)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(waterValveActuator));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(waterValveActuator, getIntlString(WATER_VALVE_ACTUATOR));
            } else {
                map.put(waterValveActuator, UtilityConstant.SYS_BLANK);
            }
        }
        return map;
    }

    // 封装静电过滤过滤段
    public static Map<String, String> getElectrostaticFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String supplier = UtilityConstant.METASEXON_ELECTROSTATICFILTER_SUPPLIER;// 供应商
        String way = UtilityConstant.METASEXON_ELECTROSTATICFILTER_WAY;// 过滤形式
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        map.put(way, getIntlString(ELECTROSTATIC_FILTRATION));
        return map;
    }

    // 封装高效过滤段
    public static Map<String, String> getHEPAFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String supplier = UtilityConstant.METASEXON_HEPAFILTER_SUPPLIER;// 供应商
        String filterArrange = UtilityConstant.METASEXON_HEPAFILTER_FILTERARRANGE;// 过滤器布置
        /* 规格信息 */
        String para1 = UtilityConstant.METASEXON_HEPAFILTER_PARA1;
        String paraN1 = UtilityConstant.METASEXON_HEPAFILTER_PARAN1;
        String para2 = UtilityConstant.METASEXON_HEPAFILTER_PARA2;
        String paraN2 = UtilityConstant.METASEXON_HEPAFILTER_PARAN2;
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                int i = 1;
                for (FilterPanelBean filterPanelBean : filterPanelBeanList) {
                    if (i == 1) {
                        if (null != filterPanelBean.getOption()) {
                            map.put(para1, filterPanelBean.getOption());
                        }
                        if (null != filterPanelBean.getCount()) {
                            map.put(paraN1, filterPanelBean.getCount());
                        }
                    } else {
                        if (null != filterPanelBean.getOption()) {
                            map.put(para2, filterPanelBean.getOption());
                        }
                        if (null != filterPanelBean.getCount()) {
                            map.put(paraN2, filterPanelBean.getCount());
                        }
                    }
                    i++;
                }
            }
        }
        return map;
    }

    // 封装消声段
    public static Map<String, String> getDischarge(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        return map;
    }

    // 封装消声段
    public static Map<String, String> getAttenuator(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String deadening = UtilityConstant.METASEXON_ATTENUATOR_DEADENING;// 消音级数
        String type = UtilityConstant.METASEXON_ATTENUATOR_TYPE;// 消音器类型
        return map;
    }

    // 封装空段
    public static Map<String, String> getAccess(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String functionS = UtilityConstant.METASEXON_ACCESS_FUNCTIONS;// 功能
        String uvLamp = UtilityConstant.METASEXON_ACCESS_UVLAMP;// 安装杀菌灯
        String functions = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_ACCESS_FUNCTION));
        map.put(functionS, getIntlString(EMPTY_SECTION));
        if (UtilityConstant.SYS_STRING_DIFFUSER.equals(functions)) {
            map.put(functionS, getIntlString(DIFFUSER));
        }

        if (map.containsKey(uvLamp)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(uvLamp, getIntlString(UV_LIGHT_CUVCA));
            } else {
                map.put(uvLamp, UtilityConstant.SYS_BLANK);
            }
        }
        return map;
    }

    // 封装新回排风段
    public static Map<String, String> getCombinedMixingChamber(Map<String, String> noChangeMap, Map<String, String> metaMap, LanguageEnum language) {
        metaMap = getAirVolume(noChangeMap, metaMap);// 计算风量
        String way = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WAY;
        String NARatio = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_NARATIO;
        String SNAVolume = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_SNAVOLUME;
        String sairvolume = UtilityConstant.METAHU_SAIRVOLUME;
        String windValveP1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP1;
        String windValveP2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP2;
        String windValveH1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH1;
        String windValveH2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH2;
        String windValveN1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN1;
        String windValveN2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN2;
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String damperMeterial = UtilityConstant.METASEXON_COMBINEDFILTER_DAMPERMETERIAL;
        String damperMeterialValue = UtilityConstant.SYS_STRING_NUMBER_1;
        if (metaMap.containsKey(damperMeterial)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(damperMeterial));
            if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_DAMPERMETERIAL_AL.equals(value)) {
                damperMeterialValue = UtilityConstant.SYS_STRING_NUMBER_2;
            }
        }

        String serialValue = UtilityConstant.SYS_BLANK;
        if (metaMap.containsKey(serial)) {
            serialValue = BaseDataUtil.constraintString(noChangeMap.get(serial));
        }

        List<DamperDimensionL> damperDimensionLList = AhuMetadata.findAll(DamperDimensionL.class);
        List<AdjustAirDoor> adjustAirDoorList = AhuMetadata.findAll(AdjustAirDoor.class);
        if (!EmptyUtil.isEmpty(damperDimensionLList)) {
            for (DamperDimensionL damperDimensionL : damperDimensionLList) {
                if (serialValue.equals(damperDimensionL.getUnit()) && damperMeterialValue.equals(damperDimensionL.getDamperType()) && UtilityConstant.SYS_STRING_NUMBER_1.equals(damperDimensionL.getMixType())) {
                    int value = BaseDataUtil.stringConversionInteger(damperDimensionL.getC()) + 108;
                    metaMap.put(windValveP1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + value);
                    metaMap.put(windValveN1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + value);
                } else if (serialValue.equals(damperDimensionL.getUnit()) && UtilityConstant.SYS_STRING_NUMBER_2.equals(damperDimensionL.getMixType())) {
                    metaMap.put(windValveH1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + damperDimensionL.getC());
                }
            }
        }

        if (!EmptyUtil.isEmpty(adjustAirDoorList)) {
            for (AdjustAirDoor adjustAirDoor : adjustAirDoorList) {
                if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && UtilityConstant.SYS_STRING_NUMBER_2.equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveP2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveP2, adjustAirDoor.getWringq());
                    }
                } else if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && UtilityConstant.SYS_STRING_NUMBER_1.equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveH2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveH2, adjustAirDoor.getWringq());
                    }
                } else if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && "3".equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveN2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveN2, adjustAirDoor.getWringq());
                    }
                }
            }
        }

        if (metaMap.containsKey(sairvolume) && metaMap.containsKey(SNAVolume)) {
            String airValue = BaseDataUtil.constraintString(noChangeMap.get(sairvolume));
            String airNewValue = BaseDataUtil.constraintString(noChangeMap.get(SNAVolume));
            double douAirValue = BaseDataUtil.stringConversionDouble(airValue);
            double douAirNewValue = BaseDataUtil.stringConversionDouble(airNewValue);
            if (douAirValue > 0 && douAirNewValue > 0) {
                double value = BaseDataUtil.decimalConvert(douAirNewValue / douAirValue, 2) * 100;
                metaMap.put(NARatio, BaseDataUtil.integerConversionString(BaseDataUtil.doubleConversionInteger(value)));
            } else {
                metaMap.put(NARatio, UtilityConstant.SYS_STRING_NUMBER_0);
            }
        }
        metaMap.put(way, getIntlString(FRESH_RETURN_AIR));

        /* add executor comment with air interface */
        String AInterfaceMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_AINTERFACE);
        String AInterface = String.valueOf(noChangeMap.get(AInterfaceMetaKey));
        if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_ED.equals(AInterface)
                || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_FL.equals(AInterface)) {
            String Executor1MetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_EXECUTOR1);
            String Executor1 = String.valueOf(noChangeMap.get(Executor1MetaKey));
            String executorString = getValueByUnit(Executor1, Executor1MetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
            if (StringUtils.isNotEmpty(executorString)) {
                AInterface = getValueByUnit(AInterface, AInterfaceMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
                metaMap.put(AInterfaceMetaKey, String.format("%s(%s)", AInterface, executorString));
            }
        }

        /* add fix repair lamp with door */
        String fixRepairLampMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER,
                KEY_FIX_REPAIR_LAMP);
        if (Boolean.valueOf(String.valueOf(noChangeMap.get(fixRepairLampMetaKey)))) {
            String osDoorMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_OSDOOR);
            String osDoor = String.valueOf(noChangeMap.get(osDoorMetaKey));
            String isDoorMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_ISDOOR);
            String isDoor = String.valueOf(noChangeMap.get(isDoorMetaKey));
            osDoor = getValueByUnit(osDoor, osDoorMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
            isDoor = getValueByUnit(isDoor, isDoorMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);

            String fixRepairLampString = getIntlString(HAS_FIX_REPAIR_LAMP);
            metaMap.put(osDoorMetaKey, String.format("%s(%s)", osDoor, fixRepairLampString));
            metaMap.put(isDoorMetaKey, String.format("%s(%s)", isDoor, fixRepairLampString));
        }
        return metaMap;
    }

    private static String getValueByUnit(String value, String parameterKey, SectionTypeEnum sectionType,
                                         LanguageEnum language) {
        if (StringUtils.isEmpty(value) || UtilityConstant.SYS_ASSERT_NULL.equals(value)) {
            return StringUtils.EMPTY;
        }
        return AhuSectionMetas.getInstance().getValueByUnit(value, parameterKey, sectionType.getId(), language);
    }

    // 封装干蒸加湿段
    public static Map<String, String> getSteamHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_STEAMHUMIDIFIER_WAY;// 加湿方式
        String supplier = UtilityConstant.METASEXON_STEAMHUMIDIFIER_SUPPLIER;// 供应商
        String tubeDiameter = UtilityConstant.METASEXON_STEAMHUMIDIFIER_TUBEDIAMETER;// 进气管径
        String type = UtilityConstant.METASEXON_STEAMHUMIDIFIER_HTYPES;// 类型
        String ControlM = UtilityConstant.METASEXON_STEAMHUMIDIFIER_CONTROLM;//控制方式
        if (map.containsKey(type)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(type));
            if ((Integer.valueOf(value)) == 1) {
                map.put(tubeDiameter, "DN15");
            } else if ((Integer.valueOf(value)) == 2) {
                map.put(tubeDiameter, "DN20");
            } else if ((Integer.valueOf(value)) == 3) {
                map.put(tubeDiameter, "DN25");
            } else if ((Integer.valueOf(value)) == 4) {
                map.put(tubeDiameter, "DN30");
            }
        }
        map.put(way, getIntlString(DRY_STEAM_AIR));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));

        String ctlMValue = BaseDataUtil.constraintString(noChangeMap.get(ControlM));
        if (EmptyUtil.isNotEmpty(ctlMValue) && "EC".equals(ctlMValue)) {
            map.put(ControlM, getIntlString(CONTROLM_EC));
        }
        return map;
    }

    // 封装电极加湿段
    public static Map<String, String> getElectrodeHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WAY;// 加湿方式
        String supplier = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SUPPLIER;// 供应商
        String waterQuality = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WATERQUALIITY;// 供水水质
        String pressure = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_PRESSURE;// 进水压力
        String tubeDiameter = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_TUBEDIAMETER;// 进气管径
        String wasteTubeDiameter = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WASTETUBEDIAMETER;// 排污管径
        String power = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_POWER;// 额定功率 查询表
        String controller = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_CONTROLLER;// 控制方式
        // TODO
        map.put(way, getIntlString(ELECTRODE_HUMIDIFICATION));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        map.put(waterQuality, getIntlString(CLEAN_TAP_WATER));
        map.put(pressure, "0.1~0.35Mpa");
        map.put(tubeDiameter, "DN15");
        map.put(wasteTubeDiameter, ">=DN35");
        map.put(controller, getIntlString(ANALOG_VALUE) + "0-10V");
        return map;
    }

    // 封装高压喷雾段
    public static Map<String, String> getSprayHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WAY;// 加湿方式
        String waterQuality = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WATERQUALITY;// 供水水质
        String quantity = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_QUANTITY;// 供水量
        String pressure = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_PRESSURE;// 进水压力
        String power = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_POWER;// 电源与额定功率
        String supplier = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SUPPLIER;// 供应商
        String install = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_INSTALL;// 安装段
        String previous = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_PREVIOUS;// 当前段标识
        if (map.containsKey(previous)) {
            String value = noChangeMap.get(previous).toString();
            if (value.equals(SectionTypeEnum.TYPE_COLD.getId())) {
                map.put(install, getIntlString(COOLING_COIL_SECTION));
            } else if (value.equals(SectionTypeEnum.TYPE_HEATINGCOIL.getId())) {
                map.put(install, getIntlString(HOT_WATER_COIL_SECTION));
            }
        }
        map.put(way, getIntlString(HIGH_PRESSURE_SPRAY));
        map.put(waterQuality, getIntlString(TAP_WATER));
        map.put(quantity, getIntlString(ONE_HALF_TIMES_GREATER_THAN_SPRAY_AMOUNT));
        map.put(pressure, PRESSURE_RANGE);
        map.put(power, POWER_STANDARD);
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        return map;
    }

    // 封装电加热盘管段
    public static Map<String, String> getElectricHeatingCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String svelocity = UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_WVELOCITY;//迎面风速
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号

        // 计算迎面风速
        double airVolume = 0.00;
        double svelocityDB = 0.00;
        String airDirection = BaseDataUtil
                .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
        if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
            airVolume = BaseDataUtil
                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
        } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
            airVolume = BaseDataUtil
                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
        }
        try {
            svelocityDB = FacaVelocityUtil.getOtherCommon(airVolume, noChangeMap.get(serial),
                    SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId());
            BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
            shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put(svelocity, String.valueOf(shaftPowerBd));
        } catch (Exception e) {
            // 计算失败则取系统迎面风速
            map.put(svelocity, noChangeMap.get(svelocity));
            e.printStackTrace();
        }

        map = getAirVolume(noChangeMap, map);// 计算风量
        String heatMedium = UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_HEATMEDIUM;// 热媒
        map.put(heatMedium, getIntlString(ELECTRIC_HEATING));
        return map;
    }

    // 封装蒸汽盘管段
    public static Map<String, String> getSteamCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String svelocity = UtilityConstant.METASEXON_STEAMCOIL_WVELOCITY;//迎面风速
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号

        // 计算迎面风速
        double airVolume = 0.00;
        double svelocityDB = 0.00;
        String airDirection = BaseDataUtil
                .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
        if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
            airVolume = BaseDataUtil
                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
        } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
            airVolume = BaseDataUtil
                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
        }
        try {
            svelocityDB = FacaVelocityUtil.getOtherCommon(airVolume, noChangeMap.get(serial),
                    SectionTypeEnum.TYPE_STEAMCOIL.getId());
            BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
            shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put(svelocity, String.valueOf(shaftPowerBd));
        } catch (Exception e) {
            // 计算失败则取系统迎面风速
            map.put(svelocity, noChangeMap.get(svelocity));
            e.printStackTrace();
        }

        map = getAirVolume(noChangeMap, map);// 计算风量
        String heatMedium = UtilityConstant.METASEXON_STEAMCOIL_HEATMEDIUM;// 热媒
        map.put(heatMedium, getIntlString(STEAM));
        return map;
    }

    // 封装单层过滤段
    public static Map<String, String> getFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String supplier = UtilityConstant.METASEXON_FILTER_SUPPLIER;// 供应商
        String filterArrange = UtilityConstant.METASEXON_FILTER_FILTERARRANGE;// 过滤器布置
        /* 规格信息 */
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                for (int i = 0; i < filterPanelBeanList.size(); i++) {
                    FilterPanelBean filterPanelBean = filterPanelBeanList.get(i);
                    String paraKey = SectionMetaUtils.getMetaSectionKey(TYPE_SINGLE, "para" + i);
                    String paraNKey = SectionMetaUtils.getMetaSectionKey(TYPE_SINGLE, "paraN" + i);
                    if (null != filterPanelBean.getOption()) {
                        map.put(paraKey, filterPanelBean.getOption());
                    }
                    if (null != filterPanelBean.getCount()) {
                        map.put(paraNKey, filterPanelBean.getCount());
                    }
                }
            }
        }
        return map;
    }

    // 封装湿膜加湿段
    public static Map<String, String> getWetfilmHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String style = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_STYLE;
        String waterQuality = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WATERQUALITY;
        String install = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_INSTALL;// 安装段
        String previous = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_PREVIOUS;// 当前段标识
        String waterQuantity = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WATERQUANTITY;// 水量

        map.put(style, getIntlString(DIRECT_WETFILM_HUMIDIFICATION));
        map.put(waterQuality, getIntlString(TAP_WATER));
        if (map.containsKey(previous)) {
            String value = noChangeMap.get(previous).toString();
            if (value.equals(SectionTypeEnum.TYPE_COLD.getId())) {
                map.put(install, getIntlString(COOLING_COIL_SECTION));
            } else if (value.equals(SectionTypeEnum.TYPE_HEATINGCOIL.getId())) {
                map.put(install, getIntlString(HOT_WATER_COIL_SECTION));
            }
        }
        if (map.containsKey(waterQuantity)) {
            Object value = noChangeMap.get(waterQuantity);
            if (!EmptyUtil.isEmpty(value)) {
                map.put(waterQuantity, String.format("%.1f", BaseDataUtil.stringConversionDouble(BaseDataUtil.objectToString(value))));
            }
        }
        return map;
    }

    // 封装风机报告名称数据
    public static String getFanName(Map<String, String> noChangeMap, Map<String, String> map, String fanName, LanguageEnum language) {
        fanName = fanName.substring(0, fanName.indexOf(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE) + 1) + getIntlString(SUPPLY_FAN);// 默认送风机
        String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
        if (map.containsKey(airDirection)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(airDirection));
            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
                fanName = fanName.substring(0, fanName.indexOf(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE) + 1) + getIntlString(RETURN_FAN);
            }
        }
        return fanName;
    }

    private static double getLw(Map<String, String> map) {
        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;
        double vt = 0.00;
        double va = 0.00;
        double vo = 0.00;
        va = BaseDataUtil.stringConversionDouble(map.get(engineData1)) - getSForFranceValueByItem("D1");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData2)) - getSForFranceValueByItem("D2");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData3)) - getSForFranceValueByItem("D3");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData4)) - getSForFranceValueByItem("D4");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData5)) - getSForFranceValueByItem("D5");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData6)) - getSForFranceValueByItem("D6");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData7)) - getSForFranceValueByItem("D7");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData8)) - getSForFranceValueByItem("D8");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        return 10 * Math.log10(vt);
    }

    private static double getSForFranceValueByItem(String item) {
        SForFrance sforFrance = AhuMetadata.findOne(SForFrance.class, item);
        if (EmptyUtil.isNotEmpty(sforFrance)) {
            return sforFrance.getValue();
        }
        return 0;
    }

    private static double getLp(Map<String, String> map) {
        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;
        double vt = 0.00;
        double va = 0.00;
        double vo = 0.00;
        va = BaseDataUtil.stringConversionDouble(map.get(engineData1)) - getSForFranceValueByItem("In1");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData2)) - getSForFranceValueByItem("In2");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData3)) - getSForFranceValueByItem("In3");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData4)) - getSForFranceValueByItem("In4");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData5)) - getSForFranceValueByItem("In5");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData6)) - getSForFranceValueByItem("In6");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData7)) - getSForFranceValueByItem("In7");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData8)) - getSForFranceValueByItem("In8");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        return 10 * Math.log10(vt);
    }

    // 封装风机报告数据
    public static Map<String, String> getFan(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String doorOnBothSide = UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE;//两侧开门
        String fixRepairLamp = UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP;//安装检修灯
        String para13 = UtilityConstant.METASEXON_FAN_PARA13;//耐盐雾型减震器
        String ptc = UtilityConstant.METASEXON_FAN_PTC;//ptc
        String beltGuard = UtilityConstant.METASEXON_FAN_BELTGUARD;// 皮带保护罩
        String seismicPringIsolator = UtilityConstant.METASEXON_FAN_SEISMICPRINGISOLATOR;// 防剪切减震器
        String insulation = UtilityConstant.METASEXON_FAN_INSULATION;// 绝缘等级
        String Protection = UtilityConstant.METASEXON_FAN_PROTECTION;// 防护等级
        String standbyMotor = UtilityConstant.METASEXON_FAN_STANDBYMOTOR;// 备用电机
        String motorBrand = UtilityConstant.METASEXON_FAN_MOTORBRAND;//	电机品牌
        String lw = UtilityConstant.METASEXON_FAN_LW;// A声功率级
        String lp = UtilityConstant.METASEXON_FAN_LP;// A声声压级
        String type = UtilityConstant.METASEXON_FAN_TYPE;// 电机类型
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String fanModel = UtilityConstant.METASEXON_FAN_FANMODEL;// 风机型号
        String motorBaseNo = UtilityConstant.METASEXON_FAN_MOTORBASENO;// 机座号
        String outlet = UtilityConstant.METASEXON_FAN_OUTLET;//风机形式
        String outletDirection = UtilityConstant.METASEXON_FAN_OUTLETDIRECTION;//出风方向
        String supplyDamper = UtilityConstant.METASEXON_FAN_SUPPLYDAMPER;//送风口型式
        String returnPosition = UtilityConstant.METASEXON_FAN_RETURNPOSITION;//送风出口位置
        String engineType = UtilityConstant.META_SECTION_FAN_ENGINETYPE;//引擎类型

        if (map.containsKey(engineType)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(engineType));
            if (FanEnginesEnum.YLD.getCode().equals(value)) {
                map.put(engineType, FanEnginesEnum.YLD.getEnName());
            } else {
                map.put(engineType, FanEnginesEnum.GK.getEnName());
            }
        }
        if (map.containsKey(doorOnBothSide)) {// 两侧开门
            String value = BaseDataUtil.constraintString(noChangeMap.get(doorOnBothSide));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(doorOnBothSide, getIntlString(YES));
            } else {
                map.put(doorOnBothSide, getIntlString(NO));
            }
        }
        if (map.containsKey(fixRepairLamp)) {// 安装检修灯
            String value = BaseDataUtil.constraintString(noChangeMap.get(fixRepairLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(fixRepairLamp, getIntlString(YES));
            } else {
                map.put(fixRepairLamp, getIntlString(NO));
            }
        }
        if (map.containsKey(para13)) {// 耐盐雾型减震器
            String value = BaseDataUtil.constraintString(noChangeMap.get(para13));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(para13, getIntlString(YES));
            } else {
                map.put(para13, getIntlString(NO));
            }
        }
        if (map.containsKey(ptc)) {// ptc
            String value = BaseDataUtil.constraintString(noChangeMap.get(ptc));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(ptc, getIntlString(YES));
            } else {
                map.put(ptc, getIntlString(NO));
            }
        }
        if (map.containsKey(beltGuard)) {// 皮带保护罩
            String value = BaseDataUtil.constraintString(noChangeMap.get(beltGuard));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(beltGuard, getIntlString(YES));
            } else {
                map.put(beltGuard, getIntlString(NO));
            }
        }
        map.put(lp, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(getLp(noChangeMap), 1)));
        map.put(lw, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(getLw(noChangeMap), 1)));
        /*增加描述*/
        String fan_remark = UtilityConstant.SYS_MAP_FAN_REMARK;
        String fan_remark_one = UtilityConstant.SYS_MAP_FAN_REMARK_ONE;
        String fan_remark_two = UtilityConstant.SYS_MAP_FAN_REMARK_TWO;
        String fan_type_remark = UtilityConstant.SYS_MAP_FAN_TYPE_REMARK;
        map.put(fan_remark, getIntlString(FAN_REMARK));
        map.put(fan_remark_one, getIntlString(FAN_REMARK_ONE));
        map.put(fan_remark_two, getIntlString(FAN_REMARK_TWO));
        if (map.containsKey(type)) {
            Object value = (Object) noChangeMap.get(type);
            value = BaseDataUtil.objectToString(value);
            if (SystemCalculateConstants.FAN_TYPE_5.equals(value) || SystemCalculateConstants.FAN_TYPE_6.equals(value)) {
                map.put(fan_type_remark, getIntlString(FAN_TYPE_REMARK));
            } else {
                map.put(fan_type_remark, UtilityConstant.SYS_BLANK);
            }
        }
        String lw1 = UtilityConstant.METASEXON_FAN_LW1;// A声功率级
        String lw2 = UtilityConstant.METASEXON_FAN_LW2;// A声功率级
        String lw3 = UtilityConstant.METASEXON_FAN_LW3;// A声功率级
        String lw4 = UtilityConstant.METASEXON_FAN_LW4;// A声功率级
        String lw5 = UtilityConstant.METASEXON_FAN_LW5;// A声功率级
        String lw6 = UtilityConstant.METASEXON_FAN_LW6;// A声功率级
        String lw7 = UtilityConstant.METASEXON_FAN_LW7;// A声功率级
        String lw8 = UtilityConstant.METASEXON_FAN_LW8;// A声功率级
        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;
        map.put(lw1, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData1)) - getSForFranceValueByItem("D1") - getSForFranceValueByItem("Ai1"), 0)));
        map.put(lw2, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData2)) - getSForFranceValueByItem("D2") - getSForFranceValueByItem("Ai2"), 0)));
        map.put(lw3, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData3)) - getSForFranceValueByItem("D3") - getSForFranceValueByItem("Ai3"), 0)));
        map.put(lw4, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData4)) - getSForFranceValueByItem("D4") - getSForFranceValueByItem("Ai4"), 0)));
        map.put(lw5, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData5)) - getSForFranceValueByItem("D5") - getSForFranceValueByItem("Ai5"), 0)));
        map.put(lw6, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData6)) - getSForFranceValueByItem("D6") - getSForFranceValueByItem("Ai6"), 0)));
        map.put(lw7, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData7)) - getSForFranceValueByItem("D7") - getSForFranceValueByItem("Ai7"), 0)));
        map.put(lw8, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData8)) - getSForFranceValueByItem("D8") - getSForFranceValueByItem("Ai8"), 0)));

        String lp1 = UtilityConstant.METASEXON_FAN_LP1;// A声声压级
        String lp2 = UtilityConstant.METASEXON_FAN_LP2;// A声声压级
        String lp3 = UtilityConstant.METASEXON_FAN_LP3;// A声声压级
        String lp4 = UtilityConstant.METASEXON_FAN_LP4;// A声声压级
        String lp5 = UtilityConstant.METASEXON_FAN_LP5;// A声声压级
        String lp6 = UtilityConstant.METASEXON_FAN_LP6;// A声声压级
        String lp7 = UtilityConstant.METASEXON_FAN_LP7;// A声声压级
        String lp8 = UtilityConstant.METASEXON_FAN_LP8;// A声声压级

        map.put(lp1, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData1)) - getSForFranceValueByItem("In1") - getSForFranceValueByItem("Ai1"), 0)));
        map.put(lp2, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData2)) - getSForFranceValueByItem("In2") - getSForFranceValueByItem("Ai2"), 0)));
        map.put(lp3, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData3)) - getSForFranceValueByItem("In3") - getSForFranceValueByItem("Ai3"), 0)));
        map.put(lp4, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData4)) - getSForFranceValueByItem("In4") - getSForFranceValueByItem("Ai4"), 0)));
        map.put(lp5, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData5)) - getSForFranceValueByItem("In5") - getSForFranceValueByItem("Ai5"), 0)));
        map.put(lp6, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData6)) - getSForFranceValueByItem("In6") - getSForFranceValueByItem("Ai6"), 0)));
        map.put(lp7, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData7)) - getSForFranceValueByItem("In7") - getSForFranceValueByItem("Ai7"), 0)));
        map.put(lp8, BaseDataUtil.doubleConversionIntString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData8)) - getSForFranceValueByItem("In8") - getSForFranceValueByItem("Ai8"), 0)));

        String positionA = UtilityConstant.METASEXON_FAN_POSITIONA;// 减震器A
        String positionB = UtilityConstant.METASEXON_FAN_POSITIONB;// 减震器B
        String positionC = UtilityConstant.METASEXON_FAN_POSITIONC;// 减震器C
        String positionD = UtilityConstant.METASEXON_FAN_POSITIOND;// 减震器D
        String positionE = UtilityConstant.METASEXON_FAN_POSITIONE;// 减震器E
        String positionF = UtilityConstant.METASEXON_FAN_POSITIONF;// 减震器F
        String positionG = UtilityConstant.METASEXON_FAN_POSITIONG;// 减震器G
        String positionH = UtilityConstant.METASEXON_FAN_POSITIONH;// 减震器H
        String positionI = UtilityConstant.METASEXON_FAN_POSITIONI;// 减震器I
        String positionJ = UtilityConstant.METASEXON_FAN_POSITIONJ;// 减震器J

        String fan = UtilityConstant.SYS_BLANK;
        String machineSiteNo = UtilityConstant.SYS_BLANK;
        if (map.containsKey(fanModel)) {
            fan = noChangeMap.get(fanModel);
            if (map.containsKey(serial)) {
                Object value = noChangeMap.get(serial);
                String unit = SystemCountUtil.getUnit(String.valueOf(value));
                if (BaseDataUtil.stringConversionInteger(unit) < 2532) {
                    if (!fan.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_SYW)) {
                        if (fan.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_FC)) {//前弯
                            fan = getSeriesByFanModel(fan) + "KTQ";
                        } else {//后弯
                            fan = getSeriesByFanModel(fan) + "KTH";
                        }
                    }
                } else {
                    fan = "KHF" + getSeriesByFanModel(fan);
                }
            }
        }

        if (map.containsKey(motorBaseNo)) {
            String valueBaseNo = noChangeMap.get(motorBaseNo);
            if (map.containsKey(serial)) {
                Object value = noChangeMap.get(serial);
                String unit = SystemCountUtil.getUnit(String.valueOf(value));
                if (BaseDataUtil.stringConversionInteger(unit) < 2532) {
                    List<SKFanMotor> skFanMotorList = AhuMetadata.findAll(SKFanMotor.class);
                    if (!EmptyUtil.isEmpty(skFanMotorList)) {
                        for (SKFanMotor skFanMotor : skFanMotorList) {
                            if (skFanMotor.getMachineSiteNo().equals(valueBaseNo)) {
                                machineSiteNo = skFanMotor.getMachineSiteNo1();
                                break;
                            }
                        }
                    }
                } else {
                    machineSiteNo = valueBaseNo;
                }
            }
        }

        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            if (BaseDataUtil.stringConversionInteger(unit) < 2532) {
                SKAbsorber skAbsorber = AhuMetadata.findOne(SKAbsorber.class, fan, machineSiteNo);
                if (null != skAbsorber) {
                    map.put(positionA, skAbsorber.getA());
                    map.put(positionB, skAbsorber.getB());
                    map.put(positionC, skAbsorber.getC());
                    map.put(positionD, skAbsorber.getD());
                    map.put(positionE, skAbsorber.getE());
                    map.put(positionF, skAbsorber.getF());
                    map.put(positionG, "");
                    map.put(positionH, "");
                    map.put(positionI, "");
                    map.put(positionJ, "");
                    if (map.containsKey(para13)) {// 耐盐雾型减震器
                        String valuePara13 = BaseDataUtil.constraintString(noChangeMap.get(para13));
                        if (valuePara13.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                            if (!EmptyUtil.isEmpty(skAbsorber.getA())) {
                                map.put(positionA, skAbsorber.getA() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getB())) {
                                map.put(positionB, skAbsorber.getB() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getC())) {
                                map.put(positionC, skAbsorber.getC() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getD())) {
                                map.put(positionD, skAbsorber.getD() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getE())) {
                                map.put(positionE, skAbsorber.getE() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getF())) {
                                map.put(positionF, skAbsorber.getF() + SYS_STRING_S500);
                            }
                        }
                    }
                }
            } else {
                SKAbsorberBig skAbsorberBig = AhuMetadata.findOne(SKAbsorberBig.class, fan, machineSiteNo);
                if (null != skAbsorberBig) {
                    map.put(positionA, skAbsorberBig.getA());
                    map.put(positionB, skAbsorberBig.getB());
                    map.put(positionC, skAbsorberBig.getC());
                    map.put(positionD, skAbsorberBig.getD());
                    map.put(positionE, skAbsorberBig.getE());
                    map.put(positionF, skAbsorberBig.getF());
                    map.put(positionG, skAbsorberBig.getG());
                    map.put(positionH, skAbsorberBig.getH());
                    map.put(positionI, skAbsorberBig.getI());
                    map.put(positionJ, skAbsorberBig.getJ());
                    if (map.containsKey(para13)) {// 耐盐雾型减震器
                        String valuePara13 = BaseDataUtil.constraintString(noChangeMap.get(para13));
                        if (valuePara13.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getA())) {
                                map.put(positionA, skAbsorberBig.getA() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getB())) {
                                map.put(positionB, skAbsorberBig.getB() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getC())) {
                                map.put(positionC, skAbsorberBig.getC() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getD())) {
                                map.put(positionD, skAbsorberBig.getD() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getE())) {
                                map.put(positionE, skAbsorberBig.getE() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getF())) {
                                map.put(positionF, skAbsorberBig.getF() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getG())) {
                                map.put(positionG, skAbsorberBig.getG() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getH())) {
                                map.put(positionH, skAbsorberBig.getH() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getI())) {
                                map.put(positionI, skAbsorberBig.getI() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getJ())) {
                                map.put(positionJ, skAbsorberBig.getJ() + SYS_STRING_S500);
                            }
                        }
                    }
                }
            }
        }
        map.put(motorBrand, getIntlString(STANDARD_SUPPLIER_MOTOR));
        if (map.containsKey(seismicPringIsolator)) {// 防剪切减震器
            String value = BaseDataUtil.constraintString(noChangeMap.get(seismicPringIsolator));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(seismicPringIsolator, getIntlString(YES));

            } else {
                map.put(seismicPringIsolator, getIntlString(NO));
            }
        }
        if (map.containsKey(insulation)) {
            String value = String.valueOf(noChangeMap.get(insulation));
            map.put(insulation, value.substring(0, 1));
            map.put(Protection, value.substring(value.length() - 4, value.length()));
        }
        if (map.containsKey(standbyMotor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(standbyMotor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(standbyMotor, getIntlString(YES));
            } else {
                map.put(standbyMotor, getIntlString(NO));
            }
        }
        if (map.containsKey(outletDirection)) {
            String str1 = UtilityConstant.SYS_BLANK;
            String str2 = UtilityConstant.SYS_BLANK;
            if (UtilityConstant.JSON_FAN_OUTLET_WWK.equals(noChangeMap.get(outlet))) {
                if (UtilityConstant.SYS_MAP_NO.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(NONE);
                } else if (FAN_SENDPOSITION_TOP.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(TOP_FIELD);
                } else if (FAN_SENDPOSITION_FACE.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(FACE_FIELD);
                }
                if (UtilityConstant.SYS_MAP_NO.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(NONE);
                } else if (FAN_SUPPLYDAMPER_FL.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(FLANGE);
                } else if (FAN_SUPPLYDAMPER_GSD.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(GL_STEEL_DAMPER);
                } else if (FAN_SUPPLYDAMPER_AD.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(ALUMINUM_DAMPER);
                }
                map.put(outletDirection, str1 + UtilityConstant.SYS_PUNCTUATION_SLASH + str2);
            }
        }
        return map;
    }

    // 追加热水盘管excel报告自定义数据
    public static Map<String, String> appendHeatingcoil(Map<String, String> noChangeMap, Map<String, String> map) {
        String headerDia = UtilityConstant.METASEXON_HEATINGCOIL_HEADERDIA;// 进出水管径
        map.put(headerDia, UtilityConstant.SYS_BLANK);
        String coilModel = UtilityConstant.METASEXON_HEATINGCOIL_COILMODEL;// 盘管型号描述
        map.put(coilModel, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装热水盘管报告数据
    public static Map<String, String> getHeatingcoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendHeatingcoil(noChangeMap, map);
        map = getAirVolume(noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_HEATINGCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_HEATINGCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_HEATINGCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_HEATINGCOIL_INTERVAL;// 管间距
        String coilFrameMaterialNote = UtilityConstant.METASEXON_HEATINGCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_HEATINGCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_HEATINGCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_HEATINGCOIL_ROWS;// 排数
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String baffleMaterial = UtilityConstant.METASEXON_HEATINGCOIL_BAFFLEMATERIAL;// 挡风板材质
        String serialStr = UtilityConstant.SYS_BLANK;
        String headerDia = UtilityConstant.METASEXON_HEATINGCOIL_HEADERDIA;// 进出水管径
        String coilModel = UtilityConstant.METASEXON_HEATINGCOIL_COILMODEL;// 盘管型号描述
        String finDensity = UtilityConstant.METASEXON_HEATINGCOIL_FINDENSITY;// 片距
        String circuit = UtilityConstant.METASEXON_HEATINGCOIL_CIRCUIT;// 回路
        String heightLength = UtilityConstant.METASEXON_HEATINGCOIL_HEIGHTLENGTH;// 高度/长度
        String sconcentration = UtilityConstant.METASEXON_HEATINGCOIL_SCONCENTRATION;//浓度
        String scoolant = UtilityConstant.METASEXON_HEATINGCOIL_SCOOLANT;//介质
        String WreturnEnteringFluidTemperature = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE;//进水温度（冬季计算结果）       
        String WreturnWTAScend = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNWTASCEND;//水温升（夏季计算结果）
        String WreturnOutFluidTemperature = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNOUTFLUIDTEMPERATURE;//出水温度
        String svelocity = UtilityConstant.METASEXON_HEATINGCOIL_WVELOCITY;//迎面风速

        // 计算迎面风速
        if (map.containsKey(svelocity)) {
            double airVolume = 0.00;
            double svelocityDB = 0.00;
            String airDirection = BaseDataUtil
                    .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
            if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
            } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
            }
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_HEATINGCOIL.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
        }

        //计算出水温度
        if (map.containsKey(WreturnEnteringFluidTemperature)) {
            double jswd = BaseDataUtil.stringConversionDouble(noChangeMap.get(WreturnEnteringFluidTemperature));
            double swj = BaseDataUtil.stringConversionDouble(noChangeMap.get(WreturnWTAScend));
            map.put(WreturnOutFluidTemperature, String.valueOf(jswd - swj));
        }


        if (map.containsKey(scoolant)) {
            if (UtilityConstant.SYS_STRING_NUMBER_1.equals(noChangeMap.get(scoolant))) {//介质 为水，浓度100%
                map.put(sconcentration, "100%");
            } else {
                map.put(sconcentration, noChangeMap.get(sconcentration) + UtilityConstant.SYS_PUNCTUATION_PERCENTAGE);
            }
        }
        String tubeDiameterValue = UtilityConstant.SYS_BLANK;
        if (map.containsKey(tubeDiameter)) {
            String value = noChangeMap.get(tubeDiameter);
            tubeDiameterValue = value;
            if (value.equals(HEATINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_HEATINGCOIL_MODENOTE_1_2);
                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, "12.7");
            }
            if (value.equals(HEATINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_HEATINGCOIL_MODENOTE_3_8);
                map.put(interval, "25.4*22");
                map.put(tubeDiameter, "9.52");
            }
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (BaseDataUtil.stringConversionInteger(unit) > 2333) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.HEATINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
//            if (map.containsKey(rows)) {
//                String valuerows = BaseDataUtil.constraintString(noChangeMap.get(rows));
//                if (valuerows.equals(2) || valuerows.equals(1)) {
//                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
//                } else {
//                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
//                }
//            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }
        // 进出水管径
        if (map.containsKey(headerDia)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
            } else {
                valueStr = 1;
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = String.valueOf(noChangeMap.get(rows));
                if (valuerows.equals(2) || valuerows.equals(1)) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(headerDia, sTakeoverSize.getHeadSize());
            } else {
                map.put(headerDia, UtilityConstant.SYS_BLANK);
            }
        }

        //盘管型号
        if (map.containsKey(coilModel)) {
            if (map.containsKey(tubeDiameter)) {
                String value = noChangeMap.get(tubeDiameter);

                String valuerows = UtilityConstant.SYS_BLANK;
                if (map.containsKey(rows)) {
                    valuerows = String.valueOf(noChangeMap.get(rows));
                }
                if (value.equals(HEATINGCOIL_TUBEDIAMETER_1_2)) {
                    map.put(coilModel, "12.7mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                } else {
                    map.put(coilModel, "9.52mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                }
            }

        }
        map.put(coilNote, "28CU");
        //冷水盘管高度长度
        StringBuffer heightLen = new StringBuffer(UtilityConstant.SYS_BLANK);
        if (tubeDiameterValue.equals(HEATINGCOIL_TUBEDIAMETER_1_2)) {
            List<SCoilInfo> coilInfos = AhuMetadata.findList(SCoilInfo.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 31.75));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (tubeDiameterValue.equals(HEATINGCOIL_TUBEDIAMETER_3_8)) {
            List<SCoilInfo1> coilInfos = AhuMetadata.findList(SCoilInfo1.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 25.4));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (heightLen.toString().length() > 0)
            map.put(heightLength, heightLen.toString().substring(0, heightLen.toString().length() - 1));
        return map;
    }

    // 封装直接蒸发式盘管报告数据
    public static Map<String, String> getDXCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_DXCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_DXCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_DXCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_DXCOIL_INTERVAL;// 管间距
        String finTem = UtilityConstant.METASEXON_DXCOIL_FINTEM;// 最小翅片表面温度
        String cpTem = UtilityConstant.METASEXON_DXCOIL_CPTEM;// 最小铜管壁表面温度
        String circuit = UtilityConstant.METASEXON_DXCOIL_CIRCUIT;// 回路
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String coilFrameMaterialNote = UtilityConstant.METASEXON_DXCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_DXCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_DXCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_DXCOIL_ROWS;// 排数
        String heightLength = UtilityConstant.METASEXON_DXCOIL_HEIGHTLENGTH;// 高度/长度
        String baffleMaterial = UtilityConstant.METASEXON_DXCOIL_BAFFLEMATERIAL;// 挡风板材质
        String serialStr = UtilityConstant.SYS_BLANK;
        String svelocity = UtilityConstant.METASEXON_DXCOIL_SVELOCITY;//迎面风速

        // 计算迎面风速
        if (map.containsKey(svelocity)) {
            double airVolume = 0.00;
            double svelocityDB = 0.00;
            String airDirection = BaseDataUtil
                    .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
            if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
            } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
            }
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
        }

        if (map.containsKey(tubeDiameter)) {
            String value = noChangeMap.get(tubeDiameter);
            if (value.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_1_2);
                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, "12.7");
            }
            if (value.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_3_8);
                map.put(interval, "25.4*22");
                map.put(tubeDiameter, "9.52");
            }
        }
        if (map.containsKey(circuit)) {// TODO
            Object value = noChangeMap.get(circuit);
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (BaseDataUtil.stringConversionInteger(unit) > 2333) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = noChangeMap.get(rows);
                if (valuerows.equals(2) || valuerows.equals(1)) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }

        map.put(coilNote, "28CW");
        map.put(finTem, ">32");
        map.put(cpTem, ">32");
        return map;
    }

    // 追加冷水盘管excel报告自定义数据
    public static Map<String, String> appendCoolingCoil(Map<String, String> noChangeMap, Map<String, String> map) {
        String headerDia = UtilityConstant.METASEXON_COOLINGCOIL_HEADERDIA;// 进出水管径
        map.put(headerDia, UtilityConstant.SYS_BLANK);
        String coilModel = UtilityConstant.METASEXON_COOLINGCOIL_COILMODEL;// 盘管型号描述
        map.put(coilModel, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装冷水盘管报告数据
    public static Map<String, String> getCoolingCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendCoolingCoil(noChangeMap, map);//追加自定义key
        map = getAirVolume(noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_COOLINGCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_COOLINGCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_COOLINGCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_COOLINGCOIL_INTERVAL;// 管间距
        String finTem = UtilityConstant.METASEXON_COOLINGCOIL_FINTEM;// 最小翅片表面温度
        String cpTem = UtilityConstant.METASEXON_COOLINGCOIL_CPTEM;// 最小铜管壁表面温度
        String circuit = UtilityConstant.METASEXON_COOLINGCOIL_CIRCUIT;// 回路
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String coilFrameMaterialNote = UtilityConstant.METASEXON_COOLINGCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_COOLINGCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_COOLINGCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_COOLINGCOIL_ROWS;// 排数
        String heightLength = UtilityConstant.METASEXON_COOLINGCOIL_HEIGHTLENGTH;// 盘管高度/长度
        String baffleMaterial = UtilityConstant.METASEXON_COOLINGCOIL_BAFFLEMATERIAL;// 挡风板材质
        String headerDia = UtilityConstant.METASEXON_COOLINGCOIL_HEADERDIA;// 进出水管径
        String coilModel = UtilityConstant.METASEXON_COOLINGCOIL_COILMODEL;// 盘管型号描述
        String finDensity = UtilityConstant.METASEXON_COOLINGCOIL_FINDENSITY;// 片距
        String finType = UtilityConstant.METASEXON_COOLINGCOIL_FINTYPE;//翅片材质

        String sconcentration = UtilityConstant.METASEXON_COOLINGCOIL_SCONCENTRATION;//浓度
        String scoolant = UtilityConstant.METASEXON_COOLINGCOIL_SCOOLANT;//介质

        String SreturnEnteringFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNENTERINGFLUIDTEMPERATURE;//进水温度（夏季计算结果）       
        String SreturnWTAScend = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWTASCEND;//水温升（夏季计算结果）
        String SreturnOutFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNOUTFLUIDTEMPERATURE;//出水温度
        String svelocity = UtilityConstant.METASEXON_COOLINGCOIL_SVELOCITY;//迎面风速
        String WreturnOutFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_WRETURNOUTFLUIDTEMPERATURE;//冬季出水温度
        String Wwtascend = UtilityConstant.METASEXON_COOLINGCOIL_WWTASCEND;//水温降冬季
        String WreturnEnteringFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE;//进水温度冬季
        
        // 计算迎面风速
        if (map.containsKey(svelocity)) {
            double airVolume = 0.00;
            double svelocityDB = 0.00;
            String airDirection = BaseDataUtil
                    .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
            if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
            } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
                airVolume = BaseDataUtil
                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
            }
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_COLD.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
        }

        //计算出水温度
        if (map.containsKey(SreturnEnteringFluidTemperature)) {
            double jswd = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(SreturnEnteringFluidTemperature)));
            double sws = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(SreturnWTAScend)));
            map.put(SreturnOutFluidTemperature, String.valueOf(jswd + sws));
        }
        
        //计算冬季出水温度
        if (map.containsKey(WreturnEnteringFluidTemperature)) {
			double jswd = BaseDataUtil
					.stringConversionDouble(String.valueOf(noChangeMap.get(WreturnEnteringFluidTemperature)));
			double swj = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(Wwtascend)));
			map.put(WreturnOutFluidTemperature, String.valueOf(jswd - swj));
		}

        if (map.containsKey(scoolant)) {
            if (UtilityConstant.SYS_STRING_NUMBER_1.equals(noChangeMap.get(scoolant))) {//介质 为水，浓度100%
                map.put(sconcentration, "100%");
            } else {
                map.put(sconcentration, String.valueOf(noChangeMap.get(sconcentration)) + UtilityConstant.SYS_PUNCTUATION_PERCENTAGE);
            }
        }
        String serialStr = UtilityConstant.SYS_BLANK;
        String tubeDiameterValue = UtilityConstant.SYS_BLANK;
        if (map.containsKey(tubeDiameter)) {
            tubeDiameterValue = noChangeMap.get(tubeDiameter);
            if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_1_2);
                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, UtilityConstant.JSON_COOLINGCOIL_COILMODEL_12_7);
            }
            if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_3_8);
                map.put(interval, "25.4*22");
                map.put(tubeDiameter, UtilityConstant.JSON_COOLINGCOIL_COILMODEL_9_52);
            }
        }
        if (map.containsKey(circuit)) {// TODO
            Object value = noChangeMap.get(circuit);
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (BaseDataUtil.stringConversionInteger(unit) > 2333) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_COLD.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_COLD.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = String.valueOf(noChangeMap.get(rows));
                if (valuerows.equals(2) || valuerows.equals(1)) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }
        // 进出水管径
        if (map.containsKey(headerDia)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
            } else {
                valueStr = 1;
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = String.valueOf(noChangeMap.get(rows));
                if (valuerows.equals(2) || valuerows.equals(1)) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(headerDia, sTakeoverSize.getHeadSize());
            } else {
                map.put(headerDia, UtilityConstant.SYS_BLANK);
            }
        }
        //盘管型号
        if (map.containsKey(coilModel)) {
            if (map.containsKey(tubeDiameter)) {
                String value = noChangeMap.get(tubeDiameter);
                String valuerows = UtilityConstant.SYS_BLANK;
                if (map.containsKey(rows)) {
                    valuerows = String.valueOf(noChangeMap.get(rows));
                }
                if (value.equals(UtilityConstant.JSON_COOLINGCOIL_COILMODEL_12_7)) {
                    map.put(coilModel, "12.7mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                } else {
                    map.put(coilModel, "9.52mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                }
            }

        }

        map.put(coilNote, "28CW");
        map.put(finTem, ">32");
        map.put(cpTem, ">32");

        //冷水盘管高度长度
        StringBuffer heightLen = new StringBuffer(UtilityConstant.SYS_BLANK);
        if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
            List<SCoilInfo> coilInfos = AhuMetadata.findList(SCoilInfo.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 31.75));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
            List<SCoilInfo1> coilInfos = AhuMetadata.findList(SCoilInfo1.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 25.4));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (heightLen.toString().length() > 0)
            map.put(heightLength, heightLen.toString().substring(0, heightLen.toString().length() - 1));

        if (map.containsKey(finType)) {
            String ft = String.valueOf(noChangeMap.get(finType));
            String fintypev = UtilityConstant.SYS_BLANK;
            if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_AL.equals(ft)) {
                fintypev = "普通铝翅片";
            } else if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_PROCOATEDAL.equals(ft)) {
                fintypev = "亲水铝翅片";
            } else if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_COPPER.equals(ft)) {
                fintypev = "铜翅片";
            }
            map.put(finType, fintypev);
        }

        return map;
    }

    // 追加综合过滤段奥雅纳工程格式报告，（袋式/板式）初、终阻力求和
    public static Map<String, String> appendCombinedFilter(Map<String, String> noChangeMap, Map<String, String> map) {
        String initialPD = UtilityConstant.METASEXON_COMBINEDFILTER_INITIALPD;// 初阻力
        map.put(initialPD, UtilityConstant.SYS_BLANK);
        String finalPD = UtilityConstant.METASEXON_COMBINEDFILTER_FINALPD;// 终阻力
        map.put(finalPD, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装综合过滤段报告数据
    public static Map<String, String> getCombinedFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendCombinedFilter(noChangeMap, map);
        map = getAirVolume(noChangeMap, map);// 计算风量
        String fitetF = UtilityConstant.METASEXON_COMBINEDFILTER_FITETF;// 过滤形式
        String rimThickness = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESS;// 边框厚度
        String rimThicknessP = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESSP;// 边框厚度
        String rimThicknessB = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESSB;// 边框厚度
        String filterArrange = UtilityConstant.METASEXON_COMBINEDFILTER_FILTERARRANGE;// 供应商
        String supplier = UtilityConstant.METASEXON_COMBINEDFILTER_SUPPLIER;// 过滤器布置
        /* 板式规格信息 */
        String paraP1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP1;
        String paraPN1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN1;
        String paraP2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP2;
        String paraPN2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN2;
        String paraP3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP3;
        String paraPN3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN3;
        String paraP4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP4;
        String paraPN4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN4;
        /* 袋式规格信息 */
        String paraB1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB1;
        String paraBN1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN1;
        String paraB2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB2;
        String paraBN2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN2;
        String paraB3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB3;
        String paraBN3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN3;
        String paraB4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB4;
        String paraBN4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN4;

        String initialPD = UtilityConstant.METASEXON_COMBINEDFILTER_INITIALPD;// 初阻力
        String finalPD = UtilityConstant.METASEXON_COMBINEDFILTER_FINALPD;// 终阻力


        map.put(fitetF, getIntlString(PANEL_TYPE_BAG_TYPE));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(rimThickness)) {
            String value = noChangeMap.get(rimThickness).toString();
            map.put(rimThicknessP, value.substring(0, 2));
            map.put(rimThicknessB, value.substring(value.length() - 2, value.length()));
        }
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                int i = 1;
                for (FilterPanelBean filterPanelBean : filterPanelBeanList) {
                    if (i == 1) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP1, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN1, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB1, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN1, filterPanelBean.getLCount());
                        }
                    } else if (i == 2) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP2, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN2, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB2, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN2, filterPanelBean.getLCount());
                        }
                    } else if (i == 3) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP3, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN3, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB3, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN3, filterPanelBean.getLCount());
                        }
                    } else if (i == 4) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP4, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN4, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB4, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN4, filterPanelBean.getLCount());
                        }
                    }
                    i++;
                }
            }
        }

        //（袋式/板式）初、终阻力求和
        double initialPDB = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(initialPD + "B")));//袋式初阻力
        double initialPDP = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(initialPD + "P")));//板式初阻力

        double finalPDB = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(finalPD + "B")));//袋式终阻力
        double finalPDP = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(finalPD + "P")));//板式终阻力

        map.put(initialPD, UtilityConstant.SYS_BLANK + (initialPDB + initialPDP));
        map.put(finalPD, UtilityConstant.SYS_BLANK + (finalPDB + finalPDP));

        //袋式压差计
        Object pressureGuageB = noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB);
        String valuePGB = BaseDataUtil.constraintString(pressureGuageB);
        //板式压差计
        Object pressureGuageP = noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP);
        String valuePGP = BaseDataUtil.constraintString(pressureGuageP);
        String attachment = "";
        String pressureguage = "";
        String pressureguage1 = "";
        if (EmptyUtil.isNotEmpty(pressureGuageP)) {
            if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDS.equals(valuePGP)) {
                pressureguage = "压差开关";
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDPG.equals(valuePGP)) {
                pressureguage = "指针式压差计";
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO.equals(valuePGP)) {
                pressureguage = UtilityConstant.SYS_BLANK;
            }
            attachment += pressureguage + (UtilityConstant.SYS_BLANK.equals(pressureguage) ? UtilityConstant.SYS_BLANK
                    : (CommonConstant.SYS_PUNCTUATION_BRACKET_OPEN
                    + I18NBundle.getString("moon_intl_str_1919", language)
                    + CommonConstant.SYS_PUNCTUATION_BRACKET_CLOSE));
        }
        if (EmptyUtil.isNotEmpty(pressureGuageB)) {
            if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDS.equals(valuePGB)) {
                pressureguage1 = "压差开关";
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDPG.equals(valuePGB)) {
                pressureguage1 = "指针式压差计";
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO.equals(valuePGB)) {
                pressureguage1 = UtilityConstant.SYS_BLANK;
            }
            attachment += pressureguage1
                    + (UtilityConstant.SYS_BLANK.equals(pressureguage1) ? UtilityConstant.SYS_BLANK
                    : (CommonConstant.SYS_PUNCTUATION_BRACKET_OPEN + I18NBundle.getString("bag_type", language)
                    + CommonConstant.SYS_PUNCTUATION_BRACKET_CLOSE));
        }
        map.put(UtilityConstant.METASEXON_COMBINEDFILTER_APPENDIX, attachment);

        return map;
    }

    /**
     * 封装奥雅纳湿膜加湿报告数据
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFWetFilmHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WHUMIDIFICATIONQ));
        }
        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(WET_FILM_HUMIDIFICATION_SECTION));//湿膜加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳高压喷雾加湿段
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFSprayHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WHUMIDIFICATIONQ));
        }
        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(HIGH_PRESSURE_SPRAY));//高压喷雾加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳干蒸加湿
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFSteamHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_HUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_SHUMIDIFICATIONQ));
        } else {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_WHUMIDIFICATIONQ));
        }

        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(DRY_STEAM_HUMIDIFICATION_SECTION));//干蒸加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳电极加湿
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFElectrodeHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ));
        }

        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(ELECTRODE_HUMIDIFICATION_SECTION));//干蒸加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    // 封装混合段报告数据
    public static Map<String, String> getMix(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(noChangeMap, map);// 计算风量
        // 封装混合形式
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;
        String fixRepairLamp = UtilityConstant.METASEXON_MIX_FIXREPAIRLAMP;
        String uvLamp = UtilityConstant.METASEXON_MIX_UVLAMP;
        String style = UtilityConstant.METASEXON_MIX_STYLE;
        String doorO = UtilityConstant.METASEXON_MIX_DOORO;// 开门情况 有检修灯的时候
        int i = 0;
        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(BACK_RETURN_AIR));
                i++;
            } else {
                map.remove(returnBack);
            }
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(DOWN_RETURN_AIR));
                i++;
            } else {
                map.remove(returnButtom);
            }
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(LEFT_RETURN_AIR));
                i++;
            } else {
                map.remove(returnLeft);
            }
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(RIGHT_RETURN_AIR));
                i++;
            } else {
                map.remove(returnRight);
            }
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(UPPER_RETURN_AIR));
                i++;
            } else {
                map.remove(returnTop);
            }
        }
        if (i >= 2) {
            map.put(style, getIntlString(MIX_RETURN_AIR));
        }

        if (map.containsKey(fixRepairLamp)) {// 是否安装检修灯
            String value = BaseDataUtil.constraintString(noChangeMap.get(fixRepairLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(fixRepairLamp, getIntlString(INSTALL_FIX_REPAIR_LAMP));
            } else {
                map.put(fixRepairLamp, UtilityConstant.SYS_BLANK);
            }
        }

        // 紫外线杀菌灯
        if (map.containsKey(uvLamp)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(uvLamp, getIntlString(UV_LIGHT_CUVCA).concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN).concat(noChangeMap.get(UtilityConstant.METASEXON_MIX_UVLAMPSERIAL).concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE)));
                i++;
            }
        }
        return map;
    }

    public static Map<String, String> getAirVolume(Map<String, String> noChangeMap, Map<String, String> map) {
        String airVolume = UtilityConstant.METAHU_AIRVOLUME;// 报表中通用风量标识
        String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向标识
        String eairvolume = UtilityConstant.METAHU_EAIRVOLUME;
        String sairvolume = UtilityConstant.METAHU_SAIRVOLUME;
        String velocity = UtilityConstant.METAHU_VELOCITY;
        String svelocity = UtilityConstant.METAHU_SVELOCITY;
        String evelocity = UtilityConstant.METAHU_EVELOCITY;
        if (map.containsKey(airDirection)) {
            String airDirectionStr = noChangeMap.get(airDirection).toString();
            if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {
                Object value = noChangeMap.get(sairvolume);
                map.put(airVolume, value.toString());
                if (map.containsKey(svelocity)) {
                    map.put(velocity, noChangeMap.get(svelocity));
                }
            } else {
                Object value = noChangeMap.get(sairvolume);
                map.put(airVolume, value.toString());
                if (map.containsKey(evelocity)) {
                    map.put(velocity, noChangeMap.get(evelocity));
                }
            }
        }
        return map;
    }

    private static int getSeriesByFanModel(String fanModel) {
        if (fanModel.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_SYW)) {
            return BaseDataUtil.stringConversionInteger(NumberUtil.onlyNumbers(fanModel));
        }
        return BaseDataUtil.stringConversionInteger(NumberUtil.onlyNumbers(fanModel));
    }

    public static Map<String, String> getReportMap(ReportData reportData, SectionTypeEnum sectionType) {
        Map<String, String> rstMap = new HashMap<String, String>();

        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = new ArrayList<>();
            for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
                if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
                    partList.add(e.getValue());
                }
            }
            Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
            for (PartPO partPO : partList) {
                Part part = partPO.getCurrentPart();
                String key = part.getSectionKey();
                if (sectionType.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
                    rstMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                }
            }

        }

        return rstMap;
    }

}
