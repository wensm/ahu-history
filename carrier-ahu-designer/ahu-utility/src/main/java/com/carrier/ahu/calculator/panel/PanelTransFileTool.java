package com.carrier.ahu.calculator.panel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.calculator.ExcelForPanelUtils;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelFace;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionGeneratorUtil;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PanelTransFileTool {
	// private static final String PATH_OPERATE =
	// "C:\\Users\\jaaka\\Desktop\\39CQOperate" + File.separator;
	private static final String FILE_OPERATE = "{0}.json";

	/** 冷水+湿膜加湿段-016 */
	private static final String CODE_16 = "016";
	/** 热水+湿膜加湿段-017 */
	private static final String CODE_17 = "017";
	/** 冷水+高压喷雾加湿段-018 */
	private static final String CODE_18 = "018";
	/** 热水+高压喷雾加湿段-019 */
	private static final String CODE_19 = "019";
	/** 冷水+热水-021 */
	private static final String CODE_21 = "021";

	private static Map<String, List<PanelJsonPO>> JsonResultMap = new HashMap<>();

	public static void genFile() throws IOException {
		Map<String, PanelXSLXPO> BigOperateMap = ExcelForPanelUtils.getInstance().getBigOperateMap();

		for (Entry<String, PanelXSLXPO> entry : BigOperateMap.entrySet()) {
			String poKey = entry.getKey();
			String[] poKeySplit = poKey.split(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN);

			if (poKeySplit.length != 3) {
				log.warn("Excel中取得数据格式异常");
			} else {
				String mapKey = poKeySplit[0];
				String sectionTypeNo = poKeySplit[1];
				String sectionLStr = poKeySplit[2];
				String sectionTypeId = null;
				if (sectionTypeNo.length() < 3) {
					sectionTypeNo = (UtilityConstant.SYS_STRING_NUMBER_0 + UtilityConstant.SYS_STRING_NUMBER_0
							+ UtilityConstant.SYS_STRING_NUMBER_0) + sectionTypeNo;
					sectionTypeNo = sectionTypeNo.substring(sectionTypeNo.length() - 3);
				}
				SectionTypeEnum section = SectionTypeEnum.getSectionTypeByCodeNum(sectionTypeNo);
				if (null == section) {
					if (CODE_16.equals(sectionTypeNo)) {
						sectionTypeId = AhuPartition.CODE_016;
					} else if (CODE_17.equals(sectionTypeNo)) {
						sectionTypeId = AhuPartition.CODE_017;
					} else if (CODE_18.equals(sectionTypeNo)) {
						sectionTypeId = AhuPartition.CODE_018;
					} else if (CODE_19.equals(sectionTypeNo)) {
						sectionTypeId = AhuPartition.CODE_019;
					} else if (CODE_21.equals(sectionTypeNo)) {
						sectionTypeId = AhuPartition.CODE_021;
					} else {
						log.warn("根据编号未找到段信息，编号：" + sectionTypeNo);
						continue;
					}
				} else {
					sectionTypeId = section.getId();
				}
				PanelXSLXPO po = entry.getValue();
				PanelFace panelFace = newOperatePanelFace(po);
				AhuPartitionGenerator.genPanelFaceFromPO4JsonTool(panelFace, po);
				addToMap(mapKey,
						new PanelJsonPO(sectionTypeId, sectionTypeNo, Integer.parseInt(sectionLStr), panelFace));
			}
		}

		for (Entry<String, List<PanelJsonPO>> entry : JsonResultMap.entrySet()) {
			output2File(MessageFormat.format(FILE_OPERATE, entry.getKey()), JSONObject.toJSONString(entry.getValue()));
		}
	}

	private static void addToMap(String key, PanelJsonPO po) {
		if (JsonResultMap.containsKey(key)) {
			List<PanelJsonPO> poList = JsonResultMap.get(key);
			poList.add(po);
			JsonResultMap.put(key, poList);
		} else {
			List<PanelJsonPO> poList = new ArrayList<>();
			poList.add(po);
			JsonResultMap.put(key, poList);
		}
	}

	private static PanelFace newOperatePanelFace(PanelXSLXPO po) {
		PanelFace panelFace = AhuPartitionGeneratorUtil.createPanelFace(Integer.parseInt(po.getW()), Integer.parseInt(po.getH()));
		panelFace.setId(UtilityConstant.SYS_PANEL_LEFT);
		return panelFace;
	}

	private static void output2File(String fileName, String codeJson) throws IOException {
		String filePath = FileNamesLoadInSystem.PANEL_INIT_PATH + fileName;
		// String filePath = "C:\\Users\\jaaka\\Desktop\\39CQOperate\\" + fileName;
		File file = new File(filePath);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream o = new FileOutputStream(file);
		o.write(codeJson.getBytes(Charset.forName(UtilityConstant.SYS_ENCODING_UTF8_UP)));
		o.close();

	}
}
