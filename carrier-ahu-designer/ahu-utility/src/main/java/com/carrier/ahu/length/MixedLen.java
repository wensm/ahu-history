package com.carrier.ahu.length;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.mix.MixSize;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.unit.BaseDataUtil;

/**
 * Created by liangd4 on 2017/9/25.
 * 混合段
 */
public class MixedLen {

    /* 计算段长*/
    //unitModel 机组型号
    //mixType 混合形式 1："顶部出风" 2："后出风" 3："左侧出风" 3："右侧出风" 1："底部出风" 任意组合
    //isReturnBack
    //airVolume 系统风量
    public Integer getCalLength(String unitModel, String mixType, boolean uv, int airVolume) {
        //数据查询
        int minsectinl = -1;
        int minsectinl1 = -1;
        int height = SystemCountUtil.getUnitHeight(unitModel);
        int width = SystemCountUtil.getUnitWidth(unitModel);
        SectionLength length = AhuMetadata.findOne(SectionLength.class, unitModel);
        Integer mixA = length.getA();//表段长
        Integer mixAMIN = length.getAMIN();//表最小段长
        if (uv) {//如果选择UV灯，则最小段长为6M
            minsectinl = 6;
        }
        char[] ar = mixType.toCharArray();
        for (char c : ar) {
            mixType = String.valueOf(c);
            if (mixType.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {//后出风
            	if(mixAMIN!=null) {
                minsectinl = mixAMIN;
            	}
            }
            if (mixType.equals("1")) {//顶部出风 底部出风
                double fc = -1;
                if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
                    fc = (width - 1) * 100 + 128 - 72;
                } else if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {
                    fc = (width - 1) * 100 + 488 - 72;
                } else if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
                    fc = (width - 1) * 100 + 76 - 72;
                }
                double fa = airVolume / 3600 / (fc / 1000) / 8 * 1000;
                MixSize mixSize = getMinimumSuitableMixSize(BaseDataUtil.doubleConversionInteger(fa), unitModel, mixType);
                minsectinl1 = mixSize.getSectionL();
            }
            if (minsectinl1 > minsectinl) {
                minsectinl = minsectinl1;
            }
            if (mixType.equals(UtilityConstant.SYS_STRING_NUMBER_3)) {//左侧出风 右侧出风
                double fc = -1;
                if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
                    fc = (height - 1) * 100 + 128 - 72;
                } else if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {
                    fc = (height - 1) * 100 + 488 - 72;
                } else if (unitModel.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
                    fc = (height - 1) * 100 + 76 - 72;
                }
                double fa = airVolume / 3600 / (fc / 1000) / 8 * 1000;
                MixSize mixSize = getMinimumSuitableMixSize(BaseDataUtil.doubleConversionInteger(fa), unitModel, mixType);
                minsectinl1 = mixSize.getSectionL();
            }
            if (minsectinl1 > minsectinl) {
                minsectinl = minsectinl1;
            }
        }
        if (minsectinl > mixA) {
            minsectinl = mixA;
        }
        
        //机组类型校验:型号在0608-0711和0811-0914时混合段最小段长都为5M，型号大于1015时混合段最小断肠都为6M
		int serialV = height * 100 + width;
		if ((serialV >= 608 && serialV <= 0711) || (serialV >= 811 && serialV <= 914)) {// 0608-0711 和 0811-0914
			if (minsectinl > 5) {
				minsectinl = 5;
			}
		} else if (serialV > 1015) {
			if (minsectinl > 6) {
				minsectinl = 6;
			}
		}
		
		if (uv && minsectinl==5) {//如果选择UV灯，则最小段长为6M
            minsectinl = 7;
        }
        
        return minsectinl;
    }
    /**
     * 计算段长
     */
    public Integer getLength(String unitModel, String mixType, boolean uv, int airVolume) {
        //数据查询
        int height = SystemCountUtil.getUnitHeight(unitModel);
        int width = SystemCountUtil.getUnitWidth(unitModel);
        SectionLength length = AhuMetadata.findOne(SectionLength.class, unitModel);
        Integer mixA = length.getA();//推荐表段长
        
        if(uv && mixA==5) {
        	return 7;
        }
        return mixA;
    }
    public MixSize getMinimumSuitableMixSize(int A, String unitModel, String mixType) {
        String unitSeries = AhuUtil.getUnitSeries(unitModel);
        List<MixSize> mixSizeList = AhuMetadata.findList(MixSize.class, unitSeries, mixType);
        Collections.sort(mixSizeList, new Comparator<MixSize>() {
            public int compare(MixSize o1, MixSize o2) {
                return new Integer(o1.getA()).compareTo(new Integer(o2.getA()));
            }
        });
        for (MixSize mixSize : mixSizeList) {
            if (mixSize.getA() > A) {
                return mixSize;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        double f1 = new BigDecimal((float) 10 / 9).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println(f1);
        System.out.println(90.0 / 100);
    }
}
