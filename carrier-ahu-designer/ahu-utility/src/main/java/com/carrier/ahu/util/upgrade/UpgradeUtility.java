package com.carrier.ahu.util.upgrade;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.NumberFormat;

import com.carrier.ahu.common.upgrade.MessageType;
import com.carrier.ahu.common.upgrade.PacketTransfer;
import com.carrier.ahu.common.upgrade.UpdateInfo;
import com.carrier.ahu.common.upgrade.UpgradeConfig;
import com.carrier.ahu.common.upgrade.UpgradeMessage;
import com.carrier.ahu.common.upgrade.UpgradePacket;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SysConstants;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Created by Braden Zhou on 2018/07/18.
 */
@Slf4j
public class UpgradeUtility {

    public interface UpgradeListener {
        public void onProgress(String progress);

        public void onError();

        public void onFinish();

    }

    public static UpdateInfo checkUpdate(String languageCode) {
        DatagramSocket socket = null;
        UpdateInfo updateInfo = null;

        try {
            socket = new DatagramSocket();
            socket.setBroadcast(true);

            // check update on upgrade server
            String checkUpdateString = MessageType.CHECK_UPDATE.name();
            LicenseInfo licenseInfo = LicenseManager.getLicenseInfo();
            if (licenseInfo != null) {
                licenseInfo.setLanguageCode(languageCode);
                checkUpdateString += new Gson().toJson(licenseInfo);
            }

            byte[] sendData = checkUpdateString.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
                    InetAddress.getByName(UtilityConstant.SYS_UPGRADE_BROADCAST_ADDRESS), UtilityConstant.SYS_UPGRADE_UPGRADE_DISCOVERY_PORT);
            socket.send(sendPacket);

            // receive server info
            byte[] buffer = new byte[1024];
            DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
            socket.setSoTimeout(UtilityConstant.SYS_UPGRADE_RECEIVE_TIMEOUT); // 30s time out
            socket.receive(receivePacket);
            String message = new String(receivePacket.getData()).trim();
            if (message.startsWith(MessageType.CHECK_UPDATE.name())) { // get update info
                String updateVersion = message.substring(MessageType.CHECK_UPDATE.name().length());
                if (EmptyUtil.isNotEmpty(updateVersion)) {
                    updateInfo = new UpdateInfo();
                    updateInfo.setIpAddress(receivePacket.getAddress().getHostAddress());
                    updateInfo.setUpdateVersion(updateVersion);
                }
            }
        } catch (IOException ex) {
            log.error("Failed to find upgrade server");
        } finally {
            socket.close();
        }
        return updateInfo;
    }

    public static void downloadUpgrade(String release, String factory, String languageCode, UpgradeListener listener)
            throws UnknownHostException, IOException {
        UpdateInfo updateInfo = checkUpdate(languageCode);
        if (updateInfo != null) {
            updateInfo.setFactory(factory);
            updateInfo.setLanguageCode(languageCode);

            Socket socket = new Socket(InetAddress.getByName(updateInfo.getIpAddress()), UtilityConstant.SYS_UPGRADE_UPGRADE_PORT);
            socket.setSoTimeout(UtilityConstant.SYS_UPGRADE_RECEIVE_TIMEOUT);
            DownloadUpgrade upgrade = new DownloadUpgrade(socket);
            upgrade.setUpdateInfo(updateInfo);
            upgrade.setRelease(release);
            upgrade.setUpgradeListener(listener);
            new Thread(upgrade).start();
        } else if (listener != null) {
            listener.onError();
        }
    }

    public static class DownloadUpgrade implements Runnable {

        private Socket socket;
        private UpgradeListener listener;
        private UpdateInfo updateInfo;
        private String release;
        private long updateSize;

        public DownloadUpgrade(Socket socket) {
            this.socket = socket;
        }

        public void setUpdateInfo(UpdateInfo updateInfo) {
            this.updateInfo = updateInfo;
        }

        public void setRelease(String release) {
            this.release = release;
        }

        public void setUpgradeListener(UpgradeListener listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            try {
                while (!socket.isClosed()) {
                    this.handleMessage();
                }
            } catch (Exception e) {
                log.error("Failed to download upgrade file", e);
                notifyError();
            } finally {
                try {
                    if (!socket.isClosed()) {
                        socket.close();
                    }
                } catch (IOException e) {
                    log.error("Failed to close socket", e);
                }
            }
        }

        public void handleMessage() throws IOException {
            UpgradePacket packet = PacketTransfer.receivePacket(socket);
            if (packet != null) {
                Gson gson = new Gson();
                UpgradeMessage message = gson.fromJson(packet.getBody(), UpgradeMessage.class);
                MessageType messageType = MessageType.valueOf(message.getType());
                log.debug("Receive message: " + messageType.name() + ", " + message.getContent());
                switch (messageType) {
                case LOGIN:
                    // send license info to upgrade server
                    LicenseInfo licenseInfo = LicenseManager.getLicenseInfo();
                    if (licenseInfo != null) {
                        sendMessage(MessageType.LOGIN, new Gson().toJson(licenseInfo));
                    } else {
                        sendMessage(MessageType.LOGIN, "null");
                    }
                    break;
                case RELEASE:
                    sendMessage(MessageType.RELEASE, release);
                    break;
                case DOWNOADING:
                    this.updateSize = Long.valueOf(message.getContent());
                    sendMessage(MessageType.READY, null);
                    this.handleDownload();
                    break;
                case DONE:
                    sendMessage(MessageType.CONFIRM, null);
                    socket.close();
                    break;
                case ERROR:
                    notifyError();
                    socket.close();
                    break;
                default:
                }
            }
        }

        private void sendMessage(MessageType messageType, String content) throws IOException {
            UpgradeMessage message = new UpgradeMessage();
            message.setType(messageType.name());
            message.setContent(content);

            String messageString = new Gson().toJson(message);
            UpgradePacket packet = new UpgradePacket(messageString);
            log.debug("Send message: " + messageType.name());
            PacketTransfer.sendPacket(socket, packet);
        }

        /**
         * 接收socket 返回的升级文件
         * @throws IOException
         */
        private void handleDownload() throws IOException {
            File updateFile = new File(SysConstants.ASSERT_DIR + UpgradeConfig.getUpdateFileName(release, updateInfo.getFactory(),
                    updateInfo.getLanguageCode(), updateInfo.getUpdateVersion()));
            if (!updateFile.exists()) {
                updateFile.createNewFile();
            }
            log.info("Start to download upgrade file: " + updateFile.getAbsolutePath());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(updateFile));

            long transferedSize = 0;
            String preProgress = "";
            int length = 0;
            byte[] buffer = new byte[1024];
            InputStream is = socket.getInputStream();
            while ((length = is.read(buffer)) != -1) {
                transferedSize += length;
                bos.write(buffer, 0, length);
                if (listener != null) {
                    NumberFormat numberFormat = NumberFormat.getInstance();
                    numberFormat.setMaximumFractionDigits(0);
                    String nextProgress = numberFormat.format((float) transferedSize / (float) updateSize * 100);
                    if (!nextProgress.equals(preProgress)) {
                        notifyProgress(preProgress);
                        preProgress = nextProgress;
                    }
                    if (updateSize == transferedSize) {
                        notifyFinish();
                    }
                }
                if (updateSize == transferedSize) {
                    log.info("Finish download upgrade file");
                    break;
                } else if (updateSize - transferedSize < 1024) {
                    buffer = new byte[(int) (updateSize - transferedSize)];
                }
            }
            bos.flush();
            bos.close();
        }

        private void notifyError() {
            if (listener != null) {
                try {
                    listener.onError();
                } catch (Exception exp) {
                    log.error("Listener error", exp);
                }
            }
        }

        private void notifyProgress(String progress) {
            try {
                listener.onProgress(progress);
            } catch (Exception exp) {
                log.error("Listener error", exp);
            }
        }

        private void notifyFinish() {
            try {
                if (listener != null) {
                    listener.onFinish();
                }
            } catch (Exception exp) {
                log.error("Listener error", exp);
            }
        }

    }

}
