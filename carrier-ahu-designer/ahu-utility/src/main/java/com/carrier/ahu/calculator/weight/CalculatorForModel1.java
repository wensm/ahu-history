package com.carrier.ahu.calculator.weight;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.carrier.ahu.ResourceConstances;
import com.carrier.ahu.calculator.ExcelForPriceUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;

public abstract class CalculatorForModel1 extends Calculator {

	private Map<String, WeightModel1> priceMap;

	public CalculatorForModel1(String name) {
		super(name);
	}

	public double weight(AhuParam ahu, PartParam part) {
		if (EmptyUtil.isEmpty(priceMap)) {
			loadPackageExcel();
		}
		String tabKey = getTabKey(ahu, part);
		String columnKey = getColumnKey(ahu, part);
		if (priceMap.containsKey(tabKey)) {
			WeightModel1 model = priceMap.get(tabKey);
			int colIndex = getColIndex(model, columnKey);
			if (colIndex == -1) {
				return UNKNOWN_VALUE;
			}
			String rowKey = getRowKey(ahu, part);
			if (model.getRowMap().containsKey(rowKey)) {
				return model.getRowMap().get(rowKey).get(colIndex);
			}
		}
		return UNKNOWN_VALUE;
	}

	/**
	 * 基本实现，子类可以扩展该方法，找到对应的列
	 * 
	 * @param model
	 * @param colKey
	 * @return
	 */
	protected int getColIndex(WeightModel1 model, String colKey) {
		return model.getKeyList().indexOf(colKey);
	}

	protected String getTabKey(AhuParam ahu, PartParam part) {
		return UtilityConstant.SYS_BLANK;
	}

	protected String getColumnKey(AhuParam ahu, PartParam part) {
		return UtilityConstant.SYS_BLANK;
	}

	protected String getRowKey(AhuParam ahu, PartParam part) {
		return UtilityConstant.SYS_BLANK;
	}

	abstract protected String getResourcePath();

	private Map<String, WeightModel1> loadPackageExcel() {
		InputStream is = null;
		try {
			is = ExcelForPriceUtils.class.getClassLoader().getResourceAsStream(getResourcePath());

			/* 验证文件是否合法 */
			// if (!ExcelUtils.validateExcel(DATA_PATH)) {
			// return null;
			// }
			/* 判断文件的类型，是2003还是2007 */
			boolean isExcel2003 = true;
			if (ExcelUtils.isExcel2007(UtilityConstant.SYS_PATH_W_AHU_PACKAGE)) {
				isExcel2003 = false;
			}

			/* 根据版本选择创建Workbook的方式 */
			Workbook wb = null;
			if (isExcel2003) {
				wb = ExcelUtils.openExcelByPOIFSFileSystem(is);
			} else {
				wb = ExcelUtils.openExcelByFactory(is);
			}

			priceMap = read2map(wb);

			// priceModel.setPriceMap(priceMap);
			is.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (EmptyUtil.isNotEmpty(is )) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		/* 返回最后读取的结果 */
		return priceMap;

	}

	/**
	 * 读取数据
	 *
	 * @param wb
	 * @param priceModel
	 * @return
	 */
	private Map<String, WeightModel1> read2map(Workbook wb) {
		Map<String, WeightModel1> priceMap = new HashMap<String, WeightModel1>();

		int sheetCount = wb.getNumberOfSheets();
		for (int i = 0; i < sheetCount; i++) {
			/* 得到第一个shell */
			Sheet sheet = wb.getSheetAt(i);
			Row row0 = sheet.getRow(0);
			/* 得到Excel的行数 */
			int totalRows = sheet.getPhysicalNumberOfRows();
			/* 得到Excel的列数 */
			int totalCells = 0;
			if (totalRows >= 1 && null != row0) {
				totalCells = row0.getPhysicalNumberOfCells();
			}
			/* 获取第一列的属性Key列表 */
			String sheetName = sheet.getSheetName();
			List<String> keyList = new ArrayList<>();
			for (int c = 1; c < totalCells; c++) {
				// CellType ctype = row0.getCell(c).getCellTypeEnum();

				String value = getStringValue(row0.getCell(c));
				if (StringUtils.isEmpty(value)) {
					break;
				} else {
					keyList.add(value.trim());
				}
			}
			Map<String, List<Double>> map = new HashMap<>();
			/* 循环Excel的行 */
			for (int r = 1; r < totalRows; r++) {

				Row row = sheet.getRow(r);
				if (EmptyUtil.isEmpty(row)) {
					continue;
				}
				String priceMapKey = row.getCell(0).getStringCellValue();
				priceMapKey = ExcelForPriceUtils.fixPriceKey(priceMapKey);
				List<Double> valueList = new ArrayList<>();
				/* 循环Excel的列 */
				for (int c = 1; c < totalCells; c++) {
					Cell cell = row.getCell(c);
					double cellValue = 0;
					if (EmptyUtil.isNotEmpty(cell)) {
						cellValue = cell.getNumericCellValue();
					}
					valueList.add(cellValue);
				}
				map.put(priceMapKey, valueList);
			}
			WeightModel1 model = new WeightModel1();
			model.setKeyList(keyList);
			model.setRowMap(map);
			model.setName(sheetName);
			priceMap.put(model.getName(), model);
		}
		return priceMap;
	}

	private String getStringValue(Cell cell) {
		CellType ctype = cell.getCellTypeEnum();
		String value = UtilityConstant.SYS_BLANK;
		if (CellType.NUMERIC == ctype) {
			value = String.valueOf(cell.getNumericCellValue());
		} else {
			value = cell.getStringCellValue();
		}
		return value;
	}

}
