package com.carrier.ahu.util.template;

import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class BlankTemplate extends TemplateFactory {

    public BlankTemplate(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.COMMON;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] {};
    }

}
