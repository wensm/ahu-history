package com.carrier.ahu.util.template;

import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_BACK;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_BOTTOM;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_LEFT;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_RIGHT;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_TOP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.AhuTemplate;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public abstract class TemplateFactory {

    private GroupTypeEnum groupType;

    public TemplateFactory(GroupTypeEnum groupType) {
        this.groupType = groupType;
    }

    public static TemplateFactory getTemplateFactory(GroupTypeEnum groupType) {
        switch (groupType) {
        case TYPE_BASIC:
            return new BasicTemplate(groupType);
        case TYPE_BASIC_2:
            return new Basic2Template(groupType);
        case TYPE_DOUBLE_PIPE:
            return new DoublePipeTemplate(groupType);
        case TYPE_FOUR_PIPE:
            return new FourPipeTemplate(groupType);
        case TYPE_FULL_YEAR:
            return new FullYearTemplate(groupType);
        case TYPE_RUNNER:
            return new RunnerTemplate(groupType);
        case TYPE_PLATE:
            return new PlateTemplate(groupType);
        case TYPE_VERTICAL_1:
            return new VerticalUnit1Template(groupType);
        case TYPE_VERTICAL_2:
            return new VerticalUnit2Template(groupType);
        case TYPE_DOUBLE_RETURN_1:
            return new DoubleReturn1Template(groupType);
        case TYPE_DOUBLE_RETURN_2:
            return new DoubleReturn2Template(groupType);
        case TYPE_SIDE_BY_SIDE_RETURN:
            return new SideBySideReturnTemplate(groupType);
        case TYPE_BLANK:
        case TYPE_UNGROUP:
        case TYPE_CUSTOM:
            return new BlankTemplate(groupType);
        }
        return null;
    }

    public AhuTemplate createTemplate() {
        AhuTemplate template = new AhuTemplate();
        template.setId(groupType.getId());
        template.setCnName(groupType.getCnName());
        template.setTemplateName(groupType.getTemplateName());
        template.setAhuContent(generateUnitJson());
        template.setParts(preConfigureSections(generateSectionContent()));
        template.setSectionContent(JSONArray.toJSONString(template.getParts()).toString());
        return template;
    }

    protected String generateUnitJson() {
        Unit unit = new Unit();
        Map<String, Object> unitMetaJson = new HashMap<String, Object>();
        unitMetaJson.putAll(TemplateUtil.genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU));
        unitMetaJson.remove(UtilityConstant.METAHU_PRODUCT); // 行业类型模板去除机组系列属性，否则确认模板后，机组系列默认为39CQ
        unit.setMetaJson(JSON.toJSONString(unitMetaJson));
        unit.setLayoutJson(
                AhuLayoutUtils.getInitAhuLayoutJsonString(getSectionLayout(), getPreDefinedSectionTypes().length));
        return JSON.toJSONString(unit).toString();
    }

    private List<Part> generateSectionContent() {
        SectionTypeEnum[] sectionTypes = getPreDefinedSectionTypes();
        List<Part> parts = new ArrayList<>();
        for (int i = 0; i < sectionTypes.length; i++) {
            SectionTypeEnum sectionTypeEnum = sectionTypes[i];
            Part part = new Part();
            part.setSectionKey(sectionTypeEnum.getId());
            part.setPosition((short) (i + 1));
            part.setMetaJson(JSON.toJSONString(TemplateUtil.genAhuSectionTemplate(sectionTypeEnum)));
            parts.add(part);
        }
        return parts;
    }

    /**
     * Pre-configure meta for each section.
     * 
     * @param parts
     * @return
     */
    protected List<Part> preConfigureSections(List<Part> parts) {
        return parts;
    }

    public abstract LayoutStyleEnum getSectionLayout();

    public abstract SectionTypeEnum[] getPreDefinedSectionTypes();

    /**
     * Set air return for mix section.
     * 
     * @param partMeta
     * @param metaKeys
     */
    protected void setMixAirReturn(Map<String, Object> partMeta, String... metaKeys) {
        setMixAirReturn(KEY_RETURN_BACK, partMeta, metaKeys);
        setMixAirReturn(KEY_RETURN_BOTTOM, partMeta, metaKeys);
        setMixAirReturn(KEY_RETURN_LEFT, partMeta, metaKeys);
        setMixAirReturn(KEY_RETURN_RIGHT, partMeta, metaKeys);
        setMixAirReturn(KEY_RETURN_TOP, partMeta, metaKeys);
    }

    protected void setMixAirReturn(String metaKey, Map<String, Object> partMeta, String... metaKeys) {
        String sectionMetaKey = SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, metaKey);
        if (Arrays.asList(metaKeys).contains(metaKey)) {
            partMeta.put(sectionMetaKey, true);
        } else {
            partMeta.put(sectionMetaKey, false);
        }
    }

}
