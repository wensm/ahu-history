package com.carrier.ahu.util.meta;

import static com.carrier.ahu.metadata.AhuMetadata.startsWith;

import java.util.List;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Utility class for Ahu metadata.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
public class AhuMetaUtils implements SystemCalculateConstants {

    public static List<AhuSizeDetail> getAhuSizeDetails(String unitSeries) {
        return AhuMetadata.findList(AhuSizeDetail.class, startsWith(unitSeries));
    }

}
