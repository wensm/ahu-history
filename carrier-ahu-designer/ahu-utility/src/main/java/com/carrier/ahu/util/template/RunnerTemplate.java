package com.carrier.ahu.util.template;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_OUTLETDIRECTION;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_BACK;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_BOTTOM;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_TOP;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_A;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.util.meta.SectionMetaUtils;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class RunnerTemplate extends TemplateFactory {

    public RunnerTemplate(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.WHEEL;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] { SectionTypeEnum.TYPE_DISCHARGE, SectionTypeEnum.TYPE_FAN,
                SectionTypeEnum.TYPE_WHEELHEATRECYCLE, SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_SINGLE,
                SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_SINGLE,
                SectionTypeEnum.TYPE_ACCESS, SectionTypeEnum.TYPE_WHEELHEATRECYCLE, SectionTypeEnum.TYPE_MIX,
                SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
                SectionTypeEnum.TYPE_FAN };
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Part> preConfigureSections(List<Part> parts) {
        String[] airReturnsOfMix = { KEY_RETURN_BOTTOM, KEY_RETURN_BACK, KEY_RETURN_BACK, KEY_RETURN_TOP };
        int mixIndex = 0;
        List<Part> updatedParts = new ArrayList<Part>();
        for (Part part : parts) {
            String metaJson = part.getMetaJson();
            Map<String, Object> partMeta = JSONArray.parseObject(metaJson, Map.class);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(part.getSectionKey());

            /* 上层回风、下层送风 */
            if (getSectionLayout().isInReturnAirLayer(part.getPosition())) {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.RETURNAIR.getCode());
            } else {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            if (SectionTypeEnum.TYPE_DISCHARGE.equals(sectionType)) {
                partMeta.put(SectionMetaUtils.getMetaSectionKey(sectionType, KEY_OUTLETDIRECTION),
                        DISCHARGE_OUTLETDIRECTION_A);
            } else if (SectionTypeEnum.TYPE_MIX.equals(sectionType)) {
                setMixAirReturn(partMeta, airReturnsOfMix[mixIndex]);
                mixIndex++;
            }
            part.setMetaJson(JSONArray.toJSONString(partMeta));
            updatedParts.add(part);
        }
        return updatedParts;
    }

}
