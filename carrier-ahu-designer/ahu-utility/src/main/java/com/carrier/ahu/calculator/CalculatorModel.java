package com.carrier.ahu.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.metadata.WeightMetadata;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;

import lombok.Data;

/**
 * A price model contains all the price param in one object;
 *
 * @author JL
 */
@Data
public class CalculatorModel {

    private List<CalculatorSpec> spec = new ArrayList<>();
    private Map<String, String> fileSpecTypeMap = new HashMap<>();
    private Map<String, Map<String, Map<String, Map<String, String>>>> unitSeriesSpecDataMap = new HashMap<>();

    public Map<String, Map<String, String>> getSpecData(String unitSeries, CalculatorSpec spec) {
        String specDataKey = spec.getSpecKey();
        Map<String, Map<String, String>> specData = null;
        if (unitSeriesSpecDataMap.containsKey(unitSeries)) {
            Map<String, Map<String, Map<String, String>>> specDatas = unitSeriesSpecDataMap.get(unitSeries);
            if (specDatas != null && specDatas.containsKey(specDataKey)) {
                specData = specDatas.get(specDataKey);
            } else {
                specData = WeightMetadata.getWeightSpecData(AHUContext.getFactoryName(), unitSeries, spec);
                specDatas.put(specDataKey, specData);
            }
        } else {
            Map<String, Map<String, Map<String, String>>> specDatas = new HashMap<>();
            specData = WeightMetadata.getWeightSpecData(AHUContext.getFactoryName(), unitSeries, spec);
            specDatas.put(specDataKey, specData);
            unitSeriesSpecDataMap.put(unitSeries, specDatas);
        }
        return specData == null ? new HashMap<>() : specData;
    }

    public void setSpec(List<? extends CalculatorSpec> priceSpec) {
        this.spec.clear();
        this.fileSpecTypeMap.clear();
        Iterator<? extends CalculatorSpec> it = priceSpec.iterator();
        while (it.hasNext()) {
            CalculatorSpec spec = it.next();
            if (!this.fileSpecTypeMap.containsKey(spec.getTab())) {
                this.fileSpecTypeMap.put(spec.getTab(), spec.getAhucal());
            }
            this.spec.add(spec);
        }
    }

}
