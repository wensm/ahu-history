package com.carrier.ahu.resistance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.access.AccessData;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResi;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResiBig;
import com.carrier.ahu.metadata.entity.resistance.SClearSoundResi;
import com.carrier.ahu.metadata.entity.resistance.SElectricityResi;
import com.carrier.ahu.metadata.entity.resistance.SJunliuqiResi;
import com.carrier.ahu.metadata.entity.resistance.SResistance;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.resistance.count.CountParam;
import com.carrier.ahu.resistance.count.TPart;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/9/13. 计算阻力
 */
public class GetEntityResistance implements SystemCalculateConstants {

	public double getEntityResistance(String partName, String unitModel, int airV, Object otherPara) {
		double resistance = 0.00;
		String table = UtilityConstant.SYS_BLANK;
		double area = part_area(unitModel, partName);// 得到部件的横截面积
		String fn = UtilityConstant.SYS_STRING_RESISTANCE;
		if (UtilityConstant.SYS_SEXON_ALIAS_ACCESS.equals(partName)) {
			table = UtilityConstant.SYS_TABLE_S_JUNLIUQIRESI;
		} else if (UtilityConstant.SYS_SEXON_ALIAS_ELECTRICHEATINGCOIL.equals(partName)) {
			table = UtilityConstant.SYS_TABLE_S_ELECTRICITYRESI;
		} else if (UtilityConstant.SYS_SEXON_ALIAS_ATTENUATOR.equals(partName)) {
			table = UtilityConstant.SYS_TABLE_S_CLEARSOUNDRESI;
            if(UtilityConstant.SYS_STRING_NUMBER_2.equals(otherPara))
			    fn = fn + UtilityConstant.SYS_STRING_NUMBER_2;
		} else {
			if (UtilityConstant.SYS_SEXON_ALIAS_COOLINGCOIL.equals(partName)) {
				if (Integer.valueOf(unitModel.substring(unitModel.length() - 4, unitModel.length())) > 2333||!("alMesh".equals(otherPara))) {
					table = UtilityConstant.SYS_TABLE_S_BREAKWATERRESI_BIG;
				} else {
					table = UtilityConstant.SYS_TABLE_S_BREAKWATERRESI;
				}
			} else if (UtilityConstant.SYS_SEXON_ALIAS_SPRAYHUMIDIFIER.equals(partName)) {
				if (!UtilityConstant.SYS_BLANK.equals(UtilityConstant.SYS_SEXON_ALIAS_COOLINGCOIL)) {// TODO
					table = UtilityConstant.SYS_TABLE_S_BREAKWATERRESI_BIG;
				} else {
					table = UtilityConstant.SYS_TABLE_S_BREAKWATERRESI;
				}
			}
		}
		double velocityTemp1 = getMinVelocity(table);
		double velocityTemp2 = getMaxVelocity(table);

		double eAirVolume = 0;
		double sAirVolume = 0;
		boolean isTop = false;
		double airVolume = getAirVolume(airV, eAirVolume, sAirVolume, isTop);
		// 计算单位面积上的风量
		double velocity = airV / 3600.0 / area;

		double velocityX = 0.00;
		double velocityY = 0.00;
		if (UtilityConstant.SYS_SEXON_ALIAS_ACCESS.equals(partName)) {// 均流段
			velocity = velocity * 2.4;
		}
		if (velocity < velocityTemp1) {
			velocityX = velocityTemp1;
			velocityY = getVelocity(table, velocityX, UtilityConstant.SYS_PUNCTUATION_BIGGER, UtilityConstant.SYS_STRING_MIN);// min
		} else if (velocity > velocityTemp2) {
			velocityY = velocityTemp2;
			velocityX = getVelocity(table, velocityY, UtilityConstant.SYS_PUNCTUATION_LESS, UtilityConstant.SYS_STRING_MAX);// max
		} else {
			velocityY = getVelocity(table, velocity, UtilityConstant.SYS_PUNCTUATION_BIGGER_EQUAL, UtilityConstant.SYS_STRING_MIN);// min
			velocityX = getVelocity(table, velocity, UtilityConstant.SYS_PUNCTUATION_LESS_EQUAL, UtilityConstant.SYS_STRING_MAX);// max
		}

		double resistanceX = UtilityConstant.SYS_STRING_RESISTANCE_2.equals(fn)?getResistance2(velocityX, table):getResistance(velocityX, table);
		double resistanceY = UtilityConstant.SYS_STRING_RESISTANCE_2.equals(fn)?getResistance2(velocityY, table):getResistance(velocityY, table);

		CountParam countParam = new CountParam();
		countParam = fInterpolation(velocityX, velocity, velocityY, resistanceX, resistanceY, countParam);
		if (countParam.isfInterpolation()) {
			resistance = countParam.getResistance();
		}
		return resistance;
	}

    private double getMinVelocity(String table) {
        SResistance resi = null;
        if (UtilityConstant.SYS_TABLE_S_JUNLIUQIRESI.equals(table)) {
            resi = AhuMetadata.findMinOne(SJunliuqiResi.class, SJunliuqiResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_ELECTRICITYRESI.equals(table)) {
            resi = AhuMetadata.findMinOne(SElectricityResi.class, SElectricityResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_CLEARSOUNDRESI.equals(table)) {
            resi = AhuMetadata.findMinOne(SClearSoundResi.class, SClearSoundResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI_BIG.equals(table)) {
            resi = AhuMetadata.findMinOne(SBreakWaterResiBig.class, SBreakWaterResiBig::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI.equals(table)) {
            resi = AhuMetadata.findMinOne(SBreakWaterResi.class, SBreakWaterResi::getVelocity);
        }
        return resi == null ? 0 : resi.getVelocity();
    }

    private double getMaxVelocity(String table) {
        SResistance resi = null;
        if (UtilityConstant.SYS_TABLE_S_JUNLIUQIRESI.equals(table)) {
            resi = AhuMetadata.findMaxOne(SJunliuqiResi.class, SJunliuqiResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_ELECTRICITYRESI.equals(table)) {
            resi = AhuMetadata.findMaxOne(SElectricityResi.class, SElectricityResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_CLEARSOUNDRESI.equals(table)) {
            resi = AhuMetadata.findMaxOne(SClearSoundResi.class, SClearSoundResi::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI_BIG.equals(table)) {
            resi = AhuMetadata.findMaxOne(SBreakWaterResiBig.class, SBreakWaterResiBig::getVelocity);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI.equals(table)) {
            resi = AhuMetadata.findMaxOne(SBreakWaterResi.class, SBreakWaterResi::getVelocity);
        }
        return resi == null ? 0 : resi.getVelocity();
    }

    public double getVelocity(String table, double velocity, String req, String res) {
        List<? extends SResistance> sResistanceList = getResistanceList(table);
        List<SResistance> resistanceList = new ArrayList<>();
        if (res.equals(UtilityConstant.SYS_STRING_MIN)) {
            Collections.sort(sResistanceList, Comparator.comparing(SResistance::getVelocity));
        } else {
            Collections.sort(sResistanceList, Comparator.comparing(SResistance::getVelocity).reversed());
        }
        for (SResistance sBreakWaterResiPo : sResistanceList) {
            if (UtilityConstant.SYS_PUNCTUATION_BIGGER.equals(req)) {
                if (sBreakWaterResiPo.getVelocity() > velocity) {
                    return sBreakWaterResiPo.getVelocity();
                }
            } else if (UtilityConstant.SYS_PUNCTUATION_LESS.equals(req)) {
                if (sBreakWaterResiPo.getVelocity() < velocity) {
                    return sBreakWaterResiPo.getVelocity();
                }
            } else if (UtilityConstant.SYS_PUNCTUATION_BIGGER_EQUAL.equals(req)) {
                if (sBreakWaterResiPo.getVelocity() >= velocity) {
                    return sBreakWaterResiPo.getVelocity();
                }
            } else if (UtilityConstant.SYS_PUNCTUATION_LESS_EQUAL.equals(req)) {
                if (sBreakWaterResiPo.getVelocity() <= velocity) {
                    return sBreakWaterResiPo.getVelocity();
                }
            } else if (UtilityConstant.SYS_PUNCTUATION_EQUAL.equals(req)) {
                if (sBreakWaterResiPo.getVelocity() == velocity) {
                    return sBreakWaterResiPo.getVelocity();
                }
            }
        }
        return 0.00;
    }

    // compare equal for double value ???
    public double getResistance(double velocity, String table) {
        List<? extends SResistance> sResistanceList = getResistanceList(table);
        for (SResistance sResistance : sResistanceList) {
            if (velocity == sResistance.getVelocity()) {
                return sResistance.getResistance();
            }
        }
        return 0.00;
    }
    public double getResistance2(double velocity, String table) {
        List<? extends SResistance> sResistanceList = getResistanceList(table);
        for (SResistance sResistance : sResistanceList) {
            if (velocity == sResistance.getVelocity()) {
                return sResistance.getResistance2();
            }
        }
        return 0.00;
    }
    public List<? extends SResistance> getResistanceList(String table) {
        List<? extends SResistance> sResistanceList = null;
        if (UtilityConstant.SYS_TABLE_S_JUNLIUQIRESI.equals(table)) {
            sResistanceList = AhuMetadata.findAll(SJunliuqiResi.class);
        } else if (UtilityConstant.SYS_TABLE_S_ELECTRICITYRESI.equals(table)) {
            sResistanceList = AhuMetadata.findAll(SElectricityResi.class);
        } else if (UtilityConstant.SYS_TABLE_S_CLEARSOUNDRESI.equals(table)) {
            sResistanceList = AhuMetadata.findAll(SClearSoundResi.class);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI_BIG.equals(table)) {
            sResistanceList = AhuMetadata.findAll(SBreakWaterResiBig.class);
        } else if (UtilityConstant.SYS_TABLE_S_BREAKWATERRESI.equals(table)) {
            sResistanceList = AhuMetadata.findAll(SBreakWaterResi.class);
        }
        return sResistanceList;
    }

    // 每段属性定义：上层或下层、进风方向左 右、
	// 机组形式：H0卧式 V1立式 D2双层 S3并排左 S4并排右 R5热回收(双层) O6热回收单层 T7板式(双层)
	// fansType机组型号、partName段类型 secl长度
	// 封装传入对象 每段属性定义
	public double getResistance(String fansType, String partName, int secl, double airVolume) {
		// 获取部件的迎风面积
		// 1.1partID验证是否存在、partID：段ID 返回p_part对象数据 TPart
		// 1.2根据TPart查询机组型号 查询s_fan表根据表字段seq排序 封装S_FanList 查询s_fanXT 根据表字段ID
		// 封装S_FanListXT 如果机组型号为AHUXT返回s_fanXT机组型号 否则s_fan机组型号
		// 1.3根据机组型号、当前段.partName

		// 获取风量
		// 2.1机组形式：双层、并排左、并排右、热回收(双层) 回风：a.上层机组&排风机组件 进风：a.下层机组 b.上层机组&送风机组件
		// 2.2机组形式：板式(双层) 回风：a.空气流向右到左 进风：否则
		// 进风：

		// 查询P_part 封装partListT条件 order by orders 如果partListT包含part返回 fantype=1
		// 如果是板式双层7 封装partListB 条件desc查询 否则 order by orders 如果 partListB 包含part
		// fantype=2 优先验证partListT
		// 如果fantype=1 判断是否为上层风机、上层风机返回回风风量 否则使用进风风量 fantype=2 返回送风风量
		// 2.3如果是板式双层7 判断空气流向 如果1使用回风 否则使用进风风量 //风通过部件的方向 0，左到右 1：右到左
		// 2.4如果包含新回风段 当前段为新回风段 风量使用回风风量

		// 3.面风速 velocity
		// 查询s_resistance 条件：段长 查询s_resistance获取 最小值：velocityTemp1
		// 最大值：velocityTemp2
		// 如果velocity < velocityTemp1 查询s_resistance 条件：段长 获取最小velocity字段 赋值给
		// velocityX 查询s_resistance 条件：段长、velocity > velocityX 获取最小velocity字段
		// 赋值给 velocityY
		// 如果velocity > velocityTemp2 查询s_resistance 条件：段长 获取最大velocityy字段 赋值给
		// velocityY 查询s_resistance 条件：段长、velocity < velocityY 获取最大velocity字段
		// 赋值给 velocityX
		// 否则 查询s_resistance 条件：段长velocity >= velocity 获取最小velocity字段 赋值给
		// velocityY 查询s_resistance 条件：段长velocity >= velocity 获取最大velocity字段 赋值给
		// velocityX

		// 4.查询s_resistance 条件：段长 velocity = velocityX resistanceX = resistance
		// 查询s_resistance 条件：段长 velocity = velocityY resistanceY = resistance
		// 计算阻力

		// 根据partID验证当前计算的段在选型中 获取part对象 orderparts 赋值待验证
		// 根据part对象 返回机组型号
		double resistance = 0.00;
		// String partId = UtilityConstant.SYS_BLANK;
		// partName = UtilityConstant.SYS_BLANK;//TODO 封装partName BDE重新封装

		// 得到部件的横截面积
		double area = part_area(fansType, partName);

		// TODO 风量
		int fanType = 0;
		double eAirVolume = 0;
		double sAirVolume = 0;
		boolean isTop = false;

		// TODO 当回风和送风都存在时候，根据送风、出风计算风量
		// double airVolume = getAirVolume(fanType, eAirVolume, sAirVolume,
		// isTop);

		// 计算单位面积上的风量
		double velocity = getVelocity(area, airVolume);

		double velocityX = 0.00;
		double velocityY = 0.00;
		List<AccessData> sResistancePoList = getSResistanceListBySectionL(secl);
		double velocityTemp1 = minVelocity(sResistancePoList);// min
		double velocityTemp2 = maxVelocity(sResistancePoList);// max
        if (velocity < velocityTemp1) {
            velocityX = velocityTemp1;
            List<AccessData> reqResistancePoList = getSResistanceListBySectionL(secl, velocityX, UtilityConstant.SYS_PUNCTUATION_BIGGER);
            velocityY = minVelocity(reqResistancePoList);// min
        } else if (velocity > velocityTemp2) {
            velocityY = velocityTemp2;
            List<AccessData> reqResistancePoList = getSResistanceListBySectionL(secl, velocityY, UtilityConstant.SYS_PUNCTUATION_LESS);
            velocityX = maxVelocity(reqResistancePoList);// max
        } else {
            List<AccessData> YsResistancePoList = getSResistanceListBySectionL(secl, velocity, UtilityConstant.SYS_PUNCTUATION_BIGGER_EQUAL);
            velocityY = minVelocity(YsResistancePoList);// min
            List<AccessData> XsResistancePoList = getSResistanceListBySectionL(secl, velocity, UtilityConstant.SYS_PUNCTUATION_LESS_EQUAL);
            velocityX = maxVelocity(XsResistancePoList);// max
        }
		AccessData XsResistancePo = getSResistancePoBySectionLAndVelocity(secl, velocityX);
		double resistanceX = XsResistancePo.getResistance();
		AccessData YsResistancePo = getSResistancePoBySectionLAndVelocity(secl, velocityY);
		double resistanceY = YsResistancePo.getResistance();
		CountParam countParam = new CountParam();
		countParam = fInterpolation(velocityX, velocity, velocityY, resistanceX, resistanceY, countParam);
		if (countParam.isfInterpolation()) {
			resistance = countParam.getResistance();
		}
		return resistance;
	}

    // 根据sectionL查询
    private List<AccessData> getSResistanceListBySectionL(int sectionL) {
        List<AccessData> sResistancePoList = AhuMetadata.findList(AccessData.class, String.valueOf(sectionL));
        Collections.sort(sResistancePoList, new Comparator<AccessData>() {
            public int compare(AccessData o1, AccessData o2) {
                return BaseDataUtil.doubleConversionInteger(o1.getVelocity())
                        .compareTo(BaseDataUtil.doubleConversionInteger(o2.getVelocity()));
            }
        });
        return sResistancePoList;
    }

    // 根据 sectionL velocity 查询
    private AccessData getSResistancePoBySectionLAndVelocity(int sectionL, double velocity) {
        List<AccessData> sResistanceList = AhuMetadata.findList(AccessData.class, String.valueOf(sectionL));
        for (AccessData sResistance : sResistanceList) {
            // ?? equals of double value ??
            if (velocity == sResistance.getVelocity()) {
                return sResistance;
            }
        }
        return null;
    }

    // req > < >= <= =
    // equal for double ???
    private List<AccessData> getSResistanceListBySectionL(int sectionL, double velocity, String req) {
        List<AccessData> accessDataList = AhuMetadata.findList(AccessData.class, String.valueOf(sectionL));
        List<AccessData> sResistanceList = new ArrayList<AccessData>();
        for (AccessData sResistancePo : accessDataList) {
            if (sectionL == sResistancePo.getSectionL()) {
                if (UtilityConstant.SYS_PUNCTUATION_BIGGER.equals(req)) {
                    if (sResistancePo.getVelocity() > velocity) {
                        sResistanceList.add(sResistancePo);
                    }
                } else if (UtilityConstant.SYS_PUNCTUATION_LESS.equals(req)) {
                    if (sResistancePo.getVelocity() < velocity) {
                        sResistanceList.add(sResistancePo);
                    }
                } else if (UtilityConstant.SYS_PUNCTUATION_BIGGER_EQUAL.equals(req)) {
                    if (sResistancePo.getVelocity() >= velocity) {
                        sResistanceList.add(sResistancePo);
                    }
                } else if (UtilityConstant.SYS_PUNCTUATION_LESS_EQUAL.equals(req)) {
                    if (sResistancePo.getVelocity() <= velocity) {
                        sResistanceList.add(sResistancePo);
                    }
                } else if (UtilityConstant.SYS_PUNCTUATION_EQUAL.equals(req)) {
                    if (sResistancePo.getVelocity() == velocity) {
                        sResistanceList.add(sResistancePo);
                    }
                }
            }
        }
        Collections.sort(sResistanceList, new Comparator<AccessData>() {
            public int compare(AccessData o1, AccessData o2) {
                return BaseDataUtil.doubleConversionInteger(o1.getVelocity())
                        .compareTo(BaseDataUtil.doubleConversionInteger(o2.getVelocity()));
            }
        });
        return sResistanceList;
    }

	// 计算 resistance fInterpolation
	public CountParam fInterpolation(double x1, double x2, double x3, double y1, double y3, CountParam countParam) {
		double PRECSIONTMP = 0.0001;
		if (Math.abs(x3 - x1) < PRECSIONTMP) {
			countParam.setfInterpolation(false);
			return countParam;
		} else if (Math.abs(x3 - x2) < PRECSIONTMP) {
			countParam.setResistance(y3);
			countParam.setResistance(y1);
        } else if (Math.abs(x1 - x2) < PRECSIONTMP) {
        } else {
			countParam.setResistance((y1 * (x3 - x2) + y3 * (x2 - x1)) / (x3 - x1));
		}
		countParam.setfInterpolation(true);
		return countParam;
	}

	// 封装最小velocity
	private double minVelocity(List<AccessData> sResistancePoList) {
		if (EmptyUtil.isNotEmpty(sResistancePoList)) {
			return sResistancePoList.get(0).getVelocity();
		}
		return 0.00;
	}

	// 封装最大velocity
	private double maxVelocity(List<AccessData> sResistancePoList) {
		if (EmptyUtil.isNotEmpty(sResistancePoList)) {
			return sResistancePoList.get(sResistancePoList.size() - 1).getVelocity();
		}
		return 0.00;
	}

	// 封装getAirVolume
	// fanType 风机形式：H0卧式 V1立式 D2双层 S3并排左 S4并排右 R5热回收(双层) O6热回收单层 T7板换(双层)
	// eAirVolume 回风
	// sAirVolume 送风
	// isTop 是否上层
	private double getAirVolume(int fanType, double eAirVolume, double sAirVolume, boolean isTop) {
		// 如果风机形式为：双层、并排左、并排右、热回收(双层)
		if (2 == fanType || 3 == fanType || 4 == fanType || 5 == fanType) {
			if (1 == 1) {// TODO findfan(part) = 1 待验证
				if (isTop) {
					return eAirVolume;
				} else {
					return sAirVolume;
				}
			} else {
				return sAirVolume;
			}
		} else if (7 == fanType) {// 如果板换(双层)
			if (1 == 1) {// TODO part.Avdirection = 1 待验证 //风通过部件的方向 0，左到右 1：右到左
				return eAirVolume;// 回风
			} else {
				return sAirVolume;// 进风
			}
		} else {
			boolean b = false;
			boolean l = false;
			// 如果包含新回风段
			l = true;// TODO 待验证
			if (l) {
				// TODO lpart.type=12 //新回风段
				b = true;
			}
			if (b) {
				return eAirVolume;
			}
		}
		return sAirVolume;
	}

	// 封装getVelocity
	private double getVelocity(double area, double airVolume) {
		if (area != 0.00) {
			return airVolume / 3600.0 / area;
		} else {
			return 0.00;
		}
	}

	// 返回结果为List
	private TPart getPartByID(String partId) {
		// 根据当前段的ID验证当前段是否存在
		return new TPart();
	}

	// 封装fansTypes
	private String fansTypes(TPart fPart) {
		// 加载s_fan表数据 判断是否为AHUXT
		// 过去风机类型：39CQ2333 机组型号
		return UtilityConstant.SYS_BLANK;
	}

    public double part_area(String fansType, String partName) {
        double partArea = 0.00;
        SectionArea sArea = AhuMetadata.findOne(SectionArea.class, fansType);
        if (EmptyUtil.isNotEmpty(sArea)) {
            try {
                String value = BeanUtils.getProperty(sArea, partName.toLowerCase());
                partArea = BaseDataUtil.objectToDouble(value);
            } catch (Exception e) {
                // if no part
            }
        }
        return partArea;
    }

	public double packageN(String unitModel, int sectionLength, double resistance, String damperOutlet) {
		SectionLength secLength = AhuMetadata.findOne(SectionLength.class, unitModel);
		// 如果段长比表段长小1 阻力增加1pa
		int length = secLength.getN();
		if (length - sectionLength > 0) {
			resistance = resistance + length - sectionLength;
		}
		// 如果风口接件不是法兰增加5pa
		if (!DISCHARGE_AINTERFACE_FLANGE.equals(damperOutlet)) {
			resistance = resistance + 5;
		}
		return resistance;
	}
}
