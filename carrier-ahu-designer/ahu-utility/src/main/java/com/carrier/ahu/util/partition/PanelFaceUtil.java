package com.carrier.ahu.util.partition;

import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.constant.CommonConstant.METAHU_PIPEORIENTATION;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_FANMODEL;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_OUTLETDIRECTION;
import static com.carrier.ahu.constant.CommonConstant.SYS_PANEL_TYPE_CHUFENG;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_LEFT;
import static com.carrier.ahu.constant.CommonConstant.SYS_UNIT_SERIES_39CQ;
import static com.carrier.ahu.constant.CommonConstant.SYS_PANEL_TYPE_JIAQIANG;
import static com.carrier.ahu.constant.CommonConstant.SYS_PANEL_PANELCONNECTOR_F;
import static com.carrier.ahu.constant.UtilityConstant.SYS_PANEL_TYPE_PUTONG;
import static com.carrier.ahu.constant.UtilityConstant.SYS_PANEL_TYPE_MEN;
import static com.carrier.ahu.constant.UtilityConstant.SYS_PANEL_TYPE_WU;
import static com.carrier.ahu.constant.UtilityConstant.SYS_PANEL_TYPE_JIASHI;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.panel.PanelException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.FrameLine;
import com.carrier.ahu.common.model.partition.PanelFace;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.util.EmptyUtil;

public class PanelFaceUtil {

	private static String connection0 = "0";
	private static String connectionA = "A";
	private static String connectionB = "B";

	/**
	 * 重构面板数据结构，从树结构转化为类似横纵排列
	 * 
	 * @param parentsPanel
	 *            父面板
	 * @param panel
	 *            当前面板
	 * @param listPanels
	 *            按行/列重构面板结构。
	 * @param cutDirectionWay
	 *            先横切还是后切
	 */
	public static void convertTreeToMap(PanelFace parentsPanel, PanelFace panel,
			LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanels,
			LinkedHashMap<String, PanelFace> allPanels, Integer cutDirectionWay) {

		PanelFace subPanelLeft = null;
		PanelFace subPanelRight = null;
		allPanels.put(panel.getId(), panel);
		if (!panel.isLeafPanel()) {
			if (EmptyUtil.isNotEmpty(parentsPanel))
				panel.setParentsId(parentsPanel.getId());
			subPanelLeft = panel.getSubPanels()[0];
			subPanelRight = panel.getSubPanels()[1];
		}

		// 从根结点取到横切还是纵切优先
		if (parentsPanel == null) {
			cutDirectionWay = panel.getDirection();

			// 如果整个面板没有切割
			if (cutDirectionWay == 0 || (subPanelLeft == null && subPanelRight == null)) {
				LinkedHashMap<String, PanelFace> panels = new LinkedHashMap<String, PanelFace>();
				panel.setRootPanelId(panel.getId());
				panels.put(panel.getId(), panel);
				listPanels.put(panel.getId(), panels);
				return;
			} else {
				subPanelLeft.setRootPanelId(subPanelLeft.getId());
				subPanelRight.setRootPanelId(subPanelRight.getId());
				convertTreeToMap(panel, subPanelLeft, listPanels, allPanels, cutDirectionWay);
				convertTreeToMap(panel, subPanelRight, listPanels, allPanels, cutDirectionWay);
				return;
			}
		}

		// 如果是叶子节点，将叶子节点增加给新结构
		if (panel.isLeafPanel()) {
			panel.setParentsId(parentsPanel.getId());
			// 如果是同方向切割来的叶子节点，本身也是自己的父节点
			if (parentsPanel.getDirection() == cutDirectionWay) {
				LinkedHashMap<String, PanelFace> panels = new LinkedHashMap<String, PanelFace>();
				panel.setRootPanelId(panel.getId());
				// panel.setMemo("type"+panel.getSectionMetaId());
				panels.put(panel.getId(), panel);

				// 自己独自成一列/行
				listPanels.put(panel.getId(), panels);

			} else {
				// 如果切割方向相反，将面板增加到父面板的节点中
				LinkedHashMap<String, PanelFace> panelList = listPanels.get(panel.getRootPanelId());
				if (panelList == null) {
					panelList = new LinkedHashMap<String, PanelFace>();
				}
				// panel.setMemo("different way"+ panel.getRootPanelId());
				// panel.setMemo("type"+panel.getSectionMetaId());
				panelList.put(panel.getId(), panel);
			}

		} else {
			panel.setParentsId(parentsPanel.getId());
			// 如果不是叶子节点，当切割方向变化时，即为新列/行 产生的标志,父节点保持不变
			if (panel.getDirection() != parentsPanel.getDirection() && parentsPanel.getDirection() == cutDirectionWay) {
				LinkedHashMap<String, PanelFace> panels = new LinkedHashMap<String, PanelFace>();
				listPanels.put(panel.getId(), panels);
				subPanelLeft.setRootPanelId(panel.getId());
				subPanelRight.setRootPanelId(panel.getId());
			} else if (panel.getDirection() == parentsPanel.getDirection()
					&& parentsPanel.getDirection() == cutDirectionWay) {
				subPanelLeft.setRootPanelId(subPanelLeft.getId());
				subPanelRight.setRootPanelId(subPanelRight.getId());
			} else {
				subPanelLeft.setRootPanelId(panel.getRootPanelId());
				subPanelRight.setRootPanelId(panel.getRootPanelId());
			}

			convertTreeToMap(panel, subPanelLeft, listPanels, allPanels, cutDirectionWay);
			convertTreeToMap(panel, subPanelRight, listPanels, allPanels, cutDirectionWay);
			return;
		}
	}

	public static void resetConnectorType(String serial,
			LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanels,
			LinkedHashMap<String, PanelFace> allPanels, int cutDirectionWay) {

		if (!serial.contains(SYS_UNIT_SERIES_39CQ)) {
			return;
		}
		if (listPanels == null && listPanels.size() == 0) {
			return;
		}

		// 主切割方向
		LinkedList<CQConnectorPram> connectionXList = new LinkedList<CQConnectorPram>();
		for (Entry<String, LinkedHashMap<String, PanelFace>> entry : listPanels.entrySet()) {
			resetXPanel(cutDirectionWay, connectionXList, entry, true);
		}

		connectionXList = resetCQProperty(connectionXList);

		// 设置默认边框为0
		for (Entry<String, PanelFace> panel : allPanels.entrySet()) {
			if (panel == null) {
				continue;
			}

			if (panel.getValue().isLeafPanel()) {
				continue;
			}
			FrameLine fl = panel.getValue().getFrameLine();
			fl.setLineConnector(connection0);

		}

		// 将AB属性设置给面板

		for (CQConnectorPram param : connectionXList) {

			if (param == null) {
				continue;
			}

			LinkedHashMap<String, PanelFace> panels = listPanels.get(param.getPanelId());

			// 另一方向
			LinkedList<CQConnectorPram> connectionYList = new LinkedList<CQConnectorPram>();

			resetYPanel(cutDirectionWay, connectionYList, panels, false);

			connectionYList = resetCQProperty(connectionYList);

			String X = connection0;
			String Y = connection0;
			if (cutDirectionWay == 1) {
				X = param.getConnectorX();
			} else if (cutDirectionWay == 2) {
				Y = param.getConnectorX();
			}
			for (CQConnectorPram paramY : connectionYList) {
				if (cutDirectionWay == 1) {
					Y = paramY.getConnectorX();
				} else if (cutDirectionWay == 2) {
					X = paramY.getConnectorX();
				}

				// 重设面板
				PanelFace panelf = (listPanels.get(param.getPanelId())).get(paramY.getPanelId());

				if (String.valueOf(SYS_PANEL_TYPE_JIAQIANG).equals(panelf.getPanelType())) {
					panelf.setPanelConnector(X.concat(Y).concat(SYS_PANEL_PANELCONNECTOR_F));
				} else {
					panelf.setPanelConnector(X.concat(Y));
				}

				// 重设框条属性
				PanelFace parentsPanel = allPanels.get(panelf.getParentsId());
				if (parentsPanel != null) {
					FrameLine fl = parentsPanel.getFrameLine();
					if (fl == null) {
						continue;
					}
					if (parentsPanel.getDirection() == 1) {
						fl.setLineConnector(Y);
					} else if (parentsPanel.getDirection() == 2) {
						fl.setLineConnector(X);
					}
				}

			}
		}

	}

	private static void resetXPanel(int cutDirectionWay, LinkedList<CQConnectorPram> connectionXList,
			Entry<String, LinkedHashMap<String, PanelFace>> entry, boolean isMainCutDirection) {
		int length = 0;
		boolean isSpecialPanel = false;
		CQConnectorPram cqParam = new CQConnectorPram();
		for (Entry<String, PanelFace> panel : entry.getValue().entrySet()) {

			if (cutDirectionWay == 1) {
				length = panel.getValue().getPanelWidth();
			}
			if (cutDirectionWay == 2) {
				length = panel.getValue().getPanelLength();
			}

			int panelType = Integer
					.parseInt(panel.getValue().getPanelType() == null ? String.valueOf(SYS_PANEL_TYPE_PUTONG)
							: panel.getValue().getPanelType());
			if (SYS_PANEL_TYPE_JIASHI == panelType || SYS_PANEL_TYPE_MEN == panelType
					|| SYS_PANEL_TYPE_WU == panelType) {
				isSpecialPanel = true;
				break;
			}
		}

		cqParam.setPanelId(entry.getKey());
		cqParam.setLength(length);

		if (isSpecialPanel) {
			cqParam.setConnectorX(connection0);

		} else {
			cqParam.setConnectorX(connectionA);
		}
		connectionXList.add(cqParam);
	}

	private static void resetYPanel(int cutDirectionWay, LinkedList<CQConnectorPram> connectionXList,
			LinkedHashMap<String, PanelFace> entry, boolean isMainCutDirection) {
		int length = 0;

		for (Entry<String, PanelFace> panel : entry.entrySet()) {
			CQConnectorPram cqParam = new CQConnectorPram();

			if (cutDirectionWay == 1) {
				length = panel.getValue().getPanelLength();
			}
			if (cutDirectionWay == 2) {
				length = panel.getValue().getPanelWidth();
			}

			try {
				int panelType = Integer.parseInt(panel.getValue().getPanelType());
				if (SYS_PANEL_TYPE_JIASHI == panelType || SYS_PANEL_TYPE_MEN == panelType
						|| SYS_PANEL_TYPE_WU == panelType) {
					cqParam.setConnectorX(connection0);
				} else {
					cqParam.setConnectorX(connectionA);
				}

			} catch (Exception e) {
				System.out.println(e);
			}

			cqParam.setPanelId(panel.getValue().getId());
			cqParam.setLength(length);

			connectionXList.add(cqParam);
		}

	}

	/**
	 * 
	 * @param connectorMap
	 * @return
	 */
	private static LinkedList<CQConnectorPram> resetCQProperty(LinkedList<CQConnectorPram> connectorList) {

		Collections.sort(connectorList);

		int connection0Num = 0;
		int connectionBNum = 0;
		int connectionANum = 0;

		for (CQConnectorPram connection : connectorList) {
			if (connection0.equals(connection.getConnectorX())) {
				connection0Num++;
			}

			if (connectionA.equals(connection.getConnectorX())) {
				connectionANum++;
			}
		}

		if (connection0Num == 0) {
			int count = 0;
			for (CQConnectorPram connection : connectorList) {
				if (count == 0) {
					connection.setConnectorX(connection0);
					count++;
					continue;
				}

				connection.setConnectorX(connectionA);

			}
		}

		if (connection0Num == 1) {
			for (CQConnectorPram connection : connectorList) {
				if (connection0.equals(connection.getConnectorX())) {
					continue;
				}

				connection.setConnectorX(connectionA);
			}

		}

		if (connection0Num > 1) {
			connectionBNum = connection0Num - 1;
			for (CQConnectorPram connection : connectorList) {
				if (connection0.equals(connection.getConnectorX())) {
					continue;
				}
				if (connectionBNum > 0) {
					connection.setConnectorX(connectionB);
					connectionBNum--;
				}

			}
		}

		return connectorList;

	}

	public static void setFanParameter(String serial,
			LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanels, Queue<String> fans) {

		if (serial.contains(SYS_UNIT_SERIES_39CQ)) {
			return;
		}

		if (fans.isEmpty()) {
			return;
		}

		for (Entry<String, LinkedHashMap<String, PanelFace>> root : listPanels.entrySet()) {
			for (Entry<String, PanelFace> entry : root.getValue().entrySet()) {
				PanelFace pface = entry.getValue();
				if (String.valueOf(SYS_PANEL_TYPE_CHUFENG).equals(pface.getPanelType())
						&& EmptyUtil.isNotEmpty(pface.getSectionMetaId())
						&& ((pface.getSectionMetaId().contains(SectionTypeEnum.TYPE_FAN.getId()))
								|| ((pface.getSectionMetaId().contains(SectionTypeEnum.TYPE_WWKFAN.getId()))))) {
					if (!fans.isEmpty()) {
						pface.setMemo(fans.poll());
					}
				}
			}
		}

	}

	public static Queue<String> getFanProperty(String serial, AhuPartition partition) {

		if (serial.contains(SYS_UNIT_SERIES_39CQ)) {
			return null;
		}

		Queue<String> queue = new ArrayDeque<String>();
		LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
		int size = coupleSections.size();
		Map<String, Object> ahuParaMap = partition.getAhuParameters();
		int sectionsStepIndex = 0;
		// 接管方向
		String pipe = "R";
		String pipeOrientationStr = String.valueOf(partition.getAhuParameters().get(METAHU_PIPEORIENTATION));
		if (SYS_STRING_LEFT.equals(pipeOrientationStr)) {
			pipe = "L";
		}
		;// 接管方向：左

		for (Map<String, Object> coupleSection : coupleSections) {

			String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
			SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
			if (SectionTypeEnum.TYPE_FAN != sectionType) {
				sectionsStepIndex++;
				continue;
			}

			Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
			Map<String, String> sectionMap = new HashMap<>();
			String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
			sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
			String fanModel = sectionMap.get(METASEXON_FAN_FANMODEL).replaceAll("[^\\d]+", "");
			String fanDirection = sectionMap.get(METASEXON_FAN_OUTLETDIRECTION).toUpperCase();
			queue.add(fanModel + fanDirection + pipe);
			sectionsStepIndex++;
		}
		return queue;
	}

	/**
	 * 合并面板
	 * 
	 * @param panelId1
	 * @param panelId2
	 * @param listPanels
	 *            Map结构面板数据
	 * @param allPanels
	 *            List结构面板数据
	 * @return
	 * @throws PanelException
	 */
	public static PanelFace conbinePanel(String panelId1, String panelId2,
			LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanels,
			LinkedHashMap<String, PanelFace> allPanels) throws PanelException {

		// 校验是否可以合并
		PanelFace panel1 = allPanels.get(panelId1);
		PanelFace panel2 = allPanels.get(panelId2);
		if (panel1 == null || panel2 == null) {
			throw new PanelException(PANEL_MUST_TWO_FAILED);
		}

		PanelFace rootPanel1 = allPanels.get(panel1.getRootPanelId());
		PanelFace rootPanel2 = allPanels.get(panel2.getRootPanelId());
		if (rootPanel1 == null || rootPanel2 == null) {
			throw new PanelException(PANEL_MERGE_FAILED);
		}

		if (!rootPanel1.getId().equals(rootPanel2.getId())) {
			if (!rootPanel1.isLeafPanel() || !rootPanel2.isLeafPanel()) {
				throw new PanelException(PANEL_MERGE_FAILED);
			}

			int panel1Pos = 0;
			int panel2Pos = 0;
			int pos = 0;
			for (String key : listPanels.keySet()) {
				if (rootPanel1.getId().equals(key)) {
					panel1Pos = pos;
				}
				if (rootPanel2.getId().equals(key)) {
					panel2Pos = pos;
				}
				pos++;

			}
			if (Math.abs(panel1Pos - panel2Pos) >1) {
				throw new PanelException(PANEL_MERGE_FAILED);
			}
		}

		 if(rootPanel1.getId().equals(rootPanel2.getId())) {
		 //获取ID在Set中的位置，如果不相邻，不能合并
			    int panel1Pos = 0;
				int panel2Pos = 0;
				int pos = 0;
				
				LinkedHashMap<String, PanelFace> panels=listPanels.get(rootPanel1.getId());
				
				if(panels==null) {
					return null;
				}
				
				
				
				for (String key : panels.keySet()) {
					if (panelId1.equals(key)) {
						panel1Pos = pos;
					}
					if (panelId2.equals(key)) {
						panel2Pos = pos;
					}
					pos++;

				}
				if (Math.abs(panel1Pos - panel2Pos) >1) {
					throw new PanelException(PANEL_MERGE_FAILED);
				}
		 }

		PanelFace parentPanel1 = allPanels.get(panel1.getParentsId());
		PanelFace parentPanel2 = allPanels.get(panel2.getParentsId());
		if (parentPanel1.getDirection() != parentPanel2.getDirection()) {
			throw new PanelException(PANEL_MERGE_FAILED);
		}

		int updatePanelSize = 0;

		// 从层级深的节点删除
		if (panelId1.length() > panelId2.length()) {
			if (1 == parentPanel1.getDirection()) {
				updatePanelSize = panel1.getPanelWidth();

			} else {
				updatePanelSize = panel1.getPanelLength();

			}
			updatePanelSize(panelId1, allPanels, updatePanelSize, -1);
			updatePanelSize(panelId2, allPanels, updatePanelSize, 1);

		} else {

			if (1 == parentPanel2.getDirection()) {
				updatePanelSize = panel2.getPanelWidth();

			} else {
				updatePanelSize = panel2.getPanelLength();

			}
			updatePanelSize(panelId1, allPanels, updatePanelSize, 1);
			updatePanelSize(panelId2, allPanels, updatePanelSize, -1);
		}
		return null;

	}

	/**
	 * 修改当前Panel的父节点的面板大小，
	 * 
	 * @param allPanels
	 * @param updatePanelSize
	 *            改变量
	 * @param isAdd
	 *            1 增加 -1减小
	 */
	private static void updatePanelSize(String panelId, LinkedHashMap<String, PanelFace> allPanels, int updatePanelSize,
			int isAdd) {
		int updateSize = updatePanelSize * isAdd;

		PanelFace panel = allPanels.get(panelId);
		PanelFace parentPanel = allPanels.get(panel.getParentsId());
		
		if(parentPanel==null) {
			return;
		}

		int direction = parentPanel.getDirection();
		if (direction == 0) {
			return;
		}

		// 删除当前节点
		if (isAdd == -1) {
			PanelFace[] subPanels=parentPanel.getSubPanels();
			if(null!=subPanels&subPanels.length>0) {
				for(PanelFace subPanel:subPanels) {
					if(panel.getId()!=subPanel.getId()) {
						
						//如果当前是叶子节点，直接删除节点
						if(subPanel.isLeafPanel()) {
							parentPanel.setDirection(0);
							parentPanel.setSubPanels(null);
							parentPanel.setPanelType(panel.getPanelType());
						}else {
							//如果节点下面有子节点
						
							parentPanel.setCutDirection(subPanel.getCutDirection());
							parentPanel.setDirection(subPanel.getDirection());
							parentPanel.setFrameLine(subPanel.getFrameLine());
							parentPanel.setMemo(subPanel.getMemo());
							parentPanel.setPanelConnector(subPanel.getPanelConnector());
							parentPanel.setPanelType(subPanel.getPanelConnector());
							parentPanel.setSectionMetaId(subPanel.getSectionMetaId());
							parentPanel.setSubPanels(subPanel.getSubPanels());
						    
						}
						
					}
				}
			}
		}

		if (direction == 1) {
			if (1 == isAdd) {
				panel.setPanelWidth(panel.getPanelWidth() + updateSize);
			}
		} else {
			if (1 == isAdd) {
				panel.setPanelLength(panel.getPanelLength() + updateSize);
			}
		}

		while (parentPanel != null) {
			if (direction == 1) {
				parentPanel.setPanelWidth(parentPanel.getPanelWidth() + updateSize);
			} else {
				parentPanel.setPanelLength(parentPanel.getPanelLength() + updateSize);
			}

			parentPanel = allPanels.get(parentPanel.getParentsId());

		}

	}
}
