package com.carrier.ahu.resistance;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.humidifier.SISupplier;
import com.carrier.ahu.resistance.count.CountParam;
import com.carrier.ahu.resistance.count.WetFilmResultInfo;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liangd4 on 2017/9/11.
 * 湿膜加湿段
 */
public class WetFilmRes implements SystemCalculateConstants {

    static final GetEntityResistance getEntityResistance = new GetEntityResistance();

    private Comparator<SISupplier> velocityComparator = new Comparator<SISupplier>() {
		public int compare(SISupplier o1, SISupplier o2) {
			return (UtilityConstant.SYS_BLANK + o1.getVelocity())
					.compareTo(UtilityConstant.SYS_BLANK + o2.getVelocity());
		}
    };

    /**
     * partName :段的标识code
     * unitModel : 机组型号
     * airV : 风量
     * supplier : 供应商
     * Humidification : 加湿量 sHumidification : 夏季 、 wHumidification : 冬季
     * E_Db : 干球温度
     * E_Wb : 湿球温度
     */
    public WetFilmResultInfo getResistance(String partName, String unitModel, int airV, String supplier, String season, double sHumidification, double wHumidification, double S_Db, double S_Wb, double W_Db, double W_Wb) {
        WetFilmResultInfo wetFilmResultInfo = new WetFilmResultInfo();
        //得到部件的横截面积
        double fv = getEntityResistance.part_area(unitModel, partName);
        if (fv > 0.001) {
            fv = airV / 3600.0 / fv;
        } else {
            return wetFilmResultInfo;
        }
        double fv1 = fv;
        if (fv >= 4) {
            fv1 = 3.9;
        } else if (fv <= 1.5) {
            fv1 = 1.1;
        }
        double[] vel = new double[2];
        double maxVelocity = getMaxVelocity(supplier, fv1);
        vel[0] = maxVelocity;
        double minVelocity = getMinVelocity(supplier, fv1);
        vel[1] = minVelocity;
        Map<Integer, SISupplier> maxMap = getSiSupplierByMaxVelocity(supplier, maxVelocity);
        Map<Integer, SISupplier> minMap = getSiSupplierByMaxVelocity(supplier, minVelocity);
        double wCalEfficiency = 0.00;
        double sCalEfficiency = 0.00;
        if (seasonW.equals(season)) {
            wCalEfficiency = calEfficiency(wHumidification, airV, W_Db, W_Wb);//冬季
        } else if (seasonS.equals(season)) {
            sCalEfficiency = calEfficiency(sHumidification, airV, S_Db, S_Wb);//夏季
        } else {
            wCalEfficiency = calEfficiency(wHumidification, airV, W_Db, W_Wb);//冬季
            sCalEfficiency = calEfficiency(sHumidification, airV, S_Db, S_Wb);//夏季
        }
        if (seasonW.equals(season)) {
            fv1 = wCalEfficiency;
        } else if (seasonS.equals(season)) {
            fv1 = sCalEfficiency;
        } else {
            if (sCalEfficiency > wCalEfficiency) {
                fv1 = sCalEfficiency;
            } else {
                fv1 = wCalEfficiency;
            }
        }
        double resistance = 0.00;
        for (int i = 0; i < 4; i++) {
            SISupplier maxSISupplierPo = maxMap.get(i);
            SISupplier minSISupplierPo = minMap.get(i);
            CountParam countParam = new CountParam();
            countParam = getEntityResistance.fInterpolation(vel[0], fv, vel[1], maxSISupplierPo.getEfficiency(), minSISupplierPo.getEfficiency(), countParam);
            resistance = countParam.getResistance();
            if (resistance > fv1) {
                CountParam countParam1 = new CountParam();
                countParam1 = getEntityResistance.fInterpolation(vel[0], fv, vel[1], maxSISupplierPo.getResistance(), minSISupplierPo.getResistance(), countParam1);
                resistance = countParam1.getResistance();
                System.out.println("thickness :" + maxSISupplierPo.getThickness());
                wetFilmResultInfo.setResistance(resistance);
                wetFilmResultInfo.setThickness(maxSISupplierPo.getThickness());
                break;
            }
        }
        return wetFilmResultInfo;
    }

    private double getMaxVelocity(String supplier, double velocity) {
        List<SISupplier> siSuppliers = AhuMetadata.findList(SISupplier.class, supplier);
        ListUtils.sort(siSuppliers, true, UtilityConstant.SYS_STRING_VELOCITY);

        List<SISupplier> siSupplierList = new ArrayList<SISupplier>();
        for (SISupplier siSupplier : siSuppliers) {
            if (velocity >= siSupplier.getVelocity()) {
                siSupplierList.add(siSupplier);
            }
        }
        ListUtils.sort(siSupplierList, true, UtilityConstant.SYS_STRING_VELOCITY);
        return siSupplierList.get(siSupplierList.size()-1).getVelocity();
    }

    private double getMinVelocity(String supplier, double velocity) {
        List<SISupplier> siSuppliers = AhuMetadata.findList(SISupplier.class, supplier);
        ListUtils.sort(siSuppliers, true, UtilityConstant.SYS_STRING_VELOCITY);

        List<SISupplier> siSupplierList = new ArrayList<SISupplier>();
        for (SISupplier siSupplier : siSuppliers) {
            if (velocity < siSupplier.getVelocity()) {
                siSupplierList.add(siSupplier);
            }
        }
        ListUtils.sort(siSupplierList, true, UtilityConstant.SYS_STRING_VELOCITY);
        return siSupplierList.get(0).getVelocity();
    }

    private Map<Integer, SISupplier> getSiSupplierByMaxVelocity(String supplier, double maxVelocity) {
        List<SISupplier> siSuppliers = AhuMetadata.findList(SISupplier.class, supplier);
        Map<Integer, SISupplier> map = new HashMap<Integer, SISupplier>();
        List<SISupplier> siSupplierList = new ArrayList<SISupplier>();
        for (SISupplier siSupplier : siSuppliers) {
            if (maxVelocity == siSupplier.getVelocity()) {
                siSupplierList.add(siSupplier);
            }
        }
        ListUtils.sort(siSupplierList, true, UtilityConstant.SYS_STRING_VELOCITY);
        int i = 0;
        for (SISupplier siSupplier : siSupplierList) {
            map.put(i, siSupplier);
            i++;
        }
        return map;
    }

    /**
     * Humidification :加湿量
     * FG ：风量
     * E_DB 干球温度
     * E_WB 湿球温度
     */
    private double calEfficiency(double Humidification, int fG, double E_Db, double E_Wb) {
        AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(E_Db, E_Wb);
        double fD = Humidification * 1000 / 1.2 / fG + airConditionBean.getParamD();
        double fauxi = AirConditionUtils.FIterationTI(airConditionBean.getParmT(), airConditionBean.getParamI(), airConditionBean.getParmF(), true);
        double fauxi1 = AirConditionUtils.fSearchTBDb(fauxi);
        double fEfficiency = (fD - airConditionBean.getParamD()) / (fauxi1 - airConditionBean.getParamD()) * 100;
        return fEfficiency;
    }

}
