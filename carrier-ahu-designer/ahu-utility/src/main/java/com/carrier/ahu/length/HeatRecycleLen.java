package com.carrier.ahu.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.section.SectionLength;

/**
 * Created by liangd4 on 2018/1/15. 转轮热回收 板式热回收
 */
public class HeatRecycleLen {
    // 计算段长
    public double getLength(String serial, String sectionType) {
        // 段长计算 1：板式 2：转轮
        SectionLength sectionLength = AhuMetadata.findOne(SectionLength.class, serial);
        if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType)) { // 转轮
            return sectionLength.getW2();
        } else {
            return sectionLength.getW1();
        }
    }
}
