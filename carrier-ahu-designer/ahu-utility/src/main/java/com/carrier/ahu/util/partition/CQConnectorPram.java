package com.carrier.ahu.util.partition;

import lombok.Data;

@Data
public class CQConnectorPram implements Comparable<Object>{

	private String panelId;
	private int length;
	private String connectorX;
	

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return this.length-((CQConnectorPram)arg0).getLength();
	}
	
}
