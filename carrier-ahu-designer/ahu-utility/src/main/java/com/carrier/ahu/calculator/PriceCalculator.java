package com.carrier.ahu.calculator;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.metadata.entity.calc.PriceCalculatorSpec;
import com.carrier.ahu.util.EmptyUtil;

/**
 * 计算器引擎 [价钱计算器]
 * 
 * @author Simon
 *
 */
@Component
public class PriceCalculator extends AbstractCalculator {

    public double queryPrice(String smodel, CalculatorSpec table, Map<String, Object> ahuValueMap) {
        Map<String, Map<String, String>> priceMap = getCalculatorMap();
        if (EmptyUtil.isEmpty(priceMap)) {
            return -1d;
        }
        // Map<String, String> modelPriceMap = priceMap.get(smodel);
        String propStr = table.getProperty();
        String priceKey = CalculatorUtil.calPropertyValues(propStr, ahuValueMap, table);
        // 2.根据Key查询获得值
        for (String mapKey : priceMap.keySet()) {
            Map<String, String> map = priceMap.get(mapKey);
            if (map.containsKey(priceKey)) {
                try {
                    return Double.parseDouble(map.get(priceKey));
                } catch (Exception e) {
                    return -2d;
                }
            }
        }
        return -1d;
    }

    @Override
    protected Map<String, Map<String, String>> resolveData(CalculatorModel model) {
        return ExcelForPriceUtils.read2map(null, model);
    }

    @Override
    protected Class<PriceCalculatorSpec> getCalculatorSpecClass() {
        return PriceCalculatorSpec.class;
    }

}
