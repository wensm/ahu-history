package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_PSY_CHART_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_TRIPLE_VIEW_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.CARRIER_CORPORATION_FULL_NAME;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONFOREPART;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONPOSITIVE;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONVERTICAL;
import static com.carrier.ahu.common.intl.I18NConstants.EQUIPMENT_DATA_TABLE;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_CURVE;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CONNECTION_LIST;
import static com.carrier.ahu.common.intl.I18NConstants.TECHNICAL_SPECIFICATION;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_MODEL_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_NAMEPLATE_DATA;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_PTC;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_AHU_NONSTANDARD;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_SALES_AHU_OVERALL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_AHU_OVERALL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_FAN_CURVE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_HEADER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_PRODUCTPACKINGLIST;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_PRODUCTPACKINGLIST_EXPORT;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_SECTION_CONNECTION_LIST;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA_SHAPE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_T_ARUPPF;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_T_BOCHENG_AHU;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_T_BOCHENG_HEADER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_T_BOCHENG_SPEC;
import static com.carrier.ahu.util.DateUtil.HH_MM;
import static com.carrier.ahu.util.DateUtil.YYYY_MM_DD;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_PRODUCT_PACKING_LIST_NAME;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_SERIAL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.CadUtils;
import com.carrier.ahu.metadata.ReportMetadata;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.model.calunit.UnitUtil;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.psychometric.PsyCalBean;
import com.carrier.ahu.psychometric.PsychometricDrawer;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.Report;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.report.ReportItem;
import com.carrier.ahu.report.ReportUnitContent;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.report.pdf.PDFTableGenerator;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.carrier.ahu.vo.SysConstants;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;

public class PDF4ReportUtils {
	protected static Logger logger = LoggerFactory.getLogger(PDF4ReportUtils.class);

	/** PDF类型 - 奥雅纳工程格式报告 */
	public static final String T_ARUPPF = "aruppf";
	/** PDF类型 - 柏诚工程格式报告 */
	public static final String T_BOCHENGPF = "bochengpf";
	/** PDF类型 - 技术说明(工程版) */
	public static final String T_TECHPROJ = "techprojPdf";
	/** PDF类型 - 技术说明(销售版) */
	public static final String T_TECHSALER = "techsaler";

	/** 字体大小 文档 大标题 */
	public final static Integer TITLE_BIGGER = 32;
	/** 字体大小 表格 大表头 */
	public final static Integer TABLE_TITLE_BIGGER = 15;
	/** 字体大小 表格 常规表头 */
	public final static Integer TABLE_TITLE_NOMAL = 10;
	/** 字体大小 常规内容 */
	public final static Integer CONTENT_NOMAL = 10;
	/** 字体大小 页眉 */
	public final static Integer HEADER_TITLE = 11;
	/** 字体大小 页眉 bold */
	public final static Integer HEADER_TITLE_BOLD = 12;
	/** 字体大小 页脚 */
	public final static Integer FOOTER_TITLE = 12;

//	/** 汉字字体 宋体 */
//	private static final String UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH = "asserts/font/simsun.ttf";
//	/** 汉字字体 微软雅黑 */
//	private static final String UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH = "asserts/font/msyh.ttf";

	/**
	 * 奥雅纳工程格式报告
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Aruppf(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws DocumentException, IOException {
        FileOutputStream fos = new FileOutputStream(file.getPath());
        Document document = new Document(PageSize.A3);// 文档
        PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
        writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
        writer.setTagged();
        writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
        writer.createXmpMetadata();
        document.open();// 写入数据之前要打开文档

        addTitle(document, report.getName());
        List<ReportUnitContent> units = reportData2ReportUnitContent(reportData, T_ARUPPF, language, unitType);
        for (int i = 0; i < units.size(); i++) {
            ReportUnitContent u = units.get(i);
            addLine(document);
            addTableTitle(document, getIntlString(EQUIPMENT_DATA_TABLE) + u.getUnit().getDrawingNo());
            try {
                document.add(PDFTableGenerator.generate(u.getContent(), ReportConstants.REPORT_T_ARUPPF));
            } catch (Exception e) {
                logger.error("奥雅纳工程格式报告》报错", e);
            }
            if (i < units.size() - 1) {
                document.newPage();
            }
        }
        close(document, writer, fos);
    }

	/**
	 * 柏诚工程格式报告
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Bochengpf(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();


		//writer.setPageEvent(new TechHeader());// 页头黑色边框
		Phrase phrase = new Phrase();
		Date d = new Date();
        Chunk timeChunk = new Chunk(format("  Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
                DateUtil.getDateTimeString(d, HH_MM)), getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
        phrase.add(timeChunk);
		Chunk wenzi = new Chunk("          " + report.getName(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
		phrase.add(wenzi);
		phrase.add(Chunk.NEWLINE);// 换行
		String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_HEADER), reportData.getProject(),
				null, null, language, unitType);
		for (int i = 0; i < headerContents.length; i++) {
			phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
		}
		phrase.add(Chunk.NEWLINE);// 换行
		HeaderFooter header = new HeaderFooter(phrase, false);
		header.setBorder(Rectangle.BOX); // 设置为没有边框
		header.setAlignment(Element.ALIGN_LEFT);
		document.setHeader(header);


		document.open();// 写入数据之前要打开文档

        int pageIndex = 0;
		for (Unit unit : reportData.getUnitList()) {
			if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
				document.newPage();
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Project project = reportData.getProject();
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			addBr(document);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_SPEC), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			Map<String, String> mixMap = new HashMap<String,String>();
			for (PartPO partPO : partList) {
				Part part = partPO.getCurrentPart();
				String key = part.getSectionKey();
				if ("ahu.mix".equals(key)) {
					mixMap = allMap.get(UnitConverter.genUnitKey(unit, part));
				}else {
					continue;
				}
			}
			ahuContents = SectionContentConvertUtils.getBCAhu(ahuContents, allMap.get(UnitConverter.UNIT), mixMap, null);
			document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_T_BOCHENG_SPEC));

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("柏诚工程格式报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}", new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					addLine(document);
					Map<String, String> noChangeMap=allMap.get(UnitConverter.genUnitKey(unit, part));
	                Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
					PDFTableGen.addSectionBoCheng(document, key, order, noChangeMap, cloneMap, language, unitType, project, unit);
					order++;
				}
			}
			//增加柏诚报告下部内容
			addBr(document);
			String[][] ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_AHU), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			for (PartPO partPO : partList) {
				Part part = partPO.getCurrentPart();
				String key = part.getSectionKey();
				if(key.contains(UtilityConstant.METASEXON_FAN)) {
					ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_AHU), reportData.getProject(), unit,
							allMap.get(UnitConverter.genUnitKey(unit, part)), language, unitType);
					ahu2Contents = SectionContentConvertUtils.getBCAhu2(ahu2Contents, allMap.get(UnitConverter.genUnitKey(unit, part)), null);
				}else {
					continue;
				}
			}
			// 封装过滤段特殊格式数据
			Map<String, Partition> partitionMap = reportData.getPartitionMap();
	    	Partition partition = null;
	    	String partitionJson =UtilityConstant.SYS_BLANK;
	    	List<AhuPartition> ahuPartitionList = new ArrayList<>();
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			if (EmptyUtil.isNotEmpty(partition)) {
				partitionJson = partition.getPartitionJson();
			}
			if (EmptyUtil.isNotEmpty(partitionJson)) {
				ahuPartitionList = JSONArray.parseArray(partitionJson, AhuPartition.class);
			}
			
			ahu2Contents = SectionContentConvertUtils.getBCAhu2(ahu2Contents, allMap.get(UnitConverter.UNIT), ahuPartitionList);
			document.add(PDFTableGenerator.generate(ahu2Contents, ReportConstants.REPORT_T_BOCHENG_AHU));
		}
		close(document, writer, fos);
	}

	/**
     * 转化工程格式数据
     * 
     * @param reportData
     * @param pdfClassify
     * @param arrayMap
     * @return
     * @throws IOException
     */
    public static List<ReportUnitContent> reportData2ReportUnitContent(ReportData reportData, String pdfClassify,
            LanguageEnum language, UnitSystemEnum unitType) throws IOException {
        List<ReportUnitContent> reportUnitContents = new ArrayList<>();

        Project project = reportData.getProject();

        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = new ArrayList<>();

            for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
                if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
                    partList.add(e.getValue());
                }
            }
            //获取所有段、ahu 的值
            Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
            Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
            
            ReportUnitContent content = new ReportUnitContent();
            content.setUnit(unit);
            if (PDF4ReportUtils.T_ARUPPF.equals(pdfClassify)) {// 奥雅纳工程格式报告
                String[][] contents = ReportContent.getReportContent(REPORT_T_ARUPPF);
                
                //-----------------ahu数据 特殊处理start-----------------
                BigDecimal weightBd = new BigDecimal(BaseDataUtil.constraintString(unit.getWeight()));//四舍五入，保留两位小数
            	weightBd = weightBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                allMap2.put(UtilityConstant.METAHU_WEIGHT1, String.valueOf(weightBd));//设备运作重量
                //最大尺寸(长*宽*高)
				Map<String, String> ahuMap = allMap.get(UnitConverter.UNIT);
				double totalL = 0;
				double sectionL = 0;
				double weight = 0;//临时存储重量
				double MaxWeight = 0;//最重段重量
				String MaxWtName= UtilityConstant.SYS_BLANK;//最重段名
				for(Map<String, String> entityMap : allMap.values()) {//循环遍历每个段，收集段长和重量数据
					for(String mapKey : entityMap.keySet()) {
						if (mapKey.contains(UtilityConstant.METACOMMON_POSTFIX_SECTIONL) && !mapKey.contains(UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU)) {
							sectionL += BaseDataUtil.stringConversionDouble(String.valueOf(entityMap.get(mapKey)));
						}
						if (mapKey.contains(UtilityConstant.METACOMMON_POSTFIX_WEIGHT) && !mapKey.contains(UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU)) {
							weight = BaseDataUtil.stringConversionDouble(String.valueOf(entityMap.get(mapKey)));
							if (weight>MaxWeight) {
								MaxWeight = weight;
								MaxWtName = mapKey;
							}
						}
					}
					totalL = sectionL*100+90;
				}
				BigDecimal sectionLBd = new BigDecimal(BaseDataUtil.constraintString(totalL));// 四舍五入，保留两位小数
				sectionLBd = sectionLBd.setScale(0, BigDecimal.ROUND_HALF_UP);
				
				String length = BaseDataUtil.constraintString(String.valueOf(sectionLBd));
				String width = BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_WIDTH));
				String height = BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_HEIGHT));
				String ahuSize = length + UtilityConstant.SYS_PUNCTUATION_STAR + width + UtilityConstant.SYS_PUNCTUATION_STAR + height;
				allMap2.put(UtilityConstant.METAHU_AHUSIZE, ahuSize);
				MaxWtName = UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU + MaxWtName.substring(MaxWtName.indexOf(UtilityConstant.METACOMMON_POSTFIX_SECTION)+8, MaxWtName.indexOf(UtilityConstant.METACOMMON_POSTFIX_WEIGHT));
				for(SectionTypeEnum e : SectionTypeEnum.values()) {
					if (MaxWtName.equals(e.getId())) {
						MaxWtName = getIntlString(e.getCnName());
					}
				}
				//				getIntlString(type.getCnName());
				allMap2.put("meta.section.heaviestSection.name", MaxWtName);
				BigDecimal MaxWeightBd = new BigDecimal(BaseDataUtil.constraintString(MaxWeight));//四舍五入，保留两位小数
				MaxWeightBd = MaxWeightBd.setScale(2, BigDecimal.ROUND_HALF_UP);
				allMap2.put("meta.section.heaviestSection.weight", String.valueOf(MaxWeightBd));
				//-----------------ahu数据 特殊处理end-----------------
                
                //送风、回风、冷水盘管、热水盘管 特殊处理
                for (PartPO partPO : partList) {
                    Part part = partPO.getCurrentPart();
                    String key = part.getSectionKey();
                    //风机段
                    if(SectionTypeEnum.TYPE_FAN.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        Map<String, String> fanMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        String airDir = UtilityConstant.METASEXON_AIRDIRECTION;//风向
                        if (fanMap.containsKey(airDir)) {
                            String value = BaseDataUtil.constraintString(String.valueOf(fanMap.get(airDir)));
                            if (value.equals("S")) {//送风map 处理
                                for(int i=0;i<contents.length;i++){
                                    String[] contenti = contents[i];
                                    for(int j=0;j<contenti.length;j++){
                                        String contentj = contenti[j];
                                        if(contentj.indexOf(UtilityConstant.METASEXON_FAN_SFAN)!=-1){
                                            allMap2.put(contentj, String.valueOf(fanMap.get(contentj.replace(".sfan", ".fan"))));
                                        }
                                    }                                   
                                }
                            }
                            if (value.equals("R")) {//回风map 处理
                                for(int i=0;i<contents.length;i++){
                                    String[] contenti = contents[i];
                                    for(int j=0;j<contenti.length;j++){
                                        String contentj = contenti[j];
                                        if(contentj.indexOf(UtilityConstant.METASEXON_FAN_RFAN)!=-1){
                                            allMap2.put(contentj, String.valueOf(fanMap.get(contentj.replace(".rfan", ".fan"))));
                                        }
                                    }                                   
                                }
                            }
                        }
                    }
                    //冷水盘管
                    if(SectionTypeEnum.TYPE_COLD.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> coilMap=UnitUtil.clone(noChangedMap);
                        coilMap = SectionDataConvertUtils.getCoolingCoil(noChangedMap,coilMap, language);
                        allMap2.putAll(coilMap);
                    }
                    //热水盘管
                    if(SectionTypeEnum.TYPE_HEATINGCOIL.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                    	 Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                         Map<String, String> heatMap=UnitUtil.clone(noChangedMap);
                         
                        heatMap = SectionDataConvertUtils.getHeatingcoil(noChangedMap,heatMap, language);
                        allMap2.putAll(heatMap);
                    }
                    //综合过滤段
                    if(SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        
                        Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> combinedFilterMap=UnitUtil.clone(noChangedMap);
                        
                        combinedFilterMap = SectionDataConvertUtils.getCombinedFilter(noChangedMap,combinedFilterMap, language);
                        allMap2.putAll(combinedFilterMap);
                    }
					// 加湿
					Map<String, String> humidifiers = new HashMap<String, String>();
					if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> wetFilmHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						wetFilmHumidifierMap = SectionDataConvertUtils.getARUPPFWetFilmHumidifier(noChangedMap,wetFilmHumidifierMap,
								language);
						humidifiers.putAll(wetFilmHumidifierMap);
					} else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> sprayHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						sprayHumidifierMap = SectionDataConvertUtils.getARUPPFSprayHumidifier(noChangedMap,sprayHumidifierMap,
								language);
						humidifiers.putAll(sprayHumidifierMap);
					} else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> steamHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						steamHumidifierMap = SectionDataConvertUtils.getARUPPFSteamHumidifier(noChangedMap,steamHumidifierMap,
								language);
						humidifiers.putAll(steamHumidifierMap);
					} else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER
							.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> electrodeHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						electrodeHumidifierMap = SectionDataConvertUtils
								.getARUPPFElectrodeHumidifier(noChangedMap,electrodeHumidifierMap, language);
						humidifiers.putAll(electrodeHumidifierMap);
					}
					allMap2.putAll(humidifiers);
                }
                
                
                content.setContent(
                        ValueFormatUtil.transReport(contents, project, unit, allMap2, language, unitType));
            } else if (PDF4ReportUtils.T_BOCHENGPF.equals(pdfClassify)) {// 柏诚工程格式报告
                String[][] contents = ReportContent.getReportContent(REPORT_T_BOCHENG_SPEC);
                content.setContent(
                        ValueFormatUtil.transReport(contents, project, unit, allMap2, language, unitType));
            } else if (PDF4ReportUtils.T_TECHPROJ.equals(pdfClassify)) {// 技术说明(工程版)
            } else if (PDF4ReportUtils.T_TECHSALER.equals(pdfClassify)) {// 技术说明(销售版)
            } else {
                logger.warn("Failed to generate pdf for te reason : Classify is undefined !  >>PDFClassify : "
                        + pdfClassify);
            }
            reportUnitContents.add(content);
        }

        return reportUnitContents;
    }

	/**
	 * 技术说明(工程版)
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Techproj(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();

        writer.setPageEvent(new TechHeader(getIntlString(CARRIER_CORPORATION_FULL_NAME),
                getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)));// 页头黑色边框

        Date d = new Date();

		// setFooter(document, "Page ");
		// addTitle(document, report.getName());
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		// 添加机组信息
		int pageIndex = 0;
        for (Unit unit : reportData.getUnitList()) {
            Phrase phrase = new Phrase();
            Chunk timeChunk = new Chunk(
                    format("  Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
                            DateUtil.getDateTimeString(d, HH_MM)),
                    getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
            phrase.add(timeChunk);
            Chunk wenzi = new Chunk("          " + getIntlString(TECHNICAL_SPECIFICATION),
                    getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
            phrase.add(wenzi);
            phrase.add(Chunk.NEWLINE);// 换行

            String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEADER),
                    reportData.getProject(), unit, null, language, unitType);
            headerContents = SectionContentConvertUtils.getHeader(headerContents, unit, language);
            for (int i = 0; i < headerContents.length; i++) {
                phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
                        getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
            }

            phrase.add(Chunk.NEWLINE);// 换行

            HeaderFooter header = new HeaderFooter(phrase, false);
            header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
            header.setAlignment(Element.ALIGN_LEFT);
            document.setHeader(header);
            if (!document.isOpen()) {
                document.open();// 写入数据之前要打开文档
                addImageLogo(document, language);
                addLine(document);
            }
            if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
                document.newPage();
                addBr(document);
                addLine(document);
            }
			pageIndex++;
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}

			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_AHU_OVERALL), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			ahuContents = SectionContentConvertUtils.getAhuOverall(ahuContents, partition, unit, language, allMap.get(UnitConverter.UNIT));
			document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_AHU_OVERALL));

			//TODO把這段代碼提取到共通方法中，和Word版本的報告公用
			if (EmptyUtil.isNotEmpty(partition)) {
//				String[][] titleContents = arrayMap.get("TechAhu2");
                String[][] ahuboxContents = SectionContentConvertUtils.getAhu(partition, unit, language,
                        allMap.get(UnitConverter.UNIT));
				String[][] ahu2Contents = ValueFormatUtil.transReport(ahuboxContents, reportData.getProject(),
						unit, allMap.get(UnitConverter.UNIT), language, unitType);
				document.add(PDFTableGenerator.generate(ahu2Contents, ReportConstants.REPORT_TECH_AHU_DELIVERY));
			}
			
			//添加ahu非标
			String enable = String.valueOf(allMap.get(UnitConverter.UNIT).get(UtilityConstant.METANS_NS_AHU_ENABLE));//ahu是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), project, unit, allMap.get(UnitConverter.UNIT), language,
						unitType);
				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_AHU.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getAhuNS(nsAHUcontents, partition, unit, language, allMap.get(UnitConverter.UNIT));
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_AHU_NONSTANDARD));
			}

			Paragraph paragraph = getChineseParagraph(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION), CONTENT_NOMAL);
			paragraph.setAlignment(Element.ALIGN_LEFT);
			document.add(paragraph);

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("技术说明报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}", new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOList(unit, partList);
				for (PartPO partPO : orderedPartList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
                    addLine(document);
                    Map<String, String> noChangeMap=allMap.get(UnitConverter.genUnitKey(unit, part));
                    Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
                    PDFTableGen.addSection(document, key, order, noChangeMap,cloneMap,
                            language, unitType, project, unit);
                    order++;
				}
			}

			for (ReportItem item : report.getItems()) {
				if (!item.isOutput()) {
					continue;
				}

				if (ReportConstants.CLASSIFY_TVIEW.equals(item.getClassify())) {
					try {
						addImage3View(document, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), language);
					} catch (Exception e) {
						logger.error("工程版报告》添加三视图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_HANSHI.equals(item.getClassify())) {
					try {
						addImagePsychometric(document, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition),
								language);
					} catch (Exception e) {
						logger.error("工程版报告》添加焓湿图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_FAN.equals(item.getClassify())) {
					try {
                        addImageFan(document, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), allMap,
                                language, unitType, project, unit);
					} catch (Exception e) {
						logger.error("工程版报告》添加风机曲线图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PANEL_SCREENSHOTS.equals(item.getClassify())) {
					try {
						addImagePanelScreenshots(document , unit , partition , language);
					} catch (Exception e) {
						logger.error("工程版报告》添加面板布置图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_UNIT_NAMEPLATE_DATA.equals(item.getClassify())) {
					try {
                        List<Part> parts = new ArrayList<>();
                        for (PartPO partPO : partList) {
                            parts.add(partPO.getCurrentPart());
                        }
                        List<Part> airflowParts = AhuPartitionUtils.getAirflowOrderedPartList(unit, parts);
                        allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
                                String.valueOf(AhuUtil.getAhuSectionCount(unit.getSeries(), airflowParts)));
                        addUnitNameplateData(document, allMap2, project, unitType, unit, partition, language);
					} catch (Exception e) {
						logger.error("工程版报告》机组铭牌数据》报错",e);
					}
				}
				List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
				int connectionNum=ahuPartitionList.size()-1;
				if (ReportConstants.CLASSIFY_SECTION_CONNECTION_LIST.equals(item.getClassify())
						&& connectionNum > 0) {//当只有一个分段时，不需要打印段连接清单
					try {
						for(int i=1;i<=connectionNum;i++) {
						//生成用户特殊段连接
						String paneltype=UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
						if(i==1) {
						paneltype = unit.getPaneltype();
						if (EmptyUtil.isEmpty(paneltype)) {//paneltype为空的时候默认“标准”
							paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
						}
						}
						addSectionConnectionList(paneltype,document, unit, language, allMap.get(UnitConverter.UNIT));
						}
					} catch (Exception e) {
						logger.error("工程版报告》段连接清单》报错", e);
					}
				}
				if (ReportConstants.CLASSIFY_PRODUCT_PACKING_LIST.equals(item.getClassify())) {
					boolean connectionFlag = false;
					if (ahuPartitionList.size() > 1) {
						connectionFlag = true;
					} else {
						connectionFlag = false;
					}
					try {
						addProductPackingList(document, allMap,allMap2, project, unitType, unit, partition, language, connectionNum,partList);
					} catch (Exception e) {
						logger.error("工程版报告》产品装箱单》报错",e);
					}
				}
			}
		}
		close(document, writer, fos);
	}
	
	/**
	 * 产品装箱单
	 * @param document
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addProductPackingList(Document document, Map<String, Map<String, String>> ahuMap, Map<String, String> allMap, Project project,
            UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language, int connectionNum,List<PartPO> partList)
            throws IOException, DocumentException {
        logger.info("开始添加产品装箱单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
        document.newPage();
        addLine(document);
        addBr(document);

        String productPackingListName = SectionMetaUtils.getMetaAHUKey(KEY_PRODUCT_PACKING_LIST_NAME);
        String serial = SectionMetaUtils.getMetaAHUKey(KEY_SERIAL);
        allMap.put(productPackingListName,
                BaseDataUtil.constraintString(allMap.get(serial) + getIntlString(I18NConstants.PRODUCT_PACKING_LIST)));
        String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(
                AHUContext.isExportVersion() ? REPORT_TECH_PRODUCTPACKINGLIST_EXPORT : REPORT_TECH_PRODUCTPACKINGLIST),
                project, unit, allMap, language, unitType);
       
        int PTMNum=0;
        List<Part> parts=unit.getParts();
        for(PartPO part:partList) {
        	
        	if(SectionTypeEnum.TYPE_FAN.getId().equals(part.getCurrentPart().getSectionKey())) {
        		Map<String, String> fanMap= ahuMap.get(UnitConverter.genUnitKey(unit, part.getCurrentPart()));
        		if(Boolean.valueOf(String.valueOf(fanMap.get(METASEXON_FAN_PTC)))) {
        			PTMNum++;
        		}
        	}
        }
        if (!AHUContext.isExportVersion()) {//非出口版
            if(PTMNum==0) {
            	for(int i=0;i<ahuContents[0].length;i++) {
    				ahuContents[5][i] = UtilityConstant.SYS_BLANK;
    			}
            }else {
            	ahuContents[5][3]=String.valueOf(PTMNum);
            }
            
            if (connectionNum==0) {//没有分段的时候，去掉段连接信息
    			for(int i=0;i<ahuContents[0].length;i++) {
    				ahuContents[6][i] = UtilityConstant.SYS_BLANK;
    			}			
    		}else if (PTMNum==0) {//无ptc的情况
    			for(int i=0;i<ahuContents[6].length;i++) {//第7行数据移到第6行（段连接清单数据上移1行）
    				ahuContents[5][i] = ahuContents[6][i];
    				ahuContents[6][i] = UtilityConstant.SYS_BLANK;
    			}
    			ahuContents[5][0] = UtilityConstant.SYS_STRING_NUMBER_4;
    			ahuContents[5][3]=String.valueOf(connectionNum);
    		}else{
    			ahuContents[6][3]=String.valueOf(connectionNum);
    		}
		}else {//出口版不考虑ptc          
            if (connectionNum==0) {//没有分段的时候，去掉段连接信息
    			for(int i=0;i<ahuContents[0].length;i++) {
    				ahuContents[5][i] = UtilityConstant.SYS_BLANK;
    			}			
    		}else {
    			ahuContents[5][3]=String.valueOf(connectionNum);
    		}
		}

        document.add(PDFTableGenerator.generate(ahuContents, REPORT_TECH_PRODUCTPACKINGLIST));
    }
	
	/**
	 * 段连接清单
	 * @param document
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
	private static void addSectionConnectionList(String paneltype,Document document, Unit unit, LanguageEnum language, Map<String, String> ahuMap) throws IOException, DocumentException {
		logger.info("开始添加段连接清单：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String unitSeries = AhuUtil.getUnitSeries(unit.getSeries());
		String unitModel = unit.getSeries();
		
		String[][] sectionConnectionListC = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel, paneltype);
		sectionConnectionListC = SectionContentConvertUtils.getConnectionList(sectionConnectionListC, ahuMap, unitSeries, paneltype);
		document.newPage();
		addLine(document);
		
		String connectionStr=getIntlString(SECTION_CONNECTION_LIST);
		if(UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONFOREPART);
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONPOSITIVE);
			
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONVERTICAL);
			
		}
		Paragraph paragraph = new Paragraph(getIntlString(UNIT_MODEL_COLON) + unit.getSeries() +" "+ connectionStr
				, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		addBr(document);
		document.add(PDFTableGenerator.generate(sectionConnectionListC, REPORT_TECH_SECTION_CONNECTION_LIST
				+ UtilityConstant.SYS_PUNCTUATION_DOT + paneltype.toLowerCase()));
	}

	/**
	 * 机组铭牌数据
	 * @param document
	 * @param arrayMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addUnitNameplateData(Document document, Map<String, String> allMap, Project project,
            UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language)
            throws IOException, DocumentException {
        logger.info("开始添加机组铭牌数据：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		document.newPage();
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(UNIT_NAMEPLATE_DATA) , getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		addBr(document);

		String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA), project, unit,
				allMap, language, unitType);
		ahuContents = SectionContentConvertUtils.getUnitNamePlateData(ahuContents, allMap, language);
		document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA));


//		String[][] titleContents = arrayMap.get("TechUnitnameplatedata2");
		String[][] titleContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA_SHAPE), project, unit,
				allMap, language, unitType);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			int i = 2;
			for (AhuPartition ap : ahuPartitionList) {
                double partitionWeight = 0;
                if (AhuPartitionUtils.hasEndFacePanel(ap, ahuPartitionList)) {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), true);
                } else {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), false);
                }
				if(unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
					String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos()+1),
							UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()), UtilityConstant.SYS_BLANK + (ap.getWidth()*100 + ap.getCasingWidth()), 
//							UtilityConstant.SYS_BLANK + (ap.getHeight()*100 + ap.getCasingWidth()),
							SectionContentConvertUtils.wrapHeight(allMap, partition, unit, ap),
							UtilityConstant.SYS_BLANK + BaseDataUtil.decimalConvert(partitionWeight, 1) };
					partitionContents[i] = apArray;
					i++;
				}else {
					String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos()+1),
							UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()), UtilityConstant.SYS_BLANK + ap.getWidth(),
//							UtilityConstant.SYS_BLANK + ap.getHeight(),
							SectionContentConvertUtils.wrapHeight(allMap, partition, unit, ap),
							UtilityConstant.SYS_BLANK + BaseDataUtil.decimalConvert(partitionWeight, 1) };
					partitionContents[i] = apArray;
					i++;
				}
			}
			document.add(PDFTableGenerator.generate(partitionContents, ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA_SHAPE));
		}
	}


	/**
	 * 技术说明(销售版)
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Techsaler(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();

        writer.setPageEvent(new TechHeader(getIntlString(CARRIER_CORPORATION_FULL_NAME),
                getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)));
        Date d = new Date();

		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		// 添加机组信息
		int pageIndex = 0;
		for (Unit unit : reportData.getUnitList()) {
            Phrase phrase = new Phrase();
            
            Chunk timeChunk = new Chunk(
                    format("Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
                            DateUtil.getDateTimeString(d, HH_MM)),
                    getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
            phrase.add(timeChunk);

            Chunk wenzi = new Chunk("                    " + getIntlString(TECHNICAL_SPECIFICATION),
                    getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
            phrase.add(wenzi);
            phrase.add(Chunk.NEWLINE);

            String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEADER),
                    reportData.getProject(), unit, null, language, unitType);
            for (int i = 0; i < headerContents.length; i++) {
                phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
                        getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
            }
            phrase.add(Chunk.NEWLINE);

            HeaderFooter header = new HeaderFooter(phrase, false);
            header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
            header.setAlignment(Element.ALIGN_LEFT);
            document.setHeader(header);
            if (!document.isOpen()) {
                document.open();// 写入数据之前要打开文档
                addTitle(document, report.getName());
            }
			if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
				document.newPage();
			}
			pageIndex++;

			logger.info("开始添加技术说明(销售版)：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			addLine(document);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_AHU_OVERALL), reportData.getProject(),
					unit, allMap.get(UnitConverter.UNIT), language, unitType);
			document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_AHU_OVERALL));

			Paragraph paragraph = getChineseParagraph(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION), CONTENT_NOMAL);
			paragraph.setAlignment(Element.ALIGN_LEFT);
			document.add(paragraph);

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("技术说明报告-AHU组编码信息不合法，报告忽略生成对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOList(unit, partList);
                for (PartPO partPO : orderedPartList) {
                	if(partPO==null) {
                		continue;
                	}
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					addLine(document);
					Map<String, String> noChangeMap=allMap.get(UnitConverter.genUnitKey(unit, part));
	                Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
                    PDFTableGen.addSection4Saler(document, key, order, noChangeMap,cloneMap,
                            language, unitType, project, unit);
                    order++;
				}
			}
			try {
				addImage3View(document, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), language);
			} catch (Exception e) {
				logger.error("销售版报告》添加三视图》报错",e);
			}

			try {
				addImagePsychometric(document, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), language);
			} catch (Exception e) {
				logger.error("销售版报告》添加焓湿图》报错",e);
			}
		}
		close(document, writer, fos);
	}

	/**
	 * 添加段落标题
	 * 
	 * @param document
	 * @param title
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addTitle(Document document, String title) throws DocumentException, IOException {
		Paragraph titleParagraph = getChineseParagraph(title, TITLE_BIGGER);
		titleParagraph.setSpacingAfter(10f);
		titleParagraph.setAlignment(Element.ALIGN_CENTER);
		document.add(titleParagraph);
	}

	/**
	 * 获取中文字体
	 * 
	 * @param fontType
	 *            字体类型
	 * @param fontSize
	 *            字体大小
	 * 
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Font getBaseFontChinese(String fontType, Integer fontSize) throws DocumentException, IOException {
		BaseFont bfChinese = BaseFont.createFont(fontType, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		return new Font(bfChinese, fontSize, Font.NORMAL); // 设置为中文
	}

	/**
	 * 获取中文字体-加粗
	 * 
	 * @param fontType
	 *            字体类型
	 * @param fontSize
	 *            字体大小
	 * 
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Font getBaseFontChineseBold(String fontType, Integer fontSize) throws DocumentException, IOException {
		BaseFont bfChinese = BaseFont.createFont(fontType, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		return new Font(bfChinese, fontSize, Font.BOLD); // 设置为中文
	}

	/**
	 * 中文转换工具
	 * 
	 * @param str
	 * @param fontSize
	 *            字体大小
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Paragraph getChineseParagraph(String str, Integer fontSize) throws DocumentException, IOException {
		Paragraph pragraph = new Paragraph(str, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, fontSize));
		return pragraph;
	}

	/**
	 * 中文转换工具
	 * 
	 * @param str
	 * @param fontSize
	 *            字体大小
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Paragraph getChineseBoldParagraph(String str, Integer fontSize)
			throws DocumentException, IOException {
		Paragraph pragraph = new Paragraph(str, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, fontSize));
		return pragraph;
	}

	/**
	 * 设置页眉
	 * 
	 * @param document
	 * @param headerStr
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void setHeader(Document document, String headerStr) throws DocumentException, IOException {
		Paragraph para = new Paragraph(headerStr, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
		HeaderFooter header = new HeaderFooter(para, false);
		header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
		header.setAlignment(1);
		document.setHeader(header);
	}

	/**
	 * 添加品牌图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImageLogo(Document document, LanguageEnum locale) throws DocumentException, IOException {
		String aeroPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;
		String carrierPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;

		if (LanguageEnum.Chinese.equals(locale)) {
			aeroPath += "Aero_ch.png";
			carrierPath += "CAR_LOGO_ch.png";
		} else {
			aeroPath += "Aero_en.bmp";
			carrierPath += "CAR_LOGO_en.png";
		}

		Image imgAero = Image.getInstance(aeroPath);
		imgAero.scalePercent(4);
		if (!LanguageEnum.Chinese.equals(locale)) {
			imgAero.scalePercent(20);
		}
		float x = 36;
		float y = (PageSize.A4.getHeight() - imgAero.getScaledHeight()) - 150;
		imgAero.setAbsolutePosition(x, y);

		Image imgCarrier = Image.getInstance(carrierPath);
		imgCarrier.scalePercent(25);
		float x1 = PageSize.A4.getWidth() - imgCarrier.getScaledWidth() - 36;
		float y1 = (PageSize.A4.getHeight() - imgCarrier.getScaledHeight()) - 135;
		imgCarrier.setAbsolutePosition(x1, y1);

		Paragraph info = new Paragraph();
		info.add(new Chunk(UtilityConstant.SYS_BLANK));
		info.setSpacingAfter(90f);// 设置段落下空白，放置图片
		document.add(info);
		document.add(imgAero);
		document.add(imgCarrier);
	}

	/**
	 * 设置页脚
	 * 
	 * @param document
	 * @param footerStr
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void setFooter(Document document, String footerStr) throws DocumentException, IOException {
		HeaderFooter footer = new HeaderFooter(
				new Phrase(footerStr, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, FOOTER_TITLE)), true);
		footer.setBorder(Rectangle.NO_BORDER);
		/**
		 * 0是靠左 1是居中 2是居右
		 */
		footer.setAlignment(2);
		document.setFooter(footer);
	}

	/**
	 * 添加段落
	 * 
	 * @param document
	 * @param title
	 * @param content
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addParagraph(Document document, String title, String content, Integer alignment)
			throws DocumentException, IOException {
		// 定义段落
		Paragraph paragraph = getChineseParagraph(title, TABLE_TITLE_BIGGER);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);

		Paragraph paragraphContent = getChineseParagraph(content, CONTENT_NOMAL);
		// Phrase phrase = new Phrase();
		// // 插入十条文本块到段落中
		// int i = 0;
		// for (i = 0; i < 10; i++) {
		// Chunk chunk = new Chunk(content + i + ".\n ");
		// phrase.add(chunk);
		// }
		// paragraphContent.add(phrase);
		// 设置一个段落前后的间距
		paragraphContent.setSpacingAfter(50);
		paragraphContent.setSpacingBefore(50);
		paragraphContent.setAlignment(alignment);

		// 添加段落
		document.add(paragraphContent);
	}

	/**
	 * 添加直线
	 * @param document
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addLine(Document document) throws DocumentException, IOException {
		Paragraph line = new Paragraph();
		line.add(new Chunk(new LineSeparator()));
		line.setSpacingAfter(8f);
		document.add(line);
	}

	/**
	 * 添加换行
	 * @param document
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addBr(Document document) throws DocumentException, IOException {
		Paragraph line = new Paragraph();
		line.add(new Chunk(UtilityConstant.SYS_PUNCTUATION_NEWLINE));
		document.add(line);
	}

	/**
	 * 添加表格名称
	 * @param document
	 * @param titleStr
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addTableTitle(Document document, String titleStr) throws DocumentException, IOException {
		Paragraph title = getChineseParagraph(titleStr, TABLE_TITLE_BIGGER);
		title.setSpacingAfter(20);
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
	}

	/**
	 * 添加工程格式报告段名称
	 * @param document
	 * @param partName
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addPartName(Document document, String partName) throws DocumentException, IOException {
		Paragraph paragraph = new Paragraph(partName, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setSpacingAfter(10f);
		document.add(paragraph);
	}

	/**
	 * 修正页眉格式
	 * 
	 * @param strs
	 * @return
	 */

	private static String fixHeaderLine(String[] strs) {
		StringBuffer sb = new StringBuffer();
		int leftHalf = String_length(strs[0]) + String_length(strs[1]) + String_length(strs[2]);

		sb.append("\n  ").append(strs[0]).append(strs[1]).append(strs[2]);

		if (leftHalf < 60) {
			for (int i = leftHalf; i < 60; i++) {
				sb.append(" ");
			}
			sb.append(strs[3]).append(strs[4]).append(strs[5]);
		} else {
			sb.append(UtilityConstant.SYS_PUNCTUATION_NEWLINE).append(strs[3]).append(strs[4]).append(strs[5]);
		}
		return sb.toString();
	}

	/**
	 * 判断字段真实长度的实例(中文2个字符,英文1个字符)
	 * 
	 * @param value
	 * @return
	 */
	public static int String_length(String value) {
		int valueLength = 0;
		String chinese = "[\u4e00-\u9fa5]";
		for (int i = 0; i < value.length(); i++) {
			String temp = value.substring(i, i + 1);
			if (temp.matches(chinese)) {
				valueLength += 2;
			} else {
				valueLength += 1;
			}
		}
		return valueLength;
	}

	/**
	 * 关闭文件相关
	 * 
	 * @param document
	 * @param writer
	 * @param fos
	 * @throws IOException
	 */
	private static void close(Document document, PdfWriter writer, FileOutputStream fos) throws IOException {
		if (EmptyUtil.isNotEmpty(document)) {
			document.close();
		}
		if (EmptyUtil.isNotEmpty(writer)) {
			writer.close();
		}
		if (EmptyUtil.isNotEmpty(fos)) {
			fos.close();
		}
	}

	/**
	 * 添加三视图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImage3View(Document document, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成三视图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		String destPath = String.format(
				SysConstants.CAD_DIR + File.separator + "%s" + File.separator + "%s" + File.separator + "%s.bmp", param.getPid(),
				param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);
		Image img = Image.getInstance(SysConstants.ASSERT_DIR + File.separator + destPath);
		img.scalePercent(45);
		float x = (PageSize.A4.getWidth() - img.getScaledWidth()) / 2;
		float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
		img.setAbsolutePosition(x, y);
		document.newPage();
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(AHU_TRIPLE_VIEW_SEQUENCE_NUMBER) + param.getUnitNo(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		document.add(img);
	}

	/**
	 * 添加面板布置图
	 * @param document
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	private static void addImagePanelScreenshots(Document document, Unit unit, Partition partition, LanguageEnum language) throws DocumentException, IOException {
		logger.info("开始添加面板切割布置图：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		List<AhuPartition> partitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
		for(int i=0;i<partitions.size();i++) {
			String destPath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unit.getUnitid()) + File.separator + partition.getPartitionid()+ (i+1) + ".png";
			Image img = null;
			try {
				img = Image.getInstance(destPath);
			} catch (Exception e) {
//				e.printStackTrace();
				continue;
			}
			if ((PageSize.A4.getHeight() - img.getScaledHeight())<0) {
				float pageheight = PageSize.A4.getHeight();
				double scale = ((pageheight-pageheight*0.55)*100)/pageheight;
				BigDecimal scaleBd = new BigDecimal(scale);//四舍五入，保留两位小数
				scaleBd = scaleBd.setScale(2, BigDecimal.ROUND_HALF_UP);
				img.scalePercent(Float.parseFloat(String.valueOf(scaleBd)));
			}else {
				img.scalePercent(38);				
			}
			
			float x = (PageSize.A4.getWidth() - img.getScaledWidth()) / 2;
			float y = ((PageSize.A4.getHeight() - img.getScaledHeight()) / 2) - (float)(img.getScaledHeight()*0.12);
			img.setAbsolutePosition(x, y);
			document.newPage();
			addLine(document);
			Paragraph paragraph = new Paragraph(getIntlString(AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER) + unit.getUnitNo() + "-" + (i+1),
					getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
			paragraph.setAlignment(Element.ALIGN_LEFT);
			document.add(paragraph);
			document.add(img);
		}
		
	}

	/**
	 * 添加焓湿图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImagePsychometric(Document document, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成焓湿图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		Iterator<PartParam> it = param.getPartParams().iterator();

		List<PsyCalBean> datas = PsychometricDrawer.drawPsy(it);

		String path = PsychometricDrawer.genPsychometric(param.getPid(), param.getUnitid(), datas);
		
		Image img = Image.getInstance(SysConstants.ASSERT_DIR + path.substring(0,path.indexOf("?")));
		img.scalePercent(13);
		float x = (PageSize.A4.getWidth() - img.getScaledWidth()) / 2;
		float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
		img.setAbsolutePosition(x, y);
		document.newPage();
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(AHU_PSY_CHART_SEQUENCE_NUMBER) + param.getUnitNo(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		document.add(img);

	}

	/**
	 * 添加风机曲线图
	 * 
	 * @param document
	 * @param param
	 * @param unit
	 * @param project
	 * @param unitType
	 * @param language
	 * @param allMap
	 * @param arrayMap
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void addImageFan(Document document, AhuParam param, Map<String, Map<String, String>> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws DocumentException, IOException {
		logger.info("开始生成风机曲线图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		List<PartParam> partParams = param.getPartParams();
		for (PartParam partParam : partParams) {
			if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
				Map<String, Object> params = partParam.getParams();
				String path = String.valueOf(params.get(UtilityConstant.METASEXON_FAN_CURVE));
				path = path.replace("files/", UtilityConstant.SYS_BLANK);// 页面使用的为files 后端路径去掉files
				if (EmptyUtil.isNotEmpty(path)) {
					Image img = Image.getInstance(SysConstants.ASSERT_DIR + path);
					img.scalePercent(83);
					float x = (PageSize.A4.getWidth() - img.getScaledWidth()) - 60;
					float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
					img.setAbsolutePosition(x, y);
					document.newPage();
					addLine(document);
					Paragraph paragraph = new Paragraph(getIntlString(FAN_CURVE),
							getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
					paragraph.setAlignment(Element.ALIGN_LEFT);
					document.add(paragraph);

					String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_CURVE), project, unit,
							allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
									partParam.getKey())),
							language, unitType);
					
					Map<String, String> allMap1 = new HashMap<String, String>();
					for (String key : allMap.keySet()) {
						if(key.indexOf(UtilityConstant.METASEXON_FAN)>0) {
							allMap1 = (Map<String, String>)allMap.get(key);
						}
					}
					// 封装风机段特殊字段
					contents = SectionContentConvertUtils.getFan4(contents, allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
							partParam.getKey())), language);
					
					addBr(document);
					document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FAN_CURVE));
					document.add(img);
				}
			} else {
				continue;
			}
		}
	}

	private static String format(String pattern, Object... arguments) {
		return MessageFormat.format(pattern, arguments);
	}

}
