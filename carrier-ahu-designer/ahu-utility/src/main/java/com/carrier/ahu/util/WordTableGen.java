package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getFontFamily;
import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_CURVE;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.vo.SysConstants;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WordTableGen {
	
	private static Map<String, int[]> sectionConnectionWidth = new HashMap<>();
	
	static {
		sectionConnectionWidth.put(UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD,
				new int[] { 8, 10, 18, 10, 10, 8, 8, 10, 10, 8 });
		sectionConnectionWidth.put(UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART,
				new int[] { 4, 8, 12, 5, 5, 8, 8, 8, 4, 4, 8, 8, 3, 8, 3, 2, 2 });
		sectionConnectionWidth.put(UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE,
				new int[] { 4, 8, 12, 5, 5, 8, 8, 8, 4, 4, 8, 8, 3, 8, 3, 2, 2 });
		sectionConnectionWidth.put(UtilityConstant.JSON_UNIT_PANELTYPE_VERTICAL,
				new int[] { 4, 8, 12, 5, 5, 8, 8, 8, 4, 4, 8, 8, 3, 8, 3, 2, 2 });
	}

	
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Mix]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechMix(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 16, 12, 15, 10, 14, 7, 26 };
		
		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(true,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Mix2]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechMix2(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 18, 7, 11, 16, 7, 11, 14, 8, 10 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(false,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Filter]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechFilter(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 21, 11, 20, 17, 11, 21 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(true,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Combined]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechCombined(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 21, 9, 18, 21, 8, 23 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(true,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[CoolingCoil]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechCoolingCoil(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 22, 7, 21, 2, 24, 6, 18 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(true,table);//顶部边框是否存在
		
		XWPFTableRow theRow0 = table.getRow(0);
		XWPFTableCell cell00 = theRow0.getCell(0);
		CTTcPr cellPr00 = cell00.getCTTc().addNewTcPr();
		int cellWidth00 = getCellWidth(widthlist,0);
		cellPr00.addNewTcW().setW(BigInteger.valueOf(cellWidth00));	
		setCellText(cell00, false, contents[0][0], ParagraphAlignment.LEFT);
		
		XWPFTableCell cell01 = theRow0.getCell(1);
		CTTcPr cellPr01 = cell01.getCTTc().addNewTcPr();
		int cellWidth01 = getCellWidth(widthlist,1);
		cellPr01.addNewTcW().setW(BigInteger.valueOf(cellWidth01));	
		setCellText(cell01, false, contents[0][1], ParagraphAlignment.LEFT);
		mergeCellsHorizontal(table, 0, 1, 6);
		
		for (int i = 1; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[CoolingCoil1]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechCoolingCoil1(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 20, 10, 15, 10, 20, 10, 15 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(false,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Hea]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechHea(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 23, 10, 12, 25, 7, 24 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(true,table);//顶部边框是否存在
		
		XWPFTableRow theRow0 = table.getRow(0);
		XWPFTableCell cell00 = theRow0.getCell(0);
		CTTcPr cellPr00 = cell00.getCTTc().addNewTcPr();
		int cellWidth00 = getCellWidth(widthlist,0);
		cellPr00.addNewTcW().setW(BigInteger.valueOf(cellWidth00));	
		setCellText(cell00, false, contents[0][0], ParagraphAlignment.LEFT);
		
		XWPFTableCell cell01 = theRow0.getCell(1);
		CTTcPr cellPr01 = cell01.getCTTc().addNewTcPr();
		int cellWidth01 = getCellWidth(widthlist,1);
		cellPr01.addNewTcW().setW(BigInteger.valueOf(cellWidth01));	
		setCellText(cell01, false, contents[0][1], ParagraphAlignment.LEFT);
		mergeCellsHorizontal(table, 0, 1, 5);
		
		for (int i = 1; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Ste]
	 * @param contents
	 * @param doc
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechSte(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 18, 12, 20, 19, 11, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Elec2]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechElec2(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 18, 12, 20, 17, 13, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Ste1]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechSte1(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 18, 12, 20, 17, 13, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Wet]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechWet(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 18, 12, 20, 15, 15, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Spr]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechSpr(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] {20, 9, 22, 19, 9, 22 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Fan1]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechFan1(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 16, 12, 11, 3, 3, 19, 8, 28 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				
				/*if(i==3 || i==6 || i==7|| i==8|| i==9) {
					if(j==1|| j==2|| j==3) {
						continue;
					}else {
						addCellSpan(table, contents[i][j], 1, 4, "F", PdfPCell.ALIGN_LEFT,CONTENT_NOMAL_1);
						continue;	
					}
				}else if(i==4 || i==5) {
					if(j==1 || j==2) {
						continue;
					}else if(j==3) {
						addCellSpan(table, contents[i][j], 1, 3, "F", PdfPCell.ALIGN_LEFT,CONTENT_NOMAL_1);
						continue;	
					}
				}*/
				
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Fan2]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechFan2(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 14, 9, 8, 9, 10, 10, 10, 10, 10, 10 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Fan3]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechFan3(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}

    public static void genTechFan5(String[][] contents, XWPFDocument doc, boolean tabletop)
            throws DocumentException, IOException {
        int rows = contents.length;
        int cols = contents[0].length;
        int[] widthlist = new int[] { 100 };

        XWPFTable table = doc.createTable(rows, cols);
        setTableProp(tabletop, table);

        for (int i = 0; i < rows; i++) {
            XWPFTableRow theRow = table.getRow(i);
            for (int j = 0; j < cols; j++) {
                XWPFTableCell cell = theRow.getCell(j);
                CTTcPr cellPr = cell.getCTTc().addNewTcPr();
                // 设置宽度
                int cellWidth = getCellWidth(widthlist, j);
                cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
                setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
            }
        }
    }
    
    public static void genTechFan6(String[][] contents, XWPFDocument doc, boolean tabletop)
            throws DocumentException, IOException {
        int rows = contents.length;
        int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 85 };

        XWPFTable table = doc.createTable(rows, cols);
        setTableProp(tabletop, table);

        for (int i = 0; i < rows; i++) {
            XWPFTableRow theRow = table.getRow(i);
            for (int j = 0; j < cols; j++) {
            	if (i==0 && j==0) {
					String picPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR + "AHRI_LOGO_ch.png";
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					// 设置宽度
					int cellWidth = getCellWidth(widthlist, j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
					setCellPic(cell, ParagraphAlignment.LEFT, picPath, "AHRI_LOGO_ch.png");
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
				}else {
	                XWPFTableCell cell = theRow.getCell(j);
	                CTTcPr cellPr = cell.getCTTc().addNewTcPr();
	                // 设置宽度
	                int cellWidth = getCellWidth(widthlist, j);
	                cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
	                setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
	                cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
				}
            }
        }
    }

	/**
	 * 创建技术说明(工程版)报告 - 表格[Comb]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechComb(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 17, 8, 25, 17, 7, 26 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Comb1]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechComb1(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 16, 7, 11, 16, 7, 11, 14, 8, 10 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Comb2]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechComb2(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 16, 7, 11, 16, 7, 11, 14, 8, 10 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Atte]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechAtte(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 16, 9, 16, 7, 7, 13, 9, 9, 7, 7 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Dis]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechDis(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 15, 20, 15, 9, 26 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Acce]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechAcce(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 15, 20, 15, 8, 27 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Hepa]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechHepa(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 21, 11, 20, 17, 11, 21 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Elec1]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechElec1(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 20, 11, 20, 19, 11, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechElec(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 15, 20, 15, 15, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Ctr]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechCtr(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 15, 15, 20, 15, 15, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Hepa]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechWhee(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 21, 9, 18, 21, 11, 20 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[Whee1]
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechWhee1(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 50, 50 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 风机图左简介表格
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genFanLeftTable(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 50, 50 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在

        CTTblWidth tblWidth = table.getCTTbl().getTblPr().getTblW();
        tblWidth.setW(tblWidth.getW().multiply(BigInteger.valueOf(30)).divide(BigInteger.valueOf(100)));

		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
    public static void genFanCurveTable(XWPFDocument doc, String[][] contents, InputStream fanCurveImg,
            boolean tabletop) throws DocumentException, IOException, InvalidFormatException {
        // add title
        XWPFParagraph paragraph = doc.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setFontFamily(getFontFamily());
        run.setBold(true);
        run.setFontSize(11);
        run.setText(getIntlString(FAN_CURVE));

        XWPFTable table = doc.createTable(1, 2);
        setTableProp(tabletop, table);

        // add fan curve table
        XWPFTableRow row = table.getRow(0);
        XWPFTableCell cell = row.getCell(0);
        cell.setVerticalAlignment(XWPFVertAlign.CENTER);
        paragraph = cell.getParagraphs().get(0);
        XmlCursor cursor = paragraph.getCTP().newCursor();
        XWPFTable fanCurve = cell.insertNewTbl(cursor);
        genFanLeftTable(contents, fanCurve, tabletop);

        // add fan curve image
        cell = row.getCell(1);
        paragraph = cell.getParagraphs().get(0);
        run = paragraph.createRun();
        run.addPicture(fanCurveImg, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK,
                Units.toEMU((int) (PageSize.A4.getWidth() * 0.53)),
                Units.toEMU((int) (PageSize.A4.getHeight() * 0.53)));

        addBr(doc);
    }

    public static void genFanLeftTable(String[][] contents, XWPFTable table, boolean tabletop)
            throws DocumentException, IOException {
        int rows = contents.length;
        int cols = contents[0].length;
        int[] widthlist = new int[] { 50, 50 };
        addBorder(table);

        CTTblWidth tblWidth = table.getCTTbl().getTblPr().getTblW();
        tblWidth.setW(tblWidth.getW().multiply(BigInteger.valueOf(40)).divide(BigInteger.valueOf(100)));

        for (int i = 0; i < rows; i++) {
            if (EmptyUtil.isEmpty(contents[i][0])) { // skip empty line
                continue;
            }
            XWPFTableRow row = table.createRow();
            for (int j = 0; j < cols; j++) {
                XWPFTableCell cell = row.getCell(j);
                if (cell == null) {
                    cell = row.addNewTableCell();
                }
                CTTcPr cellPr = cell.getCTTc().addNewTcPr();
                // 设置宽度
                int cellWidth = getCellWidth(widthlist, j);
                cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth / 2));
                setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
            }
        }
    }

	/**
	 * 创建技术说明(工程版)报告 - 表格[TechUnitnameplatedata]  机组铭牌数据
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechUnitnameplatedata(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 20, 20, 10, 20, 20, 10 };

		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 创建技术说明(工程版)报告 - 表格[TechUnitnameplatedata2] 机组铭牌数据分段信息
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechUnitnameplatedata2(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 25, 15, 15, 15, 15, 15 };
		
		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(tabletop,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}
	
	/**
	 * 段连接清单
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genSectionConnectionList(String[][] contents, XWPFDocument doc, boolean tabletop, String paneltype) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
//		int[] widthlist = new int[] { 8, 10, 18, 10, 10, 8, 8, 10, 10, 8 };
		int[] widthlist = sectionConnectionWidth.get(paneltype);

		XWPFTable table = doc.createTable(rows, cols);
		addBorder(table);
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
		addBr(doc);	
	}

    private static void addBorder(XWPFTable table) {
        CTTblPr tblPr = table.getCTTbl().getTblPr() == null ? table.getCTTbl().addNewTblPr()
                : table.getCTTbl().getTblPr();
        CTTblBorders borders = tblPr.addNewTblBorders();
        CTBorder hBorder = borders.addNewInsideH();
        hBorder.setVal(STBorder.SINGLE);
        hBorder.setSz(new BigInteger("1"));
        hBorder.setColor("000000");

        CTBorder vBorder = borders.addNewInsideV();
        vBorder.setVal(STBorder.SINGLE);
        vBorder.setSz(new BigInteger("1"));
        vBorder.setColor("000000");

        CTBorder lBorder = borders.addNewLeft();
        lBorder.setVal(STBorder.SINGLE);
        lBorder.setSz(new BigInteger("1"));
        lBorder.setColor("000000");

        CTBorder rBorder = borders.addNewRight();
        rBorder.setVal(STBorder.SINGLE);
        rBorder.setSz(new BigInteger("1"));
        rBorder.setColor("000000");

        CTBorder tBorder = borders.addNewTop();
        tBorder.setVal(STBorder.SINGLE);
        tBorder.setSz(new BigInteger("1"));
        tBorder.setColor("000000");

        CTBorder bBorder = borders.addNewBottom();
        bBorder.setVal(STBorder.SINGLE);
        bBorder.setSz(new BigInteger("1"));
        bBorder.setColor("000000");

        CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
                .addNewTblW();
        CTJc cTJc = tblPr.addNewJc();
        cTJc.setVal(STJc.Enum.forString("center"));
        tblWidth.setW(new BigInteger("10000"));
        tblWidth.setType(STTblWidth.DXA);
    }

	/**
	 * 产品装箱单
	 * @param contents
	 * @param doc
	 * @param tabletop
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechProductPackingListdata(String[][] contents, XWPFDocument doc, boolean tabletop) throws DocumentException, IOException {
		int rows = contents.length;
		int cols = contents[0].length;
		int[] widthlist = new int[] { 14, 30, 13, 13, 30 };

		XWPFTable table = doc.createTable(rows, cols);
//		setTableProp(tabletop,table);//顶部边框是否存在
		
//		for (int i = 0; i < rows; i++) {
//			XWPFTableRow theRow = table.getRow(i);
//			for (int j = 0; j < cols; j++) {
//				XWPFTableCell cell = theRow.getCell(j);
//				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
//				//设置宽度
//				int cellWidth = getCellWidth(widthlist,j);
//				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
//				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
//			}
//		}
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				if (i==0 && j==0) {
					String picPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR + "CAR_LOGO_ch.png";
//					Image img = Image.getInstance(picPath);
//					img.scalePercent(10f);

					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					//设置宽度
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
					setCellPic(cell, ParagraphAlignment.CENTER, picPath,"CAR_LOGO_ch.png");
				}else if (i==0 && j==1) {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));	
					setCellText(cell, false, contents[i][j], ParagraphAlignment.CENTER);
					mergeCellsHorizontal(table, i, 1, 4);
				}else if (i==0 && j>=2) {
					continue;
				}else if (i>7 && i<13) {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					//设置宽度
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
					setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
				}else if (i==14 && j==0) {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));	
					setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
					mergeCellsVertically(table, j, i, i+1);
				}else if (i==14 && j==1) {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));	
					setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
					mergeCellsHorizontal(table, i, 1, 4);
				}else if (i==14 && j>=2) {
					continue;
				}else if (i==15 && j==0) {
					continue;
				}else if (i==15 && j==1) {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));	
					setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
					mergeCellsHorizontal(table, i, 1, 4);
				}else if (i==15 && j>=2) {
					continue;
				}else {
					XWPFTableCell cell = theRow.getCell(j);
					CTTcPr cellPr = cell.getCTTc().addNewTcPr();
					//设置宽度
					int cellWidth = getCellWidth(widthlist,j);
					cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
					setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);	
				}
			}
		}
		
		addBr(doc);	
	}
	
	/**
	 * word添加表格内容
	 * @param doc
	 * @param pa
	 * @param hasTopBorder
	 * @param bothBold
	 * @param rows
	 * @param cols
	 * @param contents 数据来源二维数组
	 * @throws Exception
	 */
	public static void createTable(XWPFDocument doc, ParagraphAlignment pa, boolean hasTopBorder, boolean bothBold,
			int rows, int cols, String[][] contents) throws Exception {
		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(hasTopBorder, table);
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			// 设置行的高度
            boolean mergeStarted = false;
            XWPFTableCell lastCell = null;
            for (int j = 0; j < cols; j++) {
                XWPFTableCell cell = theRow.getCell(j);
                CTTcPr cellPr = cell.getCTTc().addNewTcPr();
                // 设置宽度
                int cellWidth = getCellWidth(new int[] { 16, 12, 16, 10, 16, 12, 16, 2 }, j);
                cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));
                boolean isBold = false;
                if (bothBold) {
                    isBold = true;
                }
                setCellText(cell, isBold, contents[i][j], pa);
                if (lastCell != null && contents[i][j] == null) {
                    if (!mergeStarted) {
                        lastCell.getCTTc().getTcPr().addNewHMerge().setVal(STMerge.RESTART);
                        cell.getCTTc().getTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
                        mergeStarted = true;
                    } else {
                        cell.getCTTc().getTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
                    }
                    continue;
                }
                mergeStarted = false;
                lastCell = cell;
            }
        }
	}
	
	/**
	 * 获取非标选项
	 * 
	 * @param contents
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void genTechCommonF(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
	    addBr(doc);    
		int rows = contents.length;
		int cols = contents[0].length;
		if (cols != 6) {
			throw new IOException(MessageFormat.format(PDFUtils.ContentsErrorMsg,
					new Object[] { "Column counts is illegal @ genTechCommonF" }));
		}

		int[] widthlist = new int[] { 20, 8, 11, 11, 8, 11, 15, 8, 8 };
		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(false,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	public static void genTechSectionNS(String[][] contents, XWPFDocument doc) throws DocumentException, IOException {
	    addBr(doc);    
		int rows = contents.length;
		int cols = contents[0].length;
		if (cols != 8) {
			throw new IOException(MessageFormat.format(PDFUtils.ContentsErrorMsg,
					new Object[] { "Column counts is illegal @ genTechCommonF" }));
		}

		int[] widthlist = new int[] { 15, 15, 15, 15, 15, 15, 5, 5};
		XWPFTable table = doc.createTable(rows, cols);
		setTableProp(false,table);//顶部边框是否存在
		
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(widthlist,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));				
				setCellText(cell, false, contents[i][j], ParagraphAlignment.LEFT);
			}
		}
	}
	
	/**
	 * 设置表格属性
	 * @param hasTopBorder
	 * @param table
	 */
	private static void setTableProp(boolean hasTopBorder, XWPFTable table) {
        CTTblPr tblPr = table.getCTTbl().getTblPr() == null ? table.getCTTbl().addNewTblPr()
                : table.getCTTbl().getTblPr();
        CTTblBorders borders = tblPr.addNewTblBorders();
        CTBorder hBorder = borders.addNewInsideH();
		hBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		hBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		hBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder vBorder = borders.addNewInsideV();
		vBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		vBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		vBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder lBorder = borders.addNewLeft();
		lBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		lBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		lBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder rBorder = borders.addNewRight();
		rBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		rBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		rBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder tBorder = borders.addNewTop();
		if(hasTopBorder) {
			tBorder.setVal(STBorder.Enum.forString("single"));
			tBorder.setSz(new BigInteger("12"));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setColor("000000");
		}else{
			tBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
			tBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		}
		CTBorder bBorder = borders.addNewBottom();
		bBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		bBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		bBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
				.addNewTblW();
		CTJc cTJc = tblPr.addNewJc();
		cTJc.setVal(STJc.Enum.forString("center"));
		tblWidth.setW(new BigInteger("10000"));
		tblWidth.setType(STTblWidth.DXA);
	}
	
	/**
	 * 获取列宽
	 * @param cols
	 * @param j
	 * @return
	 */
	private static int getCellWidth(int[] widthlist, int j) {
		int cellWidth = 0;
		try {
			cellWidth = (int)(((float)widthlist[j]/100)*8000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cellWidth;
	}
	
	/**
	 * 设置单元格内容、样式
	 * @param cell
	 * @param fontFamily
	 * @param isBold
	 * @param text
	 * @param pa
	 */
	private static void setCellText(XWPFTableCell cell, boolean isBold,String text,ParagraphAlignment pa){
		if(StringUtils.isBlank(text)) {
			return;
		}
		
		if (text.startsWith(UtilityConstant.SYS_PUNCTUATION_POUND)) {
			text = text.substring(1);
		}
		XWPFParagraph p1 = cell.getParagraphs().get(0);
		p1.setAlignment(pa);
//		p1.setVerticalAlignment(TextAlignment.TOP);
		XWPFRun r1 = p1.createRun();
		r1.setBold(isBold);
		r1.setText(reCheckText(text));//内容
		r1.setFontFamily(getFontFamily());
		r1.setFontSize(8);
	}
	
	private static void setCellPic(XWPFTableCell cell,ParagraphAlignment pa, String path, String fileName) {
				
		XWPFParagraph p1 = cell.getParagraphs().get(0);
		p1.setAlignment(pa);
		XWPFRun r1 = p1.createRun();
//		r1.addCarriageReturn();
//		InputStream is = cell.getClass().getResourceAsStream(path);
		try {
			File file = new File(path);
//			InputStream input = new FileInputStream(file);
//			byte[] byt = new byte[input.available()];
//			input.read(byt);
//			InputStream is = new ByteArrayInputStream(byt);
			InputStream is = new FileInputStream(file);
			r1.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, fileName,
					Units.toEMU((int) (PageSize.A4.getWidth() * 0.11)),
					Units.toEMU((int) (PageSize.A4.getWidth() * 0.07)));
		} catch (InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String reCheckText(String checkResult){
		String result = checkResult;
		checkResult = checkResult.trim().toLowerCase();

		if(UtilityConstant.SYS_ASSERT_TRUE.equals(checkResult)){
			return "是";
		}
		if(UtilityConstant.SYS_ASSERT_FALSE.equals(checkResult)){
			return "否";
		}
		return result;
	}
	
    /**
     * 换行
     * 
     * @param doc
     */
    public static void addBr(XWPFDocument doc) {
        // 创建一个段落
        XWPFParagraph para = doc.createParagraph();
    }

    public static void addNewPage(XWPFDocument doc) {
        XWPFParagraph paragraph = doc.createParagraph();
        paragraph.setPageBreak(true);
    }

	/**
	 * 跨列合并单元格
	 * @param table
	 * @param row
	 * @param fromCell
	 * @param toCell
	 */
	public static void mergeCellsHorizontal(XWPFTable table, int row, int fromCell, int toCell) {
		for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
			XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
			if (cellIndex == fromCell) {
				// The first merged cell is set with RESTART merge value
				cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
			} else {
				// Cells which join (merge) the first one, are set with CONTINUE
				cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
			}
		}

	}
	
	/**
	 * 跨行合并单元格
	 * @param table
	 * @param col
	 * @param fromRow
	 * @param toRow
	 */
	public static void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow) {
		for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
			XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
			if (rowIndex == fromRow) {
				// The first merged cell is set with RESTART merge value
				cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
			} else {
				// Cells which join (merge) the first one, are set with CONTINUE
				cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
			}
		}
	} 
	
}