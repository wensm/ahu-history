package com.carrier.ahu.util.partition;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Validate ahu partition data before saving from manual update from screen.
 * 
 * Created by Braden Zhou on 2018/06/25.
 */
public final class AhuPartitionValidator {

    public static final int ONSITE_INSTALLATION_LENGTH_THRESHOLD = 18; // 18M
    public static final String CMC_SECTION_NOSIGLE_UNIT_NO_START = "0608";
    public static final String CMC_SECTION_NOSIGLE_UNIT_NO_END = "1015";
    public static final String UNIT_HEIGHT_CHECK_NO = "2226";
    public static final String UNIT_WITH_TOP_DAMPER_HEIGHT_CHECK_NO = "2025";
    public static final String UNIT_WITH_ROOF_HEIGHT_CHECK_NO = "2025";
    public static final String UNIT_WITH_TOP_DAMPER_AND_ROOF_HEIGHT_CHECK_NO = "1825";
    public static final String UNIT_WIDTH_CHECK_NO = "1420";
    public static final String UNIT_WITH_ROOF_WIDTH_CHECK_NO = "1418";
    public static final String UNIT_WITH_ROOF_ONSITE_INSTALL_LENGTH_CHECK_NO = "1418";
    public static final int PARTITION_FOR_NORMAL_CONTAINER_LENGTH = 20;
    public static final int PARTITION_LENGTH_CKD_RANGE_FROM = 21; // 21M ~ 27M
    public static final int PARTITION_LENGTH_CKD_RANGE_TO = 27;
    public static final String UNIT_LENGTH_CKD_CHECK_NO = "1420";
    public static final int PARTITION_WITH_ROOF_ONSITE_INSTALL_LENGTH = 18; // 18M
    public static final String[][] MINIMUM_PARTITION_LENGTH_CHECK_NOS = { { "0608", "0711", "3" },
            { "0811", "0914", "4" }, { "1015", "1117", "5" }, { "1317", "1420", "6" }, { "1621", "2333", "6" } };

    public static void validate(Partition partition, Unit unit, AhuParam ahuParam) throws ApiException {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        List<AhuPartition> ahuPartitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        if (LayoutStyleEnum.COMMON.style() == ahuLayout.getStyle()
                || LayoutStyleEnum.NEW_RETURN.style() == ahuLayout.getStyle()) { // 单层布局
            validate(ahuPartitions, ahuParam, ahuLayout.getStyle(), true);
        } else if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
                || LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) { // 双层布局
            int indexOfSecondLayer = getIndexOfSecondLayer(ahuPartitions, ahuLayout.getLayoutData());
            List<AhuPartition> firstLayerAhuPartitions = ahuPartitions.subList(0, indexOfSecondLayer);
            List<AhuPartition> secondLayerAhuPartitions = ahuPartitions.subList(indexOfSecondLayer,
                    ahuPartitions.size());
            validate(firstLayerAhuPartitions, ahuParam, ahuLayout.getStyle(), true);
            validate(secondLayerAhuPartitions, ahuParam, ahuLayout.getStyle(), false);
            if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
                // 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
                if (!validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions)) {
                    throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE);
                }
            } else if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
                // 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
                if (!validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions)) {
                    throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE);
                }
            }
        }

        int totalPartitionLength = getTotalAhuPartitionLength(ahuPartitions); // 机组所有分段总长度

        // 高度限制
        // 1. 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 2. 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 3. 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 4. 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 宽度、长度限制
        // 1. 1420及以上所有机组：如果机组段长≤20M，可选普通集装箱；在21~27M之间，需CKD或特种箱出厂。如果段长>27M, 只能选用CKD
        // 2. 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。
        if (AHUContext.isExportVersion()) { // 出口版
            validateUnitHeightAgainstContainer(ahuParam);
            validateUnitWidthAgainstContainer(ahuParam, totalPartitionLength);
        }

        for (AhuPartition ahuPartition : ahuPartitions) {
            // 规则： 所有机组单独分段长度小于6M, 建议改成CKD出厂。
//            if (!validateLeaveFactoryUseCKD(ahuParam, ahuPartition.getLength())) {
//                throw new ApiException(ErrorCode.PARTITION_VALIDATE_LEN_TOTAL);
//            }
            // 规则：装箱段段长大于等于18M同时机组的宽度大于等于18M，如果选择顶棚，需要提示段长超过集装箱限制，顶棚需现场安装！
            if (!validateRoofOnsiteInstallation(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_LEN);
            }
            if (!validateMinimumPartitionLength(ahuPartition, ahuParam)) {
                String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
                int index = getIndexOfMinimumCheckNo(unitNo);
                throw new ApiException(ErrorCode.PARTITION_LENGTH_NO_LESS_THAN,
                        MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][0] + "~"
                                + MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][1],
                        MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][2]);
            }
        }
    }

    private static void validate(List<AhuPartition> ahuPartitions, AhuParam ahuParam, int layoutStyle, boolean isTop)
            throws ApiException {
        Map<String, Object> preLastSection = null;
        String preLastSectionId = "";
        boolean switchAirFlow = false; // 默认气流方向从左到右, 不用切换
        boolean foundHeatRecycle = false;
        if (LayoutStyleEnum.WHEEL.style() == layoutStyle && isTop) { // 转轮热回收上层气流方向从右至左
            switchAirFlow = true;
        } else if (LayoutStyleEnum.PLATE.style() == layoutStyle && !isTop) { // 板式热回收下层初始方向从右至左
            switchAirFlow = true;
        }

        for (AhuPartition ahuPartition : ahuPartitions) {
            Map<String, Object> nextFirstSection = ahuPartition.getSections().get(0);
            String nextFirstSectionId = AhuPartition.getMetaIdOfSection(nextFirstSection); // 当前分段块里面的第一个段

            // 规则：检查不可拆分段
            if (!validateUnsplittableSection(preLastSectionId, nextFirstSectionId)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则：所有机组【空段】选择了门选项且段长小于5M，不能独立作为分成一段出厂，也就是空段的前面或者后面都必须有其他功能段！
            if (!validateAccessSectionWithODoor(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_ACCESS_LENGH);
            }

            // 规则： 装箱段不能只有一个【控制段】
            if (!validateSingleControlSection(ahuPartition)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_CTR_EXIST);
            }

            // 规则： 带顶棚选项且有顶部风阀（【混合段】【出风段】【风机段】），机组的宽度大于等于18M, 整机出厂时，英文版时
            // 需要提示,出厂方式自动默认为开顶箱或者CKD。
            if(AHUContext.isExportVersion()) { // 出口版
                if (!validateRoofWithTopOutlet(ahuPartition, ahuParam)) {
                    throw new ApiException(ErrorCode.EXPORT_UNIT_WITH_TOP_DAMPER_NEED_CKD);
                }
            }

            // 规则：0608~1015机组的新回排段不能单独分段。
            if (!validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_COMBINEDMIXINGCHAMBER_SPLIT);
            }

            // 规则：所有机组小于5M段长的带检修门功能段（混合段，出风段，风机段，空段，二次回风段）不能单独分段。
            if (!validateSingleSectionWithAccessDoor(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_DOOR);
            }

            // 规则: 由于现场段连接操作困难,两个都不带检修门功能段之间禁止分段!
            if (!validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则: 由于现场段连接操作困难,过滤段前或者侧抽过滤段前后禁止分段!
            // 需要考虑气流方向
            Map<String, Object> preFlowSection = preLastSection;
            Map<String, Object> nextFlowSection = nextFirstSection;
            if (switchAirFlow) {
                preFlowSection = nextFirstSection;
                nextFlowSection = preLastSection;
            }
            if (!validateSplittedFilterSection(preFlowSection, nextFlowSection, ahuParam)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则: 控制段需要与风机段作为一段出厂，不能单独分段！
            if (!validateSplittedCtrAndFanSection(preLastSectionId, nextFirstSectionId)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            preLastSection = ahuPartition.getSections().get(ahuPartition.getSections().size() - 1);
            preLastSectionId = AhuPartition.getMetaIdOfSection(preLastSection); // 上个分段块里面的最后一个段
            if (LayoutStyleEnum.PLATE.style() == layoutStyle && !foundHeatRecycle) { // 从板式热回段下一个开始切换气流方向
                if (isHeatRecycleInPartition(ahuPartition)) {
                    switchAirFlow = !switchAirFlow;
                }
            }
        }
    }

    public static boolean isDeliveryCKD(AhuParam ahuParam) {
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        return SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery);
    }

    private static void throwCannotBeDismantledException(String preLastSectionId, String nextFirstSectionId) {
        String preLastSectionName = getIntlString(SectionTypeEnum.getSectionTypeFromId(preLastSectionId).getCnName());
        String nextFirstSectionName = getIntlString(
                SectionTypeEnum.getSectionTypeFromId(nextFirstSectionId).getCnName());
        throw new ApiException(ErrorCode.SECTION_CANNOT_BE_DISMANTLED, preLastSectionName, nextFirstSectionName);
    }

    private static boolean isHeatRecycleInPartition(AhuPartition ahuPartition) {
        LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionId)
                    || SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionId)) { // 找到热回收段
                return true;
            }
        }
        return false;
    }

    /**
     * 校验返回false, 如果都不带检修门。 </br>
     * 
     * 规则: 由于现场段连接操作困难,两个都不带检修门功能段之间禁止分段!
     * 
     * @param preLastSection
     * @param nextFirstSection
     * @param ahuParam
     * @return
     */
    public static boolean validateAccessDoorOfAdjacentSection(Map<String, Object> preLastSection,
            Map<String, Object> nextFirstSection, AhuParam ahuParam) {
        if (preLastSection != null && nextFirstSection != null) {
            // 返回false，如果都没有检修门
            return hasAccessDoor(preLastSection, ahuParam) || hasAccessDoor(nextFirstSection, ahuParam);
        }
        return true;
    }

    private static boolean hasAccessDoor(Map<String, Object> section, AhuParam ahuParam) {
        String sectionId = AhuPartition.getMetaIdOfSection(section);
        if (!AhuPartitionGenerator.SECTION_WITH_DOOR.contains(sectionId)) {
            return false;
        }

        String accessDoor = null;
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_MIX, section, MetaKey.KEY_DOORO);
            return SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_DISCHARGE, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(accessDoor)
                    || SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section, MetaKey.KEY_ACCESSDOOR);
            return true;
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)) {
            String osDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_OSDOOR);
            String isDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_ISDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(isDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(isDoor);
        }else if(SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)
        		||SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionId)
        		||SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionId)
        		||SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)) {
        	return true;
        }
        return false;
    }

    private static int getIndexOfSecondLayer(List<AhuPartition> ahuPartitions, int[][] layoutData) {
        int indexOfSecondLayer = 0;
        if (layoutData.length == 4) { // two layers
            for (int i = 0; i < ahuPartitions.size(); i++) {
                AhuPartition ahuPartition = ahuPartitions.get(i);
                LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
                int position = AhuPartition.getPosOfSection(sections.get(0));
                if (Arrays.binarySearch(layoutData[2], position) != -1) { // in second layer
                    return i;
                }
            }
        }
        return indexOfSecondLayer;
    }

    private static int getTotalAhuPartitionLength(List<AhuPartition> ahuPartitions) {
        int totalLength = 0;
        for (AhuPartition ahuPartition : ahuPartitions) {
            totalLength += ahuPartition.getLength();
        }
        return totalLength;
    }

    /**
     * 根据layout数据划分开上下层分段。
     * 
     * @param ahuPartitions
     * @param layoutData
     * @return
     */
    public static List<List<AhuPartition>> getLayeredAhuPartitions(List<AhuPartition> ahuPartitions,
            int[][] layoutData) {
        List<List<AhuPartition>> layeredAhuPartitions = new ArrayList<>();
        if (layoutData.length == 4) { // two layers
            List<AhuPartition> firstLayerAhuPartition = new ArrayList<>();
            List<AhuPartition> secondLayerAhuPartition = new ArrayList<>();
            for (AhuPartition ahuPartition : ahuPartitions) {
                LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
                int position = AhuPartition.getPosOfSection(sections.get(0));
                // in first layer
                if (Arrays.binarySearch(layoutData[0], position) != -1
                        || Arrays.binarySearch(layoutData[1], position) != -1) {
                    firstLayerAhuPartition.add(ahuPartition);
                } else { // in second layer
                    secondLayerAhuPartition.add(ahuPartition);
                }
            }
            layeredAhuPartitions.add(firstLayerAhuPartition);
            layeredAhuPartitions.add(secondLayerAhuPartition);
        } else { // one layer
            layeredAhuPartitions.add(ahuPartitions);
        }

        return layeredAhuPartitions;
    }

    /**
     * 校验失败返回false，如果分段长度大于最大段长，并且包含可拆分的段。
     * 
     * @param partition
     * @return
     */
    public static boolean validatePartitionLength(AhuPartition partition) {
        int maxLength = AhuPartitionGenerator.getMaxPartitionLength();
        if (partition.getLength() > maxLength && hasSplittableSection(partition.getSections())) {
            return false;
        }
        return true;
    }

    /**
     * 检查是否有可拆分的段。
     * 
     * @param sections
     * @return
     */
    private static boolean hasSplittableSection(List<Map<String, Object>> sections) {
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (EmptyUtil.isNotEmpty(sectionId) && !AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(sectionId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 返回false，如果前后两个段都不能被拆开。
     * 
     * @param preLastSectionId
     * @param nextFirstSectionId
     * @return
     */
    public static boolean validateUnsplittableSection(String preLastSectionId, String nextFirstSectionId) {
        return !(AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(nextFirstSectionId)
                && AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(preLastSectionId));
    }

    /**
     * 校验返回true, 如果, </br>
     * 规则：所有机组【空段】选择了门选项且段长小于5M，不能独立作为分成一段出厂，也就是空段的前面或者后面都必须有其他功能段！
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateAccessSectionWithODoor(AhuPartition partition, AhuParam ahuParam) {
        Map<String, Object> section = partition.getSections().get(0);
        String oDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
        return !((partition.getSections().size() == 1)
                && AhuPartition.getSectionLOfSection(section) < AhuPartitionGenerator.getMinAccessLength()
                && SectionTypeEnum.TYPE_ACCESS.getId().equals(AhuPartition.getMetaIdOfSection(section))
                && (SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(oDoor)
                        || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(oDoor)));
    }

    private static String getSectionParameterByKey(AhuParam ahuParam, SectionTypeEnum sectionType,
            Map<String, Object> section, String metaKey) {
        return getSectionParameterByKey1(ahuParam, sectionType.getId(), AhuPartition.getPosOfSection(section),
                SectionMetaUtils.getMetaSectionKey(sectionType, metaKey));
    }

    private static String getSectionParameterByKey1(AhuParam ahuParam, String sectionKey, short sectionPos,
            String key) {
        for (PartParam partParam : ahuParam.getPartParams()) {
            if (partParam.getKey().equals(sectionKey) // 段type相同
                    && partParam.getPosition() == sectionPos) {// 位置相等
                return String.valueOf(partParam.getParams().get(key));
            }
        }
        return null;
    }

    /**
     * 校验返回false, 如果分段只有一个控制段。
     * 
     * @param partition
     * @return
     */
    public static boolean validateSingleControlSection(AhuPartition partition) {
        Map<String, Object> section = partition.getSections().get(0);
        return !(partition.getSections().size() == 1
                && SectionTypeEnum.TYPE_CTR.getId().equals(AhuPartition.getMetaIdOfSection(section)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 带顶棚选项且有顶部风阀（【混合段】【出风段】【风机段】），机组的宽度大于等于18M,
     * 整机出厂时，英文版时需要提示,出厂方式自动默认为开顶箱或者CKD。
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateRoofWithTopOutlet(AhuPartition partition, AhuParam ahuParam) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String unitModel = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        int unitWidth = AhuUtil.getWidthOfAHU(unitModel);
        if (Boolean.valueOf(isprerain) && unitWidth >= ONSITE_INSTALLATION_LENGTH_THRESHOLD) {
            for (Map<String, Object> section : partition.getSections()) {
                if (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String returnTop = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_MIX, section,
                            MetaKey.KEY_RETURNTOP);
                    if (Boolean.valueOf(returnTop)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String outletDirection = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_DISCHARGE, section,
                            MetaKey.KEY_OUTLETDIRECTION);
                    if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(outletDirection)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                } else if (SectionTypeEnum.TYPE_FAN.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String outletDirection = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section,
                            MetaKey.KEY_oUTLETDIRECTION);
                    if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(outletDirection)
                            || SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验返回false, 如果, </br>
     * 规则 ：装箱段段长大于等于18M同时机组的宽度大于等于18M，如果选择顶棚，需要提示段长超过集装箱限制，顶棚需现场安装！
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateRoofOnsiteInstallation(AhuPartition partition, AhuParam ahuParam) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String unitModel = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        int unitWidth = AhuUtil.getWidthOfAHU(unitModel);
        return !(Boolean.valueOf(isprerain) && unitWidth >= ONSITE_INSTALLATION_LENGTH_THRESHOLD
                && partition.getLength() >= AhuUtil.getRealSize(ONSITE_INSTALLATION_LENGTH_THRESHOLD));
    }

    /**
     * 校验返回false, 如果,
     * 
     * </br>
     * 规则：0608~1015机组的新回排段不能单独分段。
     * 
     * @param partition
     * @return
     */
    public static boolean validateSingleCombinedMixingChamberSection(AhuPartition partition, AhuParam ahuParam) {
        Map<String, Object> section = partition.getSections().get(0);
        String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
        return !((partition.getSections().size() == 1) && CMC_SECTION_NOSIGLE_UNIT_NO_START.compareTo(unitNo) < 1
                && CMC_SECTION_NOSIGLE_UNIT_NO_END.compareTo(unitNo) > -1
                && SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(AhuPartition.getMetaIdOfSection(section)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则8：所有机组小于5M段长的带检修门功能段（混合段，出风段，风机段，空段，二次回风段）不能单独分段。
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateSingleSectionWithAccessDoor(AhuPartition partition, AhuParam ahuParam) {
        if (partition.getSections().size() == 1) { // 独立分段
            Map<String, Object> section = partition.getSections().get(0);
            int sectionLength = AhuPartition.getSectionLOfSection(section);
            if (sectionLength < AhuPartitionGenerator.getMinPartitionWithDoorLength()) { // 小于 5M
                return !hasAccessDoor(section, ahuParam);
            }
        }
        return true;
    }

    /**
     * 校验返回false, 如果前后分段末尾是单层过滤段。 </br>
     * 
     * 规则: 由于现场段连接操作困难,过滤段前或者侧抽过滤段前后禁止分段!
     * 
     * @param preLastSection
     * @param nextFirstSection
     * @param ahuParam
     * @return
     */
    public static boolean validateSplittedFilterSection(Map<String, Object> preLastSection,
            Map<String, Object> nextFirstSection, AhuParam ahuParam) {
        String preLastSectionId = null;
        String nextFirstSectionId = null;
        String fitetF = null;
        if (preLastSection != null) {
            preLastSectionId = AhuPartition.getMetaIdOfSection(preLastSection);
            if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(preLastSectionId)) {
                fitetF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_HEPAFILTER, preLastSection,
                        MetaKey.KEY_FITETF);
                // 高效过滤段，1. 如果选择箱型，可以在看作有门，但是门在箱体后部，可以分到一个分段的末尾
                if (!SystemCalculateConstants.HEPAFILTER_FILTERF_P.equals(fitetF)) {
                    return false;
                }
            }
        }
        if (nextFirstSection != null) {
            nextFirstSectionId = AhuPartition.getMetaIdOfSection(nextFirstSection);
            if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(nextFirstSectionId)) {
                fitetF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_HEPAFILTER, nextFirstSection,
                        MetaKey.KEY_FITETF);
                // 高效过滤段，2.如果选择V形，门可以看作是在段的前面，可以被分到一个分段的开头
                if (!SystemCalculateConstants.HEPAFILTER_FILTERF_V.equals(fitetF)) {
                    return false;
                }
            }
        }
        if (preLastSection != null && nextFirstSection != null) {
            String mediaLoading = null;
            if (SectionTypeEnum.TYPE_SINGLE.getId().equals(preLastSectionId)) { // 侧抽后不允许分段
                mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_SINGLE, preLastSection,
                        MetaKey.KEY_MEDIALOADING);
                return !SystemCalculateConstants.FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading);
//            } 
//            else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(preLastSectionId)) { // 侧抽后不允许分段
//                mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMPOSITE, preLastSection,
//                        MetaKey.KEY_MEDIALOADING);
//                return !SystemCalculateConstants.COMBINEDFILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading);
            } else { // 过滤段前都不允许分段
                return !(SectionTypeEnum.TYPE_SINGLE.getId().equals(nextFirstSectionId)
//                        || SectionTypeEnum.TYPE_COMPOSITE.getId().equals(nextFirstSectionId)
                        || SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId().equals(nextFirstSectionId));
            }
        }
        return true;
    }

    /**
     * 返回false，如果, </br>
     * 
     * 规则: 控制段需要与风机段作为一段出厂，不能单独分段！
     * 
     * @param preLastSectionId
     * @param nextFirstSectionId
     * @return
     */
    public static boolean validateSplittedCtrAndFanSection(String preLastSectionId, String nextFirstSectionId) {
        return !((SectionTypeEnum.TYPE_CTR.getId().equals(preLastSectionId)
                && SectionTypeEnum.TYPE_FAN.getId().equals(nextFirstSectionId))
                || (SectionTypeEnum.TYPE_CTR.getId().equals(nextFirstSectionId)
                        && SectionTypeEnum.TYPE_FAN.getId().equals(preLastSectionId)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则：所有机组单独分段长度小于6M, 建议改成CKD出厂。
     * 
     * @param ahuParam
     * @param partitionLength
     * @return
     */
    public static boolean validateLeaveFactoryUseCKD(AhuParam ahuParam, int partitionLength) {
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        return SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                || !(partitionLength < AhuPartitionGenerator.getMinPartitionLength());
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
     * 
     * @param ahuLayout
     * @param ahuPartitions
     * @return
     */
    public static boolean validatePlateHeatRecycleBypass(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
            int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
            int positionOfBottomRecyle = -1;
            if (bottomLeftLayout.length > 0) { // 回收段在最后一个
                positionOfBottomRecyle = bottomLeftLayout[bottomLeftLayout.length - 1];
            }

            if (positionOfBottomRecyle != -1) {
                Map<String, Object> lastSectionOfPrePartition = null;
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfBottomRecyle) { // 找到了回收段
                            // 回收段是分段中第一个段
                            if (s == 0) {
                                if (lastSectionOfPrePartition != null) {
                                    int lastSectionPosition = AhuPartition.getPosOfSection(lastSectionOfPrePartition);
                                    // 而且上个分段的最后一个段也是在底层左侧的布局里面
                                    if (Arrays.binarySearch(bottomLeftLayout, lastSectionPosition) != -1) {
                                        // 同时上个分段的最后一个段是旁通(混合段或者出风段)
                                        if ((SectionTypeEnum.TYPE_MIX.getId()
                                                .equals(AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition))
                                                || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                        AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition)))) {
                                            return false; // 不允许分段
                                        }
                                    }
                                }
                            }
                            if (s == ahuPartition.getSections().size() - 1) {// 回收段是分段中最后一个段
                                if (p < ahuPartitions.size() - 1) {// 获取下一个分段, 再获取分段中的第一个段
                                    AhuPartition nextPartition = ahuPartitions.get(p + 1);
                                    Map<String, Object> firstSectionOfNextPartiton = nextPartition.getSections().get(0);
                                    // 下个分段的第一个段是旁通(混合段或者出风段)
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton)))) {
                                        return false; // 不允许分段
                                    }
                                } else { // 沒有后续分段
                                    return true;
                                }
                            }
                            // 回收段没有独立分开
                            return true;
                        } else if (s == ahuPartition.getSections().size() - 1) { // 记录本分段最后一个段
                            lastSectionOfPrePartition = section;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 转轮热回收段下一个段是旁通(混合段或者出风段), 不允许分段
     * 
     * @param preSection
     * @param nextSection
     * @return
     */
    public static boolean validateWheelHeatRecycleBypass(Map<String, Object> preSection,
            Map<String, Object> nextSection) {
        if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(AhuPartition.getMetaIdOfSection(preSection))
                && (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(nextSection))
                        || SectionTypeEnum.TYPE_DISCHARGE.getId()
                                .equals(AhuPartition.getMetaIdOfSection(nextSection)))) {
            return false;
        }
        return true;
    }

    /**
     * 板式热回收段下一个段是旁通(混合段或者出风段), 不允许分段
     * 
     * @param preSection
     * @param nextSection
     * @return
     */
    public static boolean validatePlateHeatRecycleBypass(Map<String, Object> preSection,
            Map<String, Object> nextSection) {
        if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(AhuPartition.getMetaIdOfSection(preSection))
                && (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(nextSection))
                        || SectionTypeEnum.TYPE_DISCHARGE.getId()
                                .equals(AhuPartition.getMetaIdOfSection(nextSection)))) {
            return false;
        }
        return true;
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
     * 
     * @param ahuLayout
     * @param ahuPartitions
     * @return
     */
    public static boolean validateWheelHeatRecycleBypass(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        // 转轮回收型
        if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
            int[] topLeftLayout = ahuLayout.getLayoutData()[0];
            int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
            int positionOfTopRecyle = -1;
            int positionOfBottomRecyle = -1;
            if (topLeftLayout.length > 0) { // 回收段在最后一个
                positionOfTopRecyle = topLeftLayout[topLeftLayout.length - 1];
            }
            if (bottomLeftLayout.length > 0) { // 回收段在最后一个
                positionOfBottomRecyle = bottomLeftLayout[bottomLeftLayout.length - 1];
            }

            if (positionOfTopRecyle != -1) { // 检查上层回收段
                Map<String, Object> lastSectionOfPrePartition = null;
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfTopRecyle) { // 找到了回收段
                            if (s == 0) {// 回收段是分段中第一个段
                                if (lastSectionOfPrePartition != null) { // 上层气流方向从右到左, 反向检查分段
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition)))) {// 上个分段的最后一个段是旁通(混合段或者出风段)
                                        return false; // 不允许分段
                                    }
                                }
                            }
                        } else if (s == ahuPartition.getSections().size() - 1) { // 记录本分段最后一个段
                            lastSectionOfPrePartition = section;
                        }
                    }
                }
            }

            if (positionOfBottomRecyle != -1) { // 检查下层回收段
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfBottomRecyle) { // 找到了回收段
                            if (s == ahuPartition.getSections().size() - 1) {// 回收段是分段中最后一个段, 下层气流方向从左到右, 正向检查分段
                                if (p < ahuPartitions.size() - 1) {// 获取下一个分段, 再获取分段中的第一个段
                                    AhuPartition nextPartition = ahuPartitions.get(p + 1);
                                    Map<String, Object> firstSectionOfNextPartiton = nextPartition.getSections().get(0);
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton)))) {// 下个分段的第一个段是旁通(混合段或者出风段)
                                        return false; // 不允许分段
                                    }
                                } else { // 沒有后续分段
                                    return true;
                                }
                            }
                            // 回收段没有独立分开
                            return true;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验返回false，如果, </br>
     * 1. 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。 </br>
     * 2. 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 3. 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 4. 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 
     * @param ahuParam
     * @return
     */
    public static void validateUnitHeightAgainstContainer(AhuParam ahuParam) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        String unitNo = AhuUtil.getUnitNo(ahuParam.getSeries());
        if (Boolean.valueOf(isprerain)) { // has roof
            if (hasTopDamper(ahuParam)) {
                // 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_TOP_DAMPER_AND_ROOF_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD);
                }
            } else {
                // 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_ROOF_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_ROOF_EXCEED_HEIGHT_NEED_CKD);
                }
            }
        } else {
            if (hasTopDamper(ahuParam)) {
                // 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_TOP_DAMPER_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD);
                }
            } else {
                // 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.NORMAL_UNIT_EXCEED_HEIGHT_NEED_CKD);
                }
            }
        }
    }

    private static boolean hasTopDamper(AhuParam ahuParam) {
        List<PartParam> partParams = ahuParam.getPartParams();
        for (PartParam partParam : partParams) {
            if (SectionTypeEnum.TYPE_MIX.getId().equals(partParam.getKey())) {
                String returnTop = partParam.getParams()
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURNTOP))
                        .toString();
                return Boolean.valueOf(returnTop);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(partParam.getKey())) {
                String outletDirection = partParam.getParams().get(
                        SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION))
                        .toString();
                return SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(outletDirection);
            } else if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
                String outletDirection = partParam.getParams()
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION))
                        .toString();
                return SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(outletDirection)
                        || SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(outletDirection);
            }
        }
        return false;
    }

    /**
     * 1. 1420及以上所有机组：如果机组段长≤20M，可选普通集装箱；在21~27M之间，需CKD或特种箱出厂。如果段长>27M, 只能选用CKD</br>
     * 2. 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。</br>
     * 
     * @param ahuParam
     * @param totalPartitionLength
     */
    public static void validateUnitWidthAgainstContainer(AhuParam ahuParam, int totalPartitionLength) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        String unitNo = AhuUtil.getUnitNo(ahuParam.getSeries());
        if (UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1) { // 1420及以上所有机组
            if (totalPartitionLength <= AhuUtil.getRealSize(PARTITION_FOR_NORMAL_CONTAINER_LENGTH)) {
                // 机组段长≤20M，可选普通集装箱
                // 可以选择任何一个出厂方式
            } else if (totalPartitionLength >= AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_FROM)
                    && totalPartitionLength <= AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_TO)) {
                // 段长在21~27M之间，需CKD或特种箱出厂
                if (!(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                        || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.PARTITION_LENGHT_BETWEEN_21M_TO_27M_NEED_CKD_OR_SPECIAL_CONTAINER);
                }
            } else if (totalPartitionLength > AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_TO)) {
                // 段长>27M, 只能选用CKD
                if (!SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)) {
                    throw new ApiException(ErrorCode.PARTITION_LENGHT_GREATER_THAN_27M_NEED_CKD);
                }
            }
        }

        // 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。
        if (Boolean.valueOf(isprerain)
                && UNIT_WITH_ROOF_ONSITE_INSTALL_LENGTH_CHECK_NO.compareTo(unitNo) < 1
                && totalPartitionLength > AhuUtil.getRealSize(PARTITION_WITH_ROOF_ONSITE_INSTALL_LENGTH)) {
            throw new ApiException(ErrorCode.PARTITION_LENGTH_GREATER_THAN_18M_NEED_TO_INSTALL_ONSITE);
        }
    }

    /**
     * 0608~0711 3M <br/>
     * 0811~0914 4M <br/>
     * 1015~1117 5M <br/>
     * 1317~1420 6M <br/>
     * 1621~2333 6M <br/>
     * 
     * @param ahuPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateMinimumPartitionLength(AhuPartition ahuPartition, AhuParam ahuParam) {
        String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
        int index = getIndexOfMinimumCheckNo(unitNo);
        if (index != -1) {
            return (ahuPartition.getLength() >= AhuUtil
                    .getRealSize(Integer.valueOf(MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][2])));
        }
        return true;
    }

    public static int getIndexOfMinimumCheckNo(String unitNo) {
        for (int i = 0; i < MINIMUM_PARTITION_LENGTH_CHECK_NOS.length; i++) {
            String[] checkNoCase = MINIMUM_PARTITION_LENGTH_CHECK_NOS[i];
            if (checkNoCase[0].compareTo(unitNo) < 1 && checkNoCase[1].compareTo(unitNo) > -1) {
                return i;
            }
        }
        return -1;
    }

}
