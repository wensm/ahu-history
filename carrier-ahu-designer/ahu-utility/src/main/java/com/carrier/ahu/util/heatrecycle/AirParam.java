package com.carrier.ahu.util.heatrecycle;

import lombok.Data;

@Data
public class AirParam {

    private double dryBulbT;
    private double wetBulbT;
    private double relativeT;

    public AirParam(double dryBulbT, double wetBulbT, double relativeT) {
        this.dryBulbT = dryBulbT;
        this.wetBulbT = wetBulbT;
        this.relativeT = relativeT;
    }

}