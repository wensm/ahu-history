package com.carrier.ahu.util.upgrade;

import java.util.StringTokenizer;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.common.util.VersionUtils;
import com.carrier.ahu.constant.CommonConstant;

/**
 * 
 * Created by Braden Zhou on 2018/12/17.
 */
public class UpgradeUtils {

    public static boolean isValidZipVersion(String zipFileName, String sysVersion) {
        if (zipFileName.startsWith(CommonConstant.SYS_UPGRADE_FILE_PREFIX)
                && zipFileName.endsWith(CommonConstant.SYS_ZIP_EXTENSION)) {
            String upgradeVersion = zipFileName.replace(CommonConstant.SYS_UPGRADE_FILE_PREFIX, "");
            upgradeVersion = upgradeVersion.replace(CommonConstant.SYS_ZIP_EXTENSION, "");
            upgradeVersion = getVersionNumber(upgradeVersion);
            return NumberUtils.isParsable(upgradeVersion.replaceAll("\\.", ""))
                    && (VersionUtils.compare(upgradeVersion, sysVersion) >= 0);
        }
        return false;
    }

    private static String getVersionNumber(String versionString) {
        StringTokenizer tokenizer = new StringTokenizer(versionString, CommonConstant.SYS_PUNCTUATION_HYPHEN);
        String versionNumber = "";
        while (tokenizer.hasMoreTokens()) {
            versionNumber = tokenizer.nextToken();
        }
        return versionNumber;
    }

    public static boolean isValidJarVersion(String jarFileName, String sysVersion) {
        String upgradeVersion = getVersionNumber(jarFileName.replace(CommonConstant.SYS_JAR_EXTENSION, ""));
        return NumberUtils.isParsable(upgradeVersion.replaceAll("\\.", ""))
                && (VersionUtils.compare(upgradeVersion, sysVersion) >= 0);
    }

}
