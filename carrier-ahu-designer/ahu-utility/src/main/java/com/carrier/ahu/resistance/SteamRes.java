package com.carrier.ahu.resistance;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.S4FArea;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/11. 蒸汽盘管段
 */
public class SteamRes {

	// fanType:机组型号
	public double getResistance(String fanType, int airVolume) {
		// 查表[S_4FArea]，得到FArea，FfArea Vr（质量风速）=[FArea]*1.2/[ FfArea]
		// 阻力ΔP=1.08*2* power(Vr,1.98)
		S4FArea s4FArea = AhuMetadata.findOne(S4FArea.class, fanType);
		if (EmptyUtil.isNotEmpty(s4FArea)) {
			double ffArea = s4FArea.getFfArea();
			double vr = airVolume / 3600.0 * 1.2 / ffArea;
			double res = 1.08 * 2.0 * Math.pow(vr, 1.98);
			return res;
		}
		return 0.00;
	}

}
