package com.carrier.ahu.util.template;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class FullYearTemplate extends TemplateFactory {

    public FullYearTemplate(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.NEW_RETURN;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] { SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER,
                SectionTypeEnum.TYPE_SINGLE, SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_COLD,
                SectionTypeEnum.TYPE_STEAMHUMIDIFIER, SectionTypeEnum.TYPE_FAN };
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Part> preConfigureSections(List<Part> parts) {
        List<Part> updatedParts = new ArrayList<Part>();
        String airDirection = AirDirectionEnum.RETURNAIR.getCode();
        for (Part part : parts) {
            String partMetaJson = part.getMetaJson();
            Map<String, Object> partMeta = JSONArray.parseObject(partMetaJson, Map.class);
            partMeta.put(METASEXON_AIRDIRECTION, airDirection);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(part.getSectionKey());
            /* 二次回风段前为回风，后面均为送风 */
            if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.equals(sectionType)) {
                airDirection = AirDirectionEnum.SUPPLYAIR.getCode();
            }
            part.setMetaJson(JSONArray.toJSONString(partMeta));
            updatedParts.add(part);
        }
        return updatedParts;
    }

}
