package com.carrier.ahu.util.section;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by Braden Zhou on 2019/01/07.
 */
public class FanSectionUtils {

    /**
     * FC - front curve.
     * 
     * @param fanModel
     * @return
     */
    public static boolean isFrontCurveFan(String fanModel) {
        if (EmptyUtil.isNotEmpty(fanModel)) {
            fanModel = fanModel.toUpperCase();
            return fanModel.startsWith(UtilityConstant.SYS_MAP_FAN_FANMODEL_FC);
        }
        return false;
    }

    /**
     * BC - back curve.
     * 
     * @param fanModel
     * @return
     */
    public static boolean isBackCurveFan(String fanModel) {
        if (EmptyUtil.isNotEmpty(fanModel)) {
            fanModel = fanModel.toUpperCase();
            return fanModel.startsWith(UtilityConstant.SYS_MAP_FAN_FANMODEL_BC);
        }
        return false;
    }

    public static boolean isStrengthenFan(String fanModel) {
        if (EmptyUtil.isNotEmpty(fanModel)) {
            fanModel = fanModel.toUpperCase();
            return fanModel.endsWith(UtilityConstant.SYS_MAP_FAN_FANMODEL_K);
        }
        return false;
    }

}
