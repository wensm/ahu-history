package com.carrier.ahu.util.section;

import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;

/**
 * Created by Braden Zhou on 2019/01/07.
 */
public class HumidifierUtils {

    /**
     * SELECT HUMIDQ, PWValue from S_4XHUMID a, s_Weight4X b where val(b.hTypes) = a.HTYPES order by a.HUMIDQ
     */
    private static int[] HUMIDQ = new int[] { 4, 6, 9, 11, 13, 14, 15, 17, 22, 26, 29, 32, 36, 39, 43, 50, 54, 58, 65, 69, 72, 76, 86,
            97, 108, 130, 144, 180, 216, 230, 252, 288, 324, 360, 432 };

    /**
     * Get max humidification if both winter and summer enabled
     * 
     * @param sectionKey
     * @param partMetaMap
     * @return
     */
    public static double getMaxSeasonHumidification(String sectionKey, Map<String, Object> partMetaMap) {
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(sectionKey);
        return getMaxSeasonHumidification(sectionType, partMetaMap);
    }

    public static double getMaxSeasonHumidification(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        double maxHumid = 0.0;
        if (isSummerEnabled(sectionType, partMetaMap)) {
            maxHumid = getSummerHumidification(sectionType, partMetaMap);
        }
        if (isWinterEnabled(sectionType, partMetaMap)) {
            double winterHumid = getWinterHumidification(sectionType, partMetaMap);
            if (maxHumid < winterHumid) {
                maxHumid = winterHumid;
            }
        }
        return maxHumid;
    }

    /**
     * Get max heat if both winter and summer enabled
     * 
     * @param sectionKey
     * @param partMetaMap
     * @return
     */
    public static double getMaxSeasonHeat(String sectionKey, Map<String, Object> partMetaMap) {
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(sectionKey);
        double maxHeat = 0.0;
        if (isSummerEnabled(sectionType, partMetaMap)) {
            maxHeat = getSummerHeat(sectionType, partMetaMap);
        }
        if (isWinterEnabled(sectionType, partMetaMap)) {
            double winterHeat = getWinterHeat(sectionType, partMetaMap);
            if (maxHeat < winterHeat) {
                maxHeat = winterHeat;
            }
        }
        return maxHeat;
    }

    private static boolean isSummerEnabled(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String enableSummerMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ENABLE_SUMMER);
        return Boolean.valueOf(String.valueOf(partMetaMap.get(enableSummerMetaKey)));
    }

    private static boolean isWinterEnabled(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String enableWinterMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_ENABLE_WINTER);
        return Boolean.valueOf(String.valueOf(partMetaMap.get(enableWinterMetaKey)));
    }

    private static double getSummerHumidification(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String humidMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_SHUMIDIFICATIONQ);
        return NumberUtils.toDouble(String.valueOf(partMetaMap.get(humidMetaKey)));
    }

    private static double getWinterHumidification(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String humidMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_WHUMIDIFICATIONQ);
        return NumberUtils.toDouble(String.valueOf(partMetaMap.get(humidMetaKey)));
    }

    private static double getSummerHeat(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String heatMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_SHEATQ);
        return NumberUtils.toDouble(String.valueOf(partMetaMap.get(heatMetaKey)));
    }

    private static double getWinterHeat(SectionTypeEnum sectionType, Map<String, Object> partMetaMap) {
        String heatMetaKey = SectionMetaUtils.getMetaSectionKey(sectionType, MetaKey.KEY_WHEATQ);
        return NumberUtils.toDouble(String.valueOf(partMetaMap.get(heatMetaKey)));
    }

    public static String getHumidQForWeight(double humid) {
        int roundupHumid = Long.valueOf(Math.round(humid)).intValue();
        for (int i = 0; i < HUMIDQ.length; i++) {
            int humidQ = HUMIDQ[i];
            if (humidQ >= roundupHumid) {
                return String.valueOf(humidQ);
            }
        }
        return String.valueOf(HUMIDQ[0]);
    }

}
