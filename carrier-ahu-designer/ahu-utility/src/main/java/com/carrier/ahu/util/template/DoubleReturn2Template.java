package com.carrier.ahu.util.template;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_oUTLETDIRECTION;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OUTLETDIRECTION_THF;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.util.meta.SectionMetaUtils;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class DoubleReturn2Template extends TemplateFactory {

    public DoubleReturn2Template(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.DOUBLE_RETURN_2;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] { SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_MIX,
                SectionTypeEnum.TYPE_SINGLE, SectionTypeEnum.TYPE_ACCESS, SectionTypeEnum.TYPE_COMPOSITE,
                SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_FAN };
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Part> preConfigureSections(List<Part> parts) {
        String[] outletDirectionsOfFan = { FAN_OUTLETDIRECTION_THF, FAN_OUTLETDIRECTION_UBF };
        int fanIndex = 0;
        List<Part> updatedParts = new ArrayList<Part>();
        for (Part part : parts) {
            String metaJson = part.getMetaJson();
            Map<String, Object> partMeta = JSONArray.parseObject(metaJson, Map.class);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(part.getSectionKey());

            /* 上层回风、下层送风 */
            if (getSectionLayout().isInReturnAirLayer(part.getPosition())) {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.RETURNAIR.getCode());
            } else {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            if (SectionTypeEnum.TYPE_FAN.equals(sectionType)) {
                partMeta.put(SectionMetaUtils.getMetaSectionKey(sectionType, KEY_oUTLETDIRECTION),
                        outletDirectionsOfFan[fanIndex]);
                fanIndex++;
            }
            part.setMetaJson(JSONArray.toJSONString(partMeta));
            updatedParts.add(part);
        }
        return updatedParts;
    }

}
