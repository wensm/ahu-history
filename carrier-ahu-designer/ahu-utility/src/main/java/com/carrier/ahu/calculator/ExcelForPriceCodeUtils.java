package com.carrier.ahu.calculator;

import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_FAN;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_HEATINGCOIL;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_FAN_MODEL;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_HUMIDIFICATION;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER;
import static com.carrier.ahu.vo.SystemCalculateConstants.BOX_SECTION_CODE;
import static com.carrier.ahu.vo.SystemCalculateConstants.DEFAULT_SCALE;
import static com.carrier.ahu.vo.SystemCalculateConstants.SECTION_BOX;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.report.SAPFunction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.calculator.price.CalPriceTool;
import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.calculator.price.PriceCodeXSLXPO;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.util.section.FanSectionUtils;
import com.carrier.ahu.util.section.HumidifierUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.google.common.primitives.Ints;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ExcelForPriceCodeUtils {

    private static final String RESERVED = "yl";
    private static final String RESERVED_PRICE_CODE = "0";

    public static final String DEFAULT_PRICE_CODE = "*";
    private static final String DEFAULT_META_VALUE = "default";
    private static final String CONVERT_SLASH = "\\";
    private static final String META_VALUE_OR = "|";
    private static final String META_VALUE_HASH = "#";
    private static final String META_VALUE_NUM_PAD = "0";

    private static Map<String, Map<String, List<PriceCodeXSLXPO>>> priceCodeDataMap = new HashMap<>();
    private static Map<String, Integer> priceCodeLengthMap = new HashMap<>();

    static {
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SECTION_BOX);
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_MIX.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_SINGLE.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_COMPOSITE.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_COLD.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_HEATINGCOIL.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_STEAMCOIL.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_FAN.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_ATTENUATOR.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_DISCHARGE.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_ACCESS.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_HEPAFILTER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId());
        load(FileNamesLoadInSystem.PRICE_CODERULE_XLSX, SectionTypeEnum.TYPE_CTR.getId());
    }

    private static void load(String filePath, String sectionType) {
        String sheetName = sectionType;
        if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionType)
                || SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionType)) {
            sheetName = SectionTypeEnum.TYPE_HEATRECYCLE.getId();
        } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionType)) {
            sheetName = SectionTypeEnum.TYPE_COLD.getId();
        }

        InputStream is = ExcelForPriceCodeUtils.class.getClassLoader().getResourceAsStream(filePath);
        List<List<String>> priceCodeLines = Collections.emptyList();
        try {
            priceCodeLines = ExcelUtils.read(is, ExcelUtils.isExcel2003(filePath), sheetName, 1);
        } catch (IOException e) {
            log.error("Failed to load excel: " + filePath, e);
        } catch (InvalidFormatException e) {
            log.error("Failed to load excel: " + filePath, e);
        }

        Map<String, List<PriceCodeXSLXPO>> priceCodePositionMap;
        if (priceCodeDataMap.containsKey(sectionType)) {
            priceCodePositionMap = priceCodeDataMap.get(sectionType);
        } else {
            priceCodePositionMap = new HashMap<>();
        }

        // parse price code lines
        for (List<String> priceCodeLine : priceCodeLines) {
            String no = fixNumberic(priceCodeLine.get(0));
            String name = priceCodeLine.get(1);
            String description = priceCodeLine.get(2);
            String valueType = priceCodeLine.get(3);
            String value = priceCodeLine.get(4);
            String valueDesp = priceCodeLine.get(5);
            String code = priceCodeLine.get(6);
            String group = priceCodeLine.get(7);
            String metaKey = priceCodeLine.get(8);
            String metaValue = priceCodeLine.get(9);

            PriceCodeXSLXPO priceCodePo = new PriceCodeXSLXPO();
            priceCodePo.setSectionKey(sectionType);
            priceCodePo.setNo(no);
            priceCodePo.setName(name);
            priceCodePo.setDescription(description);
            priceCodePo.setValueType(valueType);
            priceCodePo.setValue(value);
            priceCodePo.setValueDesp(valueDesp);
            priceCodePo.setCode(fixNumberic(code));
            priceCodePo.setGroup(group);
            priceCodePo.setMetaKey(metaKey);
            priceCodePo.setMetaValue(metaValue);

            // group price code based on position
            if (priceCodePositionMap.containsKey(no)) {
                List<PriceCodeXSLXPO> priceCodePos = priceCodePositionMap.get(no);
                priceCodePos.add(priceCodePo);
                priceCodePositionMap.put(no, priceCodePos);
            } else {
                List<PriceCodeXSLXPO> priceCodePos = new ArrayList<>();
                priceCodePos.add(priceCodePo);
                priceCodePositionMap.put(no, priceCodePos);
            }

            // update the biggest price code length for the section
            if (!priceCodeLengthMap.containsKey(sectionType)) { // set default length 1
                priceCodeLengthMap.put(sectionType, 1);
            }
            Integer priceCodeLength = priceCodeLengthMap.get(sectionType);
            Integer newPriceCodeLength = getNum(no);
            if (priceCodeLength < newPriceCodeLength) {
                priceCodeLengthMap.put(sectionType, newPriceCodeLength);
            }
        }
        priceCodeDataMap.put(sectionType, priceCodePositionMap);
    }

    private static String fixNumberic(String no) {
        try {
            Double n = Double.parseDouble(no);
            return String.valueOf(n.intValue());
        } catch (Exception e) {
            return no;
        }
    }

    private static Integer getNum(String no) {
        try {
            Integer n = Integer.parseInt(no);
            return n;
        } catch (Exception e) {
            return -1;
        }
    }

    public static PriceCodePO getPriceCode(String sectionType, Map<String, Object> allMetaMap) {
        PriceCodePO priceCodePo = new PriceCodePO();
        if (!priceCodeDataMap.containsKey(sectionType)) {
            log.warn(MessageFormat.format("Excel中不存在该段[{0}]的数据", sectionType));
            priceCodePo.setPriceCode(StringUtils.EMPTY);
            return priceCodePo;
        }
        Map<String, List<PriceCodeXSLXPO>> priceCodePosMap = priceCodeDataMap.get(sectionType);

        LinkedList<String> sectionPriceCodes = new LinkedList<>();

        // get section price code at the first position
        int sectionPriceCodeIndex = 1;
        List<PriceCodeXSLXPO> sectionPriceCodePos = priceCodePosMap.get(String.valueOf(sectionPriceCodeIndex));
        PriceCodeXSLXPO sectionPriceCodePo = sectionPriceCodePos.get(0);
        priceCodePo.addCodeRule(sectionPriceCodePo);
        sectionPriceCodes.add(sectionPriceCodePo.getCode());

        // add following price code from second position
        Integer priceCodeLength = priceCodeLengthMap.get(sectionType);
        for (int i = 2; i < priceCodeLength + 1; i++) {
            List<PriceCodeXSLXPO> priceCodePOs = priceCodePosMap.get(String.valueOf(i));
            sectionPriceCodes.add(getPriceCode(priceCodePo, String.valueOf(i), priceCodePOs, sectionType, allMetaMap));
        }
        priceCodePo.setPriceCode(CalPriceTool.transList2String(sectionPriceCodes));
        return priceCodePo;
    }

    private static String getMetaValue(String seqNo, String sectionType, String metaKey, Map<String, Object> allMetaMap) {
        if (metaKey.contains(UtilityConstant.SYS_PUNCTUATION_PERCENTAGE)) { // replace common section name
            metaKey = metaKey.replaceAll(UtilityConstant.SYS_PUNCTUATION_PERCENTAGE, SectionTypeEnum.simpleSectionName(sectionType));
        }
        if (UtilityConstant.METACOMMON_CONSTANCE.equalsIgnoreCase(metaKey)) {
            return UtilityConstant.SYS_STRING_NUMBER_0;
        }
        if (metaKey.contains(UtilityConstant.SYS_PUNCTUATION_SLASH)) {
            String[] metaKeyArray = metaKey.split(UtilityConstant.SYS_PUNCTUATION_SLASH);
            List<String> values = new ArrayList<>();

            for (String key : metaKeyArray) {
                String value = String.valueOf(allMetaMap.get(key));
                if (UtilityConstant.SYS_ASSERT_NULL.equalsIgnoreCase(value)) {
                    String defaultValue = String.valueOf(AhuSectionMetas.getInstance().getAllDefaultValue().get(key));
                    if (UtilityConstant.SYS_ASSERT_FALSE.equalsIgnoreCase(defaultValue)
                            || UtilityConstant.SYS_ASSERT_TRUE.equalsIgnoreCase(defaultValue)) {
                        values.add(UtilityConstant.SYS_ASSERT_FALSE.toUpperCase());
                    } else {
                        values.add(value);
                    }
                } else {
                    values.add(value);
                }
            }
            if (values.size() == metaKeyArray.length) {
                return String.join(UtilityConstant.SYS_PUNCTUATION_SLASH, values);
            }
        } else if (UtilityConstant.METAHU_WIDTH.equals(metaKey) || UtilityConstant.METAHU_HEIGHT.equals(metaKey)) { // get mode value
            String metaValue = String.valueOf(allMetaMap.get(metaKey));
            Double modeValue = Math.floor(Double.valueOf(metaValue) / 100);
            return parseNumberValue(String.valueOf(modeValue.intValue()));
        }
        String metaValue = String.valueOf(allMetaMap.get(metaKey));
		if (metaKey.equals(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB)
				&& (UtilityConstant.SYS_STRING_NUMBER_1 + UtilityConstant.SYS_STRING_NUMBER_3).equals(seqNo)) {
			if (!metaValue.equalsIgnoreCase(UtilityConstant.SYS_STRING_W_O)) {
				metaValue = UtilityConstant.SYS_STRING_HAVE;
			}
		}
		if (metaKey.equals(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP)
				&& (UtilityConstant.SYS_STRING_NUMBER_1 + UtilityConstant.SYS_STRING_NUMBER_5).equals(seqNo)) {
			if (!metaValue.equalsIgnoreCase(UtilityConstant.SYS_STRING_W_O)) {
				metaValue = UtilityConstant.SYS_STRING_HAVE;
			}
		}
		if (metaKey.equals(UtilityConstant.METASEXON_FILTER_PRESSUREGUAGE)
				&& (UtilityConstant.SYS_STRING_NUMBER_1 + UtilityConstant.SYS_STRING_NUMBER_0).equals(seqNo)) {
			if (!metaValue.equalsIgnoreCase(UtilityConstant.SYS_STRING_W_O)) {
				metaValue = UtilityConstant.SYS_STRING_HAVE;
			}
		}
        if (metaKey.equals(UtilityConstant.METASEXON_HEPAFILTER_PRESSUREGUAGE)
                && (UtilityConstant.SYS_STRING_NUMBER_7).equals(seqNo)) {
            if (!metaValue.equalsIgnoreCase(UtilityConstant.SYS_STRING_W_O)) {
                metaValue = UtilityConstant.SYS_STRING_HAVE;
            }
        }
        return parseNumberValue(metaValue);
    }

    public static String getPriceCode(PriceCodePO priceCodePo, String seqNo, List<PriceCodeXSLXPO> priceCodeXSLXPOs, String sectionType,
            Map<String, Object> allMetaMap) {
        if (EmptyUtil.isEmpty(priceCodeXSLXPOs)) {
            return UtilityConstant.SYS_PUNCTUATION_STAR;
        }

        String metaKey = getFirstPriceCodeMetaKey(priceCodeXSLXPOs);
        if ("yl".equalsIgnoreCase(metaKey)) { // check if reserved price code
            return UtilityConstant.SYS_STRING_NUMBER_0;
        }

        String metaValue = getMetaValue(seqNo, sectionType, metaKey, allMetaMap);
        for (PriceCodeXSLXPO priceCodeXSLXPO : priceCodeXSLXPOs) {
            if (!priceCodeXSLXPO.getMetaKey().equals(metaKey)) { // if use different key in same position
                metaKey = priceCodeXSLXPO.getMetaKey();
                metaValue = getMetaValue(seqNo, sectionType, metaKey, allMetaMap);
            }
            if (UtilityConstant.SYS_ASSERT_TRUE.equalsIgnoreCase(priceCodeXSLXPO.getMetaValue())
                    || UtilityConstant.SYS_ASSERT_FALSE.equalsIgnoreCase(priceCodeXSLXPO.getMetaValue())) {
                metaValue = Boolean.valueOf(metaValue).toString();
            }
            List<String> metaValues = new ArrayList<>();
            String priceCodeMetaValue = parseNumberValue(priceCodeXSLXPO.getMetaValue().toLowerCase());
            if (priceCodeMetaValue.contains(UtilityConstant.SYS_PUNCTUATION_OR)) {
                List<String> priceCodeMetaValues = Arrays
                        .asList(priceCodeMetaValue.split(UtilityConstant.SYS_PUNCTUATION_BACKSLASH + UtilityConstant.SYS_PUNCTUATION_OR));
                priceCodeMetaValues.forEach(value -> {
                    metaValues.add(parseNumberValue(value));
                });
            } else if (priceCodeMetaValue.contains(UtilityConstant.SYS_PUNCTUATION_POUND)) {
                metaValues.add(priceCodeMetaValue);
                metaValue = formatMetaValueByHash(metaValue, priceCodeMetaValue);
            } else {
                metaValues.add(priceCodeMetaValue);
            }
            if (metaKey.equals(priceCodeXSLXPO.getMetaKey()) && metaValues.contains(metaValue.toLowerCase())) {
                log.debug("Find price code: {}-{}-{}", priceCodeXSLXPO.getMetaKey(), priceCodeXSLXPO.getMetaValue(), metaValue);
                priceCodePo.addCodeRule(priceCodeXSLXPO);
                return priceCodeXSLXPO.getCode();
            }
        }
        log.debug("Can not find price code: {}-{}", metaKey , metaValue);
        // use first default code
        return getDefaultPriceCode(priceCodePo, priceCodeXSLXPOs);
    }

    private static String getFirstPriceCodeMetaKey(List<PriceCodeXSLXPO> priceCodeXSLXPOs) {
        String metaKey = priceCodeXSLXPOs.get(0).getMetaKey();
        return metaKey;
    }

    private static String parseNumberValue(String metaValue) {
        return NumberUtil.scale(metaValue, DEFAULT_SCALE);
    }

    private static String formatMetaValueByHash(String metaValue, String hashMetaValue) {
        if (NumberUtils.isParsable(metaValue)) { // support integer only
            metaValue = String.valueOf(Double.valueOf(NumberUtils.toDouble(metaValue)).intValue());
        }
        if (hashMetaValue.length() > metaValue.length()) {
            metaValue = StringUtils.leftPad(metaValue, hashMetaValue.length(), UtilityConstant.SYS_STRING_NUMBER_0);
        } else if (hashMetaValue.length() < metaValue.length()) {
            metaValue = StringUtils.right(metaValue, hashMetaValue.length());
        }
        char[] chValues = metaValue.toCharArray();
        for (int i = 0; i < hashMetaValue.length(); i++) {
            char ch = hashMetaValue.charAt(i);
            if (UtilityConstant.SYS_PUNCTUATION_POUND.equals(String.valueOf(ch))) {
                chValues[i] = ch;
            }
        }
        return String.valueOf(chValues);
    }

    private static String getDefaultPriceCode(PriceCodePO priceCode, List<PriceCodeXSLXPO> priceCodeXSLXPOs) {
        // get default price code if no match meta key
        String defaultPriceCode = null;
        for (PriceCodeXSLXPO priceCodeXSLXPO : priceCodeXSLXPOs) {
            if (UtilityConstant.SYS_STRING_DEFAULT.equals(priceCodeXSLXPO.getMetaValue())) {
                priceCode.addCodeRule(priceCodeXSLXPO);
                defaultPriceCode = priceCodeXSLXPO.getCode();
            }
        }
        if (EmptyUtil.isNotEmpty(defaultPriceCode)) {
            return defaultPriceCode;
        }
        return priceCodeXSLXPOs.isEmpty() ? UtilityConstant.SYS_PUNCTUATION_STAR : priceCodeXSLXPOs.get(0).getCode();
    }

    /**
     * 重置段对象所属分段编号
     * @param part
     * @param partitions
     */
    private static void setPartitionOfPart(Part part, List<AhuPartition> partitions) {
    	
    	if(partitions==null || partitions.size()==0) {
    		part.setPos((short)1);
    		return;
    	}
        for (AhuPartition partition : partitions) {
            LinkedList<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                short posOfSection = AhuPartition.getPosOfSection(section);
                if (part.getPosition().equals(posOfSection)) {
                    part.setPos((short)(1+partition.getPos()));
                    return;
                }
            }
        }
    }
    public static List<PriceCodePO> getPriceCodes(Unit unit, List<Part> parts, List<AhuPartition> partitions) {
        for (Part part : parts) {
            setPartitionOfPart(part, partitions);
        }
        List<Part> airflowParts = AhuPartitionUtils.getAirflowOrderedPartList(unit, parts);

        Map<String, Object> allMetaMap = new HashMap<>();
        if(EmptyUtil.isNotEmpty(unit))
            allMetaMap.put(UtilityConstant.INFO_AHU_SERIES, unit.getProduct());

        allMetaMap.put(UtilityConstant.INFO_AHU_SECTION_COUNT, AhuUtil.getAhuSectionCount(unit.getSeries(), airflowParts));
        allMetaMap.put(UtilityConstant.INFO_AHU_WIDTH, AhuUtil.getWidthOfAHU(unit.getSeries()));
        allMetaMap.put(UtilityConstant.INFO_AHU_HEIGHT, AhuUtil.getHeightOfAHU(unit.getSeries()));

        if(EmptyUtil.isNotEmpty(partitions))
            allMetaMap.put(UtilityConstant.INFO_AHU_BOX_COUNT, partitions.size());

        if(EmptyUtil.isNotEmpty(partitions))
            allMetaMap.put(UtilityConstant.INFO_AHU_BOX_ZYDNUMBER, getZYDNumber(partitions));

        if(EmptyUtil.isNotEmpty(unit))
            allMetaMap.put(UtilityConstant.INFO_HEATRECYCLE_TOPLAYER_SECTIONL, getTopLayerSectionLength(unit, airflowParts));

        if(EmptyUtil.isNotEmpty(unit)) {
            Map<String, Object> unitMetaMap = (Map<String, Object>) JSONObject.parseObject(unit.getMetaJson());
            allMetaMap.putAll(unitMetaMap);
        }

        List<PriceCodePO> priceCodes = new ArrayList<>();
        // add AHU box
        PriceCodePO priceCodePo = ExcelForPriceCodeUtils.getPriceCode(SECTION_BOX, allMetaMap);
        priceCodePo.setSectionCode(BOX_SECTION_CODE);
        priceCodePo.setSectionNo((short)0);
        priceCodePo.setSectionList((short)0);
        priceCodePo.setPartitionNo((short)0);
        priceCodes.add(priceCodePo);

        Map<String, Short> sectionNoMap = new HashMap<>();
        for (int i = 0; i < airflowParts.size(); i++) {
            Part part = airflowParts.get(i);
            Map<String, Object> partMetaMap = (Map<String, Object>) JSONObject.parseObject(part.getMetaJson());
            allMetaMap.putAll(partMetaMap);

            allMetaMap.put("section.type", SectionTypeEnum.getSectionTypeFromId(part.getSectionKey()).getCode());
            allMetaMap.put("followsection", getFollowSection(i, airflowParts));
            allMetaMap.put("previoussection", getPreviousSection(i, airflowParts));

            if (SectionTypeEnum.TYPE_ACCESS.getId().equals(part.getSectionKey())) {
                // 均流段前风机型号
                if (ACCESS_FUNCTION_DIFFUSER.equals(partMetaMap.get(UtilityConstant.METASEXON_ACCESS_FUNCTION))) {
                    Part prePart = getPreviousPart(i, airflowParts);
                    if (SectionTypeEnum.TYPE_FAN.getId().equals(prePart.getSectionKey())) {
                        Map<String, Object> preFanMap = (Map<String, Object>) JSONObject
                                .parseObject(prePart.getMetaJson());
                        String fanModelMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_FAN, KEY_FAN_MODEL);
                        String fanModel = String.valueOf(preFanMap.get(fanModelMetaKey));
                        allMetaMap.put(fanModelMetaKey, NumberUtil.onlyNumbers(fanModel));
                    }
                }
            }
            if (SectionTypeEnum.TYPE_FAN.getId().equals(part.getSectionKey())) {
                String insulation = UtilityConstant.METASEXON_FAN_INSULATION;// 绝缘等级
                String protection = UtilityConstant.METASEXON_FAN_PROTECTION;// 防护等级
                if (partMetaMap.containsKey(insulation)) {
                    String value = String.valueOf(partMetaMap.get(insulation)).trim();
                    allMetaMap.put(insulation, value.substring(0, 1));
                    allMetaMap.put(protection, value.substring(value.length() - 4, value.length()));
                }

                String fanModelMetaKey = UtilityConstant.METASEXON_FAN_FANMODEL;
                if (partMetaMap.containsKey(fanModelMetaKey)) {
                    String fanOutletMetaKey = UtilityConstant.METASEXON_FAN_OUTLET;
                    String fanOutlet = String.valueOf(partMetaMap.get(fanOutletMetaKey));
                    String fanModel = String.valueOf(partMetaMap.get(fanModelMetaKey));
                    String fanModelNumber = NumberUtil.onlyNumbers(fanModel);
                    if (FanSectionUtils.isFrontCurveFan(fanModel)) {
                        fanOutlet = UtilityConstant.SYS_MAP_FAN_FANMODEL_FC;
                        if (FanSectionUtils.isStrengthenFan(fanModel)) {
                            fanOutlet += UtilityConstant.SYS_MAP_FAN_FANMODEL_K;
                        }
                    } else if (FanSectionUtils.isBackCurveFan(fanModel)) {
                        fanOutlet = UtilityConstant.SYS_MAP_FAN_FANMODEL_BC;
                        if (FanSectionUtils.isStrengthenFan(fanModel)) {
                            fanOutlet += UtilityConstant.SYS_MAP_FAN_FANMODEL_K;
                        }
                    }
                    allMetaMap.put(fanOutletMetaKey, fanOutlet);
                    allMetaMap.put(fanModelMetaKey, fanModelNumber);

                    // set fan frame
                    String fanFrameInfoKey = SectionMetaUtils.getInfoSectionKey(TYPE_FAN, KEY_FAN_FRAME);
                    String fanFrame = fanModel.substring(fanModel.indexOf(fanModelNumber) + fanModelNumber.length());
                    allMetaMap.put(fanFrameInfoKey, fanFrame);
                }
            }
            if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(part.getSectionKey())) {
                allMetaMap.put(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_POWER, 0);
            }
            //冷水后面的热水盘管排数
            if (SectionTypeEnum.TYPE_COLD.getId().equals(part.getSectionKey())) {
                Part nextPart = getNextPart(i, airflowParts);
                if (nextPart != null && SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(nextPart.getSectionKey())) {
                    Map<String, Object> nextHeatingCoilMap = (Map<String, Object>) JSONObject
                            .parseObject(nextPart.getMetaJson());
                    String rowsMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_HEATINGCOIL, KEY_ROWS);
                    String rows = String.valueOf(nextHeatingCoilMap.get(rowsMetaKey));
                    allMetaMap.put(rowsMetaKey, rows);
                }else{
                    String rowsMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_HEATINGCOIL, KEY_ROWS);
                    allMetaMap.put(rowsMetaKey, "0");
                }
            }
            // get max humid if both season enabled
            if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(part.getSectionKey())
                    || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(part.getSectionKey())
                    || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(part.getSectionKey())
                    || SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(part.getSectionKey())) {
                String humidMetaKey = SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), KEY_HUMIDIFICATION);
                double humidification = HumidifierUtils.getMaxSeasonHumidification(part.getSectionKey(), partMetaMap);
                allMetaMap.put(humidMetaKey, String.valueOf(humidification));
            }
            // get max heat if both season enabled
            if (SectionTypeEnum.TYPE_STEAMCOIL.getId().equals(part.getSectionKey())
                    || SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(part.getSectionKey())) {
                String heatMetaKey = SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), KEY_HEAT);
                double heat = HumidifierUtils.getMaxSeasonHeat(part.getSectionKey(), partMetaMap);
                allMetaMap.put(heatMetaKey, String.valueOf(heat));
            }
            if (SectionTypeEnum.TYPE_CTR.getId().equals(part.getSectionKey())) {
                Part nextPart = getNextPart(i, airflowParts);
                if (nextPart != null && SectionTypeEnum.TYPE_FAN.getId().equals(nextPart.getSectionKey())) {
                    Map<String, Object> nextFanMap = (Map<String, Object>) JSONObject
                            .parseObject(nextPart.getMetaJson());
                    String motorPowerMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_FAN, KEY_MOTOR_POWER);
                    String motorPower = String.valueOf(nextFanMap.get(motorPowerMetaKey));
                    allMetaMap.put(motorPowerMetaKey, motorPower);
                }
            }
            priceCodePo = ExcelForPriceCodeUtils.getPriceCode(part.getSectionKey(), allMetaMap);
            priceCodePo.setSectionCode(priceCodePo.getPriceCode().substring(0, 1)); // first letter is section code
            priceCodePo.setSectionNo(getSectionNo(part.getSectionKey(),sectionNoMap));
            priceCodePo.setPartitionNo(part.getPos());
            priceCodePo.setSectionList(part.getPosition());
            priceCodePo.setPart(part);
            priceCodes.add(priceCodePo);
        }
        return priceCodes;
    }

    @SuppressWarnings("unchecked")
    private static int getZYDNumber(List<AhuPartition> partitions) {
        for (int i = 0; i < partitions.size(); i++) {
            AhuPartition partition = partitions.get(i);
            LinkedList<Map<String, Object>> sections = partition.getSections();
            String sectionId = AhuPartition.getMetaIdOfSection(sections.getLast());
            if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
                String sectionMetaJson = AhuPartition.getMetaJsonOfSection(sections.getLast());
                Map<String, Object> sectionMeta = JSON.parseObject(sectionMetaJson, HashMap.class);
                String airDirection = String.valueOf(sectionMeta.get(UtilityConstant.METASEXON_AIRDIRECTION));
                if (AirDirectionEnum.SUPPLYAIR.getCode().equals(airDirection)) {
                    return partitions.size() - i - 1;
                }
            }
        }
        return 0;
    }

    private static int getTopLayerSectionLength(Unit unit, List<Part> parts) {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        if (ahuLayout != null && (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()
                || LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle())) { // calculate top layer sections' length
            List<Integer> topLayerSections = new ArrayList<>();
            topLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[0]));
            topLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[1]));
            int topLayerSectionLength = 0;
            for (Part part : parts) {
                if (topLayerSections.contains(part.getPosition().intValue())) {
                    topLayerSectionLength += AhuUtil.getSectionL(part);
                }
            }
            return topLayerSectionLength;
        }
        return 0;
    }

    private static Short getSectionNo(String sectionKey, Map<String, Short> sectionNoMap) {
        if (sectionNoMap.containsKey(sectionKey)) {
            Short sectionNo = sectionNoMap.get(sectionKey);
            sectionNo++;
            sectionNoMap.put(sectionKey, sectionNo);
            return sectionNo;
        } else {
            Short sectionNo = 1; // section no start from 1
            sectionNoMap.put(sectionKey, sectionNo);
            return sectionNo;
        }
    }

    private static Part getNextPart(int index, List<Part> airflowParts) {
        if (index < airflowParts.size() - 1) {
            return airflowParts.get(index + 1);
        }
        return null;
    }

    private static String getFollowSection(int index, List<Part> airflowParts) {
        Part followPart = getNextPart(index, airflowParts);
        if (followPart != null) {
            if (SectionTypeEnum.TYPE_SINGLE.getId().equals(followPart.getSectionKey())) {
                String fitetFMetaKey = SectionMetaUtils.getMetaSectionKey(followPart.getSectionKey(),
                        MetaKey.KEY_FITETF);
                Map<String, Object> partMetaMap = (Map<String, Object>) JSONObject
                        .parseObject(followPart.getMetaJson());
                String fitetF = String.valueOf(partMetaMap.get(fitetFMetaKey));
                return followPart.getSectionKey() + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN + fitetF;
            }
            return followPart.getSectionKey();
        }
        return null;
    }

    private static String getPreviousSection(int index, List<Part> airflowParts) {
        Part previousPart = getPreviousPart(index, airflowParts);
        if (previousPart != null) {
            return previousPart.getSectionKey();
        }
        return null;
    }

    private static Part getPreviousPart(int index, List<Part> airflowParts) {
        if (index > 0) {
            return airflowParts.get(index - 1);
        }
        return null;
    }

}
