package com.carrier.ahu.entity;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/11/28.
 * 热回收段
 */
@Data
public class HeatRecycleInfo {

    private String SectionSideL;//截面边长
    private String HeatExchangerL;//热交换器长度

    /*夏季引擎结果*/
    private String ReturnWheelSize;//转轮尺寸
    private String ReturnSeason;//季节
    private String ReturnSDryBulbT;//送风干球温度送风侧
    private String ReturnSWetBulbT;//送风湿球温度送风侧
    private String ReturnSRelativeT;//送风相对湿度送风侧
    private String ReturnSAirResistance;//送风空气阻力送风侧
    private String ReturnSEfficiency;//送风效率送风侧
    private String ReturnEDryBulbT;//排风干球温度进风侧
    private String ReturnEWetBulbT;//排风湿球温度进风侧
    private String ReturnERelativeT;//排风相对湿度进风侧
    private String ReturnEAirResistance;//排风空气阻力进风侧
    private String ReturnEEfficiency;//排风效率进风侧
    private String ReturnTotalHeat;//全热
    private String ReturnSensibleHeat;//显热

}
