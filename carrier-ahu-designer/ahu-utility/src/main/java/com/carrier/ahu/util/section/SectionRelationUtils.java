package com.carrier.ahu.util.section;

import com.carrier.ahu.metadata.MatrixMetadata;
import com.carrier.ahu.metadata.common.MatrixType;

import lombok.Data;

/**
 * @Description 段关系规则 里面按照纵轴、横轴的坐标所在点的值：1/2/3/4 进行四种关系的绑定 </br>
 *              1-relationEnabled 2- relationMust 3-relationMustUp
 *              4-relationDisableInAhu
 * 
 * @author DongXiangxiang
 * @author JL
 * @author Created by Braden Zhou on 2018/05/11.
 */
@Data
public class SectionRelationUtils {

    public final static String unrecognizedRelation = "-1";
    /**
     * 1-4：
     * 暂时统一逻辑处理。
     */
    public final static String relationEnabled = "1";
    public final static String relationMust = "2";
    public final static String relationMustUp = "3";
    public final static String relationDisableInAhu = "4";

    /**
     * 5：
     * 如果up down 进行校验的时候，down后面的afterdown 满足特定段，即使up down 不满足也可以进行选型；
     * 例如：消音段前后必须有一个空段.	如果消音段后为出风段时，即使前后没有空段，也可以选型；
      */
    public final static String relationAfterDownok = "5";

    public static boolean isEnabled(String before, String after) {
        String relation = MatrixMetadata.findMatrixValue(MatrixType.SECTION_RELATION, before, after);
        return relationEnabled.equals(relation) || relationMust.equals(relation) || relationMustUp.equals(relation)
                || unrecognizedRelation.equals(relation)
                || relationAfterDownok.equals(relation);
    }

    public static boolean isEnabledAfterDown(String down, String afterdown) {
        String relation = MatrixMetadata.findMatrixValue(MatrixType.SECTION_RELATION, down, afterdown);
        return relationAfterDownok.equals(relation);
    }

    public static boolean isFirstRelationEnabled(String first) {
        String relation = MatrixMetadata.findFirstMatrixValue(MatrixType.SECTION_RELATION, first);
        return relationEnabled.equals(relation);
    }

}
