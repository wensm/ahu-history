package com.carrier.ahu.util.template;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class VerticalUnit1Template extends TemplateFactory {

    public VerticalUnit1Template(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.VERTICAL_UNIT_1;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] { SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_MIX,
                SectionTypeEnum.TYPE_COMPOSITE, SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_COLD,
                SectionTypeEnum.TYPE_ACCESS };
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Part> preConfigureSections(List<Part> parts) {
        List<Part> updatedParts = new ArrayList<Part>();
        for (Part part : parts) {
            String metaJson = part.getMetaJson();
            Map<String, Object> partMeta = JSONArray.parseObject(metaJson, Map.class);
            partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.SUPPLYAIR.getCode());
            part.setMetaJson(JSONArray.toJSONString(partMeta));
            updatedParts.add(part);
        }
        return updatedParts;
    }

}
