package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.configuration.AHUContext.getLanguage;
import static com.carrier.ahu.common.intl.I18NConstants.BAG_TYPE;
import static com.carrier.ahu.common.intl.I18NConstants.BOTH_SIDES;
import static com.carrier.ahu.common.intl.I18NConstants.INSTALL_FIX_REPAIR_LAMP;
import static com.carrier.ahu.common.intl.I18NConstants.MOLECULAR_OPTION;
import static com.carrier.ahu.common.intl.I18NConstants.NO_FIX_REPAIR_LAMP;
import static com.carrier.ahu.common.intl.I18NConstants.PANEL_TYPE;
import static com.carrier.ahu.common.intl.I18NConstants.POSITIVE_PRESSURE_DOOR;
import static com.carrier.ahu.common.intl.I18NConstants.PRODUCER_COUNTRY_FIELD;
import static com.carrier.ahu.common.intl.I18NConstants.PURE_ALUMINUM_OPTION;
import static com.carrier.ahu.common.intl.I18NConstants.SILICA_GEL_OPTION;
import static com.carrier.ahu.constant.CommonConstant.SYS_PUNCTUATION_SLIGHT_PAUSE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_AHU_DELIVERY;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.IniConfigMapUtil;
import com.carrier.ahu.entity.HeatRecycleInfo;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.coil.STakeoversize;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.metadata.entity.humidifier.S4xhumid;
import com.carrier.ahu.metadata.entity.report.AdjustAirDoor;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.common.reflect.TypeToken;

/**
 * Created by LIANGD4 on 2018/1/3.
 * 报告使用
 */
@Component
public class SectionContentConvertUtils {
	
	private static String PatchVersion;
	
	@Value("${ahu.patch.version}")
	private void setPatchVersion(String pv) {
		PatchVersion = pv;
	}	

	private static Logger logger = LoggerFactory.getLogger(SectionContentConvertUtils.class);
	
	
	/**
	 * 封装技术说明header，头部内容
	 * 
	 * @param contents
	 * @param unit
	 * @param language
	 * @return
	 */
	public static String[][] getHeader(String[][] contents, Unit unit, LanguageEnum language) {
		String SystemVersion = contents[0][4];
		if (AHUContext.isExportVersion()) {// 判断是否出口版
			contents[0][4] = SystemVersion + UtilityConstant.SYS_BLANK_SPACE + UtilityConstant.SYS_VERSION_EXPORT
					+ UtilityConstant.SYS_BLANK_SPACE + PatchVersion;
		} else {
			contents[0][4] = SystemVersion + UtilityConstant.SYS_BLANK_SPACE + PatchVersion;
		}
		return contents;
	}
	
	public static String[][] getAhuOverall(String[][] contents, Partition partition,
			Unit unit, LanguageEnum language, Map<String, String> allMap) {
		contents[1][2] = UtilityConstant.JSON_UNIT_NAME_AHU;
		String isprerain = BaseDataUtil.constraintString(allMap.get(UtilityConstant.METAHU_ISPRERAIN));
		if (isprerain==null || !UtilityConstant.SYS_ASSERT_TRUE.equals(isprerain)) {
			contents[17][0] = UtilityConstant.SYS_BLANK;
			contents[17][2] = UtilityConstant.SYS_BLANK;			
		}

		return contents;
	}
	
	/**
	 * 技术说明封装ahu非标数据
	 * @param contents
	 * @param partition
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getAhuNS(String[][] contents, Partition partition,
			Unit unit, LanguageEnum language, Map<String, String> allMap) {
		contents = filterContents(contents);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//		
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
//		
//		contents[3][6] = UtilityConstant.SYS_BLANK;
//		contents[3][7] = UtilityConstant.SYS_BLANK;
		
		return contents;
	}
	
	/**
	 * 技术说明封装单层过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getSingleNS(String[][] contents, Unit unit, LanguageEnum language,
			Map<String, String> allMap) {
		contents = filterContents(contents);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
		
		return contents;
	}
	
	/**
	 * 技术说明封装综合过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getCompositeNS(String[][] contents, Unit unit, LanguageEnum language,
			Map<String, String> allMap) {
		contents = filterContents(contents);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
		
		return contents;
	}
	
	/**
	 * 技术说明封装高效过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getHepaNS(String[][] contents, Unit unit, LanguageEnum language,
			Map<String, String> allMap) {
		contents = filterContents(contents);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
		
		return contents;
	}
	
	/**
	 * 技术说明封装风机段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getFanNS(String[][] contents, Unit unit, LanguageEnum language,
			Map<String, String> allMap) {
		
		contents = filterContents(contents);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//		
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
//		
//		contents[3][6] = UtilityConstant.SYS_BLANK;
//		contents[3][7] = UtilityConstant.SYS_BLANK;
//		
//		contents[4][6] = UtilityConstant.SYS_BLANK;
//		contents[4][7] = UtilityConstant.SYS_BLANK;
		return contents;
	}
	
	/**
	 * 封装ahu报告特殊数据
	 * @param content
	 * @param arrayMap
	 * @param partition
	 * @param unit
	 * @param language
	 * @return
	 */
	public static String[][] getAhu(Partition partition,
			Unit unit, LanguageEnum language, Map<String, String> allMap) {
		
		String[][] titleContents = ReportContent.getReportContent(REPORT_TECH_AHU_DELIVERY);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			partitionContents[2] = titleContents[2];
			int i = 3;
			//组装装箱段数据
			for (AhuPartition ap : ahuPartitionList) {
				double partitionWeight = 0;
                if (AhuPartitionUtils.hasEndFacePanel(ap, ahuPartitionList)) {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), true);
                } else {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), false);
                }
//				if(unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
					String series = unit.getSeries();
					int type = Integer.valueOf(series.substring(series.length() - 4, series.length()));
					if (608 <= type && type <= 2333) {
						String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
								UtilityConstant.SYS_BLANK + (Integer.valueOf(ap.getLength() + ap.getCasingWidth())),
								UtilityConstant.SYS_BLANK + (Integer.valueOf(series.substring(series.length() - 2, series.length()))
										* 100 + ap.getCasingWidth()),
//								UtilityConstant.SYS_BLANK + (ap.getHeight() * 100 + ap.getCasingWidth() + (ap.isTopLayer()?0:100)),//如果是topLayer不需要加上底座
                                String.valueOf(BaseDataUtil.stringConversionInteger(wrapHeight(allMap, partition, unit, ap))),
								UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
						partitionContents[i] = apArray;
					} else if (2532 <= type && type <= 3438) {
						String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
								UtilityConstant.SYS_BLANK + (Integer.valueOf(ap.getLength()+ap.getCasingWidth())),
								UtilityConstant.SYS_BLANK + (Integer.valueOf(series.substring(series.length() - 2, series.length()))
										* 100 + ap.getCasingWidth()),
//								UtilityConstant.SYS_BLANK + (ap.getHeight() * 100 + ap.getCasingWidth() + (ap.isTopLayer()?0:200)),
                                String.valueOf(BaseDataUtil.stringConversionInteger(wrapHeight(allMap, partition, unit, ap))),
								UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
						partitionContents[i] = apArray;
					} else {
						String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
								UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()),
								UtilityConstant.SYS_BLANK + (ap.getWidth() * 100 + ap.getCasingWidth()),
								UtilityConstant.SYS_BLANK + (ap.getHeight() * 100 + ap.getCasingWidth()),
								UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
						partitionContents[i] = apArray;
					}
					i++;
//				}else {
//					String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos()+1),
//							UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()), UtilityConstant.SYS_BLANK + ap.getWidth(), UtilityConstant.SYS_BLANK + ap.getHeight(),
//							UtilityConstant.SYS_BLANK + BaseDataUtil.decimalConvert(partitionWeight, 1) };
//					partitionContents[i] = apArray;
//					i++;
//				}
			}
			return partitionContents;
		}
		return null;
	}
	
    //封装综合过滤段报告数据
    public static String[][] getCombinedFilter(String[][] contents, Map<String, String> map) {

        return contents;
    }

    /**
     * 封装混合段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getMix(String[][] contents, Map<String, String> noChangeMap,Map<String, String> map,LanguageEnum language) {

        int length = contents.length;
        //封装混合形式
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;//1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;//2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;//3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;//4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;//5
        String uvLamp = UtilityConstant.METASEXON_MIX_UVLAMP;
        String damperExecutorKey = UtilityConstant.METASEXON_MIX_DAMPEREXECUTOR;
        
        
        
        int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;
        
        String dampertype = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPEROUTLET);//风口接件
        String serial = noChangeMap.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);//风阀材料
        String mixSectionL = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
		String damperExecutor = noChangeMap.get(damperExecutorKey);//执行器
		damperExecutor = MetaSelectUtil.getTextForSelect(damperExecutorKey, damperExecutor, language);
        
        
        int valvem = 1;//镀锌板
		if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(damperMeterial)) {
			valvem = 2;//铝合金
		}
		
        String product = noChangeMap.get(UtilityConstant.METAHU_PRODUCT);
        
        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
		
		if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(dampertype)) {
			boolFL = true;
		}
		
		//风量设置
		setAirVolume(contents, map, 1, 2);
		
		//风阀电动+(执行器)情况
        if (map.containsKey(UtilityConstant.METASEXON_MIX_DAMPEROUTLET)) {
            String value = contents[2][2];
            if (EmptyUtil.isNotEmpty(damperExecutor)) {
            	if ((EmptyUtil.isNotEmpty(dampertype) && UtilityConstant.JSON_MIX_DAMPEROUTLET_FD.equals(dampertype))) {
            		contents[2][2] = value;
				} else if (EmptyUtil.isNotEmpty(dampertype)) {
					contents[2][2] = value + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + damperExecutor
							+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
				}				
			}
        }
        
		
		String accessDoor = UtilityConstant.METASEXON_MIX_DOORO;//是否开门:
        if (!SystemCalculateConstants.MIX_DOORO_NODOOR.equals(accessDoor)) {//开门
        	//添加：
        	String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_DOORDIRECTION));//两侧开门
            String value = contents[2][6];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[2][6] = value;
            }
        }
        String PositivePressureDoor = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
        }else{
            PositivePressureDoor = UtilityConstant.SYS_BLANK;
        }
        String value26 = contents[2][6];
        String fixRepairLamp = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_FIXREPAIRLAMP));//安装检修灯 
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
        	value26 = value26 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}else{
    		value26 = value26 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}
    	contents[2][6] = value26;
        
        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        
        if (map.containsKey(uvLamp)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
            	length--;
            }
        } else {
            length--;
        }
        
        int k=0;
        String[][] newContents = new String[length][7];
        for (int i = 0; i < contents.length; i++) {
        	
        	int returnWF = 1;
			if (i == 6) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
                if (map.containsKey(returnTop) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnTopWF;
                }else{
                    continue;
                }
            }

            if (i == 7) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
                if (map.containsKey(returnButtom) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnButtomWF;
                }else{
                    continue;
                }
            }
            
            if (i == 8) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
                if (map.containsKey(returnLeft) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnLeftWF;
                }else{
                    continue;
                }
            }

            if (i == 9) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
                if (map.containsKey(returnRight) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnRightWF;
                }else{
                    continue;
                }
            }
            
            if (i == 10) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
                if (map.containsKey(returnBack) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnBackWF;
                }else{
                    continue;
                }
            }
            
            if(i == 6 || i == 7 || i == 8 || i == 9 || i == 10){
                DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
                        serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
                String A = UtilityConstant.SYS_BLANK;
                String C = UtilityConstant.SYS_BLANK;
	            
	            int lenth = serial.length();
				String heightStr = serial.substring(lenth - 4, lenth - 2);
				String weightStr = serial.substring(lenth - 2);
				int height = Integer.parseInt(heightStr);
				int width = Integer.parseInt(weightStr);
				
	    		if (EmptyUtil.isEmpty(nsion)) {
	    			logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
	    		} else {
	    			String metrial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
	    			if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
	    				double c1 = Double.parseDouble(nsion.getC1());
	    				A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
	    			} else {
	    				A = nsion.getA();
	    			}
	    			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
	    			
	    			if (returnWF == 1 || returnWF == 2) {
	    				C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 3 || returnWF == 4) {
	    				C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 5) {
	    				C = UtilityConstant.SYS_BLANK+((width - 4) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    		}
	    		if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
	    			contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
	    		}
                AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), "A", UtilityConstant.SYS_BLANK + returnWF);
                if (EmptyUtil.isEmpty(ad)) {
					logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
				} else {
					if(!boolFL){
						if(2 == valvem){
							contents[i][6] = ad.getWringqAl();//回风阀门扭矩
						}else{
							contents[i][6] = ad.getWringq();//回风阀门扭矩
						}
					}
				}
            }
            
            if (i == 13) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
                if (map.containsKey(uvLamp) && value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                        continue;
                }
            }
            for (int j = 0; j < 7; j++) {
                newContents[k][j] = contents[i][j];
            }
            k++;
        }
        return newContents;
    }

    /**
     * 封装单层过滤段数据
     * @param noChangeMap
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSingle(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
    	
    	return contents;
    }
    
    /**
     * 封装综合过滤段
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getComposite(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
    	
    	return contents;
    }
    
    /**
     * 该方法需要重构
     */
    public static String[][] getMix4Sale(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        int length = contents.length;
        // 封装混合形式
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;// 1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;// 2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;// 3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;// 4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;// 5
        int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;

        String serial = noChangeMap.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);//风阀材料
        String mixSectionL = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
        int valvem = 1;//镀锌板
        if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(damperMeterial)) {
            valvem = 2;//铝合金
        }
        
        String product = noChangeMap.get(UtilityConstant.METAHU_PRODUCT);
        
        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
        
        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        int k = 0;
        String[][] newContents = new String[length][7];
        for (int i = 0; i < contents.length; i++) {
            int returnWF = 1;
            if (map.containsKey(returnTop)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 2) {
                        continue;
                    }
                }else{
                    returnWF = returnTopWF;
                }
            } else {
                if (i == 2) {
                    continue;
                }
            }

            if (map.containsKey(returnButtom)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 3) {
                        continue;
                    }
                }else{
                    returnWF = returnButtomWF;
                }
            } else {
                if (i == 3) {
                    continue;
                }
            }
            
            if (map.containsKey(returnLeft)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 4) {
                        continue;
                    }
                }else{
                    returnWF = returnLeftWF;
                }
            } else {
                if (i == 4) {
                    continue;
                }
            }

            if (map.containsKey(returnRight)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 5) {
                        continue;
                    }
                }else{
                    returnWF = returnRightWF;
                }
            } else {
                if (i == 5) {
                    continue;
                }
            }
            
            if (map.containsKey(returnBack)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 6) {
                        continue;
                    }
                }else{
                    returnWF = returnBackWF;
                }
            } else {
                if (i == 6) {
                    continue;
                }
            }

            if(i == 2 || i == 3 || i == 4 || i == 5 || i == 6){
                DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
                        serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
                String A = UtilityConstant.SYS_BLANK;
                String C = UtilityConstant.SYS_BLANK;
                
                int lenth = serial.length();
                String heightStr = serial.substring(lenth - 4, lenth - 2);
                String weightStr = serial.substring(lenth - 2);
                int height = Integer.parseInt(heightStr);
                int width = Integer.parseInt(weightStr);
                
                if (EmptyUtil.isEmpty(nsion)) {
                    logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
                } else {
                    String metrial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
                    if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
                        double c1 = Double.parseDouble(nsion.getC1());
                        A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
                    } else {
                        A = nsion.getA();
                    }
                    int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
                    
                    if (returnWF == 1 || returnWF == 2) {
                        C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
                    }
                    if (returnWF == 3 || returnWF == 4) {
                        C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
                    }
                    if (returnWF == 5) {
                        C = UtilityConstant.SYS_BLANK+((width - 4) * 100 + DIMENSION_C_calPara - 72);
                    }
                }
                if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
                    contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                }
                AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), "A", UtilityConstant.SYS_BLANK + returnWF);
                if (EmptyUtil.isEmpty(ad)) {
                    logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
                } else {
                    if(!boolFL){
                        if(2 == valvem){
                            contents[i][6] = ad.getWringqAl();//回风阀门扭矩
                        }else{
                            contents[i][6] = ad.getWringq();//回风阀门扭矩
                        }
                    }
                }
            }
            for (int j = 0; j < 7; j++) {
                newContents[k][j] = contents[i][j];
            }
            k++;
        }
        return newContents;
    }

    /**
     * 封装风机段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getFan(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map,LanguageEnum language) {

        String accessDoor = UtilityConstant.METASEXON_FAN_ACCESSDOOR;//是否开门:
        
        //是否开门需要添加：
        String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
        String fixRepairLamp = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP));//安装检修灯 
        
        if (map.containsKey(accessDoor)) {
            String value = contents[1][7];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[1][7] = value;
            }
        }
        String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
        }else{
            PositivePressureDoor = UtilityConstant.SYS_BLANK;
        }

        String value17 = contents[1][7];
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
        	value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}else{
    		value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}
    	contents[1][7] = value17;
    	
    	//计算轴功率
    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
    	double totalPressure = Double.parseDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
    	contents[4][7] = String.valueOf(shaftPowerBd);
    	
    	//计算输入功率
    	double motorSafetyFacor = new Double(0);
    	double motorPower = BaseDataUtil.stringConversionDouble((String)noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	if(motorPower<0.5) {
    		motorSafetyFacor = 1.5;
    	}else if (motorPower>0.5 && motorPower<1) {
			motorSafetyFacor = 1.4;
		}else if (motorPower>1 && motorPower<2) {
			motorSafetyFacor = 1.3;
		}else if (motorPower>2 && motorPower<5) {
			motorSafetyFacor = 1.2;
		}else if(motorPower>5) {
			motorSafetyFacor = 1.15;
		}
    	double actualMotorPower = (shaftPower*motorSafetyFacor)/(1);
    	System.out.println("#############################actualMotorPower=========="+actualMotorPower);
    	String motorEfficiencyStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
    	NumberFormat nf=NumberFormat.getPercentInstance();
    	Number motorEfficiencyNum;
		try {
			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);				
			}else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if(motorEfficiency==0) {
	    		motorEfficiency = 0.94;
	    	}
			double inputPower = (motorPower) / (motorEfficiency);
			BigDecimal inputPowerBd = new BigDecimal(inputPower);// 四舍五入，保留两位小数
			inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
			contents[6][7] = String.valueOf(inputPowerBd);
			if (UtilityConstant.SYS_STRING_NUMBER_4.equals(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE))) {// 双速电机输入功率从表s_twospeedmotor表中取
				int fanPole = BaseDataUtil
						.stringConversionInteger(noChangeMap.get(UtilityConstant.META_SECTION_FAN_POLE));
				double twoSpeedMotorPower = BaseDataUtil
						.stringConversionDouble(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
				List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findAll(STwoSpeedMotor.class);
				for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
					if (sTwoSpeedMotor.getJs() == fanPole && sTwoSpeedMotor.getGl() == twoSpeedMotorPower) {
						contents[6][7] = sTwoSpeedMotor.getInputPower();
						break;
					}
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
		//添加电机效率
		String oldValue = contents[12][7];
		String outlet = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_OUTLET)));//风机形式
		String type = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE));//电机类型
		String motorEff = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));//电机效率
		String hz = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_HZ)));//变频频率
		String twoSpeed = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFFPOLE)));//变频频率
		String runningRage=BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_FREQUENCYRANGE)));
		if (UtilityConstant.SYS_STRING_NUMBER_6.equals(type)) {//无蜗壳风机，电机类型后边的括号中展示变频频率
			if (EmptyUtil.isNotEmpty(hz)) {
				contents[12][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_FREQUENCY) + hz + "）*";				
			}
		}else if(UtilityConstant.SYS_STRING_NUMBER_5.equals(type)){
				contents[12][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_RANGE) + runningRage + getIntlString(I18NConstants.FAN_VMOTOR_COMMENTS)+ "）*";				
		}else if(UtilityConstant.SYS_STRING_NUMBER_4.equals(type)) {
			contents[12][7]=oldValue + "（" + twoSpeed + "）*";
		}else{//其他形式风机，电机类型后边的括号中展示电机效率
			contents[12][7]= oldValue + "（" + motorEff + "）*";
		}
		
		//根据风向（送/回），分别设置风量、风口接件
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
        if (map.containsKey(airDirection)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(airDirection));
            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
            	//设置风量
            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[14][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);
				contents[14][2] = temp[2] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[3];
            }else {//送风
            	//设置风量
            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[14][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);
				contents[14][2] = temp[0] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[1];
            }
        }
		
		//设置风量
        setAirVolume(contents, map, 4, 2);
        
        return contents;
    }

    public static String[][] getFan4Sale(String[][] contents, Map<String, String> map, LanguageEnum language) {
        // 添加电机效率
        String oldValue = contents[3][7];
        String motorEff = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_MOTOREFF));// 电机效率
        contents[3][7] = oldValue + "（" + motorEff + "）*";
        return contents;
    }

    /**
     * 封装消音段
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getAttenuator(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
    	
    	return contents;
    }
    
    /**
     * 封装转轮热回收特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getWheelHeatRecycle(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	
    	String Model = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_MODEL));
    	if(EmptyUtil.isNotEmpty(Model)) {
    		if (UtilityConstant.SYS_STRING_NUMBER_1.equals(Model)) {
				contents[3][2] = getIntlString(MOLECULAR_OPTION);
			}else if (UtilityConstant.SYS_STRING_NUMBER_2.equals(Model)) {
				contents[3][2] = getIntlString(SILICA_GEL_OPTION);
			}else if (UtilityConstant.SYS_STRING_NUMBER_3.equals(Model)) {
				contents[3][2] = getIntlString(PURE_ALUMINUM_OPTION);
			}
    	}
    	
    	String wheelHeatRecycle = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_HEATRECYCLEDETAIL));
    	
		List<HeatRecycleInfo> heatRecycleInfo = new ArrayList<HeatRecycleInfo>();
		if (EmptyUtil.isNotEmpty(wheelHeatRecycle)) {
			heatRecycleInfo = JSON.parseObject(wheelHeatRecycle, new TypeToken<List<HeatRecycleInfo>>() {
			}.getType());
		}

    	if (heatRecycleInfo.size() > 0) {
			for (int i = 0; i < heatRecycleInfo.size(); i++) {
				HeatRecycleInfo hri = (HeatRecycleInfo)heatRecycleInfo.get(i);
				String season = hri.getReturnSeason();
				if(EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_SUMMER.equals(season)) {//填充夏季数据
					contents[13][2] = hri.getReturnTotalHeat();//全热
					contents[14][2] = hri.getReturnSensibleHeat();//显热
					String qianreS = String.valueOf(BaseDataUtil.stringConversionDouble(contents[13][2])
							- BaseDataUtil.stringConversionDouble(contents[14][2]));// 潜热
					BigDecimal qianreSBd = new BigDecimal(qianreS);//四舍五入，保留两位小数
					qianreSBd = qianreSBd.setScale(2, BigDecimal.ROUND_HALF_UP);
			    	contents[15][2] = String.valueOf(qianreSBd);
			    	
					contents[20][2] = hri.getReturnEDryBulbT();//排风干球温度
					contents[21][2] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();//排风湿球/相对湿度
				}
				if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_WINTER.equals(season)) {// 填充冬季数据
					contents[13][5] = hri.getReturnTotalHeat();//全热
					contents[14][5] = hri.getReturnSensibleHeat();//显热
					String qianreW = String.valueOf(BaseDataUtil.stringConversionDouble(contents[13][5])
							- BaseDataUtil.stringConversionDouble(contents[14][5]));// 潜热
					BigDecimal qianreWBd = new BigDecimal(qianreW);//四舍五入，保留两位小数
					qianreWBd = qianreWBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					contents[15][5] = String.valueOf(qianreWBd);
					
					contents[20][5] = hri.getReturnEDryBulbT();//排风干球温度
					contents[21][5] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();//排风湿球/相对湿度
				}
			}
		}
    	
    	contents[18][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINDRYBULBT));
    	contents[18][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINDRYBULBT));
    	
		contents[19][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINRELATIVET));
		contents[19][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINRELATIVET));
    	
    	return contents;
    }
    
    /**
     * 封装板式热回收特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
	public static String[][] getPlateHeatRecycle(String[][] contents, Map<String, String> map, LanguageEnum language) {

		String plateHeatRecycle = BaseDataUtil
				.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_HEATRECYCLEDETAIL));

		List<HeatRecycleInfo> heatRecycleInfo = new ArrayList<HeatRecycleInfo>();
		if (EmptyUtil.isNotEmpty(plateHeatRecycle)) {
			heatRecycleInfo = JSON.parseObject(plateHeatRecycle, new TypeToken<List<HeatRecycleInfo>>() {
			}.getType());
		}

		if (heatRecycleInfo.size() > 0) {
			for (int i = 0; i < heatRecycleInfo.size(); i++) {
				HeatRecycleInfo hri = (HeatRecycleInfo) heatRecycleInfo.get(i);
				String season = hri.getReturnSeason();
				if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_SUMMER.equals(season)) {// 填充夏季数据
					contents[12][2] = hri.getReturnTotalHeat();// 全热
					contents[13][2] = hri.getReturnSensibleHeat();// 显热
					String qianreS = String.valueOf(BaseDataUtil.stringConversionDouble(contents[12][2])
							- BaseDataUtil.stringConversionDouble(contents[13][2]));// 潜热
					BigDecimal qianreSBd = new BigDecimal(qianreS);// 四舍五入，保留两位小数
					qianreSBd = qianreSBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					contents[14][2] = String.valueOf(qianreSBd);
					
					contents[10][2] = hri.getReturnSDryBulbT();//送风干球温度
					contents[11][2] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度
					
					contents[19][2] = hri.getReturnEDryBulbT();// 排风干球温度
					contents[20][2] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();// 排风湿球/相对湿度
				}
				if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_WINTER.equals(season)) {// 填充冬季数据
					contents[12][5] = hri.getReturnTotalHeat();// 全热
					contents[13][5] = hri.getReturnSensibleHeat();// 显热
					String qianreW = String.valueOf(BaseDataUtil.stringConversionDouble(contents[12][5])
							- BaseDataUtil.stringConversionDouble(contents[13][5]));// 潜热
					BigDecimal qianreWBd = new BigDecimal(qianreW);// 四舍五入，保留两位小数
					qianreWBd = qianreWBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					contents[14][5] = String.valueOf(qianreWBd);
					
					contents[10][5] = hri.getReturnSDryBulbT();//送风干球温度
					contents[11][5] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度
					
					contents[19][5] = hri.getReturnEDryBulbT();// 排风干球温度
					contents[20][5] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();// 排风湿球/相对湿度
				}
			}
		}
		
		contents[8][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWDRYBULBT));
		contents[8][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWDRYBULBT));
		
		contents[9][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWRELATIVET));
		contents[9][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWRELATIVET));
		
//		contents[17][2] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInDryBulbT"));
//		contents[17][5] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInDryBulbT"));
//
//		contents[18][2] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInWetBulbT")) + UtilityConstant.SYS_PUNCTUATION_SLASH
//				+ BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInRelativeT"));
//		contents[18][5] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInWetBulbT")) + UtilityConstant.SYS_PUNCTUATION_SLASH
//				+ BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInWetBulbT"));

		return contents;
	}
    
    /**
     * 封装湿膜加湿段特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getWetFilmHumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
    	
    	contents[2][2] = BaseDataUtil.constraintString(BaseDataUtil.stringConversionInteger(BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_THICKNESS))));
    	return contents;
    }
    
    /**
     * 封装高压喷雾加湿段
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSprayhumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);    	
    	return contents;
    }
    
    /**
     * 封装风机曲线图数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getFan4(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	
    		
    	String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
    	String airVolumeStr = "";
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// R--回风
				airVolumeStr = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// S--送风
				airVolumeStr = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}
		contents[21][1] = airVolumeStr;
		
		//计算轴功率    
//    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
		double airVolume = BaseDataUtil.stringConversionDouble(airVolumeStr);
    	double totalPressure = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
    	contents[27][1] = String.valueOf(shaftPowerBd);
    	
    	if("en-US".equals(language.getId())) {
    		
    	}
    	
    	//电机额定功率
    	double motorPower = BaseDataUtil.stringConversionDouble((String)map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	contents[28][1] = String.valueOf(motorPower);
    	
    	return contents;
    }
    
    /**
     * 封装冷水盘管报告特殊数据
     * @param contents
     * @param map
     * @param unitType 
     * @return
     */
    public static String[][] getCoolingCoil(String[][] contents, Map<String, String> map,LanguageEnum language, UnitSystemEnum unitType) {

        /*String accessDoor = UtilityConstant.METASEXON_FAN_ACCESSDOOR;//是否开门:
        String serial = map.get(UtilityConstant.METAHU_SERIAL);
        CoilInfo sCoilInfo = PDFDataGen.getInstance().getSCoilInfo(serial, "6", "FL");
        double T_No = Double.parseDouble(sCoilInfo.getT_no());
        double Cube_len = Double.parseDouble(sCoilInfo.getT_len());
        double Cube_height = BaseDataUtil.decimalConvert(T_No*31.75,0);
        
		if(UnitSystemEnum.M.getId().equals(unitType)) {
			
		} else if (UnitSystemEnum.B.getId().equals(unitType)) {
			//英制替换值前面cell 的单位
			contents[2][1] = "inch";
		}
		
		
		
		
        if(sCoilInfo.isHasOther()){
        	for (int i = 0; i < sCoilInfo.getOthres().size(); i++) {
        		T_No = Double.parseDouble(sCoilInfo.getOthres().get(i).getT_no());
                Cube_len = Double.parseDouble(sCoilInfo.getOthres().get(i).getT_len());
                Cube_height = BaseDataUtil.decimalConvert(T_No*31.75,0);
                
			}
        }*/
        
    	String waterCollectingMaterial = map.get(UtilityConstant.METASEXON_COOLINGCOIL_WATERCOLLECTING);
    	String pipeMaterial = map.get(UtilityConstant.METASEXON_COOLINGCOIL_PIPE);
    	String waterCollectingM = UtilityConstant.SYS_BLANK;
    	String pipeM = UtilityConstant.SYS_BLANK;
    	String report = UtilityConstant.SYS_BLANK;
    	if(!StringUtils.isEmpty(waterCollectingMaterial)) {
    		if("steel".equals(waterCollectingMaterial)) {
    			waterCollectingM = "钢";
    		}else if ("copper".equals(waterCollectingMaterial)) {
				waterCollectingM = "铜";
			}else if ("steelH".equals(waterCollectingMaterial)) {
				waterCollectingM = "钢(高耐久)";
			}
    	}
    	if(!StringUtils.isEmpty(pipeMaterial)) {
    		if("steel".equals(pipeMaterial)) {
    			pipeM = "钢";
    		}
    	}
    	report = pipeM+UtilityConstant.SYS_PUNCTUATION_SLASH+waterCollectingM;
		contents[10][6] = report;
		
		//夏季水流量单位m³/h转换成L/s，数值相应也要换算
		String SreturnWaterFlow = String.valueOf(map.get(UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWATERFLOW));
    	double srwf = UnitConversionUtil.m3hTols(Double.parseDouble(SreturnWaterFlow), 2);
		contents[21][5] = "L/s";
		contents[21][6] = String.valueOf(srwf);
		
		//设置风量
		setAirVolume(contents, map, 8, 2);
		
        return contents;
    }
    
    public static String[][] getCoolingCoil4Sale(String[][] contents, Map<String, String> map,LanguageEnum language, UnitSystemEnum unitType) {
        //夏季水流量单位m³/h转换成L/s，数值相应也要换算
        String SreturnWaterFlow = map.get(UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWATERFLOW);
        double srwf = UnitConversionUtil.m3hTols(Double.parseDouble(SreturnWaterFlow), 2);
        contents[9][5] = "L/s";
        contents[9][6] = String.valueOf(srwf);

        return contents;
    }

    /**
     * 冷水盘管夏季报告封装特殊数据
     * @param contents
     * @param map
     * @param language
     * @param unitType
     * @return
     */
    public static String[][] getCoolingCoil1(String[][] contents, Map<String, String> map,LanguageEnum language, UnitSystemEnum unitType) {
        
    	//冬季水流量单位m³/h转换成L/s，数值相应也要换算
    	String WreturnWaterFlow = map.get(UtilityConstant.METASEXON_COOLINGCOIL_WRETURNWATERFLOW);
    	double wrwf = UnitConversionUtil.m3hTols(Double.parseDouble(WreturnWaterFlow), 2);

    	contents[5][5] = "L/s";
    	contents[5][6] = String.valueOf(wrwf);
        return contents;
    }
    
    /**
     * 封装直接蒸发式盘管
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getDirectexpensioncoil(String[][] contents, Map<String, String> map,LanguageEnum language){
    	//设置风量
    	setAirVolume(contents, map, 8, 2);
    	
    	return contents;
    }
    
    
    /**
     * 封装热水盘管报告特殊数据
     * @param contents
     * @param map
     * @param unitType
     * @return
     */
    public static String[][] getHeatingCoil(String[][] contents, Map<String, String> map,LanguageEnum language, UnitSystemEnum unitType) {

        String pipe = UtilityConstant.METASEXON_HEATINGCOIL_PIPE;// 进水管
        String waterCollection = UtilityConstant.METASEXON_HEATINGCOIL_WATERCOLLECTION;// 集水管材质
        StringBuffer pipeWaterCollectionSBF = new StringBuffer();
        if (map.containsKey(pipe)) {
            String value = map.get(pipe).toString();
            pipeWaterCollectionSBF.append(AhuSectionMetas.getInstance().getValueByUnit(value, pipe,
                    SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));
        }
        pipeWaterCollectionSBF.append(UtilityConstant.SYS_PUNCTUATION_SLASH);
        if (map.containsKey(waterCollection)) {
            String value = map.get(waterCollection).toString();
            pipeWaterCollectionSBF.append(AhuSectionMetas.getInstance().getValueByUnit(value, waterCollection,
                    SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));
        }
        contents[11][5] = pipeWaterCollectionSBF.toString();
        
        //翅片类型
        String finType = UtilityConstant.METASEXON_HEATINGCOIL_FINTYPE;
        String ft = map.get(finType);
        
        if(!EmptyUtil.isEmpty(ft)) {
        	if ("al".equals(ft)) {
				ft = "普通铝翅片";
			}else if("procoatedAl".equals(ft)) {
				ft = "亲水铝翅片";
			}else if("copper".equals(ft)) {
				ft = "铜翅片";
			}
        }
        contents[10][5] = ft;
        
        //防冻开关
        String frostprotectionoption = UtilityConstant.METASEXON_HEATINGCOIL_FROSTPROTECTIONOPTION;//防冻开关
        String content = "";
        if(map.containsKey(frostprotectionoption)) {
        	String is = BaseDataUtil.constraintString(map.get(frostprotectionoption));
        	if(is!=null && UtilityConstant.SYS_ASSERT_TRUE.equals(is)) {
				content = I18NBundle.getString("switch_capacity_colon", language)
						+ I18NBundle.getString("8A,24~250VAC", language)
						+ I18NBundle.getString("work_environment_colon", language)
						+ I18NBundle.getString("factory_only_components_no_control", language);
	        	contents[15][1]= content;
        	}else {
            	contents[15][0]= UtilityConstant.SYS_BLANK;
            	contents[15][1]= UtilityConstant.SYS_BLANK;
            }
        }
        
        //设置风量
        setAirVolume(contents, map, 9, 2);
        return contents;
    }
    
    /**
     * 组装干蒸加湿段特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSteamHumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	
    	String ControlM = map.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_CONTROLM);
    	if (EmptyUtil.isNotEmpty(ControlM)) {
			if ("EC".equals(ControlM)) {
				contents[1][5] = "电动（电磁阀）";
			}
		}
    	
    	return contents;
    }
    
    /**
     * 封装柏诚报告 (filter 表格)
     * @param contents
     * @param allMap
     * @param part
     * @return
     */
    public static String[][] getBCFilter(String[][] contents, Map<String,String> allMap, SectionTypeEnum part) {
        /*[
              [ "种类", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK ],
              [ "面积（㎡）", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK ]
        ]*/

        StringBuffer partName = new StringBuffer(getIntlString(part.getCnName()));
        switch (part) {
            case TYPE_SINGLE: {
                String filterF = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_FILTER_FITETF,
                        allMap.get(UtilityConstant.METASEXON_FILTER_FITETF),
                        getLanguage());
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_FILTER_FILTEREFFICIENCY);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+filterF+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(filterEfficiency);
                break;
            }
            case TYPE_COMPOSITE: {

                String LMaterial = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIAL,
                        allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIAL),
                        getLanguage());
                String LMaterialE = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIALE);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+LMaterial+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(LMaterialE);


                String RMaterial = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIAL,
                        allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIAL),
                        getLanguage());
                String RMaterialE = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIALE);
                partName.append(";("+RMaterial+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(RMaterialE);

                break;
            }
            case TYPE_HEPAFILTER: {
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_HEPAFILTER_FILTEREFFICENCY);
                partName.append(filterEfficiency);
                break;
            }
            case TYPE_ELECTROSTATICFILTER: {

                String filterType = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTERTYPE,
                        allMap.get(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTERTYPE),
                        getLanguage());
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTEREFFICIENCY);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+filterType+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(filterEfficiency);
                break;
            }
        }
        contents[0][3] = partName.toString();

        String filterArrange = allMap.get(UtilityConstant.METASEXON_PREFIX+(part.getId().split("\\.")[1])+".FilterArrange");
        JSONArray faArray = JSONArray.parseArray(filterArrange);
        StringBuffer filterStrs = new StringBuffer();
        for (int i = 0; i < faArray.size(); i++) {
            JSONObject faObj = faArray.getJSONObject(i);
            if(SectionTypeEnum.TYPE_COMPOSITE == part){
                filterStrs.append(getIntlString(BAG_TYPE)+":"+faObj.getString("lOption") + "," + faObj.getString("lCount") + ";");
                filterStrs.append(getIntlString(PANEL_TYPE)+":"+faObj.getString("pOption") + "," + faObj.getString("pCount") + ";");
            }else {
                filterStrs.append(faObj.getString("option") + "," + faObj.getString("count") + ";");
            }
        }
        contents[1][3] = filterStrs.toString();
        return contents;
    }
    /**
     * 封装柏诚报告 (加湿段 表格)
     * @param contents
     * @param allMap
     * @param part
     * @return
     */
    public static String[][] getBCWet(String[][] contents, Map<String,String> allMap, SectionTypeEnum part) {
        /*[
          [ "数量", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "制造商/原产国家", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "型号", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "类型", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "电力供应（V/相/Hz）", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "输入功率", UtilityConstant.SYS_BLANK,"KW", "须填报" ],
          [ "进水温度", UtilityConstant.SYS_BLANK,"℃", "须填报" ],
          [ "加湿量", UtilityConstant.SYS_BLANK,"kg/hr", "须填报" ]
        ]*/

        StringBuffer partName = new StringBuffer(getIntlString(part.getCnName()));
        contents[3][3] = partName.toString();

        String partKey = part.getId().split("\\.")[1];
        String InDryBulbT = String.valueOf(allMap.get(UtilityConstant.METASEXON_PREFIX+partKey+".WInDryBulbT"));
        contents[6][3] = InDryBulbT;
        String WHumidificationQ =  String.valueOf(allMap.get(UtilityConstant.METASEXON_PREFIX+partKey+".WHumidificationQ"));
        contents[7][3] = WHumidificationQ;
        return contents;
    }
    
    /**
     * 封装柏诚热水盘管
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getBCheatingcoil(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	
    	double WenteringFluidTemperature = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_HEATINGCOIL_WENTERINGFLUIDTEMPERATURE)));
    	double WWTAScend = BaseDataUtil.stringConversionDouble((String)map.get(UtilityConstant.METASEXON_HEATINGCOIL_WWTASCEND));
    	String outWaterTemp = String.valueOf(WenteringFluidTemperature+WWTAScend);

    	contents[5][3] = outWaterTemp + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(WenteringFluidTemperature);
    	return contents;
    }
    
    /**
     * 封装柏诚风机
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getBCfan(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//风机种类
    	String outlet = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_OUTLET));
    	if(EmptyUtil.isNotEmpty(outlet)) {
    		if (UtilityConstant.JSON_FAN_OUTLET_FOB.equals(outlet) || UtilityConstant.SYS_ALPHABET_F_UP.equals(outlet) || UtilityConstant.JSON_FAN_OUTLET_B.equals(outlet)) {
				contents[0][3] = "离心风机";
			}else if (UtilityConstant.JSON_FAN_OUTLET_WWK.equals(outlet)) {
				contents[0][3] = "无蜗壳风机";
			}
    		
    	}
    	
    	//出口风速
    	contents[3][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_OUTLETVELOCITY));
    	
    	//风机转速
    	contents[8][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_RPM));
    	contents[10][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_RPM));
    	
    	//输入功率
    	double motorPower = BaseDataUtil.stringConversionDouble((String)map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	String motorEfficiencyStr = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
    	NumberFormat nf=NumberFormat.getPercentInstance();
    	Number motorEfficiencyNum;
		try {
			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);				
			}else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if(motorEfficiency==0) {
	    		motorEfficiency = 0.01;
	    		contents[11][3] = String.valueOf(0);
	    	}else {
	    		double inputPower = (motorPower)/(motorEfficiency);
	    		BigDecimal inputPowerBd = new BigDecimal(inputPower);//四舍五入，保留两位小数
	    		inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
		    	contents[11][3] = String.valueOf(inputPowerBd);
	    	}
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return contents;
    }
    
    /**
     * 封装柏诚报告（ahu）
     * @param contents
     * @param allMap
     * @param ahuPartitionList
     * @return
     */
    public static String[][] getBCAhu(String[][] contents, Map<String,String> ahuMap, Map<String, String> mixMap,List<AhuPartition> ahuPartitionList) {
    	
    	contents[3][3] = getIntlString(PRODUCER_COUNTRY_FIELD);
    	
    	double sairvolume = BaseDataUtil.stringConversionDouble(String.valueOf(ahuMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
    	String naratioStr = String.valueOf(ahuMap.get(UtilityConstant.METASEXON_MIX_NARATIO));
    	double NARatio = BaseDataUtil.stringConversionDouble(String.valueOf(mixMap.get(UtilityConstant.METASEXON_MIX_NARATIO)));
		contents[4][3] = String.valueOf(sairvolume * NARatio) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_SAIRVOLUME));
    	return contents;
    }
    
    /**
     * 封装柏诚报告（ahu2）
     * @param contents
     * @param allMap
     * @return
     */
    public static String[][] getBCAhu2(String[][] contents, Map<String,String> allMap, List<AhuPartition> ahuPartitionList) {
    	/*[ 
    	  [ "机组总尺寸",UtilityConstant.SYS_BLANK,	"须填报", "AHU.sectionL*AHU.Height*AHU.Width"],
    	  [ "运行重量（kg)",UtilityConstant.SYS_BLANK,		"须填报","AHU.Weight"],
    	  [ "运行噪声（dB（A)）", UtilityConstant.SYS_BLANK,		"meta.section.fan.noise","meta.section.fan.noise"],
    	  ["声功率级",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["八倍频程（Hz)",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,"               63     125     250     500     1k     2k     4k     8k"],
    	  ["dB",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,"               68     71     77     82     79     76     71     62"],
    	  ["备注：",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["1.机组采用橡胶减震，空调箱内风机须设置弹簧减震台座；",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["2.此空调箱为组合式空调箱；",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["3.此空调箱变频，须自带变频控制箱。",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK]
    	]*/
    	//计算机组总尺寸
		if(ahuPartitionList!=null && ahuPartitionList.size()>0) {
			int length = 0;
	    	int width = 0;
	    	int height = 0;
	    	//double weight = 0.0d;
	    	for(AhuPartition ap: ahuPartitionList) {
	    		length += ap.getLength()+ap.getCasingWidth();
	    		width = ap.getWidth()*100 + ap.getCasingWidth();
	    		height = ap.getHeight()*100 + ap.getCasingWidth();
	    		//weight += ap.getWeight();
	    	}
	    	
			String unitSize = String.valueOf(length + UtilityConstant.SYS_PUNCTUATION_STAR + width
					+ UtilityConstant.SYS_PUNCTUATION_STAR + height);
	        contents[0][3] = unitSize;
		}
    	
        //计算A声功率级（A声声压级）
//		if(allMap.get(UtilityConstant.METASEXON_FAN_LW1)!=null &&
//				!UtilityConstant.SYS_BLANK.equals(allMap.get(UtilityConstant.METASEXON_FAN_LW1))) {
//			String dB = "               ";
//			for (int i = 1; i < 9; i++) {
//				dB += (String) allMap.get(UtilityConstant.METASEXON_FAN_LW + i) + "     ";
//			}
//			contents[5][3] = String.valueOf(dB);
//		}
		
		
//        String totalWeight = String.valueOf(weight);
//        contents[1][3] = totalWeight;
        
//        String noise = String.valueOf(allMap.get("meta.section.fan.noise"));
//        contents[2][3] = noise;
        return contents;
    }
    
    /**
     * 封装出风段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getDischarge(String[][] contents, Map<String, String> map,LanguageEnum language) {

        String outletDirection = UtilityConstant.METASEXON_DISCHARGE_OUTLETDIRECTION;
        
        int returnWF = 1;
        /*
        T顶部出风
        A后出风
        L左侧出风
        R右侧出风
        F底部出风

		1上出风
		2前出风
		3左出风
		4右出风
		5底出风
        */
        switch (map.get(outletDirection).toString()) {
		case "A":
			returnWF = 2;
			break;
		case "L":
			returnWF = 3;
			break;
		case UtilityConstant.SYS_ALPHABET_R_UP:
			returnWF = 4;
			break;
		case UtilityConstant.SYS_ALPHABET_F_UP:
			returnWF = 5;
			break;
		default:
			returnWF = 1;
			break;
		}
        
        
        
        //封装出风段参数形式
        String damperExecutorKey = UtilityConstant.METASEXON_DISCHARGE_DAMPEREXECUTOR;//执行器
        String dampertype = map.get(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE);//风口接件
        String serial = map.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterialKey = UtilityConstant.METASEXON_DISCHARGE_DAMPERMETERIAL;//风阀材料
        String product = map.get(UtilityConstant.METAHU_PRODUCT);
        
		String damperMeterial = map.get(damperMeterialKey);
		String damperExecutor = map.get(damperExecutorKey);
		damperExecutor = MetaSelectUtil.getTextForSelect(damperExecutorKey, damperExecutor, language);
        
        
        int valvem = 1;//镀锌板
		if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(damperMeterial)) {
			valvem = 2;//铝合金
		}
		
        
        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
		
		if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(dampertype)) {
			boolFL = true;
		}
		
		//风阀电动+(执行器)情况
        if (map.containsKey(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE)) {
            String value = contents[1][5];
            if (SystemCalculateConstants.DISCHARGE_AINTERFACE_ELECTRICDAMPER.equals(dampertype)) {
            	contents[1][5] = value + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+damperExecutor+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
        }
        String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_DISCHARGE_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
        }else{
            PositivePressureDoor = UtilityConstant.SYS_BLANK;
        }
        //安装检修灯 
        String value25 = contents[2][5];
    	String fixRepairLamp = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_DISCHARGE_FIXREPAIRLAMP));
    	if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
    		value25 = value25 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}else{
    		value25 = value25 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
    	}
    	contents[2][5] = value25;
        
        	
        DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class, serial.substring(0, serial.length() - 4),
                returnWF + UtilityConstant.SYS_BLANK, damper.getSectionL());
        String A = UtilityConstant.SYS_BLANK;
        String C = UtilityConstant.SYS_BLANK;
        
        int lenth = serial.length();
		String heightStr = serial.substring(lenth - 4, lenth - 2);
		String weightStr = serial.substring(lenth - 2);
		int height = Integer.parseInt(heightStr);
		int width = Integer.parseInt(weightStr);
		
		if (EmptyUtil.isEmpty(nsion)) {
			logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
		} else {
			if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(damperMeterial)) {			
                double c1 = Double.parseDouble(nsion.getC1());
                A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
            } else {
                A = nsion.getA();
            }
			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
			
			if (returnWF == 1 || returnWF == 2) {
				C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			if (returnWF == 3 || returnWF == 4) {
				C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			if (returnWF == 5) {
				C = UtilityConstant.SYS_BLANK+((width - 4) * 100 + DIMENSION_C_calPara - 72);
			}
		}
		if (!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)) {
			if(A.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {
				A=A.substring(0,A.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT));
			}
			contents[6][2] = C + UtilityConstant.SYS_ALPHABET_X_UP + A;// 回风阀门尺寸
		}
        AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, map.get(UtilityConstant.METAHU_SERIAL), UtilityConstant.SYS_ALPHABET_N_UP, UtilityConstant.SYS_BLANK + returnWF);
        if (EmptyUtil.isEmpty(ad)) {
			logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
		} else {
			if(!boolFL){
				if(2 == valvem){
					contents[6][5] = ad.getWringqAl();//回风阀门扭矩
				}else{
					contents[6][5] = ad.getWringq();//回风阀门扭矩
				}
			}
		}
        
        
        //设置风量
        setAirVolume(contents, map, 1, 2);
        return contents;
    }

    /**
     * 封装空段报告特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
	public static String[][] getAccess(String[][] contents, Map<String, String> map, LanguageEnum language) {
		String accessDoor = UtilityConstant.METASEXON_ACCESS_ODOOR;//是否开门:
        //是否开门需要添加：
        String doorOnBothSide = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_DOORONBOTHSIDE));//两侧开门
        String fixRepairLamp = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_FIXREPAIRLAMP));//安装检修灯 
        if (map.containsKey(accessDoor)) {
            String value = contents[1][5];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[1][5] = value;
            }
        }
        String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
        }else{
            PositivePressureDoor = UtilityConstant.SYS_BLANK;
        }

        String value15 = contents[1][5];
		if (UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)) {
			value15 = value15 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)
					+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
		} else {
			value15 = value15 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)
					+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
		}
    	contents[1][5] = value15;
    	
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
	/**
	 * 封装高效过滤段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getHepafilter(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
		setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
    /**
     * 封装pdf报告特殊数据
     * @param contents
     * @param allMap
     * @param type
     * @return
     */
	public static String[][] getCommonF(String[][] contents, Map<String, String> allMap ,SectionTypeEnum type) {
		String id = type.getId().replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.SYS_BLANK);
		contents[2][1] = UtilityConstant.METANS_ID_MEMO.replace(UtilityConstant.METACOMMON_ID, id);
		contents[2][4] = UtilityConstant.METANS_ID_PRICE.replace(UtilityConstant.METACOMMON_ID, id);
		String enableKey = UtilityConstant.METANS_ID_ENABLE.replace(UtilityConstant.METACOMMON_ID, id);
		String enable = String.valueOf(allMap.get(enableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			return contents;
		}else{
			return null;
		}
	}

    /**
     * 封装非标word报告特殊数据
     * @param contents
     * @param allMap
     * @param type
     * @return
     */
    public static String[][] getNsCommonF(String[][] contents, Map<String, String> allMap, SectionTypeEnum type) {
        String id = type.getId().replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.SYS_BLANK);
        contents[1][1] = UtilityConstant.METANS_ID_MEMO.replace(UtilityConstant.METACOMMON_ID, id);
        contents[1][7] = UtilityConstant.METANS_ID_PRICE.replace(UtilityConstant.METACOMMON_ID, id);
        String enableKey = UtilityConstant.METANS_ID_ENABLE.replace(UtilityConstant.METACOMMON_ID, id);
        String enable = String.valueOf(allMap.get(enableKey));
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
            return contents;
        }else{
            return null;
        }
    }

	/**
	 * 封装电极加湿段特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getTechElec1(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//计算额定功率
		double SHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ)));//夏季加湿量
		double WHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)));//冬季加湿量
		int humidificationQ = 0;
		//加湿量优先取冬季的
		if (WHumidificationQ == 0.0) {
			humidificationQ = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(SHumidificationQ));
		}else {
			humidificationQ = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(WHumidificationQ));
		}
		
		//根据加湿量查询额定功率
		List<S4xhumid> s4xhumidList = AhuMetadata.findAll(S4xhumid.class);
		if(s4xhumidList!=null) {
			ListUtils.sort(s4xhumidList, true,"humidq");
			S4xhumid s4xhumid=null;
			for (int i=0;i<s4xhumidList.size();i++) {
				s4xhumid=s4xhumidList.get(i);
				if(humidificationQ<=s4xhumid.getHumidq()) {
					break;
				}
			}
			
			double rpower = s4xhumid.getRpower();
			
			contents[5][2] = String.valueOf(rpower);
		}
		
		//设置风量
		setAirVolume(contents, map, 1, 2);
		
		return contents;
	}
	
    public static String[][] getTechElec14Sale(String[][] contents, Map<String, String> map, LanguageEnum language) {
        //计算额定功率
        double SHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ)));//夏季加湿量
        double WHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)));//冬季加湿量
        int humidificationQ = 0;
        //加湿量优先取冬季的
        if (WHumidificationQ == 0.0) {
            humidificationQ = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(SHumidificationQ));
        }else {
            humidificationQ = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(WHumidificationQ));
        }
        
        //根据加湿量查询额定功率
        
        List<S4xhumid> s4xhumidList = AhuMetadata.findAll(S4xhumid.class);
		if(s4xhumidList!=null) {
			ListUtils.sort(s4xhumidList, true,"humidq");
			S4xhumid s4xhumid=null;
			for (int i=0;i<s4xhumidList.size();i++) {
				s4xhumid=s4xhumidList.get(i);
				if(humidificationQ<=s4xhumid.getHumidq()) {
					break;
				}
			}
			
			double rpower = s4xhumid.getRpower();
			contents[3][2] = String.valueOf(rpower);
		}
        return contents;
    }
	
	/**
	 * 蒸汽盘管段封装特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getTechSte(String[][] contents, Map<String, String> map, LanguageEnum language) {
		// 计算管接头
		String fantype = (String) map.get(UtilityConstant.METAHU_SERIAL);// 机组型号
		String flange = (String) map.get(UtilityConstant.METASEXON_STEAMCOIL_CONNECTIONTYPE);// 连接形式
		if (EmptyUtil.isNotEmpty(flange)) {
			flange = "TD".equals(flange) ? UtilityConstant.SYS_STRING_NUMBER_0 : UtilityConstant.SYS_STRING_NUMBER_1;
		}

		// 根据加湿量查询额定功率
		String finalvalue = UtilityConstant.SYS_BLANK;
		STakeoversize sTakeoversize = AhuMetadata.findOne(STakeoversize.class, fantype, UtilityConstant.SYS_ALPHABET_F_UP, flange);
		if (sTakeoversize != null) {
			finalvalue = UtilityConstant.SYS_STRING_NUMBER_1.equals(flange) ? "法兰" : "螺纹" + sTakeoversize.getSizevalue();
		}

		contents[3][5] = String.valueOf(finalvalue);
		
		//设置风量
		setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
	/**
	 * 设置电子加热盘管段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getElectricheatingcoil(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
		setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
	/**
	 * 封装干蒸加湿段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getSteamhumidifier(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
		setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
	/**
	 * 封装静电过滤段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getElectrostaticfilter(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
		setAirVolume(contents, map, 1, 2);
		return contents;
	}
	
	/**
	 * 封装机组铭牌
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getUnitNamePlateData(String[][] contents, Map<String, String> noChangeMap, LanguageEnum language) {
		
		//计算轴功率
    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
    	double totalPressure = Double.parseDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
		//计算输入功率
    	double motorSafetyFacor = new Double(0);
    	double motorPower = BaseDataUtil.stringConversionDouble((String)noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	if(motorPower<0.5) {
    		motorSafetyFacor = 1.5;
    	}else if (motorPower>0.5 && motorPower<1) {
			motorSafetyFacor = 1.4;
		}else if (motorPower>1 && motorPower<2) {
			motorSafetyFacor = 1.3;
		}else if (motorPower>2 && motorPower<5) {
			motorSafetyFacor = 1.2;
		}else if(motorPower>5) {
			motorSafetyFacor = 1.15;
		}
    	double actualMotorPower = (shaftPower*motorSafetyFacor)/(1);
    	System.out.println("#############################actualMotorPower=========="+actualMotorPower);
    	String motorEfficiencyStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
    	NumberFormat nf=NumberFormat.getPercentInstance();
    	Number motorEfficiencyNum;
		try {
			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);				
			}else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if(motorEfficiency==0) {
	    		motorEfficiency = 0.94;
	    	}
			double inputPower = (motorPower) / (motorEfficiency);
			BigDecimal inputPowerBd = new BigDecimal(inputPower);// 四舍五入，保留两位小数
			inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
			contents[4][1] = String.valueOf(inputPowerBd);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return contents;
	}
	
	/**
	 * 封装主要参数列表 风机特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getFan4Paralist(String[][] contents, Map<String, String> map,LanguageEnum language) {
		
		//计算轴功率
    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
    	double totalPressure = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
    	contents[7][2] = String.valueOf(shaftPowerBd);
		
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// 回风
				contents[0][2] = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// 送风
				contents[0][2] = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}

		//参数汇总表中增加大功率的电机（≥30kw）电机基座型号；
        double motorPower = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)));
        if(motorPower<30){
            contents[13][2] = "";
        }

        return contents;
	}
	
	/**
	 * 封装报告中有关ahu高度的计算
	 * @param allMap
	 * @param partition
	 * @param unit
	 * @return
	 */
	public static String wrapHeight(Map<String, String> allMap, Partition partition, Unit unit, AhuPartition ap) {
		String finalHeight = UtilityConstant.SYS_BLANK;
		String series = unit.getSeries();
		int type = Integer.valueOf(series.substring(series.length() - 4, series.length()));
		String heightInMap = String.valueOf(allMap.get(UtilityConstant.METAHU_HEIGHT));
		if (EmptyUtil.isNotEmpty(heightInMap)) {
			if (608 <= type && type <= 2333) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:100),1));
			} else if (2532 <= type && type <= 3438) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:200),1));
			}
		} else {
			return finalHeight;
		}
		return finalHeight;
	}
	
	/**
	 * 封装段连接清单报告
	 * @param contents
	 * @param ahuMap
	 * @param paneltype
	 * @return
	 */
	public static String[][] getConnectionList(String[][] contents, Map<String, String> ahuMap, String unitSeries,
			String paneltype) {
		String inskinm = ahuMap.get(UtilityConstant.METAHU_INSKINM);//材质（内）
		String targetStr = "";
		if ("GlSteel".equals(inskinm)) {
			targetStr = UtilityConstant.SYS_STRING_NUMBER_2;// 2=镀锌
		} else if ("prePaintedSteal".equals(inskinm) || "paintedColdRolledSteel".equals(inskinm)
				|| "HDPPrePaintedSteel".equals(inskinm)) {// 1=冷轧，彩钢板，高耐久性聚酯面板
			targetStr = UtilityConstant.SYS_STRING_NUMBER_1;
		} else if ("stainless".equals(inskinm)) {// 3=不锈钢
			targetStr = UtilityConstant.SYS_STRING_NUMBER_3;
		}
		
		if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39G
			contents = handleReplacement(contents, 1, 1, 10, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, 12, contents[2][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, 12, contents[3][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {//39GForepart
			contents = handleReplacement(contents, 1, 1, 10, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, 12, contents[2][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, 12, contents[3][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39GPositive
			contents = handleReplacement(contents, 1, 1, 12, contents[1][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39CQStandard
			contents = handleReplacement(contents, 1, 1, 11, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length(), targetStr);
			contents = handleReplacement(contents, 11, 1, 10, contents[11][1].length(), targetStr);
			contents = handleReplacement(contents, 12, 1, 10, contents[12][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {//39CQForepart
			contents = handleReplacement(contents, 1, 1, 11, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 8, 1, 10, contents[8][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length()-8, targetStr);
			contents = handleReplacement(contents, 11, 1, 10, contents[11][1].length()-8, targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39CQPositive
			contents = handleReplacement(contents, 1, 1, 13, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39XTStandard
			contents = handleReplacement(contents, 1, 1, 11, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, 10, contents[8][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 4, 1, 10, contents[10][1].length(), targetStr);
			contents = handleReplacement(contents, 5, 1, 10, contents[11][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39XTPositive
			contents = handleReplacement(contents, 1, 1, 12, contents[1][1].length(), targetStr);
		}		
		
		return contents;
	}
	
	/**
	 * 从二位数组中指定位置的字符串中，替换指定位置的字符。
	 * @param contents	二维数组
	 * @param row	二维数组行数
	 * @param col	二维数组列数
	 * @param start	替换字符串起始位置
	 * @param targetStr	目标字符
	 * @return	修改过后的二维数组（String[][]）
	 */
	private static String[][] handleReplacement(String[][] contents, int row, int col, int start, int end, String targetStr) {
		String content = contents[row][col];
		StringBuilder contentSB = new StringBuilder(content);
		contentSB.replace(start, end, targetStr);
		contents[row][col] = contentSB.toString();
		return contents;
	}
	
	/**
	 * 主要参数列表 [混合段]
	 * @param contents
	 * @param allMap
	 * @param language
	 * @return
	 */
	public static String[][] getParaMix(String[][] contents, Map<String, String> allMap, LanguageEnum language){
		String serial = allMap.get(UtilityConstant.METAHU_SERIAL);
		String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;//1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;//2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;//3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;//4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;//5
        
		int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;
        
        String product = allMap.get(UtilityConstant.METAHU_PRODUCT);
        String mixSectionL = String.valueOf(allMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
        
        for (int i = 0; i < contents.length; i++) {
        	
        	int returnWF = 1;
			if (allMap.containsKey(returnTop)) {
                String value = BaseDataUtil.constraintString(allMap.get(returnTop));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 6) {
                        continue;
                    }
                }else{
                	returnWF = returnTopWF;
                }
            } else {
                if (i == 6) {
                    continue;
                }
            }

            if (allMap.containsKey(returnButtom)) {
                String value = BaseDataUtil.constraintString(allMap.get(returnButtom));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 7) {
                        continue;
                    }
                }else{
                	returnWF = returnButtomWF;
                }
            } else {
                if (i == 7) {
                    continue;
                }
            }
            
            if (allMap.containsKey(returnLeft)) {
                String value = BaseDataUtil.constraintString(allMap.get(returnLeft));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 8) {
                        continue;
                    }
                }else{
                	returnWF = returnLeftWF;
                }
            } else {
                if (i == 8) {
                    continue;
                }
            }

            if (allMap.containsKey(returnRight)) {
                String value = BaseDataUtil.constraintString(allMap.get(returnRight));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 9) {
                        continue;
                    }
                }else{
                	returnWF = returnRightWF;
                }
            } else {
                if (i == 9) {
                    continue;
                }
            }
            
            if (allMap.containsKey(returnBack)) {
                String value = BaseDataUtil.constraintString(allMap.get(returnBack));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 10) {
                        continue;
                    }
                }else{
                	returnWF = returnBackWF;
                }
            } else {
                if (i == 10) {
                    continue;
                }
            }
            
            
			if (i == 0 || i == 1) {
	            DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
	                    serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
	            String A = UtilityConstant.SYS_BLANK;
	            String C = UtilityConstant.SYS_BLANK;
	            
	            int lenth = serial.length();
				String heightStr = serial.substring(lenth - 4, lenth - 2);
				String weightStr = serial.substring(lenth - 2);
				int height = Integer.parseInt(heightStr);
				int width = Integer.parseInt(weightStr);
				
	    		if (EmptyUtil.isEmpty(nsion)) {
	    			logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
	    		} else {
	    			String metrial = allMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
	    			if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
	    				double c1 = Double.parseDouble(nsion.getC1());
	    				A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
	    			} else {
	    				A = nsion.getA();
	    			}
	    			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
	    			
	    			if (returnWF == 1 || returnWF == 2) {
	    				C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 3 || returnWF == 4) {
	    				C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 5) {
	    				C = UtilityConstant.SYS_BLANK+((width - 4) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    		}
	    		if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
	    			contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
	    		}
	            
	        }
        }
		return contents;
	}
	
	/**
	 * 根据空气方向设置风量
	 * @param contents
	 * @param map
	 * @param row
	 * @param col
	 */
	private static void setAirVolume(String[][] contents, Map<String, String> map,int row,int col) {
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// R--回风
				contents[row][col] = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// S--送风
				contents[row][col] = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}
	}
	
	private static String[][] filterContents(String[][] contents){
		ArrayList<String[]> templist = new ArrayList<String[]>(java.util.Arrays.asList(contents));
		List<Integer> tempI = new ArrayList<Integer>();
		
		//判断第一个值是否为true，将最后的价格去除，否则标记这一行
		for (int i = 1; i < templist.size()-1; i++) {
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(templist.get(i)[1])) {
				templist.get(i)[6] = UtilityConstant.SYS_BLANK;
				templist.get(i)[7] = UtilityConstant.SYS_BLANK;
			} else {
				tempI.add(i);
			}
		}
		//根据标记的行数进行删除
		int i = 0;
		for(int a: tempI) {
			templist.remove(a-i);
			i++;
		}
		String[][] returnContents = new String[templist.size()][contents[0].length];
		templist.toArray(returnContents);
		
		//最后一行memo的价格删除
		returnContents[returnContents.length-1][6] = UtilityConstant.SYS_BLANK;
		returnContents[returnContents.length-1][7] = UtilityConstant.SYS_BLANK;
		return returnContents;
		
	}
	
}
