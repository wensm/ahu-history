package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getFontFamily;
import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_PSY_CHART_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_TRIPLE_VIEW_SEQUENCE_NUMBER;
import static com.carrier.ahu.common.intl.I18NConstants.REPORT_NS_TAIL1;
import static com.carrier.ahu.common.intl.I18NConstants.REPORT_NS_TAIL2;
import static com.carrier.ahu.common.intl.I18NConstants.REPORT_NS_TAIL3;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CONNECTION_LIST;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_MODEL_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_NAMEPLATE_DATA;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD_EN;
import static com.carrier.ahu.constant.CommonConstant.TEMPLATE_WORD;
import static com.carrier.ahu.constant.CommonConstant.TEMPLATE_WORD_EN;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_AHU;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_AHU_NONSTANDARD;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_COMMON;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_COOLINGCOIL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_FAN;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_FILTER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_HEPAFILTER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_COMPOSITE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_HEATINGCOIL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_SPRAYHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_STEAMHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_NS_WETFILMHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_AHU_OVERALL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_FAN_CURVE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_PRODUCTPACKINGLIST;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_PRODUCTPACKINGLIST_EXPORT;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA_SHAPE;
import static com.carrier.ahu.util.DateUtil.HH_MM;
import static com.carrier.ahu.util.DateUtil.YYYY_MM_DD;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_PRODUCT_PACKING_LIST_NAME;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_SERIAL;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONFOREPART;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONPOSITIVE;
import static com.carrier.ahu.common.intl.I18NConstants.CONNECTION_CONVERTICAL;
import static com.carrier.ahu.report.common.ReportConstants.METASEXON_FAN_PTC;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.ReflectionUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.CadUtils;
import com.carrier.ahu.metadata.ReportMetadata;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.model.calunit.UnitUtil;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.psychometric.PsyCalBean;
import com.carrier.ahu.psychometric.PsychometricDrawer;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.Report;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.report.ReportItem;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.report.pdf.PDFTableGenerator;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.carrier.ahu.vo.SysConstants;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

public class Word4ReportUtils {
	protected static Logger logger = LoggerFactory.getLogger(Word4ReportUtils.class);
	/** WORD类型 - 非标清单 */
	public static final String T_SPECIALLIST = "speciallist";
	/** WORD类型 - 技术说明 （工程版）*/
	public static final String T_TECHPROJ = "techprojWord";

	/**
	 * 非标清单word文件生成入口，
	 * @param file
	 * @param report
	 * @param reportData
	 * @param arrayMap
	 * @param language
	 * @param unitType
	 * @throws Exception
	 */
    public static void genWord4Speciallist(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws Exception {
		String reportTemplate = LanguageEnum.Chinese.equals(language)
				? TEMPLATE_WORD
				: TEMPLATE_WORD_EN;
		InputStream is = new FileInputStream(reportTemplate);
		XWPFDocument doc = new XWPFDocument(is);

		/* 1加载模板 */
		//警告，params 的 key 不能拿包含特殊符号必须是一个单词否则 poi截取出现问题
		Map<String, Object> params = getProjectParameters(reportData.getProject());
		XWPFHeader header = doc.getHeaderFooterPolicy().getDefaultHeader();
		/* 2替换模板页眉信息 */
		replaceInHeaderPara(header, params);// 替换header 段落变量
		replaceInHeaderTable(header, params);// 替换header 表格变量
		
		/* 3添加非标具体信息 */
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		for (Unit unit : reportData.getUnitList()) {
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}

			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allInOneMap = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);

			//ahu信息添加
			Map<String, String> ahuMap = allMap.get(UnitConverter.UNIT);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU), reportData.getProject(), unit,
					ahuMap, language, unitType);
			createTable(doc,ParagraphAlignment.LEFT, true, false, ahuContents.length, ahuContents[0].length, ahuContents);

			//总价
			double totalPrice[] = new double[1];
			String enable = String.valueOf(ahuMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
				//ahu ns 信息添加
				String[][] ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), reportData.getProject(), unit,
						ahuMap, language, unitType);
				createTable(doc,ParagraphAlignment.LEFT, true, false, ahu2Contents.length, ahu2Contents[0].length, ahu2Contents);

				try {
					double price1 = Double.parseDouble(ahu2Contents[1][7]);					
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(ahu2Contents[1][1])) {//变形价格
						totalPrice[0] = totalPrice[0] + price1;
					}
					double price2 = Double.parseDouble(ahu2Contents[2][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(ahu2Contents[2][1])) {//变形价格
						totalPrice[0] = totalPrice[0] + price2;
					}
					double price3 = Double.parseDouble(ahu2Contents[3][7]);
					totalPrice[0] = totalPrice[0] + price3;

				} catch (Exception e) {

				}
			}

			//段信息添加
			String groupCode = unit.getGroupCode();
			if (null  == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("非标报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
                int order = 1;
                for (PartPO partPO : partList) {
                    Part part = partPO.getCurrentPart();
                    String key = part.getSectionKey();
                    addSection(doc, key, order, totalPrice, allInOneMap, language, unitType, project, unit);
                    order++;
                }
			}
			//总价信息添加
			String[][] totalPriceContents =  new String[1][4];
			totalPriceContents[0][0] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][1] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][2] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][3] = "TOTAL："+totalPrice[0];
			createTable(doc,ParagraphAlignment.RIGHT, true, false, totalPriceContents.length, totalPriceContents[0].length, totalPriceContents);

			//报告尾部信息添加
			String[][] tailContents1 =  new String[1][1];
			tailContents1[0][0] = getIntlString(REPORT_NS_TAIL1);
			createTable(doc,ParagraphAlignment.LEFT, false,true, tailContents1.length, tailContents1[0].length, tailContents1);
			String[][] tailContents2 =  new String[1][2];
			tailContents2[0][0] = getIntlString(REPORT_NS_TAIL2);
			tailContents2[0][1] = getIntlString(REPORT_NS_TAIL3);
			createTable(doc,ParagraphAlignment.LEFT,false, true, tailContents2.length, tailContents2[0].length, tailContents2);

		}

		OutputStream os = new FileOutputStream(file.getPath());
		doc.write(os);
		close(os);
		close(is);
		
	}

    private static Map<String, Object> getProjectParameters(Project project) {
        Map<String, Object> params = new HashMap<String, Object>();
        Date date = new Date();
        params.put(ReportConstants.SPECIALLIST_ATTR_DATE, DateUtil.getDateTimeString(date, YYYY_MM_DD));
        params.put(ReportConstants.SPECIALLIST_ATTR_TIME, DateUtil.getDateTimeString(date, HH_MM));
        params.put(ReportConstants.SPECIALLIST_ATTR_SYSTEMVERSION, AHUContext.getAhuVersion());
        params.put(ReportConstants.SPECIALLIST_ATTR_GZL, StringUtils.EMPTY);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_NO);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_CONTRACT);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_NAME);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_SALER);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_ADDRESS);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_ENQUIRYNO);
        return params;
    }
    
    private static Map<String, Object> getUnitParameters(Unit unit) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ReportConstants.SPECIALLIST_ATTR_CUSTOMERPO, unit.getCustomerName());
        return params;
    }

    private static void putProperty(Map<String, Object> params, Project project, String name) {
        params.put(name, ReflectionUtils.getProperty(project, name));
    }

	/**
	 * 技术说明（工程版）文件生成入口
	 * @param file
	 * @param report
	 * @param reportData
	 * @param arrayMap
	 * @param language
	 * @param unitType
	 * @throws Exception
	 */
    public static void genWord4Techproj(File file, Report report, ReportData reportData, LanguageEnum language,
            UnitSystemEnum unitType) throws Exception {
		String reportTemplate = LanguageEnum.Chinese.equals(language)
				? TECHPROJ_TEMPLATE_WORD
				: TECHPROJ_TEMPLATE_WORD_EN;
		InputStream is = new FileInputStream(reportTemplate);
		XWPFDocument doc = new XWPFDocument(is);
		
		/* 1加载模板 */
		Map<String, Object> projectParams = getProjectParameters(reportData.getProject());
		XWPFHeader header = doc.getHeaderFooterPolicy().getDefaultHeader();
        /* 2替换模板页眉信息 */
        replaceInHeaderPara(header, projectParams);// 替换header 段落变量
        replaceInHeaderTable(header, projectParams);// 替换header 表格变量

		/* 3添加非标具体信息 */
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		for (int i = 0; i < reportData.getUnitList().size(); i++) {
            Unit unit = reportData.getUnitList().get(i);

            Map<String, Object> unitParams = getUnitParameters(unit);
            replaceInHeaderTable(header, unitParams); // update customer PO number in header

			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			
			//添加ahu机组信息
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_AHU_OVERALL), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			ahuContents = SectionContentConvertUtils.getAhuOverall(ahuContents, partition, unit, language, allMap.get(UnitConverter.UNIT));
			WordTableGen.createTable(doc,ParagraphAlignment.LEFT, true, false, ahuContents.length, ahuContents[0].length, ahuContents);

			if (EmptyUtil.isNotEmpty(partition)) {
				// String[][] titleContents = arrayMap.get("TechAhu2");
				String[][] ahuboxContents = SectionContentConvertUtils.getAhu(partition, unit, language , allMap.get(UnitConverter.UNIT));
				String[][] ahu2Contents = ValueFormatUtil.transReport(ahuboxContents, reportData.getProject(), unit,
						allMap.get(UnitConverter.UNIT), language, unitType);
				WordTableGen.createTable(doc, ParagraphAlignment.LEFT, false, false, ahu2Contents.length,
						ahu2Contents[0].length, ahu2Contents);
			}

			//添加ahu非标
			String enable = String.valueOf(allMap.get(UnitConverter.UNIT).get(UtilityConstant.METANS_NS_AHU_ENABLE));//ahu是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), project, unit, allMap.get(UnitConverter.UNIT), language,
						unitType);
				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_AHU.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getAhuNS(nsAHUcontents, partition, unit, language, allMap.get(UnitConverter.UNIT));
				WordTableGen.createTable(doc, ParagraphAlignment.LEFT, false, false, nsAHUcontents.length,
						nsAHUcontents[0].length, nsAHUcontents);
			}
			
			XWPFParagraph p = createParagraph(doc);
			p.setAlignment(ParagraphAlignment.LEFT);
			XWPFRun r = p.createRun();
			r.setText(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION));
			
			//段信息添加
			String groupCode = unit.getGroupCode();
			if (null  == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("非标报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					 Map<String, String> noChangeMap=(allMap.get(UnitConverter.genUnitKey(unit, part)));
					 Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
					DocTableGen.addSection(doc,key, order, noChangeMap,cloneMap, language, unitType, project, unit);
					order++;
				}
			}
			
			for (ReportItem item : report.getItems()) {
				if (!item.isOutput()) {
					continue;
				}

				if (ReportConstants.CLASSIFY_TVIEW.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImage3View(doc, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), language);
					} catch (Exception e) {
						logger.error("工程版报告》添加三视图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_HANSHI.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImagePsychometric(doc, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition),
								language);
					} catch (Exception e) {
						logger.error("工程版报告》添加焓湿图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_FAN.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
                        addImageFan(doc, ValueFormatUtil.transDBData2AhuParam(unit, partList, partition), allMap,
                                language, unitType, project, unit);
					} catch (Exception e) {
						logger.error("工程版报告》添加风机曲线图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PANEL_SCREENSHOTS.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImagePanelScreenshots(doc , unit , partition , language);
					} catch (Exception e) {
						logger.error("工程版报告》添加面板布置图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_UNIT_NAMEPLATE_DATA.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addUnitNameplateData(doc, allMap2,project,unitType,unit , partition , language);
					} catch (Exception e) {
						logger.error("工程版报告》机组铭牌数据》报错",e);
					}
				}
				List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
				int connectionNum=ahuPartitionList.size()-1;
				if (ReportConstants.CLASSIFY_SECTION_CONNECTION_LIST.equals(item.getClassify())
						&& connectionNum > 0) {//当只有一个分段时，不需要打印段连接清单
					try {
						for(int num=1;num<=connectionNum;num++) {
							//生成用户特殊段连接
							String paneltype=UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
							if(num==1) {
							paneltype = unit.getPaneltype();
							if (EmptyUtil.isEmpty(paneltype)) {//paneltype为空的时候默认“标准”
								paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
							}
							}else {
								paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
							}
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addSectionConnectionList(paneltype,doc , unit , language);
						}
					} catch (Exception e) {
						logger.error("工程版报告》段连接清单》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PRODUCT_PACKING_LIST.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addProductPackingList(doc,allMap, allMap2, project, unitType, unit, partition, language, connectionNum,partList);
					} catch (Exception e) {
						logger.error("工程版报告》产品装箱单》报错",e);
					}
				}
			}
            if (i < reportData.getUnitList().size() - 1) {
                WordTableGen.addNewPage(doc);
            }
		}
		OutputStream os = new FileOutputStream(file.getPath());
		doc.write(os);
		close(os);
		close(is);
	}

    private static XWPFParagraph createParagraph(XWPFDocument doc) {
        XWPFParagraph paragraph = doc.createParagraph();
        paragraph.setSpacingAfter(5);
        return paragraph;
    }

	/**
	 * 添加段信息
	 * @param doc
	 * @param key
	 * @param order
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @throws Exception
	 */
    private static void addSection(XWPFDocument doc, String key, int order, double[] totalPrice,
            Map<String, String> allMap, LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws Exception {
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(key);
        switch (sectionType) {
        case TYPE_COLD:
            addCoolingCoilNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_HEATINGCOIL:
            addHeatingCoilNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_STEAMHUMIDIFIER:
            addSteamHumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_WETFILMHUMIDIFIER:
            addWetfilmhumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_SPRAYHUMIDIFIER:
            addSprayHumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_FAN:
            addFanNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            return;
        case TYPE_COMBINEDMIXINGCHAMBER:
        case TYPE_ATTENUATOR:
        case TYPE_DISCHARGE:
        case TYPE_ACCESS:
        case TYPE_HEPAFILTER:
        	addHepafilterNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_ELECTRODEHUMIDIFIER:
        case TYPE_ELECTROSTATICFILTER:
        case TYPE_CTR:
        case TYPE_HEATRECYCLE:
        case TYPE_WHEELHEATRECYCLE:
        case TYPE_STEAMCOIL:
        case TYPE_ELECTRICHEATINGCOIL:
        case TYPE_DIRECTEXPENSIONCOIL:
        case TYPE_MIX:
        case TYPE_SINGLE:
        	addSingleNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            return;
        case TYPE_COMPOSITE:
            addCompositeNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        default:
            logger.warn(MessageFormat.format("非标报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}",
                    unit.getUnitNo(), key));
        }
    }

	/**
	 * 添加非标信息
	 * @param doc
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
	private static void addCommonNS(XWPFDocument doc, double[] totalPrice,  Map<String, String> allMap,
									LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
			throws Exception {
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){//启用非标
			String[][] nsCommoncontents = ReportContent.getReportContent(REPORT_NS_COMMON);
			nsCommoncontents = SectionContentConvertUtils.getNsCommonF(nsCommoncontents, allMap, sectionType);
			if (null != nsCommoncontents) {
				nsCommoncontents = ValueFormatUtil.transReport(nsCommoncontents, project, unit, allMap, language,
						unitType);
				nsCommoncontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(), language) + nsCommoncontents[0][0];//替换段名称
				createTable(doc, ParagraphAlignment.LEFT, true, false, nsCommoncontents.length, nsCommoncontents[0].length, nsCommoncontents);
				try {
					double price = Double.parseDouble(nsCommoncontents[1][7]);
					totalPrice[0] = totalPrice[0] + price;
				} catch (Exception e) {

				}
			}
		}
	}

	/**
	 * 湿膜加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addWetfilmhumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsWetcontents = ReportContent.getReportContent(REPORT_NS_WETFILMHUMIDIFIER);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsWetcontents = ValueFormatUtil.transReport(nsWetcontents, project, unit, allMap, language,
					unitType);
			nsWetcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsWetcontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsWetcontents.length, nsWetcontents[0].length, nsWetcontents);

			try{
				if(!UtilityConstant.SYS_BLANK.equals(nsWetcontents[1][7])) {
					double price1 = Double.parseDouble(nsWetcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsWetcontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsWetcontents[2][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

	/**
	 * 高压喷雾加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addSprayHumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsSprcontents = ReportContent.getReportContent(REPORT_NS_SPRAYHUMIDIFIER);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsSprcontents = ValueFormatUtil.transReport(nsSprcontents, project, unit, allMap, language,
					unitType);
			nsSprcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsSprcontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsSprcontents.length, nsSprcontents[0].length, nsSprcontents);

			try{
				if(!UtilityConstant.SYS_BLANK.equals(nsSprcontents[1][7])) {
					double price1 = Double.parseDouble(nsSprcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsSprcontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsSprcontents[2][7]);
				totalPrice[0] = totalPrice[0] + price2;

			}catch (Exception e){

			}
		}
	}

	/**
	 * 干蒸加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addSteamHumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsStecontents = ReportContent.getReportContent(REPORT_NS_STEAMHUMIDIFIER);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsStecontents = ValueFormatUtil.transReport(nsStecontents, project, unit, allMap, language,
					unitType);
			nsStecontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsStecontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsStecontents.length, nsStecontents[0].length, nsStecontents);

			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsStecontents[2][7])) {
					double price1 = Double.parseDouble(nsStecontents[2][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsStecontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsStecontents[3][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

    private static void addCoolingCoilNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsCoolingCoilcontents = ReportContent.getReportContent(REPORT_NS_COOLINGCOIL);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsCoolingCoilcontents = ValueFormatUtil.transReport(nsCoolingCoilcontents, project, unit, allMap, language,
					unitType);
			nsCoolingCoilcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsCoolingCoilcontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsCoolingCoilcontents.length, nsCoolingCoilcontents[0].length, nsCoolingCoilcontents);

			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsCoolingCoilcontents[1][7])) {
					double price1 = Double.parseDouble(nsCoolingCoilcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsCoolingCoilcontents[1][1])) {//变更材质选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsCoolingCoilcontents[2][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

    private static void addHeatingCoilNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsHeatingCoilcontents = ReportContent.getReportContent(REPORT_NS_HEATINGCOIL);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsHeatingCoilcontents = ValueFormatUtil.transReport(nsHeatingCoilcontents, project, unit, allMap, language,
					unitType);
			nsHeatingCoilcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsHeatingCoilcontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsHeatingCoilcontents.length, nsHeatingCoilcontents[0].length, nsHeatingCoilcontents);

			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsHeatingCoilcontents[1][7])) {
					double price1 = Double.parseDouble(nsHeatingCoilcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsHeatingCoilcontents[1][1])) {//变更材质选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsHeatingCoilcontents[2][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

	/**
	 * 风机段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param arrayMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addFanNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsFancontents = ReportContent.getReportContent(REPORT_NS_FAN);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsFancontents = ValueFormatUtil.transReport(nsFancontents, project, unit, allMap, language,
					unitType);
			nsFancontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsFancontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsFancontents.length, nsFancontents[0].length, nsFancontents);
			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[1][7])) {
					double price1 = Double.parseDouble(nsFancontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsFancontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[2][7])) {
					double price2 = Double.parseDouble(nsFancontents[2][7]);
					if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsFancontents[2][1])){//变更变更电机功率选中计算价格
						totalPrice[0] = totalPrice[0] + price2;
					}
				}
				if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[3][7])) {
					double price3 = Double.parseDouble(nsFancontents[3][7]);
					if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsFancontents[3][1])){//JD弹簧改HD弹簧选中计算价格
						totalPrice[0] = totalPrice[0] + price3;
					}
				}
				double price4 = Double.parseDouble(nsFancontents[4][7]);
				totalPrice[0] = totalPrice[0] + price4;
			}catch (Exception e){

			}

		}
	}
    
    /**
     * 单层过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addSingleNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsFiltercontents = ReportContent.getReportContent(REPORT_NS_FILTER);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsFiltercontents = ValueFormatUtil.transReport(nsFiltercontents, project, unit, allMap, language,
					unitType);
			nsFiltercontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsFiltercontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsFiltercontents.length, nsFiltercontents[0].length, nsFiltercontents);
			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsFiltercontents[1][7])) {
					double price1 = Double.parseDouble(nsFiltercontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsFiltercontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsFiltercontents[4][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
    /**
     * 高效过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addHepafilterNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsHepacontents = ReportContent.getReportContent(REPORT_NS_HEPAFILTER);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsHepacontents = ValueFormatUtil.transReport(nsHepacontents, project, unit, allMap, language,
					unitType);
			nsHepacontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsHepacontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsHepacontents.length, nsHepacontents[0].length, nsHepacontents);
			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsHepacontents[1][7])) {
					double price1 = Double.parseDouble(nsHepacontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsHepacontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsHepacontents[4][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
    /**
     * 综合过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addCompositeNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String[][] nsCompositecontents = ReportContent.getReportContent(REPORT_NS_COMPOSITE);
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			nsCompositecontents = ValueFormatUtil.transReport(nsCompositecontents, project, unit, allMap, language,
					unitType);
			nsCompositecontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsCompositecontents[0][0];//替换段名称
			createTable(doc,ParagraphAlignment.LEFT, true, false, nsCompositecontents.length, nsCompositecontents[0].length, nsCompositecontents);
			try {
				if(!UtilityConstant.SYS_BLANK.equals(nsCompositecontents[1][7])) {
					double price1 = Double.parseDouble(nsCompositecontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsCompositecontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}
				double price2 = Double.parseDouble(nsCompositecontents[4][7]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
	/**
	 * 创建表格
	 * @param doc
	 * @param pa 水平位置
	 * @param hasTopBorder 顶部边框是否存在
	 * @param bothBold 所有列是否都加粗
	 * @param rows
	 * @param cols
	 * @param contents
	 * @throws Exception
	 */
	public static void createTable(XWPFDocument doc, ParagraphAlignment pa, boolean hasTopBorder, boolean bothBold, int rows, int cols, String[][] contents) throws Exception {
		XWPFTable table = doc.createTable(rows, cols);

		setTableProp(hasTopBorder,table);
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			//设置行的高度
//			theRow.setHeight(180);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(cols,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));

				boolean isBold = false;

				if((j+1)%2 != 0) {
					isBold = true;
					setCellText(cell, getFontFamily(), isBold, contents[i][j], pa);
				}
				if((j+1)%2 == 0) {
					if(bothBold){
						isBold = true;
					}
					setCellText(cell, getFontFamily(),isBold,contents[i][j],pa);
				}
			}
		}
		WordTableGen.addBr(doc);
	}

	/**
	 * 获取列宽
	 * @param cols
	 * @param j
	 * @return
	 */
	private static int getCellWidth(int cols, int j) {
		int cellWidth = 0;
		if(8 == cols) {
			cellWidth = 1000;
			if (j == 0) {
				cellWidth = 1600;
			} else if (j == 7) {
				cellWidth = 400;
			}
		}else{
			cellWidth = 8000/cols;
		}
		return cellWidth;
	}

	/**
	 * 设置单元格内容、样式
	 * @param cell
	 * @param fontFamily
	 * @param isBold
	 * @param text
	 * @param pa
	 */
	private static void setCellText(XWPFTableCell cell ,String fontFamily,boolean isBold,String text,ParagraphAlignment pa){
		XWPFParagraph p1 = cell.getParagraphs().get(0);
		p1.setAlignment(pa);
//		p1.setVerticalAlignment(TextAlignment.TOP);
		XWPFRun r1 = p1.createRun();
		r1.setBold(isBold);
		r1.setText(reCheckText(text));//内容
		r1.setFontFamily(fontFamily);
		r1.setFontSize(8);
	}
	private static String reCheckText(String checkResult){
		checkResult = checkResult.trim().toLowerCase();

		if(UtilityConstant.SYS_ASSERT_TRUE.equals(checkResult)){
			return "是";
		}
		if(UtilityConstant.SYS_ASSERT_FALSE.equals(checkResult)){
			return "否";
		}
		return checkResult;
	}
	/**
	 * 设置表格属性
	 * @param hasTopBorder
	 * @param table
	 */
	private static void setTableProp(boolean hasTopBorder, XWPFTable table) {
		CTTblBorders borders = table.getCTTbl().getTblPr().addNewTblBorders();
		CTBorder hBorder = borders.addNewInsideH();
		hBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		hBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		hBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder vBorder = borders.addNewInsideV();
		vBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		vBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		vBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder lBorder = borders.addNewLeft();
		lBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		lBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		lBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder rBorder = borders.addNewRight();
		rBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		rBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		rBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder tBorder = borders.addNewTop();
		if(hasTopBorder) {
			tBorder.setVal(STBorder.Enum.forString("single"));
			tBorder.setSz(new BigInteger("12"));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setColor("000000");
		}else{
			tBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
			tBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		}
		CTBorder bBorder = borders.addNewBottom();
		bBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		bBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		bBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));


		CTTbl ttbl = table.getCTTbl();
		CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl
				.getTblPr();
		CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
				.addNewTblW();
		CTJc cTJc = tblPr.addNewJc();
		cTJc.setVal(STJc.Enum.forString("center"));
		tblWidth.setW(new BigInteger("8000"));
		tblWidth.setType(STTblWidth.DXA);
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	public static void replaceInPara(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
		XWPFParagraph para;
		while (iterator.hasNext()) {
			para = iterator.next();
			replaceInPara(para, params);
		}
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
    public static void replaceInHeaderPara(XWPFHeader header, Map<String, Object> params) {
        List<XWPFParagraph> headParas = header.getParagraphs();
        for (XWPFParagraph headPara : headParas) {
            replaceInPara(headPara, params);
        }
    }

	/**
	 * 替换段落里面的变量
	 * 
	 * @param para
	 *            要替换的段落
	 * @param params
	 *            参数
	 */
	public static void replaceInPara(XWPFParagraph para, Map<String, Object> params) {
		List<XWPFRun> runs = para.getRuns();
		for (int i = 0; i < runs.size(); i++) {
			XWPFRun run = runs.get(i);
			String runText = run.toString().trim();
			for (Entry<String, Object> e : params.entrySet()) {
				if (runText.contains(e.getKey())) {
					/*直接调用XWPFRun的setText()方法设置文本时，在底层会重新创建一个XWPFRun，把文本附加在当前文本后面，
					所以我们不能直接设值，需要先删除当前run,然后再自己手动插入一个新的run。*/
					para.removeRun(i);
					XWPFRun paragraphRun = para.createRun();
                    paragraphRun.setText(runText.replaceAll(e.getKey(),
                            EmptyUtil.isEmpty(e.getValue()) ? StringUtils.EMPTY : String.valueOf(e.getValue())));
                }
			}

		}

	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
    public static void replaceInHeaderTable(XWPFHeader header, Map<String, Object> params) {
        List<XWPFTable> headTables = header.getTables();
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		for (XWPFTable table : headTables) {
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	public static void replaceInTable(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFTable> iterator = doc.getTablesIterator();
		XWPFTable table;
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		while (iterator.hasNext()) {
			table = iterator.next();
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 关闭输入流
	 * 
	 * @param is
	 */
	public static void close(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 关闭输出流
	 * 
	 * @param os
	 */
	public static void close(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 添加工程格式报告段名称
	 * @param document
	 * @param partName
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addPartName(XWPFDocument doc, String partName) throws DocumentException, IOException {
		XWPFParagraph p = doc.createParagraph();
		p.setAlignment(ParagraphAlignment.LEFT);
		p.setSpacingAfter(10);
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(partName);
	}
	
	/**
	 * 添加三视图
	 * @param doc
	 * @param param
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImage3View(XWPFDocument doc, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成三视图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		String destPath = String.format(
				SysConstants.CAD_DIR + File.separator + "%s" + File.separator + "%s" + File.separator + "%s.bmp", param.getPid(),
				param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);

		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(AHU_TRIPLE_VIEW_SEQUENCE_NUMBER) + param.getUnitNo());//添加标题
		r.addCarriageReturn();
		File file = new File(SysConstants.ASSERT_DIR + File.separator + destPath);
		InputStream is = new FileInputStream(file);
		try {
			r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU((int) (PageSize.A4.getWidth() * 0.75)),
					Units.toEMU((int) (PageSize.A4.getHeight() * 0.45)));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 添加焓湿图
	 * @param document
	 * @param param
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImagePsychometric(XWPFDocument doc, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成焓湿图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		Iterator<PartParam> it = param.getPartParams().iterator();

		List<PsyCalBean> datas = PsychometricDrawer.drawPsy(it);

		String path = PsychometricDrawer.genPsychometric(param.getPid(), param.getUnitid(), datas);

//		Image img = Image.getInstance(SysConstants.ASSERT_DIR + path);
//		img.scalePercent(16);
		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(AHU_PSY_CHART_SEQUENCE_NUMBER) + param.getUnitNo());//添加标题
		r.addCarriageReturn();
		File file = new File(SysConstants.ASSERT_DIR + path.substring(0,path.indexOf("?")));
		InputStream is = new FileInputStream(file);
		try {
			r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU((int) (PageSize.A4.getWidth() * 0.70)),
					Units.toEMU((int) (PageSize.A4.getHeight() * 0.45)));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 添加风机曲线图
	 * @param doc
	 * @param param
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void addImageFan(XWPFDocument doc, AhuParam param, Map<String, Map<String, String>> allMap,
            LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws DocumentException, IOException {
		logger.info("开始生成风机曲线图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		List<PartParam> partParams = param.getPartParams();
		for (PartParam partParam : partParams) {
			if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
				Map<String, Object> params = partParam.getParams();
				String path = String.valueOf(params.get(UtilityConstant.METASEXON_FAN_CURVE));
				path = path.replace(UtilityConstant.SYS_FILES_DIR, UtilityConstant.SYS_BLANK);// 页面使用的为files 后端路径去掉files
				if (EmptyUtil.isNotEmpty(path)) {
					String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_CURVE), project, unit,
							allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
									partParam.getKey())),
							language, unitType);
					Map<String, String> allMap1 = new HashMap<String, String>();
					for (String key : allMap.keySet()) {
						if(key.indexOf(UtilityConstant.METASEXON_FAN)>0) {
							allMap1 = (Map<String, String>)allMap.get(key);
						}
					}
					// 封装风机段特殊字段
					contents = SectionContentConvertUtils.getFan4(contents, allMap1, language);
                    try {
                        File file = new File(SysConstants.ASSERT_DIR + path);
                        InputStream is = new FileInputStream(file);
                        WordTableGen.genFanCurveTable(doc, contents, is, true);
                    } catch (InvalidFormatException e) {
                        e.printStackTrace();
                    }
                    WordTableGen.addBr(doc);
                }
			}
		}
	}
	
	/**
	 * 添加面板布置图
	 * @param doc
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	private static void addImagePanelScreenshots(XWPFDocument doc, Unit unit, Partition partition, LanguageEnum language) throws DocumentException, IOException {
		logger.info("开始添加面板切割布置图：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		List<AhuPartition> partitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
		for(int i=0;i<partitions.size();i++) {
			String destPath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unit.getUnitid()) + File.separator + partition.getPartitionid() + (i+1) + ".png";
			XWPFParagraph p = doc.createParagraph();
			XWPFRun r = p.createRun();
			r.setFontFamily(getFontFamily());
			r.setBold(true);
			r.setFontSize(11);
			r.setText(getIntlString(AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER) + unit.getUnitNo() + "-" + (i+1));//添加标题
			File file = new File(destPath);
			InputStream is = new FileInputStream(file);
			try {
				r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU(PageSize.A4.getWidth() * 0.7),
						Units.toEMU(PageSize.A4.getHeight() * 0.5));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 机组铭牌数据
	 * @param doc
	 * @param arrayMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addUnitNameplateData(XWPFDocument doc, Map<String, String> allMap, Project project,
            UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language)
            throws IOException, DocumentException {
		logger.info("开始添加机组铭牌数据：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		
		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(UNIT_NAMEPLATE_DATA));//添加标题

		String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA), project, unit,
				allMap, language, unitType);
		WordTableGen.addBr(doc);
		WordTableGen.genTechUnitnameplatedata(ahuContents,doc,true);
		
        String[][] titleContents = ValueFormatUtil.transReport(
                ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA_SHAPE), project, unit, allMap, language,
                unitType);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			int i = 2;
			for (AhuPartition ap : ahuPartitionList) {
				double partitionWeight = 0;
                if (AhuPartitionUtils.hasEndFacePanel(ap, ahuPartitionList)) {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), true);
                } else {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), false);
                }
				if (unit.getSeries().contains("39CQ")) {
					String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
							UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()),
							UtilityConstant.SYS_BLANK + (ap.getWidth() * 100 + ap.getCasingWidth()),
							UtilityConstant.SYS_BLANK + (ap.getHeight() * 100 + ap.getCasingWidth()),
							UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
					partitionContents[i] = apArray;
					i++;
				} else {
					String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
							UtilityConstant.SYS_BLANK + (ap.getLength() + ap.getCasingWidth()), UtilityConstant.SYS_BLANK + ap.getWidth(), UtilityConstant.SYS_BLANK + ap.getHeight(),
							UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
					partitionContents[i] = apArray;
					i++;
				}
			}
			WordTableGen.genTechUnitnameplatedata2(partitionContents, doc, false);
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 段连接清单
	 * @param doc
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
	private static void addSectionConnectionList(String paneltype, XWPFDocument doc, Unit unit, LanguageEnum language)
			throws IOException, DocumentException {
		logger.info("开始添加段连接清单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		String unitSeries = AhuUtil.getUnitSeries(unit.getSeries());
		String unitModel = unit.getSeries();
		String[][] sectionConnectionListC = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel, paneltype);
		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		
		String connectionStr=getIntlString(SECTION_CONNECTION_LIST);
		if(UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONFOREPART);
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONPOSITIVE);
			
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONVERTICAL);
			
		}
		r.setText(getIntlString(UNIT_MODEL_COLON) + unit.getSeries() + " " + connectionStr);// 添加标题

		WordTableGen.genSectionConnectionList(sectionConnectionListC, doc, true, paneltype);
		WordTableGen.addBr(doc);
	}	
	
	/**
	 * 产品装箱单
	 * @param doc
	 * @param arrayMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addProductPackingList(XWPFDocument doc,Map<String, Map<String, String>> ahuMap, Map<String, String> allMap, Project project,
            UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language, int connectionNum,List<PartPO> partList)
            throws IOException, DocumentException {
        logger.info("开始添加产品装箱单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
        WordTableGen.addBr(doc);

        String productPackingListName = SectionMetaUtils.getMetaAHUKey(KEY_PRODUCT_PACKING_LIST_NAME);
        String serial = SectionMetaUtils.getMetaAHUKey(KEY_SERIAL);
        allMap.put(productPackingListName,
                BaseDataUtil.constraintString(allMap.get(serial) + getIntlString(I18NConstants.PRODUCT_PACKING_LIST)));
        String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(
                AHUContext.isExportVersion() ? REPORT_TECH_PRODUCTPACKINGLIST_EXPORT : REPORT_TECH_PRODUCTPACKINGLIST),
                project, unit, allMap, language, unitType);
        int PTMNum=0;
        for(PartPO part:partList) {
        	
        	if(SectionTypeEnum.TYPE_FAN.getId().equals(part.getCurrentPart().getSectionKey())) {
        		Map<String, String> fanMap= ahuMap.get(UnitConverter.genUnitKey(unit, part.getCurrentPart()));
        		if(Boolean.valueOf(String.valueOf(fanMap.get(METASEXON_FAN_PTC)))) {
        			PTMNum++;
        		}
        	}
        }
        
        if(PTMNum==0) {
        	for(int i=0;i<ahuContents[0].length;i++) {
				ahuContents[5][i] = UtilityConstant.SYS_BLANK;
			}
        }else {
        	ahuContents[5][3]=String.valueOf(PTMNum);
        }
        
        if (connectionNum==0) {//没有分段的时候，去掉段连接信息
			for(int i=0;i<ahuContents[0].length;i++) {
				ahuContents[6][i] = UtilityConstant.SYS_BLANK;
			}			
		}else {
			ahuContents[6][3]=String.valueOf(connectionNum);
		}
        
        WordTableGen.genTechProductPackingListdata(ahuContents, doc, true);
        WordTableGen.addBr(doc);
    }
}
