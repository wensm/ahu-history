package com.carrier.ahu.section.meta;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.po.meta.RulesVO;
import com.carrier.ahu.po.meta.SerialVO;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * @deprecated use SectionMetaUtils instead of.
 */
@Deprecated
@Slf4j
public class MetaValueGen {
	private static MetaValueGen instance;
	private Map<String, MetaValue> metaValues = new HashMap<>();
	private Map<String, List<RulesVO>> metaValidation = new HashMap<String, List<RulesVO>>();
	private Map<String, List<SerialVO>> metaSerial = new HashMap<String, List<SerialVO>>();

	private MetaValueGen() {
	}

	private void init() {
		for (int i = 0; i < AhuSectionMetas.SECTIONTYPES.length; i++) {
			SectionTypeEnum type = AhuSectionMetas.SECTIONTYPES[i];
			String id = type.getId();
			// First check if the json file exist;
			Map<String, MetaValue> values = loadValueFile(id);
			if (EmptyUtil.isNotEmpty(values)) {
				// 为了兼容value的key值只有simple name，这儿对简单名字做一次转换
				Iterator<String> it = values.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					if (key.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {// 全名
						metaValues.put(key, values.get(key));
					} else {
						String valueId = MetaCodeGen.calculateAttributePrefix(id) + UtilityConstant.SYS_PUNCTUATION_DOT + key;
						metaValues.put(valueId, values.get(key));
					}
				}
			} else {
				log.info(String.format("Failed to resolve source for: ", id));
			}
		}
	}

	private void initValidation() {
		for (int i = 0; i < AhuSectionMetas.SECTIONTYPES.length; i++) {
			SectionTypeEnum type = AhuSectionMetas.SECTIONTYPES[i];
			String id = type.getId();
			// First check if the json file exist;
			Map<String, List<RulesVO>> values = loadValidationFile(id);
			if (EmptyUtil.isNotEmpty(values)) {
				// 为了兼容value的key值只有simple name，这儿对简单名字做一次转换
				Iterator<String> it = values.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					if (key.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {// 全名
						metaValidation.put(key, values.get(key));
					} else {
						String valueId = MetaCodeGen.calculateAttributePrefix(id) + UtilityConstant.SYS_PUNCTUATION_DOT + key;
						metaValidation.put(valueId, values.get(key));
					}
				}
			} else {
				log.info(String.format("Failed to resolve source for: ", id));
			}
		}
	}
    private void initSerial() {
        // First check if the json file exist;
        String jsonName = "ahu.39CQ";//加载39CQ 后续加载其它系列重新管理
        Map<String, List<SerialVO>> values = loadSerialFile(jsonName);
        if (EmptyUtil.isNotEmpty(values)) {
            // 为了兼容value的key值只有simple name，这儿对简单名字做一次转换
            Iterator<String> it = values.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next();
                if (key.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {// 全名
                    metaSerial.put(key, values.get(key));
                } else {
                    String valueId = MetaCodeGen.calculateAttributePrefix(jsonName) + UtilityConstant.SYS_PUNCTUATION_DOT + key;
                    metaSerial.put(valueId, values.get(key));
                }
            }
        } else {
            log.info(String.format("Failed to resolve source for: ", jsonName));
        }
    }

	private Map<String, MetaValue> loadValueFile(String fileName) {
		try {
			InputStream is = AhuSectionMetas.class.getClassLoader().getResourceAsStream(
					MessageFormat.format(FileNamesLoadInSystem.SECTIONS_VALUE_JSON_NAME, fileName));
			if (EmptyUtil.isEmpty(is)) {
				log.info(String.format("Faile to get json file for: %s", fileName));
				return null;
			}
			Gson gson = new Gson();
			Map<String, MetaValue> result = gson.fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),
					new TypeToken<Map<String, MetaValue>>() {
					}.getType());
			log.debug("Successfully to resolve json file for: " + fileName);
			// initPair
			Iterator<MetaValue> it = result.values().iterator();
			while (it.hasNext()) {
				MetaValue value = it.next();
				value.getOption().initValuePair(value.getKey());
			}
			return result;
		} catch (JsonIOException | JsonSyntaxException e) {
			log.error("Failed to resolve file : " + fileName, e);
		}
		return null;

	}

	private Map<String, List<RulesVO>> loadValidationFile(String fileName) {
		try {
			InputStream is = AhuSectionMetas.class.getClassLoader().getResourceAsStream(
					MessageFormat.format(FileNamesLoadInSystem.SECTIONS_VALIDATION_JSON_NAME, fileName));
			if (EmptyUtil.isEmpty(is)) {
				log.info(String.format("Faile to get json file for: %s", fileName));
				return null;
			}
			Gson gson = new Gson();
			Map<String, List<RulesVO>> result = gson.fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),new TypeToken<Map<String, List<RulesVO>>>() {}.getType());
			log.debug("Successfully to resolve json file for: " + fileName);
			return result;
		} catch (JsonIOException | JsonSyntaxException e) {
			log.error("Failed to resolve file : " + fileName, e);
		}
		return null;
	}

	private Map<String, List<SerialVO>> loadSerialFile(String fileName) {
		try {
			InputStream is = AhuSectionMetas.class.getClassLoader().getResourceAsStream(
					MessageFormat.format(FileNamesLoadInSystem.SECTIONS_SERIAL_JSON_NAME, fileName));
			if (EmptyUtil.isEmpty(is)) {
				log.info(String.format("Faile to get json file for: %s", fileName));
				return null;
			}
			Gson gson = new Gson();
			Map<String, List<SerialVO>> result = gson.fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),new TypeToken<Map<String, List<SerialVO>>>() {}.getType());
			log.debug("Successfully to resolve json file for: " + fileName);
			return result;
		} catch (JsonIOException | JsonSyntaxException e) {
			log.error("Failed to resolve file : " + fileName, e);
		}
		return null;
	}

	public Map<String, MetaValue> getMetaValueMap() {
		return metaValues;
	}

	public Map<String, List<RulesVO>> getMetaValidationMap() {
		return metaValidation;
	}

    public Map<String, List<SerialVO>> getMetaSerial() {
        return metaSerial;
    }

    public static MetaValueGen getInstance() {
		if (EmptyUtil.isEmpty(instance)) {
			instance = new MetaValueGen();
			instance.init();
			instance.initValidation();
			instance.initSerial();
		}
		return instance;
	}

	/**
	 * Tool method, to force the instance to be reloaded
	 */
	public static void reloadMeta() {
		instance = null;
		getInstance();
	}

	public static void main(String[] args) {
		String fileName = "ahu";
		try {
			InputStream is = AhuSectionMetas.class.getClassLoader().getResourceAsStream(
					MessageFormat.format(FileNamesLoadInSystem.SECTIONS_VALUE_JSON_NAME, fileName));
			if (EmptyUtil.isEmpty(is)) {
				log.info(String.format("Faile to get json file for: %s", fileName));
			}
			Gson gson = new Gson();
			Map<String, MetaValue> result = gson.fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),
					new TypeToken<Map<String, MetaValue>>() {
					}.getType());
			log.debug("Successfully to resolve json file for: " + fileName);
			// initPair
			Iterator<MetaValue> it = result.values().iterator();
			while (it.hasNext()) {
				MetaValue value = it.next();
				value.getOption().initValuePair(value.getKey());
			}
		} catch (JsonIOException | JsonSyntaxException e) {
			log.error("Failed to resolve file : " + fileName, e);
		}
	}
}
