package com.carrier.ahu.util.upgrade;

import static com.carrier.ahu.constant.CommonConstant.SYS_JAR_EXTENSION;
import static com.carrier.ahu.constant.CommonConstant.SYS_UPGRADE_FILE_PREFIX;
import static com.carrier.ahu.constant.CommonConstant.SYS_ZIP_EXTENSION;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UpgradeUtilsTest {

    @Test
    public void testIsValidZipVersion() {
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("0.9"), "1.0"));
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("1.0"), "1.0"));
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("1.0"), "1.0.0"));
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.0"), "1.0"));
        assertTrue(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.1"), "1.0"));
        assertTrue(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.12"), "1.0"));
        assertTrue(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.2"), "1.0.1"));
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("1.0"), "1.0.10"));
        assertFalse(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.9"), "1.0.10"));
        assertTrue(UpgradeUtils.isValidZipVersion(getZipFileName("1.0.11"), "1.0.10"));
    }

    private String getZipFileName(String version) {
        return SYS_UPGRADE_FILE_PREFIX + version + SYS_ZIP_EXTENSION;
    }

    private String getJarFileName(String version) {
        return SYS_UPGRADE_FILE_PREFIX + version + SYS_JAR_EXTENSION;
    }

    @Test
    public void testIsValidJarVersion() {
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("0.9"), "1.0"));
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("1.0"), "1.0"));
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("1.0"), "1.0.0"));
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.0"), "1.0"));
        assertTrue(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.1"), "1.0"));
        assertTrue(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.12"), "1.0"));
        assertTrue(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.2"), "1.0.1"));
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("1.0"), "1.0.10"));
        assertFalse(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.9"), "1.0.10"));
        assertTrue(UpgradeUtils.isValidJarVersion(getJarFileName("1.0.11"), "1.0.10"));
    }

}
