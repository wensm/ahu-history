package com.carrier.ahu.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.carrier.ahu.common.enums.FactoryEnum;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.po.meta.RulesVO;
import com.carrier.ahu.po.meta.SerialVO;

/**
 * Test for section meta data, serial, values and validation handling different
 * factory and unit series.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
public class SectionMetadataTest {

    @Test
    public void testSectionValues() {
        String factory = FactoryEnum.THC.name();
        String unitSeries = "39G";
        String key = "meta.ahu.exskincolor";
        Map<String, MetaValue> metaValue = SectionMetadata.getMetaValue(factory);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("white", metaValue.get(key).getDefaultValue());

        metaValue = SectionMetadata.getMetaValue(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("blue", metaValue.get(key).getDefaultValue());

        metaValue = SectionMetadata.getMetaValue(factory);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("white", metaValue.get(key).getDefaultValue());

        metaValue = SectionMetadata.getMetaValue(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("blue", metaValue.get(key).getDefaultValue());
    }

    @Test
    public void testSectionValidations() {
        String factory = FactoryEnum.THC.name();
        String unitSeries = "39G";
        String key = "meta.ahu.sairvolume";
        Map<String, List<RulesVO>> metaValue = SectionMetadata.getMetaValidation(factory);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("2000~200000", metaValue.get(key).get(0).getRuleValueSet().iterator().next());

        metaValue = SectionMetadata.getMetaValidation(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("3000~300000", metaValue.get(key).get(0).getRuleValueSet().iterator().next());

        metaValue = SectionMetadata.getMetaValidation(factory);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("2000~200000", metaValue.get(key).get(0).getRuleValueSet().iterator().next());

        metaValue = SectionMetadata.getMetaValidation(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("3000~300000", metaValue.get(key).get(0).getRuleValueSet().iterator().next());
    }

    @Test
    public void testSectionSerial() {
        String factory = FactoryEnum.THC.name();
        String unitSeries = "39G";
        String key = "ahu.wheelHeatRecycle";
        Map<String, List<SerialVO>> metaValue = SectionMetadata.getMetaSerial(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("39G0608-39G2532", metaValue.get(key).get(0).getSerial());

        unitSeries = "39CQ";
        metaValue = SectionMetadata.getMetaSerial(factory, unitSeries);
        assertNotNull(metaValue);
        assertTrue(metaValue.containsKey(key));
        assertEquals("39CQ0608-39CQ2532", metaValue.get(key).get(0).getSerial());
    }

}
