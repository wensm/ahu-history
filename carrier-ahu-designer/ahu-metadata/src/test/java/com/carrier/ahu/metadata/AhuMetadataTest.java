package com.carrier.ahu.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.carrier.ahu.datahouse.Datahouse;
import com.carrier.ahu.datahouse.DatahouseFactory;
import com.carrier.ahu.metadata.common.FileConstants;
import com.carrier.ahu.metadata.entity.AhuPropSetting;
import com.carrier.ahu.metadata.entity.AhuPropUnit;
import com.carrier.ahu.metadata.entity.AhuSize;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.metadata.entity.access.AccessData;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionL;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionR;
import com.carrier.ahu.metadata.entity.cad.CoilBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.CoilDimension;
import com.carrier.ahu.metadata.entity.cad.ColdCoilDimension;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionFac;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionL;
import com.carrier.ahu.metadata.entity.cad.ECoilDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimensionBMotor;
import com.carrier.ahu.metadata.entity.cad.FilterDrawing;
import com.carrier.ahu.metadata.entity.cad.HeatCoilDimension;
import com.carrier.ahu.metadata.entity.cad.PaneledDoor;
import com.carrier.ahu.metadata.entity.cad.SteamBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.SteamCoilDimension;
import com.carrier.ahu.metadata.entity.calc.PriceCalculatorSpec;
import com.carrier.ahu.metadata.entity.calc.SUVCLight;
import com.carrier.ahu.metadata.entity.calc.WeightCalculatorSpec;
import com.carrier.ahu.metadata.entity.coil.CoilDllCalculation;
import com.carrier.ahu.metadata.entity.coil.CoilSize;
import com.carrier.ahu.metadata.entity.coil.S4FArea;
import com.carrier.ahu.metadata.entity.coil.SCalSeq;
import com.carrier.ahu.metadata.entity.coil.SCoilCal;
import com.carrier.ahu.metadata.entity.coil.SCoilCal1;
import com.carrier.ahu.metadata.entity.coil.SCoilCalEuro;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo1;
import com.carrier.ahu.metadata.entity.coil.SDPAirFactorHDia1;
import com.carrier.ahu.metadata.entity.coil.SDPAirFactorHDia2;
import com.carrier.ahu.metadata.entity.coil.SSurfaceGeometrics;
import com.carrier.ahu.metadata.entity.coil.SWaterDropFactor;
import com.carrier.ahu.metadata.entity.coil.SWaterDropFactorAhri;
import com.carrier.ahu.metadata.entity.coil.SZua;
import com.carrier.ahu.metadata.entity.coil.TvColdCoilGridHeader;
import com.carrier.ahu.metadata.entity.coil.TvColdCoilGridHeaderE;
import com.carrier.ahu.metadata.entity.coil.TvHeatCoilGridHeader;
import com.carrier.ahu.metadata.entity.coil.TvHeatCoilGridHeaderE;
import com.carrier.ahu.metadata.entity.fan.Bdd;
import com.carrier.ahu.metadata.entity.fan.Bds;
import com.carrier.ahu.metadata.entity.fan.FanMotor;
import com.carrier.ahu.metadata.entity.fan.Noise;
import com.carrier.ahu.metadata.entity.fan.PlugFanType;
import com.carrier.ahu.metadata.entity.fan.SFan;
import com.carrier.ahu.metadata.entity.fan.SFanLength;
import com.carrier.ahu.metadata.entity.fan.SFanLength1;
import com.carrier.ahu.metadata.entity.fan.SFanLength2;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SKFanMotorEff;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.metadata.entity.fan.SKFanType1;
import com.carrier.ahu.metadata.entity.fan.SKPlugFanType;
import com.carrier.ahu.metadata.entity.fan.SSectionKBIG;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.metadata.entity.filter.ALSingleFilterResistance;
import com.carrier.ahu.metadata.entity.filter.ElectrostaticFilterResistance;
import com.carrier.ahu.metadata.entity.filter.FilterComposing;
import com.carrier.ahu.metadata.entity.filter.HEPAFilterComposing;
import com.carrier.ahu.metadata.entity.filter.HEPAFilterResistance;
import com.carrier.ahu.metadata.entity.filter.NWFSingleFilterResistance;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWPlate;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.metadata.entity.heatrecycle.Rotorpara;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SISupplier;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.metadata.entity.mix.MixSize;
import com.carrier.ahu.metadata.entity.panel.SPanelDBigOperate;
import com.carrier.ahu.metadata.entity.panel.SPanelDBottomCQ;
import com.carrier.ahu.metadata.entity.panel.SPanelDNormalSide;
import com.carrier.ahu.metadata.entity.price.CsPriceRate;
import com.carrier.ahu.metadata.entity.price.NonstdMotor;
import com.carrier.ahu.metadata.entity.price.PriceCode;
import com.carrier.ahu.metadata.entity.report.AdjustAirDoor;
import com.carrier.ahu.metadata.entity.report.CoilInfo;
import com.carrier.ahu.metadata.entity.report.DeliveryInfo;
import com.carrier.ahu.metadata.entity.report.SForFrance;
import com.carrier.ahu.metadata.entity.report.SKAbsorber;
import com.carrier.ahu.metadata.entity.report.STakeoverSize;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResi;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResiBig;
import com.carrier.ahu.metadata.entity.resistance.SClearSoundResi;
import com.carrier.ahu.metadata.entity.resistance.SElectricityResi;
import com.carrier.ahu.metadata.entity.resistance.SJunliuqiResi;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.metadata.entity.section.SectionParameter;
import com.carrier.ahu.metadata.entity.validation.S4GRow;

/**
 * Created by Braden Zhou on 2018/04/27.
 */
public class AhuMetadataTest {

    @Test
    public void testAhuSize() {
        Datahouse<AhuSize, String> ahuSizeData = DatahouseFactory.csv(AhuSize.class, FileConstants.AHU_SIZE_CSV);

        String id = "39CQ1621";
        List<AhuSize> ahuSizes = ahuSizeData.findAll();
        assertNotNull(ahuSizes);
        AhuSize ahuSize = ahuSizeData.findOne(id);
        assertNotNull(ahuSize);
        assertEquals(ahuSize.getAhu(), id);
        boolean exists = ahuSizeData.exists(id);
        assertTrue(exists);
        int count = ahuSizeData.count();
        assertEquals(count, ahuSizes.size());
    }

    @Test
    public void testAhuSizeDetail() {
        Datahouse<AhuSizeDetail, String> ahuSizeDetailData = DatahouseFactory.csv(AhuSizeDetail.class,
                FileConstants.AHU_SIZE_DETAIL_CSV);
        String id = "39CQ1621";
        List<AhuSizeDetail> ahuSizeDetails = ahuSizeDetailData.findAll();
        assertNotNull(ahuSizeDetails);
        AhuSize ahuSize = ahuSizeDetailData.findOne(id);
        assertNotNull(ahuSize);
        assertEquals(ahuSize.getAhu(), id);
        boolean exists = ahuSizeDetailData.exists(id);
        assertTrue(exists);
        int count = ahuSizeDetailData.count();
        assertEquals(count, ahuSizeDetails.size());
    }

    @Test
    public void testAhuMetadata() {
        List<AhuSize> ahuSizes = AhuMetadata.findAll(AhuSize.class);
        assertNotNull(ahuSizes);
        List<AhuSizeDetail> ahuSizeDetails = AhuMetadata.findAll(AhuSizeDetail.class);
        assertNotNull(ahuSizeDetails);

        String id = "39CQ1621";
        AhuSize ahuSize = AhuMetadata.findOne(AhuSize.class, id);
        assertNotNull(ahuSize);
        assertEquals(ahuSize.getAhu(), id);
        boolean exists = AhuMetadata.exists(AhuSize.class, id);
        assertTrue(exists);
        int count = AhuMetadata.count(AhuSize.class);
        assertEquals(count, ahuSizes.size());
    }

    @Test
    public void testCoilSize() {
        List<CoilSize> coilSizes = AhuMetadata.findAll(CoilSize.class);
        assertNotNull(coilSizes);

        String id = "39CQ1621";
        CoilSize coilSize = AhuMetadata.findOne(CoilSize.class, id);
        assertNotNull(coilSize);
        assertEquals(coilSize.getAhu(), id);
        boolean exists = AhuMetadata.exists(CoilSize.class, id);
        assertTrue(exists);
        int count = AhuMetadata.count(CoilSize.class);
        assertEquals(count, coilSizes.size());
    }

    @Test
    public void testCoilDllCalculation() {
        List<CoilDllCalculation> coilDllCalculations = AhuMetadata.findAll(CoilDllCalculation.class);
        assertNotNull(coilDllCalculations);

        String id = "39G1822";
        CoilDllCalculation coilDllCalculation = AhuMetadata.findOne(CoilDllCalculation.class, id);
        assertNotNull(coilDllCalculation);
        assertEquals(coilDllCalculation.getAhu(), id);
        boolean exists = AhuMetadata.exists(CoilDllCalculation.class, id);
        assertTrue(exists);
        int count = AhuMetadata.count(CoilDllCalculation.class);
        assertEquals(count, coilDllCalculations.size());
    }

    @Test
    public void testElectrostaticFilterResistance() {
        List<ElectrostaticFilterResistance> resistances = AhuMetadata.findAll(ElectrostaticFilterResistance.class);
        assertNotNull(resistances);
        int count = AhuMetadata.count(ElectrostaticFilterResistance.class);
        assertEquals(count, resistances.size());
    }

    @Test
    public void testNWFSingleFilterResistance() {
        List<NWFSingleFilterResistance> resistances = AhuMetadata.findAll(NWFSingleFilterResistance.class);
        assertNotNull(resistances);
        int count = AhuMetadata.count(NWFSingleFilterResistance.class);
        assertEquals(count, resistances.size());
    }

    @Test
    public void testALSingleFilterResistance() {
        List<ALSingleFilterResistance> resistances = AhuMetadata.findAll(ALSingleFilterResistance.class);
        assertNotNull(resistances);
        int count = AhuMetadata.count(ALSingleFilterResistance.class);
        assertEquals(count, resistances.size());
    }

    @Test
    public void testHEPAFilterResistance() {
        List<HEPAFilterResistance> resistances = AhuMetadata.findAll(HEPAFilterResistance.class);
        assertNotNull(resistances);
        int count = AhuMetadata.count(HEPAFilterResistance.class);
        assertEquals(count, resistances.size());
    }

    @Test
    public void testFilterComposing() {
        List<FilterComposing> fcs = AhuMetadata.findAll(FilterComposing.class);
        assertNotNull(fcs);
        int count = AhuMetadata.count(FilterComposing.class);
        assertEquals(count, fcs.size());

        String fanType = "39CQ0608";
        String filterFormat = "3";
        FilterComposing sfan = AhuMetadata.findOne(FilterComposing.class, fanType, filterFormat);
        assertEquals(fanType, sfan.getFanType());
        assertEquals(filterFormat, String.valueOf(sfan.getFilterFormat()));
    }

    @Test
    public void testHEPAFilterComposing() {
        List<HEPAFilterComposing> fcs = AhuMetadata.findAll(HEPAFilterComposing.class);
        assertNotNull(fcs);
        int count = AhuMetadata.count(HEPAFilterComposing.class);
        assertEquals(count, fcs.size());

        String fanType = "39CQ0608";
        String filterFormat = "3";
        HEPAFilterComposing sfan = AhuMetadata.findOne(HEPAFilterComposing.class, fanType, filterFormat);
        assertEquals(fanType, sfan.getFanType());
        assertEquals(filterFormat, String.valueOf(sfan.getFilterFormat()));
    }

    @Test
    public void testPlugFanType() {
        List<PlugFanType> plugFanTypes = AhuMetadata.findAll(PlugFanType.class);
        assertNotNull(plugFanTypes);

        String id = "0711";
        PlugFanType plugFanType = AhuMetadata.findOne(PlugFanType.class, id);
        assertNotNull(plugFanType);
        assertEquals(plugFanType.getAhu(), id);
        boolean exists = AhuMetadata.exists(PlugFanType.class, id);
        assertTrue(exists);
        int count = AhuMetadata.count(PlugFanType.class);
        assertEquals(count, plugFanTypes.size());
    }

    @Test
    public void testFanMotor() {
        List<FanMotor> fanMotors = AhuMetadata.findAll(FanMotor.class);
        assertNotNull(fanMotors);
        int count = AhuMetadata.count(FanMotor.class);
        assertEquals(count, fanMotors.size());

        FanMotor fanMotor = AhuMetadata.findOne(FanMotor.class, "0.37", "6");
        assertEquals("0.37", String.valueOf(fanMotor.getPower()));
        assertEquals(6, fanMotor.getLevel());

        fanMotor = AhuMetadata.findOne(FanMotor.class, "1.1", "2");
        assertEquals("1.1", String.valueOf(fanMotor.getPower()));
        assertEquals(2, fanMotor.getLevel());
    }

    @Test
    public void testSFan() {
        List<SFan> sfans = AhuMetadata.findAll(SFan.class);
        assertNotNull(sfans);
        int count = AhuMetadata.count(SFan.class);
        assertEquals(count, sfans.size());

        String fanType = "39CQ4444";
        SFan sfan = AhuMetadata.findOne(SFan.class, fanType);
        assertEquals(fanType, sfan.getType());
    }

    @Test
    public void testSFanLength() {
        List<SFanLength> sfans = AhuMetadata.findAll(SFanLength.class);
        assertNotNull(sfans);
        int count = AhuMetadata.count(SFanLength.class);
        assertEquals(count, sfans.size());

        String id = "1";
        String type = "200";
        SFanLength sfan = AhuMetadata.findOne(SFanLength.class, id, type);
        assertEquals(id, sfan.getId());
        assertEquals(type, sfan.getType());
    }

    @Test
    public void testSFanLength1() {
        List<SFanLength1> sfans = AhuMetadata.findAll(SFanLength1.class);
        assertNotNull(sfans);
        int count = AhuMetadata.count(SFanLength1.class);
        assertEquals(count, sfans.size());

        String unitType = "39CQ0711";
        String id = "1";
        String type = "200";
        SFanLength1 sfan = AhuMetadata.findOne(SFanLength1.class, unitType, id, type);
        assertEquals(unitType, sfan.getUnitType());
        assertEquals(id, sfan.getId());
        assertEquals(type, sfan.getType());
    }

    @Test
    public void testSFanLength2() {
        List<SFanLength2> sfans = AhuMetadata.findAll(SFanLength2.class);
        assertNotNull(sfans);
        int count = AhuMetadata.count(SFanLength2.class);
        assertEquals(count, sfans.size());

        String unitType = "39CQ0711";
        String id = "1";
        String type = "SYW315R";
        SFanLength2 sfan = AhuMetadata.findOne(SFanLength2.class, unitType, id, type);
        assertEquals(unitType, sfan.getUnitType());
        assertEquals(id, sfan.getId());
        assertEquals(type, sfan.getType());
    }

    @Test
    public void testSSectionKBIG() {
        List<SSectionKBIG> sskbs = AhuMetadata.findAll(SSectionKBIG.class);
        assertNotNull(sskbs);
        int count = AhuMetadata.count(SSectionKBIG.class);
        assertEquals(count, sskbs.size());

        String unitType = "39CBF2532";
        String fanType = "900";
        String airDir = "D";
        SSectionKBIG sskb = AhuMetadata.findOne(SSectionKBIG.class, unitType, fanType, airDir);
        assertEquals(unitType, sskb.getUnitType());
        assertEquals(fanType, sskb.getFanType());
        assertEquals(airDir, sskb.getAirDir());
    }

    @Test
    public void testSectionArea() {
        List<SectionArea> sareas = AhuMetadata.findAll(SectionArea.class);
        assertNotNull(sareas);
        int count = AhuMetadata.count(SectionArea.class);
        assertEquals(count, sareas.size());

        String fanType = "39CQ0912";
        SectionArea sarea = AhuMetadata.findOne(SectionArea.class, fanType);
        assertEquals(fanType, sarea.getFanType());
    }

    @Test
    public void testSectionLength() {
        List<SectionLength> slengths = AhuMetadata.findAll(SectionLength.class);
        assertNotNull(slengths);
        int count = AhuMetadata.count(SectionLength.class);
        assertEquals(count, slengths.size());

        String type = "39CQ0912";
        SectionLength slength = AhuMetadata.findOne(SectionLength.class, type);
        assertEquals(type, slength.getType());
    }

    @Test
    public void testAdjustAirDoor() {
        List<AdjustAirDoor> aadoors = AhuMetadata.findAll(AdjustAirDoor.class);
        assertNotNull(aadoors);
        int count = AhuMetadata.count(AdjustAirDoor.class);
        assertEquals(count, aadoors.size());

        String fanType = "39CQ0609";
        String section = "A";
        String whichFlag = "1";
        AdjustAirDoor aadoor = AhuMetadata.findOne(AdjustAirDoor.class, fanType, section, whichFlag);
        assertEquals(fanType, aadoor.getFanType());
        assertEquals(section, aadoor.getSection());
        assertEquals(whichFlag, aadoor.getWhichFlag());
    }

    @Test
    public void testCoilInfo() {
        List<CoilInfo> coilInfos = AhuMetadata.findAll(CoilInfo.class);
        assertNotNull(coilInfos);
        int count = AhuMetadata.count(CoilInfo.class);
        assertEquals(count, coilInfos.size());

        String unit = "39G0914";
        String row = "1";
        String circuit = "HF";
        CoilInfo coilInfo = AhuMetadata.findOne(CoilInfo.class, unit, row, circuit);
        assertEquals(unit, coilInfo.getUnitModel());
        assertEquals(row, coilInfo.getRow());
        assertEquals(circuit, coilInfo.getCircuit());
    }

    @Test
    public void testDeliveryInfo() {
        List<DeliveryInfo> slengths = AhuMetadata.findAll(DeliveryInfo.class);
        assertNotNull(slengths);
        int count = AhuMetadata.count(DeliveryInfo.class);
        assertEquals(count, slengths.size());

        String partName = "FAN";
        String option = "ADA315CM";
        DeliveryInfo slength = AhuMetadata.findOne(DeliveryInfo.class, partName, option);
        assertEquals(partName, slength.getPartName());
        assertEquals(option, slength.getOption());
    }

    @Test
    public void testCoilBigDimensionL() {
        List<CoilBigDimensionL> dimensions = AhuMetadata.findAll(CoilBigDimensionL.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(CoilBigDimensionL.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ3132";
        CoilBigDimensionL dimension = AhuMetadata.findOne(CoilBigDimensionL.class, unit);
        assertEquals(unit, dimension.getUnit());
    }

    @Test
    public void testCoilBigDimensionR() {
        List<CoilBigDimensionR> dimensions = AhuMetadata.findAll(CoilBigDimensionR.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(CoilBigDimensionR.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ3132";
        CoilBigDimensionR dimension = AhuMetadata.findOne(CoilBigDimensionR.class, unit);
        assertEquals(unit, dimension.getUnit());
    }

    @Test
    public void testCoilBigTuojiaDimension() {
        List<CoilBigTuojiaDimension> dimensions = AhuMetadata.findAll(CoilBigTuojiaDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(CoilBigTuojiaDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ3132";
        CoilBigTuojiaDimension dimension = AhuMetadata.findOne(CoilBigTuojiaDimension.class, unit);
        assertEquals(unit, dimension.getUnit());
    }

    @Test
    public void testCoilDimension() {
        List<CoilDimension> dimensions = AhuMetadata.findAll(CoilDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(CoilDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ1015";
        CoilDimension dimension = AhuMetadata.findOne(CoilDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testColdCoilDimension() {
        List<ColdCoilDimension> dimensions = AhuMetadata.findAll(ColdCoilDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(ColdCoilDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39G1015";
        ColdCoilDimension dimension = AhuMetadata.findOne(ColdCoilDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testDamperDimension() {
        List<DamperDimension> dimensions = AhuMetadata.findAll(DamperDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(DamperDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CBFI";
        String mixType = "1";
        String sectionL = "6";
        DamperDimension dimension = AhuMetadata.findOne(DamperDimension.class, unit, mixType, sectionL);
        assertEquals(unit, dimension.getUnit());
        assertEquals(mixType, dimension.getMixType());
        assertEquals(sectionL, dimension.getSectionL());
    }

    @Test
    public void testDamperDimensionFac() {
        List<DamperDimensionFac> dimensions = AhuMetadata.findAll(DamperDimensionFac.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(DamperDimensionFac.class);
        assertEquals(count, dimensions.size());

        String unit = "39G1420";
        DamperDimensionFac dimension = AhuMetadata.findOne(DamperDimensionFac.class, unit);
        assertEquals(unit, dimension.getUnit());
    }

    @Test
    public void testDamperDimensionL() {
        List<DamperDimensionL> dimensions = AhuMetadata.findAll(DamperDimensionL.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(DamperDimensionL.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ0914";
        String mixType = "1";
        String damperType = "1";
        DamperDimensionL dimension = AhuMetadata.findOne(DamperDimensionL.class, unit, mixType, damperType);
        assertEquals(unit, dimension.getUnit());
        assertEquals(mixType, dimension.getMixType());
        assertEquals(damperType, dimension.getDamperType());

        mixType = "2";
        damperType = "2";
        dimension = AhuMetadata.findOne(DamperDimensionL.class, unit, mixType, damperType);
        assertEquals(unit, dimension.getUnit());
        assertEquals(mixType, dimension.getMixType());
        assertEquals(damperType, dimension.getDamperType());
    }

    @Test
    public void testECoilDimension() {
        List<ECoilDimension> dimensions = AhuMetadata.findAll(ECoilDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(ECoilDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39G1621";
        ECoilDimension dimension = AhuMetadata.findOne(ECoilDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testFanDimension() {
        List<FanDimension> dimensions = AhuMetadata.findAll(FanDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(FanDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39G1015";
        String fanType = "H";
        String unitModel = "1";
        FanDimension dimension = AhuMetadata.findOne(FanDimension.class, unit, fanType, unitModel);
        assertEquals(unit, dimension.getUnitType());
        assertEquals(fanType, dimension.getFanType());
        assertEquals(unitModel, dimension.getUnitModel());
    }

    @Test
    public void testFanDimensionBMotor() {
        List<FanDimensionBMotor> dimensions = AhuMetadata.findAll(FanDimensionBMotor.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(FanDimensionBMotor.class);
        assertEquals(count, dimensions.size());

        String unit = "39G1015";
        String fanType = "H";
        String unitModel = "1";
        FanDimensionBMotor dimension = AhuMetadata.findOne(FanDimensionBMotor.class, unit, fanType, unitModel);
        assertEquals(unit, dimension.getUnitType());
        assertEquals(fanType, dimension.getFanType());
        assertEquals(unitModel, dimension.getUnitModel());
    }

    @Test
    public void testFilterDrawing() {
        List<FilterDrawing> fdrawings = AhuMetadata.findAll(FilterDrawing.class);
        assertNotNull(fdrawings);
        int count = AhuMetadata.count(FilterDrawing.class);
        assertEquals(count, fdrawings.size());

        String fanType = "39CQ1418";
        String filterFormat = "3";
        FilterDrawing fdrawing = AhuMetadata.findOne(FilterDrawing.class, fanType, filterFormat);
        assertEquals(fanType, fdrawing.getFanType());
        assertEquals(filterFormat, fdrawing.getFilterFormat());
    }

    @Test
    public void testHeatCoilDimension() {
        List<HeatCoilDimension> dimensions = AhuMetadata.findAll(HeatCoilDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(HeatCoilDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ1015";
        HeatCoilDimension dimension = AhuMetadata.findOne(HeatCoilDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testPaneledDoor() {
        List<PaneledDoor> fdrawings = AhuMetadata.findAll(PaneledDoor.class);
        assertNotNull(fdrawings);
        int count = AhuMetadata.count(PaneledDoor.class);
        assertEquals(count, fdrawings.size());

        String unit = "39CBF0711";
        String partType = "12";
        PaneledDoor fdrawing = AhuMetadata.findOne(PaneledDoor.class, unit, partType);
        assertEquals(unit, fdrawing.getUnit());
        assertEquals(partType, fdrawing.getPartType());
    }

    @Test
    public void testSteamBigTuojiaDimension() {
        List<SteamBigTuojiaDimension> dimensions = AhuMetadata.findAll(SteamBigTuojiaDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(SteamBigTuojiaDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ4444";
        SteamBigTuojiaDimension dimension = AhuMetadata.findOne(SteamBigTuojiaDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testSteamCoilDimension() {
        List<SteamCoilDimension> dimensions = AhuMetadata.findAll(SteamCoilDimension.class);
        assertNotNull(dimensions);
        int count = AhuMetadata.count(SteamCoilDimension.class);
        assertEquals(count, dimensions.size());

        String unit = "39CQ2025";
        SteamCoilDimension dimension = AhuMetadata.findOne(SteamCoilDimension.class, unit);
        assertEquals(unit, dimension.getUnitType());
    }

    @Test
    public void testDamper() {
        List<Damper> dampers = AhuMetadata.findAll(Damper.class);
        assertNotNull(dampers);
        int count = AhuMetadata.count(Damper.class);
        assertEquals(count, dampers.size());

        String hw = "1621";
        Damper damper = AhuMetadata.findOne(Damper.class, hw);
        assertEquals(hw, damper.getHw());
    }

    @Test
    public void testPriceCalculatorSpec() {
        List<PriceCalculatorSpec> specs = AhuMetadata.findAll(PriceCalculatorSpec.class);
        assertNotNull(specs);
        int count = AhuMetadata.count(PriceCalculatorSpec.class);
        assertEquals(count, specs.size());
    }

    @Test
    public void testWeightCalculatorSpec() {
        List<WeightCalculatorSpec> specs = AhuMetadata.findAll(WeightCalculatorSpec.class);
        assertNotNull(specs);
        int count = AhuMetadata.count(WeightCalculatorSpec.class);
        assertEquals(count, specs.size());
    }

    @Test
    public void testAhuPropUnit() {
        List<AhuPropUnit> units = AhuMetadata.findAll(AhuPropUnit.class);
        assertNotNull(units);
        int count = AhuMetadata.count(AhuPropUnit.class);
        assertEquals(count, units.size());

        String key = "Pressure1";
        AhuPropUnit unit = AhuMetadata.findOne(AhuPropUnit.class, key);
        assertEquals(key, unit.getKey());
    }

    @Test
    public void testAhuPropSetting() {
        List<AhuPropSetting> settings = AhuMetadata.findAll(AhuPropSetting.class);
        assertNotNull(settings);
        int count = AhuMetadata.count(AhuPropSetting.class);
        assertEquals(count, settings.size());

        String key = "Pressure1";
        AhuPropSetting unit = AhuMetadata.findOne(AhuPropSetting.class, key);
        assertEquals(key, unit.getKey());
    }

    @Test
    public void testAccessData() {
        List<AccessData> ads = AhuMetadata.findAll(AccessData.class);
        assertNotNull(ads);
        int count = AhuMetadata.count(AccessData.class);
        assertEquals(count, ads.size());

        String sectionL = "1";
        List<AccessData> adList = AhuMetadata.findList(AccessData.class, sectionL);
        assertEquals(12, adList.size());
    }

    @Test
    public void testMixSize() {
        List<MixSize> mixSizes = AhuMetadata.findAll(MixSize.class);
        assertNotNull(mixSizes);
        int count = AhuMetadata.count(MixSize.class);
        assertEquals(count, mixSizes.size());

        String unit = "39CQ";
        String mixType = "1";
        List<MixSize> mixSizeList = AhuMetadata.findList(MixSize.class, unit, mixType);
        assertEquals(14, mixSizeList.size());
    }

    @Test
    public void testS4FArea() {
        List<S4FArea> areas = AhuMetadata.findAll(S4FArea.class);
        assertNotNull(areas);
        int count = AhuMetadata.count(S4FArea.class);
        assertEquals(count, areas.size());

        String fanType = "39CQ0913";
        S4FArea area = AhuMetadata.findOne(S4FArea.class, fanType);
        assertEquals(fanType, area.getFanType());
    }

    @Test
    public void testS4HHumiQ() {
        List<S4HHumiQ> humis = AhuMetadata.findAll(S4HHumiQ.class);
        assertNotNull(humis);
        int count = AhuMetadata.count(S4HHumiQ.class);
        assertEquals(count, humis.size());

        String supplier = "B";
        List<S4HHumiQ> humiList = AhuMetadata.findList(S4HHumiQ.class, supplier);
        assertEquals(20, humiList.size());
    }

    @Test
    public void testSISupplier() {
        List<SISupplier> sis = AhuMetadata.findAll(SISupplier.class);
        assertNotNull(sis);
        int count = AhuMetadata.count(SISupplier.class);
        assertEquals(count, sis.size());

        String supplier = "B";
        List<SISupplier> siList = AhuMetadata.findList(SISupplier.class, supplier);
        assertEquals(48, siList.size());
    }

    @Test
    public void testSJSupplier() {
        List<SJSupplier> sis = AhuMetadata.findAll(SJSupplier.class);
        assertNotNull(sis);
        int count = AhuMetadata.count(SJSupplier.class);
        assertEquals(count, sis.size());

        String supplier = "B";
        List<SJSupplier> siList = AhuMetadata.findList(SJSupplier.class, supplier);
        assertEquals(24, siList.size());
    }

    @Test
    public void testS4GRow() {
        List<S4GRow> rows = AhuMetadata.findAll(S4GRow.class);
        assertNotNull(rows);
        int count = AhuMetadata.count(S4GRow.class);
        assertEquals(count, rows.size());

        String fanType = "39CQ1317";
        S4GRow row = AhuMetadata.findOne(S4GRow.class, fanType);
        assertEquals(fanType, row.getFanType());
    }

    @Test
    public void testSUVCLight() {
        List<SUVCLight> rows = AhuMetadata.findAll(SUVCLight.class);
        assertNotNull(rows);
        int count = AhuMetadata.count(SUVCLight.class);
        assertEquals(count, rows.size());

        String fanType = "39CQ1317";
        SUVCLight row = AhuMetadata.findOne(SUVCLight.class, fanType);
        assertEquals(fanType, row.getFanType());
    }

    @Test
    public void testSBreakWaterResi() {
        List<SBreakWaterResi> resis = AhuMetadata.findAll(SBreakWaterResi.class);
        assertNotNull(resis);
        int count = AhuMetadata.count(SBreakWaterResi.class);
        assertEquals(count, resis.size());
    }

    @Test
    public void testSBreakWaterResiOrdering() {
        SBreakWaterResi resi = AhuMetadata.findMinOne(SBreakWaterResi.class, SBreakWaterResi::getVelocity);
        assertNotNull(resi);

        resi = AhuMetadata.findMaxOne(SBreakWaterResi.class, SBreakWaterResi::getVelocity);
        assertNotNull(resi);
    }

    @Test
    public void testSBreakWaterResiBig() {
        List<SBreakWaterResiBig> resis = AhuMetadata.findAll(SBreakWaterResiBig.class);
        assertNotNull(resis);
        int count = AhuMetadata.count(SBreakWaterResiBig.class);
        assertEquals(count, resis.size());
    }

    @Test
    public void testSClearSoundResi() {
        List<SClearSoundResi> resis = AhuMetadata.findAll(SClearSoundResi.class);
        assertNotNull(resis);
        int count = AhuMetadata.count(SClearSoundResi.class);
        assertEquals(count, resis.size());
    }

    @Test
    public void testSElectricityResi() {
        List<SElectricityResi> resis = AhuMetadata.findAll(SElectricityResi.class);
        assertNotNull(resis);
        int count = AhuMetadata.count(SElectricityResi.class);
        assertEquals(count, resis.size());
    }

    @Test
    public void testSJunliuqiResi() {
        List<SJunliuqiResi> resis = AhuMetadata.findAll(SJunliuqiResi.class);
        assertNotNull(resis);
        int count = AhuMetadata.count(SJunliuqiResi.class);
        assertEquals(count, resis.size());
    }

    @Test
    public void testSTakeoverSize() {
        List<STakeoverSize> list = AhuMetadata.findAll(STakeoverSize.class);
        assertNotNull(list);
        int count = AhuMetadata.count(STakeoverSize.class);
        assertEquals(count, list.size());

        String fanType = "39CQ1317";
        String section = "E";
        String flange = "0";
        STakeoverSize row = AhuMetadata.findOne(STakeoverSize.class, fanType, section, flange);
        assertEquals(fanType, row.getFanType());
        assertEquals(section, row.getSection());
        assertEquals(flange, String.valueOf(row.getFlange()));
    }

    @Test
    public void testSForFrance() {
        List<SForFrance> list = AhuMetadata.findAll(SForFrance.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SForFrance.class);
        assertEquals(count, list.size());

        String item = "W1";
        SForFrance row = AhuMetadata.findOne(SForFrance.class, item);
        assertEquals(item, row.getItem());
    }

    @Test
    public void testSKAbsorber() {
        List<SKAbsorber> list = AhuMetadata.findAll(SKAbsorber.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKAbsorber.class);
        assertEquals(count, list.size());

        String fan = "SYW900R";
        String machineSiteNo = "Y200L2";
        SKAbsorber row = AhuMetadata.findOne(SKAbsorber.class, fan, machineSiteNo);
        assertEquals(fan, row.getFan());
        assertEquals(machineSiteNo, row.getMachineSiteNo());
    }

    @Test
    public void testPartWPlate() {
        List<PartWPlate> list = AhuMetadata.findAll(PartWPlate.class);
        assertNotNull(list);
        int count = AhuMetadata.count(PartWPlate.class);
        assertEquals(count, list.size());

        String product = "39CQ1418";
        PartWPlate row = AhuMetadata.findOne(PartWPlate.class, product);
        assertEquals(product, row.getProduct());
    }

    @Test
    public void testPartWWheel() {
        List<PartWWheel> list = AhuMetadata.findAll(PartWWheel.class);
        assertNotNull(list);
        int count = AhuMetadata.count(PartWWheel.class);
        assertEquals(count, list.size());

        String product = "1317";
        String efficiencyType = "1";
        PartWWheel row = AhuMetadata.findOne(PartWWheel.class, product, efficiencyType);
        assertEquals(product, row.getProduct());
        assertEquals(efficiencyType, String.valueOf(row.getEfficiencyType()));
    }

    @Test
    public void testRotorpara() {
        List<Rotorpara> list = AhuMetadata.findAll(Rotorpara.class);
        assertNotNull(list);
        int count = AhuMetadata.count(Rotorpara.class);
        assertEquals(count, list.size());
    }

    @Test
    public void testBdd() {
        List<Bdd> list = AhuMetadata.findAll(Bdd.class);
        assertNotNull(list);
        int count = AhuMetadata.count(Bdd.class);
        assertEquals(count, list.size());

        String pn = "YFT-R9-7";
        Bdd row = AhuMetadata.findOne(Bdd.class, pn);
        assertEquals(pn, row.getPn());
    }

    @Test
    public void testBds() {
        List<Bds> list = AhuMetadata.findAll(Bds.class);
        assertNotNull(list);
        int count = AhuMetadata.count(Bds.class);
        assertEquals(count, list.size());

        String pn = "YFT-R9-7";
        Bds row = AhuMetadata.findOne(Bds.class, pn);
        assertEquals(pn, row.getPn());
    }

    @Test
    public void testNoise() {
        List<Noise> list = AhuMetadata.findAll(Noise.class);
        assertNotNull(list);
        int count = AhuMetadata.count(Noise.class);
        assertEquals(count, list.size());

        String fanType = "YFB(2R)180";
        Noise row = AhuMetadata.findOne(Noise.class, fanType);
        assertEquals(fanType, row.getFanType());
    }

    @Test
    public void testSKFanMotor() {
        List<SKFanMotor> list = AhuMetadata.findAll(SKFanMotor.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKFanMotor.class);
        assertEquals(count, list.size());

        String power = "0.75";
        String level = "6";
        SKFanMotor row = AhuMetadata.findOne(SKFanMotor.class, power, level);
        assertEquals(power, String.valueOf(row.getPower()));
        assertEquals(level, String.valueOf(row.getLevel()));
    }

    @Test
    public void testSKFanMotorEff() {
        List<SKFanMotorEff> list = AhuMetadata.findAll(SKFanMotorEff.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKFanMotorEff.class);
        assertEquals(count, list.size());

        String power = "0.75";
        String level = "6";
        SKFanMotorEff row = AhuMetadata.findOne(SKFanMotorEff.class, power, level);
        assertEquals(power, String.valueOf(row.getPower()));
        assertEquals(level, String.valueOf(row.getLevel()));
    }

    @Test
    public void testSKFanType() {
        List<SKFanType> list = AhuMetadata.findAll(SKFanType.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKFanType.class);
        assertEquals(count, list.size());

        String type = "39CQ0711";
        String supplier = "B";
        SKFanType row = AhuMetadata.findOne(SKFanType.class, type, supplier);
        assertEquals(type, row.getType());
        assertEquals(supplier, row.getSupplier());

        String bend = "1";
        row = AhuMetadata.findOne(SKFanType.class, type, supplier, null, bend);
        assertEquals(type, row.getType());
        assertEquals(supplier, row.getSupplier());
        assertEquals(bend, String.valueOf(row.getBend()));
    }

    @Test
    public void testSKFanType1() {
        List<SKFanType1> list = AhuMetadata.findAll(SKFanType1.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKFanType1.class);
        assertEquals(count, list.size());

        String type = "39G0711";
        String supplier = "B";
        String fanQuery = "KTQ2.0";
        String bend = "1";
        SKFanType1 row = AhuMetadata.findOne(SKFanType1.class, type, supplier, fanQuery);
        assertEquals(type, row.getType());
        assertEquals(supplier, row.getSupplier());
        assertEquals(fanQuery, row.getFanQuery());

        row = AhuMetadata.findOne(SKFanType1.class, type, supplier, fanQuery, bend);
        assertEquals(type, row.getType());
        assertEquals(supplier, row.getSupplier());
        assertEquals(fanQuery, row.getFanQuery());
        assertEquals(bend, String.valueOf(row.getBend()));
    }

    @Test
    public void testSKPlugFanType() {
        List<SKPlugFanType> list = AhuMetadata.findAll(SKPlugFanType.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SKPlugFanType.class);
        assertEquals(count, list.size());

        String type = "0711";
        String supplier = "A";
        SKPlugFanType row = AhuMetadata.findOne(SKPlugFanType.class, type, supplier);
        assertEquals(type, row.getType());
        assertEquals(supplier, row.getSupplier());
    }

    @Test
    public void testSTwoSpeedMotor() {
        List<STwoSpeedMotor> list = AhuMetadata.findAll(STwoSpeedMotor.class);
        assertNotNull(list);
        int count = AhuMetadata.count(STwoSpeedMotor.class);
        assertEquals(count, list.size());

        // TODO wrong config???
        // String js = "6";
        // String gl = "1.2";
        // String supplier = "A";
        // STwoSpeedMotor row = AhuMetadata.findOne(STwoSpeedMotor.class, js, gl, supplier);
        // assertEquals(js, String.valueOf(row.getJs()));
        // assertEquals(gl, String.valueOf(row.getGl()));
        // assertEquals(supplier, row.getSupplier());
    }

    @Test
    public void testSCalSeq() {
        List<SCalSeq> list = AhuMetadata.findAll(SCalSeq.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCalSeq.class);
        assertEquals(count, list.size());
    }

    @Test
    public void testSCoilCal() {
        List<SCoilCal> list = AhuMetadata.findAll(SCoilCal.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCoilCal.class);
        assertEquals(count, list.size());

        String finType = "1";
        SCoilCal row = AhuMetadata.findOne(SCoilCal.class, finType);
        assertEquals(finType, String.valueOf(row.getFinType()));
    }

    @Test
    public void testSCoilCal1() {
        List<SCoilCal1> list = AhuMetadata.findAll(SCoilCal1.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCoilCal1.class);
        assertEquals(count, list.size());

        String finType = "1";
        SCoilCal1 row = AhuMetadata.findOne(SCoilCal1.class, finType);
        assertEquals(finType, String.valueOf(row.getFinType()));
    }

    @Test
    public void testSCoilCalEuro() {
        List<SCoilCalEuro> list = AhuMetadata.findAll(SCoilCalEuro.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCoilCalEuro.class);
        assertEquals(count, list.size());

        String finType = "1";
        SCoilCalEuro row = AhuMetadata.findOne(SCoilCalEuro.class, finType);
        assertEquals(finType, String.valueOf(row.getFinType()));
    }

    @Test
    public void testSCoilInfo() {
        List<SCoilInfo> list = AhuMetadata.findAll(SCoilInfo.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCoilInfo.class);
        assertEquals(count, list.size());

        String unitModel = "39CQ1015";
        String row1 = "1";
        String circuit = "HF";
        SCoilInfo row = AhuMetadata.findOne(SCoilInfo.class, unitModel, row1, circuit);
        assertEquals(unitModel, row.getUnitModel());
        assertEquals(row1, String.valueOf(row.getRow()));
        assertEquals(circuit, row.getCircuit());
    }

    @Test
    public void testSCoilInfo1() {
        List<SCoilInfo1> list = AhuMetadata.findAll(SCoilInfo1.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SCoilInfo1.class);
        assertEquals(count, list.size());

        String unitModel = "39CQ1015";
        String row1 = "1";
        String circuit = "HF";
        SCoilInfo1 row = AhuMetadata.findOne(SCoilInfo1.class, unitModel, row1, circuit);
        assertEquals(unitModel, row.getUnitModel());
        assertEquals(row1, String.valueOf(row.getRow()));
        assertEquals(circuit, row.getCircuit());
    }

    @Test
    public void testSDPAirFactorHDia1() {
        List<SDPAirFactorHDia1> list = AhuMetadata.findAll(SDPAirFactorHDia1.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SDPAirFactorHDia1.class);
        assertEquals(count, list.size());

        String unitType = "39G1418";
        SDPAirFactorHDia1 row = AhuMetadata.findOne(SDPAirFactorHDia1.class, unitType);
        assertEquals(unitType, row.getUnitType());
    }

    @Test
    public void testSDPAirFactorHDia2() {
        List<SDPAirFactorHDia2> list = AhuMetadata.findAll(SDPAirFactorHDia2.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SDPAirFactorHDia2.class);
        assertEquals(count, list.size());

        String unitType = "39G1418";
        SDPAirFactorHDia2 row = AhuMetadata.findOne(SDPAirFactorHDia2.class, unitType);
        assertEquals(unitType, row.getUnitType());
    }

    @Test
    public void testSSurfaceGeometrics() {
        List<SSurfaceGeometrics> list = AhuMetadata.findAll(SSurfaceGeometrics.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SSurfaceGeometrics.class);
        assertEquals(count, list.size());

        String id = "6";
        SSurfaceGeometrics row = AhuMetadata.findOne(SSurfaceGeometrics.class, id);
        assertEquals(id, String.valueOf(row.getId()));
    }

    @Test
    public void testSWaterDropFactor() {
        List<SWaterDropFactor> list = AhuMetadata.findAll(SWaterDropFactor.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SWaterDropFactor.class);
        assertEquals(count, list.size());

        String unitType = "39CQ1418";
        SWaterDropFactor row = AhuMetadata.findOne(SWaterDropFactor.class, unitType);
        assertEquals(unitType, row.getUnitType());
    }

    @Test
    public void testSWaterDropFactorAhri() {
        List<SWaterDropFactorAhri> list = AhuMetadata.findAll(SWaterDropFactorAhri.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SWaterDropFactorAhri.class);
        assertEquals(count, list.size());

        String unitType = "39CQ1418";
        SWaterDropFactorAhri row = AhuMetadata.findOne(SWaterDropFactorAhri.class, unitType);
        assertEquals(unitType, row.getUnitType());
    }

    @Test
    public void testSZua() {
        List<SZua> list = AhuMetadata.findAll(SZua.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SZua.class);
        assertEquals(count, list.size());

        String r = "1";
        SZua row = AhuMetadata.findOne(SZua.class, r);
        assertEquals(r, String.valueOf(row.getRow()));
    }

    @Test
    public void testTvColdCoilGridHeader() {
        List<TvColdCoilGridHeader> list = AhuMetadata.findAll(TvColdCoilGridHeader.class);
        assertNotNull(list);
        int count = AhuMetadata.count(TvColdCoilGridHeader.class);
        assertEquals(count, list.size());

        String interfaceName = "COLDCOILVIEW";
        TvColdCoilGridHeader row = AhuMetadata.findOne(TvColdCoilGridHeader.class, interfaceName);
        assertEquals(interfaceName, row.getInterfaceName());
    }

    @Test
    public void testTvColdCoilGridHeaderE() {
        List<TvColdCoilGridHeaderE> list = AhuMetadata.findAll(TvColdCoilGridHeaderE.class);
        assertNotNull(list);
        int count = AhuMetadata.count(TvColdCoilGridHeaderE.class);
        assertEquals(count, list.size());

        String interfaceName = "COLDCOILVIEW";
        TvColdCoilGridHeaderE row = AhuMetadata.findOne(TvColdCoilGridHeaderE.class, interfaceName);
        assertEquals(interfaceName, row.getInterfaceName());
    }

    @Test
    public void testTvHeatCoilGridHeader() {
        List<TvHeatCoilGridHeader> list = AhuMetadata.findAll(TvHeatCoilGridHeader.class);
        assertNotNull(list);
        int count = AhuMetadata.count(TvHeatCoilGridHeader.class);
        assertEquals(count, list.size());

        String interfaceName = "HEATCOILVIEW";
        TvHeatCoilGridHeader row = AhuMetadata.findOne(TvHeatCoilGridHeader.class, interfaceName);
        assertEquals(interfaceName, row.getInterfaceName());
    }

    @Test
    public void testTvHeatCoilGridHeaderE() {
        List<TvHeatCoilGridHeaderE> list = AhuMetadata.findAll(TvHeatCoilGridHeaderE.class);
        assertNotNull(list);
        int count = AhuMetadata.count(TvHeatCoilGridHeaderE.class);
        assertEquals(count, list.size());

        String interfaceName = "HEATCOILVIEW";
        TvHeatCoilGridHeaderE row = AhuMetadata.findOne(TvHeatCoilGridHeaderE.class, interfaceName);
        assertEquals(interfaceName, row.getInterfaceName());
    }

    @Test
    public void testSPanelDBigOperate() {
        List<SPanelDBigOperate> list = AhuMetadata.findAll(SPanelDBigOperate.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SPanelDBigOperate.class);
        assertEquals(count, list.size());

        String serial = "39G2125";
        String sectionTypeNo = "1";
        String w = "11";
        SPanelDBigOperate row = AhuMetadata.findOne(SPanelDBigOperate.class, serial, sectionTypeNo, w);
        assertEquals(serial, row.getSerial());
        assertEquals(sectionTypeNo, row.getSectionTypeNo());
        assertEquals(w, row.getW());
    }

    @Test
    public void testSPanelDNormalSide() {
        List<SPanelDNormalSide> list = AhuMetadata.findAll(SPanelDNormalSide.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SPanelDNormalSide.class);
        assertEquals(count, list.size());

        String serial = "39G2125";
        String sectionTypeNo = "0";
        String w = "25";
        SPanelDNormalSide row = AhuMetadata.findOne(SPanelDNormalSide.class, serial, sectionTypeNo, w);
        assertEquals(serial, row.getSerial());
        assertEquals(sectionTypeNo, row.getSectionTypeNo());
        assertEquals(w, row.getW());
    }

    @Test
    public void testSPanelDBottomCQ() {
        List<SPanelDBottomCQ> list = AhuMetadata.findAll(SPanelDBottomCQ.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SPanelDBottomCQ.class);
        assertEquals(count, list.size());

        String serial = "39G2125";
        String sectionTypeNo = "1";
        String w = "11";
        SPanelDBottomCQ row = AhuMetadata.findOne(SPanelDBottomCQ.class, serial, sectionTypeNo, w);
        assertEquals(serial, row.getSerial());
        assertEquals(sectionTypeNo, row.getSectionTypeNo());
        assertEquals(w, row.getW());
    }

    @Ignore
    @Test
    public void testPriceCode() {
        List<PriceCode> list = AhuMetadata.findAll(PriceCode.class);
        assertNotNull(list);
        int count = AhuMetadata.count(PriceCode.class);
        assertEquals(count, list.size());

        String sheetName = "ahu.ctr";
        List<PriceCode> rows = AhuMetadata.findList(PriceCode.class, sheetName);
        assertNotNull(rows);
    }

    @Test
    public void testSectionParameter() {
        List<SectionParameter> list = AhuMetadata.findAll(SectionParameter.class);
        assertNotNull(list);
        int count = AhuMetadata.count(SectionParameter.class);
        assertEquals(count, list.size());

        String sheetName = "ahu";
        List<SectionParameter> rows = AhuMetadata.findList(SectionParameter.class, sheetName);
        assertNotNull(rows);

        sheetName = "meta.nonStandard";
        rows = AhuMetadata.findList(SectionParameter.class, sheetName);
        assertNotNull(rows);
    }

    @Test
    public void testNonstdMotor() {
        List<NonstdMotor> list = AhuMetadata.findAll(NonstdMotor.class);
        assertNotNull(list);
        int count = AhuMetadata.count(NonstdMotor.class);
        assertEquals(count, list.size());
    }

    @Test
    public void testCsPriceRate() {
        List<CsPriceRate> list = AhuMetadata.findAll(CsPriceRate.class);
        assertNotNull(list);
        int count = AhuMetadata.count(CsPriceRate.class);
        assertEquals(count, list.size());
        
        CsPriceRate rate = AhuMetadata.findOne(CsPriceRate.class, "10#");
        assertNotNull(rate);
    }

}
