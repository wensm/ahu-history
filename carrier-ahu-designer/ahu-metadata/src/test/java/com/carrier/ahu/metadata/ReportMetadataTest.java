package com.carrier.ahu.metadata;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.carrier.ahu.metadata.common.MetadataException;
import com.carrier.ahu.metadata.common.ReportType;

/**
 * Created by Braden Zhou on 2018/05/10.
 */
public class ReportMetadataTest {

    @Test
    public void testReportMetadata() {
        for (ReportType reportType : ReportType.values()) {
            assertNotNull(ReportMetadata.getReportMetadata(reportType));
        }
    }

    @Test
    public void testTechSpecSectionConnection() {
        String unitModel = "39CQ1420";
        String unitSeries = unitModel.substring(0, unitModel.length() - 4);
        String[][] sectionConnection = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel, null);
        assertNotNull(sectionConnection);
    }

    @Test(expected = MetadataException.class)
    public void testTechSpecSectionConnectionWithException() {
        String unitModel = "39XX0000";
        String unitSeries = unitModel.substring(0, unitModel.length() - 4);
        String[][] sectionConnection = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel, null);
        assertNotNull(sectionConnection);
    }

}
