#规则介绍：

1. '>' 条件变量(relatedKeys)大于指定值，满足
      
2. '<' 条件变量(relatedKeys)小于指定值，满足

3. '!' 条件变量(relatedKeys)不等于指定值，满足

4. 'startw:' 条件变量(relatedKeys)以指定值开始，满足

5. 'endw:' 条件变量(relatedKeys)以指定值结尾，满足

6. 'contain:'  条件变量(relatedKeys)包含指定值，满足

4. 无比较符号：条件变量(relatedKeys)等于指定值，满足




#联动下拉框可选择项
######说明
* 条件满足显示对应条件的结果集，索引从0开始
######配置原则
1. relatedKeys 指定多个，valueOptionMap 用#分割例如：
```
      "relatedKeys": [
      	"meta.section.mix.uvLamp",
      	"assis.section.ahu.height"
      ],
      "valueOptionMap": {
      	"true#true": [
          0
        ],
        "!true#!true": [
          1
        ]
      }
```   
#联动【复选，输入框，下拉框】是否禁用
######说明
* json条件满足  : 【页面是否可编辑为：非(ahu.section.parameters1.3.xls的editable)】
* json条件不满足：【页面是否可编辑为：  (ahu.section.parameters1.3.xls的editable)】
 ######配置原则
 1. 优先在ahu.section.parameters1.3.xls配置editable，json 里面的规则为非editable
 2. enableRelatedKeys 指定多个，enableValueMap 用#分割例如：
```      
      "enableRelatedKeys": [
        "meta.section.fan.outlet",
        "meta.section.airDirection",
        "assis.section.position"
      ],
      "enableValueMap": {
        "!wwk#R#1": true
      }
```
# 多重条件组合联动(包含：pre上个段、next下个段)
######说明
relationKeys 、relationMap按照位置一一对应，有且只有一个条件满足。
######配置原则
1. relatedKeys 和 relationMap 里面的值按照位置一一对应
```
"relationKeys": [
        "pre#filter",
        "pre#combinedFilter",
      ],
      "relationMap": [
            {"true": [
              1
            ]},
            {"true": [
              1
            ]}
        ]
```
#多重条件组合联动(包含：pre上个段、next下个段)
######说明
* relationKeys 、relationMap按照位置一一对应，有且只有一个条件满足。
* 上个段风机形式为WWK，下个段为单层过滤或综合过滤或高效过滤段
######配置原则
1. relatedKeys 和 relationMap 里面的值按照位置一一对应
```
"relationKeys": [
        "pre#meta.section.fan.outlet;next#filter",
        "pre#meta.section.fan.outlet;next#combinedFilter",
        "pre#meta.section.fan.outlet;next#HEPAFilter"
      ],
      "relationMap": [
            {"wwk#true": [
              1
            ]},
            {"wwk#true": [
              1
            ]},
            {"wwk#true": [
              1
            ]}
        ]
      
```