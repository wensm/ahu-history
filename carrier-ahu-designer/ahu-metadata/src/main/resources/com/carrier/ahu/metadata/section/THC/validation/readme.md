#规则说明
配置JSON 实现页面元素值范围校验
* level： 提醒的方式 warning【文字在元素下方方式提醒】、error【文字弹出框方式提醒】
* ruleMassage： 提醒的文字类型 default【系统默认格式】、i18n 【通过rule_message.jsonKey做多语言提醒】
##规则1:range
###说明
范围符号 ~ 
1. 0到100(包含) ：0~100
2. 大于等于0 ：0~
3. 小于等于100  ：~100
####例如：
````
"meta.section.mix.NARatio": [
    {
      "rule": "range",
      "level":"warning",
      "ruleMassage": "default",
      "ruleValueSet": [
        "0~100"
      ]
    }
  ]
````
##规则2:equals
相等
####例如：
````
"meta.section.fan.supplier": [
    {
      "rule": "equals",
      "level":"warning",
      "ruleMassage": "default",
      "ruleValueSet": [
        "Dongyuan",
        "ABB",
        "Simens"
      ]
    }
  ]
````
##规则3:noEquals
不等于
####例如：
````
  "meta.ahu.baseType": [
    {
      "rule": "noEquals",
      "level":"warning",
      "ruleMassage": "i18n",
      "ruleValueSet": [
        "powderCoatedCR"
      ]
    }
  ]
````