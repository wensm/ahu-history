package com.carrier.ahu.metadata.entity.mix;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MixSize extends DataEntity {

    @EntityId
    private String unit;
    private int sectionL;
    private int b1;
    private int c1;
    private double d1;
    private double e1;
    private int a;
    private double b;
    @EntityId(sequence = 1)
    private String mixType;

}
