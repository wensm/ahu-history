package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/8/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Bdd extends DataEntity {

    @EntityId
    private String pn;
    private double n1;
    private double n2;
    private int n3;
    private double n4;
    private double n5;
    private double n6;
    private double n7;
    private double n8;
    private double n9;
    private double n10;
    private double n11;
    private double n12;
    private double n13;
    private double n14;
    private double n15;
    private double n16;
    private double n17;
    private int n18;
    private int n19;
    private double n20;
    private double n21;
    private int n22;
    private double n23;
    private double n24;
    private double n25;
    private double n26;
    private double n27;
    private double n28;
    private double n29;
    private double n30;
    private double n31;
    private double n32;
    private double n33;
    private double n34;
    private double n35;
    private double n36;
    private double n37;
    private double n38;
    private double n39;
    private double n40;
    private double n41;
    private double n42;

}
