package com.carrier.ahu.metadata.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Ahu size data.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AhuSize extends Ahu {

    private double area;
    private double damperWidth;
    private double damperHeight;
    private double height;
    private double width;

}
