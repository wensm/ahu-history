package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DamperDimension extends DataEntity {

    @EntityId
    private String unit;
    @EntityId(sequence = 1)
    private String mixType;
    @EntityId(sequence = 2)
    private String sectionL;
    private String b1;
    private String c1;
    private String d1;
    private String e1;
    private String a;
    private String b;

}
