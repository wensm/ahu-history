package com.carrier.ahu.metadata;

import static com.carrier.ahu.datahouse.DatahouseFactory.csv;
import static com.carrier.ahu.datahouse.DatahouseFactory.excel;
import static com.carrier.ahu.metadata.common.FileConstants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.datahouse.Datahouse;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.metadata.entity.AhuPropSetting;
import com.carrier.ahu.metadata.entity.AhuPropUnit;
import com.carrier.ahu.metadata.entity.AhuSize;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.metadata.entity.access.AccessData;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionL;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionR;
import com.carrier.ahu.metadata.entity.cad.CoilBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.CoilDimension;
import com.carrier.ahu.metadata.entity.cad.ColdCoilDimension;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionFac;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionL;
import com.carrier.ahu.metadata.entity.cad.ECoilDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimensionBMotor;
import com.carrier.ahu.metadata.entity.cad.FilterDrawing;
import com.carrier.ahu.metadata.entity.cad.HCoilBigDimension;
import com.carrier.ahu.metadata.entity.cad.HCoilBigDimensionNew;
import com.carrier.ahu.metadata.entity.cad.HCoilBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.HeatCoilDimension;
import com.carrier.ahu.metadata.entity.cad.PaneledDoor;
import com.carrier.ahu.metadata.entity.cad.SteamBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.SteamCoilDimension;
import com.carrier.ahu.metadata.entity.calc.PriceCalculatorSpec;
import com.carrier.ahu.metadata.entity.calc.SUVCLight;
import com.carrier.ahu.metadata.entity.calc.WeightCalculatorSpec;
import com.carrier.ahu.metadata.entity.coil.CoilDllCalculation;
import com.carrier.ahu.metadata.entity.coil.CoilSize;
import com.carrier.ahu.metadata.entity.coil.S4FArea;
import com.carrier.ahu.metadata.entity.coil.S4FTHPress;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo1;
import com.carrier.ahu.metadata.entity.coil.SWaterDensity;
import com.carrier.ahu.metadata.entity.coil.UnitTypeRow;
import com.carrier.ahu.metadata.entity.fan.*;
import com.carrier.ahu.metadata.entity.filter.*;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWPlate;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.metadata.entity.heatrecycle.Rotorpara;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SISupplier;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.metadata.entity.mix.MixSize;
import com.carrier.ahu.metadata.entity.price.PriceCode;
import com.carrier.ahu.metadata.entity.report.AdjustAirDoor;
import com.carrier.ahu.metadata.entity.report.CoilInfo;
import com.carrier.ahu.metadata.entity.report.DeliveryInfo;
import com.carrier.ahu.metadata.entity.report.SForFrance;
import com.carrier.ahu.metadata.entity.report.SKAbsorber;
import com.carrier.ahu.metadata.entity.report.SKAbsorberBig;
import com.carrier.ahu.metadata.entity.report.STakeoverSize;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResi;
import com.carrier.ahu.metadata.entity.resistance.SBreakWaterResiBig;
import com.carrier.ahu.metadata.entity.resistance.SClearSoundResi;
import com.carrier.ahu.metadata.entity.resistance.SElectricityResi;
import com.carrier.ahu.metadata.entity.resistance.SJunliuqiResi;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.metadata.entity.section.SectionParameter;
import com.carrier.ahu.metadata.entity.templet.ExportTemplet;
import com.carrier.ahu.metadata.entity.validation.S4GRow;
import com.google.common.base.CaseFormat;

/**
 * Common metadata access interface.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AhuMetadata {

    // csv元数据文件名与bean类名约定保持一致, 按照camel格式, 下划线分割. 可以通过下面方法验证文件名是否合规.
    // CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, class.getSimpleName)
    @Deprecated
    private static Map<Class, Datahouse> datahouses = new HashMap<>();
    private static Map<String, String> alFilterStandards = new HashMap<>();
    private static Map<String, String> hepaFilterStandards = new HashMap<>();

    static {
        alFilterStandards.put("G1", "1");
        alFilterStandards.put("G3", "3");
        alFilterStandards.put("G4", "4");
        alFilterStandards.put("M5", "5");
        alFilterStandards.put("M6", "6");
        alFilterStandards.put("F7", "7");
        alFilterStandards.put("F8", "8");
        alFilterStandards.put("F9", "9");
        alFilterStandards.put("F7G", "A");//高容尘量
        alFilterStandards.put("F8G", "B");//高容尘量
        alFilterStandards.put("F9G", "C");//高容尘量
        alFilterStandards.put("C3", "J");
        alFilterStandards.put("C2", "D");
        alFilterStandards.put("C1", "E");
        alFilterStandards.put("Z2", "F");
        alFilterStandards.put("Z1", "G");
        alFilterStandards.put("GZ", "H");
        alFilterStandards.put("YG", "I");

        //add new filter H10,H12,H14,C
        hepaFilterStandards.put("H10", "H10");
        hepaFilterStandards.put("H11", "H11");
        hepaFilterStandards.put("H12", "H12");
        hepaFilterStandards.put("H13", "H13");
        hepaFilterStandards.put("H14", "H14");
        hepaFilterStandards.put("A", "A");
        hepaFilterStandards.put("B", "B");
        hepaFilterStandards.put("C", "C");
    }

    static {
        datahouses.put(AhuSize.class, csv(AhuSize.class, AHU_SIZE_CSV));
        datahouses.put(AhuSizeDetail.class, csv(AhuSizeDetail.class, AHU_SIZE_DETAIL_CSV));
        datahouses.put(CoilSize.class, csv(CoilSize.class, COIL_SIZE_CSV));
        datahouses.put(CoilDllCalculation.class, csv(CoilDllCalculation.class, COIL_DLL_CALCULATION_CSV));
        datahouses.put(ElectrostaticFilterResistance.class,
                csv(ElectrostaticFilterResistance.class, ELECTROSTATIC_FILTER_RESISTANCE_CSV));
        datahouses.put(NWFSingleFilterResistance.class,
                csv(NWFSingleFilterResistance.class, NWF_SINGLE_FILTER_RESISTANCE_CSV));
        datahouses.put(ALSingleFilterResistance.class,
                csv(ALSingleFilterResistance.class, AL_SINGLE_FILTER_RESISTANCE_CSV));
        datahouses.put(HEPAFilterResistance.class, csv(HEPAFilterResistance.class, HEPA_FILTER_RESISTANCE_CSV));
        datahouses.put(FilterComposing.class, csv(FilterComposing.class, FILTER_COMPOSING_CSV));
        datahouses.put(S4yFilterFormat.class, csv(S4yFilterFormat.class, S_4Y_FILTER_FORMAT));
        datahouses.put(HEPAFilterComposing.class, csv(HEPAFilterComposing.class, HEPA_FILTER_COMPOSING_CSV));
        datahouses.put(PlugFanType.class, csv(PlugFanType.class, PLUG_FAN_TYPE_CSV));
        datahouses.put(FanMotor.class, csv(FanMotor.class, FAN_MOTOR_CSV));
        datahouses.put(SFan.class, csv(SFan.class, S_FAN_CSV));
        datahouses.put(SFanLength.class, csv(SFanLength.class, S_FAN_LENGTH_CSV));
        datahouses.put(SFanLength1.class, csv(SFanLength1.class, S_FAN_LENGTH1_CSV));
        datahouses.put(SFanLength2.class, csv(SFanLength2.class, S_FAN_LENGTH2_CSV));
        datahouses.put(SSectionKBIG.class, csv(SSectionKBIG.class, S_SECTION_K_BIG_CSV));
        datahouses.put(SectionArea.class, csv(SectionArea.class, SECTION_AREA_CSV));
        datahouses.put(SectionLength.class, csv(SectionLength.class, SECTION_LENGTH_CSV));
        datahouses.put(AdjustAirDoor.class, csv(AdjustAirDoor.class, ADJUST_AIR_DOOR_CSV));
        datahouses.put(CoilInfo.class, csv(CoilInfo.class, COIL_INFO_CSV));
        datahouses.put(SCoilInfo.class, csv(SCoilInfo.class, COIL_INFO_CSV));
        datahouses.put(SCoilInfo1.class, csv(SCoilInfo1.class, COIL_INFO_CSV1));
        datahouses.put(DeliveryInfo.class, csv(DeliveryInfo.class, DELIVERY_INFO_CSV));
        datahouses.put(CoilBigDimensionL.class, csv(CoilBigDimensionL.class, COIL_BIG_DIMENSION_L_CSV));
        datahouses.put(CoilBigDimensionR.class, csv(CoilBigDimensionR.class, COIL_BIG_DIMENSION_R_CSV));
        datahouses.put(CoilBigTuojiaDimension.class, csv(CoilBigTuojiaDimension.class, COIL_BIG_TUOJIA_DIMENSION_CSV));
        datahouses.put(CoilDimension.class, csv(CoilDimension.class, COIL_DIMENSION_CSV));
        datahouses.put(ColdCoilDimension.class, csv(ColdCoilDimension.class, COLD_COIL_DIMENSION_CSV));
        datahouses.put(DamperDimension.class, csv(DamperDimension.class, DAMPER_DIMENSION_CSV));
        datahouses.put(DamperDimensionFac.class, csv(DamperDimensionFac.class, DAMPER_DIMENSION_FAC_CSV));
        datahouses.put(DamperDimensionL.class, csv(DamperDimensionL.class, DAMPER_DIMENSION_L_CSV));
        datahouses.put(ECoilDimension.class, csv(ECoilDimension.class, E_COIL_DIMENSION_CSV));
        datahouses.put(FanDimension.class, csv(FanDimension.class, FAN_DIMENSION_CSV));
        datahouses.put(FanDimensionBMotor.class, csv(FanDimensionBMotor.class, FAN_DIMENSION_BMOTOR_CSV));
        datahouses.put(FilterDrawing.class, csv(FilterDrawing.class, FILTER_DRAWING_CSV));
        datahouses.put(HeatCoilDimension.class, csv(HeatCoilDimension.class, HEAT_COIL_DIMENSION_CSV));
        datahouses.put(PaneledDoor.class, csv(PaneledDoor.class, PANELED_DOOR_CSV));
        datahouses.put(SteamBigTuojiaDimension.class,
                csv(SteamBigTuojiaDimension.class, STEAM_BIG_TUOJIA_DIMENSION_CSV));
        datahouses.put(SteamCoilDimension.class, csv(SteamCoilDimension.class, STEAM_COIL_DIMENSION_CSV));
        datahouses.put(Damper.class, csv(Damper.class, DAMPER_CSV));
        datahouses.put(PriceCalculatorSpec.class, csv(PriceCalculatorSpec.class, PRICE_CALCULATOR_SPEC_CSV));
        datahouses.put(WeightCalculatorSpec.class, csv(WeightCalculatorSpec.class, WEIGHT_CALCULATOR_SPEC_CSV));
        datahouses.put(AccessData.class, csv(AccessData.class, ACCESS_DATA_CSV));
        datahouses.put(S4FArea.class, csv(S4FArea.class, S_4F_AREA_CSV));
        datahouses.put(S4FTHPress.class, csv(S4FTHPress.class, S_4FTHPRESS_CSV));
        datahouses.put(S4HHumiQ.class, csv(S4HHumiQ.class, S_4H_HUMI_Q_CSV));
        datahouses.put(SISupplier.class, csv(SISupplier.class, S_I_SUPPLIER_CSV));
        datahouses.put(SJSupplier.class, csv(SJSupplier.class, S_J_SUPPLIER_CSV));
        datahouses.put(S4GRow.class, csv(S4GRow.class, S_4G_ROW_CSV));
        datahouses.put(SUVCLight.class, csv(SUVCLight.class, S_UVC_LIGHT_CSV));
        datahouses.put(SBreakWaterResi.class, csv(SBreakWaterResi.class, S_BREAD_WATER_RESI_CSV));
        datahouses.put(SBreakWaterResiBig.class, csv(SBreakWaterResiBig.class, S_BREAD_WATER_RESI_BIG_CSV));
        datahouses.put(SClearSoundResi.class, csv(SClearSoundResi.class, S_CLEAR_SOUND_RESI_CSV));
        datahouses.put(SElectricityResi.class, csv(SElectricityResi.class, S_ELECTRICITY_RESI_CSV));
        datahouses.put(SJunliuqiResi.class, csv(SJunliuqiResi.class, S_JUNLIUQI_RESI_CSV));
        datahouses.put(SForFrance.class, csv(SForFrance.class, S_FOR_FRANCE_CSV));
        datahouses.put(SKAbsorber.class, csv(SKAbsorber.class, S_K_ABSORBER_CSV));
        datahouses.put(STakeoverSize.class, csv(STakeoverSize.class, S_TAKEOVER_SIZE_CSV));
        datahouses.put(SVMotorRange.class, csv(SVMotorRange.class, S_VMOTOR_RANGE_CSV));
        datahouses.put(UnitTypeRow.class, csv(UnitTypeRow.class, UNIT_TYPE_TD_ROWS_CSV));
        datahouses.put(ExportTemplet.class, csv(ExportTemplet.class, EXPORT_TEMPLET_CSV));
        datahouses.put(HCoilBigDimension.class, csv(HCoilBigDimension.class, S_HCOILBIGDIMENSION_CSV));
        datahouses.put(HCoilBigTuojiaDimension.class, csv(HCoilBigTuojiaDimension.class,S_HCOILBIGTUOJIADIMENSION_CSV));
        datahouses.put(HCoilBigDimensionNew.class, csv(HCoilBigDimensionNew.class,S_HCOILBIGDIMENSION_NEW_CSV));
        datahouses.put(SKAbsorberBig.class, csv(SKAbsorberBig.class,S_KABSORBERBIG_CSV));
        datahouses.put(SKFanVMotorEff.class, csv(SKFanVMotorEff.class,S_K_FAN_VMOTOR_EFF_CSV));
        datahouses.put(SWaterDensity.class, csv(SWaterDensity.class,S_WATER_DENSITY_CSV));
        datahouses.put(SFanMotorBaseDimension.class, csv(SFanMotorBaseDimension.class,S_FAN_MOTORBASEDIMENSION_CSV));
        datahouses.put(SKFanMotorDimension.class, csv(SKFanMotorDimension.class,S_K_FAN_MOTOR_DIMENSION_CSV));
        datahouses.put(ConverterMotorBig.class, csv(ConverterMotorBig.class,CONVERTER_MOTOR_BIG_CSV));
        datahouses.put(FrequencySmall.class, csv(FrequencySmall.class,FREQUENCY_SMALL_CSV));
        datahouses.put(STwoSpeedMotor.class, csv(STwoSpeedMotor.class,S_TWO_SPEED_MOTOR_CSV));

        datahouses.put(AhuPropUnit.class, excel(AhuPropUnit.class, AHU_PROP_UNIT_EXCEL));
        datahouses.put(AhuPropSetting.class, excel(AhuPropSetting.class, AHU_PROP_UNIT_EXCEL));
        datahouses.put(MixSize.class, excel(MixSize.class, MIX_SIZE_EXCEL));
        datahouses.put(PartWPlate.class, excel(PartWPlate.class, HEAT_RECYCLE_4W_EXCEL));
        datahouses.put(PartWWheel.class, excel(PartWWheel.class, HEAT_RECYCLE_4W_EXCEL));
        datahouses.put(Rotorpara.class, excel(Rotorpara.class, HEAT_RECYCLE_4W_EXCEL));

        datahouses.put(PriceCode.class, excel(PriceCode.class, PRICE_CODE_EXCEL, true));
        datahouses.put(SectionParameter.class, excel(SectionParameter.class, SECTION_PARAMETER_EXCEL, true));
        
    }

    public static <T extends DataEntity> List<T> findAll(Class<T> clazz) {
        List<T> list = getDatahouse(clazz).findAll();
        List<T> modifiableList = new ArrayList<T>();
        modifiableList.addAll(list);
        return modifiableList;
    }

    public static <T extends DataEntity> T findOne(Class<T> clazz, String id) {
        return (T) getDatahouse(clazz).findOne(id);
    }

    public static <T extends DataEntity> boolean exists(Class<T> clazz, String id) {
        return getDatahouse(clazz).exists(id);
    }

    public static <T extends DataEntity> int count(Class<T> clazz) {
        return getDatahouse(clazz).count();
    }

    public static <T extends DataEntity, K> T findOne(Class<T> clazz, String... searchs) {
        return (T) getDatahouse(clazz).findOne(searchs);
    }

    public static <T extends DataEntity, K> List<T> findList(Class<T> clazz, String... searchs) {
        return (List<T>) getDatahouse(clazz).findList(searchs);
    }

    private static <T extends DataEntity> Datahouse getDatahouse(Class<T> clazz) {
        Datahouse datahouse = null;
        if (datahouses.containsKey(clazz)) {
            datahouse = datahouses.get(clazz);
        } else {
            String fileName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, clazz.getSimpleName())
                    + CSV_EXTENSION;
            datahouse = csv(clazz, fileName);
            datahouses.put(clazz, datahouse);
        }
        if (datahouse == null) {
            throw new AhuException("Failed to load metadata of " + clazz.getName());
        }
        return datahouse;
    }

    public static String getALStandardCode(String standard, String efficiency) {
        if (alFilterStandards.containsKey(efficiency)) {
            return alFilterStandards.get(efficiency);
        } else {
            return null;
        }
    }

    public static String getHEPAStandardCode(String standard, String efficiency) {
        if (hepaFilterStandards.containsKey(efficiency)) {
            return hepaFilterStandards.get(efficiency);
        } else {
            return null;
        }
    }

    public static <T extends DataEntity, U, C extends Comparable<? super C>> T findMinOne(Class<T> clazz,
            Function<? super T, ? extends C> keyExtractor) {
        List<T> list = findAll(clazz);
        Collections.sort(list, Comparator.comparing(keyExtractor));
        return list.isEmpty() ? null : list.get(0);
    }

    public static <T extends DataEntity, U, C extends Comparable<? super C>> T findMaxOne(Class<T> clazz,
            Function<? super T, ? extends C> keyExtractor) {
        List<T> list = findAll(clazz);
        Collections.sort(list, Comparator.comparing(keyExtractor).reversed());
        return list.isEmpty() ? null : list.get(0);
    }

    public static String startsWith(String prefix) {
        return Datahouse.startsWith(prefix);
    }

}
