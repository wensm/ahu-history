package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanMotorDimension extends DataEntity {
    @EntityId(sequence = 1)
    private double power;
    @EntityId(sequence = 2)
    private int level;
    private String machineSiteNo;
    private double a;
    private double b;
    private double c;
    private double s;
    @EntityId(sequence = 3)
    private String motorType;
}
