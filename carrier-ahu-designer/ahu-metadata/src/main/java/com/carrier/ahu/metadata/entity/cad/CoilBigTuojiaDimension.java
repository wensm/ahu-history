package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoilBigTuojiaDimension extends DataEntity {

    @EntityId
    private String unit;
    private String a;
    private String b;
    @AllowNull
    private String c;
    private String d;
    private String e;
    private String f;
    @AllowNull
    private String g;
    private String w;

}
