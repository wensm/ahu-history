package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HeatCoilDimension extends DataEntity {

    @EntityId
    @AllowNull
    private String unitType;
    @AllowNull
    private String a;
    @AllowNull
    private String b;
    @AllowNull
    private String c;
    @AllowNull
    private String g;
    @AllowNull
    private String d;
    @AllowNull
    private String e;
    @AllowNull
    private String coilh;
    @AllowNull
    private String coilw;
    @AllowNull
    private String dia;

}
