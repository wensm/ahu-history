package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SCalSeq extends DataEntity {

    private int seq;
    private int row;
    private String circuit;
    private int fpi;
    @AllowNull
    private int bigAhu;

}
