package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 风机 查询条件是否为 （ oldunit（老型号> KTQ:前弯 KTH：后弯） + supplier (新供应商) + partno(新编号)
 * ）的唯一的一条记录
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdFan extends DataEntity implements NonstdPrice {

    @EntityId
    private String oldUnit;// 老型号 (KTQ:前弯 KTH：后弯) 例如：KTQ180
    @EntityId(sequence = 1)
    private String supplier;// 新供应商
    @EntityId(sequence = 2)
    private String partNo;// 新编号
    private String fan;
    private String base;
    private double tp;// 新价格 (fan+base)
    private double oldPrice;// 老价格

}
