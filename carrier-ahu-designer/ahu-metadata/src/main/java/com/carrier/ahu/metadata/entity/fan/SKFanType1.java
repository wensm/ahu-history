package com.carrier.ahu.metadata.entity.fan;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanType1 extends SKFanType {

}
