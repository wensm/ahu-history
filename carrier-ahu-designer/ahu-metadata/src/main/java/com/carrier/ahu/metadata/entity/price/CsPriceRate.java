package com.carrier.ahu.metadata.entity.price;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 槽钢底座价格系数
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class CsPriceRate extends LpRate {

}
