package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/11/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Rotorpara extends DataEntity {

    private int rotorDia;
    private int height;
    private int thick;
    private int weight;
    private int minVolume;
    private int maxVolume;
    private double power;
    private double powerEn;
    private double powerDri;
    private double powerHeatX;

}
