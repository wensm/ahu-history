package com.carrier.ahu.metadata;

import static com.carrier.ahu.metadata.common.MetadataConstants.METADATA_PACKAGE;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.po.meta.RulesVO;
import com.carrier.ahu.po.meta.SerialVO;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * Provide serial, validation, values metadata from json files.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
@Slf4j
public class SectionMetadata {

    private static final String SECTION_SERIES_FILE_FORMAT = METADATA_PACKAGE + "section/{0}/serial/ahu.{1}.json";
    private static final String SECTION_VALUES_FILE_FORMAT = METADATA_PACKAGE + "section/{0}/values/{1}.json";
    private static final String UNIT_SECTION_VALUES_FILE_FORMAT = METADATA_PACKAGE + "section/{0}/values/_{1}/{2}.json";
    private static final String SECTION_VALIDATION_FILE_FORMAT = METADATA_PACKAGE + "section/{0}/validation/{1}.json";
    private static final String UNIT_SECTION_VALIDATION_FILE_FORMAT = METADATA_PACKAGE
            + "section/{0}/validation/_{1}/{2}.json";

    private static Map<String, Map<String, List<SerialVO>>> sectionMetaSerials = new HashMap<>();
    private static Map<String, Map<String, MetaValue>> factoryMetaValues = new HashMap<>();
    private static Map<String, Map<String, MetaValue>> unitMetaValues = new HashMap<>();
    private static Map<String, Map<String, List<RulesVO>>> factoryMetaValidations = new HashMap<>();
    private static Map<String, Map<String, List<RulesVO>>> unitMetaValidations = new HashMap<>();

    public static Map<String, MetaValue> getMetaValue(String factory) {
        if (EmptyUtil.isNotEmpty(factory)) {
            if (factoryMetaValues.containsKey(factory)) {
                return factoryMetaValues.get(factory);
            }
            Map<String, MetaValue> factoryMetaValue = loadFactoryMetaValue(factory, null);
            factoryMetaValues.put(factory, factoryMetaValue);
            return factoryMetaValue;
        }
        return new HashMap<>();
    }

    private static Map<String, MetaValue> getUnitMetaValue(String factory, String unitSeries) {
        if (EmptyUtil.isNotEmpty(unitSeries)) {
            if (unitMetaValues.containsKey(unitSeries)) {
                return unitMetaValues.get(unitSeries);
            }
            Map<String, MetaValue> unitMetaValue = loadFactoryMetaValue(factory, unitSeries);
            unitMetaValues.put(unitSeries, unitMetaValue);
            return unitMetaValue;
        }
        return new HashMap<>();
    }

    private static Map<String, MetaValue> loadFactoryMetaValue(String factory, String unitSeries) {
        SectionTypeEnum[] sections = SectionTypeEnum.values();
        Map<String, MetaValue> metaValues = new HashMap<>();
        for (SectionTypeEnum section : sections) {
            String id = section.getId();
            String metaFileName = null;
            if (EmptyUtil.isEmpty(unitSeries)) {
                metaFileName = getSectionValuesFile(factory, id);
            } else {
                metaFileName = getUnitSectionValueFile(factory, unitSeries, id);
            }
            try {
                InputStream in = AHUContext.getJsonInputStream(metaFileName);
                if (EmptyUtil.isNotEmpty(in)) {
                    Gson gson = new Gson();
                    Map<String, MetaValue> values = gson.fromJson(new InputStreamReader(in, Charset.forName("UTF-8")),
                            new TypeToken<Map<String, MetaValue>>() {
                            }.getType());
                    Iterator<MetaValue> it = values.values().iterator();
                    while (it.hasNext()) {
                        MetaValue value = it.next();
                        value.getOption().initValuePair(value.getKey());
                    }
                    metaValues.putAll(values);
                    log.debug("Parse section values successfully for section: " + id);
                }
            } catch (Exception e) {
                log.error("Failed to parse section values for section: " + id, e);
            }
        }
        return metaValues;
    }

    public static Map<String, MetaValue> getMetaValue(String factory, String unitSeries) {
        Map<String, MetaValue> factoryMetaValue = new HashMap<>();
        factoryMetaValue.putAll(getMetaValue(factory));
        Map<String, MetaValue> unitMetaValue = new HashMap<>();
        unitMetaValue = getUnitMetaValue(factory, unitSeries);
        unitMetaValue.forEach((key, metaValue) -> {
            if (factoryMetaValue.containsKey(key)) { // merge unit series meta to factory
                factoryMetaValue.put(key, metaValue);
            }
        });
        return factoryMetaValue;
    }

    public static Map<String, List<RulesVO>> getMetaValidation(String factory) {
        if (EmptyUtil.isNotEmpty(factory)) {
            if (factoryMetaValidations.containsKey(factory)) {
                return factoryMetaValidations.get(factory);
            }
            Map<String, List<RulesVO>> factoryMetaValidation = loadFactoryMetaValidation(factory, null);
            factoryMetaValidations.put(factory, factoryMetaValidation);
            return factoryMetaValidation;
        }
        return new HashMap<>();
    }

    private static Map<String, List<RulesVO>> getUnitMetaValidation(String factory, String unitSeries) {
        if (EmptyUtil.isNotEmpty(unitSeries)) {
            if (unitMetaValidations.containsKey(unitSeries)) {
                return unitMetaValidations.get(unitSeries);
            }
            Map<String, List<RulesVO>> unitMetaValidation = loadFactoryMetaValidation(factory, unitSeries);
            unitMetaValidations.put(unitSeries, unitMetaValidation);
            return unitMetaValidation;
        }
        return new HashMap<>();
    }

    private static Map<String, List<RulesVO>> loadFactoryMetaValidation(String factory, String unitSeries) {
        SectionTypeEnum[] sections = SectionTypeEnum.values();
        Map<String, List<RulesVO>> metaValidations = new HashMap<>();
        for (SectionTypeEnum section : sections) {
            String id = section.getId();
            String metaFileName = null;
            if (EmptyUtil.isEmpty(unitSeries)) {
                metaFileName = getSectionValidationFile(factory, id);
            } else {
                metaFileName = getUnitSectionValidationFile(factory, unitSeries, id);
            }
            try {
                InputStream in = AHUContext.getJsonInputStream(metaFileName);
                if (EmptyUtil.isNotEmpty(in)) {
                    Gson gson = new Gson();
                    Map<String, List<RulesVO>> validations = gson.fromJson(
                            new InputStreamReader(in, Charset.forName("UTF-8")),
                            new TypeToken<Map<String, List<RulesVO>>>() {
                            }.getType());
                    metaValidations.putAll(validations);
                    log.debug("Parse section validations successfully for section: " + id);
                }
            } catch (Exception e) {
                log.error("Failed to parse section validations for section: " + id, e);
            }
        }
        return metaValidations;
    }

    public static Map<String, List<RulesVO>> getMetaValidation(String factory, String unitSeries) {
        Map<String, List<RulesVO>> factoryMetaValidation = new HashMap<>();
        factoryMetaValidation.putAll(getMetaValidation(factory));
        Map<String, List<RulesVO>> unitMetaValidation = new HashMap<>();
        unitMetaValidation = getUnitMetaValidation(factory, unitSeries);
        unitMetaValidation.forEach((key, metaValidation) -> {
            if (factoryMetaValidation.containsKey(key)) { // merge unit series meta to factory
                factoryMetaValidation.put(key, metaValidation);
            }
        });
        return factoryMetaValidation;
    }

    public static Map<String, List<SerialVO>> getMetaSerial(String factory, String unitSeries) {
        if (EmptyUtil.isNotEmpty(unitSeries)) {
            if (sectionMetaSerials.containsKey(unitSeries)) {
                return sectionMetaSerials.get(unitSeries);
            }
            Map<String, List<SerialVO>> sectionMetaSerial = loadSectionMetaSerial(factory, unitSeries);
            sectionMetaSerials.put(unitSeries, sectionMetaSerial);
            return sectionMetaSerial;
        }
        return new HashMap<>();
    }

    private static Map<String, List<SerialVO>> loadSectionMetaSerial(String factory, String unitSeries) {
        String metaFileName = getSectionSerialFile(factory, unitSeries);
        Map<String, List<SerialVO>> sectionMetaSerial = null;
        try {
            InputStream in = AHUContext.getJsonInputStream(metaFileName);
            if (EmptyUtil.isNotEmpty(in)) {
                Gson gson = new Gson();
                sectionMetaSerial = gson.fromJson(new InputStreamReader(in, Charset.forName("UTF-8")),
                        new TypeToken<Map<String, List<SerialVO>>>() {
                        }.getType());
                log.debug("Parse section serial successfully for series: " + unitSeries);
            }
        } catch (JsonIOException | JsonSyntaxException e) {
            log.error("Failed to parse section serial for series: " + unitSeries, e);
        }
        return (sectionMetaSerial == null) ? new HashMap<>() : sectionMetaSerial;
    }

    private static String getSectionSerialFile(String factory, String unitSeries) {
        return MessageFormat.format(SECTION_SERIES_FILE_FORMAT, factory, unitSeries);
    }

    private static String getSectionValuesFile(String factory, String sectionId) {
        return MessageFormat.format(SECTION_VALUES_FILE_FORMAT, factory, sectionId);
    }

    private static String getUnitSectionValueFile(String factory, String unitSeries, String sectionId) {
        return MessageFormat.format(UNIT_SECTION_VALUES_FILE_FORMAT, factory, unitSeries, sectionId);
    }

    private static String getSectionValidationFile(String factory, String sectionId) {
        return MessageFormat.format(SECTION_VALIDATION_FILE_FORMAT, factory, sectionId);
    }

    private static String getUnitSectionValidationFile(String factory, String unitSeries, String sectionId) {
        return MessageFormat.format(UNIT_SECTION_VALIDATION_FILE_FORMAT, factory, unitSeries, sectionId);
    }

}
