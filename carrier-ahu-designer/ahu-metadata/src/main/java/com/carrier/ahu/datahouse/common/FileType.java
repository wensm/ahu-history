package com.carrier.ahu.datahouse.common;

/**
 * Supported file types in datahouse.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
public enum FileType {

    CSV, XLS, XLSX

}
