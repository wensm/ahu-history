package com.carrier.ahu.metadata.entity.price;

/**
 * 
 * Created by Braden Zhou on 2019/03/12.
 */
public interface NonstdPrice {

    public double getTp();

    public double getOldPrice();

}
