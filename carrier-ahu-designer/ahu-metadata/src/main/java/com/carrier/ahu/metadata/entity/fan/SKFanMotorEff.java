package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2018/3/30.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanMotorEff extends DataEntity {

    @EntityId
    private double power;
    @EntityId(sequence = 1)
    private int level;
    private String machinesiteNo;
    private double BEFF3;
    private double BEFF2;
    private double CEFF2;
    private double CEFF3;
    private double V0E3W;
    private double V1E3W;
    private double V2E3W;
    private double V3E3W;
    private double V4E3W;
    private double V5E3W;
    private double V6E3W;
    private double V7E3W;
    private double V8E3W;
    private double V9E3W;
    private double VAE3W;
    private double V0E2T;
    private double V0E3T;
    private double V1E2T;
    private double V1E3T;
    private double V2E2T;
    private double V2E3T;
    private double V3E2T;
    private double V3E3T;
    private double V4E2T;
    private double V4E3T;
    private double V5E2T;
    private double V5E3T;
    private double V6E2T;
    private double V6E3T;
    private double V7E2T;
    private double V7E3T;
    private double V8E2T;
    private double V8E3T;
    private double V9E2T;
    private double V9E3T;
    private double VAE2T;
    private double VAE3T;
    private double V0E3A;
    private double V0E2W;
    private double V1E2W;
    private double V2E2W;
    private double V3E2W;
    private double V4E2W;
    private double V5E2W;
    private double V6E2W;
    private double V7E2W;
    private double V8E2W;
    private double V9E2W;
    private double VAE2W;
    private double V4E3A;

}
