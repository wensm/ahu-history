package com.carrier.ahu.metadata.entity.calc;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * The Price table is a metric, where defined all price index for different
 * ahu/sections/elements.
 *
 * @author liujianfeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class CalculatorSpec extends DataEntity{

	private String type;
	private String ahucal;
	private String name;
	private String property;
	private String tab;
	private String version;
	// Appended by JL for Weight Calculation
	// 价格计算，并没使用到下面的两个属性
	private String section;
	private String file;

	public String getSectionName() {
		int index = name.indexOf(".");
		if (index < 0) {
			return name;
		} else {
			String tmp = name.substring(0, index);
			if (SectionTypeEnum.TYPE_AHU.getId().equals(tmp)) {
				return tmp;
			} else {
				return SectionTypeEnum.TYPE_AHU.getId() + "." + tmp.toLowerCase();
			}
		}
	}

	public String getSimpleInfo() {
		StringBuffer sipBuf = new StringBuffer();
		sipBuf.append(",").append("file=").append(file);
		sipBuf.append(",").append("tab=").append(tab);
		sipBuf.append(",").append("property=").append(property);
		sipBuf.append(",").append("section=").append(section);
		sipBuf.append(",").append("name=").append(name);
		return sipBuf.substring(1);
	}

    public String getSpecKey() {
        return this.getSection() + "." + this.getFile() + "." + this.getName();
    }

}
