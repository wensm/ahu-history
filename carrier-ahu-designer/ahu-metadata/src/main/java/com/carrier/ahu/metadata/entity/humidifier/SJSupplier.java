package com.carrier.ahu.metadata.entity.humidifier;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/10/24.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SJSupplier extends DataEntity {

    @EntityId
    private String supplier;
    private int nozzleN;
    private String aperture;
    private int hq;

}
