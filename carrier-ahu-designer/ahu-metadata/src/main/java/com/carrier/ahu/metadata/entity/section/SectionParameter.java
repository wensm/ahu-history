package com.carrier.ahu.metadata.entity.section;

import com.carrier.ahu.datahouse.entity.ExcelEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Section meta data from section_parameter excel file.
 * 
 * Created by Braden Zhou on 2018/06/01.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SectionParameter extends ExcelEntity {

    // 唯一的属性的名字
    private String key;
    // 英文名字，临时用于多语言时，界面的英文展现
    private String name;
    // 属性的虚拟分组，暂时没有意义，与界面上的属性面板的名字对应，可以先写name
    private String group;
    // 描述该属性是否与材料相关
    private boolean rMaterial;
    // 描述该属性是否与ahu的方向相关
    private boolean rDirection;
    // 描述该属性是否与性能相关
    private boolean rPerformance;
    // 描述该属性是否可编辑，不可编辑，意味着时通过计算获取
    private boolean editable;
    // 是否为全局属性，默认为false，暂时不使用
    private boolean global;
    // 一个属性有很多选项值，例如材料，这些值需要来自另一个元数据
    // SOURCE 定义在：MetaValueSource
    private String valueSource;
    // 属性的值的类型
    private String valueType;
    // 其他额外信息，现在存放中文名称
    private String memo;
    // 默认存放英文的信息
    private String ememo;
    // 改属性是否允许非标配置，如果允许，会从该属性衍生出一个非标属性，元数据与原始属性相同
    private boolean urEnable;
    // 默认值
    private String defaultValue;

    public SectionParameter() {

    }

}
