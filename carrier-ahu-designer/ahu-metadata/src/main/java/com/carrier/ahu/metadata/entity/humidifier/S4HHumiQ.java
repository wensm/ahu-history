package com.carrier.ahu.metadata.entity.humidifier;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE)//忽略整个类jackson默认首字母转小写
public class S4HHumiQ extends DataEntity {

    @EntityId
    private String supplier;
    private double humidificationQ;
    private int HTypes;
    private double VaporPressure;

}
