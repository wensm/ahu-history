package com.carrier.ahu.datahouse.csv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.carrier.ahu.datahouse.Datahouse;
import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Using super csv library to load bean data from csv file.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
@SuppressWarnings({ "rawtypes" })
public class CsvDatahouse<T, K> extends Datahouse<T, K> {

    protected static Logger logger = LoggerFactory.getLogger(CsvDatahouse.class);

    public CsvDatahouse(Class<T> clazz, String fileName) {
        super(clazz, fileName);
    }

    @Override
    protected List<T> loadRecords() {
        InputStream inputStream = this.getClazz().getResourceAsStream(this.getFileName());
        InputStreamReader fileReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));

        List<T> records = new ArrayList<T>();
        ICsvBeanReader beanReader = null;
        try {
            beanReader = new CsvBeanReader(fileReader, CsvPreference.STANDARD_PREFERENCE);
            final String[] header = beanReader.getHeader(true);
            final CellProcessor[] processors = new CellProcessor[header.length];
            for (int i = 0; i < header.length; i++) {
                String fieldName = header[i];
                Class c = BeanUtils.findPropertyType(fieldName, this.getClazz());
                processors[i] = getCellProcessor(c, isNullable(fieldName));
            }
            T record;
            while (EmptyUtil.isNotEmpty((record = beanReader.read(this.getClazz(), header, processors)))) {
                records.add(record);
            }
        } catch (Exception e) {
            throw new RuntimeException(
                    String.format("Failed to load bean [%s] from %s.", this.getClazz().getName(), this.getFileName()),
                    e);
        } finally {
            if (EmptyUtil.isNotEmpty(beanReader)) {
                try {
                    beanReader.close();
                } catch (IOException e) {
                    throw new RuntimeException(
                            String.format("Failed to close bean reader for bean [%s].", this.getClazz().getName()), e);
                }
            }
        }
        return records;
    }

    private boolean isNullable(String fieldName) throws Exception {
        Field field = getDeclaredField(this.getClazz(), fieldName);
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof AllowNull) {
                return true;
            }
        }
        return false;
    }

    private Field getDeclaredField(Class<?> clazz, String fieldName) {
        if (clazz != null) {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                return getDeclaredField(clazz.getSuperclass(), fieldName);
            }
        }
        return null;
    }

    private CellProcessor getCellProcessor(Class fieldClass, boolean isNullable) {
        if (fieldClass.equals(String.class)) {
            return isNullable ? new ConvertNullTo("") : new NotNull();
        } else if (fieldClass.equals(int.class) || fieldClass.equals(Integer.class)) {
            return isNullable ? new ConvertNullTo(0, new ParseInt()) : new NotNull(new ParseInt());
        } else if (fieldClass.equals(double.class) || fieldClass.equals(Double.class)) {
            return isNullable ? new ConvertNullTo(0.0, new ParseDouble()) : new NotNull(new ParseDouble());
        } else if (fieldClass.equals(float.class) || fieldClass.equals(Float.class)) {
            return isNullable ? new ConvertNullTo(0.0, new ParseDouble()) : new NotNull(new ParseDouble());
        } else if (fieldClass.equals(long.class) || fieldClass.equals(Long.class)) {
            return isNullable ? new ConvertNullTo(0, new ParseLong()) : new NotNull(new ParseLong());
        } else if (fieldClass.equals(boolean.class) || fieldClass.equals(Boolean.class)) {
            return isNullable ? new ConvertNullTo(false, new ParseBool()) : new NotNull(new ParseBool());
        }
        return isNullable ? new ConvertNullTo("") : new NotNull();
    }

}
