package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PaneledDoor extends DataEntity {

    @EntityId
    private String unit;
    @EntityId(sequence = 1)
    private String partType;
    @AllowNull
    private String door;

}
