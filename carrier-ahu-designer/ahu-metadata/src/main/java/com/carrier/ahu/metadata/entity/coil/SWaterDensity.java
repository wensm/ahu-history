package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SWaterDensity extends DataEntity {

    private int waterTemp;
    private double waterDensity;

}
