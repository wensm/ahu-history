package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HCoilBigTuojiaDimension extends DataEntity {

    @EntityId
    private String unitType;
    @AllowNull
    private String a;
    @AllowNull
    private String b;
    @AllowNull
    private String c;
    @AllowNull
    private String d;
    @AllowNull
    private String e;
    @AllowNull
    private String f;
    @AllowNull
    private String g;
    @AllowNull
    private String h;
    @AllowNull
    private String j;
    @AllowNull
    private String k;
    @AllowNull
    private String l;
    @AllowNull
    private String m;
    @AllowNull
    private String n;
    @AllowNull
    private String w;

}
