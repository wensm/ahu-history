package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DeliveryInfo extends DataEntity {

    @EntityId(sequence = 0)
    private String partName;
    @EntityId(sequence = 1)
    private String option;
    private String delivery;

}
