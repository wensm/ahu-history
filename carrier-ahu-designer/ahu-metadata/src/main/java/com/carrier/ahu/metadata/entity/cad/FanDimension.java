package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FanDimension extends DataEntity {

    @EntityId
    private String unitType;
    @EntityId(sequence = 1)
    private String fanType;
    private String a;
    private String d;
    private String thf;
    private String bhf;
    private String ubf;
    private String ubr;
    @EntityId(sequence = 2)
    private String unitModel;

}
