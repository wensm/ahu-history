package com.carrier.ahu.metadata.entity.coil;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.metadata.entity.Ahu;
import com.carrier.ahu.util.EmptyUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CoilSize extends Ahu {

    private double finHeight;
    private double finLength;
    private String coilHeightStr;
    private int[] coilHeights;
    private double coilLength;
    private double faceArea;
    private double finHeightH;
    private double finLengthH;
    private String coilHeightHStr;
    private int[] coilHeightHs;
    private double coilLengthH;
    private double faceAreaH;
    private double numOfLayer;
    private double noOfSide;
    private String tubeDiameter;
    private String tubeHeightStr;
    private int[] tubeHeights;

    public int[] getCoilHeights() {
        if (EmptyUtil.isNotEmpty(coilHeights)) {
            return coilHeights;
        }
        if (StringUtils.isEmpty(coilHeightStr)) {
            coilHeights = new int[0];
            return coilHeights;
        }

        coilHeights = getIntArrFromString(coilHeightStr);
        return coilHeights;
    }

    public int[] getCoilHeightHs() {
        if (EmptyUtil.isNotEmpty(coilHeightHs)) {
            return coilHeightHs;
        }
        if (StringUtils.isEmpty(coilHeightHStr)) {
            coilHeightHs = new int[0];
            return coilHeightHs;
        }

        coilHeightHs = getIntArrFromString(coilHeightHStr);
        return coilHeightHs;
    }

    public int[] getTubeHeights() {
        if (EmptyUtil.isNotEmpty(tubeHeights)) {
            return tubeHeights;
        }
        if (StringUtils.isEmpty(tubeHeightStr)) {
            tubeHeights = new int[0];
            return tubeHeights;
        }
        tubeHeights = getIntArrFromString(tubeHeightStr);
        return tubeHeights;
    }

    private int[] getIntArrFromString(String str) {
        String[] arr = str.trim().split("/");
        int[] tempIntArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            String string = arr[i];
            try {
                tempIntArr[i] = Integer.parseInt(string.trim());
            } catch (Exception e) {
                tempIntArr[i] = 0;
            }
        }
        return tempIntArr;
    }

}
