package com.carrier.ahu.metadata.common;

/**
 * Excel file name and sheet name.
 * 
 * Created by Braden Zhou on 2018/05/11.
 */
public enum ExcelMetaType {

    // @format off
    AHU_PROP_UNIT("unit", FileConstants.AHU_PROP_UNIT_EXCEL);
    // @format on

    private String sheetName;
    private String fileName;

    private ExcelMetaType(String sheetName, String fileName) {
        this.sheetName = sheetName;
        this.fileName = fileName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public String getFileName() {
        return fileName;
    }

}
