package com.carrier.ahu.metadata.entity.access;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AccessData extends DataEntity {

    private double velocity;
    @EntityId
    private int sectionL;
    private double resistance;

}
