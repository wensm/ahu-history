package com.carrier.ahu.datahouse.csv;

import org.supercsv.cellprocessor.ift.CellProcessor;

/**
 * Created by Braden Zhou on 2018/04/27.
 */
public interface CellProcessorProvider {

    public CellProcessor[] getCellProcessors(Class<?> clazz);

}
