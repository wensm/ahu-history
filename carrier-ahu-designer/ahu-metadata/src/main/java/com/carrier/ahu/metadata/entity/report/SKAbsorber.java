package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2018/3/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SKAbsorber extends DataEntity {

    @EntityId
    private String fan;
    @EntityId(sequence = 1)
    private String machineSiteNo;
    @AllowNull
    private String a;
    @AllowNull
    private String b;
    @AllowNull
    private String c;
    @AllowNull
    private String d;
    @AllowNull
    private String e;
    @AllowNull
    private String f;

}
