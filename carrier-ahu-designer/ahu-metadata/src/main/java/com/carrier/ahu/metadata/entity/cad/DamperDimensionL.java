package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DamperDimensionL extends DataEntity {

    @EntityId
    private String unit;
    @EntityId(sequence = 1)
    private String mixType;
    @EntityId(sequence = 2)
    private String damperType;
    private String sectionL;
    private String a;
    private String b;
    private String c;
    private String d;
    private String b1;

}
