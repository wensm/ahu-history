package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SFanLength2 extends DataEntity {

    @EntityId
    private String unitType;
    @EntityId(sequence = 1)
    private String id;
    @EntityId(sequence = 2)
    private String type;
    private int typeLen;
    @AllowNull
    private int typeLen1;
    @AllowNull
    private String remark;
    private int inletLen;

}
