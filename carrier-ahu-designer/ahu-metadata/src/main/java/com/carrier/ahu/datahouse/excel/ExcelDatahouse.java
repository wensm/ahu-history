package com.carrier.ahu.datahouse.excel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.datahouse.Datahouse;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;

/**
 * Load bean data from excel file.
 * 
 * Created by Braden Zhou on 2018/05/11.
 */
public class ExcelDatahouse<T, K> extends Datahouse<T, K> {

    protected static Logger logger = LoggerFactory.getLogger(ExcelDatahouse.class);
    private boolean isClassified;

    public ExcelDatahouse(Class<T> clazz, String fileName) {
        this(clazz, fileName, false);
    }

    public ExcelDatahouse(Class<T> clazz, String fileName, boolean isClassified) {
        super(clazz, fileName);
        this.isClassified = isClassified;
    }

    @Override
    protected List<T> loadRecords() {
        InputStream excelStream = null;
        Workbook workbook = null;
        List<T> records = new ArrayList<T>();
        try {
            excelStream = this.getClazz().getResourceAsStream(this.getFileName());
            workbook = ExcelUtils.read(excelStream, ExcelUtils.isExcel2003(this.getFileName()));
            List<String> sheetNames = getSheetNames(workbook);
            for (String sheetName : sheetNames) {
                List<List<String>> list = ExcelUtils.readSheet(workbook, sheetName, 0);
                List<String> columnNames = parseColumnNames(list.remove(0));
                for (List<String> row : list) {
                    T t = this.getClazz().newInstance();
                    for (int i = 0; i < row.size(); i++) {
                        // workaround solution to remove zero decimal
                        String value = row.get(i);
                        if (value.endsWith(".0")) {
                            value = value.substring(0, value.length() - 2);
                        }
                        BeanUtils.setProperty(t, columnNames.get(i), value);
                    }
                    if (isClassified) {
                        BeanUtils.setProperty(t, "sheetName", sheetName);
                    }
                    records.add(t);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(
                    String.format("Failed to load bean [%s] from %s.", this.getClazz().getName(), this.getFileName()));
        } finally {
            if (EmptyUtil.isNotEmpty(workbook)) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    throw new RuntimeException(
                            String.format("Failed to close bean reader for bean [%s].", this.getClazz().getName()));
                }
            }
            if (EmptyUtil.isNotEmpty(excelStream)) {
                try {
                    excelStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(
                            String.format("Failed to close bean reader for bean [%s].", this.getClazz().getName()));
                }
            }
        }
        return records;
    }

    private List<String> getSheetNames(Workbook workbook) {
        List<String> sheetNames = new ArrayList<>();
        if (isClassified) {
            int sheetNumber = workbook.getNumberOfSheets();
            for (int i = 0; i < sheetNumber; i++) {
                sheetNames.add(workbook.getSheetName(i));
            }
        } else {
            sheetNames.add(this.getClazz().getSimpleName());
        }
        return sheetNames;
    }

    /**
     * Remove Chinese chars from column name. e.g. '序号/no' to 'no'.
     * 
     * @param originalColumnNames
     * @return
     */
    private List<String> parseColumnNames(List<String> originalColumnNames) {
        List<String> columnNames = new ArrayList<>();
        for (String columnName : originalColumnNames) {
            int index = columnName.indexOf("/");
            if (index > -1) {
                columnNames.add(columnName.substring(index + 1));
            } else {
                columnNames.add(columnName);
            }
        }
        return columnNames;
    }

}
