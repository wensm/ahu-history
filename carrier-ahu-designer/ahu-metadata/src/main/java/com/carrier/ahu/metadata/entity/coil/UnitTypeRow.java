package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UnitTypeRow extends DataEntity {

    @EntityId
    private String unit;
    private String type;
    private String tubeDiameter;
    private String rows;
}
