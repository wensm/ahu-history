package com.carrier.ahu.metadata.entity.coil;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/8/22.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SDPAirFactorHDia1 extends SAirDropFactor {
}
