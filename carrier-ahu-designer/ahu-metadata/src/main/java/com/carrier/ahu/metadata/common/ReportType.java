package com.carrier.ahu.metadata.common;

/**
 * Report type's metadata and file name. 
 * 
 * Created by Braden Zhou on 2018/05/10.
 */
public enum ReportType {

    //@format off
    ;

    // @format on

    private String reportName;
    private String fileName;

    private ReportType(String reportName, String fileName) {
        this.reportName = reportName;
        this.fileName = fileName;
    }

    public String getReportName() {
        return reportName;
    }

    public String getFileName() {
        return fileName;
    }

}
