package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SSurfaceGeometrics extends DataEntity {

    @EntityId
    private int id;
    private String geometrics;
    private double odExp;
    private double tw;
    private double idBend;
    private double idHairpin;
    private double pt;
    private double pr;

}
