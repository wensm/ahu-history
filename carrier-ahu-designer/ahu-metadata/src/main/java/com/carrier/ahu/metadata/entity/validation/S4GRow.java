package com.carrier.ahu.metadata.entity.validation;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2018/4/25.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class S4GRow extends DataEntity {

    @EntityId
    private String fanType;
    private int row1;
    private int row2;
    private int row3;

}
