package com.carrier.ahu.datahouse.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Matrix entity class represents as column names and row data in a map with
 * column name as the key.
 * 
 * Created by Braden Zhou on 2018/05/11.
 */
public class MatrixEntity {

    private List<String> columnNames;
    private Map<String, String[]> rows;

    public String findMatrixValue(String column, String row) {
        String[] rowData = null;
        if (rows.containsKey(row)) {
            rowData = rows.get(row);
            int index = columnNames.indexOf(column);
            if (index != -1) {
                return rowData[index];
            }
        }

        return null;
    }

    public void addColumnNames(List<String> columnNames) {
        if (this.columnNames == null) {
            this.columnNames = new ArrayList<>();
        }
        this.columnNames.addAll(columnNames);
    }

    public void addRow(String columnName, String[] row) {
        if (this.rows == null) {
            this.rows = new HashMap<>();
        }
        this.rows.put(columnName, row);
    }

    public String findFirstMatrixValue(String row) {
        String[] rowData = null;
        if (rows.containsKey(row)) {
            rowData = rows.get(row);
            return rowData[0];
        }
        return null;
    }

}
