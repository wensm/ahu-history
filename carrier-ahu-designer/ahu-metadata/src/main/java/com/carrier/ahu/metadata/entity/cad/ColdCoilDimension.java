package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ColdCoilDimension extends DataEntity {

    @EntityId
    private String unitType;
    private String a;
    private String b;
    private String c;
    private String rd3;
    private String rd4;
    private String rd5;
    private String rd6;
    private String rd7;
    private String rd8;
    private String rd10;
    private String rd12;
    private String re3;
    private String re4;
    private String re5;
    private String re6;
    private String re7;
    private String re8;
    private String re10;
    private String re12;
    private String f;
    private String g;
    private String h;
    private String dia;

}
