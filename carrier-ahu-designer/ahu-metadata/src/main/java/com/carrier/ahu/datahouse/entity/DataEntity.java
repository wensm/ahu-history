package com.carrier.ahu.datahouse.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Base entity class, the naming convention should be consistent with files.
 * </br>
 * For example, AhuSize.class -> ahu_size.csv </br>
 * </br>
 * Created by Braden Zhou on 2018/04/27.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataEntity {

}
