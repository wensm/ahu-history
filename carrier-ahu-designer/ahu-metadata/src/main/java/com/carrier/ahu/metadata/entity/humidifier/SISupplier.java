package com.carrier.ahu.metadata.entity.humidifier;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/9/11.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SISupplier extends DataEntity {

    @EntityId
    private String supplier;
    private int thickness;
    private double velocity;
    private double resistance;
    private double efficiency;

}
