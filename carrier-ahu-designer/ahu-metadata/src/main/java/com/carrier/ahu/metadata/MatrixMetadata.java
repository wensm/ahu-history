package com.carrier.ahu.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.datahouse.entity.MatrixEntity;
import com.carrier.ahu.metadata.common.MatrixType;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Read matrix data from csv file which defined in MatrixType enum.
 * 
 * Created by Braden Zhou on 2018/05/11.
 */
public class MatrixMetadata {

    private static Map<MatrixType, MatrixEntity> matrixMetadata = new HashMap<>();

    public static MatrixEntity findMatrix(MatrixType matrixType) {
        MatrixEntity matrixEntity = null;
        if (matrixMetadata.containsKey(matrixType)) {
            matrixEntity = matrixMetadata.get(matrixType);
        } else {
            matrixEntity = loadMatrixEntity(matrixType);
        }
        return matrixEntity;
    }

    private static MatrixEntity loadMatrixEntity(MatrixType matrixType) {
        InputStream inputStream = MatrixMetadata.class.getResourceAsStream(matrixType.getFileName());
        InputStreamReader fileReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        CsvListReader listReader = null;
        MatrixEntity matrixEntity = null;
        try {
            listReader = new CsvListReader(fileReader, CsvPreference.STANDARD_PREFERENCE);
            matrixEntity = new MatrixEntity();
            List<String> row = listReader.read();
            row.remove(0);
            matrixEntity.addColumnNames(row);
            while (EmptyUtil.isNotEmpty((row = listReader.read()))) {
                String columnName = row.remove(0);
                matrixEntity.addRow(columnName, row.toArray(new String[0]));
            }
        } catch (Exception e) {
            throw new AhuException("Failed to read matrix from " + matrixType.getFileName(), e);
        } finally {
            if (EmptyUtil.isNotEmpty(listReader)) {
                try {
                    listReader.close();
                } catch (IOException e) {
                    throw new AhuException("Failed to read matrix from " + matrixType.getFileName(), e);
                }
            }
        }
        return matrixEntity;
    }

    public static String findMatrixValue(MatrixType matrixType, String column, String row) {
        MatrixEntity matrixEntity = findMatrix(matrixType);
        return matrixEntity.findMatrixValue(column, row);
    }

    public static String findFirstMatrixValue(MatrixType matrixType, String row) {
        MatrixEntity matrixEntity = findMatrix(matrixType);
        return matrixEntity.findFirstMatrixValue(row);
    }

}
