package com.carrier.ahu.metadata.common;

/**
 * All source file name are declared here.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
public class FileConstants {

    public static final String CSV_EXTENSION = ".csv";

    /** CSV FILES */
    public static final String AHU_SIZE_CSV = "ahu_size.csv";
    public static final String AHU_SIZE_DETAIL_CSV = "ahu_size_detail.csv";
    public static final String COIL_SIZE_CSV = "coil_size.csv";
    public static final String COIL_DLL_CALCULATION_CSV = "coil_dll_calculation.csv";
    public static final String ELECTROSTATIC_FILTER_RESISTANCE_CSV = "electrostatic_filter_resistance.csv";
    public static final String NWF_SINGLE_FILTER_RESISTANCE_CSV = "nwf_single_filter_resistance.csv";
    public static final String AL_SINGLE_FILTER_RESISTANCE_CSV = "al_single_filter_resistance.csv";
    public static final String HEPA_FILTER_RESISTANCE_CSV = "hepa_filter_resistance.csv";
    public static final String FILTER_COMPOSING_CSV = "filter_composing.csv";
    public static final String S_4Y_FILTER_FORMAT = "s_4yfilterformat.csv";
    public static final String HEPA_FILTER_COMPOSING_CSV = "hepa_filter_composing.csv";
    public static final String PLUG_FAN_TYPE_CSV = "s_k_plugfan_type.csv";
    public static final String FAN_MOTOR_CSV = "fan_motor.csv";
    public static final String S_FAN_CSV = "s_fan.csv";
    public static final String S_FAN_LENGTH_CSV = "s_fan_length.csv";
    public static final String S_FAN_LENGTH1_CSV = "s_fan_length1.csv";
    public static final String S_FAN_LENGTH2_CSV = "s_fan_length2.csv";
    public static final String S_SECTION_K_BIG_CSV = "s_section_k_big.csv";
    public static final String SECTION_AREA_CSV = "section_area.csv";
    public static final String SECTION_LENGTH_CSV = "section_length.csv";
    public static final String ADJUST_AIR_DOOR_CSV = "s_adjust_air_door.csv";
    public static final String COIL_INFO_CSV = "s_coil_info.csv";
    public static final String COIL_INFO_CSV1 = "s_coil_info1.csv";
    public static final String DELIVERY_INFO_CSV = "s_delivery_info.csv";
    public static final String COIL_BIG_DIMENSION_L_CSV = "s_coil_big_dimension_l.csv";
    public static final String COIL_BIG_DIMENSION_R_CSV = "s_coil_big_dimension_r.csv";
    public static final String COIL_BIG_TUOJIA_DIMENSION_CSV = "s_coil_big_tuojia_dimension.csv";
    public static final String COIL_DIMENSION_CSV = "s_coil_dimension.csv";
    public static final String COLD_COIL_DIMENSION_CSV = "s_cold_coil_dimension.csv";
    public static final String DAMPER_DIMENSION_FAC_CSV = "s_damper_dimension_fac.csv";
    public static final String DAMPER_DIMENSION_L_CSV = "s_damper_dimension_l.csv";
    public static final String DAMPER_DIMENSION_CSV = "s_damper_dimension.csv";
    public static final String E_COIL_DIMENSION_CSV = "s_e_coil_dimension.csv";
    public static final String FAN_DIMENSION_BMOTOR_CSV = "s_fan_dimension_bmotor.csv";
    public static final String FAN_DIMENSION_CSV = "s_fan_dimension.csv";
    public static final String FILTER_DRAWING_CSV = "s_filter_drawing.csv";
    public static final String HEAT_COIL_DIMENSION_CSV = "s_heat_coil_dimension.csv";
    public static final String PANELED_DOOR_CSV = "s_paneled_door.csv";
    public static final String STEAM_BIG_TUOJIA_DIMENSION_CSV = "s_steam_big_tuojia_dimension.csv";
    public static final String STEAM_COIL_DIMENSION_CSV = "s_steam_coil_dimension.csv";
    public static final String DAMPER_CSV = "damper.csv";
    public static final String PRICE_CALCULATOR_SPEC_CSV = "price_calculator_spec.csv";
    public static final String WEIGHT_CALCULATOR_SPEC_CSV = "weight_calculator_spec.csv";
    public static final String ACCESS_DATA_CSV = "access_data.csv";
    public static final String S_4F_AREA_CSV = "s_4f_area.csv";
    public static final String S_4FTHPRESS_CSV = "s_4fthpress.csv";
    public static final String S_4H_HUMI_Q_CSV = "s_4h_humi_q.csv";
    public static final String S_I_SUPPLIER_CSV = "s_i_supplier.csv";
    public static final String S_J_SUPPLIER_CSV = "s_j_supplier.csv";
    public static final String S_4G_ROW_CSV = "s_4g_row.csv";
    public static final String S_UVC_LIGHT_CSV = "s_uvc_light.csv";
    public static final String S_BREAD_WATER_RESI_BIG_CSV = "s_bread_water_resi_big.csv";
    public static final String S_BREAD_WATER_RESI_CSV = "s_bread_water_resi.csv";
    public static final String S_CLEAR_SOUND_RESI_CSV = "s_clear_sound_resi.csv";
    public static final String S_ELECTRICITY_RESI_CSV = "s_electricity_resi.csv";
    public static final String S_JUNLIUQI_RESI_CSV = "s_junliuqi_resi.csv";
    public static final String S_FOR_FRANCE_CSV = "s_for_france.csv";
    public static final String S_K_ABSORBER_CSV = "s_k_absorber.csv";
    public static final String S_TAKEOVER_SIZE_CSV = "s_takeover_size.csv";
    public static final String S_VMOTOR_RANGE_CSV = "s_vmotor_range.csv";
    public static final String UNIT_TYPE_TD_ROWS_CSV = "unit_type_td_rows.csv";
    public static final String EXPORT_TEMPLET_CSV = "export_templet.csv";
    public static final String S_HCOILBIGDIMENSION_CSV="s_hcoilbigdimension.csv";
    public static final String S_HCOILBIGDIMENSION_NEW_CSV="s_hcoilbigdimension_new.csv";
    public static final String S_HCOILBIGTUOJIADIMENSION_CSV="s_hcoilbigtuojiadimension.csv";
    public static final String S_HEATCOILDIMENSION_CSV="s_heatcoildimension.csv";
    public static final String S_HEATCOILDIMENSION_NEW_CSV="s_heatcoildimension_new.csv";
    public static final String S_KABSORBERBIG_CSV="s_k_absorber_big.csv";
    public static final String S_K_FAN_VMOTOR_EFF_CSV="s_k_fan_vmotor_eff.csv";
    public static final String S_WATER_DENSITY_CSV="s_waterdensity.csv";
    public static final String S_FAN_MOTORBASEDIMENSION_CSV="s_fan_motorbasedimension.csv";
    public static final String S_K_FAN_MOTOR_DIMENSION_CSV="s_k_fan_motor_dimension.csv";
    public static final String CONVERTER_MOTOR_BIG_CSV="converter_motor_big.csv";
    public static final String FREQUENCY_SMALL_CSV="frequency_small.csv";
    public static final String S_TWO_SPEED_MOTOR_CSV="s_two_speed_motor.csv";



    /** EXCEL FILES */
    public static final String AHU_PROP_UNIT_EXCEL = "ahu_meta_unit.xlsx";
    public static final String MIX_SIZE_EXCEL = "mix_size.xlsx";
    public static final String HEAT_RECYCLE_4W_EXCEL = "heat_recycle_4w.xlsx";
    public static final String PRICE_CODE_EXCEL = "price_code.xlsx";
    public static final String SECTION_PARAMETER_EXCEL = "section_parameter.xls";

}
