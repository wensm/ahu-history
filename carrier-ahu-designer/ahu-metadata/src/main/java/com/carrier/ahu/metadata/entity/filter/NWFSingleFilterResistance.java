package com.carrier.ahu.metadata.entity.filter;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NWFSingleFilterResistance extends DataEntity {

    private String filterf;
    private int length;
    private String standard;
    private double windSpeed;
    private double beginResi;
    private double averResi;
    private double endResi;
    private String ahu;

}
