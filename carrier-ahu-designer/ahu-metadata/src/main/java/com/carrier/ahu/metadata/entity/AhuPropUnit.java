package com.carrier.ahu.metadata.entity;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AhuPropUnit extends DataEntity {

    @EntityId
    private String key;
    private String name;
    private String blabel;
    private String mlabel;
    private float tom;
    private float tob;

}
