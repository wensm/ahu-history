package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HeatCoilDimension2 extends DataEntity {

    @EntityId
    private String unitType;
    private String a;
    private String b;
    private String c;
    private String d1;
    private String d2;
    private String e1;
    private String e2;
    private String f;
    private String dia;

}
