package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ECoilDimension extends DataEntity {

    @EntityId
    private String unitType;
    private String h;
    private String l;
    private String m;
    private String n;
    private String h1;
    private String w;

}
