package com.carrier.ahu.metadata.entity.calc;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class WeightCalculatorSpec extends CalculatorSpec {
}