package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class S4FArea extends DataEntity {

    @EntityId
    private String fanType;
    private double fArea;
    private double faArea;
    private double ffArea;

}
