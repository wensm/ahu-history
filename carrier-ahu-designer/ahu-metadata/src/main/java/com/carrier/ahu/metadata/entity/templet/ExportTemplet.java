package com.carrier.ahu.metadata.entity.templet;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 导出模板字段控制
 * @author wangd1
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ExportTemplet extends DataEntity{
	
	private String exportField;
	
}