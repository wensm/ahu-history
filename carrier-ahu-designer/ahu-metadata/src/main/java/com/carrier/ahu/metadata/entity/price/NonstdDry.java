package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 干蒸加湿段
 * 查询条件为:（老unit 型号） + （控制方式a:手动b：自动）+（加湿量 >= 系统加适量） 的第一条记录
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdDry extends DataEntity {
    @EntityId
    private String unit;//老型号
    private double tp;//新价格
    private double oldPrice;//老价格
    @EntityId(sequence = 1)
    private String control;//控制方式 a:手动b：自动
    private String sprayV;//加湿量
}
