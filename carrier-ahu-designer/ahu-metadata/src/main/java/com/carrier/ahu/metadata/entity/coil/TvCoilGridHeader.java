package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TvCoilGridHeader extends DataEntity {

    private int id;
    @AllowNull
    private String name;
    @AllowNull
    private int parentId;
    private boolean band;
    private int width;
    private boolean filtering;
    private boolean sorting;
    @EntityId
    private String interfaceName;

}
