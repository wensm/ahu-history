package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Use power and sequence as searching fields.
 * 
 * Created by Braden Zhou on 2018/05/03.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FanMotor extends DataEntity {

    @EntityId(sequence = 1)
    private double power;
    @EntityId(sequence = 2)
    private int level;
    private String machineSiteNo;
    private String machineSite;
    private int weight;
    private double inputPower;
    private String machineSiteNo1;
    private String motorCode;
    private int seq;

}
