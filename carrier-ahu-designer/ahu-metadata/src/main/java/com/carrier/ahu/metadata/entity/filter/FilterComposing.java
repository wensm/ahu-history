package com.carrier.ahu.metadata.entity.filter;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FilterComposing extends DataEntity {

    @EntityId
    private String fanType;
    @EntityId(sequence = 1)
    private int filterFormat;
    private String filterSize;
    private int filterNum;

}
