package com.carrier.ahu.datahouse;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.datahouse.entity.EntityId;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Provides basic interface for data retrieve, find by id and count.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
public abstract class Datahouse<T, K> {

    protected static Logger logger = LoggerFactory.getLogger(Datahouse.class);

    private static String HASH = "#";
    private static String NULL = "null";
    private static String START_WITH = "#startsWith";
    private static String EQUALS = "#equals";
    private static String IGNORE = "#ignore";

    private String fileName;
    private Class<T> clazz;
    private boolean loaded;
    private List<T> records;
    private List<Field> idFields;

    public Datahouse(Class<T> clazz, String fileName) {
        this.clazz = clazz;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public List<T> getRecords() {
        return records;
    }

    /**
     * Keep it unchangeable.
     * 
     * @param records
     */
    private void setRecords(List<T> records) {
        this.records = records;
    }

    public List<T> findAll() {
        if (!isLoaded()) {
            this.setRecords(Collections.unmodifiableList(this.loadRecords()));
        }
        return this.getRecords();
    }

    public T findOne(K id) {
        List<T> ts = this.findList(id);
        if (ts.size() > 0) {
            return ts.get(0);
        }
        return null;
    }

    public List<T> findList(K id) {
        this.findAll();
        List<T> ts = new ArrayList<T>();
        List<Field> idFields = getIdFields(this.clazz);
        if (idFields.size() > 0) {
            Map<String, String> matchOperations = this.parseSearchText(id);
            for (T t : records) {
                if (isMatch(t, idFields, matchOperations)) {
                    ts.add(t);
                }
            }
        } else {
            logger.warn("No Id declaration found in class {}。", this.getClazz().getName());
        }
        return ts;
    }

    private Map<String, String> parseSearchText(K id) {
        Map<String, String> matchOperations = new LinkedHashMap<String, String>();

        // support String array only at the moment
        String[] searchTexts = null;
        if (id instanceof String[]) {
            searchTexts = (String[]) id;
        } else {
            searchTexts = new String[] { String.valueOf(id) };
        }
        if (searchTexts != null) {
            int n = 0;
            for (String searchText : searchTexts) {
                if (searchText != null) {
                    if (searchText.startsWith(HASH)) {
                        if (searchText.startsWith(START_WITH)) {
                            String prefix = searchText.substring(START_WITH.length() + 1, searchText.length() - 1);
                            matchOperations.put(prefix + HASH + n, START_WITH);
                            n++;
                            continue;
                        }
                    }
                    matchOperations.put(searchText + HASH + n, EQUALS);
                } else {
                    matchOperations.put(NULL + HASH + n, IGNORE); // handle multiple null text
                }
                n++;
            }
        }
        return matchOperations;
    }

    private List<Field> getIdFields(Class<?> clazz) {
        if (idFields == null) {
            idFields = parseIdFields(clazz);
        }
        return idFields;
    }

    private boolean isMatch(T t, List<Field> idFields, Map<String, String> matchOperations) {
        boolean matched = true;
        String[] searchs = matchOperations.keySet().toArray(new String[0]);
        for (int i = 0; i < idFields.size(); i++) {
            if (i < searchs.length) {
                String searchText = searchs[i];
                if (searchText.contains(HASH)) {
                    searchText = searchText.substring(0, searchText.indexOf(HASH));
                }
                Field idField = idFields.get(i);
                try {
                    if (!searchText.startsWith(NULL)) { // ignore null search text
                        Object value = FieldUtils.readField(t, idField.getName(), true);
                        String operation = matchOperations.get(searchs[i]);
                        if (START_WITH.equals(operation)) {
                            if (!String.valueOf(value).startsWith(searchText)) {
                                matched = false;
                                break;
                            }
                        } else if (!String.valueOf(value).equals(searchText)) {
                            matched = false;
                            break;
                        }
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    logger.error("Failed to read [{}] value from bean.", idField.getName());
                }
            } else {
                break;
            }
        }
        return matched;
    }

    /**
     * Multiple Id fields supported.
     * 
     * @param clazz
     * @return
     */
    private List<Field> parseIdFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<Field>();
        if (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                Annotation[] annotations = field.getAnnotations();
                if (annotations.length > 0) {
                    if (annotations[0] instanceof EntityId) {
                        fields.add(field);
                    }
                }
            }
            fields.addAll(parseIdFields(clazz.getSuperclass()));
        }
        Collections.sort(fields, new Comparator<Field>() {
            public int compare(Field f1, Field f2) {
                return Integer.compare(f1.getAnnotation(EntityId.class).sequence(),
                        f2.getAnnotation(EntityId.class).sequence());
            }
        });
        return fields;
    }

    public boolean exists(K id) {
        T bean = this.findOne(id);
        return EmptyUtil.isNotEmpty(bean);
    }

    public int count() {
        return this.findAll().size();
    }

    protected abstract List<T> loadRecords();

    public static String startsWith(String prefix) {
        return String.format("%s(%s)", START_WITH, prefix);
    }

}
