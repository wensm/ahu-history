package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class STwoSpeedMotor extends DataEntity {

    private String partNo;
    private int js;
    private double gl;
    private String kwp;
    private String memo;
    @EntityId(sequence = 1)
    private String supplier;
    private String motorBase;
    @AllowNull
    private String inputPower;
    @AllowNull
    private String eff;

}
