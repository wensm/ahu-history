package com.carrier.ahu.metadata;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.carrier.ahu.common.enums.FileType;
import com.carrier.ahu.constant.MetadataConstant;
import com.carrier.ahu.metadata.common.MetadataException;
import com.carrier.ahu.metadata.common.ReportType;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import lombok.extern.slf4j.Slf4j;

/**
 * Report metadata for each type of report.
 * 
 * Created by Braden Zhou on 2018/05/10.
 */
@Slf4j
public class ReportMetadata {

    private static Map<ReportType, String[][]> reportMetadata = new HashMap<>();
    private static Map<String, String[][]> sectionConnectionMetadata = new HashMap<>();
    private static final String REPORT_PARAM_METADATA_PATH = "report/param/";
    private static final String REPORT_TECH_SPEC_METADATA_PATH = "report/techspec/";
    private static final String SECTION_CONNECTION_PREFIX = "section_connection_";

    public static String[][] getReportMetadata(ReportType reportType) {
        String[][] metadata = null;
        if (reportMetadata.containsKey(reportType)) {
            metadata = reportMetadata.get(reportType);
        } else {
            metadata = loadReportMetadata(reportType);
            reportMetadata.put(reportType, metadata);
        }
        return metadata;
    }

    /**
     * Report metadata are two dimensional string array.
     * 
     * @param reportType
     * @return
     */
    private static String[][] loadReportMetadata(ReportType reportType) {
        InputStream inputStream = ReportMetadata.class
                .getResourceAsStream(REPORT_PARAM_METADATA_PATH + reportType.getFileName());
        InputStreamReader fileReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        JsonReader reader = new JsonReader(fileReader);

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(reader, String[][].class);
    }

    /**
     * Return double string array of technique specification of section connection
     * for unit series and unit model.
     * 
     * Metadata file name: section_connection_&lt;series&gt;.xlsx, if for more series metadata, e.g.
     * section_connection_39CQ.xlsx, section_connection_39XT.xlsx, and so on
     * 
     * @param unitSeries '39CQ'
     * @param unitModel '39CQ1234'
     * @return
     */
	public static String[][] getTechSpecSectionConnection(String unitSeries, String unitModel, String paneltype) {
		String templateName = unitModel + paneltype.toLowerCase();
		if (sectionConnectionMetadata.containsKey(templateName)) {
			return sectionConnectionMetadata.get(templateName);
		} else {
			String[][] sectionConnection = loadTechSpecSectionConnection(unitSeries, unitModel, paneltype);
			sectionConnectionMetadata.put(templateName, sectionConnection);
			return sectionConnection;
		}
	}

	private static String[][] loadTechSpecSectionConnection(String unitSeries, String unitModel, String paneltype) {
		String sectionConnectionFile = "";
		if (!MetadataConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//盘管前段连接、正压、立式：section_connection_39CQ_forepart.xlsx
			if (MetadataConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)
					&& MetadataConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)) {//39XT系列没有盘管前段连接
				return null;
			} else {
				sectionConnectionFile = REPORT_TECH_SPEC_METADATA_PATH + SECTION_CONNECTION_PREFIX + unitSeries
						+ MetadataConstant.SYS_PUNCTUATION_LOW_HYPHEN + paneltype.toLowerCase()
						+ FileType.XLSX.extension();
			}
		} else {//标准情况：section_connection_39CQ.xlsx
			sectionConnectionFile = REPORT_TECH_SPEC_METADATA_PATH + SECTION_CONNECTION_PREFIX + unitSeries
					+ FileType.XLSX.extension();
		}

		try (InputStream is = ReportMetadata.class.getResourceAsStream(sectionConnectionFile)) {
			Workbook book = ExcelUtils.read(is, false);
			List<List<String>> list = ExcelUtils.readSheet(book,
					unitModel + (MetadataConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype) ? MetadataConstant.SYS_BLANK
									: paneltype.toLowerCase()),
					0);
			if (EmptyUtil.isNotEmpty(list)) {
				String[][] sectionConnection = new String[list.size()][list.get(0).size()];
				for (int i = 0; i < list.size(); i++) {
					for (int j = 0; j < list.get(0).size(); j++) {
						sectionConnection[i][j] = list.get(i).get(j).toString();
					}
				}
				return sectionConnection;
			}
		} catch (Exception e) {
			log.error("Failed to load section connection file for " + unitModel, e);
			throw new MetadataException("Failed to load section connection file for " + unitModel);
		}
		return null;
	}

}
