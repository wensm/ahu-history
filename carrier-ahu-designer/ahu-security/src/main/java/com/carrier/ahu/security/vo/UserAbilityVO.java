package com.carrier.ahu.security.vo;

/**
 * Abilities of user.
 * 
 * Created by Braden Zhou on 2018/04/22.
 */
public class UserAbilityVO {

    private String subject;
    private boolean inverted;
    private String[] actions;

    public UserAbilityVO(String subject, String... actions) {
        this(subject, false, actions);
    }

    public UserAbilityVO(String subject, boolean inverted, String... actions) {
        this.subject = subject;
        this.inverted = inverted;
        this.actions = actions;
    }

    public String getSubject() {
        return subject;
    }

    public boolean isInverted() {
        return inverted;
    }

    public void setInverted(boolean inverted) {
        this.inverted = inverted;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String[] getActions() {
        return actions;
    }

    public void setActions(String[] actions) {
        this.actions = actions;
    }

}
