package com.carrier.ahu.security.permission;

/**
 * Created by Braden Zhou on 2018/04/25.
 */
public class Ability {

    public static final String RESOURCE_ALL = "*";
    public static final String ALL = "all";

    private String subject;
    private String action;
    private String resource;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

}
