package com.carrier.ahu.security.interceptor;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

import com.carrier.ahu.security.permission.Ability;
import com.carrier.ahu.security.permission.AbilityConfig;

/**
 * Check deny first then allow.
 * 
 * Created by Braden Zhou on 2018/04/26.
 */
@Component
public class AhuAccessDecisionManager implements AccessDecisionManager {

    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
            throws AccessDeniedException, InsufficientAuthenticationException {
        boolean isAllowed = false;
        if (configAttributes != null) {
            // check if the request method is denied for the resource
            Iterator<ConfigAttribute> attributeIterator = configAttributes.iterator();
            String method = ((FilterInvocation) object).getHttpRequest().getMethod();
            while (attributeIterator.hasNext()) {
                AbilityConfig abilityConfig = ((AbilityConfig) attributeIterator.next());
                String attribute = abilityConfig.getAttribute();
                if (abilityConfig.isInverted()) {
                    if (attribute.equalsIgnoreCase(method)) {
                        isAllowed = false;
                        break;
                    }
                } else if (Ability.ALL.equalsIgnoreCase(attribute) || attribute.equalsIgnoreCase(method)) {
                    isAllowed = true;
                    break;
                }
            }
        }
        if (!isAllowed) {
            throw new AccessDeniedException("No Permission");
        }
    }

    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    public boolean supports(Class<?> clazz) {
        return true;
    }

}
