package com.carrier.ahu.security.permission;

/**
 * Created by Braden Zhou on 2018/04/25.
 */
public class RolePermission {

    private String role;
    private Permission permission;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

}
