package com.carrier.ahu.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.carrier.ahu.security.common.SecurityConstants;
import com.carrier.ahu.security.handler.LoginFailureHandler;
import com.carrier.ahu.security.handler.LoginSuccessHandler;
import com.carrier.ahu.security.handler.LogoutSuccessHandler;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
@Configuration
//激活安全保护
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
    /**
     * 加载系统环境变量
     */
    @Autowired
    private Environment env;

    /**
     * 静态资源白名单
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/static/**",//静态资源目录
                "/js/**",//js包
                "/css/**",//css包
                "/vendors/**",
                "/asserts/static/font/**",//字体包
                "/api",
                "/swagger-ui**",
                "/swagger-resources/**",
                "/v2/**",
                "/h2/**",
                "/fonts/**",//字体包
                "/img/**",//图片包
                "/images/**",//图片包
                "/tlds/**",//标签包
                "/webjars/**",//静态资源jar包
                "/**/favicon.ico"//系统icon
        );
    }

    /**
     *  对请求拦截的定义
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().anyRequest().permitAll()
            .and().headers().frameOptions().sameOrigin();
        // @formatter:off
        /**
        http.csrf().disable()//支持跨域请求
                .authorizeRequests()
                .antMatchers("/ajaxLogin")//请求白名单
                .permitAll()
                .anyRequest().authenticated()//所有的请求需要认证
                .and()
                .formLogin()
                .loginPage(SecurityConstants.LOGIN_URL)//认证请求url
                .defaultSuccessUrl(SecurityConstants.LOGIN_SUCCESS_URL)//认证成功请求url
                .failureUrl(SecurityConstants.LOGIN_FAILURE_URL)//失败请求url
                .permitAll()
                .successHandler(loginSuccessHandler())//认证成功回调函数
                .failureHandler(loginFailureHandler())//认证失败回调函数
                .and()
                .logout()
                .logoutSuccessUrl(SecurityConstants.LOGIN_URL)//退出成功请求url
                .permitAll()
                .logoutSuccessHandler(logoutSuccessHandler())//退出成功回调函数
                .invalidateHttpSession(true)//退出成功后清空session
                .clearAuthentication(true)//清空认证对象
                .and()
                .sessionManagement()
                .maximumSessions(1)//一个用户只允许存在一个会话
                .expiredUrl(SecurityConstants.LOGIN_URL)//会话失效跳转请求url
                ;
                */
        // @formatter:on
    }


    /**
     * 用户身份构造器
     * 1,目前是单机应用，仅构造内存中指定的配置授权用户
     * 2,换成b/s架构后，这里需要从数据库拉取用户
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //换成b/s架构后实现以下service
        //auth.userDetailsService()......

        //单机应用
        auth.inMemoryAuthentication()
                .withUser(env.getProperty(SecurityConstants.ACCOUNT_KEY))//account
                .password(env.getProperty(SecurityConstants.PASSWORD_KEY))//password
                .roles(SecurityConstants.DEFAULT_ROLE)//用户角色
        ;
    }

    @Bean
    public LoginSuccessHandler loginSuccessHandler(){
        return new LoginSuccessHandler();
    }

    @Bean
    public LoginFailureHandler loginFailureHandler(){
        return new LoginFailureHandler();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler(){
        return new LogoutSuccessHandler();
    }
}
