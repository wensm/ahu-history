package com.carrier.ahu.security.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.security.permission.Ability;
import com.carrier.ahu.security.permission.PermissionProvider;
import com.carrier.ahu.security.permission.RolePermission;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.security.vo.UserAbilityVO;
import com.carrier.ahu.util.EmptyUtil;

import io.swagger.annotations.Api;

/**
 * 安全控制器 Created by Wen zhengtao on 2017/6/20.
 */
@Api(description = "认证授权接口")
@RestController
public class SecurityController {

	@Autowired
	private AuthenticationManager authenticationManager;

	/**
	 * 登录测试页
	 *
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("login");
		return modelAndView;
	}

	/**
	 * 认证成功
	 *
	 * @return
	 */
	@RequestMapping(value = "success")
	public ApiResult<String> success() {
		return ApiResult.success("login success!");
	}

	/**
	 * 认证失败
	 *
	 * @return
	 */
	@RequestMapping(value = { "failure" })
	public ApiResult<String> failure() {
		return ApiResult.error("login failure!");
	}

	/**
	 * 前端登录接口
	 *
	 * @param request
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "ajaxLogin", method = RequestMethod.POST)
	public ApiResult<User> ajaxLogin(HttpServletRequest request, String username, String password) {
		// 验证账号和密码
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
			return ApiResult.error("username or password can not empty!");
		}
		// 构造账户密码token
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		// 开始认证
		Authentication authentication = authenticationManager.authenticate(token);
		// 存储认证对象
		SecurityContextHolder.getContext().setAuthentication(authentication);
		// 存储会话内容
		HttpSession session = request.getSession();
		session.setAttribute("SPRING_SECURITY_CONTEXT", SecurityContextHolder.getContext());
		// 构造用户对象
		User user = new User();
		user.setUserName(authentication.getName());// 用户名,其他属性在这里扩展
		// 认证成功
		return ApiResult.success(user);
	}

	/**
	 * 前端退出接口
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "ajaxLogout", method = RequestMethod.GET)
	public ApiResult<String> ajaxLogout(HttpServletRequest request, HttpServletResponse response) {
		// 获得当前认证对象
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (EmptyUtil.isNotEmpty(authentication)) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
		}
		// 退出成功
		return ApiResult.success();
	}

    /**
     * Get abilities of user role which assigned in license file.
     * 
     * @return
     */
    @RequestMapping(value = "abilities", method = RequestMethod.GET)
    public ApiResult<List<UserAbilityVO>> getUserRules() {
        List<UserAbilityVO> userRules = new ArrayList<UserAbilityVO>();
        RolePermission rolePermission = PermissionProvider.getRolePermission(LicenseManager.getUserRole());
        if (rolePermission != null) {
            for (Ability deny : rolePermission.getPermission().getDeny()) {
                userRules.add(new UserAbilityVO(deny.getSubject(), true, deny.getAction()));
            }
            for (Ability allow : rolePermission.getPermission().getAllow()) {
                userRules.add(new UserAbilityVO(allow.getSubject(), allow.getAction()));
            }
        }
        return ApiResult.success(userRules);
    }

}
