package com.carrier.ahu.security.permission;

import java.util.List;

/**
 * Created by Braden Zhou on 2018/04/25.
 */
public class Permission {

    private List<Ability> deny;
    private List<Ability> allow;

    public List<Ability> getDeny() {
        return deny;
    }

    public void setDeny(List<Ability> deny) {
        this.deny = deny;
    }

    public List<Ability> getAllow() {
        return allow;
    }

    public void setAllow(List<Ability> allow) {
        this.allow = allow;
    }

}
