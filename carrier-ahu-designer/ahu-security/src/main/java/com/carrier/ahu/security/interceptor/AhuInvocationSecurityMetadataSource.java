package com.carrier.ahu.security.interceptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.security.permission.Ability;
import com.carrier.ahu.security.permission.AbilityConfig;
import com.carrier.ahu.security.permission.PermissionProvider;
import com.carrier.ahu.security.permission.RolePermission;

/**
 * Role permission is from license file.
 * 
 * Created by Braden Zhou on 2018/04/25.
 */
@Component
public class AhuInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private static RolePermission rolePermission;

    public AhuInvocationSecurityMetadataSource() {
        rolePermission = PermissionProvider.getRolePermission(LicenseManager.getUserRole());
    }

    /**
     * Return null to ignore.
     */
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        if (rolePermission != null) {
            FilterInvocation invocation = ((FilterInvocation) object);
            // String url = ((FilterInvocation) object).getRequestUrl();
            // System.out.println(url);

            Collection<ConfigAttribute> cattributes = new ArrayList<>();
            parseAbilities(invocation.getHttpRequest(), rolePermission.getPermission().getDeny(), cattributes, true);
            parseAbilities(invocation.getHttpRequest(), rolePermission.getPermission().getAllow(), cattributes, false);

            return cattributes.isEmpty() ? null : cattributes;
        }
        return null;
    }

    private void parseAbilities(HttpServletRequest httpRequest, List<Ability> abilities,
            Collection<ConfigAttribute> configAttributes, boolean inverted) {
        for (Ability ability : abilities) {
            if (Ability.RESOURCE_ALL.equals(ability.getResource())) {
                ConfigAttribute attribute = new AbilityConfig(ability.getAction(), inverted);
                configAttributes.add(attribute);
            } else {
                RequestMatcher matcher = new AntPathRequestMatcher(ability.getResource());
                if (matcher.matches(httpRequest)) {
                    ConfigAttribute attribute = new AbilityConfig(ability.getAction(), inverted);
                    configAttributes.add(attribute);
                }
            }
        }
    }

    public boolean supports(Class<?> clazz) {
        return true;
    }

    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return new ArrayList<ConfigAttribute>();
    }

}
