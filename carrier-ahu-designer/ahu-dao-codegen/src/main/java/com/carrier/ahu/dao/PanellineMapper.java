package com.carrier.ahu.dao;

import com.carrier.ahu.po.Panelline;

public interface PanellineMapper {
    int insert(Panelline record);

    int insertSelective(Panelline record);
}