package com.carrier.ahu.dao;

import com.carrier.ahu.po.Mixed4a;

public interface Mixed4aMapper {
    int insert(Mixed4a record);

    int insertSelective(Mixed4a record);
}