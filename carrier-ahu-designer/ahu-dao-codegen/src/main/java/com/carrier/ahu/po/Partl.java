package com.carrier.ahu.po;

public class Partl {
    private String pid;

    private String partid;

    private Double sindrybulbt;

    private Double sinwetbulbt;

    private Double sinrelativet;

    private Double windrybulbt;

    private Double winwetbulbt;

    private Double winrelativet;

    private Double soutdrybulbt;

    private Double soutwetbulbt;

    private Double soutrelativet;

    private Double woutdrybulbt;

    private Double woutwetbulbt;

    private Double woutrelativet;

    private Double snewdrybulbt;

    private Double snewwetbulbt;

    private Double snewrelativet;

    private Double wnewdrybulbt;

    private Double wnewwetbulbt;

    private Double wnewrelativet;

    private Double navolume;

    private String ainterface;

    private Double ainterfacer;

    private String osdoor;

    private String isdoor;

    private Double rresistance;

    private Double iresistance;

    private Integer sectionl;

    private Double weight;

    private Double price;

    private Double resistance;

    private String nonstandard;

    private String season;

    private String memo;

    private String actuator;

    private String valvem;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public Double getSindrybulbt() {
        return sindrybulbt;
    }

    public void setSindrybulbt(Double sindrybulbt) {
        this.sindrybulbt = sindrybulbt;
    }

    public Double getSinwetbulbt() {
        return sinwetbulbt;
    }

    public void setSinwetbulbt(Double sinwetbulbt) {
        this.sinwetbulbt = sinwetbulbt;
    }

    public Double getSinrelativet() {
        return sinrelativet;
    }

    public void setSinrelativet(Double sinrelativet) {
        this.sinrelativet = sinrelativet;
    }

    public Double getWindrybulbt() {
        return windrybulbt;
    }

    public void setWindrybulbt(Double windrybulbt) {
        this.windrybulbt = windrybulbt;
    }

    public Double getWinwetbulbt() {
        return winwetbulbt;
    }

    public void setWinwetbulbt(Double winwetbulbt) {
        this.winwetbulbt = winwetbulbt;
    }

    public Double getWinrelativet() {
        return winrelativet;
    }

    public void setWinrelativet(Double winrelativet) {
        this.winrelativet = winrelativet;
    }

    public Double getSoutdrybulbt() {
        return soutdrybulbt;
    }

    public void setSoutdrybulbt(Double soutdrybulbt) {
        this.soutdrybulbt = soutdrybulbt;
    }

    public Double getSoutwetbulbt() {
        return soutwetbulbt;
    }

    public void setSoutwetbulbt(Double soutwetbulbt) {
        this.soutwetbulbt = soutwetbulbt;
    }

    public Double getSoutrelativet() {
        return soutrelativet;
    }

    public void setSoutrelativet(Double soutrelativet) {
        this.soutrelativet = soutrelativet;
    }

    public Double getWoutdrybulbt() {
        return woutdrybulbt;
    }

    public void setWoutdrybulbt(Double woutdrybulbt) {
        this.woutdrybulbt = woutdrybulbt;
    }

    public Double getWoutwetbulbt() {
        return woutwetbulbt;
    }

    public void setWoutwetbulbt(Double woutwetbulbt) {
        this.woutwetbulbt = woutwetbulbt;
    }

    public Double getWoutrelativet() {
        return woutrelativet;
    }

    public void setWoutrelativet(Double woutrelativet) {
        this.woutrelativet = woutrelativet;
    }

    public Double getSnewdrybulbt() {
        return snewdrybulbt;
    }

    public void setSnewdrybulbt(Double snewdrybulbt) {
        this.snewdrybulbt = snewdrybulbt;
    }

    public Double getSnewwetbulbt() {
        return snewwetbulbt;
    }

    public void setSnewwetbulbt(Double snewwetbulbt) {
        this.snewwetbulbt = snewwetbulbt;
    }

    public Double getSnewrelativet() {
        return snewrelativet;
    }

    public void setSnewrelativet(Double snewrelativet) {
        this.snewrelativet = snewrelativet;
    }

    public Double getWnewdrybulbt() {
        return wnewdrybulbt;
    }

    public void setWnewdrybulbt(Double wnewdrybulbt) {
        this.wnewdrybulbt = wnewdrybulbt;
    }

    public Double getWnewwetbulbt() {
        return wnewwetbulbt;
    }

    public void setWnewwetbulbt(Double wnewwetbulbt) {
        this.wnewwetbulbt = wnewwetbulbt;
    }

    public Double getWnewrelativet() {
        return wnewrelativet;
    }

    public void setWnewrelativet(Double wnewrelativet) {
        this.wnewrelativet = wnewrelativet;
    }

    public Double getNavolume() {
        return navolume;
    }

    public void setNavolume(Double navolume) {
        this.navolume = navolume;
    }

    public String getAinterface() {
        return ainterface;
    }

    public void setAinterface(String ainterface) {
        this.ainterface = ainterface == null ? null : ainterface.trim();
    }

    public Double getAinterfacer() {
        return ainterfacer;
    }

    public void setAinterfacer(Double ainterfacer) {
        this.ainterfacer = ainterfacer;
    }

    public String getOsdoor() {
        return osdoor;
    }

    public void setOsdoor(String osdoor) {
        this.osdoor = osdoor == null ? null : osdoor.trim();
    }

    public String getIsdoor() {
        return isdoor;
    }

    public void setIsdoor(String isdoor) {
        this.isdoor = isdoor == null ? null : isdoor.trim();
    }

    public Double getRresistance() {
        return rresistance;
    }

    public void setRresistance(Double rresistance) {
        this.rresistance = rresistance;
    }

    public Double getIresistance() {
        return iresistance;
    }

    public void setIresistance(Double iresistance) {
        this.iresistance = iresistance;
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard == null ? null : nonstandard.trim();
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getActuator() {
        return actuator;
    }

    public void setActuator(String actuator) {
        this.actuator = actuator == null ? null : actuator.trim();
    }

    public String getValvem() {
        return valvem;
    }

    public void setValvem(String valvem) {
        this.valvem = valvem == null ? null : valvem.trim();
    }
}