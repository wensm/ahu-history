package com.carrier.ahu.dao;

import com.carrier.ahu.po.Combinedfilter4c;

public interface Combinedfilter4cMapper {
    int insert(Combinedfilter4c record);

    int insertSelective(Combinedfilter4c record);
}