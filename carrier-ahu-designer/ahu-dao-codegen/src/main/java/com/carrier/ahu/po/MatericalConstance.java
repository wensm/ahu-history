/**
 * Title: MatericalConstance <br>
 * @Description:[] <br>
 * @date 2017-06-22 <br>
 * Copyright (c) 2016 ZXZL YCT.  <br>
 * 
 * @author zhiqiang1.zhang@dhc.com.cn
 * @version V1.0
 */
package com.carrier.ahu.po;

/**
 * @Description: 本类为自动生成，对应数据库表名： matericalconstance <br>
 * @date 2017-06-22 16:37 <br>
 * 
 * @author taosy@adinnet.cn
 * @version V1.0
 */
public class MatericalConstance {

	/**
	 * [db_column:pid]
	 */
	private String pid;
	/**
	 * [db_column:projid]
	 */
	private String projid;
	/**
	 * [db_column:groupid]
	 */
	private String groupid;
	/**
	 * [db_column:prokey]
	 */
	private String prokey;

	/**
	 * Getter :
	 */
	public String getPid() {
		return pid;
	}

	/**
	 * Setter : [db_column:pid]
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}

	/**
	 * Getter :
	 */
	public String getProjid() {
		return projid;
	}

	/**
	 * Setter : [db_column:projid]
	 */
	public void setProjid(String projid) {
		this.projid = projid;
	}

	/**
	 * Getter :
	 */
	public String getGroupid() {
		return groupid;
	}

	/**
	 * Setter : [db_column:groupid]
	 */
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	/**
	 * Getter :
	 */
	public String getProkey() {
		return prokey;
	}

	/**
	 * Setter : [db_column:prokey]
	 */
	public void setProkey(String prokey) {
		this.prokey = prokey;
	}
}