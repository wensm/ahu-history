package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partj;

public interface PartjMapper {
    int insert(Partj record);

    int insertSelective(Partj record);
}