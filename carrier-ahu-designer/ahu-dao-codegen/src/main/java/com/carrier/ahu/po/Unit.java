package com.carrier.ahu.po;

public class Unit {
    private String unitid;

    private String pid;

    private String series;

    private String product;

    private Short mount;

    private String name;

    private Double weight;

    private Double price;

    private String modifiedno;

    private String paneltype;

    private Double nsprice;

    private Double cost;

    private Double lb;

    private byte[] drawing;

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series == null ? null : series.trim();
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public Short getMount() {
        return mount;
    }

    public void setMount(Short mount) {
        this.mount = mount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getModifiedno() {
        return modifiedno;
    }

    public void setModifiedno(String modifiedno) {
        this.modifiedno = modifiedno == null ? null : modifiedno.trim();
    }

    public String getPaneltype() {
        return paneltype;
    }

    public void setPaneltype(String paneltype) {
        this.paneltype = paneltype == null ? null : paneltype.trim();
    }

    public Double getNsprice() {
        return nsprice;
    }

    public void setNsprice(Double nsprice) {
        this.nsprice = nsprice;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getLb() {
        return lb;
    }

    public void setLb(Double lb) {
        this.lb = lb;
    }

    public byte[] getDrawing() {
        return drawing;
    }

    public void setDrawing(byte[] drawing) {
        this.drawing = drawing;
    }
}