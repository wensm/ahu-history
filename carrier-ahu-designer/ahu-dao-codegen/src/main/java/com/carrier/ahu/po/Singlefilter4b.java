package com.carrier.ahu.po;

public class Singlefilter4b {
    private String pid;

    private String partid;

    private String filterf;

    private String materiale;

    private String rimmaterial;

    private String rimthick;

    private String plankm;

    private String framem;

    private String supplier;

    private Integer sectionl;

    private Double resistance;

    private String resiflag;

    private Double weight;

    private Double price;

    private String nostandard;

    private String memo;

    private String nosfilter;

    private Double beginresi;

    private Double averresi;

    private Double endresi;

    private Boolean ycjchk;

    private String ycjtype;

    private String loading;

    private String standard;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getFilterf() {
        return filterf;
    }

    public void setFilterf(String filterf) {
        this.filterf = filterf == null ? null : filterf.trim();
    }

    public String getMateriale() {
        return materiale;
    }

    public void setMateriale(String materiale) {
        this.materiale = materiale == null ? null : materiale.trim();
    }

    public String getRimmaterial() {
        return rimmaterial;
    }

    public void setRimmaterial(String rimmaterial) {
        this.rimmaterial = rimmaterial == null ? null : rimmaterial.trim();
    }

    public String getRimthick() {
        return rimthick;
    }

    public void setRimthick(String rimthick) {
        this.rimthick = rimthick == null ? null : rimthick.trim();
    }

    public String getPlankm() {
        return plankm;
    }

    public void setPlankm(String plankm) {
        this.plankm = plankm == null ? null : plankm.trim();
    }

    public String getFramem() {
        return framem;
    }

    public void setFramem(String framem) {
        this.framem = framem == null ? null : framem.trim();
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier == null ? null : supplier.trim();
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public String getResiflag() {
        return resiflag;
    }

    public void setResiflag(String resiflag) {
        this.resiflag = resiflag == null ? null : resiflag.trim();
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNostandard() {
        return nostandard;
    }

    public void setNostandard(String nostandard) {
        this.nostandard = nostandard == null ? null : nostandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getNosfilter() {
        return nosfilter;
    }

    public void setNosfilter(String nosfilter) {
        this.nosfilter = nosfilter == null ? null : nosfilter.trim();
    }

    public Double getBeginresi() {
        return beginresi;
    }

    public void setBeginresi(Double beginresi) {
        this.beginresi = beginresi;
    }

    public Double getAverresi() {
        return averresi;
    }

    public void setAverresi(Double averresi) {
        this.averresi = averresi;
    }

    public Double getEndresi() {
        return endresi;
    }

    public void setEndresi(Double endresi) {
        this.endresi = endresi;
    }

    public Boolean getYcjchk() {
        return ycjchk;
    }

    public void setYcjchk(Boolean ycjchk) {
        this.ycjchk = ycjchk;
    }

    public String getYcjtype() {
        return ycjtype;
    }

    public void setYcjtype(String ycjtype) {
        this.ycjtype = ycjtype == null ? null : ycjtype.trim();
    }

    public String getLoading() {
        return loading;
    }

    public void setLoading(String loading) {
        this.loading = loading == null ? null : loading.trim();
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard == null ? null : standard.trim();
    }
}