package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partl;

public interface PartlMapper {
    int insert(Partl record);

    int insertSelective(Partl record);
}