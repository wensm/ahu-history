import $http from '../misc/$http'

export const PRO_RECEIVE_PROJECTS = 'PRO_RECEIVE_PROJECTS'
export const PRO_SET_CURRENT_PROJECT_ID = 'PRO_SET_CURRENT_PROJECT_ID'

import {onProjectsPageFilterUpdate} from '../actions/projects'

export function deleteProject(projectId) {
  return dispatch => $http.post(`/project/delete/${projectId}`).then(data => dispatch(fetchProjects()))
}

export function setCurrentProjectId(projectId) {
  return {
    type: PRO_SET_CURRENT_PROJECT_ID,
    projectId
  }
}

export function saveProject(project) {
  return dispatch => $http.post(`project/save`, project).then(data => dispatch(fetchProjects()))
}

export function fetchProjects() {
  return dispatch => $http.get(`project/list`).then(data => dispatch(receiveProjects(data.data)))
}

function receiveProjects(projects) {
  return {
    type: PRO_RECEIVE_PROJECTS,
    projects
  }
}
