import {
    SIDE_PANEL,
    SIDE_OPERATION_PANEL,
    TERM_PANEL,
    RIGHT_PANEL,
    TOP_PANEL,
    BOTTOM_PANEL,
    MERGE_DIVISION_FORWARD,
    SPLIT_DIVISION,
    SAVE_SUCCESS,
    SAVED_SPLIT_SECTION_CONFIG,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import $http from '../misc/$http'
import NProgress from 'nprogress'
import sweetalert from 'sweetalert'
import { isTwinLine, dismiss } from "./ahu"
import DateUtils from '../misc/DateUtils'
import { isEmpty } from 'lodash'

export const PARTITION_RECEIVE_AHU_SECTIONS = 'PARTITION_RECEIVE_AHU_SECTIONS'
export const PARTITION_PANEL_DEVIDED = 'PARTITION_PANEL_DEVIDED'
export const PARTITION_PANEL_DELETED = 'PARTITION_PANEL_DELETED'
export const PARTITION_PANEL_SELECTED = 'PARTITION_PANEL_SELECTED'
export const PARTITION_PANEL_RECT_SELECTED = 'PARTITION_PANEL_RECT_SELECTED'
export const PARTITION_PANEL_RECT_UPDATED = 'PARTITION_PANEL_RECT_UPDATED'
export const PARTITION_PANEL_TYPE_UPDATED = 'PARTITION_PANEL_TYPE_UPDATED'
export const PARTITION_PANEL_LINE_UPDATED = 'PARTITION_PANEL_LINE_UPDATED'
export const PARTITION_FACE_SWITCHED = 'PARTITION_FACE_SWITCHED'
export const PARTITION_PANEL_RECT_UPDATE_CONFIRM = 'PARTITION_PANEL_RECT_UPDATE_CONFIRM'
export const PARTITION_SELECTED = 'PARTITION_SELECTED'
export const PARTITION_PANEL_BACKWARD = 'PARTITION_PANEL_BACKWARD'
export const PARTITION_PANEL_FORWARD = 'PARTITION_PANEL_FORWARD'
export const PARTITION_PANEL_SPLIT = 'PARTITION_PANEL_SPLIT'
export const PARTITION_PARTITIONS_SAVED = 'PARTITION_PARTITIONS_SAVED'
export const PARTITION_TIP = 'PARTITION_TIP'
export const PARTITION_RESETTIP = 'PARTITION_RESETTIP'
export const PARTITION_TIP_SAVE = 'PARTITION_TIP_SAVE'
export const PARTITION_PANEL_CLEAN = 'PARTITION_PANEL_CLEAN'
export const PARTITION_CHANGEBASE = 'PARTITION_CHANGEBASE'
export const PARTITION_SPIN = 'PARTITION_SPIN'
export const PARTITION_PANEL_LOCKING = 'PARTITION_PANEL_LOCKING'
export const PARTITION_PANEL_MERGES = 'PARTITION_PANEL_MERGES'
export const PARTITION_SYNCAANDB = 'PARTITION_SYNCAANDB'

export function fetchAhuSections(ahuId) {
    return dispatch => {
        return $http.get(`section/list?ahuId=${ahuId}`).then(data => {
            dispatch(receiveAhuSections(data.data))
        })
    }
}

function receiveAhuSections(sections) {
    return {
        type: PARTITION_RECEIVE_AHU_SECTIONS,
        sections
    }
}

export function generatePanelRect(panel, parentPanel, ratio, gap) {

    let width, height;
    if (!parentPanel) {
        const minSize = 50
        const maxSize = 240
        width = 180 + 'px';
        if (panel.id == '4' || panel.id == '5' || Number(panel.id) > 5) {
            // width = 120 + 'px';
        }
        // height = Math.min(Math.max(panel.panelWidth / panel.panelLength * 180, minSize), maxSize) + 'px';
        height = 140 + 'px'
        if (panel.id == '4' || panel.id == '5' || Number(panel.id) > 5) {
            // height = Math.min(Math.max(panel.panelWidth / panel.panelLength * 120, minSize), maxSize) + 'px';
        }

    } else {
        if (parentPanel.direction === 2) {
            width = 100 * panel.panelLength / parentPanel.panelLength + '%';
            height = '100%';
        } else if (parentPanel.direction === 1) {
            width = '100%';
            height = 100 * panel.panelWidth / parentPanel.panelWidth + '%';
        }
    }
    // console.log('width',width,height,parentPanel)
    return {
        id: panel.id,
        width: width,
        height: height,
        top: 0,
        left: 0,
        panel: panel,
        ratio: ratio
    }
}

function generateRect(rectArr, panel, top, left, ratio) {
    let subPanels = panel.subPanels;
    if (panel.direction == 0 || !subPanels || subPanels.length != 2) {
        rectArr.push({
            id: panel.id,
            width: panel.panelLength / ratio + 'px',
            height: panel.panelWidth / ratio * 100 + 'px',
            top: top + 'px',
            left: left + 'px',
            panel: panel,
            ratio: ratio
        });
    } else {
        generateRect(rectArr, subPanels[0], top, left, ratio);
        if (panel.direction == 1) {
            generateRect(rectArr, subPanels[1], top, left, ratio);
        } else {
            generateRect(rectArr, subPanels[1], top, left, ratio);
        }
    }
}

export function translatePanelForRender(panel, ratio) {
    var rectArray = [];
    if (panel) {
        generateRect(rectArray, panel, 0, 0, ratio);
    }
    return rectArray;

}
function findSubpanel(id, rootPanel) {
    let findResult = false

    rootPanel.subPanels && rootPanel.subPanels.forEach((sub, index) => {
        if (sub.id == id) {
            findResult = true
        }
    })
    if (findResult) {
        // return rootPanel
        // rootPanel.subPanels = ''
        delete rootPanel.subPanels
    } else {
        if (rootPanel.subPanels) {
            findSubpanel(id, rootPanel.subPanels[0])
            findSubpanel(id, rootPanel.subPanels[1])
        }
    }
}
function findSubpanel2(id, rootPanel) {
    // console.log('findSubpanel2', id,  rootPanel)
    let findResult = false
    let reduceLength = ''

    rootPanel.subPanels && rootPanel.subPanels.forEach((sub, index) => {
        if (sub.id == id) {
            findResult = true
            reduceLength = sub.panelLength
        }
    })
    if (findResult) {
        // return rootPanel
        // rootPanel.subPanels = ''
        rootPanel.panelLength = rootPanel.panelLength - reduceLength
        window.reduceLength = reduceLength

        // console.log('jianhoudekuandu', rootPanel)
        delete rootPanel.subPanels
    } else {
        if (rootPanel.subPanels) {
            let bo = true
            let ke = undefined
            rootPanel.id.split('_').forEach((ele, index)=>{
                if(ele != id.split('_')[index]){
                    bo = false
                    ke = ele
                }
            })
            if(ke == undefined){
                findSubpanel2(id, rootPanel.subPanels[0])
                findSubpanel2(id, rootPanel.subPanels[1])                
            }
            if(ke == 1){
                findSubpanel2(id, rootPanel.subPanels[0])
            }else if(ke == 0){
                findSubpanel2(id, rootPanel.subPanels[1])
            }
        }
    }
}
function findSubpanel3(id, rootPanel) {
    let findResult = false
    // let plusLength = ''
    // window.plusWidth = plusWidth
    rootPanel.subPanels && rootPanel.subPanels.forEach((sub, index) => {
        if (sub.id == id) {
            findResult = true
            sub.panelLength = sub.panelLength + window.reduceLength
        }
    })
    if (findResult) {
        // return rootPanel
        // rootPanel.subPanels = ''
        rootPanel.panelLength = rootPanel.panelLength + window.reduceLength
        // console.log('jianhoudekuandu', rootPanel)
        // delete rootPanel.subPanels
    } else {
        if (rootPanel.subPanels) {

            let bo = true
            let ke = undefined
            rootPanel.id.split('_').forEach((ele, index)=>{
                if(ele != id.split('_')[index]){
                    bo = false
                    ke = ele
                }
            })
            if(ke == undefined){
                findSubpanel3(id, rootPanel.subPanels[0])
                findSubpanel3(id, rootPanel.subPanels[1])                
            }
            if(ke == 1){
                findSubpanel3(id, rootPanel.subPanels[0])
            }else if(ke == 0){
                findSubpanel3(id, rootPanel.subPanels[1])
            }

        }
    }
}
function reduceParentPanel(lockingPanel, thisPanel, rootPanel) {//第一个参数为需要递减的面
    let lockingPanelId = lockingPanel.id
    let lockingIdSplits = lockingPanelId.split('_')
    let thisPanelId = thisPanel.id
    let thisPanelIdSplits = thisPanelId.split('_')
    idSplits.forEach((idSplit, index)=>{
        if(index == 0 ){
            //不操作
        }else if(index > 0 ){

        }
    })

}
function plusParentPanel(thisPanel, lockingPanel, rootPanel) {//第一个参数为需要递增的面
    let thisPanelId = thisPanel.id
}
export function doMergePanle(thisPanel, lockingPanel, rootPanel) {
    let thisPId = thisPanel.id.substr(0, thisPanel.id.length - 2)
    let lockingPId = lockingPanel.id.substr(0, lockingPanel.id.length - 2)
    if (thisPId == lockingPId) {//父级相同
        findSubpanel(thisPanel.id, rootPanel)//找到合并的父面，删掉里面的子面
    } else {//锁定面父级，层层递减；合并面父级，层层递增
        let panel1Id = thisPanel.id
        let panel1Split = panel1Id.split('_')
        let panel2Id = lockingPanel.id
        let panel2Split = panel2Id.split('_')
        
        if( panel1Split.length == panel2Split.length ){//锁定面和合并面层级相同
        findSubpanel2(lockingPanel.id, rootPanel)//锁定面找到父级，减少长度
        findSubpanel3(thisPanel.id, rootPanel)//合并面增加长度。父级增加长度
            
        }else{//锁定面和合并面层级不同
            if( panel1Split.length > panel2Split.length){
                window.differentParentReduceLength = thisPanel.length//全局变量需要递减的长度，不支持宽度

                reduceParentPanel(thisPanel, lockingPanel, rootPanel,  )//锁定面递减
                plusParentPanel(lockingPanel, thisPanel, rootPanel, )//合并面递增
            }else{
                reduceParentPanel(lockingPanel, thisPanel, rootPanel,  )//合并面递减
                plusParentPanel(thisPanel, lockingPanel, rootPanel, )//锁定面递增
            }
            
        }
    }
    let returnObj = {
        lockingPanel2: '',
        rootPanel2: rootPanel,
    }

    return returnObj



}
export function devidePanel(panel, subPanelId, direction, product, percent) {
    // panel = JSON.parse(JSON.stringify(panel))
    let panelToDevide = innerSearchToDevide(panel, subPanelId)
    // console.log(panelToDevide)
    innerDoDividePanel(panelToDevide, direction, product, percent)
    // console.log("Panel after deviding..." + subPanelId)
    return {
        panel: panel,
        dividePanel: panelToDevide
    };
}
function getPanelConnectorArr(panelConnector, direction) {
    //panelConnector 为 undefined时，表示当前切的是第一块面板，直接赋予初始值
    //横切，当前面左边参数为0，则切后上段为继承，下段左边为A，右边继承右边
    //横切，当前面左边为A，则切后上下段均为继承
    //纵切，当前面右边参数为0，则切后左段为继承，右段左边继承左边，右边为A，
    //纵切，当前面右边为A，则切后左右段均为继承
    let arr = ['00', '0A']

    //特殊情况，风机底面板为加强面板，多一个字母，比如：0AF
    if (panelConnector && panelConnector.split('').length == 3) {
        if (direction == 1) {//横切
            let parleft = panelConnector.split('')[0]
            let parright = panelConnector.split('')[1]

            if (parleft == 'A') {
                arr[0] = panelConnector;
                arr[1] = panelConnector;
            } else if (parleft == '0') {
                arr[0] = panelConnector;
                arr[1] = `A${panelConnector.split('')[1]}F`
            }
        } else if (direction == 2) {//纵切
            let parleft = panelConnector.split('')[0]
            let parright = panelConnector.split('')[1]

            if (parright == 'A') {
                arr[0] = panelConnector;
                arr[1] = panelConnector;
            } else if (parright == '0') {
                arr[0] = panelConnector;
                arr[1] = `${panelConnector.split('')[0]}AF`
            }
        }
        return arr
    }


    //普通切割
    if (direction == 1 && panelConnector) {//横切
        let parleft = panelConnector.split('')[0]
        let parright = panelConnector.split('')[1]

        if (parleft == 'A') {
            arr[0] = panelConnector;
            arr[1] = panelConnector;
        } else if (parleft == '0') {
            arr[0] = panelConnector;
            arr[1] = `A${panelConnector.split('')[1]}`
        }
    } else if (direction == 2 && panelConnector) {//纵切
        let parleft = panelConnector.split('')[0]
        let parright = panelConnector.split('')[1]

        if (parright == 'A') {
            arr[0] = panelConnector;
            arr[1] = panelConnector;
        } else if (parright == '0') {
            arr[0] = panelConnector;
            arr[1] = `${panelConnector.split('')[0]}A`
        }

    }
    return arr
}
function innerDoDividePanel(panel, direction, product, percent) {//zzf切割面板,product产品
    //    console.log("innerDoDividePanel", panel, direction)
    let width = panel.panelWidth;
    let length = panel.panelLength;
    let panelTypeLeft = panel.panelType != undefined ? panel.panelType : ''
    let panelTypeRight = panel.panelType != undefined ? panel.panelType : ''
    if (panel.panelType == '2') {//门面板横切后，下面为门面板，上面为普通面板,下面，右面继承。剩余面板为普通面板
        panelTypeLeft = '0'
    }
    let subPanels = [];
    let panelConnectorArr = getPanelConnectorArr(panel.panelConnector, direction)
    if (direction == 1) {//横
        // let newWidth = Math.floor(width / 2)
        let newWidth = Math.round(width * percent)
        //        console.log('width',width,percent,newWidth)

        if (newWidth == 0) {
            return panel;
        }
        let panel0 = {
            direction: 0,
            panelType: panelTypeLeft,
            panelWidth: newWidth,
            panelLength: length,
            id: panel.id + "_0",
            panelConnector: product == '39CQ' ? panelConnectorArr[0] : ''
        }
        let panel1 = {
            direction: 0,
            panelType: panelTypeRight,
            panelWidth: width - newWidth,
            panelLength: length,
            id: panel.id + "_1",
            panelConnector: product == '39CQ' ? panelConnectorArr[1] : ''

        }
        if(panel.sectionMetaId){
            panel0.sectionMetaId = panel.sectionMetaId
            panel1.sectionMetaId = panel.sectionMetaId
        }
        subPanels.push(panel0);
        subPanels.push(panel1);
    } else if (direction == 2) {//竖

        // let newLength = Math.round(length / 2)
        let newLength = Math.round(length * percent)
        //        console.log('length',length,percent,newLength)
        let panel0 = {
            direction: 0,
            panelType: panelTypeLeft,
            panelWidth: width,
            panelLength: newLength,
            id: panel.id + "_0",
            panelConnector: product == '39CQ' ? panelConnectorArr[0] : ''

        }
        let panel1 = {
            direction: 0,
            panelType: panelTypeRight,
            panelWidth: width,
            panelLength: length - newLength,
            id: panel.id + "_1",
            panelConnector: product == '39CQ' ? panelConnectorArr[1] : ''

        }
        if(panel.sectionMetaId){
            panel0.sectionMetaId = panel.sectionMetaId
            panel1.sectionMetaId = panel.sectionMetaId
        }
        subPanels.push(panel0);
        subPanels.push(panel1);
    } else {
        return panel;
    }
    panel.direction = direction;
    panel.subPanels = subPanels;
    // 需要在当前层加框
    let panelConnectorArr2 = panel.panelConnector.split('')
    if (panelConnectorArr2.length == 0) {//未切过的面无00属性
        panelConnectorArr2 = ['0', '0']
    }
    if (direction == 1) {//横切
        panel.frameLine = {
            type: 0,
            lineConnector: panelConnectorArr2[1],
            lineLength: panel.panelLength
        }
    } else if (direction == 2) {//竖切
        panel.frameLine = {
            type: 0,
            lineConnector: panelConnectorArr2[0],
            lineLength: panel.panelWidth
        }
    }

    return panel;
}

export function deleteSubPanel(panel, subPanelId) {
    panel = JSON.parse(JSON.stringify(panel))

    if (panel.id == subPanelId) {//It is root panel
        return panel;
    } else {
        let panelToDel = innerSearchToDeleteChild(panel, subPanelId)
        innerDoDeleteChild(panelToDel)
    }
    return panel;
}

//Clean the panel's children and update the direction
function innerDoDeleteChild(parentPanel) {
    parentPanel.direction = 0;
    parentPanel.subPanels = null;
}
function innerSearchToDeleteChild(panel, subPanelId) {
    if (panel.subPanels) {
        if (subPanelId == panel.subPanels[0].id) {
            return panel;
        } else if (subPanelId.indexOf(panel.subPanels[0].id) == 0) {
            return innerSearchToDeleteChild(panel.subPanels[0], subPanelId)
        } else {
            if (subPanelId == panel.subPanels[1].id) {
                return panel;
            } else if (subPanelId.indexOf(panel.subPanels[1].id) == 0) {
                return innerSearchToDeleteChild(panel.subPanels[1], subPanelId)
            }
        }
    }
}
export function innerSearchToDevide(panel, subPanelId) {
    // console.log('zzfpfront', panel, subPanelId)
    if (panel.id == subPanelId) {
        return panel;
    }

    let indexList = subPanelId.split("_").slice(1);
    let dPanel = panel;
    for (let i = 0; i < indexList.length; i++) {
        dPanel = dPanel.subPanels && dPanel.subPanels[indexList[i]]
        if (!dPanel) {
            return dPanel;
        }
    }
    // console.log('zzfpfront2', dPanel)
    return dPanel;

    // if (panel.subPanels) {
    //     if (subPanelId == panel.subPanels[0].id) {
    //         return panel.subPanels[0];
    //     } else if (subPanelId.indexOf(panel.subPanels[0].id) == 0) {
    //         return innerSearchToDevide(panel.subPanels[0], subPanelId)
    //     } else {
    //         if (subPanelId == panel.subPanels[1].id) {
    //             return panel.subPanels[1];
    //         } else if (subPanelId.indexOf(panel.subPanels[1].id) == 0) {
    //             return innerSearchToDevide(panel.subPanels[1], subPanelId)
    //         }
    //     }
    // }
}

export function searchParentPanel(panel, subPanelId) {
    if (panel.id == subPanelId) {
        return {};
    }
    if (panel.subPanels) {
        if (subPanelId == panel.subPanels[0].id) {
            return panel;
        } else if (subPanelId.indexOf(panel.subPanels[0].id) == 0) {
            return searchParentPanel(panel.subPanels[0], subPanelId)
        } else {
            if (subPanelId == panel.subPanels[1].id) {
                return panel;
            } else if (subPanelId.indexOf(panel.subPanels[1].id) == 0) {
                return searchParentPanel(panel.subPanels[1], subPanelId)
            }
        }
    }
}
export function calcSubPanelIndexes(panel, lastIndex) {
    // console.log("==calcSubPanelIndexes==",panel.id)
    if (!lastIndex) {
        lastIndex = 0
    }
    if (panel) {
        if (panel.subPanels && panel.subPanels.length > 0) {
            panel.subPanels.forEach((sp) => {
                lastIndex = calcSubPanelIndexes(sp, lastIndex)
            })
        } else {
            lastIndex++;
            panel.subPanelIndex = lastIndex
        }
    }
    return lastIndex
}

function changeUpdateToSubPanels(panel, parentPanel) {

    if (panel.subPanels && panel.subPanels.length) {
        // console.log("changeUpdateToSubPanels")
        // console.log(panel)
        // console.log(parentPanel)

        if (parentPanel.direction === panel.direction) {
            // console.log("父子面板的方向一样 panel:" + panel.id + ",parentPanel:" + parentPanel.id)
            if (panel.direction === 1) {
                panel.subPanels.forEach((sp) => {
                    sp.panelLength = panel.panelLength
                    changeUpdateToSubPanels(sp, panel)
                })
                // console.log("子面板的length都更新")
            } else {
                panel.subPanels.forEach((sp) => {
                    sp.panelWidth = panel.panelWidth
                    changeUpdateToSubPanels(sp, panel)
                })
                // console.log("子面板的width都更新")
            }
        } else {
            // console.log("父子面板的方向不一样 panel:" + panel.id + ",parentPanel:" + parentPanel.id)
            if (panel.direction === 2) {

                panel.subPanels[1].panelLength = panel.panelLength - panel.subPanels[0].panelLength
                panel.subPanels.forEach((sp) => {
                    sp.panelWidth = panel.panelWidth
                    changeUpdateToSubPanels(sp, panel)
                })
            } else {
                panel.subPanels[1].panelWidth = panel.panelWidth - panel.subPanels[0].panelWidth
                panel.subPanels.forEach((sp) => {
                    sp.panelLength = panel.panelLength
                    changeUpdateToSubPanels(sp, panel)
                })
            }
        }
    } else {
        // console.log("【" + panel.id + "】没有子面板")
    }
}

export function changeSubPanelSize(parentPanel, subPanel, num) {
    num = parseInt(num)
    let direction = parentPanel.direction;
    if (direction == 1) {
        if (parentPanel.subPanels[0].id != subPanel.id) {
            parentPanel.subPanels[1].panelWidth = parseInt(num)
            parentPanel.subPanels[0].panelWidth = parentPanel.panelWidth - num
            subPanel = parentPanel.subPanels[1]
        } else {
            parentPanel.subPanels[1].panelWidth = parentPanel.panelWidth - num
            parentPanel.subPanels[0].panelWidth = num
            subPanel = parentPanel.subPanels[0]
        }

        parentPanel.subPanels.forEach((mp) => {
            changeUpdateToSubPanels(mp, parentPanel)
        })
        return { parentPanel: parentPanel, subPanel: subPanel }
    } else if (direction == 2) {
        if (parentPanel.subPanels[0].id != subPanel.id) {
            parentPanel.subPanels[0].panelLength = parentPanel.panelLength - num
            parentPanel.subPanels[1].panelLength = num
            subPanel = parentPanel.subPanels[1]
        } else {
            parentPanel.subPanels[0].panelLength = num
            parentPanel.subPanels[1].panelLength = parentPanel.panelLength - num
            subPanel = parentPanel.subPanels[0]
        }
        parentPanel.subPanels.forEach((mp) => {
            changeUpdateToSubPanels(mp, parentPanel)
        })
    } else {
        return { parentPanel: parentPanel, subPanel: subPanel }
    }
    return { parentPanel: parentPanel, subPanel: subPanel }
}


/**
 * replace the subpanel in panel with the "subPanel" on same id
 * @param panel
 * @param subPanel
 */
export function updatePanelNode(panel, thisPanel) {
    let subPanelId = thisPanel.id
    if (panel.id == subPanelId) {
        return thisPanel;
    }
    if (panel.subPanels) {
        if (subPanelId == panel.subPanels[0].id) {
            panel.subPanels[0] = thisPanel;
        } else if (subPanelId.indexOf(panel.subPanels[0].id) == 0) {
            updatePanelNode(panel.subPanels[0], thisPanel)

        } else {
            if (subPanelId == panel.subPanels[1].id) {
                panel.subPanels[1] = thisPanel;
            } else if (subPanelId.indexOf(panel.subPanels[1].id) == 0) {
                updatePanelNode(panel.subPanels[1], thisPanel)
            }
        }
    }
    return panel
}
export function changeSubPanelMemo(parentPanel, thisPanel, value, name) {
    let subPanelCopy = { ...thisPanel }
    if (name == 'memo') {
        subPanelCopy.memo = value
    } else if (name == 'lineConnector') {
        subPanelCopy.frameLine = { ...subPanelCopy.frameLine, lineConnector: value }

    } else {
        subPanelCopy.panelConnector = value
    }
    let parentPanelCopy = { ...parentPanel }
    for (let key in parentPanel.subPanels) {
        if (parentPanel.subPanels[key].id == thisPanel.id) {
            parentPanelCopy.subPanels[key] = subPanelCopy
        }

    }
    return { parentPanel: parentPanelCopy, thisPanel: subPanelCopy }
}

/**
 * 更新选中的sub panel的lenght / width
 * @param num new number
 */
export function subPanelChanged(currentPanel, parentPanel, thisPanel, num, name) {
    // if(name == 'lineConnector'){
    //     parentPanel = JSON.parse(JSON.stringify(parentPanel))

    // }
    if (name == 'memo' || name == 'type' || name == 'lineConnector') {
        parentPanel = JSON.parse(JSON.stringify(parentPanel))
        let pa = isEmpty(parentPanel) ? JSON.parse(JSON.stringify(currentPanel)) : parentPanel
        let result = changeSubPanelMemo(pa, thisPanel, num, name)
        let subPanelCopy = { ...thisPanel }
        if (name == 'memo') {
            subPanelCopy.memo = num
        } else if (name == 'lineConnector') {
            subPanelCopy.frameLine = { ...subPanelCopy.frameLine, lineConnector: num }
        } else {
            subPanelCopy.panelConnector = num
        }
        // console.log('subPanelCopy', subPanelCopy)
        let newCurrentPanel2 = JSON.parse(JSON.stringify(currentPanel))
        let currentPal = ''
        if (isEmpty(parentPanel)) {
            currentPal = updatePanelNode(newCurrentPanel2, subPanelCopy)
        } else {
            currentPal = updatePanelNode(newCurrentPanel2, result.parentPanel)

        }
        return {
            currentPanel: currentPal,
            parentPanel: result.parentPanel,
            thisPanel: result.thisPanel
        }
    }

    parentPanel = JSON.parse(JSON.stringify(parentPanel))
    let result = changeSubPanelSize(parentPanel, thisPanel, num);

    let newCurrentPanel = JSON.parse(JSON.stringify(currentPanel))
    return {
        currentPanel: updatePanelNode(newCurrentPanel, result.parentPanel),
        parentPanel: result.parentPanel,
        thisPanel: result.subPanel
    }

}
export function subPanelTypeChanged(currentPanel, parentPanel, subPanel, type) {
    subPanel.panelType = type;
    subPanel = JSON.parse(JSON.stringify(subPanel))

    let newCurrentPanel = JSON.parse(JSON.stringify(currentPanel))
    return {
        currentPanel: updatePanelNode(newCurrentPanel, subPanel),
        parentPanel: parentPanel,
        subPanel: subPanel
    }

}
export function subPanelLineChanged(currentPanel, parentPanel, subPanel, type) {
    // subPanel.divideType = type;
    let subPanelFrameLine = {...subPanel.frameLine, type:type}
    subPanel.frameLine = subPanelFrameLine
    subPanel = JSON.parse(JSON.stringify(subPanel))

    let newCurrentPanel = JSON.parse(JSON.stringify(currentPanel))
    return {
        currentPanel: updatePanelNode(newCurrentPanel, subPanel),
        parentPanel: parentPanel,
        subPanel: subPanel
    }

}


export function getPanelCaption(index) {
    let caption = ""
    //console.log(index)
    index = Number(index)
    switch (index) {
        case 0:
            caption = intl.get(TOP_PANEL)
            break;
        case 1:
            caption = intl.get(BOTTOM_PANEL)
            break;
        case 2:
            caption = intl.get(SIDE_PANEL)
            break;
        case 3:
            caption = intl.get(SIDE_OPERATION_PANEL)
            break;
        case 4:
            caption = intl.get(TERM_PANEL)
            break;
        case 5:
            caption = intl.get(RIGHT_PANEL)
            break;

    }
    return caption;
}

/**
 * 向后合并分区
 * @param partitions
 * @param index
 */
export function backwardPartition(partitions, index, layout) {
    //检查拉直的ahu段列表，看看是否是最后一段
    if (index == partitions.size - 1) {
        return partitions
    }
    //如果是twin类型，只能在一个AHULine内部合并，所以要做一次AHULine的内部检查
    if (layout && isTwinLine(layout.style)) {
        let selectedPartition = partitions[index]
        let fpos = selectedPartition.sections[selectedPartition.sections.length - 1].pos + 1;
        let epos = fpos + 1;
        //检验两个positon是否落在同一个AHULine
        let valid = false;
        let fidx = -1, eidx = -1;
        layout.layoutData.forEach((data, index) => {
            if (data.includes(fpos)) {
                fidx = index;
            }
            if (data.includes(epos)) {
                eidx = index;
            }
            if (data.includes(fpos) && data.includes(epos)) {
                valid = true;
                return;
            }
        })
        if (!valid) {
            if (([1, 0].includes(fidx) && [1, 0].includes(eidx))
                || ([3, 2].includes(fidx) && [3, 2].includes(eidx))) {
                valid = true;
            }
        }
        if (!valid) {
            // console.log("Has reach the end of AHULine, cannot do the backward operation!")
            return partitions;
        }
    }
    let newPartitions = JSON.parse(JSON.stringify(partitions))
    let first = newPartitions[index]
    let second = newPartitions[index + 1]
    if (!second) {
        return partitions
    }
    let merged = merge(first, second)
    newPartitions[index] = merged;
    let result = []
    newPartitions.forEach((partition, i) => {
        if (i > index + 1) {
            partition.pos = i - 1
            result.push(partition)
        } else if (i == index + 1) {

        } else {
            result.push(partition)
        }
    })
    return result
}

function merge(first, second) {
    if (!first || !second) {
        // console.log("Invalid first or section partition, do nothing")
        return first
    }

    let length = first.length + second.length
    first.length = length
    let tempIndex = first.sections.length
    second.sections.forEach((section, i) => {
        first.sections.push(section)
    })
    //Update panel size
    for (var key in first.panels) {
        let panel = first.panels[key]
        switch (parseInt(key)) {
            case 0:
            case 1:
            case 4:
            case 5:
                panel.panelLength = length
                panel.direction = 0
                panel.subPanels = null
                break
            default:
                break
        }
    }
    return first
}

/**
 * {intl.get(MERGE_DIVISION_FORWARD)}
 * @param partitions
 * @param index
 */
export function forwardPartition(partitions, index, layout) {
    if (index == 0) {
        return partitions
    }
    //如果是twin类型，只能在一个AHULine内部合并，所以要做一次AHULine的内部检查
    if (layout && isTwinLine(layout.style)) {
        let selectedPartition = partitions[index]
        let fpos = selectedPartition.sections[0].pos + 1;
        let epos = fpos - 1;
        //检验两个positon是否落在同一个AHULine
        let valid = false;
        let fidx = -1, eidx = -1;
        layout.layoutData.forEach((data, index) => {
            if (data.includes(fpos)) {
                fidx = index;
            }
            if (data.includes(epos)) {
                eidx = index;
            }
            if (data.includes(fpos) && data.includes(epos)) {
                valid = true;
                return;
            }
        })
        if (!valid) {
            if (([1, 0].includes(fidx) && [1, 0].includes(eidx))
                || ([3, 2].includes(fidx) && [3, 2].includes(eidx))) {
                valid = true;
            }
        }

        if (!valid) {
            // console.log("Has reach the head of AHULine, cannot do the forward operation!")
            return partitions;
        }
    }
    let newPartitions = JSON.parse(JSON.stringify(partitions))
    let second = newPartitions[index]
    let first = newPartitions[index - 1]
    let merged = merge(first, second)
    newPartitions[index - 1] = merged;
    let result = []
    newPartitions.forEach((partition, i) => {
        if (i > index) {
            partition.pos = i - 1
            result.push(partition)
        } else if (i == index) {

        } else {
            result.push(partition)
        }
    })
    return result

}
/**
 * {intl.get(SPLIT_DIVISION)}
 * @param partitions
 * @param index
 */
export function splitPartition(partitions, index, layout) {
    let partition = partitions[index]
    if (!partition) {
        // console.log("Null partition, quit...")
        return partitions
    }
    if (partition.sections.length < 2) {
        return partitions
    }
    // console.log("before split:")
    logPartitions(partitions)
    let newPartitions = JSON.parse(JSON.stringify(partitions))
    let newPartition = innerSplitPartition(newPartitions[index])
    let result = []
    // console.log(newPartition)
    newPartitions.forEach((partition, i) => {
        if (i > index) {
            partition.pos = i + 1
            result.push(partition)
        } else if (i == index) {
            result.push(partition)
            // console.log("insert newPartition after index:"+i)
            result.push(newPartition)
            // console.log("Hit here!")
        } else {
            result.push(partition)
        }
    })
    //console.log(newPartitions)
    // console.log("after split result:")
    // logPartitions(newPartitions)
    logPartitions(result)
    // console.log(result)

    return result
}

function logPartitions(partitions) {
    let res = []
    partitions.forEach((partition, i) => {
        let sections = []
        partition.sections.forEach((sec) => {
            sections.push(sec.metaId)
        })
        res.push(sections)
    })
    // console.log(res)
}

// old parition
// 1. lenght - sectionL
// 2. update Panel length
// 3. sections slice (reject the last one)
// 4. pos: the same
//
// new partition
// 1. length = sectionL
// 2. width, height the same
// 3. slice sections  (keep the last one)
// 4. pos: original + 1
function innerSplitPartition(partition) {
    //Split the last one from current partition
    // Create the second partition from the origial one

    let newPartition = JSON.parse(JSON.stringify(partition))
    let section = newPartition.sections[newPartition.sections.length - 1]

    //Update the new added partition position
    newPartition.pos = partition.pos + 1
    //Keep the last section
    let tmpSections = []
    partition.sections.forEach((section, i) => {
        if (i == partition.sections.length - 1) {
            tmpSections.push(section)
        }
    })
    newPartition.sections = tmpSections
    //Update panel length
    newPartition.length = section.sectionL
    for (var key in newPartition.panels) {
        let panel = newPartition.panels[key]
        switch (parseInt(key)) {
            case 0:
            case 1:
            case 4:
            case 5:
                panel.panelLength = newPartition.length
                panel.direction = 0
                panel.subPanels = null
                break
            default:
                break
        }
    }

    //Update the original partition
    //Keep the last section
    tmpSections = []
    partition.sections.forEach((section, i) => {
        if (i != partition.sections.length - 1) {
            tmpSections.push(section)
        }
    })
    partition.sections = tmpSections
    partition.length = partition.length - section.sectionL
    for (var key in partition.panels) {
        let panel = partition.panels[key]
        switch (parseInt(key)) {
            case 0:
            case 1:
            case 4:
            case 5:
                panel.panelLength = partition.length
                panel.direction = 0
                panel.subPanels = null
                break
            default:
                break
        }
    }

    return newPartition
}
export function savePartitions(currentPartitionPObj, partitions, show) {
    //    console.log('currentPartitionPObj.partitionJson', partitions[0].base, currentPartitionPObj)
    currentPartitionPObj.partitionJson = JSON.stringify(partitions)
    return dispatch => $http.post('ahu/partition/validate', currentPartitionPObj).then(data => {
        if (data.pass) {
            $http.post('ahu/partition', currentPartitionPObj).then(cdata => {
                if (cdata.code === '0' && cdata.msg === 'Success') {
                    if (data.msg) { // allow validation message
                        sweetalert({
                            title: intl.get(SAVE_SUCCESS),
                            text: data.msg,
                            timer: 2000,
                            type: 'warning',
                            showConfirmButton: false
                        })
                    } else {
                        sweetalert({
                            title: intl.get(SAVE_SUCCESS),
                            text: intl.get(SAVED_SPLIT_SECTION_CONFIG),
                            timer: 1000,
                            type: 'success',
                            showConfirmButton: false
                        })
                    }
                }
                if (show) {
                    dispatch(partitionTipSave(show, DateUtils.getSmpFormatNowDate()))
                }

                dispatch(partitionsSaved(cdata.data))
            })
        } else {
            dispatch(tip(data.msg))
        }
        dispatch(spin(false))

    })
}

export function spin(bool) {
    return {
        type: PARTITION_SPIN,
        bool,
    }
}

export function syncAandB(data) {
    return {
        type: PARTITION_SYNCAANDB,
        data,
    }
}
export function syncAB(panels){
    return (dispatch) => {
        let obj = {
            panelJson: JSON.stringify(panels.panels)
        }
        $http.post('panel/initab', obj).then((data)=>{
            dispatch(syncAandB(JSON.parse(data.data)))
        })
    }
}
// rootPanelId: "2_0_0"
// sectionMetaId: "ahu.access"
function merge2 (data){
    return {
        type: PARTITION_PANEL_MERGES,
        data,
    }
}
export function merges(id1, id2, panels, product){
    return (dispatch) => {
        let obj = {
            panel1:id1,
            panel2:id2,
            panelJson: JSON.stringify(panels),
            product
        }
        $http.post('panel/mergePanel', obj).then((data)=>{
            // dispatch(tip(data))
            dispatch(merge2(JSON.parse(data.data)))
            
        })
        
    }
    
}
export function savePartitionImage(currentPartitionPObj, imgData, show, callBack) {
    return (dispatch) => {
        $http.post('panel/img/upload/base64', {
            unitid: currentPartitionPObj.unitid,
            partitionid: currentPartitionPObj.partitionid,
            partitionNumber: show,
            base64: imgData
        }).then(data => {
            if (data.code === '0') {
                callBack()
                // sweetalert({
                //     title: intl.get(SAVE_SUCCESS),
                //     text: intl.get(SAVE_SUCCESS),
                //     timer: 2000,
                //     type: 'success',
                //     showConfirmButton: true
                // })


                // dispatch(partitionTipSave(show, DateUtils.getSmpFormatNowDate()))


            } else {
                dispatch(tip(data.msg))
            }
        })
    }
}

function partitionTipSave(show, time) {
    return {
        type: PARTITION_TIP_SAVE,
        show,
        time
    }
}
function tip(msg) {
    return {
        type: PARTITION_TIP,
        msg
    }
}

// export function savePartitions(currentPartitionPObj, partitions) {
//     currentPartitionPObj.partitionJson = JSON.stringify(partitions)

//     return dispatch => $http.post('ahu/partition', currentPartitionPObj).then(data => {
//         if (data.code === '0' && data.msg === 'Success') {
//             sweetalert({
//                 title: intl.get(SAVE_SUCCESS),
//                 text: intl.get(SAVED_SPLIT_SECTION_CONFIG),
//                 timer: 2000,
//                 type: 'success',
//                 showConfirmButton: true
//             })
//         }
//         dispatch(partitionsSaved(data.data))
//     })
// }

function partitionsSaved(partitionsId) {
    return {
        type: PARTITION_PARTITIONS_SAVED,
        partitionsPObj: partitionsId
    }
}



export function reset() {
    return (dispatch, getState) => {
        // let locale = getState().general.user.preferredLocale;
        dispatch({
            type: PARTITION_RESETTIP,
            tips: 'default',
        })
    }

}



export function changeBase(value, pos) {
    return (dispatch, getState) => {
        dispatch({
            type: PARTITION_CHANGEBASE,
            value,
            pos
        })
    }

}