import {
    CONFIRM,
    OK
} from '../intl/i18n'

import React from 'react'
import classnames from 'classnames'
import intl from 'react-intl-universal'
import $http from '../../misc/$http'
import sweetalert from 'sweetalert'

import { Tree, Button } from 'antd'
const TreeNode = Tree.TreeNode;

export default class ForTest extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            // expandedKeys: ['0-0-0', '0-0-1'],
            autoExpandParent: true,
            // checkedKeys: ['0-0-0'],
            selectedKeys: [],
        }
    }
    componentDidMount() {
        // $('[data-toggle="tooltip"]').tooltip()
        $http.get(`testCase/getAllAhu`).then(data => {
            this.setState(data)
            // console.log(data)
            // dispatch(receiveAhuInfo(data.data, unitSetting, metaUnit, getState().metadata.layout, propUnit))

        })
    }


    onExpand = (expandedKeys) => {
        // console.log('onExpand', expandedKeys);
        // if not set autoExpandParent to false, if children expanded, parent can not collapse.
        // or, you can remove all expanded children keys.
        this.setState({
            expandedKeys,
            autoExpandParent: false,
        });
    }

    onCheck = (checkedKeys) => {
        // console.log('onCheck', checkedKeys);
        this.setState({ checkedKeys });
    }

    onSelect = (selectedKeys, info) => {
        // console.log('onSelect', info);
        this.setState({ selectedKeys });
    }
    renderTreeNodes = (data) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} />;
        });
    }
    onSave = ()=>{
        let checkedKeys = JSON.stringify(this.state.checkedKeys)
        $http.post(`testCase/doTest`, {selectedAhu:checkedKeys}).then(data => {
            // this.setState(data)
            // dispatch(receiveAhuInfo(data.data, unitSetting, metaUnit, getState().metadata.layout, propUnit))
            if (data.code == "0" && data.msg == "Success") {
                let cText = ''

                JSON.parse(data.data).forEach((element)=>{
                    cText += element.unitId +' '+ (element.result===true?'成功':'<span style="color:red">失败</span>')+'<br/>'
                })

                sweetalert({
                    title: data.msg, type: 'success',
                    showCancelButton: false,
                    confirmButtonText: intl.get(OK),
                    text: cText,
                    html: true,
                    closeOnConfirm: true
                }, isConfirm => {
                    // window.location.reload();
                })
            }
            if (data.code == "1") {
                sweetalert({ title: data.msg, type: 'error',timer: 1000, showConfirmButton: false });
            }

        })
    }

    render() {

        return (
            <div>
                <Button
                    type="primary"
                    style={{margin:'10px 0 0 0'}}
                    onClick={() => { this.onSave() }}
                >
                {intl.get(CONFIRM)}
                </Button>
                <Tree
                    checkable={true}
                    onCheck={this.onCheck}
                    // onExpand={this.onExpand}
                    // expandedKeys={this.state.expandedKeys}
                    autoExpandParent={this.state.autoExpandParent}
                    checkedKeys={this.state.checkedKeys}
                    onSelect={this.onSelect}
                    selectedKeys={this.state.selectedKeys}
                >
                    {this.state.data && this.renderTreeNodes(this.state.data)}
                </Tree>
            </div>

        )
    }
}