import {
    SPLIT_SECTION,
    PANEL_ARRANGEMENT,
    PSYCHROMETRIC_CHART,
    TRIPLE_VIEW,
    CALCULATE_PRICE,
    NON_STANDARD,
    STANDARD,
    SAVE,
    IMPORT,
    EXPORT
} from '../intl/i18n'

import ability from '../../config/ability';

import intl from 'react-intl-universal'
import React from 'react'
import {Link} from 'react-router'
import { InputNumber } from 'antd';

export default class AnInput extends React.Component {
	constructor(props) {
	    super(props);
	   
	 }

    render() {
        return (
            <InputNumber min={0} max={10}  />
        )
    }
}
