import {connect} from 'react-redux'
import AHU from './AHU'
import {clickComponent} from '../../actions/ahu'
const mapStateToProps = state => {
    return {
        sections: state.ahu.sections,
        layout: state.ahu.layout,
        //Add this line,make sure that the AHU be refreshed after basic section is ready
        baseSections: state.ahu.baseSections,
        templateId: state.ahu.templateId,
    }
}

const mapDispatchToProps = dispatch => {
    return {
    	onClickSection: (e) => {
            dispatch(clickComponent(0, 'ahu.ahu'))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHU)
