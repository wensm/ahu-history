import React from 'react'
import { connect } from 'react-redux'
import { Field as FD, reduxForm } from 'redux-form'
import intl from 'react-intl-universal'
import { normalizeValueForUnit, formatValueForUnit, syncAhuComponentValues, syncAhuComponentValidate, syncOtherParams, getPreAndNextSection, TOOLTIP_INPUT } from '../../actions/ahu'
import { syncBatchValues } from '../../actions/groups'
import assert from './assert'
import style1 from './Field.css'
import classnames from 'classnames'
import AnInput from './anInput'
import { Icon, Tooltip } from 'antd'
import { isObject } from 'lodash'

import {
    LABEL_TOOLTIP,
    LABEL_WWK_FAN_SECTION
    // SInWetBulbT,
    // SInRelativeT,
    // SNewWetBulbT,
    // SNewRelativeT,
    // WInWetBulbT,
    // WInRelativeT,
    // WNewWetBulbT,
    // WNewRelativeT,

} from '../intl/i18n'
let _this = null

class Field extends React.Component {

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            bool: false,
        }
        this.renderSelectField = this.renderSelectField.bind(this);
    }

    getIntlValue(intlKey) {
        if (intlKey.startsWith('moon')) {
            return intl.get('meta.' + intlKey)
        } else {
            return intlKey
        }
    }

    getParameterIntlValue(data) {
        if (intl.options.currentLocale == "en-US") {
            return data.ememo
        } else {
            return data.memo
        }
    }

    getParameterValueIntlValue(option) {
        if (intl.options.currentLocale == "en-US") {
            return option.label
        } else {
            return option.clabel
        }
    }
    componentDidMount() {
    }
    componentWillReceiveProps(newProps) {
        if (this.props.unitPreferCode == 'M' && newProps.unitPreferCode == 'B')
            this.setState({
                bool: true,
            })
    }


    renderInput(field) {
        let {
            input,
            label,
            className,
            type,
            readOnly,
            disabled,
            prefix,
            fieldType,
            meta: { touched, error, warning, asyncValidating },
            ManualWarn,
            warnsAlarm,
            toRed,
            from
        } = field
        let ManualCheck = true
        if (ManualWarn && ManualWarn.length > 0) {
            ManualCheck = ManualWarn[0](input.value)
        }
        let name = input.name
        let isSectionL = name == 'meta_section_mix_sectionL' || name == 'meta_section_access_sectionL' || name == 'meta_section_combinedMixingChamber_sectionL' || name == 'meta_section_discharge_sectionL'
        // console.log('zzfmeta2', input.name, touched)
        //        if (input && input.name == "meta_section_mix_sectionL") {
        //            console.log('zzf10', warning, ManualCheck)
        //
        //        }

        return (
            <div className={style1.fieldFormGroup} data-field-type={fieldType} style={{ color: toRed && from == 'batch' ? 'red' : 'rgba(0, 0, 0, 0.65)', marginBottom: isSectionL ? '20px' : '2px' }}>
                <label className={style1.fieldLabel}>
                    {prefix && prefix + " - "}{label}
                </label>
                {warning && ManualCheck && <Tooltip placement="bottomLeft" title={warning}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} /></Tooltip>}
                {error && <Tooltip placement="bottomLeft" title={error}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#a94442' }} /></Tooltip>}

                <input className={classnames(className, style1.fieldInput)} {...input} value={input.value != undefined ? input.value : ''} type={type} disabled={disabled} readOnly={readOnly} />
                {isSectionL && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '14px', overflow: 'hidden', display: 'block' }} title={warning} >{intl.get('rule_message.meta_section_mix_sectionL')}</span>}

                {/*((error && <span className="text-danger"> ({error})</span>) ||
                    (warning && ManualCheck && (!warnsAlarm || warnsAlarm.length == 0) && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '14px', overflow: 'hidden' }} title={warning} >{warning}</span>) ||
                    (warning && ManualCheck && warnsAlarm && warnsAlarm[0] == 'true' && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '16px', overflow: 'visible' }} title={warning} >{warning}</span>)
                )*/}
            </div>
        )
    }
    renderCheckbox({
        input,
        label,
        className,
        type,
        readOnly,
        disabled,
        prefix,
        fieldType,
        data,
        warnsAlarm,
        toRed,
        from,
        meta: { touched, error, warning }
        }) {
        //         if(input && input.name == 'meta_section_mix_returnBack'){
        //
        //         }
        //alarm  zzf新的警告
        return (
            <div className={style1.fieldFormGroup} data-field-type={fieldType} style={{ color: toRed && from == 'batch' ? 'red' : 'rgba(0, 0, 0, 0.65)' }}>
                <input className={classnames(className, style1.fieldInput)} style={{ top: '0px' }} {...input} type={type} disabled={disabled} readOnly={readOnly} />
                <label className={style1.fieldLabel}>
                    {/* {prefix && prefix + " - "}{label} */}
                    {prefix && prefix + " - "}
                    {data && _this.getParameterIntlValue(data)}

                </label>
                {warning && input.value && <Tooltip placement="bottomLeft" title={warning}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} /></Tooltip>}
                {error && <Tooltip placement="bottomLeft" title={error}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#a94442' }} /></Tooltip>}

                {/* {error && <span className="text-danger"> {error}</span>}
                {warning && input.value && (!warnsAlarm || warnsAlarm.length == 0) && <span className={classnames("text-warning", style1.fieldWarnSpan)} title={warning} style={{ display: 'block', fontSize: '14px', overflow: 'hidden' }}>{warning}</span>}
                {warning && input.value && warnsAlarm && warnsAlarm[0] == 'true' && <span className={classnames("text-warning", style1.fieldWarnSpan)} title={warning} style={{ display: 'block', fontSize: '16px', overflow: 'visible' }}>{warning}</span>} */}

            </div>
        )
    }
    renderTextarea(field) {
        let {
            input,
            label,
            className,
            type,
            readOnly,
            disabled,
            prefix,
            fieldType,
            meta: { touched, error, warning, asyncValidating },
            ManualWarn,
            warnsAlarm,
            toRed,
            from
        } = field
        let ManualCheck = true
        if (ManualWarn && ManualWarn.length > 0) {
            ManualCheck = ManualWarn[0](input.value)
        }
        let name = input.name
        let isSectionL = name == 'meta_section_mix_sectionL' || name == 'meta_section_access_sectionL' || name == 'meta_section_combinedMixingChamber_sectionL' || name == 'meta_section_discharge_sectionL'
        // console.log('zzfmeta2', input.name, touched)
        //        if (input && input.name == "meta_section_mix_sectionL") {
        //            console.log('zzf10', warning, ManualCheck)
        //
        //        }
        return (
            <div className={style1.fieldFormGroup} data-field-type={fieldType} style={{ color: toRed && from == 'batch' ? 'red' : 'rgba(0, 0, 0, 0.65)', marginBottom: isSectionL ? '20px' : '2px' }}>
                <label className={style1.fieldLabel}>
                    {prefix && prefix + " - "}{label}
                </label>
                {warning && ManualCheck && <Tooltip placement="bottomLeft" title={warning}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} /></Tooltip>}
                {error && <Tooltip placement="bottomLeft" title={error}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#a94442' }} /></Tooltip>}

                <textarea className={classnames(className, style1.fieldInput)} {...input} value={input.value != undefined ? input.value : ''} type={type} disabled={disabled} readOnly={readOnly} rows="3" />
                {isSectionL && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '14px', overflow: 'hidden', display: 'block' }} title={warning} >{intl.get('rule_message.meta_section_mix_sectionL')}</span>}

                {/*((error && <span className="text-danger"> ({error})</span>) ||
                    (warning && ManualCheck && (!warnsAlarm || warnsAlarm.length == 0) && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '14px', overflow: 'hidden' }} title={warning} >{warning}</span>) ||
                    (warning && ManualCheck && warnsAlarm && warnsAlarm[0] == 'true' && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '16px', overflow: 'visible' }} title={warning} >{warning}</span>)
                )*/}
            </div>
        )
    }


    renderSelectField(field) {
        let {
            input,
            label, disabled,
            className,
            readOnly,
            prefix, options,
            meta: { touched, error, warning },
            ManualWarn,
            warnsAlarm,
            toRed,
            from
       } = field
        let ManualCheck = true
        //    console.log('zzf10', warnsAlarm)
        //    console.log('zzf10',  input, field,ManualWarn, warning)
        if (ManualWarn && ManualWarn.length > 0) {
            ManualCheck = ManualWarn[0](input.value)

        }
        // if (input && input.name == "meta_section_mix_damperMeterial") {
        // console.log('toRedmeta_section_mix_damperMeterial', toRed)
        // }


        return (
            <div className={style1.fieldFormGroup} style={{ color: toRed && from == 'batch' ? 'red' : 'rgba(0, 0, 0, 0.65)' }}>
                <label className={style1.fieldLabel}>
                    {prefix && prefix + " - "}{label}
                </label>
                {warning && ManualCheck && <Tooltip placement="bottomLeft" title={warning}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} /></Tooltip>}
                {error && <Tooltip placement="bottomLeft" title={error}><Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#a94442' }} /></Tooltip>}
                <select className={classnames(className)} {...input} disabled={disabled} readOnly={readOnly}>
                    {options.map((option, index) => option && <option key={index}
                        value={option && option.option}>{this.getParameterValueIntlValue(option)}</option>)}
                </select>
                {/*((error && <span className="text-error"> ({error})</span>) ||
                    (warning && ManualCheck && (!warnsAlarm || warnsAlarm.length == 0) && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '14px', overflow: 'hidden' }} title={warning}>{warning}</span>) ||
                    (warning && ManualCheck && warnsAlarm && warnsAlarm[0] == 'true' && <span className={classnames("text-warning", style1.fieldWarnSpan)} style={{ fontSize: '16px', overflow: 'visible' }} title={warning}>{warning}</span>)

                )*/}
            </div>
        )


    }

    render() {
        const { doBatchComponentValue, currentOptionName, warnsAlarm, unitPreferCode, customDisabled, from, ManualWarn, id, data, componentValues, metaValidates, selectedComponent, warn, validate, onChange, onBlur, onFocus, propUnit, metaUnit, metaLayout, initialValues, prefix } = this.props
        let { basicGroupSecList, batchComponentValue } = this.props
        let componentValue = componentValues[selectedComponent.id]
        if (id == 'meta.section.combinedFilter.sectionL') {
        }
        // let copyFunction = formatValueForUnit

        //此处为hack方法，Filed空间本身加了很多page3才有的逻辑，在page2，page1引用是，会出现不兼容的情况
        if (!componentValue) {
            componentValue = initialValues
        } else {

        }
        if (!componentValues[0]) {
            componentValues[0] = { 'assis.section.ahu.height': "0" }
        }

        if (from == 'batch') {
            // console.log('render', id, data, componentValue, componentValues, from, selectedComponent)

        }
        //End of Hack
        let elem = null
        if (typeof componentValue === typeof undefined) {
            // console.log('zzfnull',componentValue,componentValues,selectedComponent,data)
            return elem
        }
        // if (data && data.name == '"Tube Diameter"') {
        // console.log('zzf9', data, id,componentValues)

        // }
        if (data && data.valueType == 'BooleanV') {
            // console.log('zzf9', data, id, warn)

        }
        const keyName = id.replace(/\./gi, '_')
        let text = ''
        let arr = id.split('.')
        let lastArr = arr[arr.length - 1]
        // let hasTooltip = TOOLTIP_INPUT.some((element) => {//取消了该功能
        //     if (element == lastArr) {
        //         text = intl.get('label.' + element)
        //         return true
        //     }
        // })
        // console.log('TOOLTIP_INPUT', TOOLTIP_INPUT, id.split('.'), hasTooltip, text)
        const this_keyName = id.replace(/\./gi, '_')
        let toRed = false
        // console.log('batchComponentValue', basicGroupSecList, batchComponentValue, this_keyName)
        if (basicGroupSecList && batchComponentValue && basicGroupSecList[this_keyName] != batchComponentValue[this_keyName]) {
            if (this_keyName == "meta_section_fan_fixRepairLamp" && basicGroupSecList) {
                // console.log('1', basicGroupSecList, batchComponentValue)
                // console.log('2', basicGroupSecList['meta_section_fan_fixRepairLamp'], batchComponentValue['meta_section_fan_fixRepairLamp'])

            }
            if (this_keyName == "meta_section_fan_fixRepairLamp" && basicGroupSecList) {
                // console.log('3', basicGroupSecList, batchComponentValue)
                // console.log('4', basicGroupSecList['meta_section_mix_damperOutlet'], batchComponentValue['meta_section_mix_damperOutlet'])

            }
            let compare1 = basicGroupSecList[this_keyName] == '' ? 'false' : basicGroupSecList[this_keyName]
            let compare2 = batchComponentValue[this_keyName] == '' ? 'false' : batchComponentValue[this_keyName]
            if (compare1 != compare2) {
                toRed = true
            }
        }




        // data && console.log(data.valueType) 

        if (data) {
            switch (true) {
                case data.valueType === 'BooleanV':

                    let enable4Checkbox = false
                    if (data.option && data.option.enableRelatedKeys) {
                        const enableValues = []
                        const enableRelatedKeys = data.option.enableRelatedKeys
                        if (enableRelatedKeys && enableRelatedKeys.length) {
                            enableRelatedKeys.forEach((enableRelatedKey, index) => {
                                if (enableRelatedKey.split('.')[1] == 'ahu') {//ahu属性在这里获取
                                    enableValues.push(componentValues[0][enableRelatedKey.replace(/\./gi, '_')])

                                } else if (enableRelatedKey.split('.')[1] == 'pre') {

                                } else if (enableRelatedKey.split('.')[1] == 'next') {

                                } else {
                                    enableValues.push(componentValue[enableRelatedKey.replace(/\./gi, '_')])

                                }
                            })
                        }
                        // console.log(enableValues);
                        let returnValue = getEnableAndDefaultValue(enableValues, data.option.enableValueMap)

                        enable4Checkbox = returnValue.enable
                        let cur = false
                        data.option.relatedKeys && data.option.relatedKeys.forEach((ele) => {
                            if (ele.replace(/\./gi, '_') == currentOptionName) cur = true
                        })
                        if (returnValue.dValue != undefined) {
                            componentValue[keyName] = returnValue.dValue   //新增功能json配置的默认值...
                        }
                    }

                    elem = (
                        <label className="checkbox-inline">
                            <FD name={id.replace(/\./gi, '_')} component={this.renderCheckbox} type="checkbox"
                                disabled={enable4Checkbox ? data.editable : !data.editable}
                                // onFocus={(event) => onFocus(event)}
                                validate={validate}
                                warn={warn}
                                data={data}
                                toRed={toRed}
                                from={from}
                                prefix={prefix}
                                warnsAlarm={warnsAlarm}
                                // onBlur={(event, newValue, previousValue) => {event.preventDefault }}
                                onChange={(event, newValue, previousValue) => onChange(event, newValue, previousValue, '', '', metaLayout, 'checkbox')}
                                format={(value, previousValue) => {
                                    // console.log('zzf10', value)
                                    if (typeof value === 'boolean') {
                                        return value;
                                    } else {
                                        return value == 'true';
                                    }
                                }}
                            />
                            {/* {prefix && prefix + " - "}{data && this.getParameterIntlValue(data)} */}
                        </label>
                    )
                    break;
                case data.option && data.option.optionPairs && (data.option.optionPairs.length > 0):

                    let values = []
                    const relatedKeys = data.option.relatedKeys//relatedKeys有内容则该下拉框的option和relatedKeys下的key对应的内容有关
                    if (relatedKeys && relatedKeys.length) {
                        relatedKeys.forEach((relatedKey, index) => {

                            if (relatedKey.indexOf('.ahu.') != -1) {//兼容所有通过ahu属性联动的需求
                                values.push(componentValues[0][relatedKey.replace(/\./gi, '_')])
                            } else if (relatedKey.split('.')[2] == 'pre') {
                                values.push(getPreAndNextSection(componentValues, selectedComponent).pre)
                            } else if (relatedKey.split('.')[2] == 'next') {
                                values.push(getPreAndNextSection(componentValues, selectedComponent).next)
                            } else {
                                values.push(componentValue[relatedKey.replace(/\./gi, '_')])
                            }
                            /*switch (relatedKey) {
                                case 'assis.section.ahu.height':
                                    values.push(componentValues[0][relatedKey.replace(/\./gi, '_')])
                                    break;
                                case 'meta.ahu.serial':
                                    values.push(componentValues[0][relatedKey.replace(/\./gi, '_')])
                                    break;
                                default:
                                    values.push(componentValue[relatedKey.replace(/\./gi, '_')])
                            }*/
                        })
                    }

                    let options = []
                    let { optionIndexs, defaultValue = undefined } = findOptionIndexs(values, data.option.valueOptionMap, data.defaultValueMap, id)

                    if (data.option.relatedSectionOptionMap) {
                        let sectionName = null;
                        let wwkFanDisplayName = intl.get(LABEL_WWK_FAN_SECTION);
                        if (selectedComponent.displayName && selectedComponent.displayName === wwkFanDisplayName) {
                            sectionName = "ahu.wwkFan";
                        } else {
                            sectionName = selectedComponent.name;
                        }
                        let sectionOptionMap = data.option.relatedSectionOptionMap[sectionName];
                        if (optionIndexs && optionIndexs.length) {
                            let newOptionIndexs = [];
                            optionIndexs.forEach(optionIndex => {
                                if (sectionOptionMap.includes(optionIndex)) {
                                    newOptionIndexs.push(optionIndex);
                                }
                            })
                            optionIndexs = newOptionIndexs;
                        } else {
                            optionIndexs = sectionOptionMap;
                        }
                    }

                    // let {optionIndexs, defaultValue = undefined} = findOptionIndexs(values, data.option.valueOptionMap)
                    //                    console.log('optionIndexs', optionIndexs, defaultValue, findOptionIndexs(values, data.option.valueOptionMap))
                    if (data.option.relationKeys && data.option.relationKeys.length > 0) {
                        optionIndexs = findOptionIndexs2(data.option.relationKeys, data.option.relationMap, componentValues, selectedComponent, getPreAndNextSection, optionIndexs, id)
                        // if(id == 'meta.section.coolingCoil.eliminator'){
                        // console.log('option', data.option.relationKeys, optionIndexs)
                        // }
                    }

                    //                    if (id == 'meta.section.wetfilmHumidifier.material') {
                    //                        console.log('optionIndexs', optionIndexs)
                    //                    }

                    if (optionIndexs && optionIndexs.length) {
                        optionIndexs.forEach(optionIndex => {
                            options.push(data.option.optionPairs[optionIndex])
                        })
                    } else {
                        options = data.option.optionPairs
                    }
                    // console.log('options',options,optionIndexs,data,data.option.relationKeys, data.option.relationMap)
                    let findValue = false;
                    // console.log('zzf', options, optionIndexs, findValue, data, data.memo)
                    for (let i = 0; i < options.length; i++) {
                        let option = options[i];
                        // console.log('option', option, i)
                        if (!findValue && option) {
                            findValue = (componentValue[keyName] == option['option']);
                            let curr = false
                            data.option.relatedKeys && data.option.relatedKeys.forEach((ele) => {
                                if (ele.replace(/\./gi, '_') == currentOptionName) curr = true
                            })
                            if (defaultValue && curr) {
                                componentValue[keyName] = defaultValue//新增功能json配置的默认值...
                            }
                        }
                    }
                    //找不到原来的值，自动赋值为默认的第一个
                    if (!findValue && options.length > 0 && options[0]) {
                        // 日志暂时开着，可以检测哪些字段的设置值和关联值的冲突
                        // console.log('not find  [' + keyName + ']{' + componentValue[keyName] + '} in ' + JSON.stringify(options) + ': set Value ' + options[0].option)
                        if (from != 'batch') {
                            componentValue[keyName] = options[0].option;

                        }
                        // batchComponentValue[keyName] = options[0].option;
                        // doBatchComponentValue(keyName, options[0].option);//同步批量里面的，找不到原来的值，自动赋值为默认的第一个
                        // toRed = !toRed
                        // console.log('render', toRed, id)

                    } else {
                    }
                    let findValue2 = false;
                    if (from == 'batch') {

                        for (let i = 0; i < options.length; i++) {
                            let option = options[i];
                            // console.log('option', option, i)
                            if (!findValue && option) {
                                findValue = (batchComponentValue[keyName] == option['option']);
                                if (defaultValue && (keyName != currentOptionName)) {
                                    batchComponentValue[keyName] = defaultValue//新增功能json配置的默认值...
                                }
                            }
                        }
                    }
                    if (!findValue && options.length > 0 && options[0]) {
                        if (from == 'batch') {

                            batchComponentValue[keyName] = options[0].option;
                        }
                    }

                    let enable = false
                    if (data.option.enableRelatedKeys) {
                        const enableValues = []
                        const enableRelatedKeys = data.option.enableRelatedKeys
                        if (enableRelatedKeys && enableRelatedKeys.length) {
                            enableRelatedKeys.forEach((enableRelatedKey, index) => {
                                if (enableRelatedKey.indexOf('.ahu.') != -1) {//兼容所有通过ahu属性联动的需求
                                    enableValues.push(componentValues[0][enableRelatedKey.replace(/\./gi, '_')])
                                } else {
                                    enableValues.push(componentValue[enableRelatedKey.replace(/\./gi, '_')])
                                }


                            })
                        }
                        // console.log('enableValues', enableValues, data.option.enableValueMap);
                        let returnValue = getEnableAndDefaultValue(enableValues, data.option.enableValueMap)
                        enable = returnValue.enable
                    }

                    //console.log(data);
                    //console.log("enable:"+enable);
                    //console.log("editable:"+data.editable);
                    //console.log(enable ? enable : !data.editable);
                    // if(id == 'meta.section.discharge.DoorDirection'){

                    //     console.log('enable', enable, data)
                    // }
                    elem = (
                        <FD name={keyName}
                            label={(prefix && prefix + " - ") || "" + this.getParameterIntlValue(data) || ''}
                            component={this.renderSelectField} className={classnames("form-control", style1.fieldInput)}
                            disabled={enable ? data.editable : !data.editable}
                            onFocus={(event) => onFocus(event)}
                            validate={validate}
                            warn={warn}
                            toRed={toRed}
                            from={from}
                            ManualWarn={ManualWarn}
                            warnsAlarm={warnsAlarm}
                            options={options}
                            onBlur={(event, newValue, previousValue) => onBlur(event, newValue, previousValue, unitPreferCode)}
                            onChange={(event, newValue, previousValue) => onChange(event, newValue, previousValue, propUnit, metaUnit, metaLayout)}
                        >
                        </FD>

                    )
                    break;
                case data.valueType === 'LongStringV':
                    let unitStr2 = ""
                    let style2 = null
                    if (data && propUnit[data.valueSource]) {
                        style2 = propUnit[data.valueSource]
                    }
                    if (metaUnit[data.valueSource]) {
                        if (style2 == 'M') {
                            unitStr2 = "(" + metaUnit[data.valueSource]["mlabel"] + ")"
                        } else if (style2 == "B") {
                            unitStr2 = "(" + metaUnit[data.valueSource]["blabel"] + ")"
                        }
                    }
                    let inputEnable2 = false
                    if (data.option && data.option.enableRelatedKeys) {
                        const enableValues = []
                        const enableRelatedKeys = data.option.enableRelatedKeys
                        if (enableRelatedKeys && enableRelatedKeys.length) {
                            enableRelatedKeys.forEach((enableRelatedKey, index) => {
                                enableValues.push(componentValue[enableRelatedKey.replace(/\./gi, '_')])
                            })
                        }
                        // console.log(enableValues);

                        let cinputEnable = getEnableAndDefaultValue(enableValues, data.option && data.option.enableValueMap)
                        inputEnable2 = cinputEnable.enable
                    }
                    elem = <FD
                        fieldType={data.valueType}
                        label={this.getParameterIntlValue(data) + unitStr2}
                        name={id.replace(/\./gi, '_')}
                        component={this.renderTextarea}
                        type={data.valueType === 'DoubleV' || data.valueType === 'IntV' ? 'number' : 'text'}
                        ManualWarn={ManualWarn}
                        warnsAlarm={warnsAlarm}
                        className="form-control"
                        placeholder=""
                        prefix={prefix}
                        readOnly={!data.editable}
                        disabled={customDisabled != undefined ? customDisabled : inputEnable ? data.editable : !data.editable}
                        validate={validate}
                        warn={warn}
                        onFocus={(event) => onFocus(event, propUnit, metaUnit, metaLayout)}
                        onBlur={(event, newValue, previousValue) => onBlur(event, newValue, previousValue, unitPreferCode)}
                        onChange={(event, newValue, previousValue) => { onChange(event, newValue, previousValue, propUnit, metaUnit, metaLayout) }}


                    />
                    break;

                default:
                    let unitStr = ""
                    let style
                    if (data && propUnit[data.valueSource]) {
                        style = propUnit[data.valueSource]
                    }
                    if (metaUnit[data.valueSource]) {
                        if (style == 'M') {
                            unitStr = "(" + metaUnit[data.valueSource]["mlabel"] + ")"
                        } else if (style == "B") {
                            unitStr = "(" + metaUnit[data.valueSource]["blabel"] + ")"
                        }
                    }
                    let inputEnable = false
                    if (data.option && data.option.enableRelatedKeys) {
                        const enableValues = []
                        const enableRelatedKeys = data.option.enableRelatedKeys
                        if (enableRelatedKeys && enableRelatedKeys.length) {
                            enableRelatedKeys.forEach((enableRelatedKey, index) => {
                                enableValues.push(componentValue[enableRelatedKey.replace(/\./gi, '_')])
                            })
                        }
                        // console.log(enableValues);

                        let cinputEnable = getEnableAndDefaultValue(enableValues, data.option && data.option.enableValueMap)
                        inputEnable = cinputEnable.enable
                    }
                    if (id === "meta.section.mix.NAVolume" || id === "meta.section.mix.NARatio") {
                        inputEnable = this.getBool(componentValue)

                    }
                    if (id === "meta.section.coolingCoil.Sconcentration") {
                        // console.log("inputEnable:" + inputEnable)
                    }

                    elem = <FD
                        fieldType={data.valueType}
                        label={this.getParameterIntlValue(data) + unitStr}
                        name={id.replace(/\./gi, '_')}
                        component={this.renderInput}
                        type={data.valueType === 'Double2V' || data.valueType === 'DoubleV' || data.valueType === 'IntV' ? 'number' : 'text'}
                        ManualWarn={ManualWarn}
                        warnsAlarm={warnsAlarm}
                        className="form-control"
                        placeholder=""
                        prefix={prefix}
                        readOnly={!data.editable}
                        disabled={customDisabled != undefined ? customDisabled : inputEnable ? data.editable : !data.editable}
                        validate={validate}
                        warn={warn}
                        onFocus={(event) => onFocus(event, propUnit, metaUnit, metaLayout)}
                        onBlur={(event, newValue, previousValue) => onBlur(event, newValue, previousValue, unitPreferCode)}
                        onChange={(event, newValue, previousValue) => { onChange(event, newValue, previousValue, propUnit, metaUnit, metaLayout) }}
                        normalize={(value, previousValue) => {//normalize为格式化输出数据，比如说小数点一位，两位等
                            // return value
                            if (String(value).length < String(previousValue).length) {//删除输入框字符不考虑小数点位数的逻辑
                                return value
                            }
                            if (value.indexOf('.') !== -1 && value.split('.')[1] == '') {
                                return value
                            }
                            //有最大值的时候
                            if (this.props.maxValue) {
                                if (Number(value) > Number(this.props.maxValue)) {
                                    return previousValue;
                                }
                            }
                            //有最小值的时候
                            if (this.props.minValue) {
                                if (Number(value) > Number(this.props.minValue)) {
                                    return previousValue;
                                }
                            }
                            // if (data && data.key == 'meta.section.steamHumidifier.VaporPressure') {
                            if (data.valueType === 'Double2V') {

                                if ((value + "").indexOf('.') !== -1 && Number(value) != Number.NaN) {
                                    if ((value + "").split('.')[1].length == 1) {
                                        return Number(value).toFixed(1);
                                    }
                                    return Number(value).toFixed(2);
                                }
                            }
                            if (data.valueType === 'DoubleV') {
                                //double类型的字段，取小数后面1位
                                if ((value + "").indexOf('.') !== -1 && (value + "").split('.')[1] != '' && Number(value) != Number.NaN) {
                                    // value.toString().match(/^\d+(?:\.\d{0,1})?/)
                                    let num = Number(value).toFixed(1)
                                    // let num = parseInt(Number(value) * 100, 0) / 100;
                                    // if ((num + '').substr(-2) === '00') {
                                    //     return Number(num).toFixed(0)
                                    // }
                                    return num;
                                }
                            }
                            if (data.valueType === 'IntV') {
                                if ((value + "").indexOf('.') !== -1 && Number(value) != Number.NaN) {
                                    return Number(value).toFixed(0);
                                }
                            }
                            // let q = normalizeValueForUnit(id.replace(/\./gi, '_'), value,  propUnit, metaUnit, metaLayout)//暂时注释
                            // console.log('normalize', value, q)

                            return value
                        }}
                        format={(value, name) => {//format 方法为value如何显示，比如字符串的时间可以转为Date类型的时间
                            // if (value !== undefined && data.valueType == 'DoubleV') {
                            //     let a = value.toString().match(/^\d+(?:\.\d{0,2})?/)
                            //     // console.log('zzff', value, data.valueType)
                            //     return value
                            // }
                            if (value && (value + "").indexOf('.') !== -1 && (value + "").split('.')[1] == '') {
                                return value
                            }
                            if ((value + "").indexOf('.') !== -1) {
                                if (data.valueType === 'IntV') {
                                    value = Number(value).toFixed(0)
                                } else if (data.valueType === 'DoubleV') {
                                    //  num = ''
                                    // if (name == 'meta_section_steamHumidifier_VaporPressure') {//只允许输入2位，保留2位
                                    // num = value.toString().match(/^\d+(?:\.\d{0,2})?/)
                                    // } else {
                                    // }
                                    // let num = value.toString().match(/^\d+(?:\.\d{0,1})?/)//只允许输入1位，保留一位
                                    // console.log('num', num)
                                    value = Number(value).toFixed(1);;
                                } else if (data.valueType === 'Double2V') {
                                    // let num = value.toString().match(/^\d+(?:\.\d{0,2})?/)
                                    // let num =  ''
                                    if ((value + "").split('.')[1].length == 1) {
                                        value = Number(value).toFixed(1);
                                    }else{
                                        value = Number(value).toFixed(2);
                                    }
                                    
                                    // value = num;

                                }
                            }
                            let f = value
                            if (value != undefined && this.state.bool) {
                                // f = formatValueForUnit(name, value, propUnit, metaUnit, metaLayout);//暂时注释
                                this.setState({ bool: false })
                                // copyFunction = null
                                if (name == 'meta_ahu_sairvolume') {
                                    // console.log('format', value, f)
                                }
                            }
                            return value
                        }}
                    />
            }
        }
        return elem
    }
    getBool = (componentValue) => {//混合段，如果只选一个出风方向，新风量和新风比应该不可输入
        const list = [
            'meta_section_mix_returnTop',
            'meta_section_mix_returnBack',
            'meta_section_mix_returnLeft',
            'meta_section_mix_returnRight',
            'meta_section_mix_returnButtom',
        ];
        let arr = [];
        list.forEach((record) => {
            if (componentValue[record]) arr.push(record)
        })
        if (arr.length == 1 || arr.length == 0) {
            return true
        } else {
            return false
        }
    }
}


/*
 *联动【复选，输入框，下拉框】是否禁用
 值为null匹配的是是否可编辑，
 dValue表示默认值
 */
function getEnableAndDefaultValue(enableValues, mapValueToEnable) {

    for (var variable in mapValueToEnable) {
        if (mapValueToEnable.hasOwnProperty(variable)) {
            const assertions = variable.split('#')
            let mark = true
            enableValues.forEach((enableValue, index) => {//参数必须全部匹配，否则mark变为false
                if (!assert(enableValue, assertions[index])) {
                    mark = false
                }
            })
            if (mark) {
                //return mapValueToEnable[variable] 废弃json里面的true/false
                // return true;
                if (mapValueToEnable[variable] == null) {
                    return {
                        enable: true,
                        dValue: undefined
                    }
                } else {
                    return {
                        enable: true,
                        dValue: mapValueToEnable[variable]
                    }
                }
            }
        }
    }
    return {
        enable: false,
        dValue: undefined
    }
    // return false
}

function findOptionIndexs(values, mapValueToOptionIndexs, defaultValueMap, id) {
    // if(id == 'meta.section.coolingCoil.drainpanType'){
    //     console.log('zzf6.2', values, mapValueToOptionIndexs)
    //     }
    for (var variable in mapValueToOptionIndexs) {
        if (mapValueToOptionIndexs.hasOwnProperty(variable)) {
            const assertions = variable.split('#')
            let mark = true
            values.forEach((value, index) => {
                if (assertions[index] || assertions[index] == '') {
                    if (!assert(value, assertions[index])) {
                        mark = false
                    }
                } else {
                    // console.error("Error meta definition: ")zzf
                    // console.error(index, values)zzf
                }

            })
            if (mark) {
                // return mapValueToOptionIndexs[variable]
                if (defaultValueMap && defaultValueMap[variable]) {
                    return {
                        optionIndexs: mapValueToOptionIndexs[variable],
                        defaultValue: defaultValueMap[variable]
                    }
                } else {
                    return {
                        optionIndexs: mapValueToOptionIndexs[variable],
                        defaultValue: undefined
                    }
                }
            }
        }
    }
    return {
        optionIndexs: [],
        defaultValue: undefined
    }
}

function findOptionIndexs2(relationKeys, relationMap, componentValues, selectedComponent, getPreAndNextSection, optionIndexs, id) {
    // if(id == 'meta.section.coolingCoil.eliminator'){
    //     console.log('param', relationKeys, relationMap, componentValues, selectedComponent, getPreAndNextSection, optionIndexs, id)
    // }
    let copyValues = { ...componentValues }
    let values = {}
    for (let key in copyValues) {
        values = { ...values, ...copyValues[key] }
    }
    let retu = ''
    if (relationKeys) {
        let preAndNextSection = getPreAndNextSection(componentValues, selectedComponent)
        let { pre, next } = preAndNextSection
        retu = relationKeys.map((record, index) => {//index为匹配成功的index
            let condition = record.split(';');
            if (condition.length == 1) {//前/后只有一个有规则  
                let result = condition.map((_record) => {
                    let con = _record.split('#');
                    if (con[1].indexOf('.') > -1) {//前后段里面的具体的某个属性
                        let conCopy = con[1].split('meta.section.')[1].split('.')[0]
                        if (con[0] == 'pre') {//前面段
                            if (conCopy == pre) {
                                let copyKey = con[1].replace(/\./gi, '_')
                                for (let vk in relationMap[index]) {
                                    if (vk == values[copyKey]) {
                                        let arr = relationMap[index][vk]
                                        return arr
                                    }
                                }
                            }
                        } else {//后面段
                            if (conCopy == next) {
                                let copyKey = con[1].replace(/\./gi, '_')
                                for (let vk in relationMap[index]) {
                                    if (vk == values[copyKey]) {
                                        let arr = relationMap[index][vk]
                                        return arr
                                    }
                                }
                            }
                        }
                    } else {//前后段是什么
                        if (con[0] == 'pre') {//前面段
                            if (con[1] == pre) {
                                let arr = relationMap[index]['true']
                                return arr
                            }
                        } else {//后面段
                            if (con[1] == next) {
                                let arr = relationMap[index]['true']

                                return arr
                            }
                        }
                    }
                })
                return result[0]
            } else {//前后段都有规则
                let lcon = false, rcon = false;
                condition.forEach((_record, _index) => {
                    let con = _record.split('#');
                    if (con[1].indexOf('.') > -1) {//前后段里面的具体的某个属性
                        let conCopy = con[1].split('meta.section.')[1].split('.')[0]
                        if (con[0] == 'pre') {//前面段
                            if (conCopy == pre) {
                                let copyKey = con[1].replace(/\./gi, '_')
                                for (let vk in relationMap[index]) {
                                    if (_index == 1 && vk.split('#')[_index] == values[copyKey]) {
                                        rcon = true
                                    } else if (_index == 0 && vk.split('#')[_index] == values[copyKey]) {
                                        lcon = true
                                    }
                                }
                            }
                        } else {//后面段
                            if (conCopy == next) {
                                let copyKey = con[1].replace(/\./gi, '_')
                                for (let vk in relationMap[index]) {
                                    if (vk == values[copyKey]) {
                                        for (let vk in relationMap[index]) {
                                            if (_index == 1 && vk.split('#')[_index] == values[copyKey]) {
                                                rcon = true
                                            } else if (_index == 0 && vk.split('#')[_index] == values[copyKey]) {
                                                lcon = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {//前后段是什么
                        if (con[0] == 'pre') {//前面段
                            if (con[1] == pre) {
                                if (_index == 1) {
                                    rcon = true
                                } else {
                                    lcon = true
                                }
                            }
                        } else {//后面段
                            if (con[1] == next) {
                                if (_index == 1) {
                                    rcon = true
                                } else {
                                    lcon = true
                                }
                            }
                        }
                    }
                })
                if (lcon && rcon) {
                    for (let kk in relationMap[index]) {
                        let arr = relationMap[index][kk]
                        return arr
                    }
                }
            }

        })
    }

    //TODO
    //  return optionIndexs
    if (retu && retu.length > 0) {
        let r = []
        retu && retu.forEach((re) => {
            if (re) r.push(re)
        })
        if (r.length > 0) {
            return r[0]
        }
        return optionIndexs
    }
    return optionIndexs

}

const mapStateToProps = (state, ownProps) => {

    function getDefaultValidate() {
        return [].concat(ownProps.validate || []).concat(state.metadata.metaValidations[ownProps.id] || []);
    }

    function getFieldWarn() {
        return [].concat(ownProps.warn || []).concat(state.metadata.metaWarns[ownProps.id] || []);
    }
    let a = ownProps.from == 'batch' ? state.groups.groupSecList : state.ahu.componentValue
    // console.log('bb', state, state.metadata, ownProps)
    // console.log('bc', state.metadata)
    // let b = state.metadata.layout[ownProps.id]
    // console.log('zzf4444',b, ownProps.id)
    return {
        id: ownProps.id,
        validate: getDefaultValidate(),
        warn: getFieldWarn(),
        data: state.metadata.layout[ownProps.id],
        componentValues: a,
        // componentValues:   state.ahu.componentValue,
        selectedComponent: state.ahu.selectedComponent,

        propUnit: state.general.user && state.general.user.unitPrefer,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,

        ManualWarn: state.metadata.metaWarns[ownProps.id],
        warnsAlarm: state.metadata.warnsAlarm && state.metadata.warnsAlarm[ownProps.id],
        customDisabled: ownProps.disabled,
        unitPreferCode: state.general.user ? state.general.user.unitPreferCode : '',
        currentOptionName: state.ahu.currentOptionName,
        basicGroupSecList: state.groups.basicGroupSecList && state.groups.basicGroupSecList[0],
        batchComponentValue: state.groups.groupSecList[0],


    }
}

const mapDispatchToProps = (dispatch, ownProps) => {

    return {

        onFocus: (event, newValue, previousValue) => {
            event.target._focusValue = event.target.value;
        },
        onBlur: (event, newValue, previousValue, unitPreferCode) => {
            if (ownProps.from == 'batch') {
                return;
            } else {

                if (ownProps.onBlur) ownProps.onBlur(event, newValue, previousValue)
                if (event.target._focusValue !== undefined && event.target._focusValue !== newValue) {
                    // clearTimeout(this.timeout)
                    // this.timeout = setTimeout(() => {
                    if (ownProps.onBlurChange) ownProps.onBlurChange(event, newValue, event.target._focusValue)
                    // }, 2000)
                }


                event.target._focusValue = undefined;
                dispatch(syncOtherParams(event, newValue, previousValue, ownProps, unitPreferCode))
            }

        },
        onChange: (event, newValue, previousValue, propUnit, metaUnit, metaLayout, type) => {

            // console.log('zzf1', event, event.target.value, event.target.name, newValue)
            // let value = newValue
            // if (String(previousValue) === 'true') {
            //     value = false
            // }
            clearTimeout(this.timeout)
            if (ownProps.from == 'batch') {
                let newValue2 = event.target.value
                if (type == 'checkbox') {
                    newValue2 = (newValue == 'true' || newValue == true) ? true : false
                    newValue2 = (newValue == 'false' || newValue == false) ? false : true
                }
                dispatch(syncBatchValues(event.target.name, newValue2))

            } else {
                if (type == 'checkbox') {
                    newValue = (newValue == 'true' || newValue == true) ? true : false
                    newValue = (newValue == 'false' || newValue == false) ? false : true
                }

                this.timeout = setTimeout(() => {
                    if (ownProps.onChange) ownProps.onChange(event, newValue, previousValue)


                    // dispatch(syncAhuComponentValidate(event.target.name, newValue, previousValue))
                }, 100)
                dispatch(syncAhuComponentValues(event.target.name, newValue, previousValue, propUnit, metaUnit, metaLayout))
            }
        },
        doBatchComponentValue: (name, value) => {
            dispatch(syncBatchValues(name, value))

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Field)

