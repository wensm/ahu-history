import {
    OPTION,
    PERFORMANCE,
    FILTER_ARRANGEMENT,
    PARAMETER,
    ATTACHMENT,
    CALCULATION_RESULT,
    SEASON_TEMP,
    MIX_FORM,
    COIL_INFORMATION,
    OUTLET_AIR_FORM,
    FILTER_QUANTITY,
    FAN_COMPONENT,
    UNIT_GROUP,
    PANEL,
    STANDARD_CONFIGURATION,
    ACCESSORY,
    HEAT_RECYCLE_PARAMETER,
    SELECTION_RESULT,
    RESISTANCE_AND_WEIGHT,
    NON_STANDARD
} from '../intl/i18n'

import React from 'react'
import Link from 'react-router'
import classnames from 'classnames'
import intl from 'react-intl-universal'
import { connect } from 'react-redux'
import CombinedFilterContainer from '../section/combineFilter/CombinedFilterContainer'
import CombinedFilterNSContainer from '../section/combineFilter/CombinedFilterNSContainer'
import CoolingCoilContainer from '../section/coolingCoil/CoolingCoilContainer'
import CoolingCoilNSContainer from '../section/coolingCoil/CoolingCoilNSContainer'
import FilterContainer from '../section/filter/FilterContainer'
import FilterNSContainer from '../section/filter/FilterNSContainer'
import AHUFormContainer from '../section/ahu/AHUFormContainer'
import AHUFormNSContainer from '../section/ahu/AHUFormNSContainer'
import FanContainer from '../section/fan/FanContainer'
import FanNSContainer from '../section/fan/FanNSContainer'
import MixContainer from '../section/mix/MixContainer'
import MixNSContainer from '../section/mix/MixNSContainer'
import HeatingCoilContainer from '../section/heatingCoil/HeatingCoilContainer'
import HeatingCoilNSContainer from '../section/heatingCoil/HeatingCoilNSContainer'
import SteamCoilContainer from '../section/steamCoil/SteamCoilContainer'
import SteamCoilNSContainer from '../section/steamCoil/SteamCoilNSContainer'
import ElectricHeatingCoilContainer from '../section/electricHeatingCoil/ElectricHeatingCoilContainer'
import ElectricHeatingCoilNSContainer from '../section/electricHeatingCoil/ElectricHeatingCoilNSContainer'
import WetfilmHumidifierContainer from '../section/wetfilmHumidifier/WetfilmHumidifierContainer'
import WetfilmHumidifierNSContainer from '../section/wetfilmHumidifier/WetfilmHumidifierNSContainer'
import SprayHumidifierContainer from '../section/sprayHumidifier/SprayHumidifierContainer'
import SprayHumidifierNSContainer from '../section/sprayHumidifier/SprayHumidifierNSContainer'
import SteamHumidifierContainer from '../section/steamHumidifier/SteamHumidifierContainer'
import SteamHumidifierNSContainer from '../section/steamHumidifier/SteamHumidifierNSContainer'
import CombinedMixingChamberContainer from '../section/combinedMixingChamber/CombinedMixingChamberContainer'
import CombinedMixingChamberNSContainer from '../section/combinedMixingChamber/CombinedMixingChamberNSContainer'
import AccessContainer from '../section/access/AccessContainer'
import AccessNSContainer from '../section/access/AccessNSContainer'
import AttenuatorContainer from '../section/attenuator/AttenuatorContainer'
import AttenuatorNSContainer from '../section/attenuator/AttenuatorNSContainer'
import DischargeContainer from '../section/discharge/DischargeContainer'
import DischargeNSContainer from '../section/discharge/DischargeNSContainer'
import HEPAFilterContainer from '../section/HEPAFilter/HEPAFilterContainer'
import HEPAFilterNSContainer from '../section/HEPAFilter/HEPAFilterNSContainer'
import ElectrodeHumidifierContainer from '../section/electrodeHumidifier/ElectrodeHumidifierContainer'
import ElectrodeHumidifierNSContainer from '../section/electrodeHumidifier/ElectrodeHumidifierNSContainer'
import ElectrostaticFilterContainer from '../section/electrostaticFilter/ElectrostaticFilterContainer'
import ElectrostaticFilterNSContainer from '../section/electrostaticFilter/ElectrostaticFilterNSContainer'
import CtrContainer from '../section/ctr/CtrContainer'
import CtrNSContainer from '../section/ctr/CtrNSContainer'
import HeatRecycleContainer from '../section/heatRecycle/HeatRecycleContainer'
import HeatRecycleNSContainer from '../section/heatRecycle/HeatRecycleNSContainer'
import WheelHeatRecycleContainer from '../section/wheelHeatRecycle/WheelHeatRecycleContainer'
import WheelHeatRecycleContainerNS from '../section/wheelHeatRecycle/WheelHeatRecycleNSContainer'
import PlateHeatRecycleContainer from '../section/plateHeatRecycle/PlateHeatRecycleContainer'
import PlateHeatRecycleNSContainer from '../section/plateHeatRecycle/PlateHeatRecycleNSContainer'
import DirectExpensionCoilContainer from '../section/directExpensionCoil/DirectExpensionCoilContainer'
import DirectExpensionCoilNSContainer from '../section/directExpensionCoil/DirectExpensionCoilNSContainer'
import { setTab } from '../../actions/groups'
import { Button } from 'antd'
import style from './Form.css'

import {
    Form as AntdForm
  } from 'antd';

import FormItems from '../../commonComponents/formItem'



export default class Form extends React.Component {

    componentDidUpdate(prevProps, prevState) {
        if (!prevProps.selectedComponent || prevProps.selectedComponent.id != this.props.selectedComponent.id || (!prevProps.standard && this.props.standard) || (prevProps.standard && !this.props.standard)) {
            $('#componentForm').scrollTop(0)
            this.checkBtn();
        }

        this.checkScroll();

    }

    componentDidMount() {

        $('#myScrollspy').on('click', 'a[href^="#"]', function (e) {
            e.preventDefault();
            e.stopPropagation();
            //$(this).parent().addClass('active').siblings().removeClass('active');
            var top = $(this.hash).position() ? $(this.hash).position().top : 0,
                nt = $('#componentForm').scrollTop(),
                delta = top + nt;
            $('#componentForm').animate({ scrollTop: delta }, 400, () => { });
        });

        $('#componentForm').scroll(this.checkScroll);

    }

    checkBtn() {
        //将段设置 btnGroup 里面的按钮移动到导航条上去
        if ($('#componentForm .btnGroup.has-moved').length) {
            return;
        }
        $("#myScrollspy .btnContainer").children().remove()
        if ($('#componentForm .btnGroup').length) {
            $('#componentForm .btnGroup').addClass('has-moved').hide();
            if ($('#componentForm .btnGroup button').length) {
                $("#myScrollspy .btnContainer").children().remove()
                $("#myScrollspy .btnContainer").append($('#componentForm .btnGroup button'))
                $("#myScrollspy .btnContainer").append($('#componentForm .btnGroup i'))
            }
        }
    }

    checkScroll() {
        $('#myScrollspy li.dropdown').removeClass('active');
        var pHeight = $('#componentForm').height();
        var pOffset = $('#componentForm').offset().top;
        $("#componentForm [data-name='Group'][id]").each((i, item) => {
            $(item).css('borderLeft', '10px solid #55AAAA')
        })
        $("#componentForm [data-name='Group'][id]").each((i, item) => {
            var id = $(item).attr('id');
            var fItem = $(item);
            var pTop = fItem.offset().top - pOffset;

            var isInside = pTop >= 0 && (pTop < pHeight),
                isContained = pTop <= 0 && (fItem.height() + pTop >= pHeight),
                isMetLast = pTop <= 0 && fItem.offset().top + fItem.height() - pOffset >= pHeight - 40;

            if (isInside || isContained || isMetLast) {
                $('#myScrollspy a[href="#' + id + '"]').parent().addClass('active');
                $(`#componentForm #${id}`).each((ii, itemm) => {
                    $(item).css('borderLeft', '10px solid #11FFEE')
                })

            }
        })
    }

    clickNavi(e) {

        let theLink = e.target
        if (theLink) {
            let theHref = theLink.getAttribute("href")
            theHref = theHref.substr(1)
            setTab(theHref)
            const element = document.getElementById(theHref);
            if (element) element.scrollIntoView();
            e.preventDefault()
        }
        const mapStateToProps = state => {
            return {

            }
        }

    }

    render() {
        const {
            selectedComponent,
            standard,
        } = this.props
        let section = null
        let nav = [];
        switch (selectedComponent && selectedComponent.name) {
            case 'ahu.filter':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group5", stand: intl.get(PERFORMANCE) },
                    { group: "#group6", stand: intl.get(FILTER_ARRANGEMENT) },
                ]
                if (standard) {
                    section = <FilterContainer {...this.props} />
                } else {
                    section = <FilterNSContainer {...this.props} />
                }
                break;
            case 'ahu.fan':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group3", stand: intl.get(PARAMETER) },
                    { group: "#group5", stand: intl.get(ATTACHMENT) },
                    { group: "#group6", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <FanContainer {...this.props} />
                } else {
                    section = <FanNSContainer {...this.props} />
                }
                break;
            case 'ahu.mix':
                nav = [
                    { group: "#group6", stand: intl.get(SEASON_TEMP) },
                    { group: "#group1", stand: intl.get(MIX_FORM) },
                    { group: "#group3", stand: intl.get(OPTION) },
                    { group: "#group5", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <MixContainer {...this.props} />
                } else {
                    section = <MixNSContainer {...this.props} />
                }
                break;
            case 'ahu.combinedFilter':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group15", stand: intl.get(PERFORMANCE) },
                    { group: "#group6", stand: intl.get(FILTER_ARRANGEMENT) },
                ]
                if (standard) {
                    section = <CombinedFilterContainer {...this.props} />
                } else {
                    section = <CombinedFilterNSContainer {...this.props} />
                }
                break;
            case 'ahu.coolingCoil':
                nav = [
                    { group: "#group1", stand: intl.get(COIL_INFORMATION) },
                    { group: "#group3", stand: intl.get(OPTION) },
                    { group: "#group13", stand: intl.get(SEASON_TEMP) },
                    { group: "#group12", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <CoolingCoilContainer {...this.props} />
                } else {
                    section = <CoolingCoilNSContainer {...this.props} />
                }
                break;
            case 'ahu.heatingCoil':
                nav = [
                    { group: "#group1", stand: intl.get(COIL_INFORMATION) },
                    { group: "#group2", stand: intl.get(OPTION) },
                    { group: "#group21", stand: intl.get(SEASON_TEMP) },
                    { group: "#group3", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <HeatingCoilContainer {...this.props} />
                } else {
                    section = <HeatingCoilNSContainer {...this.props} />
                }
                break;
            case 'ahu.steamCoil':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group9", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <SteamCoilContainer {...this.props} />
                } else {
                    section = <SteamCoilNSContainer {...this.props} />
                }
                break;
            case 'ahu.electricHeatingCoil':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group9", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <ElectricHeatingCoilContainer {...this.props} />
                } else {
                    section = <ElectricHeatingCoilNSContainer {...this.props} />
                }
                break;
            case 'ahu.wetfilmHumidifier':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group0", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <WetfilmHumidifierContainer {...this.props} />
                } else {
                    section = <WetfilmHumidifierNSContainer {...this.props} />
                }
                break;
            case 'ahu.sprayHumidifier':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group0", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <SprayHumidifierContainer {...this.props} />
                } else {
                    section = <SprayHumidifierNSContainer {...this.props} />
                }
                break;
            case 'ahu.steamHumidifier':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group8", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <SteamHumidifierContainer {...this.props} />
                } else {
                    section = <SteamHumidifierNSContainer {...this.props} />
                }
                break;
            case 'ahu.combinedMixingChamber':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group8", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <CombinedMixingChamberContainer {...this.props} />
                } else {
                    section = <CombinedMixingChamberNSContainer {...this.props} />
                }
                break;
            case 'ahu.access':
                nav = [
                    { group: "#group2", stand: intl.get(OPTION) },
                    { group: "#group2", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <AccessContainer {...this.props} />
                    section = [[{
                        id: 'test1',
                        type: 'input'
                    }, {
                        id: 'test2',
                        type: 'checkbox'
                    }, {
                        id: 'test3',
                        type: 'select'
                    }], [{
                        id: 'test4',
                        type: 'input'
                    }, {
                        id: 'test5',
                        type: 'input'
                    }], [{
                        id: 'test6',
                        type: 'input'
                    }], [{
                        id: 'test7',
                        type: 'input'
                    }, {
                        id: 'test8',
                        type: 'input'
                    }]]
                } else {
                    section = <AccessNSContainer {...this.props} />
                }
                break;
            case 'ahu.attenuator':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group1", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <AttenuatorContainer {...this.props} />
                } else {
                    section = <AttenuatorNSContainer {...this.props} />
                }
                break;
            case 'ahu.discharge':
                nav = [
                    { group: "#group1", stand: intl.get(OUTLET_AIR_FORM) },
                    { group: "#group2", stand: intl.get(OPTION) },
                    { group: "#group3", stand: intl.get(RESISTANCE_AND_WEIGHT) },
                ]
                if (standard) {
                    section = <DischargeContainer {...this.props} />
                } else {
                    section = <DischargeNSContainer {...this.props} />
                }
                break;
            case 'ahu.HEPAFilter':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group4", stand: intl.get(PERFORMANCE) },
                    { group: "#group6", stand: intl.get(FILTER_QUANTITY) },
                ]
                if (standard) {
                    section = <HEPAFilterContainer {...this.props} />
                } else {
                    section = <HEPAFilterNSContainer {...this.props} />
                }
                break;
            case 'ahu.electrodeHumidifier':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group7", stand: intl.get(OPTION) },
                    { group: "#group8", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <ElectrodeHumidifierContainer {...this.props} />
                } else {
                    section = <ElectrodeHumidifierNSContainer {...this.props} />
                }
                break;
            case 'ahu.electrostaticFilter':
                nav = [
                    { group: "#group1", stand: intl.get(OPTION) },
                    { group: "#group4", stand: intl.get(PERFORMANCE) },
                    { group: "#group6", stand: intl.get(FILTER_ARRANGEMENT) },
                ]
                if (standard) {
                    section = <ElectrostaticFilterContainer {...this.props} />
                } else {
                    section = <ElectrostaticFilterNSContainer {...this.props} />
                }
                break;
            case 'ahu.ahu':
                nav = [
                    { group: "#group1", stand: intl.get(FAN_COMPONENT) },
                    { group: "#group2", stand: intl.get(UNIT_GROUP) },
                    { group: "#group4", stand: intl.get(PANEL) },
                    { group: "#group5", stand: intl.get(OPTION) },
                ]
                if (standard) {
                    section = <AHUFormContainer {...this.props} />
                } else {
                    section = <AHUFormNSContainer {...this.props} />
                }
                break;
            case 'ahu.ctr':
                nav = [
                    { group: "#group1", stand: intl.get(STANDARD_CONFIGURATION) },
                    { group: "#group2", stand: intl.get(ACCESSORY) },
                    { group: "#group3", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <CtrContainer {...this.props} />
                } else {
                    section = <CtrNSContainer {...this.props} />
                }
                break;
            case 'ahu.heatRecycle':
                if (standard) {
                    section = <HeatRecycleContainer {...this.props} />
                } else {
                    section = <HeatRecycleNSContainer {...this.props} />
                }
                break;
            case 'ahu.wheelHeatRecycle':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group8", stand: intl.get(HEAT_RECYCLE_PARAMETER) },
                    { group: "#group7", stand: intl.get(PERFORMANCE) },
                    { group: "#group10", stand: intl.get(SELECTION_RESULT) },
                ]
                if (standard) {
                    section = <WheelHeatRecycleContainer {...this.props} />
                } else {
                    section = <WheelHeatRecycleContainer {...this.props} />

                    // section = <WheelHeatRecycleContainerNS {...this.props} />
                }
                break;
            case 'ahu.plateHeatRecycle':
                nav = [
                    { group: "#group1", stand: intl.get(SEASON_TEMP) },
                    { group: "#group8", stand: intl.get(HEAT_RECYCLE_PARAMETER) },
                    { group: "#group7", stand: intl.get(PERFORMANCE) },
                    { group: "#group10", stand: intl.get(SELECTION_RESULT) },
                ]
                if (standard) {
                    section = <PlateHeatRecycleContainer {...this.props} />
                } else {
                    section = <PlateHeatRecycleContainer {...this.props} />

                    // section = <PlateHeatRecycleNSContainer {...this.props} />
                }
                break;
            case 'ahu.directExpensionCoil':
                nav = [
                    { group: "#group1", stand: intl.get(COIL_INFORMATION) },
                    { group: "#group3", stand: intl.get(OPTION) },
                    { group: "#group13", stand: intl.get(SEASON_TEMP) },
                    { group: "#group12", stand: intl.get(PERFORMANCE) },
                ]
                if (standard) {
                    section = <DirectExpensionCoilContainer {...this.props} />
                } else {
                    section = <DirectExpensionCoilNSContainer {...this.props} />
                }
                break;
            default:

        }
        if (!standard) {
            nav = [
                { group: "#group1", stand: intl.get(NON_STANDARD) },
            ]
        }

        let a = nav.map((name, nub) => {
            return (<li className="dropdown" key={nub}>
                <a href={name.group} id="navbarDrop1" className="dropdown-toggle"
                    onClick={this.clickNavi} data-toggle="dropdown">
                    {name.stand}
                </a>
            </li>)
        })


        let formLayout = 'vertical'

        return (
            <div>
                <div className="bs-example" data-example-id="embedded-scrollspy">
                    <nav id="myScrollspy" className="navbar navbar-default navbar-static" style={{ "marginBottom": "0px" }} role="navigation">
                        <div className="container-fluid">
                            <div className="collapse navbar-collapse bs-js-navbar-scrollspy">
                                <ul className="nav navbar-nav">
                                    {a}
                                </ul>
                                <div className="pull-right btnContainer" style={{ paddingTop: "0" }}>

                                </div>
                            </div>
                        </div>
                    </nav>
                </div>

                <div id="componentForm" data-name="Form" data-target="#myScrollspy" data-offset="0"
                    style={{
                        height: window.innerHeight - 295,
                        overflow: "auto", position: "relative", paddingTop: "6px"
                    }}>
                    {selectedComponent && selectedComponent.name != 'ahu.access' && section}
                    {
                        <AntdForm layout={formLayout}>
                            {section && section.length > 0 && section.map((sec) => {
                                return (
                                    <div style={{ border: '1px solid #ddd' }}>
                                        <FormItems data={sec} />
                                    </div>
                                )
                            })}
                        </AntdForm>
                    }
                </div>
            </div>
        )
    }
}
//295
