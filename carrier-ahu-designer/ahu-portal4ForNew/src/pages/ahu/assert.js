
export default function assert(componentValue, assertion) {
  switch (true) {
    case assertion.startsWith('>'):
      return Number(componentValue) > Number(assertion.substr(1, assertion.length - 1))
    case assertion.startsWith('<'):
      return Number(componentValue) < Number(assertion.substr(1, assertion.length - 1))
    case assertion.startsWith('!'):
      return String(componentValue) !== String(assertion.substr(1, assertion.length - 1))
    case assertion.startsWith('startw:'):
        return String(componentValue).startsWith(String(assertion.substr(7, assertion.length - 1)))
    case assertion.startsWith('endw:'):
        return String(componentValue).endsWith(String(assertion.substr(5, assertion.length - 1)))
    case assertion.startsWith('contain:'):
        return (String(componentValue).indexOf(String(assertion.substr(8, assertion.length - 1)))!=-1)
    default:
      return String(componentValue) === String(assertion)
  }
}
