import {
    PLEASE_CHOOSE,
    IMPORT_UNIT,
    IMPORT_PROJECT,
    CHOOSE_FILE,
    BROWSING,
    CANCEL,
    IMPORT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import Select from 'react-select';
import SelectStyle from 'react-select/dist/react-select.min.css';


export default class ImportAhu extends React.Component {
  constructor(props) {
    super(props);
    this.selFileBtn = this.selFileBtn.bind(this);
    this.selFileChange = this.selFileChange.bind(this);
    this.saveImportAhu = this.saveImportAhu.bind(this);
    this.setCoverItem = this.setCoverItem.bind(this);

    this.state = {
      selectedCoverId: "",
      fileName:""
    };
  }

  selFileBtn() {
    $("#configInputFile").click();
  }

  selFileChange() {
    var value = $("#configInputFile").val()||'';
    var indenNum = value.lastIndexOf('\\');
    var fileName = value.substring((indenNum + 1));
    this.setState({
      fileName:fileName
    });
    return true;
  }

  saveImportAhu() {
    var ahuId = $("#ImportAhu").data("ahuId");
    var projectId = $("#ImportAhu").data("projectId");
    var formData = new FormData();
    formData.append("myfile", document.getElementById("configInputFile").files[0]);
    this.props.saveImportAhuData(formData,projectId ,ahuId);
  }

  setCoverItem(selectedOption) {
    this.setState({selectedCoverId: selectedOption ? selectedOption.value : ''});
  }

  render() {
    const {coverList, coverId, coverName,coverOption,projectId} = this.props;

    let selectOptions = [{
      value: "",
      label: intl.get(PLEASE_CHOOSE)
    }];
    (coverList||[]).forEach((a) => {
      let op = {value: a[coverId || 'value'], label: a[coverName || 'label']};
      selectOptions.push(op);
    });

    return (
      <div className="modal fade" id="ImportAhu" data-name="ImportAhu" tabIndex="-1" role="dialog"
           aria-labelledby="myModalLabel" data-ahuId="" data-projectId="">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="myModalLabel"><b>{projectId?intl.get(IMPORT_UNIT):intl.get(IMPORT_PROJECT)}</b>
              </h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-3">
                  <label style={{lineHeight: '32px'}}>{intl.get(CHOOSE_FILE)}</label>
                </div>
                <div className="col-lg-6">
                  <input className="form-control" id="fileInput" readOnly value={this.state.fileName} name="trueattachment"
                         type="text"/>
                  <input style={{display: 'none'}} type="file" id="configInputFile"
                         onChange={this.selFileChange}/>
                </div>
                <div className="col-lg-2">
                  <button className="btn btn-default" type="button"
                          onClick={this.selFileBtn}>{intl.get(BROWSING)}</button>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default"
                      data-dismiss="modal">{intl.get(CANCEL)}</button>
              <button type="button" className="btn btn-primary"
                      onClick={this.saveImportAhu}>{intl.get(IMPORT)}</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
