import { connect } from 'react-redux'
import Form from './Form'
import {
    confirmSection,
    fetchRelativeHumidity,
    fetchWetBulbTemperature,
    fetchJSBHumidificationVolume,
    fetchIsoenthalpyJSBHumidificationVolume,
    fetchIsothermalJSBHumidificationVolume,
    fetchCalcHeatingQ,
    fetchCalcHumDryBulbT,
    fetchJSBRelativeHumidity,
    fetchIsoenthalpyJSBRelativeHumidity,
    fetchIsothermalJSBRelativeHumidity,
    fetchChooseModel,
    fetchSteamHumidifierChooseModel,
    calcComponentValue,
    fetchCalcMixAir1,
    fetchCalcMixAir2,
    fetchCalcMixAir4,
    calculateUnitValueToMetric2,
    // calculateUnitValueToMetric2,
    calculateEHC
} from '../../actions/ahu'

const mapStateToProps = (state, ownProps) => {
    // let initialValues = calculateUnitValues(state.ahu.componentValue[state.ahu.selectedComponent.id],
    //     state.general.user.unitPrefer,
    //     state.metadata.metaUnit,
    //     state.metadata.layout);
    return {
        selectedComponent: state.ahu.selectedComponent,
        projectId: ownProps.projectId,
        ahuId: ownProps.ahuId,
        componentValue: state.ahu.componentValue[state.ahu.selectedComponent.id],
        componentValues: state.ahu.componentValue,
        standard: state.ahu.standard,
        values: state.form.component,
        canConfirm: state.ahu.validation.sectionPass,
        propUnit: state.general.user && state.general.user.unitPreferCode,//M或者B
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
        unitSetting: state.metadata.metaUnitSetting,
        // componentValue:state.ahu.componentValue
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onCalcRelativeHumidity: (dryBulbTemperature, wetBulbTemperature, name, self, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let dryBulbTemperature2 = ownProps.componentValue[id][dryBulbTemperature]
            let wetBulbTemperature2 = wetBulbTemperature
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(dryBulbTemperature, ownProps.componentValue[id][dryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(self, wetBulbTemperature, unitSetting['B'], metaUnit, metaLayout)
            }
            // console.log('dryBulbTemperature, wetBulbTemperature, name, self', dryBulbTemperature, wetBulbTemperature, name, self)
            dispatch(fetchRelativeHumidity(dryBulbTemperature2, wetBulbTemperature2, name, self, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalcRelativeHumidity2: (dryBulbTemperature, wetBulbTemperature, name, self, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let dryBulbTemperature2 = ownProps.componentValue[id][dryBulbTemperature]
            let wetBulbTemperature2 = wetBulbTemperature
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(dryBulbTemperature, ownProps.componentValue[id][dryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(self, wetBulbTemperature, unitSetting['B'], metaUnit, metaLayout)
            }
            // console.log('dryBulbTemperature, wetBulbTemperature, name, self', dryBulbTemperature, wetBulbTemperature, name, self)
            dispatch(fetchRelativeHumidity(wetBulbTemperature2,dryBulbTemperature2, name, self, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalcWetBulbTemperature: (dryBulbTemperature, relativeHumidity, name, self, unitSetting, metaUnit, metaLayout, propUnit) => {
            // console.log('onCalcWetBulbTemperature', dryBulbTemperature, relativeHumidity, name, self, unitSetting, metaUnit, metaLayout, propUnit)
            let id = ownProps.selectedComponent.id
            let dryBulbTemperature2 = ownProps.componentValue[id][dryBulbTemperature]
            let relativeHumidity2 = relativeHumidity
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(dryBulbTemperature, ownProps.componentValue[id][dryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                relativeHumidity2 = calculateUnitValueToMetric2(self, relativeHumidity, unitSetting['B'], metaUnit, metaLayout)
            }            
            

            dispatch(fetchWetBulbTemperature(dryBulbTemperature2, relativeHumidity2, name, self, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalIsoenthalpyJSBHumidificationVolume: (DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            // console.log('onCalIsoenthalpyJSBHumidificationVolume', DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys)
            let dryBulbTemperature2 = ownProps.componentValue[id][DryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][WetBulbTemperature]
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(DryBulbTemperature, ownProps.componentValue[id][DryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(WetBulbTemperature, ownProps.componentValue[id][WetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }            

            dispatch(fetchIsoenthalpyJSBHumidificationVolume(dryBulbTemperature2, wetBulbTemperature2, RelativeHumidity, eairvolume2, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalIsothermalJSBHumidificationVolume: (DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            // console.log('onCalIsothermalJSBHumidificationVolume', DryBulbTemperature, WetBulbTemperature, RelativeHumidity, eairvolume, names, resultKeys)
            let dryBulbTemperature2 = ownProps.componentValue[id][DryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][WetBulbTemperature]
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(DryBulbTemperature, ownProps.componentValue[id][DryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(WetBulbTemperature, ownProps.componentValue[id][WetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }
            dispatch(fetchIsothermalJSBHumidificationVolume(dryBulbTemperature2, wetBulbTemperature2, RelativeHumidity, eairvolume2, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalcHeatingQ: (season, InDryBulbTemperature, InWetBulbTemperature, InRelativeHumidity, HumDryBulbTemperature, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'

            let dryBulbTemperature2 = ownProps.componentValue[id][InDryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][InWetBulbTemperature]
            let relativeHumidity2 = ownProps.componentValue[id][InRelativeHumidity]
            let HumDryBulbTemperature2 = HumDryBulbTemperature
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            // console.log('onCalcHeatingQ', season, InDryBulbTemperature, InWetBulbTemperature, InRelativeHumidity, HumDryBulbTemperature, names, resultKeys)
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(InDryBulbTemperature, ownProps.componentValue[id][InDryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(InWetBulbTemperature, ownProps.componentValue[id][InWetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                relativeHumidity2 = calculateUnitValueToMetric2(InRelativeHumidity, ownProps.componentValue[id][InRelativeHumidity], unitSetting['B'], metaUnit, metaLayout)
                HumDryBulbTemperature2 = calculateUnitValueToMetric2(names[4], HumDryBulbTemperature, unitSetting['B'], metaUnit, metaLayout)
                // console.log('HumDryBulbTemperature2', names[4], HumDryBulbTemperature2, HumDryBulbTemperature)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }

            dispatch(fetchCalcHeatingQ(ownProps.selectedComponent.name, season, eairvolume2, dryBulbTemperature2, wetBulbTemperature2, relativeHumidity2, HumDryBulbTemperature2, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalcHumDryBulbT: (season, InDryBulbTemperature, InWetBulbTemperature, InRelativeHumidity, HeatQ, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            let dryBulbTemperature2 = ownProps.componentValue[id][InDryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][InWetBulbTemperature]
            let relativeHumidity2 = ownProps.componentValue[id][InRelativeHumidity]
            let heatQ2 = HeatQ
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(InDryBulbTemperature, ownProps.componentValue[id][InDryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(InWetBulbTemperature, ownProps.componentValue[id][InWetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                relativeHumidity2 = calculateUnitValueToMetric2(InRelativeHumidity, ownProps.componentValue[id][InRelativeHumidity], unitSetting['B'], metaUnit, metaLayout)
                heatQ2 = calculateUnitValueToMetric2(names[4], HeatQ, unitSetting['B'], metaUnit, metaLayout)
                // console.log('HumDryBulbTemperature2', names,HeatQ ,heatQ2, unitSetting, metaUnit, metaLayout, propUnit)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }
            dispatch(fetchCalcHumDryBulbT(ownProps.selectedComponent.name, season, eairvolume2, dryBulbTemperature2, wetBulbTemperature2, relativeHumidity2, heatQ2, names, resultKeys, ownProps.componentValue[0]['meta_ahu_serial'], unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalIsoenthalpycJSBRelativeHumidity: (DryBulbTemperature, WetBulbTemperature, humidification, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            // console.log('123', DryBulbTemperature, WetBulbTemperature, humidification, eairvolume, names, resultKeys)
            let dryBulbTemperature2 = ownProps.componentValue[id][DryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][WetBulbTemperature]
            let humidification2 = humidification
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(DryBulbTemperature, ownProps.componentValue[id][DryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(WetBulbTemperature, ownProps.componentValue[id][WetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                humidification2 = calculateUnitValueToMetric2(names[4], humidification, unitSetting['B'], metaUnit, metaLayout)
                // console.log('HumDryBulbTemperature2', names,HeatQ ,heatQ2, unitSetting, metaUnit, metaLayout, propUnit)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }

            dispatch(fetchIsoenthalpyJSBRelativeHumidity(dryBulbTemperature2, wetBulbTemperature2, humidification2, eairvolume2, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalIsothermalcJSBRelativeHumidity: (DryBulbTemperature, WetBulbTemperature, humidification, eairvolume, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            let dryBulbTemperature2 = ownProps.componentValue[id][DryBulbTemperature]
            let wetBulbTemperature2 = ownProps.componentValue[id][WetBulbTemperature]
            let humidification2 = humidification
            let eairvolume2 = ownProps.componentValue[0][windVolumeName]
            if(propUnit && propUnit == 'B'){
                dryBulbTemperature2 = calculateUnitValueToMetric2(DryBulbTemperature, ownProps.componentValue[id][DryBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                wetBulbTemperature2 = calculateUnitValueToMetric2(WetBulbTemperature, ownProps.componentValue[id][WetBulbTemperature], unitSetting['B'], metaUnit, metaLayout)
                humidification2 = calculateUnitValueToMetric2(names[4], humidification, unitSetting['B'], metaUnit, metaLayout)
                // console.log('HumDryBulbTemperature2', names,HeatQ ,heatQ2, unitSetting, metaUnit, metaLayout, propUnit)
                eairvolume2 = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                
            }
            dispatch(fetchIsothermalJSBRelativeHumidity(dryBulbTemperature2, wetBulbTemperature2, humidification2, eairvolume2, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        //Mix1
        onCalcMixAir1: (NARatio, NewDryBulbT, NewWetBulbT, InDryBulbT, InWetBulbT, names, resultKeys, newValue, name, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            let obj = {
                airVolume: ownProps.componentValue[0][windVolumeName],
                NARatio: name == 'NARatio' ? newValue : ownProps.componentValue[id][NARatio],
                NewDryBulbT: name == 'NewDryBulbT' ? newValue : ownProps.componentValue[id][NewDryBulbT],
                NewWetBulbT: name == 'NewWetBulbT' ? newValue : ownProps.componentValue[id][NewWetBulbT],
                InDryBulbT: name == 'InDryBulbT' ? newValue : ownProps.componentValue[id][InDryBulbT],
                InWetBulbT: name == 'InWetBulbT' ? newValue : ownProps.componentValue[id][InWetBulbT],
            }
            if (propUnit == 'B') {
                let airVolume = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                let NARatio2 = calculateUnitValueToMetric2(NARatio, ownProps.componentValue[id][NARatio], unitSetting['B'], metaUnit, metaLayout)
                let NewDryBulbT2 = calculateUnitValueToMetric2(NewDryBulbT, ownProps.componentValue[id][NewDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                let NewWetBulbT2 = calculateUnitValueToMetric2(NewWetBulbT, ownProps.componentValue[id][NewWetBulbT], unitSetting['B'], metaUnit, metaLayout)
                let InDryBulbT2 = calculateUnitValueToMetric2(InDryBulbT, ownProps.componentValue[id][InDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                let InWetBulbT2 = calculateUnitValueToMetric2(InWetBulbT, ownProps.componentValue[id][InWetBulbT], unitSetting['B'], metaUnit, metaLayout)
                let newName = `meta_section_mix_${name}`
                let newValue2 = calculateUnitValueToMetric2(newName, newValue, unitSetting['B'], metaUnit, metaLayout)
                obj = {
                    airVolume: airVolume,
                    NARatio: name == 'NARatio' ? newValue2 : NARatio2,
                    NewDryBulbT: name == 'NewDryBulbT' ? newValue2 : NewDryBulbT2,
                    NewWetBulbT: name == 'NewWetBulbT' ? newValue2 : NewWetBulbT2,
                    InDryBulbT: name == 'InDryBulbT' ? newValue2 : InDryBulbT2,
                    InWetBulbT: name == 'InWetBulbT' ? newValue2 : InWetBulbT2,
                }
            }
            dispatch(fetchCalcMixAir1(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        //mix2
        onCalcMixAir2: (NARatio, NewDryBulbT, NewRelativeT, InDryBulbT, InRelativeT, names, resultKeys, newValue, name, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'

            let obj = {
                airVolume: ownProps.componentValue[0][windVolumeName],
                NARatio: name == 'NARatio' ? newValue : ownProps.componentValue[id][NARatio],
                NewDryBulbT: name == 'NewDryBulbT' ? newValue : ownProps.componentValue[id][NewDryBulbT],
                NewRelativeT: name == 'NewRelativeT' ? newValue : ownProps.componentValue[id][NewRelativeT],
                InDryBulbT: name == 'InDryBulbT' ? newValue : ownProps.componentValue[id][InDryBulbT],
                InRelativeT: name == 'InRelativeT' ? newValue : ownProps.componentValue[id][InRelativeT],
            }
            if (propUnit == 'B') {
                let airVolume = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                let NARatio2 = calculateUnitValueToMetric2(NARatio, ownProps.componentValue[id][NARatio], unitSetting['B'], metaUnit, metaLayout)
                let NewDryBulbT2 = calculateUnitValueToMetric2(NewDryBulbT, ownProps.componentValue[id][NewDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                let NewRelativeT2 = calculateUnitValueToMetric2(NewRelativeT, ownProps.componentValue[id][NewRelativeT], unitSetting['B'], metaUnit, metaLayout)
                let InDryBulbT2 = calculateUnitValueToMetric2(InDryBulbT, ownProps.componentValue[id][InDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                let InRelativeT2 = calculateUnitValueToMetric2(InRelativeT, ownProps.componentValue[id][InRelativeT], unitSetting['B'], metaUnit, metaLayout)
                let newName = `meta_section_mix_${name}`
                let newValue2 = calculateUnitValueToMetric2(newName, newValue, unitSetting['B'], metaUnit, metaLayout)
                
                obj = {
                    airVolume: airVolume,
                    NARatio: name == 'NARatio' ? newValue2 : NARatio2,
                    NewDryBulbT: name == 'NewDryBulbT' ? newValue2 : NewDryBulbT2,
                    NewRelativeT: name == 'NewRelativeT' ? newValue2 : NewRelativeT2,
                    InDryBulbT: name == 'InDryBulbT' ? newValue2 : InDryBulbT2,
                    InRelativeT: name == 'InRelativeT' ? newValue2 : InRelativeT2,
                }
            }
            dispatch(fetchCalcMixAir2(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        //mix4
        onCalcMixAir4: (NAVolume, NewDryBulbT, NewWetBulbT, InDryBulbT, InWetBulbT, names, resultKeys, newValue, name, unitSetting, metaUnit, metaLayout, propUnit) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            let obj = {
                airVolume: ownProps.componentValue[0][windVolumeName],
                NAVolume: name == 'NAVolume' ? newValue : ownProps.componentValue[id][NAVolume],
                NewDryBulbT: name == 'NewDryBulbT' ? newValue : ownProps.componentValue[id][NewDryBulbT],
                NewWetBulbT: name == 'NewWetBulbT' ? newValue : ownProps.componentValue[id][NewWetBulbT],
                InDryBulbT: name == 'InDryBulbT' ? newValue : ownProps.componentValue[id][InDryBulbT],
                InWetBulbT: name == 'InWetBulbT' ? newValue : ownProps.componentValue[id][InWetBulbT],
            }
            if (propUnit == 'B') {
                let airVolume = calculateUnitValueToMetric2(windVolumeName, ownProps.componentValue[0][windVolumeName], unitSetting['B'], metaUnit, metaLayout)
                let NAVolume2 = calculateUnitValueToMetric2(NAVolume, ownProps.componentValue[id][NAVolume], unitSetting['B'], metaUnit, metaLayout)
                let NewDryBulbT2 = calculateUnitValueToMetric2(NewDryBulbT, ownProps.componentValue[id][NewDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                
                let NewWetBulbT2 = calculateUnitValueToMetric2(NewWetBulbT, ownProps.componentValue[id][NewWetBulbT], unitSetting['B'], metaUnit, metaLayout)
                let InDryBulbT2 = calculateUnitValueToMetric2(InDryBulbT, ownProps.componentValue[id][InDryBulbT], unitSetting['B'], metaUnit, metaLayout)
                let InWetBulbT2 = calculateUnitValueToMetric2(InWetBulbT, ownProps.componentValue[id][InWetBulbT], unitSetting['B'], metaUnit, metaLayout)
                let newName = `meta_section_mix_${name}`
                let newValue2 = calculateUnitValueToMetric2(newName, newValue, unitSetting['B'], metaUnit, metaLayout)
                obj = {
                    airVolume: airVolume,
                    NAVolume: name == 'NAVolume' ? newValue2 : NAVolume2,
                    NewDryBulbT: name == 'NewDryBulbT' ? newValue2 : NewDryBulbT2,
                    NewWetBulbT: name == 'NewWetBulbT' ? newValue2 : NewWetBulbT2,
                    InDryBulbT: name == 'InDryBulbT' ? newValue2 : InDryBulbT2,
                    InWetBulbT: name == 'InWetBulbT' ? newValue2 : InWetBulbT2,
                }
                
            }
            dispatch(fetchCalcMixAir4(obj, names, resultKeys, unitSetting, metaUnit, metaLayout, propUnit))
        },
        onCalcOutletParams: (freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN, names, resultKeys, unitSetting, metaUnit, metaLayout) => {
            let id = ownProps.selectedComponent.id
            let windDirection = ownProps.componentValue[id]['meta_section_airDirection']
            let windVolumeName = windDirection == 'S' ? 'meta_ahu_sairvolume' : 'meta_ahu_eairvolume'
            dispatch(fetchOutletParams(
                ownProps.componentValue[0][windVolumeName],
                ownProps.componentValue[id][freshAirRate],
                ownProps.componentValue[id][freshAirTIN],
                ownProps.componentValue[id][freshAirTbIN],
                ownProps.componentValue[id][returnAirTIN],
                ownProps.componentValue[id][returnTbIN], names, resultKeys))
        },
        onChooseModel: (componentValues, selectedComponent, season) => {
            let id = ownProps.selectedComponent.id

            dispatch(fetchChooseModel(season, ownProps.componentValue[id]['meta_section_sprayHumidifier_SHumidificationQ'], ownProps.componentValue[id]['meta_section_sprayHumidifier_WHumidificationQ'], "B"))
        },
        onSteamHumidifierChooseModel: (componentValues, selectedComponent) => {//zzf 选择型号 
            let id = ownProps.selectedComponent.id

            const summer = ownProps.componentValue[id]['meta_section_steamHumidifier_EnableSummer'];
            const winter = ownProps.componentValue[id]['meta_section_steamHumidifier_EnableWinter'];
            let season = null
            if (summer && winter) {
                season = "SW"
            } else if (winter) {
                season = "W"
            } else {
                season = "S"
            }
            dispatch(fetchSteamHumidifierChooseModel(season, ownProps.componentValue[id]['meta_section_steamHumidifier_SHumidificationQ'], ownProps.componentValue[id]['meta_section_steamHumidifier_WHumidificationQ'], "B"))
        },
        onCalcCoolingCoil: (componentValues, selectedComponent, season, unitSetting, metaUnit, metaLayout, propUnit) => {
            let copythePropsValues = { ...componentValues }
            // console.log('zzf222',  projectId, ahuId, componentValue, sectionsNum, layout, bool = true, unitSetting, metaUnit, metaLayout, propUnit)
            if (propUnit == 'B') {//B 是 英制,需要切到公制传给后端
                for (let key in copythePropsValues) {
                    let currentSave = { ...copythePropsValues[key] }
                    for (let _key in currentSave) {
                        if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            let a = calculateUnitValueToMetric2(_key, currentSave[_key], unitSetting['B'], metaUnit, metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            currentSave[_key] = a
                        }
                    }
                    copythePropsValues[key] = currentSave
                }
            }
            //            console.log('zzff', copythePropsValues, componentValues)

            const ahu = copythePropsValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)
            const sections = []
            let position = null
            $('.ahu-section-list').children().each((index, element) => {
                const sectionId = $(element).attr('data-section-id')
                const sectionProps = copythePropsValues[sectionId]
                if (Number(sectionId) === Number(selectedComponent.id)) {
                    position = index + 1
                }
                const newSectionProps = {}
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                    }
                }
                const section = {
                    pid: ownProps.projectId,
                    unitid: ownProps.ahuId,
                    key: $(element).attr('data-section'),
                    position: index + 1,
                    metaJson: JSON.stringify(newSectionProps),
                    partid: $(element).attr('data-partid'),
                }
                if(sectionId == selectedComponent.id){
                    sections.push(section)
                }
            })
            const sectionStr = JSON.stringify(sections)
            dispatch(calcComponentValue(ownProps.projectId, ownProps.ahuId, ahuStr, sectionStr, position, 'ahu.coolingCoil', season, propUnit))
        },
        onCalculateEHC: (componentValues, selectedComponent, season, unitSetting, metaUnit, metaLayout, propUnit, callback) => {

            let copythePropsValues = { ...componentValues }
            if (propUnit == 'B') {//B 是 英制,需要切到公制传给后端
                for (let key in copythePropsValues) {
                    let currentSave = { ...copythePropsValues[key] }
                    for (let _key in currentSave) {
                        if (metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType'] == 'IntV')) {
                            let a = calculateUnitValueToMetric2(_key, currentSave[_key], unitSetting['B'], metaUnit, metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            currentSave[_key] = a
                        }
                    }
                    copythePropsValues[key] = currentSave
                }
            }
            //            console.log('zzff', copythePropsValues, componentValues)

            const ahu = copythePropsValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)
            const sections = []
            let position = null
            $('.ahu-section-list').children().each((index, element) => {
                const sectionId = $(element).attr('data-section-id')
                const sectionProps = copythePropsValues[sectionId]
                if (Number(sectionId) === Number(selectedComponent.id)) {
                    position = index + 1
                }
                const newSectionProps = {}
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                    }
                }
                const section = {
                    pid: ownProps.projectId,
                    unitid: ownProps.ahuId,
                    key: $(element).attr('data-section'),
                    position: index + 1,
                    metaJson: JSON.stringify(newSectionProps),
                    partid: $(element).attr('data-partid'),
                }
                sections.push(section)
            })
            const sectionStr = JSON.stringify(sections)
            dispatch(calculateEHC(ownProps.projectId, ownProps.ahuId, ahuStr, sectionStr, position, '', season, propUnit, callback))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
