import {
    OK_SELECT_IT,
    BATCH_SELECTION_IN_QUOTATION_PHASE_ONLY,
    OK,
    CANCEL,
    CONFIGURATION_FAILED,
    CONFIGURATION_SUCCESS,
    BATCH_SELECTION_SETTING,
    CONFIGURATION_RULE,
    MOST_ECONOMICAL,
    OPTIMAL_PERFORMANCE,
    QUICK_CONFIG,
    BATCH_PROCESSING_ESC,
    KEEP_PERFORMANCE,
    CUSTOMIZE,
    MINIMUMEFFICIENCY,
    MAXIMUMCOILFACE,
    MAXIMUMFANOUTLET,
    ONE_CLICK_SELECTION
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'

import SocketPanel from './SocketPanel'
import sweetalert from 'sweetalert'
import { Checkbox, Input, Col } from 'antd'

export default class BatchCalculation extends React.Component {
    constructor(props) {
        super(props)
        this.saveQutSet = this.saveQutSet.bind(this)
        this.onSaveFinish = this.onSaveFinish.bind(this)
        this.state = {
            checked: false,
            minimumEfficiencyOfFan: 40,
            maximumCoilFaceVelocity: 3,
            maximumFanOutletVelocity: 14
        }
    }
    close() {
        $('#BatchCalculation').modal('hide');
        this.setState({
            checked: false,
            minimumEfficiencyOfFan: 40,
            maximumCoilFaceVelocity: 3,
            maximumFanOutletVelocity: 14
        })
    }
    saveQutSet(isButton) {
        var selType = $("input[name=judge-select]:checked").attr("id")
        var groupId = $("#BatchCalculation").data('groupid')
        var ahuId = $("#BatchCalculation").data('ahuId')
        $('#BatchCalculation').modal('hide');
        this.close()

        let obj = { ...this.state }
        if (this.state.checked) {
            delete obj.checked
        } else {
            obj = ''
        }
        // console.log('this', obj)
        sweetalert({
            title: intl.get(OK_SELECT_IT),
            text: intl.get(BATCH_SELECTION_IN_QUOTATION_PHASE_ONLY),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: intl.get(OK),
            cancelButtonText: intl.get(CANCEL),
            closeOnConfirm: true
        }, isConfirm => {
            if (isConfirm) {
                $("#isCalcIng").modal('show')
                $("#isCalcIng").one('hidden.bs.modal', () => {
                    this.refs.socketPanel.stop();
                });
                let socketUrl = "ws://" + window.location.hostname + ":20000/websocket/" + this.props.projectId;
                this.refs.socketPanel.start(socketUrl)

                this.props.onSaveQutSet(groupId, ahuId, selType, isButton, obj, () => {

                }, (err) => {
                    $("#isCalcIng").modal('hide')
                    this.refs.socketPanel.stop();
                    sweetalert({
                        title: intl.get(CONFIGURATION_FAILED),
                        text: '',
                        type: 'error'
                    })
                })
            }
        })
    }

    onSaveError() {
        $("#isCalcIng").modal("hide");
        sweetalert({
            title: intl.get(CONFIGURATION_FAILED),
            text: '',
            type: 'error',
            timer: 1000, showConfirmButton: false
        })
    }

    onSaveFinish(isButton) {//isButton为false是项目界面的一键选型或者批量选型，为true是ahu里面的一键选型
        $("#isCalcIng").modal("hide");
        if (!isButton) {
            this.props.onUpdateGroups();
            this.props.onUpdateProjectCalcData()
        } else {
            window.location.reload()
        }
        this.refs.socketPanel.stop();
        sweetalert({
            title: intl.get(CONFIGURATION_SUCCESS),
            text: '',
            type: 'success',
            timer: 1000, showConfirmButton: false
        })
    }
    onChange(e) {
        // console.log('e', e.target.checked)
        this.setState({
            checked: e.target.checked
        })
    }
    onChangeInput(e, key) {
        // console.log('e', e.target.value)
        this.setState({
            [key]: e.target.value
        })

    }

    render() {
        let { isButton = false } = this.props
        let { minimumEfficiencyOfFan, maximumCoilFaceVelocity, maximumFanOutletVelocity, checked } = this.state
        // $('#seriesModal').modal({ backdrop: 'static', keyboard: false })

        return (
            <div>
                <div className="modal fade" id="BatchCalculation" data-name="BatchCalculation" tabIndex="-1" role="dialog"
                    aria-labelledby="myModalLabel" data-groupid="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content" style={{ width: '730px' }}>
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">
                                    <b>{intl.get(ONE_CLICK_SELECTION)}</b></h4>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <div className="modal-body" style={{ textAlign: 'left' }}>
                                        <b>{intl.get(CONFIGURATION_RULE)}</b>
                                        <ul style={{ marginLeft: '20px', marginTop: '15px', marginBotton: '20px', listStyle: 'none' }}>
                                            <li className="pull-left">
                                                <div className="pull-left">
                                                    <input defaultChecked="true" type="radio" name="judge-select"
                                                        style={{ verticalAlign: 'middle', marginTop: '0px', marginRight: '5px' }} id="1" />
                                                    <label style={{ display: 'none' }}></label>
                                                </div>
                                                <span style={{ verticalAlign: 'middle' }}>{intl.get(MOST_ECONOMICAL)}</span>
                                            </li>
                                            <li className="pull-left" style={{ marginLeft: '15px' }}>
                                                <div className="pull-left">
                                                    <input type="radio" name="judge-select"
                                                        style={{ verticalAlign: 'middle', marginTop: '0px', marginRight: '5px' }} id="2" />
                                                    <label style={{ display: 'none' }}></label>
                                                </div>
                                                <span style={{ verticalAlign: 'middle' }}>{intl.get(OPTIMAL_PERFORMANCE)}</span>
                                            </li>
                                            <li className="pull-left" style={{ marginLeft: '15px' }}>
                                                <div className="pull-left">
                                                    <input type="radio" name="judge-select"
                                                        style={{ verticalAlign: 'middle', marginTop: '0px', marginRight: '5px' }} id="3" />
                                                    <label style={{ display: 'none' }}></label>
                                                </div>
                                                <span style={{ verticalAlign: 'middle' }}>{intl.get(KEEP_PERFORMANCE)}</span>
                                            </li>
                                            <li className="clearfix"></li>
                                        </ul>

                                        <Checkbox
                                            defaultChecked={false}
                                            checked={checked}
                                            onChange={(e) => this.onChange(e)}
                                        >
                                            {intl.get(CUSTOMIZE)}

                                        </Checkbox>
                                        {
                                            this.state.checked ?
                                                <Input.Group>
                                                    {/* <Col span={6}>
                                                    </Col> */}
                                                    <Col span={18} style={{margin:'10px 0 0 10px'}}>
                                                        {/* <Input defaultValue="0571" /> */}
                                                        <div style={{marginBottom:'3px'}}>{intl.get(MINIMUMEFFICIENCY)}</div>
                                                        <Input value={minimumEfficiencyOfFan}  onChange={(e) => this.onChangeInput(e, 'minimumEfficiencyOfFan')} />
                                                    </Col>
                                                    <Col span={18} style={{margin:'10px 0 0 10px'}}>
                                                        {/* <Input defaultValue="0571" /> */}
                                                        <div style={{marginBottom:'3px'}}>{intl.get(MAXIMUMCOILFACE)}</div>
                                                        
                                                        <Input value={maximumCoilFaceVelocity}  onChange={(e) => this.onChangeInput(e, 'maximumCoilFaceVelocity')} />
                                                    </Col>
                                                    <Col span={18} style={{margin:'10px 0 0 10px', paddingRight:'8px'}}>
                                                        {/* <Input defaultValue="0571" /> */}
                                                        <div style={{marginBottom:'3px'}}>{intl.get(MAXIMUMFANOUTLET)}</div>
                                                        
                                                        <Input value={maximumFanOutletVelocity} onChange={(e) => this.onChangeInput(e, 'maximumFanOutletVelocity')} />
                                                    </Col>
                                                </Input.Group>
                                                : ''
                                        }

                                    </div>
                                    <div className="modal-footer clear" style={{ border: '1px solid #e5e5e5 !important' }}>
                                        <button type="button" className="btn btn-default"
                                            onClick={() => { this.close() }}>{intl.get(CANCEL)}</button>
                                        <button type="button" className="btn btn-primary btn_add"
                                            onClick={() => { this.saveQutSet(isButton) }}>{intl.get(QUICK_CONFIG)}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="isCalcIng" data-name="isCalcIng" tabIndex="-1" role="dialog"
                    data-backdrop="static"
                    aria-labelledby="myModalLabel" data-groupid="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <h4 className="modal-title" id="myModalLabel">
                                {intl.get(BATCH_PROCESSING_ESC)}
                            </h4>
                            <div className="modal-body" style={{ padding: '0px' }}>
                                <SocketPanel ref="socketPanel" panelName="batchCalc"
                                    onError={this.onSaveError}
                                    onGetFinishMsg={() => this.onSaveFinish(isButton)} />

                                <div className="row text-center" style={{ textAlign: 'left' }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
