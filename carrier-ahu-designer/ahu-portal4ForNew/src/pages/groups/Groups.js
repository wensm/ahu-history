import React from 'react'
import {Link} from 'react-router'
import classnames from 'classnames'
import Buttons from './Buttons'
import GroupListContainer from './GroupListContainer'
import GroupFormContainer from './GroupFormContainer'
import AhuFormContainer from './AhuFormContainer'
import MoveGroupContainer from './MoveGroupContainer'
import BatchParamSet from './BatchParamSet'
import BatchImport from '../../components/BatchImport'
import BatchExport from '../../components/BatchExport'
import BatchSelectSetContainer from './BatchSelectSetContainer'
import BatchCalculationContainer from './BatchCalculationContainer'
import CreateReportContainer from '../../components/CreateReportContainer'
import style from './Groups.css'

export default class Groups extends React.Component {
    constructor(props) {
        super();
        this.state = {
            name:'',
            type:'',
            visible:false
        }
    }


    componentDidMount() {
        let {onFetchAhuGroupList, onFetchGroupTypeList} = this.props
        // let arr = location.hash.split('page_')
        // if(arr[1] != undefined){
        //     page = arr[1]
        //     onFetchAhuGroupList(page)

        // }else{

        // }
        // if(this.props.location.query.name == 'app'){//app是从主页进来的，需要走接口获取列表
            onFetchAhuGroupList(1)
            onFetchGroupTypeList();
        // }
    }
    onClick (name, type, groupid, groupType, ahuId){
        if(name == 'ahu'){
            let param = type == 'edit' ? ahuId : this.props.projectId
            this.props.onGetUsefulNm(param)
        }

        this.setState({
            name:name,
            type:type,
            visible:true,
            groupid,
            groupType
        })
    }
    close (){
        this.setState({
            name:'',
            type:'',
            visible:false
        })
    }
     

    render() {
        let {name, type, visible, groupid, groupType} = this.state
        return (
            <div data-name="Groups">
                <Buttons {...this.props} onClick={(name, type, groupid, groupType, ahuId)=>this.onClick(name, type, groupid, groupType, ahuId)} />
                <GroupFormContainer projectId={this.props.projectId} name={name} type={type} visible={visible} close = {()=>this.close()}/>
                <AhuFormContainer projectId={this.props.projectId} name={name} type={type} visible={visible} groupType={groupType} groupid={groupid} close = {()=>this.close()}/>
                <GroupListContainer projectId={this.props.projectId} onClick={(name, type, groupid, groupType, ahuId)=>this.onClick(name, type, groupid, groupType, ahuId)} />
                <MoveGroupContainer projectId={this.props.projectId}/>
                <BatchParamSet/>
                <BatchImport saveBatchImport={this.props.saveBatchImport} coverOption={true} projectId={this.props.projectId}/>
                <BatchExport saveBatchExport={this.props.saveBatchExport} projectId={this.props.projectId}/>
                <BatchSelectSetContainer projectId={this.props.projectId}/>
                <BatchCalculationContainer projectId={this.props.projectId}/>
                <CreateReportContainer flg="groups"
                                       projectId={this.props.projectId}
                                       onSaveCreateReport={this.props.onSaveCreateReport}
                                       onReportGenerated={this.props.onReportGenerated}
                                       onClearReports={this.props.onClearReports}
                />
            </div>
        )
    }
}
