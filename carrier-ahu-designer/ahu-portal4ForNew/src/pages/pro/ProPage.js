import React from 'react'
import Header from '../../components/Header'
import FormContainer from './FormContainer'
import TableContainer from './TableContainer'
import ProFormModalContainer from './ProFormModalContainer'

export default class ProPage extends React.Component {
  render() {
    return (
      <div data-name="ProPage">
        <FormContainer />
        <TableContainer />
        <ProFormModalContainer />
      </div>
    )
  }
}
