import {connect} from 'react-redux'
import {formValues} from 'redux-form'
import DefaultParameter from './DefaultParameter'
import {calcComponentValue, confirmSection, fetchRelativeHumidity, fetchWetBulbTemperature} from '../../actions/ahu'
import {updateUserDefaultParaPreferToServer} from '../../actions/general'

const mapStateToProps = state => {
    let defaultPara = state.general.user.defaultParaPrefer
    // console.log(defaultPara)
    let newDefaultPara = {}
    for (var variable in defaultPara) {
        if (defaultPara.hasOwnProperty(variable)) {
            newDefaultPara[variable.replace(/\./gi, '_')] = defaultPara[variable]
        }
    }

    return {
        initialValues: newDefaultPara,
        formValues: state.form.component && state.form.component.values,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSave: (formValues) => {
            let newDefaultPara = {}
            for (var variable in formValues) {
                if (formValues.hasOwnProperty(variable)) {
                    newDefaultPara[variable.replace(/\_/gi, '.')] = formValues[variable]
                }
            }
            dispatch(updateUserDefaultParaPreferToServer(newDefaultPara))
        },
        onCalcRelativeHumidity: (dryBulbTemperature, wetBulbTemperature, name) => {
            dispatch(fetchRelativeHumidity(ownProps.componentValue[dryBulbTemperature], wetBulbTemperature, name))
        },
        onCalcWetBulbTemperature: (dryBulbTemperature, relativeHumidity, name) => {
            dispatch(fetchWetBulbTemperature(ownProps.componentValue[dryBulbTemperature], relativeHumidity, name))
        },
        onCalcRAVolume: (aVolume,naVolume,name) => {
            dispatch(fetchRAVolume(aVolume,naVolume,name));
        },
        onCalcNAVolume: (aVolume,naVolume,name) => {
            dispatch(fetchNAVolume(aVolume,naVolume,name));
        },
        onCompleteSection: () => {
            const ahu = ownProps.componentValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)
            const sections = []
            let position = null
          $('.ahu-section-list').children().each((index, element) => {
                const sectionId = $(element).attr('data-section-id')
                const sectionProps = ownProps.componentValues[sectionId]
                if (Number(sectionId) === Number(ownProps.selectedComponent.id)) {
                    position = index + 1
                }
                const newSectionProps = {}
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                    }
                }
                const section = {
                    pid: ownProps.projectId,
                    unitid: ownProps.ahuId,
                    key: $(element).attr('data-section'),
                    position: index + 1,
                    metaJson: JSON.stringify(newSectionProps),
                    partid: $(element).attr('data-partid'),
                }
                sections.push(section)
            })
            const sectionStr = JSON.stringify(sections)
            dispatch(confirmSection(ownProps.projectId, ownProps.ahuId, ahuStr, sectionStr, position))
        },
        onCalcCoolingCoil: (season) => {
            const ahu = ownProps.componentValues[0]
            const newAhu = {}
            for (var variable in ahu) {
                if (ahu.hasOwnProperty(variable)) {
                    newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
                }
            }
            const ahuStr = JSON.stringify(newAhu)
            const sections = []
            let position = null
            $('#ahu').children().each((index, element) => {
                const sectionId = $(element).attr('data-section-id')
                const sectionProps = ownProps.componentValues[sectionId]
                if (Number(sectionId) === Number(ownProps.selectedComponent.id)) {
                    position = index + 1
                }
                const newSectionProps = {}
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
                    }
                }
                const section = {
                    pid: ownProps.projectId,
                    unitid: ownProps.ahuId,
                    key: $(element).attr('data-section'),
                    position: index + 1,
                    metaJson: JSON.stringify(newSectionProps),
                    partid: $(element).attr('data-partid'),
                }
                sections.push(section)
            })
            const sectionStr = JSON.stringify(sections)
            dispatch(calcComponentValue(ownProps.projectId, ownProps.ahuId, ahuStr, sectionStr, position, 'ahu.coolingCoil', season))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultParameter)
