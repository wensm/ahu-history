import {connect} from 'react-redux'
import setDirection from './setDirection'
import {
  setAhuListDirection,
} from '../../actions/groups'

const mapStateToProps = (state, ownProps) => {
  return {
  	ahuGroupList: ownProps.ahuGroupList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchProjects(){
      //dispatch(fetchProjects())
    },
    onSetDirection(projectId,ahuList,doorOrientation,pipeOrientation){
      dispatch(setAhuListDirection(projectId,ahuList,doorOrientation,pipeOrientation))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(setDirection)
