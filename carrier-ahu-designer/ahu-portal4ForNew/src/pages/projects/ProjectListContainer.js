import {
    CONFIRM_TO_DELETE_AHU,
    OK,
    CANCEL,
} from '../intl/i18n'

import {connect} from 'react-redux'
import ProjectList from './ProjectList'
import sweetalert from 'sweetalert'
import intl from 'react-intl-universal'
import {
    delProject,
    fetchprokey,
    updateProjectPre,
} from '../../actions/projects'
import {
    fetchAhuGroupList,
} from '../../actions/groups'

const mapStateToProps = state => {
    return {
        projectList: state.projects.projectList,
        language: state.general.language,
        projectFilterStr: state.projects.projectFilterStr
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDelProject(projectId){
            sweetalert({
                title: intl.get(CONFIRM_TO_DELETE_AHU),
                text: "Once deleted, you will not be able to recover this project!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    dispatch(delProject(projectId));
                }
            })

        },
        onFetchProkey(projectId){
            dispatch(fetchprokey(projectId));
        },
        onUpdateProjectPre(projectId){
            dispatch(updateProjectPre(projectId));
        },
        onFetchAhuGroupList(projectId){
            dispatch(fetchAhuGroupList(projectId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList)
