import {
    NEW_PROJECT,
    IMPORT_PROJECT,
    OTHERCONDITIONS,
    TIME
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from './Buttons.css'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { projectPageFilterUpdate, START_NEW_PROJECT } from '../../actions/projects'
import DateUtil from '../../misc/DateUtils';
import { DatePicker, Switch, Select,  } from 'antd'
const Option = Select.Option
class Buttons extends React.Component {
    constructor(props) {
        super()
        this.onFilterUpdate = this.onFilterUpdate.bind(this)
        this.createProject = this.createProject.bind(this)
        this.state = {
            SwitchState: 'others'
        }
    }

    onFilterUpdate(event) {

        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        clearTimeout(this._onFilterUpdateTimer)
        this._onFilterUpdateTimer = setTimeout(() => {
            this.props.onProjectsPageFilterUpdate(value)
        }, 500);

    }


    createProject() {
        this.props.onCreateProject(this.props.user.userName, this.props.price)
    }

    render() {
        let { SwitchState } = this.state
        const {onProjectsPageFilterUpdate} = this.props
        
        return (
            <div className={classnames(style.project, 'main promain clear')} data-name="Buttons">
                <div className="pro_con pull-left">
                    <ul className={classnames(style.proCap, 'clear')}>
                        <li role="button" data-toggle="collapse" href="#newProject" aria-expanded="false"
                            aria-controls="newProject" onClick={this.createProject}>
                            <i className="iconfont icon-xinjianxiangmu"></i>
                            {intl.get(NEW_PROJECT)}
                        </li>
                        <li data-toggle="modal" data-target="#BatchImport">
                            <i className="iconfont icon-daoru"></i>
                            {intl.get(IMPORT_PROJECT)}
                        </li>
                    </ul>
                </div>

                {SwitchState == 'others' ? <div className="pull-right form-group form-inline">
                    <div className="input-group">
                        <span className="input-group-addon" id="basic-addon1"><i className="fa fa-filter"
                            aria-hidden="true"></i></span>
                        <input type="text" className="form-control" placeholder="Filter String"
                            aria-describedby="basic-addon1" onChange={this.onFilterUpdate} />
                    </div>
                </div> : <div className="pull-right form-group form-inline">
                        <DatePicker format="YYYY-MM-DD" placeholder = "Filter Date" style={{width:'210px'}} onChange={(date, dateString) => {

                            onProjectsPageFilterUpdate(dateString)
                        }} />
                    </div>
                }
                <div className="pull-right form-group form-inline" style={{ marginRight: '10px' }}>
                    {/* <Switch checked={SwitchState} onChange={(e) => { this.setState({ SwitchState: e }) }} /> */}
                    <Select defaultValue="others" style={{ width: 120 }} onChange={(value)=>{  onProjectsPageFilterUpdate('');this.setState({SwitchState: value})}}>
                        <Option value="time">{intl.get(TIME)}</Option>
                        <Option value="others">{intl.get(OTHERCONDITIONS)}</Option>
                    </Select>
                </div>
                <div className={style.clear}></div>
            </div>
        )
    }
}


const mapStateToProps = (state, owProps) => {
    return {
        ahuFilterStr: state.projects.ahuFilterStr,
        user: state.general.user,
        price: state.general.priceBase
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onProjectsPageFilterUpdate(filterStr) {
            dispatch(projectPageFilterUpdate(filterStr))
        },
        onCreateProject(name, price) {
            dispatch(
                {
                    name: name,
                    projectDate: DateUtil.format(new Date(), "yyyy-MM-dd"),
                    priceBase: price[0]['key'],
                    type: START_NEW_PROJECT,
                }
            )
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)








