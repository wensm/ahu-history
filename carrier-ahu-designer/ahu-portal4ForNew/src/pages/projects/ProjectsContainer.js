import {connect} from 'react-redux'
import Projects from './Projects'
import {
    fetchProjects,
    saveBatchImport,
    saveBatchExport,
    saveCreateReport,
    receiveReport,
    clearReports,
} from '../../actions/projects'

const mapStateToProps = (state, ownProps) => {
    return {
        ahuGroupList: state.groups.ahuGroupList,
        projectList:state.projects.projectList
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchProjects(){
            dispatch(fetchProjects())
        },
        onSaveBatchImport(formData,projectId, callBack){
            dispatch(saveBatchImport(formData,projectId, callBack));
        },
        onSaveBatchExport(id, fileName){
            dispatch(saveBatchExport(id, fileName));
        },
        onSaveCreateReport(param, startGeneratingReport){
            dispatch(saveCreateReport(param, startGeneratingReport));
        },
        onReportGenerated(reports){
            dispatch(receiveReport(reports));
        },
        onClearReports(){
            dispatch(clearReports());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects)
