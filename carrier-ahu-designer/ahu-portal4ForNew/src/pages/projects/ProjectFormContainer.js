import {connect} from 'react-redux'
import ProjectForm from './ProjectForm'
import {
    saveProject,
} from '../../actions/projects'

const mapStateToProps = state => {
    return {
        user: state.general.user,
        priceBase: state.general.priceBase == undefined ?{}:state.general.priceBase,
        type: (state.projects.updateProject && state.projects.updateProject.no) ?"edit":"",
        initialValues: state.projects.updateProject,
        projectForm: state.form.ProjectForm == undefined?{}:state.form.ProjectForm.values,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSaveProject(project){
            dispatch(saveProject(project))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectForm)