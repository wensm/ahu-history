import {connect} from 'react-redux'
import RectEditor from './RectEditor'

import {PARTITION_PANEL_RECT_UPDATED,PARTITION_PANEL_TYPE_UPDATED,PARTITION_PANEL_LINE_UPDATED} from '../../actions/partition'
const mapStateToProps = (state, ownProps) => {
    return {
        currentPanel: state.partition.currentEPanel,
        panel: state.partition.currentESubPanel,
        parentPanel: state.partition.currentEParentPanel
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSubPanelChanged: (currentPanel, parentPanel, thisPanel, num, name) => {
            dispatch(subPanelChanged(currentPanel, parentPanel, thisPanel, num, name))
        },
        onSubPanelTypeChanged: (currentPanel, parentPanel, subPanel, type) => {
            dispatch(subPanelTypeChanged(currentPanel, parentPanel, subPanel, type))
        },
        onSubPanelLineChanged: (currentPanel, parentPanel, subPanel, type) => {
            dispatch(subPanelLineChanged(currentPanel, parentPanel, subPanel, type))
        }
        // onTodoClick: id => {
        //   dispatch(toggleTodo(id))
        // }
    }
}

function subPanelChanged(currentPanel, parentPanel, thisPanel, num, name) {
    return {
        type: PARTITION_PANEL_RECT_UPDATED,
        currentPanel: currentPanel,
        parentPanel: parentPanel,
        thisPanel: thisPanel,
        num: num,
        name
    }
}

function subPanelTypeChanged(currentPanel, parentPanel, subPanel, type) {
    return {
        type: PARTITION_PANEL_TYPE_UPDATED,
        currentPanel: currentPanel,
        parentPanel: parentPanel,
        subPanel: subPanel,
        panelType: type
    }
}

function subPanelLineChanged(currentPanel, parentPanel, subPanel, type) {
    return {
        type: PARTITION_PANEL_LINE_UPDATED,
        currentPanel: currentPanel,
        parentPanel: parentPanel,
        subPanel: subPanel,
        divideType: type
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RectEditor)
