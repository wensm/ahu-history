import {connect} from 'react-redux'
import Partition from './Partition'
import {PARTITION_SELECTED} from '../../actions/partition'

const mapStateToProps = state => {
    return {
        currentPartitionIndex: state.partition.currentPartitionIndex,
        ahuSections: state.ahu.sections,
        ahuLayout: state.ahu.layout,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPartitionSelected: (partitionIndex, show) => {
            dispatch(partitionSelected(partitionIndex, show));
        }
    }
}

function partitionSelected(partitionIndex, show) {
    return {
        type: PARTITION_SELECTED,
        partitionIndex: partitionIndex,
        show:show
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Partition)
