import {
    PREVIOUS,
    NEXT,
    SPLIT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import classnames from 'classnames'
import PartitionContainer from './PartitionContainer'
import style from './AHU.css'
import stylea from '../ahu/AHU.css'
import {isTwinLine, STYLE_CHAMBER, STYLE_COMMON, STYLE_PLATE, STYLE_WHELE} from "../../actions/ahu"

export default class AHU extends React.Component {

    constructor(props) {
        super()
        this.forward = this.forward.bind(this)
        this.backward = this.backward.bind(this)
        this.split = this.split.bind(this)
    }

    forward(key, opt, selector) {
        let partitionId = opt.$trigger.context.id
        this.props.onForward(parseInt(partitionId), this.props.layout)
    }

    backward(key, opt) {
        let partitionId = opt.$trigger.context.id
        this.props.onBackward(parseInt(partitionId), this.props.layout)
    }

    split(key, opt) {
        let partitionId = opt.$trigger.context.id
        this.props.onSplit(parseInt(partitionId), this.props.layout)
    }


    componentDidMount() {
        if(this.props.isPartition){
            $.contextMenu({
                // define which elements trigger this menu
                selector: ".with-cool-menu",
                // define the elements of the menu
                items: {
                    item1: {
                        name: intl.get(PREVIOUS), callback: this.forward
                    },
                    item2: {
                        name: intl.get(NEXT), callback: this.backward
                    },
                    item3: {
                        name: intl.get(SPLIT), callback: this.split
                    }
                }
            });
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.isPartition){
            $.contextMenu({
                // define which elements trigger this menu
                selector: ".with-cool-menu",
                // define the elements of the menu
                items: {
                    item1: {
                        name: intl.get(PREVIOUS), callback: this.forward
                    },
                    item2: {
                        name: intl.get(NEXT), callback: this.backward
                    },
                    item3: {
                        name: intl.get(SPLIT), callback: this.split
                    }
                }
            });
        }
    }

    render() {
        const {
            partitions, layout, isPartition,editingAHUProps
        } = this.props
        if (!layout) {
            return (<div></div>)
        }
        let twin = 0
        if (layout) {
            twin = isTwinLine(layout.style)
        }
        let partitionsArr = [[], [], [], []];
        let twOffsets = ["",""];
//        console.log('partitions', partitions)
        if (twin != 0) {
          let paritionLens =[0,0];
          let twPos =[-1,-1];
          let gaps =[0,0];
            partitions.map((partition, index) => {
                //程序中，会出现三个pos，但是意义不一样
                //1. 在partition中，pos指这个partition的在整个partition组中的位置，编号从0开始
                //2. 在layout.layoutData中，指的是在section在数组中的位置，编号从1开始，因为0被ahu给占用了
                //3. partition.sections.section中，pos为第几个section，编号从0开始，所以在判断的时候，需要+1，来判断段是落在layoutData的那个ahu line中
                let pos = partition.sections[0].pos + 1

                layout.layoutData.map((data, index) => {
                    if (data.includes(pos)) {
                        //前两部分放到一行
                        if([1,0].indexOf(index)!==-1){
                          partitionsArr[0].push(partition)
                          if(twPos[0]===-1){
                            partition.sections.forEach((msec,mindex)=>{
                              if(["ahu.wheelHeatRecycle","ahu.plateHeatRecycle"].includes(msec.metaId)){
                                twPos[0] = paritionLens[0] + mindex ;
                              }
                            })
                          }else{
                              gaps[0] ++;
                          }
                          paritionLens[0]+= partition.sections.length;

                        }else{
                          //  后两部分放到一行
                          partitionsArr[2].push(partition)
                          if(twPos[1]===-1){
                            partition.sections.forEach((msec,mindex)=>{
                              if(["ahu.wheelHeatRecycle","ahu.plateHeatRecycle"].includes(msec.metaId)){
                                twPos[1] = paritionLens[1] + mindex ;
                              }
                            })
                          }else{
                            gaps[1] ++;
                          }
                          paritionLens[1]+= partition.sections.length;
                        }

                    }
                })
            })

          // console.log("paritionLens:"+paritionLens);
          // console.log("twPos:"+twPos);
          // console.log("gaps:"+gaps);
          twPos.forEach((p,i)=>{
                if(p!==-1){
                    twOffsets[i] =( (paritionLens[i] - p -1) * 62 + gaps[i] * 10) +'px'
                  //console.log(paritionLens[i] - p -1);
                }
            }
          )
        }
        let pindex = 0
        let flashback = partitions ? partitions.length : 0
        return (
            <div className={classnames(!twin && style.AHU, twin && style.AHUTwin)}>
                {/* <div className="container-fluid">
                    <div className="row">
                        {(layout.style == STYLE_PLATE || layout.style == STYLE_CHAMBER || layout.style == STYLE_COMMON) &&
                            <div className="col-md-3">
                                <i className="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Airflow
                            </div>
                        }
                        {layout.style == STYLE_WHELE &&
                            <div className="col-md-3">
                                <i className="fa fa-arrow-left" aria-hidden="true"></i>&nbsp; Airflow
                            </div>
                        }
                        {(layout.style == STYLE_CHAMBER || layout.style == STYLE_COMMON) &&
                            <div className="col-md-1 col-md-offset-8">
                                Airflow&nbsp;<i className="fa fa-arrow-right" aria-hidden="true"></i>
                            </div>
                        }
                        {(layout.style == STYLE_WHELE || layout.style == STYLE_PLATE) &&
                            <div className="col-md-1 col-md-offset-8">
                                <i className="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Airflow
                            </div>
                        }
                    </div>
                </div> */}
                <div className={classnames(!twin && style.AHUContainer, twin && style.AHUContainerTwin)}>
                    <div className={classnames(!twin && stylea.Hidden)}>
                        <div className={style.AHULineContainer01+" "+ style.AHULineContainer02}>
                            <div id="ahutl" className={stylea.AHULinePartitionContainer + ' ahu-section-list'}
                                 style={{
                                   transform:"translateX("+twOffsets[0]+")",
                                   "WebkitTransform":"translateX("+twOffsets[0]+")"
                                 }}
                            >
                                {partitionsArr[0].map((partition, index) => <PartitionContainer key={index}
                                                                                                partition={partition}
                                                                                                pindex={pindex++}
                                                                                                flashback={ partitions.length-partition.pos}
                                                                                                loc="00"
                                                                                                sections={partition.sections}
                                                                                                editingAHUProps={editingAHUProps}

                                />)}
                            </div>

                            {/*<div id="ahutr" className={stylea.AHULineRightContainer + ' ahu-section-list'}>
                                {partitionsArr[1].map((partition, index) => <PartitionContainer key={index}
                                                                                                partition={partition}
                                                                                                pindex={pindex++}
                                                                                                loc="01"
                                                                                                sections={partition.sections}/>)}
                            </div>*/}
                        </div>
                        <div className={style.AHULineContainer01+" "+ style.AHULineContainer02}>
                            <div id="ahubl" className={stylea.AHULinePartitionContainer + ' ahu-section-list'}
                                 style={{transform:"translateX("+twOffsets[1]+")",
                                   "-webkit-transform":"translateX("+twOffsets[1]+")"}}
                            >
                                {partitionsArr[2].map((partition, index) => <PartitionContainer key={index}
                                                                                                partition={partition}
                                                                                                pindex={pindex++}
                                                                                                flashback={partitions.length-partition.pos}
                                                                                                
                                                                                                loc="10"
                                                                                                sections={partition.sections}
                                                                                                editingAHUProps={editingAHUProps}
                                                                                                
                                                                                                />)}
                            </div>
                            {/*<div id="ahubr" className={stylea.AHULineRightContainer + ' ahu-section-list'}>
                                {partitionsArr[3].map((partition, index) => <PartitionContainer key={index}
                                                                                                pindex={pindex++}
                                                                                                partition={partition}
                                                                                                loc="11"
                                                                                                sections={partition.sections}/>)}
                            </div>*/}
                        </div>
                    </div>
                    <div id="ahu" className={classnames(stylea.AHULineContainer, twin && stylea.Hidden) + ' ahu-section-list'}>
                        {partitions.map((partition, index) => <PartitionContainer key={index}
                                                                                  pindex={index}
                                                                                  partition={partition}
                                                                                  loc="00"
                                                                                  sections={partition.sections}
                                                                                  editingAHUProps={editingAHUProps}
                                                                                  
                                                                                  />)}
                    </div>
                </div>
                {/* {twin && layout.style == STYLE_WHELE &&
                <div className="container-fluid">
                    <div className="row">
                        <div className="row">
                            <div className="col-md-3">Airflow&nbsp; <i className="fa fa-arrow-right"
                                                                       aria-hidden="true"></i>
                            </div>
                            <div className="col-md-1 col-md-offset-8">Airflow&nbsp; <i className="fa fa-arrow-right"
                                                                                       aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
                }
                {twin && layout.style == STYLE_PLATE &&
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-3">Airflow&nbsp; <i className="fa fa-arrow-left" aria-hidden="true"></i>
                        </div>
                        <div className="col-md-1 col-md-offset-8"><i className="fa fa-arrow-right"
                                                                     aria-hidden="true"></i>&nbsp; Airflow
                        </div>
                    </div>
                </div>
                } */}
            </div>

        )
    }
}
