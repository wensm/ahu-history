import {
    PANEL_EDIT,
    EDIT,
    CONFIRM,
    CASING_PARTS_LIST,
    Partition,
    RESETAB,
    BACK
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import classnames from 'classnames'
import PanelContainer from './PanelContainer'
import Group from '../ahu/Group'
import style from './Partition.css'
import panelStyle from './Panel.css'
import PartitionSummary from "./PartitionSummaryContainer"
import PanelLegend from "./PanelLegend"

import { Spin } from 'antd'
window.intl = intl

export default class PartitionDetail extends React.Component {

    constructor(props) {
        super()
        this.selectPanel = this.selectPanel.bind(this)
        this.confirm = this.confirm.bind(this)
        this.syncAB = this.syncAB.bind(this)
        this.goBack = this.goBack.bind(this)
        this.showPanelIndex = this.showPanelIndex.bind(this)
        this.saveCapture = this.saveCapture.bind(this)

        this.state = {
            ShowPanelIndex: false,
            enlarge: false
        }
    }
    componentDidMount() {
        this.props.onSpin(false)



    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.threeView != '' && !$('#group111').attr('hasToBig')) {
            setTimeout(() => {
                $('#group111').mousewheel(function (event, delta) {
                    let dir = delta > 0 ? 'Up' : 'Down';
                    let widthh = $('#group111').width() * 1.02
                    let widthhh = $('#group111').width() * 0.98
                    $('#group111').css('width', widthh)
                    $('#group111').css('height', widthh)
                    if (dir == 'Up') {
                        $('#group111').css('width', widthh)
                        $('#group111').css('height', widthh)
                    } else {
                        $('#group111').css('width', widthhh)
                        $('#group111').css('height', widthhh)
                    }
                    return false;
                });
                $('#group111').attr('hasToBig', true)
            }, 300)
        }
    }

    selectPanel(face) {
        this.props.onFaceChanged(this.props.currentPartitions, this.props.currentPartitionIndex, face)
        // console.log("Selected face: " + face)
    }

    confirm() {
        this.props.onDoConfirm()
        // console.log("save ...")
        this.saveCapture()
    }
    syncAB() {
        //    console.log('this', this.props.currentPartitions[this.props.currentPartitionIndex])
        this.props.onSyncAB(this.props.currentPartitions[this.props.currentPartitionIndex])
    }
    goBack() {
        //    console.log('this', this.props.currentPartitions[this.props.currentPartitionIndex])
        // this.props.onSyncAB(this.props.currentPartitions[this.props.currentPartitionIndex])
        this.props.onGoBack()

    }
    showPanelIndex() {
        //this.props.onShowPanelIndex()
        this.setState({
            ShowPanelIndex: !this.state.ShowPanelIndex
        });

        if (this.state.ShowPanelIndex) {
            this.props.onShowPanelIndex()
        }
    }

    saveCapture() {
        html2canvas($("#group77")[0], { scale: 1 }).then(canvas => {
            let src = canvas.toDataURL()

            this.props.onPartitionImageSave(this.props.currentPartitionPObj, src, Number(this.props.currentPartitions[this.props.currentPartitionIndex].pos) + 1, () => { this.onSavePartition(Number(this.props.currentPartitions[this.props.currentPartitionIndex].pos) + 1) })
            // console.log(src)

            // $("#info_capture").attr("src", src)
            // $("#captureModal").modal("show")
            // document.body.appendChild(canvas)
            // document.body.appendChild($("<img src="+canvas.toDataURL()+"/>"))
        });

    }
    onSavePartition(num) {
        const {
            currentPartitionPObj,
            currentPartitions
        } = this.props
        currentPartitionPObj.saveFrom = this.props.route.path.split('/')[0]
        this.props.onSavePartitoins(currentPartitionPObj, currentPartitions, num)
    }
    getArr(obj) {
        let arr = []
        for (let key in obj) {
            arr.push(obj[key])
        }
        return arr
    }

    render() {
        const {
            currentPartitions, currentPartitionIndex, currentSubPanel, currentParentPanel, parentPanel, currentPanel, user, params, spinning, editingAHUProps, threeView, panelSeries
        } = this.props

        let ratio = 2

        //设定显示需要显示的面板
        let panelIndexArray = []

        panelIndexArray.push(0)
        panelIndexArray.push(1)
        // if (this.props.currentPartitionIndex == 0) {
        // }
        panelIndexArray.push(2)
        // if (this.props.currentPartitions.length - 1 == this.props.currentPartitionIndex) {
        panelIndexArray.push(3)
        panelIndexArray.push(4)
        panelIndexArray.push(5)
        // }
        let panelArr = []
        let currentPartitionsCurrentPartitionIndex = currentPartitions[currentPartitionIndex]
        if (currentPartitionsCurrentPartitionIndex && currentPartitionsCurrentPartitionIndex['panels']) {
            panelArr = this.getArr(currentPartitionsCurrentPartitionIndex['panels'])
        }
        //        console.log('panelArr', panelArr)
        // console.log('currentPartitionsCurrentPartitionIndex', currentPartitionsCurrentPartitionIndex, currentPartitions, currentPartitionIndex)
        // let backgroundImageUrl = `url('/files/output/cad/${params ? params.projectId : ""}/${params ? params.ahuId : ""}/${user.userId}.bmp')`
        // let backgroundImageUrl = `url(${threeView})`
        let backgroundImageUrl = threeView ? "url(' " + threeView + " ')" : ''
        //        console.log('this2',this.props.editingAHUProps['meta.ahu.product'])
        return (
            <div>
                <Spin spinning={spinning} size="large">
                    <div style={{ textAlign: 'center', margin: '10px 0' }}>
                        <button className="btn btn-primary"
                            style={{ marginRight: 10 }}
                            onClick={this.confirm}>{intl.get(CONFIRM)}</button>
                        <button className="btn btn-primary"
                            style={{ marginRight: 10 }}
                            onClick={this.showPanelIndex}>{intl.get(this.state.ShowPanelIndex ? "title.moon_intl_str_0801" : "title.moon_intl_str_0802")}</button>
                        <button className="btn btn-primary"
                            style={{ marginRight: 10 }}
                            onClick={this.syncAB}>{intl.get(RESETAB)}</button>
                        <button type="button" className="btn btn-primary"
                            onClick={this.goBack}>{intl.get(BACK)}
                        </button>
                    </div>
                </Spin>
                <Group title={intl.get(PANEL_EDIT)} id="group7">
                    <div style={{ marginTop: '20px' }} id="panel_detail">

                        <div className="col-lg-12">
                            <div className="row">
                                <div className="col-lg-10" id="group77">
                                    <div className="row">

                                        <div className="col-lg-8" style={{ height: 'inherit', padding: 0 }}>
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h3 className="panel-title">{intl.get(EDIT)}
                                                        -- {this.props.caption}

                                                    </h3>
                                                </div>
                                                <div
                                                    className={classnames((this.state.ShowPanelIndex ? panelStyle.ShowPanelIndex : ""), "panel-body")}
                                                    style={{ padding: 0 }}>
                                                    {panelArr && panelArr.length > 0 ? <div className="tab-content">
                                                        {panelArr.map((panelPos, index) => {
                                                            return <PanelContainer
                                                                // {...this.props}
                                                                product={editingAHUProps['meta.ahu.product']}
                                                                key={`${currentPartitionIndex}${panelPos.id}`}
                                                                panelPos={panelPos.id}
                                                                // panelPos={index}
                                                                ratio={ratio}
                                                                currentPartitions={currentPartitions}
                                                                panelSeries={panelSeries}
                                                            // currentPanel={currentPanel}

                                                            // currentPartitionIndex={currentPartitionIndex}
                                                            />
                                                        })}

                                                    </div> : ''}
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-4 " style={{ color: 'black', height: 'inherit' }}>
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h3 className="panel-title">{intl.get(CASING_PARTS_LIST)}--{intl.get(Partition)}{`${Number(currentPartitionsCurrentPartitionIndex ? currentPartitionsCurrentPartitionIndex.pos : 0) + 1}`}
                                                    </h3>
                                                </div>
                                                <div className="panel-body">
                                                    {panelArr && panelArr.length > 0 &&
                                                        <PartitionSummary product={editingAHUProps['meta.ahu.product']} />}
                                                </div>
                                            </div>
                                            <PanelLegend product={this.props.editingAHUProps['meta.ahu.product']} />

                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-2" >
                                    {backgroundImageUrl && <div style={{
                                        width: 'inherit',
                                        height: '300px',
                                        backgroundImage: backgroundImageUrl,
                                        backgroundSize: '100% 100%',
                                        // backgroundColor: 'red',
                                        position: 'fixed',
                                        right: 0,
                                        top: '20%'
                                    }}
                                        id="group111"
                                        onClick={() => {
                                            let widthh = $('#group111').width() * 2.5
                                            let widthhh = $('#group111').width() / 2.5
                                            if (!this.state.enlarge) {

                                                $('#group111').css('width', widthh)
                                                $('#group111').css('height', widthh)
                                            } else {

                                                $('#group111').css('width', widthhh)
                                                $('#group111').css('height', widthhh)
                                            }
                                            this.setState({
                                                enlarge: !this.state.enlarge
                                            })
                                        }}
                                    >

                                    </div>}

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="modal fade" id="captureModal" tabIndex="-1" role="dialog"
                        aria-labelledby="coolingCoilwModalLabel">
                        <div className="modal-dialog modal-lg" role="document">
                            <img id="info_capture" src="" alt="" style={{ width: "100%" }} />
                        </div>
                    </div>
                </Group>
            </div>

        )
    }
}
