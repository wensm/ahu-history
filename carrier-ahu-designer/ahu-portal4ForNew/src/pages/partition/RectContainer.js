import { connect } from 'react-redux'
import Rect from './Rect'
import {searchParentPanel, PARTITION_PANEL_RECT_SELECTED, PARTITION_PANEL_RECT_UPDATED, PARTITION_PANEL_LOCKING} from '../../actions/partition'
const mapStateToProps = (state, ownProps) => {

    let currentPanel =state.partition.editingAHUPartitions[state.partition.currentPartitionIndex]
        if (ownProps.rect.panel.id) {
            currentPanel = currentPanel.panels[ownProps.rect.panel.id.split("_")[0]]
        }else if(ownProps.parentPanel.id){
            currentPanel = currentPanel.panels[ownProps.parentPanel.id.split("_")[0]]
        }

  return {
      currentPanel:currentPanel,
      isLockedPanel:state.partition.lockingPanel

  }

}

const mapDispatchToProps = dispatch => {
  return {
      onRectSelected:(selectedPanel, currentPanel, fromMouseToLeft) => {
          dispatch(rectSelected(selectedPanel, currentPanel, fromMouseToLeft))
      },
      onLocking: (currentPanel) => {
        dispatch(locking(currentPanel))
    },
  }
}
function locking(currentPanel) {
    return {
        type: PARTITION_PANEL_LOCKING,
        currentPanel,
    }
}

function rectSelected(selectedPanel, currentPanel, fromMouseToLeft) {
    return {
        type: PARTITION_PANEL_RECT_SELECTED,
        selectedPanel: selectedPanel,
        currentPanel : currentPanel,
        fromMouseToLeft
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rect)
