import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { fetchAhuSeries } from '../../../actions/ahu'
import AHUForm from './AHUForm'

const mapStateToProps = state => {
    return {
        initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
        templateId: state.ahu.templateId
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {

    return {

        onFetchAhuSeries: (templateId) => {
            let sairvolume = ownProps.componentValue.meta_ahu_sairvolume
            if (ownProps.propUnit == 'B') {
                sairvolume = Number(sairvolume) * 1.6989
                sairvolume = sairvolume.toFixed(0)
            }else{
                sairvolume = Number(sairvolume).toFixed(0)
            }
            dispatch(fetchAhuSeries(templateId, ownProps.componentValue.meta_ahu_product, sairvolume))
        },

        onSairvolumeChange: (event, newValue, previousValue, templateId) => {

            //   console.log('onSairvolumeChange newValue:'+newValue+',previousValue:'+previousValue);
//             if (ownProps.componentValue.meta_ahu_eairvolume == newValue) {
                
//                 return;
//             }

            //回风联动
            ownProps.componentValue.meta_ahu_eairvolume = newValue;
            //   console.log('zzf11', ownProps)
            //   setTimeout(() => {
            //       $('#seriesModal').modal()
            //       dispatch(fetchAhuSeries(templateId, parseInt(newValue)))
            //   }, 300);

            //清空型号及其相关信息
            ownProps.componentValue.meta_ahu_serial = '';
            ownProps.componentValue.meta_ahu_width = '';
            ownProps.componentValue.meta_ahu_height = '';
            //ownProps.componentValue.meta_ahu_svelocity= '';
            //ownProps.componentValue.assis_section_ahu_height= '';
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUForm)
