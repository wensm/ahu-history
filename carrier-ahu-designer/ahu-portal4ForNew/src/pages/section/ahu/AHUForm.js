import {
    SUPPLY_FAN_UNIT,
    RETURN_FAN_UNIT_GROUP,
    UNIT_MODEL,
    OUTSIDE_PANEL,
    INTERNAL_PANEL,
    OPTION,
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength5, minValue0 } from '../../ahu/Validate'

// import { syncAhuComponentValues, fetchAhuSeries } from '../../actions/ahu'

class AHUForm extends React.Component {

    handleClickAhuSerial() {
        $('#seriesModal').modal({ backdrop: 'static', keyboard: false })
        this.props.onFetchAhuSeries(this.props.templateId);
    }
    clickNavi(e) {
        let theLink = e.target
        if (theLink) {
            let theHref = theLink.getAttribute("href")
            theHref = theHref.substr(1)
            const element = document.getElementById(theHref);
            if (element) element.scrollIntoView();
            e.preventDefault()
        }
    }
    componentDidMount() {
        setTimeout(() => {
            $("[name='meta_ahu_sairvolume']").keypress((event) => {
                if (event.keyCode == "13") {
                    this.handleClickAhuSerial()
                }
            })
        }, 3000);

    }

    render() {
        const { onSairvolumeChange } = this.props
        return (
            <form data-name="AHUSectionForm">

                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(SUPPLY_FAN_UNIT)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.sairvolume"
                                //  validate={[required]}
                                onBlurChange={(event, newValue, previousValue) => onSairvolumeChange(event, newValue, previousValue, this.props.templateId)}
                            />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.sexternalstatic" />
                        </div>
                        
                        <div className="col-lg-3">
                            <Field id="meta.ahu.svelocity" />
                        </div>
                    </Group>
                    <Group title={intl.get(RETURN_FAN_UNIT_GROUP)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.eairvolume" />
                        </div>
                        <div className="col-lg-3">
                            {/* <Field id="meta.ahu.eexternalstatic" validate={[required]} /> */}
                            <Field id="meta.ahu.eexternalstatic"  />
                        </div>
                        
                        <div className="col-lg-3">
                            <Field id="meta.ahu.evelocity" />
                        </div>
                    </Group>
                    <Group title={intl.get(UNIT_MODEL)} id="group2">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.product" />
                        </div>
                        <div className="col-lg-3" onClick={() => this.handleClickAhuSerial()}>
                            <Field id="meta.ahu.serial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.width" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.height" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.inWindDirection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.altitude" />
                        </div>
                    </Group>
                    <Group title={intl.get(OUTSIDE_PANEL)} id="group4">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.exskinm" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.exskinw" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.exskincolor" />
                        </div>
                    </Group>
                    <Group title={intl.get(INTERNAL_PANEL)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.inskinm" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.inskinw" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.panelinsulation" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.inskincolor" />
                        </div>
                    </Group>
                    <Group title={intl.get(OPTION)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.ahu.delivery" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.voltage" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.baseType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.package" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.pipeorientation" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.ahu.doororientation" />
                        </div>
                        <div className="col-lg-12">
                            <Field id="meta.ahu.ISPRERAIN" />
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'AHUSectionForm', // a unique identifier for this form
    enableReinitialize: true,
    //
    // getPanelStructure: () => {
    //     let test = [{"group1": "group1"}, {"group1": "group2"}]
    //     return test
    //
    // }
    // onChange: (values, dispatch, props) => {
    //   clearTimeout(this.timeout)
    //   this.timeout = setTimeout(() => {
    //     dispatch(syncAhuComponentValues(values))
    //   }, 500)
    // },
})(AHUForm)
