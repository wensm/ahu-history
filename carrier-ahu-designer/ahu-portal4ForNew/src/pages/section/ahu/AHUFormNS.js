import {
    NON_STANDARD_PARAMETER,
    GENERAL_NON_STANDARD
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'

class AHUFormNS extends React.Component {

	handleClickAhuWH() {
	  

  }

  render() {
    const { onSairvolumeChange } = this.props
    return (
      <form data-name="AHUSectionFormNS">
        <div className="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
          	
        	<Group title={intl.get(NON_STANDARD_PARAMETER)} id="group1">
	            <div className="col-lg-12">
	              <Field id="ns.ahu.enable"/>
	            </div>
	            <div className="col-lg-3">
	              <Field id="ns.ahu.Price"/>
	            </div>
	            <div className="col-lg-9">
	              <Field id="ns.ahu.MEMO"/>
	            </div>
	            
            </Group>
          
        	<Group title={intl.get(GENERAL_NON_STANDARD)} id="group1">
	        	<div className="col-lg-12">
	        		<Field id="ns.ahu.deformation"/>
	        	</div>
	            <div className="col-lg-3">
	              	<Field id="ns.ahu.nsheight" />
	            </div>
	            <div className="col-lg-3">
	              	<Field id="ns.ahu.nswidth" />
	            </div>
	            <div className="col-lg-3">
	              	<Field id="ns.ahu.deformationSerial"/>
	            </div>
	            <div className="col-lg-3">
	              	<Field id="ns.ahu.deformationPrice"/>
	            </div>
	        </Group>

          </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'AHUSectionFormNS', // a unique identifier for this form
  enableReinitialize: true,
})(AHUFormNS)
