import {
    SUMMER,
    WINTER,
    RETURN_AIR_PARAMETER_SUMMER,
    RETURN_AIR_PARAMETER_WINTER,
    MIX_FORM,
    TOP_RETURN_AIR,
    BACK_RETURN_AIR,
    LEFT_SIDE_RETURN_AIR,
    RIGHT_SIDE_RETURN_AIR,
    BOTTOM_RETURN_AIR,
    AIR_VALVE_OPTION,
    OTHER_OPTION,
    RESISTANCE_AND_WEIGHT,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

import mix from '../../../images/pro_mix_b.png'
const requireContext = require.context("../../../images/mix", false, /^\.\/.*\.png$/);
const images = requireContext.keys().map(requireContext);
const MAP = {
    mix
}
images.map((v, k) => {
    MAP[`mix${k + 1}`] = v
})
class Mix extends React.Component {
    clickNavi(e) {
        let theLink = e.target
        if (theLink) {
            let theHref = theLink.getAttribute("href")
            theHref = theHref.substr(1)
            const element = document.getElementById(theHref)
            if (element) element.scrollIntoView()
            e.preventDefault()
        }
    }

    render() {

        const {
            isCompleted,
            onCalcRelativeHumidity,
            // onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcMixAir1,
            onCalcMixAir2,
            onCalcMixAir4,
            componentValue,
            invalid, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_mix_EnableWinter) : false
        const ahuDirection = this.props.componentValue
        let selectedComponentId = this.props.selectedComponent.id;
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        const section = $(element).attr('data-section')
        let hasCombine = false;
        let parent = $(`[data-section-id='${selectedComponentId}']`).parent().children('[data-section]')
        parent.each((index, record) => {
            if (record.getAttribute('data-section') == 'ahu.combinedMixingChamber') {
                hasCombine = true//找到新回排
            }
        })
        function imgSet(mix1, mix2, mix3, mix4, mix5, mix6, mix7, mix8, mix9, mix10, mix11, mix12, mix13, mix14, mix15) {
            let letmix
            if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix1]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix2]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix3]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnLeft) == true) {
                letmix = MAP[mix4]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix5]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix6]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix7]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix8]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix9]
            } else if (eval(ahuDirection.meta_section_mix_returnButtom) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix10]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true) {
                letmix = MAP[mix11]
            } else if (eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix12]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix13]
            } else if (eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix14]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true) {
                letmix = MAP[mix15]
            }
            else {
                letmix = MAP[mix10]
            }
            return $(element).css("background-image", 'url(' + letmix + ')')
        }
        switch (ahuDirection.meta_section_airDirection) {
            case "R":
                if (hasCombine === true) {
                    imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                    break;
                }
                imgSet('mix12', 'mix13', 'mix15', 'mix16', 'mix6', 'mix8', 'mix24', 'mix26', 'mix5', 'mix19', 'mix10', 'mix21', 'mix5', 'mix', 'mix23')
                break;
            default:
                imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                break;
        }
        return (
            <form data-name="Mix">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div>
                        <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                            <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                role="tab"
                                data-toggle="tab">{intl.get(SUMMER)}</a>
                            </li>
                            <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                data-toggle="tab">{intl.get(WINTER)}</a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="summer">
                                <div style={{ marginBottom: '5px' }}>
                                    <Field id="meta.section.mix.EnableSummer" />
                                </div>
                                <Group title={intl.get(RETURN_AIR_PARAMETER_SUMMER)} id="group6">
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SInDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_SNewDryBulbT',
                                                'meta_section_mix_SNewWetBulbT',
                                                'meta_section_mix_SInDryBulbT',
                                                'meta_section_mix_SInWetBulbT',
                                                ['meta_section_mix_SOutDryBulbT',
                                                    'meta_section_mix_SOutWetBulbT',
                                                    'meta_section_mix_SOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SInRelativeT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SNewWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                newValue, 'InDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SInWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_SNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'InWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>

                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SInRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInRelativeT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInWetBulbT',
                                                        'meta_section_mix_SNewWetBulbT',
                                                        'meta_section_mix_SInRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'inRelativeT'],
                                                    newValue, 'InRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SNewDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_SNewDryBulbT',
                                                'meta_section_mix_SNewWetBulbT',
                                                'meta_section_mix_SInDryBulbT',
                                                'meta_section_mix_SInWetBulbT',
                                                ['meta_section_mix_SOutDryBulbT',
                                                    'meta_section_mix_SOutWetBulbT',
                                                    'meta_section_mix_SOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SInRelativeT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SNewWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                newValue, 'NewDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SNewWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_SNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SNewRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInRelativeT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInWetBulbT',
                                                        'meta_section_mix_SNewWetBulbT',
                                                        'meta_section_mix_SNewRelativeT',
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'newRelativeT'],
                                                    newValue, 'NewRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NARatio"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_NARatio'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'naratio'],
                                                    newValue, 'NARatio', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NAVolume"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir4(
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NARatio',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_NAVolume'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'naratio', 'inRelativeT', 'newRelativeT', 'navolume'],
                                                    newValue, 'NAVolume', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SOutDryBulbT" />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SOutWetBulbT"
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SOutRelativeT"
                                            />
                                        </div>
                                    </div>
                                </Group>
                            </div>
                            <div role="tabpanel" className="tab-pane" id="winter">
                                <div style={{ marginBottom: '5px' }}>
                                    <Field id="meta.section.mix.EnableWinter" />
                                </div>
                                {enableWinter ? <Group title={intl.get(RETURN_AIR_PARAMETER_WINTER)} id="group6">
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WInDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_WNewDryBulbT',
                                                'meta_section_mix_WNewWetBulbT',
                                                'meta_section_mix_WInDryBulbT',
                                                'meta_section_mix_WInWetBulbT',
                                                ['meta_section_mix_WOutDryBulbT',
                                                    'meta_section_mix_WOutWetBulbT',
                                                    'meta_section_mix_WOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_WInRelativeT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'inWetBulbT'],
                                                newValue, 'InDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WInWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WInWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'inWetBulbT'],
                                                    newValue, 'InWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WInRelativeT" maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInRelativeT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInWetBulbT',
                                                        'meta_section_mix_WNewWetBulbT',
                                                        'meta_section_mix_WInRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'inRelativeT'],
                                                    newValue, 'InRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WNewDryBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WNewWetBulbT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WNewRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInRelativeT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInWetBulbT',
                                                        'meta_section_mix_WNewWetBulbT',
                                                        'meta_section_mix_WNewRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'newRelativeT'],
                                                    newValue, 'NewRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NARatio"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_NARatio'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'naratio'],
                                                    newValue, 'NARatio', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NAVolume"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir4(
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NARatio',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_NAVolume'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'naratio', 'inRelativeT', 'newRelativeT', 'navolume'],
                                                    newValue, 'NAVolume', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WOutDryBulbT" />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WOutWetBulbT"
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WOutRelativeT"
                                            />
                                        </div>
                                    </div>
                                </Group>:''}
                            </div>
                        </div>
                    </div>


                    <Group title={intl.get(MIX_FORM)} id="group1">
                        <div className="col-lg-2">
                            <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.returnTop" />
                        </div>
                        <div className="col-lg-2">
                            <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.returnBack" />
                        </div>
                        <div className="col-lg-2">
                            <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.returnLeft" />
                        </div>
                        <div className="col-lg-2">
                            <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.returnRight" />
                        </div>
                        <div className="col-lg-2">
                            <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.returnButtom" />
                        </div>
                    </Group>
                    <Group title={intl.get(AIR_VALVE_OPTION)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperOutlet" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperMeterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperExecutor" />
                        </div>
                    </Group>
                    <Group title={intl.get(OTHER_OPTION)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.DoorO" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.DoorDirection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.uvLamp" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.uvLampSerial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.PositivePressureDoor" />
                        </div>
                        <div className="col-lg-9">
                            <Field id="meta.section.mix.fixRepairLamp" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.freshAirHood" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.outletPressure" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group5">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                //  type="button" className="btn btn-primary"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7' }}
                                disabled={!this.props.canConfirm || this.props.invalid}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'mix', // a unique identifier for this form
    enableReinitialize: true,
})(Mix)
