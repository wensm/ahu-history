import {
  COIL_INFORMATION,
  COIL_OPTION,
  SUMMER_SLASH_WINTER,
  SUMMER,
  WINTER,
  LIMITED_CONDITION,
  CALCULATION_CONDITION,
  INLET_WIND_TEMP_SUMMER,
  WATER_SIDE_PARAMETER_SUMMER,
  CALCULATE_SUMMER,
  INLET_WIND_TEMP_WINTER,
  OUTLET_AIR_PARAMETER_WINTER,
  WATER_SIDE_PARAMETER_WINTER,
  CALCULATE_WINTER,
  WEIGHT_AND_SECTION_LENGTH,
  OPERATION,
  CONFIRM,
  COOLING_PARAMETER_INPUT,
  HEATING_PARAMETER_INPUT,
  COIL_MEMO,
  INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

const TabPane = Tabs.TabPane;


class CoolingCoil extends React.Component {
  callback(e) {
    this.setState({
      tabs: e
    })
  }
  constructor(props) {
    super(props)
    this.state = {
      tabs: 1
    }
  }
  render() {
    const {
      isCompleted,
      onCalcCoolingCoil,
      onCompleteSection,
      onCalcRelativeHumidity,
      onCalcRelativeHumidity2,
      onCalcWetBulbTemperature,
      unitSetting,
      metaUnit,
      metaLayout,
      propUnit,
      componentValue
    } = this.props
    const { tabs } = this.state
    let enableWinter = componentValue ? eval(componentValue.meta_section_coolingCoil_EnableWinter) : false
      // componentValue && console.log('this.props', componentValue.meta_section_coolingCoil_EnableWinter)
    return (
      <form data-name="CoolingCoil">
        <div className="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(COIL_INFORMATION)} id="group1">
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.tubeDiameter" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.rows" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.circuit" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.finDensity" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.finType" />
            </div>
          </Group>
          <Group title={intl.get(COIL_OPTION)} id="group3">
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.connections" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.eliminator" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.pipe" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.waterCollecting" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.drainpanType" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.drainpanMaterial" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.baffleMaterial" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.coilFrameMaterial" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.uTrap" />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.uTrapNum" />
            </div>
          </Group>
          <Group title={intl.get(COOLING_PARAMETER_INPUT)} id="group13">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">{intl.get(SUMMER_SLASH_WINTER)}</h3>
              </div>
              <div className="panel-body" style={{ padding: '15px 0' }}>
                <div>
                  {/* <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                  <li role="presentation" className="active"><a href="#summer" aria-controls="summer" role="tab" data-toggle="tab">{intl.get(SUMMER)}</a></li>
                  <li role="presentation"><a href="#winter" aria-controls="winter" role="tab" data-toggle="tab">{intl.get(WINTER)}</a></li>
                </ul> */}
                  <Tabs onChange={(e) => this.callback(e)} type="card">
                    <TabPane tab={intl.get(SUMMER)} key="1">
                      <div role="tabpanel" className="tab-pane active" id="summer">
                        <Group title={intl.get(COOLING_PARAMETER_INPUT)}  showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SenteringFluidTemperature" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SmaxWPD" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Scoolant" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Sconcentration" />
                          </div>
                        </Group>

                        <Group title={intl.get(LIMITED_CONDITION)}  showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SqualifiedConditions" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.ScoldQ" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SsensibleCapacity" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SleavingAirDB" />
                          </div>

                        </Group>

                        <Group title={intl.get(CALCULATION_CONDITION)}  showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.ScalculationConditions" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SWTAScend" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SwaterFlow" />
                          </div>
                        </Group>


                        <Group title={intl.get(INLET_WIND_TEMP_SUMMER)}  showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SInDryBulbT" 
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_coolingCoil_SInWetBulbT', newValue, 'meta_section_coolingCoil_SInRelativeT', 'meta_section_coolingCoil_SInDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                            
                            />

                              <Field id="meta.section.coolingCoil.SAmbientDryBulbT"
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_coolingCoil_SAmbientWetBulbT', newValue, 'meta_section_coolingCoil_SAmbientRelativeT', 'meta_section_coolingCoil_SAmbientDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                              
                              />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.SInWetBulbT"
                              // 
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_coolingCoil_SInDryBulbT', newValue, 'meta_section_coolingCoil_SInRelativeT', 'meta_section_coolingCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                            />

                              <Field id="meta.section.coolingCoil.SAmbientWetBulbT"
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_coolingCoil_SAmbientDryBulbT', newValue, 'meta_section_coolingCoil_SAmbientRelativeT', 'meta_section_coolingCoil_SAmbientWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                              
                              />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.SInRelativeT"
                              maxValue={100}
                              // 
                              onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_coolingCoil_SInDryBulbT', newValue, 'meta_section_coolingCoil_SInWetBulbT', 'meta_section_coolingCoil_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                            />

                              <Field id="meta.section.coolingCoil.SAmbientRelativeT" maxValue={100} 
                              onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_coolingCoil_SAmbientDryBulbT', newValue, 'meta_section_coolingCoil_SAmbientWetBulbT', 'meta_section_coolingCoil_SAmbientRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                              
                              />
                          </div>

                        </Group>
                        <Group title={intl.get(WATER_SIDE_PARAMETER_SUMMER)}  showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SOutDryBulbT" />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.SOutWetBulbT"

                            />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.SOutRelativeT"

                            />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SAirResistance" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SreturnColdQ" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SreturnSensibleCapacity" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SreturnEnteringFluidTemperature" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SwaterResistance" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SreturnWaterFlow" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Sfluidvelocity" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.SSweatCheck" />
                          </div>
                          <div className="col-lg-3" style={{ paddingTop: '25px' }}>
                            {/* <button
                          // disabled={!this.props.canConfirm ||this.props.invalid}
                          type="button"
                          className="btn btn-primary"
                          style={{ width: '100%' }}
                          data-toggle="modal"
                          data-target="#coolingCoilModal"
                          onClick={() => { onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "S", unitSetting, metaUnit, metaLayout, propUnit) }}
                        >
                          {intl.get(CALCULATE_SUMMER)}
                        </button> */}
                          </div>
                          <div className="col-lg-12">
                            {intl.get(COIL_MEMO).split('<_>')[0]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[1]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[2]}<br />&nbsp;&nbsp;&nbsp;&nbsp;
                            {intl.get(COIL_MEMO).split('<_>')[3]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[4]}
                          </div>
                        </Group>
                      </div>
                    </TabPane>
                    <TabPane tab={intl.get(WINTER)} key="2">
                      <div role="tabpanel" className="tab-pane" id="winter">
                        <div style={{ marginBottom: '5px' }}>
                          <Field id="meta.section.coolingCoil.EnableWinter" />
                        </div>

                        {enableWinter ? <Group title={intl.get(HEATING_PARAMETER_INPUT)} showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WenteringFluidTemperature" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WmaxWPD" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Wcoolant" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Wconcentration" />
                          </div>
                        </Group>:''}

                        {enableWinter ? <Group title={intl.get(LIMITED_CONDITION)} showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WqualifiedConditions" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WheatQ" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WleavingAirDB" />
                          </div>

                        </Group>:''}

                        {enableWinter ? <Group title={intl.get(CALCULATION_CONDITION)} showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WcalculationConditions" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WWTAScend" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WwaterFlow" />
                          </div>
                        </Group>:''}


                        {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER)} showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WInDryBulbT" 
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_coolingCoil_WInWetBulbT', newValue, 'meta_section_coolingCoil_WInRelativeT', 'meta_section_coolingCoil_WInDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                            
                            />

                              <Field id="meta.section.coolingCoil.WAmbientDryBulbT"
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_coolingCoil_WAmbientWetBulbT', newValue, 'meta_section_coolingCoil_WAmbientRelativeT', 'meta_section_coolingCoil_WAmbientDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                              
                              />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.WInWetBulbT"
                              
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_coolingCoil_WInDryBulbT', newValue, 'meta_section_coolingCoil_WInRelativeT', 'meta_section_coolingCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                            />

                              <Field id="meta.section.coolingCoil.WAmbientWetBulbT"
                              onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_coolingCoil_WInDryBulbT', newValue, 'meta_section_coolingCoil_WInRelativeT', 'meta_section_coolingCoil_WAmbientWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                              />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.WInRelativeT"
                              maxValue={100}
                              
                              onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_coolingCoil_WInDryBulbT', newValue, 'meta_section_coolingCoil_WInWetBulbT', 'meta_section_coolingCoil_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                            />

                              <Field id="meta.section.coolingCoil.WAmbientRelativeT" maxValue={100} 
                              onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_coolingCoil_WInDryBulbT', newValue, 'meta_section_coolingCoil_WInWetBulbT', 'meta_section_coolingCoil_WAmbientRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                              />
                          </div>
                        </Group>:''}
                        {enableWinter ? <Group title={intl.get(OUTLET_AIR_PARAMETER_WINTER)} showLeftBorder={false}>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WOutDryBulbT" />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.WOutWetBulbT"
                            />
                          </div>
                          <div className="col-lg-3">
                            <Field
                              id="meta.section.coolingCoil.WOutRelativeT"
                            />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WAirResistance" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WreturnHeatQ" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WreturnEnteringFluidTemperature" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WwaterResistance" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WreturnWaterFlow" />
                          </div>
                          {/* <div className="col-lg-3">
                        <Field id="meta.section.coolingCoil.WWTAScend"/>
                      </div>*/}

                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.Wfluidvelocity" />
                          </div>
                          <div className="col-lg-3">
                            <Field id="meta.section.coolingCoil.WSweatCheck" />
                          </div>
                          <div className="col-lg-3" style={{ paddingTop: '25px' }}>
                            {/* <button
                          // disabled={!this.props.canConfirm ||this.props.invalid}
                          type="button"
                          className="btn btn-primary"
                          style={{ width: '100%' }}
                          data-toggle="modal"
                          data-target="#coolingCoilModal"
                          onClick={() => onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "W", unitSetting, metaUnit, metaLayout, propUnit)}
                        >
                          {intl.get(CALCULATE_WINTER)}
                        </button> */}
                          </div>
                          <div className="col-lg-12">
                            {intl.get(COIL_MEMO).split('<_>')[0]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[1]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[2]}<br />&nbsp;&nbsp;&nbsp;&nbsp;
                            {intl.get(COIL_MEMO).split('<_>')[3]}<br />
                            {intl.get(COIL_MEMO).split('<_>')[4]}
                          </div>
                        </Group>:''}
                      </div>
                    </TabPane>
                  </Tabs>
                  <div className="tab-content">


                  </div>
                </div>
              </div>
            </div>
          </Group>
          <Group title={intl.get(WEIGHT_AND_SECTION_LENGTH)} id="group12">

            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.driftEliminatorResistance" />
            </div>

            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.sectionL" disabled={true} />
            </div>
            <div className="col-lg-3">
              <Field id="meta.section.coolingCoil.Weight" />
            </div>
          </Group>

          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
            <div className="col-lg-12" style={{ paddingBottom: '2px' }}>
              {tabs == '1' ? <Button
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7' }}
                // disabled={!this.props.canConfirm ||this.props.invalid}
                data-toggle="modal"
                data-target="#coolingCoilModal"
                onClick={() => { onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "S", unitSetting, metaUnit, metaLayout, propUnit) }}
              >
                {intl.get(CALCULATE_SUMMER)}
              </Button> : <Button
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', color:'white' }}
                // disabled={!this.props.canConfirm ||this.props.invalid}
                disabled = {!enableWinter}
                data-toggle="modal"
                data-target="#coolingCoilModal"
                onClick={() => onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "W", unitSetting, metaUnit, metaLayout, propUnit)}
              >
                  {intl.get(CALCULATE_WINTER)}
                </Button>}
              <Button
              disabled={!this.props.canConfirm || this.props.invalid} 
              size='small' type="primary" 
              style={{ marginLeft: '10px', backgroundColor: '#337ab7' }}
              onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                {intl.get(CONFIRM)}{/*确认按钮*/}
              </Button>
              <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
              </Tooltip>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'CoolingCoil', // a unique identifier for this form
  enableReinitialize: true,
})(CoolingCoil)
