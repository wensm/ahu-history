import {
    NON_STANDARD_OPTION,
    OPERATION,
    CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class SteamCoilNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="SteamCoilNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.steamCoil.enable"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.steamCoil.Price"/>
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.steamCoil.MEMO"/>
            </div>
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{paddingBottom: '10px'}}>
                            <Button 
                            size='small' type="primary"
                            style={{backgroundColor: '#337ab7', marginLeft: '10px' }}
                            className="btn btn-primary"disabled={!this.props.canConfirm ||this.props.invalid} 
                                    onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}>
                                {intl.get(CONFIRM)}
                            </Button>
                        </div>
                    </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'SteamCoilNS', // a unique identifier for this form
  enableReinitialize: true,
})(SteamCoilNS)
