import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    STEAM_COIL_OPTION,
    PERFORMANCE,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button} from 'antd';

import { required, maxLength15, minValue0 } from '../../ahu/Validate'

class SteamCoil extends React.Component {
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcHeatingQ,
            componentValue,
            onCalcHumDryBulbT, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_steamCoil_EnableWinter) : false

        return (
            <form data-name="SteamCoil">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                            role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a></li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="summer">
                            <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.SInDryBulbT"  
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_steamCoil_SInWetBulbT', newValue, 'meta_section_steamCoil_SInRelativeT', 'meta_section_steamCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.SInWetBulbT"
                                            // validate={[required, maxLength15, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_steamCoil_SInDryBulbT', newValue, 'meta_section_steamCoil_SInRelativeT', 'meta_section_steamCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.SInRelativeT"
                                            // validate={[required, maxLength15, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_steamCoil_SInDryBulbT', newValue, 'meta_section_steamCoil_SInWetBulbT', 'meta_section_steamCoil_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.SHeatQ"
                                            // validate={[required, maxLength15, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcHumDryBulbT(
                                                'S',
                                                'meta_section_steamCoil_SInDryBulbT',
                                                'meta_section_steamCoil_SInWetBulbT',
                                                'meta_section_steamCoil_SInRelativeT',
                                                newValue,
                                                ['meta_section_steamCoil_SOutDryBulbT',
                                                    'meta_section_steamCoil_SOutWetBulbT',
                                                    'meta_section_steamCoil_SOutRelativeT',
                                                    'meta_section_steamCoil_SHumDryBulbT',
                                                    'meta_section_steamCoil_SHeatQ',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humDryBulbT', 'heatQ']
                                            )}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.SHumDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcHeatingQ(
                                                'S',
                                                'meta_section_steamCoil_SInDryBulbT',
                                                'meta_section_steamCoil_SInWetBulbT',
                                                'meta_section_steamCoil_SInRelativeT',
                                                newValue,
                                                ['meta_section_steamCoil_SOutDryBulbT',
                                                    'meta_section_steamCoil_SOutWetBulbT',
                                                    'meta_section_steamCoil_SOutRelativeT',
                                                    'meta_section_steamCoil_SHeatQ',
                                                    'meta_section_steamCoil_SHumDryBulbT',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'heatQ', 'humDryBulbT'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.SOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.SOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.SOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group>
                        </div>
                        <div role="tabpanel" className="tab-pane" id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.steamCoil.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.WInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_steamCoil_WInWetBulbT', newValue, 'meta_section_steamCoil_WInRelativeT', 'meta_section_steamCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        
                                             />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_steamCoil_WInDryBulbT', newValue, 'meta_section_steamCoil_WInRelativeT', 'meta_section_steamCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WInRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_steamCoil_WInDryBulbT', newValue, 'meta_section_steamCoil_WInWetBulbT', 'meta_section_steamCoil_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WHeatQ"
                                            validate={[required, maxLength15, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcHumDryBulbT(
                                                'W',
                                                'meta_section_steamCoil_WInDryBulbT',
                                                'meta_section_steamCoil_WInWetBulbT',
                                                'meta_section_steamCoil_WInRelativeT',
                                                newValue,
                                                ['meta_section_steamCoil_WOutDryBulbT',
                                                    'meta_section_steamCoil_WOutWetBulbT',
                                                    'meta_section_steamCoil_WOutRelativeT',
                                                    'meta_section_steamCoil_WHumDryBulbT',
                                                    'meta_section_steamCoil_WHeatQ',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outDryBulbT', 'heatQ']
                                            )}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WHumDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcHeatingQ(
                                                'W',
                                                'meta_section_steamCoil_WInDryBulbT',
                                                'meta_section_steamCoil_WInWetBulbT',
                                                'meta_section_steamCoil_WInRelativeT',
                                                newValue,
                                                ['meta_section_steamCoil_WOutDryBulbT',
                                                    'meta_section_steamCoil_WOutWetBulbT',
                                                    'meta_section_steamCoil_WOutRelativeT',
                                                    'meta_section_steamCoil_WHeatQ',
                                                    'meta_section_steamCoil_WHumDryBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'heatQ', 'outDryBulbT'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>


                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.steamCoil.WOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.steamCoil.WOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group>:''}
                        </div>
                    </div>
                </div>

                <Group title={intl.get(STEAM_COIL_OPTION)} id="group7">
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.RowNum" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.Material" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.ConnectionType" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.BaffleM" />
                    </div>
                </Group>
                <Group title={intl.get(PERFORMANCE)} id="group9">
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.VaporPressure" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.Resistance" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.sectionL" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.steamCoil.Weight" />
                    </div>
                    

                </Group>
                <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                    <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                        <Button 
                        // type="button" className="btn btn-primary" 
                        disabled={!this.props.canConfirm || this.props.invalid} 
                        size='small' type="primary"
                        style={{ marginLeft: '10px', backgroundColor: '#337ab7' }}

                            onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                            {intl.get(CONFIRM)}
                        </Button>
                        <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                            <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                        </Tooltip>
                    </div>
                </Group>
            </form>
        )
    }
}

export default reduxForm({
    form: 'SteamCoil', // a unique identifier for this form
    enableReinitialize: true,
})(SteamCoil)
