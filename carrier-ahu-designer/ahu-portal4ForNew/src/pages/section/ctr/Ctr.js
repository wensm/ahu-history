import {
    OPTIONAL_ACCESSORY,
    RESISTANCE_AND_WEIGHT,
    OPERATION,
    CONFIRM,
    STANDARD_CONFIGURATION,
    DOOR_OPTION,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class Ctr extends React.Component {
    render() {
        const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        return (
            <form data-name="Ctr">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(STANDARD_CONFIGURATION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.ControlCabinet" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.FrequencyConverter" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.Controller" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.ExpansionModule" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.OperationPanel" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.ATSensor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.FATSensor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.RAPTaHSensor" />
                        </div>
                    </Group>

                    <Group title={intl.get(OPTIONAL_ACCESSORY)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.ITaHSensor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.ICCSensor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.APHSensor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.CoilAntifreeze" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.WaterValveActuator" />
                        </div>
                    </Group>

                    <Group title={intl.get(DOOR_OPTION)} id="group2">
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.DoorO" />
                        </div>

                    </Group>

                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.ctr.Weight" />
                        </div>
                    </Group>

                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                //  type="button" className="btn btn-primary"
                                disabled={!this.props.canConfirm || this.props.invalid}
                                //   style={{ marginLeft: '10px' }}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'Ctr', // a unique identifier for this form
    enableReinitialize: true,
})(Ctr)
