import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    HUMIDIFIER_OPTION,
    ATTACHMENT,
    PARAMETER,
    OPERATION,
    CHOOSE_MODEL,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';


class SprayHumidifier extends React.Component {
    render() {
        const {
            componentValue,
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalIsoenthalpyJSBHumidificationVolume,
            onCalIsoenthalpycJSBRelativeHumidity,
            onChooseModel, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_sprayHumidifier_EnableWinter) : false

        let summer = eval(this.props.componentValue['meta_section_sprayHumidifier_EnableSummer'])
        let winter = eval(this.props.componentValue['meta_section_sprayHumidifier_EnableWinter'])
        let season = ''
        if(summer)season='S'
        if(winter)season='W'
        if(summer && winter) season='ALL'
        return (
            <form data-name="SprayHumidifier">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                            role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation" ><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a></li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="summer">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.sprayHumidifier.EnableSummer" />
                            </div>
                            <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.SInDryBulbT" 
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_sprayHumidifier_SInWetBulbT', newValue, 'meta_section_sprayHumidifier_SInRelativeT', 'meta_section_sprayHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                            />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.SInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_sprayHumidifier_SInDryBulbT', newValue, 'meta_section_sprayHumidifier_SInRelativeT', 'meta_section_sprayHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.SInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_sprayHumidifier_SInDryBulbT', newValue, 'meta_section_sprayHumidifier_SInWetBulbT', 'meta_section_sprayHumidifier_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.SHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpycJSBRelativeHumidity(
                                                'meta_section_sprayHumidifier_SInDryBulbT',
                                                'meta_section_sprayHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_sprayHumidifier_SOutDryBulbT',
                                                    'meta_section_sprayHumidifier_SOutWetBulbT',
                                                    'meta_section_sprayHumidifier_SOutRelativeT',
                                                    'meta_section_sprayHumidifier_SJSBRelativeT',
                                                    'meta_section_sprayHumidifier_SHumidificationQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.SJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpyJSBHumidificationVolume(
                                                'meta_section_sprayHumidifier_SInDryBulbT',
                                                'meta_section_sprayHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_sprayHumidifier_SOutDryBulbT',
                                                    'meta_section_sprayHumidifier_SOutWetBulbT',
                                                    'meta_section_sprayHumidifier_SOutRelativeT',
                                                    'meta_section_sprayHumidifier_SHumidificationQ',
                                                    'meta_section_sprayHumidifier_SJSBRelativeT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.SOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.SOutWetBulbT"
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.SOutRelativeT"
                                        />
                                    </div>
                                </div>

                            </Group>
                        </div>
                        <div role="tabpanel" className="tab-pane" id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.sprayHumidifier.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.WInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_sprayHumidifier_WInWetBulbT', newValue, 'meta_section_sprayHumidifier_WInRelativeT', 'meta_section_sprayHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                            />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_sprayHumidifier_WInDryBulbT', newValue, 'meta_section_sprayHumidifier_WInRelativeT', 'meta_section_sprayHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.WInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_sprayHumidifier_WInDryBulbT', newValue, 'meta_section_sprayHumidifier_WInWetBulbT', 'meta_section_sprayHumidifier_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.WHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpycJSBRelativeHumidity(
                                                'meta_section_sprayHumidifier_WInDryBulbT',
                                                'meta_section_sprayHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_sprayHumidifier_WOutDryBulbT',
                                                    'meta_section_sprayHumidifier_WOutWetBulbT',
                                                    'meta_section_sprayHumidifier_WOutRelativeT',
                                                    'meta_section_sprayHumidifier_WJSBRelativeT',
                                                    'meta_section_sprayHumidifier_WHumidificationQ',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.WJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpyJSBHumidificationVolume(
                                                'meta_section_sprayHumidifier_WInDryBulbT',
                                                'meta_section_sprayHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_sprayHumidifier_WOutDryBulbT',
                                                    'meta_section_sprayHumidifier_WOutWetBulbT',
                                                    'meta_section_sprayHumidifier_WOutRelativeT',
                                                    'meta_section_sprayHumidifier_WHumidificationQ',
                                                    'meta_section_sprayHumidifier_WJSBRelativeT',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.sprayHumidifier.WOutDryBulbT"  />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.WOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.sprayHumidifier.WOutRelativeT"

                                        />
                                    </div>
                                </div>

                            </Group>:''}
                        </div>
                    </div>


                    <Group title={intl.get(HUMIDIFIER_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.supplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.ControlM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.Eliminator" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.Material" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.BaffleM" />
                        </div>
                    </Group>
                   
                    <Group title={intl.get(PARAMETER)} id="group0">
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.nozzleN" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.aperture" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.hq" />
                        </div>
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.WaterQuantity"/>
                        </div> */}
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.WaterFlow"/>
                        </div> */}
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.sprayHumidifier.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-3" >
                            <Button 
                                data-toggle="modal"
                                data-target="#SprayHumidifierModal"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onChooseModel(this.props.componentValues, this.props.selectedComponent, season)}>
                                {intl.get(CHOOSE_MODEL)}
                            </Button>
                        </div>
                        <div className="col-lg-3" >
                            <Button  disabled={!this.props.canConfirm || this.props.invalid} 
                            size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>

                    </Group>
                </div>

            </form>
        )
    }
}
export default reduxForm({
    form: 'SprayHumidifier', // a unique identifier for this form
    enableReinitialize: true,
})(SprayHumidifier)
