import {
    NON_STANDARD_OPTION,
    OPERATION,
    CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class SprayHumidifierNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="SprayHumidifierNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.sprayHumidifier.enable"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.sprayHumidifier.Price"/>
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.sprayHumidifier.MEMO"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.sprayHumidifier.nsChangeSupplier"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.sprayHumidifier.nsSupplier"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.sprayHumidifier.nsSupplier1"/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.sprayHumidifier.nsSupplierPrice"/>
            </div>
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        {/*<div className="col-lg-3" >
                            <button type="button" className="btn btn-primary"
                                    data-toggle="modal"
                                    data-target="#SprayHumidifierModal"
                                     
                                    onClick={() => onChooseModel(this.props.componentValues, this.props.selectedComponent, "S")}>
                                {intl.get(CHOOSE_MODEL)}
                            </button>
                        </div>*/}
                        <div className="col-lg-3" >
                            <Button disabled={!this.props.canConfirm ||this.props.invalid}
                            size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                    onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}>
                                {intl.get(CONFIRM)}
                            </Button>
                        </div>

                    </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'SprayHumidifierNS', // a unique identifier for this form
  enableReinitialize: true,
})(SprayHumidifierNS)
