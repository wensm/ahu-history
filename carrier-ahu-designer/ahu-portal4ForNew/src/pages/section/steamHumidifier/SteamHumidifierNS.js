import {
  NON_STANDARD_OPTION,
  OPERATION,
  CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class SteamHumidifierNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="SteamHumidifierNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.steamHumidifier.enable" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.steamHumidifier.Price" />
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.steamHumidifier.MEMO" />
            </div>

            <div className="col-lg-1">
              <Field id="ns.section.steamHumidifier.nsChangeSupplier" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.steamHumidifier.nsSupplier" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.steamHumidifier.nsSupplier1" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.steamHumidifier.nsSupplierF" />
            </div>
            <div className="col-lg-2">
              <Field id="ns.section.steamHumidifier.nsHumidificationQ" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.steamHumidifier.nsSupplierPrice" />
            </div>
          </Group>
          <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
            {/*<div className="col-lg-3">
                            <button type="button" className="btn btn-primary"
                                    data-toggle="modal"
                                    data-target="#SteamHumidifierModal"
                                    
                                    onClick={() => onSteamHumidifierChooseModel(this.props.componentValues, this.props.selectedComponent)}>
                               {intl.get(CHOOSE_MODEL)}
                            </button>
                        </div>*/}

            <div className="col-lg-3">
              <Button
                disabled={!this.props.canConfirm || this.props.invalid}
                size='small' type="primary"
                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}>
                {intl.get(CONFIRM)}
              </Button>
            </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'SteamHumidifierNS', // a unique identifier for this form
  enableReinitialize: true,
})(SteamHumidifierNS)
