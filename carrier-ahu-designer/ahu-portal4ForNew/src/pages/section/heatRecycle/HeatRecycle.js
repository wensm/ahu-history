import {
    INFORMATION,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip } from 'antd';

class HeatRecycle extends React.Component {
    render() {
        const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        return (
            <form data-name="HeatRecycle">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(INFORMATION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.MEMO" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.Season" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.Price" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.InWetBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.OutWetBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.InDryBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.OutDryBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatRecycle.NoStandard" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <button type="button" className="btn btn-primary" disabled={!this.props.canConfirm || this.props.invalid} style={{ marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'HeatRecycle', // a unique identifier for this form
    enableReinitialize: true,
})(HeatRecycle)
