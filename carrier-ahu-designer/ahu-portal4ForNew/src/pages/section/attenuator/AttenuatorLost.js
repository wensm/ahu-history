import {
  OPTION,
  WEIGHT_AND_RESISTANCE,
  OPERATION,
  CONFIRM,
  LEVEL_ONE,
  LEVEL_TWO
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
var myStyle = {
  marginRight: '2px'
};
class AttenuatorLost extends React.Component {
  render() {
    let { arr, deadening} = this.props
    return (
      <div>
        <div className="col-lg-12">
          <input value="HZ" className="col-lg-1" style={myStyle} disabled/>
          <input value="63" className="col-lg-1" style={myStyle} disabled/>
          <input value="125" className="col-lg-1" style={myStyle} disabled/>
          <input value="250" className="col-lg-1" style={myStyle} disabled/>
          <input value="500" className="col-lg-1" style={myStyle} disabled/>
          <input value="1000" className="col-lg-1" style={myStyle} disabled/>
          <input value="2000" className="col-lg-1" style={myStyle} disabled/>
          <input value="4000" className="col-lg-1" style={myStyle} disabled/>
          <input value="8000" className="col-lg-1" style={myStyle} disabled/>
        </div>
        <div className="col-lg-12">
        {
          deadening == 'A' ? <input value={intl.get(LEVEL_ONE)} className="col-lg-1" style={myStyle} disabled/> : <input value={intl.get(LEVEL_TWO)} className="col-lg-1" style={myStyle} disabled/>
        }
        {
          arr.map((record, index) => {
            return <AttenuatorCeil record= {record} index = {index}/>
          })
        }
        </div>
      </div>
       
    )
  }
}
class AttenuatorCeil extends React.Component {
  render() {
    let {record, index} = this.props
    return (
      <input key = {index} value={record} className="col-lg-1" style={myStyle} disabled/>
    )
  }
}

export default AttenuatorLost
