import {
    RECEIVE_PROJECTS,
    PROJECT_PROKEY,
    PROJECT_ID,
    PROJECT_REPORTS,
    CLEAR_REPORTS,
    RECEIVE_UPDATE_PROJECT,
    RECEIVE_PAGE_PROJECT_FILTERUPDATE,
    START_NEW_PROJECT
} from '../actions/projects'
import {
    UPDATE_LANGUAGE
} from '../actions/general'

const projects = (state = {
    sections: [],
    projectList: [],
    prokey: {},
    projectId: '',
    updateProject: {},
    reports: [],
    projectFilterStr:""
}, action) => {
    let value = null
    let componentValue = null
    switch (action.type) {
        case RECEIVE_PROJECTS:
            return Object.assign({}, state, {projectList: action.projects})
        case PROJECT_PROKEY:
            var prokeyObj = JSON.parse(action.prokey)
            var newProkeyObj = {}
            for (var variable in prokeyObj) {
                if (prokeyObj.hasOwnProperty(variable)) {
                    newProkeyObj[variable.replace(/\./gi, '_')] = prokeyObj[variable]
                }
            }
            return Object.assign({}, state, {prokey: newProkeyObj})
        case PROJECT_ID:
            return Object.assign({}, state, {projectId: action.projectId})
        case PROJECT_REPORTS:
            return Object.assign({}, state, {reports: action.reports})
        case CLEAR_REPORTS:
            return Object.assign({}, state, {reports: []})
        case RECEIVE_UPDATE_PROJECT:
            return Object.assign({}, state, {updateProject: action.updatePeoject})
        case RECEIVE_PAGE_PROJECT_FILTERUPDATE:
            return Object.assign({}, state, {projectFilterStr: action.filterStr})
        case START_NEW_PROJECT:
            let updateProject={projectDate: action.projectDate, saler: action.name, priceBase:action.priceBase}
            return Object.assign({}, state, {updateProject: updateProject})
        default:
            return state
    }
}

export default projects
