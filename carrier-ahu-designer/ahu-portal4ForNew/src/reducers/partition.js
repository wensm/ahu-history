import {
    RECEIVE_AHU_INFO, RECEIVE_AHU_SECTIONS, RECEIVE_AHU_Partitions, RECEIVE_PARTITION_META,
    initAhuLayout, CLEAN_Partitions
} from '../actions/ahu'
import {
    PARTITION_PANEL_DEVIDED,
    PARTITION_PANEL_DELETED,
    PARTITION_PANEL_RECT_SELECTED,
    PARTITION_PANEL_SELECTED,
    PARTITION_PANEL_RECT_UPDATED,
    PARTITION_PANEL_TYPE_UPDATED,
    PARTITION_PANEL_LINE_UPDATED,
    PARTITION_FACE_SWITCHED,
    PARTITION_PANEL_RECT_UPDATE_CONFIRM,
    PARTITION_SELECTED,
    PARTITION_PANEL_SPLIT,
    PARTITION_PANEL_FORWARD,
    PARTITION_PANEL_BACKWARD,
    PARTITION_PARTITIONS_SAVED,
    searchParentPanel,
    devidePanel,
    deleteSubPanel,
    subPanelChanged,
    subPanelTypeChanged,
    subPanelLineChanged,
    backwardPartition,
    forwardPartition,
    splitPartition,
    calcSubPanelIndexes,
    savePartitions,
    PARTITION_TIP,
    PARTITION_RESETTIP,
    PARTITION_TIP_SAVE,
    PARTITION_PANEL_CLEAN,
    PARTITION_CHANGEBASE,
    PARTITION_SPIN,
    PARTITION_PANEL_LOCKING,
    PARTITION_PANEL_MERGES,
    doMergePanle,
    PARTITION_SYNCAANDB
} from '../actions/partition'
import {
    reorderPartition
} from '../models/partition'

const partition = (state = {
    editingAHUPartitions: [],
    sections: [],
    currentPanelIndex: 0,
    currentPartitionIndex: 0,
    currentEPanel: {},
    currentESubPanel: {},
    currentEParentPanel: {},
    tip: 'default',
    show: 1,
    panelConfirmTip: [],
    spinning: false,
    fromMouseToLeft: 0,
    lockingPanel: ''
}, action) => {
    switch (action.type) {
        case RECEIVE_AHU_SECTIONS:
            return Object.assign({}, state, {
                sections: action.ahuSections
            })
        case RECEIVE_AHU_INFO:
            var ahuMetaProps = JSON.parse(action.ahu.unit.metaJson)
            var sections = action.ahu.parts.sort((a, b) => Number(a.position) - Number(b.position))
            sections.forEach((section, index) => {
                var props = {}
                const sectionProps = JSON.parse(section.metaJson)
                // . => _
                for (var variable in sectionProps) {
                    if (sectionProps.hasOwnProperty(variable)) {
                        props[variable.replace(/\./gi, '_')] = sectionProps[variable]
                    }
                }
                if (Number(section.position) === 0) {
                    console.error('section position must greater than 0')
                }
            })
            // Resolve layout info
            let layoutStr = action.ahu.unit.layoutJson
            let layout = {}
            if (layoutStr && (layoutStr.trim() != '')) {
                layout = JSON.parse(layoutStr)
            } else {
                layout = initAhuLayout(sections)
            }
            return Object.assign({}, state, {
                editingAHU: action.ahu.unit,
                editingAHUProps: ahuMetaProps,
                sections: sections,
                layout: layout,
                panelConfirmTip: []

            })
        case RECEIVE_PARTITION_META:
            return Object.assign({}, state, { metadata: action.data })
        case RECEIVE_AHU_Partitions:
            var partitionProps = JSON.parse(action.ahuPartitions.partitionJson)

            for (let index in partitionProps[0].panels) {
                calcSubPanelIndexes(partitionProps[0].panels[index])
            }
            reorderPartition(partitionProps, state.layout)
            return Object.assign({}, state, {
                currentPartitionPObj: action.ahuPartitions,
                editingAHUPartitions: partitionProps,
                currentPanelIndex: -1,
                currentPartitionIndex: 0,
                currentEPanel: null
            })
        case CLEAN_Partitions:
            return Object.assign({}, state, {
                editingAHUPartitions: [],
            })
        case PARTITION_PANEL_DELETED:
            let delEPanel = deleteSubPanel(action.currentPanel, action.subPanelId)
            let deleditingAHUPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            deleditingAHUPartitions[state.currentPartitionIndex].panels[delEPanel.id] = delEPanel
            calcSubPanelIndexes(delEPanel)
            return Object.assign({}, state, {
                currentEPanel: delEPanel,
                editingAHUPartitions: deleditingAHUPartitions,
            })
        case PARTITION_PANEL_DEVIDED:
            let copyCurrentPanel = { ...action.currentPanel }
            let { panel, dividePanel } = devidePanel(copyCurrentPanel, action.subPanelId, action.direction, action.product, action.percent)
            // console.log('panel, dividePanel', panel, dividePanel)
            let eveditingAHUPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            eveditingAHUPartitions[state.currentPartitionIndex].panels[panel.id] = panel

            // console.log('eveditingAHUPartitions[state.currentPartitionIndex]', eveditingAHUPartitions[state.currentPartitionIndex])
            //eveditingAHUPartitions[state.currentPartitionIndex]  后面需要修改dividers，用来动态变更页面上的框

            calcSubPanelIndexes(panel)
            return Object.assign({}, state, {
                currentEPanel: panel,
                currentESubPanel: dividePanel.subPanels[0],
                currentEParentPanel: dividePanel,
                editingAHUPartitions: eveditingAHUPartitions
            })
        case PARTITION_PANEL_RECT_SELECTED:
            // console.log(action)
            let serCurr = { ...action.currentPanel }
            let tempPanel = searchParentPanel(serCurr, action.selectedPanel.id)

            // console.log('currentEPanel', serCurr)
            // console.log('currentESubPanel', action.selectedPanel)
            // console.log('currentEParentPanel', tempPanel)
            // console.log('currentPanelIndex', serCurr.id)
            return Object.assign({}, state, {
                currentEPanel: serCurr,
                currentESubPanel: action.selectedPanel,
                currentEParentPanel: tempPanel,
                currentPanelIndex: serCurr.id,
                fromMouseToLeft: action.fromMouseToLeft
            })
        case PARTITION_PANEL_SELECTED:
            // console.log(action)
            let spanel;
            if (action.currentPanel != state.currentEPanel) {
                spanel = action.currentPanel
            }
            return Object.assign({}, state, {
                currentEPanel: spanel,
                currentESubPanel: null,
                currentEParentPanel: null,
                currentPanelIndex: spanel ? spanel.id : -1
            })
        case PARTITION_PANEL_RECT_UPDATED:
            let result = subPanelChanged(action.currentPanel, action.parentPanel, action.thisPanel, action.num, action.name)
            let rceditingAHUPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            rceditingAHUPartitions[state.currentPartitionIndex].panels[result.currentPanel.id] = result.currentPanel

            return Object.assign({}, state, {
                currentESubPanel: result.thisPanel,
                currentEParentPanel: result.parentPanel,
                currentEPanel: result.currentPanel,
                editingAHUPartitions: rceditingAHUPartitions
            })
        case PARTITION_PANEL_TYPE_UPDATED:
            let tresult = subPanelTypeChanged(action.currentPanel, action.parentPanel, action.subPanel, action.panelType)
            let tceditingAHUPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            tceditingAHUPartitions[state.currentPartitionIndex].panels[tresult.currentPanel.id] = tresult.currentPanel

            return Object.assign({}, state, {
                currentESubPanel: tresult.subPanel,
                currentEParentPanel: tresult.parentPanel,
                currentEPanel: tresult.currentPanel,
                editingAHUPartitions: tceditingAHUPartitions
            })
        case PARTITION_PANEL_LINE_UPDATED:
            let lresult = subPanelLineChanged(action.currentPanel, action.parentPanel, action.subPanel, action.divideType)

            let leditingAHUPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            leditingAHUPartitions[state.currentPartitionIndex].panels[lresult.currentPanel.id] = lresult.currentPanel
            return Object.assign({}, state, {
                currentESubPanel: lresult.subPanel,
                currentEParentPanel: lresult.parentPanel,
                currentEPanel: lresult.currentPanel,
                editingAHUPartitions: leditingAHUPartitions
            })
        case PARTITION_FACE_SWITCHED:
            return Object.assign({}, state, {
                currentPanelIndex: action.face,
                currentEPanel: action.currentPanel,
                currentESubPanel: {},
                currentEParentPanel: {}
            })
        case PARTITION_PANEL_RECT_UPDATE_CONFIRM:
            let dupPartitions = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            dupPartitions[state.currentPartitionIndex].panels[state.currentPanelIndex] = state.currentEPanel
            // console.log('dupPartitions', dupPartitions)
            return state
            return Object.assign({}, state, {
                editingAHUPartitions: dupPartitions
            })
        case PARTITION_SELECTED:
            if (!state.editingAHUPartitions[action.partitionIndex].panels) {
                return Object.assign({}, state, {
                    currentPartitionIndex: action.partitionIndex,
                    // currentPanelIndex: 0,
                    currentEPanel: {},
                    // currentESubPanel: sPanel,
                    // currentEParentPanel: pPanel,
                    show: action.show
                })
            }
            let cPanel = state.editingAHUPartitions[action.partitionIndex].panels[0],
                pPanel = {},
                sPanel = cPanel;

            function findFirstSubPanel(panel) {
                if (panel.subPanels && panel.subPanels.length) {
                    pPanel = panel;
                    return findFirstSubPanel(panel.subPanels[0])
                }
            }

            sPanel = findFirstSubPanel(cPanel)
            for (let index in state.editingAHUPartitions[action.partitionIndex].panels) {
                calcSubPanelIndexes(state.editingAHUPartitions[action.partitionIndex].panels[index])
            }


            return Object.assign({}, state, {
                currentPartitionIndex: action.partitionIndex,
                // currentPanelIndex: 0,
                // currentEPanel: cPanel,
                currentEPanel: {},
                // currentESubPanel: sPanel,
                // currentEParentPanel: pPanel,
                show: action.show
            })
        case PARTITION_PANEL_BACKWARD:
            result = backwardPartition(state.editingAHUPartitions, action.partitionIndex, action.layout)
            reorderPartition(result, state.layout)
            return Object.assign({}, state, {
                editingAHUPartitions: result
            })
        case PARTITION_PANEL_FORWARD:
            result = forwardPartition(state.editingAHUPartitions, action.partitionIndex, action.layout)
            reorderPartition(result, state.layout)
            return Object.assign({}, state, {
                editingAHUPartitions: result
            })
        case PARTITION_PANEL_SPLIT:
            result = splitPartition(state.editingAHUPartitions, action.partitionIndex, action.layout)
            reorderPartition(result, state.layout)
            return Object.assign({}, state, {
                editingAHUPartitions: result
            })
        case PARTITION_PARTITIONS_SAVED:
            var partitionProps = JSON.parse(action.partitionsPObj.partitionJson)
            return Object.assign({}, state, {
                currentPartitionPObj: action.partitionsPObj,
                editingAHUPartitions: partitionProps,
                tip: 'default'
            })
        case PARTITION_TIP:
            return Object.assign({}, state, {
                tip: action.msg
            })
        case PARTITION_RESETTIP:
            return Object.assign({}, state, {
                tip: action.tips
            })
        case PARTITION_TIP_SAVE:
            let carr = [...state.panelConfirmTip]
            let cobj = {}
            cobj.show = action.show
            cobj.time = action.time
            carr.push(cobj)
            return Object.assign({}, state, {
                panelConfirmTip: carr
            })
        case PARTITION_PANEL_CLEAN:
            return {
                editingAHUPartitions: [],
                sections: [],
                currentPanelIndex: 0,
                currentPartitionIndex: 0,
                currentEPanel: {},
                currentESubPanel: {},
                currentEParentPanel: {},
                tip: 'default',
                show: 1,
                panelConfirmTip: []
            }
        case PARTITION_CHANGEBASE:
            //        console.log('action', action)
            let copyEditingAHUPartitions = [...state.editingAHUPartitions]
            copyEditingAHUPartitions.forEach((ele) => {
                if (ele.pos == action.pos) {
                    ele.base = action.value
                }
            })
            return Object.assign({}, state, {
                editingAHUPartitions: copyEditingAHUPartitions
            })
        case PARTITION_SPIN:
            let spinningBool = action.bool
            return Object.assign({}, state, {
                spinning: spinningBool
            })
        case PARTITION_PANEL_LOCKING:
            let lockingPanel = action.currentPanel
            return Object.assign({}, state, {
                lockingPanel: lockingPanel
            })
        case PARTITION_PANEL_MERGES:
            // let thisPanel = action.thisPanel
            let eveditingAHUPartitions2 = JSON.parse(JSON.stringify(state.editingAHUPartitions))

            // let {lockingPanel2, rootPanel2} = doMergePanle(thisPanel, state.lockingPanel, eveditingAHUPartitions2[state.currentPartitionIndex].panels[state.currentPanelIndex])
            eveditingAHUPartitions2[state.currentPartitionIndex].panels[state.currentPanelIndex] = action.data
            // eveditingAHUPartitions2[state.currentPartitionIndex].panels[panel.id] = panel
            //            console.log('rootPanel2',rootPanel2)
            return Object.assign({}, state, {
                // currentEPanel: panel,//当前的大面,一般是6个面
                // currentESubPanel: dividePanel.subPanels[0],//指切割后默认选中的子面
                // currentEParentPanel: dividePanel,//指切割后默认选中的子面的父面
                editingAHUPartitions: eveditingAHUPartitions2,
                lockingPanel: ''
            })
        case PARTITION_SYNCAANDB:
            let eveditingAHUPartitions3 = JSON.parse(JSON.stringify(state.editingAHUPartitions))
            eveditingAHUPartitions3[state.currentPartitionIndex].panels = action.data
            return Object.assign({}, state, {
                editingAHUPartitions: eveditingAHUPartitions3,

            })
        default:
            return state
    }
}

export default partition
