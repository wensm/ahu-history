import {
    LANGUAGE_SETTING,
    LANGUAGE_SELECTION,
    CHINESE,
    CANCEL,
    CONFIRM,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {Field, reduxForm, formValues} from 'redux-form'
import classnames from 'classnames'

class LanguageSetting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {locale: props.locale}
        this.saveLanguageSetting = this.saveLanguageSetting.bind(this);
    }

    saveLanguageSetting(values) {
        this.props.onUpdateLanguage(values);
        $('#LanguageSetting').modal('hide');
    }

    render() {
        const {handleSubmit, pristine, reset, submitting, dirty} = this.props
        return (
            <div className="modal fade" data-name="LanguageSetting" id="LanguageSetting" tabIndex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{width: '400px'}}>
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel"><b>{intl.get(LANGUAGE_SETTING)}</b></h4>
                        </div>
                        <form data-name="languageSetting" onSubmit={handleSubmit(this.saveLanguageSetting)}>
                            <div className="modal-body"
                                 style={{maxHeight: '400px', overflow: 'hidden', padding: '0px'}}>

                                <div style={{width: '400px', height: '300px', overflow: 'auto'}}>
                                    <div style={{padding: '20px'}}>
                                        <div style={{marginTop: '20px', marginBottom: '20px'}}>
                                            <div style={{float: 'left'}}>
                                                <div className="checkbox">
                                                    <label>{intl.get(LANGUAGE_SELECTION)}</label>
                                                    <label>
                                                        <Field
                                                            name="locale"
                                                            component="input"
                                                            type="radio"
                                                            value="zh-CN"
                                                        />{' '}
                                                        {intl.get(CHINESE)}
                                                    </label>
                                                    <label>
                                                        <Field
                                                            name="locale"
                                                            component="input"
                                                            type="radio"
                                                            value="en-US"
                                                        />{' '}
                                                        English
                                                    </label>
                                                    <label>
                                                        <Field
                                                            name="locale"
                                                            component="input"
                                                            type="radio"
                                                            value="ms-MY"
                                                        />{''}
                                                        Malysia
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
                                <button type="submit"  disabled={!dirty} className="btn btn-primary">{intl.get(CONFIRM)}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default reduxForm({
    form: 'locale', // a unique identifier for this form 23:40
    enableReinitialize: true,
})(LanguageSetting)
