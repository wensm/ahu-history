import React from 'react'
import style from './Header.css'
import logo from '../images/LOGObg.png'

export default class Header extends React.Component {
  render() {
    return (
      <div className={style.Header}>
        <img src={logo} />
      </div>
    )
  }
}
