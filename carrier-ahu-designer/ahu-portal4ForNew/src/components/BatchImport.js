import {
  PLEASE_CHOOSE,
  IMPORT_UNIT,
  IMPORT_PROJECT,
  CHOOSE_FILE,
  BROWSING,
  OVERRIDE_EXISTING_PARAMETER,
  OVERRIDE_EXISTING_PROJECT,
  CANCEL,
  IMPORT,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import Select from 'react-select';
import SelectStyle from 'react-select/dist/react-select.min.css';
import { Spin } from 'antd'


export default class BatchImport extends React.Component {
  constructor(props) {
    super(props);
    this.selFileBtn = this.selFileBtn.bind(this);
    this.selFileChange = this.selFileChange.bind(this);
    this.saveBatchImport = this.saveBatchImport.bind(this);
    this.setCoverItem = this.setCoverItem.bind(this);

    this.state = {
      selectedCoverId: "",
      fileName: "",
      imported: false,
      msg: ""
    };
  }
  componentDidMount() {
    if (this.state.imported) {
      this.setState({
        imported: false
      })
    }
  }
  selFileBtn() {
    $("#configInputFile").click();
  }

  selFileChange() {
    var value = $("#configInputFile").val() || '';
    var indenNum = value.lastIndexOf('\\');
    var fileName = value.substring((indenNum + 1));
    //$("#fileInput").val(fileName);
    this.setState({
      fileName: fileName
    });
    return true;
  }

  saveBatchImport() {
    var groupId = $("#BatchImport").data("groupId");
    var formData = new FormData();
    var coverIdOrNeedCover = this.props.coverOption ? this.refs.coverSelect.checked : this.state.selectedCoverId;
    formData.append("myfile", document.getElementById("configInputFile").files[0]);
    this.setState({
      imported:true
    })
    this.props.saveBatchImport(formData, groupId, coverIdOrNeedCover, (msg)=>{
        this.setState({
            imported:false,
            msg: msg
        })
        if(!msg) {
            $('#BatchImport').modal('hide')
        }
    });
  }

  setCoverItem(selectedOption) {
    // console.log('selectedOption:'+JSON.stringify(selectedOption));

    this.setState({ selectedCoverId: selectedOption ? selectedOption.value : '' });
  }

  render() {
    const { coverList, coverId, coverName, coverOption, projectId } = this.props;
    let { imported } = this.state
    let selectOptions = [{
      value: "",
      label: intl.get(PLEASE_CHOOSE)
    }];
    (coverList || []).forEach((a) => {
      let op = { value: a[coverId || 'value'], label: a[coverName || 'label'] };
      selectOptions.push(op);
    });

    return (
      <div className="modal fade" id="BatchImport" data-name="BatchImport" tabIndex="-1" role="dialog"
        aria-labelledby="myModalLabel" data-groupid="">
        <Spin
          spinning={imported}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                  aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>{projectId ? intl.get(IMPORT_UNIT) : intl.get(IMPORT_PROJECT)}</b>
                </h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-3">
                    <label style={{ lineHeight: '32px' }}>{intl.get(CHOOSE_FILE)}</label>
                  </div>
                  <div className="col-lg-6">
                    <input className="form-control" id="fileInput" readOnly value={this.state.fileName} name="trueattachment"
                      type="text" />
                    <input style={{ display: 'none' }} type="file" id="configInputFile"
                      onChange={this.selFileChange} />
                  </div>
                  <div className="col-lg-2">
                    <button className="btn btn-default" type="button"
                      onClick={this.selFileBtn}>{intl.get(BROWSING)}</button>
                  </div>
                </div>
                {
                  coverOption && <div className="row" style={{
                    marginTop: '10px',
                    marginBottom: '10px'
                  }}>
                    <div className="col-lg-12">

                      <label style={{ lineHeight: '32px' }}>
                        <input style={{ marginRight: '5px' }} ref="coverSelect" type="checkbox" />
                        {intl.get(OVERRIDE_EXISTING_PARAMETER)}</label>
                    </div>
                  </div>
                }
                {
                  coverList && coverList.length > 0 && (<div className="row" style={{
                    marginTop: '10px',
                    marginBottom: '10px'
                  }}>
                    <div className="col-lg-3">
                      <label style={{ lineHeight: '32px' }}>{intl.get(OVERRIDE_EXISTING_PROJECT)}</label>
                    </div>
                    <div className="col-lg-6">
                      <Select
                        value={this.state.selectedCoverId}
                        onChange={this.setCoverItem}
                        options={selectOptions}
                        pageSize={5}
                      />
                    </div>
                  </div>)
                }


              </div>
              <div className="modal-footer">
                <div className="col-lg-8" style={{ textAlign: 'left' }}>
                    <label style={{ lineHeight: '32px' }}>{this.state.msg}</label>
                </div>
                <div>
                  <button type="button" className="btn btn-default"
                    data-dismiss="modal">{intl.get(CANCEL)}</button>
                  <button type="button" className="btn btn-primary"
                    onClick={this.saveBatchImport}>{intl.get(IMPORT)}</button>
                </div>
              </div>
            </div>
          </div>
        </Spin>
      </div>
    );
  }
}
