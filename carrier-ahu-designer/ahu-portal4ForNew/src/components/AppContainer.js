import {connect} from 'react-redux'

import {fetchLayout, fetchMetaUnit,fetchMetaValidation, fetchSerial, fetchVersion} from '../actions/metadata'
import {fetchUserInfo,fetchtoken,checkSystemUpdate, fetchUserMeta, fetchPriceBase, fetchEdition} from '../actions/general'
import {fetchComponentDefaultValue, fetchAhuBaseSections, fetchAhuTemplates} from '../actions/ahu'
import APP from './App'

const mapStateToProps = state => {
    return {
        locale: state.general.user.preferredLocale
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchMetadata: () => {
            dispatch(fetchEdition())
            dispatch(fetchLayout())//后端excel的详细信息
            dispatch(fetchMetaUnit())
            dispatch(fetchtoken())
            dispatch(fetchComponentDefaultValue())
            dispatch(fetchAhuBaseSections())//左侧段和上面可拖动的段
            dispatch(fetchAhuTemplates())
            dispatch(fetchPriceBase())
            setTimeout(() => {
                // dispatch(fetchMetaValidation())//该方法不做延迟，多语言就获取不到
                dispatch(fetchSerial())// zzf 型号和段联动
                dispatch(fetchUserMeta())//  获取用户个人设置
            }, 3000);
            
            dispatch(fetchVersion())//  获取版本号

            // dispatch(fetchAhuTemplate())
        },
        onFetchUserInfoAndIntl: (locale, initIntl) => {
            dispatch(fetchUserInfo(locale, initIntl))
        },
        onCheckSystemUpdate: (lang, showSystemUpdate) => {
            dispatch(checkSystemUpdate(lang, showSystemUpdate))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APP)

