import {connect} from 'react-redux'
import UnitSetting from './UnitSetting'
import {
    saveCreateReport,
    clearReports
} from '../actions/projects'
import {
    updateUnit,
    updateUserUnitToServer
} from '../actions/general'


const mapStateToProps = (state, owProps) => {
    let tempUnits = [];
    for (var key in state.general.user.unitPrefer) {
        tempUnits.push({key: key, value: state.general.user.unitPrefer[key]})
    }
    return {
        unitPrefer: tempUnits,
        initialValues: state.general.user.unitPrefer,
        metadata: state.metadata
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onUpdateUnitPrefer(param) {
            dispatch(updateUnit(param))
            dispatch(updateUserUnitToServer(param))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UnitSetting)
